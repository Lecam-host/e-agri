import 'package:eagri/app/di.dart';
import 'package:get/get.dart';

import '../domain/model/location_model.dart';
import '../domain/usecase/speculation_price_usecase.dart';

class LocationController extends GetxController {
  SpeculationPriceUseCase speculationPriceUseCase =
      instance<SpeculationPriceUseCase>();

  var listDepartement = <DepartementModel>[].obs;
  var listeSousPrefecture = <SousPrefectureModel>[].obs;
  var listLocalite = <LocaliteModel>[].obs;

  var listeDepartementMap = <Map<String, dynamic>>[];
  var listRegionMap = <Map<String, dynamic>>[];
  var listeSousPrefectureMap = <Map<String, dynamic>>[];
  var listLocaliteMap = <Map<String, dynamic>>[];
   var listRegion = <RegionModel>[];

  Future getRegion() async {
    (await speculationPriceUseCase.getListRegion()).fold((failure) {},
        (result) {
           listRegion =result.data;
      listRegionMap = result.data
          .map((element) => {"value": element.regionId, "label": element.name})
          .toList();
    });
  }

  @override
  void onInit() {
    getRegion();
    super.onInit();
  }
}
