import 'package:eagri/app/di.dart';
import 'package:eagri/domain/model/user_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../app/app_prefs.dart';
import '../data/repository/api/repository_impl.dart';
import '../domain/model/user_stat_model.dart';
import '../domain/usecase/login_usecase.dart';

class UserController extends GetxController {
  var userInfo = UserModel().obs;
  AppSharedPreference appSharedPreference = instance<AppSharedPreference>();
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  var userStat = UserStatModel().obs;

  getUserStat() async{
    
      appSharedPreference.getUserInformation().then((value) {
      repositoryImpl.getUserStat(value.id!).then((response) {
      response.fold((failure) => null, (statData) {
        userStat.value = statData.data;
      });
    });
     });
    
  }

  getUserInfo() async {
    userInfo.value = await appSharedPreference.getUserInformation();
  }

  updateUserInfo(BuildContext context) async {
    LoginUseCase loginUseCase = instance<LoginUseCase>();
    await appSharedPreference.getUserInformation().then((userActualy) async {
      if (userActualy.isConnected == true) {
        await loginUseCase.getAccountInfoByToken().then((response) async {
          await response.fold((failure) {}, (accountData) async {
            await appSharedPreference.updateScope(
                Get.context!, accountData.data.scopes);
          });
        });
        await loginUseCase
            .getUserInfo(
          userActualy.id!,
        )
            .then((response) async {
          await response.fold((l) {}, (newUserInfo) async {
            await appSharedPreference.updateLoginSession(
                Get.context!, newUserInfo);
          });
        });
      }
      // await appSharedPreference
      //     .getUserInformation()
      //     .then((value) => print(value.scopes));
    });
  }

  @override
  void onInit() {
    getUserInfo();
    super.onInit();
  }

  init()  {
    getUserInfo();
  }
}
