import 'package:eagri/data/repository/api/repository_impl.dart';
import 'package:get/get.dart';

import '../app/di.dart';
import '../data/repository/local/speculation_repo.dart';
import '../domain/model/category_model.dart';
import '../domain/model/model.dart';
import '../domain/usecase/speculation_price_usecase.dart';

class SpeculationController extends GetxController {
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  var listCategorys = <CategoryModel>[].obs;
  var listCategorysMap = <Map<String, dynamic>>[].obs;
  var listSpeculation = <SpeculationModel>[].obs;
  var listSpeculationOnline = <SpeculationModel>[].obs;

  var listeSpeculationMap = <Map<String, dynamic>>[].obs;
  SpeculationPriceUseCase speculationPriceUseCase =
      instance<SpeculationPriceUseCase>();
  fetchSpeculation() async {
    listSpeculation.value = await SpeculationRepo().getAllItem();
    listeSpeculationMap.value = listSpeculation
        .map((e) => {"value": e.speculationId, "label": e.name})
        .toList();
  }

  @override
  void onInit() async {
    await getOnlineListSpeculation();

    fetchSpeculation();
    getListCategoryProduct();
    super.onInit();
  }

  // addFavorite(List<int> speculationIds) {
  //   repositoryImpl
  //       .addFavorite(speculationIds)
  //       .then((response) => response.fold((failure) => null, (data) {
  //             data;
  //           }));
  // }

  getOnlineListSpeculation() async {
    await speculationPriceUseCase
        .getListSpeculation()
        .then((value) => value.fold((l) {}, (r) {
              listSpeculationOnline.value = r.data!;
            }));
  }

  getListCategoryProduct() async {
    repositoryImpl.getListCategoryProduct().then((response) {
      response.fold((l) => null, (listCategorieResponse) {
        listCategorys.value = listCategorieResponse.data;
        listCategorysMap.value = listCategorys
            .map((e) => {"value": e.categoryId, "label": e.name})
            .toList();
      });
    });
    listSpeculation.value = await SpeculationRepo().getAllItem();
  }
}
