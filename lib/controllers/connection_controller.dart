import 'dart:developer';

import 'package:another_flushbar/flushbar.dart';
import 'package:dio/dio.dart';

import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:internet_connection_checker/internet_connection_checker.dart';

showCustomFlushbarReseau(BuildContext context, String message,
    {int? duration}) {
  return Flushbar(
    maxWidth: 300,
    flushbarPosition: FlushbarPosition.TOP,
    duration: Duration(seconds: duration ?? 5),
    margin: const EdgeInsets.all(10),
    padding: const EdgeInsets.all(10),
    borderRadius: BorderRadius.circular(15),
    backgroundColor: ColorManager.darkGrey,
    icon: Icon(Icons.wifi, color: ColorManager.white),
    boxShadows: const [
      BoxShadow(
        color: Colors.black45,
        offset: Offset(3, 3),
        blurRadius: 3,
      ),
    ],
    dismissDirection: FlushbarDismissDirection.HORIZONTAL,
    forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
    // title: '',
    message: message,
  ).show(context);
}

class ConnectionController extends GetxController {
  InternetConnectionChecker networkInfo = InternetConnectionChecker();
  var connectReseau = true.obs;
  var isCheckNetwork = false.obs;

  checkNetworkQuality() {
    // checkNetwork();
    InternetConnectionChecker()
        .onStatusChange
        .listen((InternetConnectionStatus status) {
      isNotConected();
    });
  }

  isNotConected() async {
    if (await networkInfo.hasConnection == false) {
      connectReseau.value = false;
      showCustomFlushbarReseau(
        Get.context!,
      "Aucune ou mauvaise connexion",
      );
    } else {
      connectReseau.value = true;
    }
  }

  checkNetwork() async {
    if (isCheckNetwork.value == false) {
      int responseTime = 5000;
      connectReseau.value = true;
      isCheckNetwork.value = true;
      if (await networkInfo.hasConnection == true) {
        Dio dio = Dio();

        final stopwatch = Stopwatch()..start();
        try {
          var response = await dio.get('https://www.google.com');
          stopwatch.stop();

          if (response.statusCode == 200) {
            final latency = stopwatch.elapsedMilliseconds;

            if (latency >= responseTime) {
              showCustomFlushbarReseau(
                Get.context!,
                "Reseau instable",
              );
            } else {}
            //  return latency < responseTime;
          } else {
            showCustomFlushbarReseau(
              Get.context!,
              "Reseau instable",
            );
          }
        } catch (e) {
          showCustomFlushbarReseau(
            Get.context!,
            "Reseau instable",
          );
        }
      } else {
        connectReseau.value = false;
      }
    }
    log("message");
    isCheckNetwork.value = false;
  }
}
