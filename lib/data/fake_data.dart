import 'package:eagri/domain/model/location_model.dart';

import '../domain/model/model.dart';

class FakeData {
  static RegionModel region = RegionModel(regionId: 1, name: "Sud-comoe");
  static DepartementModel departement = DepartementModel(
    departementId: 1,
    name: "Aboisso",
    region: region,
  );
  static SousPrefectureModel sousPrefecture = SousPrefectureModel(
      sousPrefectureId: 1, name: "Mafere", departement: departement);
  static LocaliteModel localite = LocaliteModel(localiteId: 1, name: "Aby");

  static List<SelectObjectModel> speculationList = [
    SelectObjectModel(id: 1, name: "Tomate"),
  ];
  static List<SelectObjectModel> uniteList = [
    SelectObjectModel(id: 1, name: "Kg"),
  ];
  static List<SelectObjectModel> regionList = [
    SelectObjectModel(id: 1, name: "Sud-comoé"),
  ];
  static List<SelectObjectModel> departementList = [
    SelectObjectModel(id: 1, name: "Aboisso"),
  ];
  static List<SelectObjectModel> sousprefectureList = [
    SelectObjectModel(id: 1, name: "Maféré"),
  ];
  static List<SelectObjectModel> localiteList = [
    SelectObjectModel(id: 1, name: "Aby"),
  ];
  static List<SpeculationModel> lisspeculation = [
    SpeculationModel(
      imageUrl:
          "https://images.pexels.com/photos/239587/pexels-photo-239587.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
      name: "Piment",
      speculationId: 1,
    ),
  ];
  static ListSpeculationPriceModel listSpeculationFakeData =
      ListSpeculationPriceModel(listSpeculationPrice: [
    SpeculationPriceModel(
      priceInfo: PriceModel(
        prixGros: 2000,
        prixDetails: 2300,
        regionId: 1,
        departementId: 1,
        sousPrefectureId: 1,
        localiteId: 1,
      ),
      speculationInfo: SpeculationModel(
        imageUrl:
            "https://images.pexels.com/photos/239587/pexels-photo-239587.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
        name: "Piment",
        speculationId: 1,
      ),
      locationInfo: LocationModel(
        region: region,
        departement: departement,
        sousPrefecture: sousPrefecture,
        localiteId: localite.localiteId,
        localiteName: localite.name,
      ),
    ),
    SpeculationPriceModel(
      priceInfo: PriceModel(
        prixGros: 2500,
        prixDetails: 2300,
        regionId: 1,
        departementId: 1,
        sousPrefectureId: 1,
        localiteId: 1,
      ),
      speculationInfo: SpeculationModel(
        imageUrl:
            "https://images.pexels.com/photos/1327838/pexels-photo-1327838.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
        name: "Tomate",
        speculationId: 1,
      ),
      locationInfo: LocationModel(
        region: region,
        departement: departement,
        sousPrefecture: sousPrefecture,
        localiteId: localite.localiteId,
        localiteName: localite.name,
      ),
    ),
  ]);
  // ListAdviceModel? listCaterory = ListAdviceModel([
  //   AdviceModel(
  //     1,
  //     "Arroser",
  //     "description",
  //     "2023-01-18T15:30:07.813052",
  //     [],
  //     "",
  //     [],
  //     3,
  //     "",
  //   ),
  //   AdviceModel(
  //       2,
  //       "Comment arroser une plante",
  //       AppStrings.lorem,
  //       "2023-01-18T15:30:07.813052",
  //       [
  //         FileModel(
  //           1,
  //           "Fichier pdf",
  //           AppStrings.lorem,
  //           1,
  //           "http://africau.edu/images/default/sample.pdf",
  //         ),
  //         FileModel(
  //           2,
  //           "Fichier video",
  //           AppStrings.lorem,
  //           2,
  //           "https://flutter.github.io/assets-for-api-docs/assets/videos/bee.mp4",
  //         ),
  //         FileModel(
  //           3,
  //           "Fichier Audio",
  //           AppStrings.lorem,
  //           3,
  //           "http://5.196.8.86:8810/microassur-app/public/audio/audio-62f4de90e19e0.mp3",
  //         )
  //       ],
  //       "",
  //       [],
  //       1,
  //       "")
  // ]);
}
