class ColumnsPropertiesName {
  static const String apiId = "apiId";
  static const String categorieSpeculation = "categorieSpeculation";

  static const String longitude = "lon";
  static const String latitude = "lat";
  static const String name = "name";
  static const String data = "data";
}
