// ignore_for_file: file_names

import '../responses/json_properties_name.dart';
import 'helpers_db.dart';

class ColumnsTable {
  static const String columsTableAdviceCategory =
      "${JsonPropertiesName.id} INTEGER,${JsonPropertiesName.code} INTEGER,${JsonPropertiesName.label} CHAR,${JsonPropertiesName.description} CHAR";

  static const String columsTableParcel = " ${ColumnsPropertiesName.data} BLOB";
  static const String speculationColumns =
      "${ColumnsPropertiesName.apiId} INTEGER ,${ColumnsPropertiesName.data} BLOB";
  static const String intrantColumns =
      "${ColumnsPropertiesName.apiId} INTEGER ,${ColumnsPropertiesName.data} BLOB";
}
