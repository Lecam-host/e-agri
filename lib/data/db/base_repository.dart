import 'base_entity.dart';
import 'data_base_helpers.dart';

abstract class BaseRepository<T extends BaseEntity, PK> {
  String tableName;

  BaseRepository(this.tableName);

  T getEntity();

  Future<bool> save(T t) async {
    try {
      var dbClient = await DatabaseHelper.database;

      int res = await dbClient.insert(tableName, t.toDatabase());

      return res > 0 ? true : false;
    } catch (e) {
      return false;
    }
  }

  Future<bool> rawDelete() async {
    var dbClient = await DatabaseHelper.database;
    int res = await dbClient.rawDelete(tableName);
    return res > 0 ? true : false;
  }

  Future<bool> update(T t, PK k) async {
    var dbClient = await DatabaseHelper.database;

    int res = await dbClient.update(tableName, t.toDatabase(),
        where: "id$tableName = ?", whereArgs: <PK>[k]);

    return res > 0 ? true : false;
  }

  Future<bool> deleteElement(T t) async {
    var dbClient = await DatabaseHelper.database;
    int res = await dbClient.delete(tableName);
    return res > 0 ? true : false;
  }

  Future<bool> updateFromAnotherKey(T t, argument) async {
    var dbClient = await DatabaseHelper.database;
    int res = await dbClient.update(tableName, t.toDatabase(),
        where: "$argument = ?", whereArgs: [argument]);
    if (tableName.compareTo("AppAnnoncev") == 0) {}
    return res > 0 ? true : false;
  }

  Future<List<T>> getAll() async {
    var dbClient = await DatabaseHelper.database;
    var res = await dbClient.rawQuery('SELECT * FROM $tableName');

    return List.generate(res.length, (i) {
      return getEntity().fromDatabase(res[i]);
    });
  }

  Future<List<T>> list() async {
    var dbClient = await DatabaseHelper.database;
    final List<Map<String, dynamic>> maps = await dbClient.query(' $tableName');
    List<T> lis = List.generate(maps.length, (i) {
      return getEntity().fromDatabase(maps[i]);
    });
    return lis;
  }

  Future<int> delete(PK k, String field) async {
    var dbClient = await DatabaseHelper.database;
    int res = await dbClient
        .rawDelete('DELETE FROM $tableName WHERE $field = ?', [k]);
    return res;
  }

  Future<int> count() async {
    var dbClient = await DatabaseHelper.database;

    final data = await dbClient.rawQuery('''SELECT * FROM $tableName''');

    return data.length;
  }
}
