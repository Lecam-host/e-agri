// ignore_for_file: empty_catches

import 'package:eagri/app/constant.dart';
import 'package:eagri/app/functions.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'columnsTable.dart';
import 'table_name.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;

  static Database? _db;

  static Future<Database> get database async {
    return _db ?? await initDb();
  }

  DatabaseHelper.internal();

  static initDb() async {
    //io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String documentsDirectory = await getDatabasesPath();
    String path = join(documentsDirectory, Constant.localBDName);
    var theDb = await openDatabase(path,
        version: 1, onCreate: _onCreate, onUpgrade: _onUpgrade);
    return theDb;
  }

  static void _onCreate(Database db, int version) async {
    String createAdviceCategoryTable = createTableInBD(
        TableName.adviceCategorieTable, ColumnsTable.columsTableAdviceCategory);
    String createParcelTable =
        createTableInBD(TableName.parcelTable, ColumnsTable.columsTableParcel);
    String createSpeculationTable = createTableInBD(
        TableName.speculationTable, ColumnsTable.speculationColumns);

    String createIntrantTable =
        createTableInBD(TableName.intrantTable, ColumnsTable.intrantColumns);
    try {
      await db.execute(createAdviceCategoryTable);
      await db.execute(createParcelTable);

      await db.execute(createSpeculationTable);
      await db.execute(createIntrantTable);
    } catch (error) {}
  }

  static void _onUpgrade(Database db, int oldVersion, int newVersion) async {
    try {} catch (error) {}
  }
}
