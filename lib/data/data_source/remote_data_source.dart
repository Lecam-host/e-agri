import 'dart:convert';
import 'dart:io';

import 'package:eagri/data/request/history_vente_achat.dart';
import 'package:eagri/data/request/prestation_request.dart';
import 'package:eagri/data/request/request_offre_credit_model.dart';
import 'package:eagri/data/request/shop_resquests.dart';
import 'package:eagri/data/request/transport_resquest.dart';
import 'package:eagri/data/responses/shop_response.dart';
import 'package:eagri/domain/model/alerte_model.dart';

import 'package:eagri/domain/model/category_model.dart';
import 'package:eagri/domain/model/command_model.dart';

import 'package:eagri/domain/model/creditor_model.dart';
import 'package:eagri/domain/model/favorite_model.dart';
import 'package:eagri/domain/model/history_achat_model.dart';
import 'package:eagri/domain/model/info_by_id_model.dart';
import 'package:eagri/domain/model/insurer_model.dart';
import 'package:eagri/domain/model/offre_insurance_model.dart';
import 'package:eagri/domain/model/intrant_model.dart';
import 'package:eagri/domain/model/methode_paiement_model.dart';
import 'package:eagri/domain/model/paiement_model.dart';
import 'package:eagri/domain/model/typeCulture_model.dart';
import 'package:eagri/domain/model/type_offre__insurance_model.dart';
import 'package:eagri/domain/model/user_info_model.dart';
import 'package:intl/intl.dart';

import '../../domain/model/achat_or_preachat_model.dart';
import '../../domain/model/cart_model.dart';

import '../../domain/model/mes_produits_commander_model.dart';
import '../../domain/model/model.dart';
import '../../domain/model/odered_product_credit_model.dart';
import '../../domain/model/offfre_item_model.dart';
import '../../domain/model/offre_credit_model.dart';
import '../../domain/model/history_vente_model.dart';
import '../../domain/model/insurance_by_insurer_model.dart';
import '../../domain/model/prestation_model.dart';
import '../../domain/model/rate_model.dart';
import '../../domain/model/shop_model.dart';
import '../../domain/model/transport_model.dart';
import '../../domain/model/type_offre_credit_model.dart';
import '../../domain/model/user_stat_model.dart';
import '../network/app_api.dart';
import '../request/intran_request.dart';
import '../request/notation_request.dart';
import '../request/paiement_request.dart';
import '../request/request.dart';
import '../request/request_offre_insurance_model.dart';
import '../responses/account_response.dart';
import '../responses/advice_response.dart';
import '../responses/lieu_responses.dart';
import '../responses/local/local_responses.dart';
import '../responses/responses.dart';
import '../responses/weather_response.dart';

abstract class RemoteDataSource {
  Future<AuthResponse> login(LoginRequest loginRequest);
  Future<BaseResponse> saveRegistrationId(SaveRegistrationIdRequest request);
  Future<AllAdviceDataResponse> getAllAdvice(GetListAdviceRequest request);

  Future<DetailAdivceResponse> detailAdvice(int adviceId);
  Future<AllAdviceCategoryResponse> getAllAdviceCategorie();
  Future<AllAdviceDataResponse> searchAdvice(
      SearchAdviceRequest searchAdviceRequest);
  Future<AskAdviceResponse> askAdvice(AskAdviceRequest askAdviceRequest);
  Future<WeatherResponse> getCurrentWeather(GetWeatherRequest location);
  Future<GetSpeculationPriceResponse> getListSpeculationPrice(
      GetListSpeculationPriceRequest request);
  Future<GetSpeculationPriceNonOfficielResponse>
      getListSpeculationPriceEnterByUser(
          GetListSpeculationPriceRequest request);
  Future<BaseResponse> addSpeculationPrice(AddSpeculationPriceRequest request);
  Future<ListRegionResponse> getListRegion();
  Future<ListSpeculationResponse> getListSpeculation();
  Future<GetSpeculationPriceResponse> getSpeculationPriceHistory(
      SpeculationPriceRequest request);

  Future<ListDepartementResponse> getListDepartementOfRegion(int idregion);
  Future<ListSousPrefectureResponse> getListSousPrefectureOfDepartement(
      int idDepartement);
  Future<ListLocaliteResponse> getListLocaliteOfSousPrefecture(
      int idSousPrefecture);
  Future<GetSpeculationPriceResponse> compareSpeculationPrice(
      CompareSpeculationPriceRequest request);
  Future<AddParcelInfoResponse> getParcelInfo(String parcelId);
  Future<AddParcelInfoResponse> createParcel(CreateParcelRequest request);
  Future<ListHistoryWeatherInfoResponse> historyWeather(
      HistoryWeatherRequest request);
  Future<AllConfigResponse> getAllConfig();
  Future<AddAlertResponse> addAlert(AddAlertRequest request);

  Future<AlerteModel> getListAlert();

  Future<ListAskAdviceResponse> getListAskAdviceOfUser(
      GetUserAskAdviceRequest request);
  Future<ListLocationForWeatherResponse> getListLocationForWeather();
  Future<ListParcelResponse> getUserParcelle(int userId);
  Future<CreateFullParcelResponse> createFullParcel(
      List<CreateParcelRequest> listParcelle);
  Future<GetUserInfoResponse> getUserInfo(int id);
  Future<GetMinAndMaxSpeculationPriceResponse> getMinAndMaxSpeculationPrice(
      GetMinAndMaxSpeculationnPriceRequest request);

  Future<GetListRecommandationResponse> getListRecommandation(
      GetRecommandationRequest request);
  Future<AddAdviceResponse> addAdVice(AddAdviceRequest request);
  Future<AllAdviceDataResponse> getUserAskAdviceAnswer(
      GetUserAskAdviceAnswerRequest request);

  Future<AddProductOnMarketResponse> addProductOnMarket(
      AddProductOnMarketrequest request);
  Future<GetDetailProductResponse> updateProductOnMarket(
      UpdateProductOnMarketrequest request);

  Future<GetDetailProductResponse> disableProduct(int idProduct);
  Future<GetDetailProductResponse> enableProduct(int idProduct);
  Future<ListStringResponse> getListTypeVente();
  Future<ListStringResponse> getListOffersType();

  Future<BaseResponse> addAlertFiles(int idAlert, List<File> file);

  Future<BodyGetListProductResponse> getListProduct();
  Future<GetListDomaineResponse> getAlertDomain();

  Future<DataProductUserResponse> getListProductFournisseur(int id);
  Future<DataProductUserResponse> getListProductFournisseurWithCode(
      int id, int code);
  Future<AddProductInShoppingCartResponse> addProductInShoppingCart(
      AddProductInShoppingCartsRequest request);
  Future<InfoByIdModel> getInfoById(InfoByIdRequest request);

  Future<ListCartResponse> getUserCart(int userId);
  Future<BaseResponse> deleteCatalogueProduct(int idProduct);
  Future<BaseResponse> addRate(AddRateRequest notationRequest);
  Future<CheckRateResponse> checkRating(CheckRateRequest request);
  Future<CreateCommandModel> createCommand(int userId, int cartId);
  Future<GetUserCommands> getUserCommand(int userId);
  Future<BaseResponse> deleteProdIncart(int cartId, int itemId);
  Future<CreatePaiementResponse> createPaiement(CreatePaiementRequest request);
  Future<CreatePaiementResponse> createPaiementCredit(
      String reference, int orderId, String motif, String phone);

  Future<ConfirmPaymentResponse> confirmPaiement(String signature, otp);
  Future<GetDetailProductResponse> getDetailProduct(int idProduct);

  Future<VerificationPaymentResponse> verificationPayment(String signature);
  Future<MethodPaiementModel> getMethodPaiementAll();

  Future<BodyGetListProductResponse> searchProduct(
      SearchProductRequest request);

  Future<AddUserFavoriteResponse> addFavorite(List<int> speculationId);
  Future<ListUserFavoriteResponse> getUserFavorite();
  Future<GetCommandFactureResponse> getCommandFacture(int orderId);
  Future<GetListCommentOnProductResponse> getProductComment(
      ProductListCommentRequest request);
  Future<BaseResponse> notifyConsultaionProduct(
      ConsultationProductRequest request);
  Future<HistoryAchatResponse> getUserHistoryAchat(
      GetUserHistoryAchatVente request);
  Future<GetListCategoryProductResponse> getListCategoryProduct();
  Future<HistoryVenteResponse> getUserHistoryVente(
      GetUserHistoryAchatVente request);
  Future<GetNbOffreAchatBySpeculation> getListNbOfferAchatBySpeculation();
  Future<GetListOffreAchatFind> getListOffreAchatFind(
    int idProduct,
  );
  Future<BaseResponse> deleteCommand(
    int commandId,
  );
  Future<BaseResponse> deleteFavorite(
    int favoriteId,
  );

  Future<BaseResponse> addIntrant(AddIntrantRequestModel intrantData);

  Future<TypeCultureModel> getTypeCulturesAll();
  Future<IntrantModel> getIntrantAll();
  Future<GetListIntrantResponse> searchProductIntrant(
      SearchProductRequest request);
  Future<GetListCategoryPrestation> getListCategoryPrestation();
  Future<BaseResponse> addPrestation(AddPrestationRequest request);
  Future<BaseResponse> demandePrestation(DemandePrestionRequest request);
  Future<GetListPrestationResponse> getDemandePrestationRecu(
    int userId,
  );
  Future<GetListPrestationResponse> getDemandePrestationSend(
    int userId,
  );
  Future<BaseResponse> validateDemandePrestation(
    int prestationId,
  );
  Future<GetUniteResponse> getListPrestationUnite();
  Future<BaseResponse> updatePrestation(
      UpdatePrestationRequest updatePrestationRequest);

  Future<GetListCarResponse> getListCar();

  Future<GetUniteResponse> getListTransportUnite();
  Future<GetUniteResponse> getIntrantUnite();

  Future<BaseResponse> addTransport(AddTransportRequest request);
  Future<BaseResponse> updateTransport(UpdateTransportRequest request);
  Future<BaseResponse> reserveTransport(DemandeTransportRequest request);
  Future<GetDemandeTransport> getListDemandeTransportRecu(int userId);
  Future<GetDemandeTransport> getListDemandeTransportEnvoye(int userId);

  Future<BaseResponse> validDemandeTransport(
    int fournisseurId,
    int reservationId,
  );

  Future<BaseResponse> cancelDemandeTransport(
    int reservationId,
  );

  Future<BaseResponse> validChargement(
    int reservationId,
    String personal,
    String code,
  );
  Future<GetCategoryVehicule> getListCategorieVehicule();

  Future<BaseResponse> acceptPaiementCash(
      int offerId, int productId, bool response, String? message);

  Future<BaseResponse> confirmPaiementCash(
    int demandeId,
    String confirmationCode,
  );

  Future<GetListDemandePaiementCashItem> getListDemandePaimentCash(String role);

  Future<BaseResponse> validContractPdf(
    int userId,
    int offerId,
    bool agreement,
  );

  Future<GetDetailProductResponse> updateProductIntrant(
      UpdateProductOnMarketrequest request);

  Future<GetIntrantCategoryResponse> getIntrantCategorie();

  Future<BaseResponse> deleteFirebaseToken(
      String userName, String firebaseToken);

  Future<OffreInsuranceModel> getInsurance(
    RequestOffreInsuranceModel request,
  );
  Future<InsuranceByInsurerModel> getInsuranceByInsurer(int insurerId);

  Future<OffreCreditModel> getCredit(
    RequestOffreCreditModel request,
  );
  Future<TypeOffreInsuranceModel> getTypeInsurance();
  Future<TypeOffreCreditModel> getTypeCredit();
  Future<OffreCreditModel> getCreditByCreditor(int insurerId);

  Future<CreditorModel> getCreditor();
  Future<InsurerModel> getInsurer();
  Future<BaseResponse> deleteImage(int imageId);
  Future<GetDetailProductResponse> addOtherImage(
    int productId,
    List<File> images,
  );

  Future<GetEntrepriseResponseModel> getEntrepriseDetails(
    int entrepriseId,
  );
  Future<GetEntrepriseResponseModel> getEntrepriseCredit(
    int entrepriseId,
  );

  Future<String?> sendNotif(
    String resourceIds,
    String subject,
    List<String> receivers,
    String title,
    String message,
    String messageDate,
  );

  Future<BaseResponse> updatePassword(
    int id,
    String login,
    String password,
    String oldPassword,
  );

  Future<MesProduitsCommanderResponse> getMyProductOrdered(
    GetProductCommanderRequest request,
  );

  Future<BaseResponse> reductionPrice(
    int commandId,
    int productId,
    double priceReduction,
  );
  Future<BaseResponse> updateUserInfo(
    int id,
    UserInfoModel userInfoModel,
  );
  Future<BaseResponse> sendOtpResetPassword(
    String phoneNumber,
  );
  Future<BaseResponse> resetPassword(
      String phoneNumber, String token, String password);
  Future<AccountInfoResponse> getAccountInfoByToken();
  Future<GetProductOderedInCreditResponse> getProductOderedInCredit(
    String role,
  );
  Future<GetListUnitTimeResponse> getUnitTime();
  Future<BaseResponse> demandePreAchat(
    int fournisseurId,
    int offerId,
  );

  Future<GetDemandeAchatOrPreAchat> getDemandeAchatOrPreAchat(
    int id,
    String customer,
    String type,
  );

  Future<BaseResponse> validedOrInvalidedDemandePreAchat(
    int id,
    bool state,
  );
  Future<GetUserStatResponse> getUserStat(
    int id,
  );
}

class RemoteDataSourceImplement implements RemoteDataSource {
  final AppServiceClient _appServiceClient;
  RemoteDataSourceImplement(this._appServiceClient);
  @override
  Future<AuthResponse> login(LoginRequest loginRequest) {
    return _appServiceClient.login(
      loginRequest.number,
      loginRequest.password,
    );
  }

  @override
  Future<AllAdviceDataResponse> getAllAdvice(GetListAdviceRequest request) {
    return _appServiceClient.allConseils(
      request.perPage,
      request.pageNumber,
    );
  }

  @override
  Future<AllAdviceCategoryResponse> getAllAdviceCategorie() {
    return _appServiceClient.allCategoryAdvices();
  }

  @override
  Future<AllAdviceDataResponse> searchAdvice(
      SearchAdviceRequest searchAdviceRequest) {
    return _appServiceClient.searchAdvice(
        searchAdviceRequest.categoriesId,
        searchAdviceRequest.adviceType,
        searchAdviceRequest.mediaType,
        searchAdviceRequest.keyword,
        searchAdviceRequest.userId);
  }

  @override
  Future<AskAdviceResponse> askAdvice(AskAdviceRequest askAdviceRequest) {
    return _appServiceClient.askAdvice(
        askAdviceRequest.entitiesId,
        askAdviceRequest.categoriesId,
        askAdviceRequest.adviceType,
        askAdviceRequest.mediaType,
        askAdviceRequest.description,
        askAdviceRequest.userId);
  }

  @override
  Future<WeatherResponse> getCurrentWeather(GetWeatherRequest request) {
    return _appServiceClient.getCurrentWeather(request.lat, request.lon);
  }

  @override
  Future<GetSpeculationPriceResponse> getListSpeculationPrice(
      GetListSpeculationPriceRequest request) {
    return _appServiceClient.getListSpeculationPrice(
        request.pageNumber, request.pageSize);
  }

  @override
  Future<GetSpeculationPriceNonOfficielResponse>
      getListSpeculationPriceEnterByUser(
          GetListSpeculationPriceRequest request) {
    return _appServiceClient.getListSpeculationPriceEnterByUser(
        request.pageNumber, request.pageSize);
  }

  @override
  Future<BaseResponse> addSpeculationPrice(AddSpeculationPriceRequest request) {
    return _appServiceClient.addSpeculationPrice(
      request.speculationId,
      request.userId,
      request.from,
      request.to,
      request.unit,
      request.source,
      request.official,
      request.data,
    );
  }

  @override
  Future<ListRegionResponse> getListRegion() {
    return _appServiceClient.getListRegion();
  }

  @override
  Future<ListSpeculationResponse> getListSpeculation() {
    return _appServiceClient.getListSpeculation();
  }

  @override
  Future<GetSpeculationPriceResponse> getSpeculationPriceHistory(
      SpeculationPriceRequest requestBody) {
    return _appServiceClient.getSpeculationPriceInAllRegion(
      requestBody.speculationId,
      requestBody.userId,
      requestBody.regionId,
      requestBody.from,
      requestBody.to,
      requestBody.official!,
    );
  }

  @override
  Future<ListDepartementResponse> getListDepartementOfRegion(int idRegion) {
    return _appServiceClient.getListDepartementOfRegion(idRegion);
  }

  @override
  Future<ListLocaliteResponse> getListLocaliteOfSousPrefecture(
      int idSousPrefecture) {
    return _appServiceClient.getListLocaliteOfSousPrefecture(idSousPrefecture);
  }

  @override
  Future<ListSousPrefectureResponse> getListSousPrefectureOfDepartement(
      int idDepartement) {
    return _appServiceClient.getListSousPrefectureOfDepartement(idDepartement);
  }

  @override
  Future<GetSpeculationPriceResponse> compareSpeculationPrice(
      CompareSpeculationPriceRequest request) {
    return _appServiceClient.compareSpeculationPrice(
      request.speculationId,
      request.locationByid,
      request.from,
      request.to,
      request.userType,
    );
  }

  @override
  Future<AddParcelInfoResponse> getParcelInfo(String parcelId) {
    return _appServiceClient.getParcelInfo(parcelId);
  }

  @override
  Future<AddParcelInfoResponse> createParcel(CreateParcelRequest request) {
    return _appServiceClient.createParcel(
      request.lat,
      request.lon,
      request.userId,
      request.name,
    );
  }

  @override
  Future<CreateFullParcelResponse> createFullParcel(
      List<CreateParcelRequest> request) {
    List<ParcelRequestJson> listParcel = [];
    List.generate(request.length, (i) {
      listParcel.add(ParcelRequestJson()
        ..lat = request[i].lat
        ..lon = request[i].lon
        ..name = request[i].name);
    });

    return _appServiceClient.createFullParcel(listParcel);
  }

  @override
  Future<ListHistoryWeatherInfoResponse> historyWeather(
      HistoryWeatherRequest request) {
    return _appServiceClient.historyWeather(
      request.lat,
      request.lon,
      request.dateDeDebut,
      request.dateDeDeFin,
    );
  }

  @override
  Future<AllConfigResponse> getAllConfig() {
    return _appServiceClient.getConnfig();
  }

  @override
  Future<AddAlertResponse> addAlert(AddAlertRequest request) {
    return _appServiceClient.addAlert(
      request.userId,
      request.regionId,
      request.departementId,
      request.sousprefectureId,
      request.localiteId,
      0,
      request.description,
      request.domain,
    );
  }

  @override
  Future<AlerteModel> getListAlert() async {
    return _appServiceClient.getListAlert();
  }

  @override
  Future<BaseResponse> addAlertFiles(int idAlert, List<File> file) {
    return _appServiceClient.addAlertFile(idAlert, file);
  }

  @override
  Future<ListAskAdviceResponse> getListAskAdviceOfUser(
      GetUserAskAdviceRequest request) {
    return _appServiceClient.getUserListAskAdvice(
        request.userId, request.statutAsk, request.perPage, request.page);
  }

  @override
  Future<ListLocationForWeatherResponse> getListLocationForWeather() {
    return _appServiceClient.getListLocationForWeather();
  }

  @override
  Future<ListParcelResponse> getUserParcelle(int userId) {
    return _appServiceClient.getUserParcel(userId);
  }

  @override
  Future<DetailAdivceResponse> detailAdvice(int adviceId) {
    return _appServiceClient.adviceDetails(adviceId);
  }

  @override
  Future<GetUserInfoResponse> getUserInfo(int id) {
    return _appServiceClient.getUserInfo(id);
  }

  @override
  Future<GetMinAndMaxSpeculationPriceResponse> getMinAndMaxSpeculationPrice(
      GetMinAndMaxSpeculationnPriceRequest request) {
    return _appServiceClient.getMinAndMaxSpeculationPrice(
        request.speculationId, request.official);
  }

  @override
  Future<GetListRecommandationResponse> getListRecommandation(
      GetRecommandationRequest request) {
    return _appServiceClient.getListRecommandation(
      request.speculationId,
      request.temperature,
      request.precipitation,
      request.windSpeed,
      request.humidity,
    );
  }

  @override
  Future<AddAdviceResponse> addAdVice(AddAdviceRequest request) {
    return _appServiceClient.addAdvice(
        adviceType: request.adviceType,
        title: request.title,
        categoryId: request.categoryId,
        adviceText: request.adviceText,
        descriptionFiles: json.encode(
          request.descriptionFiles,
        ),
        userIdForCreation: request.userIdForCreation,
        file1: request.file1,
        file2: request.file2,
        file3: request.file3,
        illustrationImage: request.illustrationImage,
        description: request.description);
  }

  @override
  Future<AllAdviceDataResponse> getUserAskAdviceAnswer(
      GetUserAskAdviceAnswerRequest request) {
    return _appServiceClient.getListAnswerAskAdvice(request.askId);
  }

  @override
  Future<AddProductOnMarketResponse> addProductOnMarket(
      AddProductOnMarketrequest request) {
    return _appServiceClient.appProductOnMarket(
      availability: request.availability,
      description: request.description,
      fournisseurId: request.fournisseurId,
      location: request.location.toJson(),
      offerType: request.offerType,
      price: request.price,
      quantity: request.quantity,
      speculationId: request.speculationId,
      typeVente: request.typeVente,
      unitOfMeasurment: request.unitOfMeasurment,
      images: request.images,
      paymentId: request.paymentId,
      categorieId: request.categorieId,
      availabilityDate: request.availabilityDate,
      numberSlice: request.numberSlice,
      unitTime: request.unitTime,
      pricePreOrder: request.pricePreOrder,
      origin: request.origin,
    );
  }

  @override
  Future<GetDetailProductResponse> updateProductOnMarket(
      UpdateProductOnMarketrequest request) {
    return _appServiceClient.updateProductOnMarket(
      request.idProduct,
      request.idProduct,
      request.fournisseurId,
      request.price,
      request.quantity,
      request.typeVente,
      request.availability,
      request.description,
      request.unitOfMeasurment,
      request.offerType,
      request.availabilityDate,
      // request.images,
      request.locationId,
      request.regionId,
      request.departementId,
      request.sousPrefectureId,
      request.numberSlice,
      request.unitTime,
    );
  }

  @override
  Future<ListStringResponse> getListTypeVente() {
    return _appServiceClient.getListTypesVente();
  }

  @override
  Future<ListStringResponse> getListOffersType() {
    return _appServiceClient.getListOfferTypes();
  }

  @override
  Future<BodyGetListProductResponse> getListProduct() {
    return _appServiceClient.getlistProduct();
  }

  @override
  Future<GetListDomaineResponse> getAlertDomain() {
    return _appServiceClient.getDomaineAlert();
  }

  @override
  Future<DataProductUserResponse> getListProductFournisseur(int id) {
    return _appServiceClient.getlistProductFournisseur(id);
  }

  @override
  Future<DataProductUserResponse> getListProductFournisseurWithCode(
      int id, int code) {
    return _appServiceClient.getlistProductFournisseurWithCode(id, code);
  }

  @override
  Future<AddProductInShoppingCartResponse> addProductInShoppingCart(
      AddProductInShoppingCartsRequest request) {
    return _appServiceClient.addProductInShoppingCart(
      userId: request.userId,
      items: request.items,
    );
  }

  @override
  Future<InfoByIdModel> getInfoById(InfoByIdRequest request) {
    return _appServiceClient.getListInfo(
        request.regionId,
        request.speculation,
        request.userId,
        request.departementId,
        request.sprefectureId,
        request.localiteId);
  }

  @override
  Future<ListCartResponse> getUserCart(int userId) {
    return _appServiceClient.getUserCart(userId);
  }

  @override
  Future<BaseResponse> deleteCatalogueProduct(int idProduct) {
    return _appServiceClient.deleteProduct(idProduct);
  }

  @override
  Future<BaseResponse> addRate(AddRateRequest notationRequest) {
    return _appServiceClient.addNotation(
      notationRequest.userId,
      notationRequest.codeService,
      notationRequest.resourceId,
      notationRequest.noteurId,
      notationRequest.note,
      notationRequest.comment,
    );
  }

  @override
  Future<CheckRateResponse> checkRating(CheckRateRequest request) {
    return _appServiceClient.checkRating(
      request.userId,
      request.ressourceId,
      request.codeService,
    );
  }

  @override
  Future<CreateCommandModel> createCommand(int userId, int cartId) {
    return _appServiceClient.createCommannd(
      userId,
      cartId,
    );
  }

  @override
  Future<GetUserCommands> getUserCommand(int userId) {
    return _appServiceClient.getUserCommand(
      userId,
    );
  }

  @override
  Future<BaseResponse> deleteProdIncart(int cartId, int itemId) {
    return _appServiceClient.deleteProdInCart(cartId, itemId);
  }

  @override
  Future<CreatePaiementResponse> createPaiement(CreatePaiementRequest request) {
    return _appServiceClient.createPaiement(
        request.orderId, request.methodId, request.phone);
  }

  @override
  Future<ConfirmPaymentResponse> confirmPaiement(String signature, otp) {
    return _appServiceClient.confirmPaiement(signature, otp);
  }

  @override
  Future<GetDetailProductResponse> getDetailProduct(int idProduct) {
    return _appServiceClient.getDetailsProduct(idProduct);
  }

  @override
  Future<VerificationPaymentResponse> verificationPayment(String signature) {
    return _appServiceClient.verificationPayment(signature);
  }

  @override
  Future<MethodPaiementModel> getMethodPaiementAll() {
    return _appServiceClient.getMethodPaiementAll();
  }

  @override
  Future<BodyGetListProductResponse> searchProduct(
      SearchProductRequest request) {
    return _appServiceClient.searchProduct(
      request.regionId,
      request.departementId,
      request.locationId,
      request.sprefectureId,
      request.speculationId,
      request.offerType,
      request.status,
      request.priceMin,
      request.priceMax,
      request.quantityMin,
      request.quantityMax,
      request.fournisseurId,
      request.limit,
      request.page,
      request.publicationDateMin,
      request.publicationDateMax,
      request.noteMin,
      request.noteMax,
      request.categorieId,
      request.unitOfMeasurment,
      request.codeTypeProduit,
    );
  }

  @override
  Future<GetListIntrantResponse> searchProductIntrant(
      SearchProductRequest request) {
    return _appServiceClient.searchProductIntrant(
      request.regionId,
      request.departementId,
      request.locationId,
      request.sprefectureId,
      request.offerType,
      request.status,
      request.priceMin,
      request.priceMax,
      request.quantityMin,
      request.quantityMax,
      request.fournisseurId,
      request.limit,
      request.page,
      request.publicationDateMin,
      request.publicationDateMax,
      request.noteMin,
      request.noteMax,
      request.categorieId,
      request.unitOfMeasurment,
      request.codeTypeProduit,
    );
  }

  @override
  Future<AddUserFavoriteResponse> addFavorite(List<int> speculationIds) {
    return _appServiceClient.addFavorite(speculationIds);
  }

  @override
  Future<ListUserFavoriteResponse> getUserFavorite() {
    return _appServiceClient.getUserFavorite();
  }

  @override
  Future<GetCommandFactureResponse> getCommandFacture(int orderId) {
    return _appServiceClient.getCommandFacture(orderId);
  }

  @override
  Future<GetListCommentOnProductResponse> getProductComment(
      ProductListCommentRequest request) {
    return _appServiceClient.getListCommentOnProduct(
        request.resourceId, request.codeService.toString());
  }

  @override
  Future<BaseResponse> notifyConsultaionProduct(
      ConsultationProductRequest request) {
    return _appServiceClient.notifConsultationProduct(
        request.productId,
        DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            .format(request.date.toUtc()));
  }

  @override
  Future<HistoryAchatResponse> getUserHistoryAchat(
      GetUserHistoryAchatVente request) {
    return _appServiceClient.getUserHistoryAchat(
      request.userId,
      request.page,
      request.perPage,
    );
  }

  @override
  Future<HistoryVenteResponse> getUserHistoryVente(
      GetUserHistoryAchatVente request) {
    return _appServiceClient.getUserHistoryVente(
      request.userId,
      request.page,
      request.perPage,
    );
  }

  @override
  Future<GetListCategoryProductResponse> getListCategoryProduct() {
    return _appServiceClient.getListCategoryProduct();
  }

  @override
  Future<GetNbOffreAchatBySpeculation> getListNbOfferAchatBySpeculation() {
    return _appServiceClient.getListNbOfferAchatBySpeculation();
  }

  @override
  Future<GetListOffreAchatFind> getListOffreAchatFind(int idProduct) {
    return _appServiceClient.getListOffreAchatFind(idProduct);
  }

  @override
  Future<BaseResponse> deleteCommand(int commandId) {
    return _appServiceClient.deleteCommand(commandId);
  }

  @override
  Future<BaseResponse> deleteFavorite(int favoriteId) {
    return _appServiceClient.deleteFavorite(favoriteId);
  }

  @override
  Future<GetDetailProductResponse> disableProduct(int idProduct) {
    return _appServiceClient.disableProduct(idProduct);
  }

  @override
  Future<GetDetailProductResponse> enableProduct(int idProduct) {
    return _appServiceClient.enableProduct(idProduct);
  }

  @override
  Future<BaseResponse> addIntrant(AddIntrantRequestModel intrantData) {
    return _appServiceClient.addIntrant(
      intrantData.fournisseurId!,
      intrantData.intrantId!,
      intrantData.intrantCategorieId!,
      intrantData.certification,
      intrantData.availability!,
      intrantData.availabilityDate,
      intrantData.expirationDate,
      intrantData.description,
      intrantData.offerType!,
      intrantData.images,
      intrantData.data!,
      intrantData.numberSlice,
      intrantData.unitTime,
    );
  }

  @override
  Future<TypeCultureModel> getTypeCulturesAll() {
    return _appServiceClient.getTypeCulturesAll();
  }

  @override
  Future<IntrantModel> getIntrantAll() {
    return _appServiceClient.getIntrantAll();
  }

  @override
  Future<GetListCategoryPrestation> getListCategoryPrestation() {
    return _appServiceClient.getCategoryPrestation();
  }

  @override
  Future<BaseResponse> addPrestation(AddPrestationRequest request) {
    return _appServiceClient.addPrestation(
      request.fournisseurId,
      request.categorieId,
      request.speculationId,
      request.unitOfMeasurment,
      request.price,
      request.paymentId.toString(),
      request.availabilityDate,
      request.description,
      request.location.toString(),
      request.images,
      request.availability,
      request.numberSlice,
      request.unitTime,
    );
  }

  @override
  Future<BaseResponse> demandePrestation(DemandePrestionRequest request) {
    final DateFormat formatter = DateFormat('yyyy-MM-dd');

    return _appServiceClient.demandePrestation(
      request.prestationId,
      request.quantity,
      request.userId,
      formatter.format(DateTime.parse(
        request.dateStart,
      )),
      formatter.format(DateTime.parse(
        request.dateEnd,
      )),
      request.location,
    );
  }

  @override
  Future<GetListPrestationResponse> getDemandePrestationRecu(int userId) {
    return _appServiceClient.getDemandePrestationRecu(userId);
  }

  @override
  Future<GetListPrestationResponse> getDemandePrestationSend(int userId) {
    return _appServiceClient.getDemandePrestationSend(userId);
  }

  @override
  Future<BaseResponse> validateDemandePrestation(int prestationId) {
    return _appServiceClient.validateDemandePrestation(prestationId);
  }

  @override
  Future<GetUniteResponse> getListPrestationUnite() {
    return _appServiceClient.getListPrestationUnite();
  }

  @override
  Future<BaseResponse> updatePrestation(
      UpdatePrestationRequest updatePrestationRequest) {
    return _appServiceClient.updatePrestation(
      updatePrestationRequest.prestationId,
      updatePrestationRequest.priceU,
      updatePrestationRequest.availability,
      updatePrestationRequest.availabilityDate,
      updatePrestationRequest.description,
      updatePrestationRequest.location,
    );
  }

  @override
  Future<GetListCarResponse> getListCar() {
    return _appServiceClient.getListCar();
  }

  @override
  Future<GetUniteResponse> getListTransportUnite() {
    return _appServiceClient.getListTransportUnite();
  }

  @override
  Future<BaseResponse> addTransport(AddTransportRequest request) {
    return _appServiceClient.addTransport(
      fournisseurId: request.fournisseurId,
      vehicleId: request.vehiculeId,
      categorieVehicleId: request.categorieId,
      price: request.price.toDouble(),
      capacite: request.capacite,
      availability: request.availability,
      expirationDate: request.expirationDate,
      locationStart: request.locationStart.toJson(),
      locationEnd: request.locationEnd.toJson(),
      paymentId: request.paymentId,
      images: request.images,
      description: request.description,
      availabilityDate: request.availabilityDate,
      hour: request.hour,
      location: request.locationStart.toJson(),
      unitOfMeasurment: request.unitOfMeasurment,
      matriculeVehicle: request.matricule,
    );
  }

  @override
  Future<BaseResponse> updateTransport(UpdateTransportRequest request) {
    return _appServiceClient.updateTransport(
      idProduct: request.idProduct,
      fournisseurId: request.fournisseurId,
      price: request.price != null ? request.price!.toDouble() : null,
      capacite: request.capacite,
      availability: request.availability,
      expirationDate: request.expirationDate,
      locationStart: request.locationStart != null
          ? request.locationStart!.toJson()
          : null,
      locationEnd:
          request.locationEnd != null ? request.locationEnd!.toJson() : null,
      paymentId: request.paymentId,
      description: request.description,
      availabilityDate: request.availabilityDate,
      hour: request.hour,
      location: request.locationStart != null
          ? request.locationStart!.toJson()
          : null,
      unitOfMeasurment: request.unitOfMeasurment,
      matriculeVehicle: request.matricule,
    );
  }

  @override
  Future<BaseResponse> reserveTransport(DemandeTransportRequest request) {
    return _appServiceClient.reserveTransport(
      request.idProduct,
      request.capacity,
      request.userId,
      request.unitOfMeasurment,
      request.marchandise,
      request.description,
      request.date,
      request.hour,
    );
  }

  @override
  Future<BaseResponse> saveRegistrationId(SaveRegistrationIdRequest request) {
    return _appServiceClient.saveRegistrationId(
      request.clientId,
      request.registrationId,
    );
  }

  @override
  Future<GetDemandeTransport> getListDemandeTransportRecu(int userId) {
    return _appServiceClient.getListDemandeTransportRecu(userId);
  }

  @override
  Future<GetDemandeTransport> getListDemandeTransportEnvoye(int userId) {
    return _appServiceClient.getListDemandeTransportSend(userId);
  }

  @override
  Future<BaseResponse> cancelDemandeTransport(int reservationId) {
    return _appServiceClient.cancelDemande(reservationId);
  }

  @override
  Future<BaseResponse> validDemandeTransport(
      int fournisseurId, int reservationId) {
    return _appServiceClient.validDemandeTransport(
        fournisseurId, reservationId);
  }

  @override
  Future<BaseResponse> validChargement(
    int reservationId,
    personal,
    code,
  ) {
    return _appServiceClient.validChargment(
      reservationId,
      personal,
      code,
    );
  }

  @override
  Future<GetCategoryVehicule> getListCategorieVehicule() {
    return _appServiceClient.getCategorieVehicule();
  }

  @override
  Future<BaseResponse> acceptPaiementCash(
      int offerId, int productId, bool response, String? message) {
    return _appServiceClient.acceptPaiementCash(
        offerId, productId, response, message);
  }

  @override
  Future<BaseResponse> confirmPaiementCash(
      int demandeId, String confirmationCode) {
    return _appServiceClient.confirmPaiementCash(demandeId, confirmationCode);
  }

  @override
  Future<GetListDemandePaiementCashItem> getListDemandePaimentCash(
      String role) {
    return _appServiceClient.getListDemandePaimentCash(role);
  }

  @override
  Future<BaseResponse> validContractPdf(
      int userId, int offerId, bool agreement) {
    return _appServiceClient.validContractPdf(userId, offerId, agreement);
  }

  @override
  Future<GetDetailProductResponse> updateProductIntrant(
      UpdateProductOnMarketrequest request) {
    return _appServiceClient.updateProductIntrant(
      request.idProduct,
      request.idProduct,
      request.fournisseurId,
      request.price,
      request.quantity,
      request.typeVente,
      request.availability,
      request.description,
      request.unitOfMeasurment,
      request.offerType,
      request.availabilityDate,
      // request.images,
      request.locationId,
      request.regionId,
      request.departementId,
      request.sousPrefectureId,
    );
  }

  @override
  Future<GetIntrantCategoryResponse> getIntrantCategorie() {
    return _appServiceClient.getIntrantCategoris();
  }

  @override
  Future<GetUniteResponse> getIntrantUnite() {
    return _appServiceClient.getIntrantUnite();
  }

  @override
  Future<BaseResponse> deleteFirebaseToken(
      String userName, String firebaseToken) {
    return _appServiceClient.deleteFirebaseToken(userName, firebaseToken);
  }

  @override
  Future<OffreInsuranceModel> getInsurance(
    RequestOffreInsuranceModel request,
  ) {
    return _appServiceClient.getInsurance(
      request.page,
      request.perPage,
      request.sortBy,
      request.businessLineSlug,
      request.insurerId,
      request.typeInsuranceId,
      request.insuranceId,
      request.insurerName,
      request.typeInsuranceName,
      request.insuranceName,
      request.prixMin,
      request.prixMax,
    );
  }

  @override
  Future<InsuranceByInsurerModel> getInsuranceByInsurer(int insurerId) {
    return _appServiceClient.getInsuranceByInsurer(insurerId);
  }

  @override
  Future<OffreCreditModel> getCredit(
    RequestOffreCreditModel request,
  ) {
    return _appServiceClient.getCredit(
      request.page,
      request.perPage,
      request.sortBy,
      request.businessLineSlug,
      request.creditorId,
      request.typeCreditId,
      request.creditId,
      request.creditorName,
      request.typeCreditName,
      request.creditName,
      request.prixMin,
      request.prixMax,
    );
  }

  @override
  Future<TypeOffreInsuranceModel> getTypeInsurance() {
    return _appServiceClient.getTypeInsurance();
  }

  @override
  Future<TypeOffreCreditModel> getTypeCredit() {
    return _appServiceClient.getTypeCredit();
  }

  @override
  Future<OffreCreditModel> getCreditByCreditor(int creditorId) {
    return _appServiceClient.getCreditByCreditor(creditorId);
  }

  @override
  Future<CreditorModel> getCreditor() {
    return _appServiceClient.getCreditor();
  }

  @override
  Future<InsurerModel> getInsurer() {
    return _appServiceClient.getInsurer();
  }

  @override
  Future<BaseResponse> deleteImage(int imageId) {
    return _appServiceClient.deleteImage(imageId);
  }

  @override
  Future<GetDetailProductResponse> addOtherImage(
      int productId, List<File> images) {
    return _appServiceClient.addOtherImage(productId, images);
  }

  @override
  Future<GetEntrepriseResponseModel> getEntrepriseDetails(int entrepriseId) {
    return _appServiceClient.getEntreprise(entrepriseId);
  }

  @override
  Future<GetEntrepriseResponseModel> getEntrepriseCredit(int entrepriseId) {
    return _appServiceClient.getEntrepriseCredit(entrepriseId);
  }

  @override
  Future<String?> sendNotif(
    String resourceIds,
    String subject,
    List<String> receivers,
    String title,
    String message,
    String messageDate,
  ) {
    final DateFormat formatter = DateFormat('yyyy-MM-dd');

    return _appServiceClient.sendNotif(
      resourceIds,
      subject,
      receivers,
      title,
      message,
      formatter.format(DateTime.parse(
        messageDate,
      )),
    );
  }

  @override
  Future<BaseResponse> updatePassword(
    int id,
    String login,
    String password,
    String oldPassword,
  ) {
    return _appServiceClient.updatePassword(
      id,
      login,
      password,
      oldPassword,
    );
  }

  @override
  Future<MesProduitsCommanderResponse> getMyProductOrdered(
      GetProductCommanderRequest request) {
    return _appServiceClient.getMyProductsCommanded(
      request.fournisseurId,
      request.page,
      request.perPage,
      request.identifiant,
      request.sortBy,
    );
  }

  @override
  Future<BaseResponse> reductionPrice(
      int commandId, int productId, double priceReduction) {
    return _appServiceClient.reductionPrice(
        commandId, productId, priceReduction);
  }

  @override
  Future<BaseResponse> updateUserInfo(int id, UserInfoModel userInfoModel) {
    return _appServiceClient.updateUserInfo(
        id,
        userInfoModel.username,
        userInfoModel.firstname,
        userInfoModel.lastname,
        userInfoModel.gender,
        userInfoModel.birthdate,
        userInfoModel.phone,
        userInfoModel.adress,
        userInfoModel.email,
        userInfoModel.photo,
        userInfoModel.coordonates,
        userInfoModel.localisation,
        userInfoModel.principalSpeculation,
        userInfoModel.notation);
  }

  @override
  Future<BaseResponse> sendOtpResetPassword(
    String phoneNumber,
  ) {
    return _appServiceClient.sendOtpResetPassword(
      phoneNumber,
    );
  }

  @override
  Future<BaseResponse> resetPassword(
    String phoneNumber,
    String? password,
    String? token,
  ) {
    return _appServiceClient.resetPassword(
      phoneNumber,
      password,
      token,
    );
  }

  @override
  Future<AccountInfoResponse> getAccountInfoByToken() {
    return _appServiceClient.getAccountInfoByToken();
  }

  @override
  Future<GetProductOderedInCreditResponse> getProductOderedInCredit(
      String role) {
    return _appServiceClient.getProductOderedInCredit(role);
  }

  @override
  Future<GetListUnitTimeResponse> getUnitTime() {
    return _appServiceClient.getUnitTime();
  }

  @override
  Future<CreatePaiementResponse> createPaiementCredit(
      String reference, int orderId, String motif, String phone) {
    return _appServiceClient.createPaiementCredit(
        orderId, reference, phone, motif);
  }

  @override
  Future<BaseResponse> demandePreAchat(int fournisseurId, int offerId) {
    return _appServiceClient.demandePreAchat(fournisseurId, offerId);
  }

  @override
  Future<GetDemandeAchatOrPreAchat> getDemandeAchatOrPreAchat(
      int id, String customer, String type) {
    return _appServiceClient.getDemandeAchatOrPreAchat(id, customer, type);
  }

  @override
  Future<BaseResponse> validedOrInvalidedDemandePreAchat(int id, bool state) {
    return _appServiceClient.validedOrInvalidedDemandePreAchat(id, state);
  }

  @override
  Future<GetUserStatResponse> getUserStat(int id) {
    return _appServiceClient.getUserStat(
      id,
    );
  }
}
