import 'package:eagri/app/extensions.dart';
import 'package:eagri/domain/model/location_model.dart';

import '../responses/lieu_responses.dart';

extension RegionResponseMapper on RegionResponse? {
  RegionModel toDomain() {
    return RegionModel(
      regionId: this?.regionId?.orZero() ?? zero,
      name: this?.name?.orEmpty() ?? empty,
    );
  }
}

extension DepartementResponseMapper on DepartementResponse? {
  DepartementModel toDomain() {
    return DepartementModel(
      departementId: this?.departementId?.orZero() ?? zero,
      name: this?.name?.orEmpty() ?? empty,
      region: this?.region.toDomain(),
    );
  }
}

extension SousPrefectureResponseMapper on SousPrefectureResponse? {
  SousPrefectureModel toDomain() {
    return SousPrefectureModel(
      sousPrefectureId: this?.sousPrefectureId?.orZero() ?? zero,
      name: this?.name?.orEmpty() ?? empty,
      departement: this?.departement.toDomain(),
    );
  }
}

extension LocaliteResponseMapper on LocaliteResponse? {
  LocaliteModel toDomain() {
    return LocaliteModel(
      localiteId: this?.localiteId?.orZero() ?? zero,
      name: this?.name?.orEmpty() ?? empty,
    );
  }
}

extension LocationForWeatherMapper on LocationForWeather? {
  LocationForWeatherModel toDomain() {
    return LocationForWeatherModel(
      cityName: this?.cityName.orEmpty(),
      lat: this?.lat.orEmpty(),
      lng: this?.lng.orEmpty(),
    );
  }
}

extension ListLocationForWeatherMapper on ListLocationForWeatherResponse? {
  ListLocationForWeatherModel toDomain() {
    List<LocationForWeatherModel> list = [];
    if (this?.data != null) {
      for (LocationForWeather element in this?.data!.listLocation ?? []) {
        list.add(element.toDomain());
      }
    }
    return ListLocationForWeatherModel(listLocation: list);
  }
}
