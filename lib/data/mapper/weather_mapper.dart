import '../../domain/model/weather_model.dart';
import '../responses/weather_response.dart';

import '../../app/extensions.dart';

extension ParcelInfoResponseMapper on AddParcelInfoResponse {
  ParcelInfoModel toDomain() {
    return ParcelInfoModel(
      statusCode: status,
      parcelId: parcelInfo!.parcelId.orEmpty(),
      t10: parcelInfo!.t10.orZeroDouble(),
      t0: parcelInfo!.t0.orZeroDouble(),
      dt: parcelInfo!.dt.orZero(),
      moisture: parcelInfo!.moisture.orZeroDouble(),
      id: parcelInfo!.id.orZero(),
      name: parcelInfo!.name.orEmpty(),
      centerLat: parcelInfo!.centerLat,
      centerLon: parcelInfo!.centerLon,
      coordinates: parcelInfo!.coordinates,
      createdAt: parcelInfo!.createdAt,
    );
  }
}

extension HistoryWeatherInfoMapper on HistoryWeatherInfo {
  HistoryWeatherModel toDomain() {
    return HistoryWeatherModel(
      date: date.orEmpty(),
      dayName: dayName.orEmpty(),
      tempMin: tempMin.orZeroDouble(),
      tempMax: tempMax.orZeroDouble(),
      probprecipitation: precipitationProbability.orZeroDouble(),
      clouds: clouds.orZero(),
      cityName: cityName.orEmpty(),
      description: description.orEmpty(),
      cloudCover: clouds.orZero(),
      tempDay: tempDay.orZeroDouble(),
      dewPoint: dewPoint.orZeroDouble(),
      humidity: humidity.orZero(),
      pressure: pressure.orZero(),
      iconLink: iconLink.orEmpty(),
      precipitation: precipitation.orZeroDouble(),
      precipitationProbability: precipitationProbability.orZeroDouble(),
      windSpeed: windSpeed.orZeroDouble(),
      windDeg: windDeg.orZeroDouble(),
      windGust: windGust.orZeroDouble(),
      pop: pop.orZeroDouble(),
      rain: rain.orZeroDouble(),
      dt: dt.orZero(),
    );
  }
}

extension ListHistoryWeatherInfoResponseMapper
    on ListHistoryWeatherInfoResponse? {
  ListHistoryWeatherModel toDomain() {
    List<HistoryWeatherModel> listData = [];
    if (this?.listHistory != null) {
      for (HistoryWeatherInfo element
          in this?.listHistory ?? <HistoryWeatherInfo>[]) {
        HistoryWeatherModel history = element.toDomain();
        listData.add(history);
      }
    }
    return ListHistoryWeatherModel(listHistory: listData);
  }
}

extension DayWeatherResponseDataMapper on DayWeatherDataResponse? {
  DayWeatherModel toDomain() {
    List<HoulyWeatherModel> listHoulyWeather = [];

    if (this?.houlyWeather != null) {
      for (var element in this?.houlyWeather ?? <HoulyWeatherDataResponse>[]) {
        listHoulyWeather.add(element.toDomain());
      }
    }
    return DayWeatherModel(
      date: this?.date.orEmpty(),
      dayName: this?.dayName.orEmpty(),
      tempMin: this?.tempMin.orZeroDouble(),
      tempMax: this?.tempMax.orZeroDouble(),
      probprecipitation: this?.precipitationProbability.orZeroDouble(),
      clouds: this?.clouds.orZero(),
      houlyWeather: listHoulyWeather,
      description: this?.description.orEmpty(),
      cloudCover: this?.clouds.orZero(),
      temp: this?.temp.orZeroDouble(),
      dewPoint: this?.dewPoint.orZero(),
      humidity: this?.humidity.orZero(),
      pressure: this?.pressure.orZero(),
      iconLink: this?.iconLink.orEmpty(),
      precipitation: this?.precipitation.orZeroDouble(),
      precipitationProbability: this?.precipitationProbability.orZeroDouble(),
      windSpeed: this?.windSpeed.orZero(),
      windDeg: this?.windDeg.orZero(),
      windGust: this?.windGust.orZero(),
    );
  }
}

extension HoulyWeatherResponseDataMapper on HoulyWeatherDataResponse? {
  HoulyWeatherModel toDomain() {
    return HoulyWeatherModel(
      hour: this?.hour.orEmpty(),
      temp: this?.temp.orZeroDouble(),
      dewPoint: this?.dewPoint.orZero(),
      humidity: this?.humidity.orZero(),
      pressure: this?.pressure.orZero(),
      description: this?.description.orEmpty(),
      iconLink: this?.iconLink.orEmpty(),
      precipitation: this?.precipitation.orZeroDouble(),
      precipitationProbability: this?.precipitationProbability.orZeroDouble(),
      cloudCover: this?.clouds.orZero(),
      windSpeed: this?.windSpeed.orZero(),
      windDeg: this?.windDeg.orZero(),
      windGust: this?.windGust.orZero(),
    );
  }
}

extension WeatherResponseMapper on WeatherResponse? {
  CityWeatherModel toDomain() {
    List<DayWeatherModel> listDayWeather = [];

    if (this?.weather != null) {
      for (var element
          in this?.weather!.listDayWeather ?? <DayWeatherDataResponse>[]) {
        listDayWeather.add(element.toDomain());
      }
    }
    return CityWeatherModel(
      cityName: this?.weather!.cityName.orEmpty(),
      listDayWeather: listDayWeather,
    );
  }
}

extension ParcelInfoMapper on ParcelInfoResponse {
  ParcelInfoModel toDomain() {
    return ParcelInfoModel(
      id: id.orZero(),
      t0: t0.orZeroDouble(),
      t10: t10.orZeroDouble(),
      moisture: moisture.orZeroDouble(),
      parcelId: parcelId.orEmpty(),
      name: name.orEmpty(),
      centerLat: centerLat.orZeroDouble(),
      centerLon: centerLon.orZeroDouble(),
      coordinates: coordinates.orEmpty(),
      createdAt: createdAt.orEmpty(),
      dt: dt.orZero(),
    );
  }
}

extension ListParcelResponseMapper on ListParcelResponse? {
  ListParcelModel toDomain() {
    List<ParcelInfoModel> listParcel = [];

    if (this?.listParcel != null) {
      for (ParcelInfoResponse element
          in this?.listParcel ?? <ParcelInfoResponse>[]) {
        listParcel.add(element.toDomain());
      }
    }
    return ListParcelModel(listParcel: listParcel);
  }
}

extension CreateFullParcelResponseMapper on CreateFullParcelResponse? {
  CreateFullParcelResponseModel toDomain() {
    List<ParcelInfoModel> listParcelExtist = [];
    List<ParcelInfoModel> listParcelCreated = [];
    this
        ?.listParcel!
        .listAlreadyExists!
        .map((e) => listParcelExtist.add(e.toDomain()));
    this
        ?.listParcel!
        .listCreated!
        .map((e) => listParcelCreated.add(e.toDomain()));
    return CreateFullParcelResponseModel(
      listParcelCreated: ListParcelModel(listParcel: listParcelCreated),
      listParcelExist: ListParcelModel(listParcel: listParcelExtist),
    );
  }
}
