import 'package:eagri/app/extensions.dart';
import 'package:eagri/data/responses/responses.dart';
import 'package:eagri/domain/model/alert_model.dart';

extension AlertMapper on AlertResponse? {
  AlertModel toDomain() {
    return AlertModel(
      id: this!.alertId,
      speculationId: this!.speculationId ?? zero,
      userId: this!.userId,
    );
  }
}

extension AddAlertMapper on AddAlertResponse? {
  AlertModel toDomain() {
    return this!.alert.toDomain();
  }
}
