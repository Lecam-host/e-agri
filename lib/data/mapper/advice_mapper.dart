import '../../domain/model/advice_model.dart';
import '../responses/advice_response.dart';
import 'mapper.dart';

extension RecommandationResponseMapper on RecommandationResponse? {
  RecommandationModel toDomain() {
    return RecommandationModel(
      speculationId: this?.speculationId,
      treatmentRange: this?.treatmentRange,
      message: this?.message,
      speculation: this?.speculation!=null? this?.speculation!.toDomain():null,
    );
  }
}

extension GestListRecommandationResponseMapper
    on GetListRecommandationResponse? {
  List<RecommandationModel> toDomain() {
    List<RecommandationModel> listRecommandation = [];
    if (this?.recommandations != null) {
      listRecommandation
          .addAll(this!.recommandations!.map((e) => e.toDomain()));
    }
    return listRecommandation;
  }
}
