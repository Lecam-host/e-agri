import 'package:eagri/data/fake_data.dart';
import 'package:eagri/data/mapper/location_mapper.dart';
import 'package:eagri/domain/model/location_model.dart';
import 'package:jwt_decoder/jwt_decoder.dart';

import '../../app/extensions.dart';
import '../../domain/model/model.dart';
import '../../domain/model/user_model.dart';
import '../responses/account_response.dart';
import '../responses/advice_response.dart';
import '../responses/lieu_responses.dart';
import '../responses/responses.dart';

extension BaseResponseMapper on BaseResponse? {
  BaseApiResponseModel toDomain() {
    return BaseApiResponseModel(
      statusCode: this?.status.orZero() ?? zero,
      statusMessage: this?.message.orEmpty() ?? empty,
    );
  }
}

extension AuthResponseMapper on AuthResponse? {
  AuthModel toDomain() {
    Map<String, dynamic> decodedToken = JwtDecoder.decode(this!.data!);
    var token = <String, String>{"token": this!.data!};
    decodedToken.addEntries(token.entries);

    UserInfoResponse userInfo = UserInfoResponse.fromJson(decodedToken);
    return AuthModel(
      userInfo.toDomain(),
    );
  }
}

extension GetUserInfoResponseMapper on GetUserInfoResponse? {
  UserModel toDomain() {
    return this?.data!.toDomain() ?? UserModel();
  }
}

extension UserResponseMapper on UserInfoResponse? {
  UserModel toDomain() {
    return UserModel(
      id: this?.userId?.orZero() ?? zero,
      token: this?.token?.orEmpty() ?? empty,
      firstName: this?.firstName?.orEmpty() ?? empty,
      lastName: this?.lastName?.orEmpty() ?? empty,
      number: this?.number?.orEmpty() ?? empty,
      gender: this?.gender?.orEmpty() ?? empty,
      birthdate: this?.birthdate?.orEmpty() ?? empty,
      userName: this?.username?.orEmpty() ?? empty,
      rate: this?.notation?.orZeroDouble() ,
    );
  }
}

extension AllAdviceResponseMapper on AllAdviceDataResponse? {
  ListAdviceModel toDomain() {
    List<AdviceModel> listData = [];
    if (this?.data!.listAdvice != null) {
      for (AdviceResponse advice
          in this?.data!.listAdvice ?? <AdviceResponse>[]) {
        List<FileModel> listFile = [];
        List<ObjectModel> lsitCategory = [];
        if (advice.files != null) {
          listFile.addAll(advice.files!.map((e) => (e.toDomain())));
        }
        if (advice.categorys != null) {
          lsitCategory.addAll(advice.categorys!.map((e) => (e.toDomain())));
        }

        AdviceModel adviceModel = AdviceModel(
          advice.id.orZero(),
          advice.title.orEmpty(),
          advice.description.orEmpty(),
          advice.createDate.orEmpty(),
          listFile,
          advice.adviceType.orEmpty(),
          lsitCategory,
          advice.numberView.orZero(),
          advice.illustrationImage.orEmpty(),
          advice.notation.orZeroDouble(),
          advice.publicateurId,
        );

        listData.add(adviceModel);
      }
    }

    return ListAdviceModel(
      listData,
      this?.data!.totalObject.orZero() ?? 0,
      this?.data!.currentPage.orZero() ?? 0,
      this?.data!.totalOfPages.orZero() ?? 0,
      this?.data!.perPage.orZero() ?? 0,
      this?.data!.nextPage.orZero() ?? 0,
      this?.data!.previousPage.orZero() ?? 0,
    );
  }
}

extension AllAdviceCategoryResponseMapper on AllAdviceCategoryResponse? {
  ListAdviceCategoryModel toDomain() {
    List<AdviceCategoryModel> listData = [];
    if (this?.data != null) {
      for (var element in this?.data ?? <AdviceCategoryResponse>[]) {
        AdviceCategoryModel adviceCategoryModel = AdviceCategoryModel(
          element.id.orZero(),
          element.code.orZero(),
          element.description.orEmpty(),
          element.label.orEmpty(),
        );
        listData.add(adviceCategoryModel);
      }
    }
    return ListAdviceCategoryModel(
      listData,
    );
  }
}

extension DetailsAdviceResponseMapper on AdviceResponse? {
  AdviceModel toDomain() {
    List<ObjectModel> lsitCategoryAdvice = [];

    List<FileModel> listFiles = [];
    // listFile.addAll(advice.files!.map((e) => (e.toDomain())));
    if (this != null) {
      if (this?.files != null) {
        listFiles.addAll(this!.files!.map((e) => e.toDomain()));
      }
      if (this?.categorys != null) {
        lsitCategoryAdvice.addAll(this!.categorys!.map((e) => e.toDomain()));
      }
    }

    return AdviceModel(
      this?.id.orZero() ?? zero,
      this?.title.orEmpty() ?? empty,
      this?.description.orEmpty() ?? empty,
      this?.createDate.orEmpty() ?? empty,
      listFiles,
      this?.adviceType.orEmpty() ?? empty,
      lsitCategoryAdvice,
      this?.numberView.orZero() ?? zero,
      this?.illustrationImage.orEmpty() ?? empty,
      this?.notation.orZeroDouble() ?? zero1,
      this?.publicateurId.orZero() ?? zero,
    );
  }
}

extension AskAdviceResponseMapper on AskAdviceDataResponse? {
  AskAdviceModel toDomain() {
    List<AdviceCategoryModel> categoriesAdvices = [];
    for (AdviceCategoryResponse element
        in this?.listCategorieAdvice ?? <AdviceCategoryResponse>[]) {
      categoriesAdvices.add(AdviceCategoryModel(
          element.id.orZero(),
          element.code.orZero(),
          element.description.orEmpty(),
          element.label.orEmpty()));
    }
    return AskAdviceModel(
      id: this?.id.orZero() ?? zero,
      description: this?.description.orEmpty() ?? empty,
      advicesType: this?.advicesType.orEmpty(),
      typeFiles: this?.typeFiles,
      organization: this?.organization,
      isResolve: this?.isResolve.orZero() ?? zero,
      creationDate: this?.createdAt.orEmpty() ?? empty,
      categoriesAdvice: categoriesAdvices,
    );
  }
}
// extension WeatherResponseMapper on WeatherResponse? {
//   WeatherResponseModel toDomain() {
//     return WeatherResponseModel(
//       this?.toDomain(),
//     );
//   }
// }

extension SpecultionResponseMapper on SpeculationResponse {
  SpeculationModel toDomain() {
    return SpeculationModel(
        speculationId: speculationId.orZero(),
        name: name.orEmpty(),
        imageUrl: imageUrl.orEmpty(),
        listUnit: listUnit,
        categoryInfo: CategorieSpeculationModel(
          categorieId: categorieSpeculation != null
              ? categorieSpeculation!.categoryId
              : 0,
          name: categorieSpeculation != null ? categorieSpeculation!.name : "",
        ));
  }
}

extension PriceResponseMapper on PriceResponse {
  PriceModel toDomain() {
    return PriceModel(
      prixGros: prixGros.orZero(),
      prixDetails: prixDetail.orZero(),
      speculationId: speculationId.orZero(),
      regionId: regionId.orZero(),
      departementId: departementId.orZero(),
      sousPrefectureId: sousPrefectureId.orZero(),
      localiteId: localiteId.orZero(),
      fromPeriod: fromPeriod.orEmpty(),
      toPeriod: toPeriod.orEmpty(),
      unit: unit.orEmpty(),
      source: source.orEmpty(),
    );
  }
}

extension LocationPriceResponseMapper on LocationPriceResponse {
  LocationModel toDomain() {
    return LocationModel(
      region: RegionModel(
        regionId: region?.regionId.orZero(),
        name: region?.name.orEmpty(),
      ),
      departement: DepartementModel(
        departementId: departement?.departementId.orZero(),
        name: departement?.name.orEmpty(),
        region: RegionModel(
          regionId: region?.regionId.orZero(),
          name: region?.name.orEmpty(),
        ),
      ),
      sousPrefecture: SousPrefectureModel(
        name: sousPrefecture?.name.orEmpty(),
        departement: DepartementModel(
          departementId: departement?.departementId.orZero(),
          name: departement?.name.orEmpty(),
          region: RegionModel(
            regionId: region?.regionId.orZero(),
            name: region?.name.orEmpty(),
          ),
        ),
      ),
      localiteId: localiteId.orZero(),
      localiteName: name.orEmpty(),
    );
  }
}

extension SpeculationPriceResponseMapper on SpeculationPriceResponse {
  SpeculationPriceModel toDomain() {
    return SpeculationPriceModel(
      priceInfo: priceInfo != null
          ? priceInfo!.toDomain()
          : PriceModel(
              prixGros: 2000,
              prixDetails: 2500,
              speculationId: 1,
              regionId: 1,
              departementId: 1,
              sousPrefectureId: 1,
              localiteId: 1),
      speculationInfo: speculationInfo != null
          ? speculationInfo!.toDomain()
          : SpeculationModel(
              speculationId: 1,
              name: "Poulet",
              imageUrl:
                  "https://images.pexels.com/photos/239587/pexels-photo-239587.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"),
      locationInfo: location != null
          ? location!.toDomain()
          : LocationModel(
              region: FakeData.region,
              departement: FakeData.departement,
              sousPrefecture: FakeData.sousPrefecture,
              localiteId: 1,
              localiteName: "Aby",
            ),
    );
  }
}

extension ListSpecultionPriceResponseMapper on GetSpeculationPriceResponse {
  ListSpeculationPriceModel toDomain() {
    List<SpeculationPriceModel> listSpeculationPriceOutput = [];
    if (data != null) {
      for (var element in data ?? <SpeculationPriceResponse>[]) {
        SpeculationPriceModel speculationPriceModel = SpeculationPriceModel(
          priceInfo: element.priceInfo != null
              ? element.priceInfo!.toDomain()
              : PriceModel(),
          speculationInfo: element.speculationInfo != null
              ? element.speculationInfo!.toDomain()
              : SpeculationModel(),
          locationInfo: element.location != null
              ? element.location!.toDomain()
              : LocationModel(),
        );
        listSpeculationPriceOutput.add(speculationPriceModel);
      }
    }

    return ListSpeculationPriceModel(
      listSpeculationPrice: listSpeculationPriceOutput,
    );
  }
}

extension ListSpecultionPriceResponseMapperNonOfficiel
    on GetSpeculationPriceNonOfficielDataResponse {
  ListSpeculationPriceModel toDomain() {
    List<SpeculationPriceModel> listSpeculationPriceOutput = [];
    if (listPrice != null) {
      for (var element in listPrice ?? <SpeculationPriceResponse>[]) {
        SpeculationPriceModel speculationPriceModel = SpeculationPriceModel(
          priceInfo: element.priceInfo != null
              ? element.priceInfo!.toDomain()
              : PriceModel(),
          speculationInfo: element.speculationInfo != null
              ? element.speculationInfo!.toDomain()
              : SpeculationModel(),
          locationInfo: element.location != null
              ? element.location!.toDomain()
              : LocationModel(),
        );
        listSpeculationPriceOutput.add(speculationPriceModel);
      }
    }

    return ListSpeculationPriceModel(
      listSpeculationPrice: listSpeculationPriceOutput,
    );
  }
}

extension ListRegionResponseMapper on ListRegionResponse? {
  ListRegionModel toDomain() {
    List<RegionModel> listData = [];
    if (this?.data != null) {
      for (var element in this?.data ?? <RegionResponse>[]) {
        RegionModel region = RegionModel(
          regionId: element.regionId.orZero(),
          name: element.name.orEmpty(),
        );
        listData.add(region);
      }
    }
    return ListRegionModel(listData);
  }
}

extension ListSpeculationResponseMapper on ListSpeculationResponse? {
  ListSpeculationModel toDomain() {
    List<SpeculationModel> listData = [];
    if (this?.data != null) {
      for (var element in this?.data ?? <SpeculationResponse>[]) {
        SpeculationModel speculation = element.toDomain();

        listData.add(speculation);
      }
    }
    return ListSpeculationModel(data: listData);
  }
}

extension ListDepartementResponseMapper on ListDepartementResponse? {
  ListDepartementModel toDomain() {
    List<DepartementModel> listData = [];
    if (this?.listDepartement != null) {
      for (var element in this?.listDepartement ?? <DepartementResponse>[]) {
        DepartementModel departement = DepartementModel(
          departementId: element.departementId,
          name: element.name.orEmpty(),
          region: element.region!.toDomain(),
        );
        listData.add(departement);
      }
    }
    return ListDepartementModel(listDepartement: listData);
  }
}

extension ListSousPrefecteureResponseMapper on ListSousPrefectureResponse? {
  ListSousPrefectureModel toDomain() {
    List<SousPrefectureModel> listData = [];
    if (this?.listSousPrefecture != null) {
      for (SousPrefectureResponse element
          in this?.listSousPrefecture ?? <SousPrefectureResponse>[]) {
        SousPrefectureModel sousPrefecture = SousPrefectureModel(
          sousPrefectureId: element.sousPrefectureId,
          name: element.name.orEmpty(),
          departement: element.departement!.toDomain(),
        );
        listData.add(sousPrefecture);
      }
    }
    return ListSousPrefectureModel(listSousPrefecture: listData);
  }
}

extension ListlocaliteResponseMapper on ListLocaliteResponse? {
  ListLocaliteModel toDomain() {
    List<LocaliteModel> listData = [];
    if (this?.listLocalite != null) {
      for (var element in this?.listLocalite ?? <LocaliteResponse>[]) {
        LocaliteModel localite = LocaliteModel(
          localiteId: element.localiteId,
          name: element.name.orEmpty(),
          sousPrefecture: element.sousPrefecture!.toDomain(),
        );
        listData.add(localite);
      }
    }
    return ListLocaliteModel(listLocalite: listData);
  }
}

class GetListConfigModel {
  List<ConfigModel>? allConfig;

  GetListConfigModel({
    this.allConfig,
  });
}

extension GetListAlertDomaineResponseModel on GetDomaineAlertDataResponse {
  List<ConfigModel> toDomain() {
    List<ConfigModel> list = [];
    data.listAlertDomain.map((e) => list.add(e.toDomain()));
    for (var domain in data.listAlertDomain) {
      list.add(domain.toDomain());
    }
    return list;
  }
}

extension GetListAlertDomaineMapper on GetListDomaineResponse {
  List<ConfigModel> toDomain() {
    List<ConfigModel> list = [];
    listAlertDomain.map((e) => list.add(e.toDomain()));
    for (var domain in listAlertDomain) {
      list.add(domain.toDomain());
    }
    return list;
  }
}

extension ConfigResponseMapper on ConfigResponse {
  ConfigModel toDomain() {
    return ConfigModel(
      id: id.orZero(),
      code: code.orZero(),
      isDisable: isDisable.orZero(),
      typeConfigId: typeConfigId.orZero(),
      name: name.orEmpty(),
    );
  }
}

extension ListConfigResponseMapper on ListConfigResponse {
  ListConfigModel toDomain() {
    List<ConfigModel> listConfigReponse = [];
    if (listConfig != null) {
      for (ConfigResponse element in listConfig ?? <ConfigResponse>[]) {
        ConfigModel config = element.toDomain();
        listConfigReponse.add(config);
      }
    }
    return ListConfigModel(
      id: id.orZero(),
      code: code.orZero(),
      listConfig: listConfigReponse,
      name: name.orEmpty(),
    );
  }
}

extension AllConfigResponseMapper on AllConfigResponse? {
  AllConfigModel toDomain() {
    List<ListConfigModel> listData = [];
    if (this?.listConfig != null) {
      for (ListConfigResponse element
          in this?.listConfig ?? <ListConfigResponse>[]) {
        ListConfigModel list = element.toDomain();
        listData.add(list);
      }
    }
    return AllConfigModel(allConfig: listData);
  }
}

extension UserListAskResponseMapper on ListAskAdviceResponse {
  ListAskAdviceModel toDomain() {
    List<AskAdviceModel> listAsk = [];
    if (listAskAdvice != null) {
      for (AskAdviceDataResponse element
          in listAskAdvice ?? <AskAdviceDataResponse>[]) {
        AskAdviceModel ask = element.toDomain();
        listAsk.add(ask);
      }
    }
    return ListAskAdviceModel(listAskAdvice: listAsk);
  }
}

extension FileResponseMapper on FileResponse {
  FileModel toDomain() {
    return FileModel(
      id.orZero(),
      title.orEmpty(),
      description.orEmpty(),
      typeFile.orZero(),
      fileLink.orEmpty(),
    );
  }
}

extension ObjectResponseMapper on ObjectResponse {
  ObjectModel toDomain() {
    return ObjectModel(
      id.orZero(),
      label.orEmpty(),
    );
  }
}

extension MixAndMaxSpeculationPrice on GetMinAndMaxSpeculationPriceResponse {
  MinAndMaxSpeculationPriceModel? toDomain() {
    if (data != null) {
      return MinAndMaxSpeculationPriceModel(
        maxPrice: data!.maxPrice.orZero(),
        minPrice: data!.minPrice.orZero(),
        maxPriceRegion: data!.maxPriceRegion.toDomain(),
        minPriceRegion: data!.minPriceRegion.toDomain(),
      );
    } else {
      return null;
    }
  }
}
