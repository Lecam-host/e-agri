// ignore_for_file: avoid_function_literals_in_foreach_calls

import 'package:eagri/app/constant.dart';
import 'package:eagri/app/di.dart';
import 'package:eagri/data/repository/local/speculation_repo.dart';
import 'package:eagri/data/request/request.dart';
import 'package:eagri/data/responses/shop_response.dart';
import 'package:eagri/domain/model/Product.dart';
import 'package:eagri/domain/model/info_by_id_model.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/domain/usecase/use_case.dart';

import '../../domain/model/cart_model.dart';
import '../../domain/model/image_model.dart';
import '../../domain/model/transport_model.dart';
import '../../domain/model/user_model.dart';

extension GetListProductResponseMapper on GetListProductResponse {
  Future<List<ProductModel>> toDomain() async {
    List<ProductModel> data = [];

    data = await fecthData(listProduct!);
    return data;
  }
}

extension ListProductMapper on DataProductUserResponse? {
  Future<List<ProductModel>> toDomain() async {
    List<ProductModel> data = [];
    if (this!.data != null) {
      await Future.forEach(this!.data!.listProduct!,
          (Map<String, dynamic> element) async {
        if (element.isNotEmpty) {
          ProductModel product = await fecthItemSmallDetails(element);
          data.add(product);
        }
      });
    }

    return data;
  }
}

extension DetailProductMapper on GetDetailProductResponse? {
  Future<ProductModel> toDomain() async {
    return await fecthItemMoreDetails(this!.data);
  }
}

extension GetAllProductMapper on GetAllProductResponse? {
  Future<List<ProductModel>> toDomain() async {
    List<ProductModel> data = [];
    if (this!.data != null) data = await fecthData(this!.data!);
    return data;
  }
}

Future<List<ProductModel>> fecthData(
    List<Map<String, dynamic>> listProduct) async {
  List<ProductModel> data = [];

  await Future.forEach(listProduct, (Map<String, dynamic> element) async {
    data.add(await fecthItemSmallDetails(element));
  });

  return data;
}

Future<ProductModel> fecthItemMoreDetails(Map<String, dynamic> product) async {
  if (product["code"] == codeIntrant.toString()) {
    bool isAlreadyRated = true;

    InfoByIdModel info = InfoByIdModel();
    ProductIntrantItem intrant = ProductIntrantItem.fromJson(product);
    List<String> images = [];
    Future.forEach(
        intrant.images, (ImageProduct element) => images.add(element.link));
    // await rateUseCase
    //     .checkAlreadyRated(
    //   CheckRateRequest(
    //       userId: userController.userInfo.value.id!,
    //       ressourceId: intrant.id,
    //       codeService: ServiceCode.marketPlace),
    // )
    //     .then((checkAlreadyRatedResponse) async {
    //   checkAlreadyRatedResponse.fold((l) {}, (checkAlreadyRatedResult) async {
    //     isAlreadyRated = checkAlreadyRatedResult.data;
    //   });
    // });
    // await useCase
    //     .getInfoById(InfoByIdRequest(
    //       userId: intrant.fournisseurId,
    //       regionId: intrant.location.regionId,
    //       departementId: intrant.location.departementId != 0 &&
    //               intrant.location.departementId != null
    //           ? intrant.location.departementId
    //           : null,
    //       sprefectureId: intrant.location.sprefectureId != 0 &&
    //               intrant.location.sprefectureId != null
    //           ? intrant.location.sprefectureId
    //           : null,
    //       localiteId: intrant.location.localiteId != 0 &&
    //               intrant.location.localiteId != null
    //           ? intrant.location.localiteId
    //           : null,
    //     ))
    //     .then((value) => value.fold((l) {}, (r) {
    //           info = r;
    //         }));

    return ProductModel(
      paymentId: intrant.paymentId,
      user: info.user != null
          ? UserModel(
              lastName: info.user!.laststname, firstName: info.user!.firstname)
          : null,
      code: intrant.code,
      paymentMethod: intrant.paymentMethod,
      location: ProductLocationModel(
        regionName: info.region != null ? info.region!.name : null,
        regionId: intrant.location.regionId,
        departementId: intrant.location.departementId,
        sPrefectureId: intrant.location.sprefectureId,
        localiteId: intrant.location.localiteId,
        departementName:
            info.departement != null ? info.departement!.name : null,
        sPrefectureName:
            info.sousprefecture != null ? info.sousprefecture!.name : null,
        localiteName: info.localite != null ? info.localite!.name : null,
        locationId: intrant.location.id,
      ),
      disponibilite: '',
      fournisseurId: intrant.fournisseurId,
      id: intrant.id,
      quantity: intrant.product.quantity,
      images: images,
      rating: intrant.notation.toDouble(),
      title: intrant.product.intrant != null
          ? intrant.product.intrant!.libelle
          : "",
      price: intrant.product.priceU.toInt(),
      publicationDate: intrant.publicationDate.toString(),
      status: intrant.status,
      typeOffers: intrant.offerType,
      unite: intrant.product.unitOfMeasurment,
      identifiant: intrant.identifiant,
      typeVente: intrant.typeVente ?? "",
      description: intrant.description,
      alreadyNoted: isAlreadyRated,
      imagesWithId: intrant.images,
      speculation: SpeculationModel(
        imageUrl: "",
        name: intrant.product.intrant != null
            ? intrant.product.intrant!.libelle
            : "",
        speculationId:
            intrant.product.intrant != null ? intrant.product.intrant!.id : 1,
      ),
      itrantInfo: intrant.product.intrant,
    );
  } else if (product["code"] == codePrestation.toString()) {
    ProductResponse prestation = ProductResponse.fromJson(product);
    List<String> images = [];
    InfoByIdModel info = InfoByIdModel();
    for (var element in prestation.images!) {
      images.add(element.link);
    }
    UseCase useCase = instance<UseCase>();

    // imgs.addAll(element.images!);
    await useCase
        .getInfoById(InfoByIdRequest(
          userId: prestation.fournisseurId,
          regionId: prestation.location!.regionId,
          departementId: prestation.location!.departementId != 0 &&
                  prestation.location!.departementId != null
              ? prestation.location!.departementId
              : null,
          sprefectureId: prestation.location!.sousprefectureId != 0 &&
                  prestation.location!.sousprefectureId != null
              ? prestation.location!.sousprefectureId
              : null,
          localiteId: prestation.location!.localiteId != 0 &&
                  prestation.location!.localiteId != null
              ? prestation.location!.localiteId
              : null,
        ))
        .then((value) => value.fold((l) {}, (r) {
              info = r;
            }));

    ProductModel productReturn = ProductModel(
      paymentMethod: prestation.paymentMethod,
      paymentId: prestation.paymentId,
      code: prestation.code,
      categorieProduct: CategorieProductModel(
          id: product["product"]["category"]["id"],
          name: product["product"]["category"]["name"]),
      location: ProductLocationModel(
        regionName: info.region != null ? info.region!.name : null,
        departementName:
            info.departement != null ? info.departement!.name : null,
        sPrefectureName:
            info.sousprefecture != null ? info.sousprefecture!.name : null,
        localiteName: info.localite != null ? info.localite!.name : null,
        regionId: prestation.location!.regionId,
        departementId: prestation.location!.departementId,
        sPrefectureId: prestation.location!.sousprefectureId,
        localiteId: prestation.location!.localiteId,
      ),
      disponibilite: '',
      fournisseurId: prestation.fournisseurId,
      id: prestation.id,
      quantity: prestation.product.quantity,
      images: images,
      rating: prestation.rate.toDouble(),
      title: prestation.product.speculation != null
          ? prestation.product.speculation!.name!
          : "",
      price: prestation.product.price.toInt(),
      description: prestation.description,
      speculation: SpeculationModel(
        imageUrl: "",
        name: "",
        speculationId: prestation.product.speculationId,
      ),
      imagesWithId: prestation.images,
      publicationDate: prestation.publicationDate,
      status: prestation.status,
      typeOffers: prestation.typeOffer,
      typeVente: "",
      unite: product["product"]["unitOfMeasurmentName"],
    );
    if (info.user != null) {
      productReturn.user = UserModel(
          lastName: info.user!.laststname, firstName: info.user!.firstname);
    }
    //productReturn.alreadyNoted = isAlreadyRated;
    if (info.departement != null) {
      productReturn.location!.departementName = info.departement!.name;
    }
    if (info.region != null) {
      productReturn.location!.regionName = info.region!.name;
    }
    if (info.sousprefecture != null) {
      productReturn.location!.sPrefectureName = info.sousprefecture!.name;
    }
    if (info.localite != null) {
      productReturn.location!.localiteName = info.localite!.name;
    }
    return productReturn;
  } else if (product["code"] == codeTransport.toString()) {
    List<String> images = [];
    InfoByIdModel info = InfoByIdModel();
    TransportModel transport = TransportModel.fromJson(product);
    if (transport.images != null) {
      for (var element in transport.images!) {
        images.add(element.link);
      }
    }

    UseCase useCase = instance<UseCase>();

    // imgs.addAll(element.images!);
    await useCase
        .getInfoById(InfoByIdRequest(
          userId: transport.fournisseurId,
          regionId: transport.location.regionId,
          departementId: transport.location.departementId != 0 &&
                  transport.location.departementId != null
              ? transport.location.departementId
              : null,
          sprefectureId: transport.location.sprefectureId != 0 &&
                  transport.location.sprefectureId != null
              ? transport.location.sprefectureId
              : null,
          localiteId: transport.location.localiteId != 0 &&
                  transport.location.localiteId != null
              ? transport.location.localiteId
              : null,
        ))
        .then((value) => value.fold((l) {}, (r) {
              info = r;
            }));

    ProductModel productReturn = ProductModel(
      startTrajetInfo: TrajetInfoModel(
        region: product["product"]["startTrajet"]["region"]?? '',
        departement: product["product"]["startTrajet"]["departement"] != null
            ? product["product"]["startTrajet"]["departement"]["name"]
            : null,
        sprefecture: product["product"]["startTrajet"]["sprefecture"] != null
            ? product["product"]["startTrajet"]["sprefecture"]["name"]
            : null,
        localite: product["product"]["startTrajet"]["localite"] != null
            ? product["product"]["startTrajet"]["localite"]["name"]
            : null,
      ),
      arrivalTrajetInfo: TrajetInfoModel(
        region: product["product"]["arrivalTrajet"]["region"]["name"],
        departement: product["product"]["arrivalTrajet"]["departement"] != null
            ? product["product"]["arrivalTrajet"]["departement"]["name"]
            : null,
        sprefecture: product["product"]["arrivalTrajet"]["sprefecture"] != null
            ? product["product"]["arrivalTrajet"]["sprefecture"]["name"]
            : null,
        localite: product["product"]["arrivalTrajet"]["localite"] != null
            ? product["product"]["arrivalTrajet"]["localite"]["name"]
            : null,
      ),
      paymentMethod: transport.paymentMethod,
      trajet: transport.product.trajet,
      paymentId: transport.paymentId,
      code: transport.code,
      location: ProductLocationModel(
        regionName: info.region != null ? info.region!.name : null,
        departementName:
            info.departement != null ? info.departement!.name : null,
        sPrefectureName:
            info.sousprefecture != null ? info.sousprefecture!.name : null,
        localiteName: info.localite != null ? info.localite!.name : null,
        regionId: transport.location.regionId,
        departementId: transport.location.departementId,
        sPrefectureId: transport.location.sprefectureId,
        localiteId: transport.location.localiteId,
      ),
      disponibilite: '',
      fournisseurId: transport.fournisseurId,
      id: transport.id,
      quantity: transport.product.quantity.toInt(),
      images: images,
      rating: 0,
      title: transport.product.speculation != null
          ? transport.product.speculation!.libelle
          : "",
      price: transport.product.priceU.toInt(),
      description: transport.description,
      speculation: SpeculationModel(
        imageUrl: transport.product.speculation != null
            ? transport.product.speculation!.image
            : "",
        name: transport.product.speculation != null
            ? transport.product.speculation!.libelle
            : "",
        speculationId: transport.product.speculationId,
      ),
      publicationDate: transport.publicationDate.toString(),
      status: transport.status,
      typeOffers: transport.offerType,
      typeVente: "",
      unite: transport.product.unitOfMeasurment,
    );
    if (info.user != null) {
      productReturn.user = UserModel(
          lastName: info.user!.laststname, firstName: info.user!.firstname);
    }
    //productReturn.alreadyNoted = isAlreadyRated;
    if (info.departement != null) {
      productReturn.location!.departementName = info.departement!.name;
    }
    if (info.region != null) {
      productReturn.location!.regionName = info.region!.name;
    }
    if (info.sousprefecture != null) {
      productReturn.location!.sPrefectureName = info.sousprefecture!.name;
    }
    if (info.localite != null) {
      productReturn.location!.localiteName = info.localite!.name;
    }
    return productReturn;
  } else {
    // ProductResponse productAgricul = ProductResponse.fromJson(product);
    // RateUseCase rateUseCase = instance<RateUseCase>();

    // UserController userController = Get.put(UserController());
    // bool isAlreadyRated = true;
    // UseCase useCase = instance<UseCase>();

    InfoByIdModel info = InfoByIdModel();
    ProductModel productReturn = await fecthItemSmallDetails(product);

    // imgs.addAll(element.images!);
    // await useCase
    //     .getInfoById(InfoByIdRequest(
    //       userId: productAgricul.fournisseurId,
    //       regionId: productAgricul.location!.regionId,
    //       departementId: productAgricul.location!.departementId != 0 &&
    //               productAgricul.location!.departementId != null
    //           ? productAgricul.location!.departementId
    //           : null,
    //       sprefectureId: productAgricul.location!.sousprefectureId != 0 &&
    //               productAgricul.location!.sousprefectureId != null
    //           ? productAgricul.location!.sousprefectureId
    //           : null,
    //       localiteId: productAgricul.location!.localiteId != 0 &&
    //               productAgricul.location!.localiteId != null
    //           ? productAgricul.location!.localiteId
    //           : null,
    //     ))
    //     .then((value) => value.fold((l) {}, (r) {
    //           info = r;
    //         }));

    // await rateUseCase
    //     .checkAlreadyRated(
    //   CheckRateRequest(
    //       userId: userController.userInfo.value.id!,
    //       ressourceId: productAgricul.id,
    //       codeService: ServiceCode.marketPlace),
    // )
    //     .then((checkAlreadyRatedResponse) async {
    //   checkAlreadyRatedResponse.fold((l) {}, (checkAlreadyRatedResult) async {
    //     isAlreadyRated = checkAlreadyRatedResult.data;
    //   });
    // });
    if (info.user != null) {
      productReturn.user = UserModel(
          lastName: info.user!.laststname, firstName: info.user!.firstname);
    }

    if (info.departement != null) {
      productReturn.location!.departementName = info.departement!.name;
    }
    if (info.region != null) {
      productReturn.location!.regionName = info.region!.name;
    }
    if (info.sousprefecture != null) {
      productReturn.location!.sPrefectureName = info.sousprefecture!.name;
    }
    if (info.localite != null) {
      productReturn.location!.localiteName = info.localite!.name;
    }
    return productReturn;
  }
}

Future<ProductModel> fecthItemSmallDetails(
    Map<String, dynamic> productItem) async {
  if (productItem["code"] == codeIntrant.toString()) {
    ProductIntrantItem intrant = ProductIntrantItem.fromJson(productItem);
    List<String> images = [];
    for (var element in intrant.images) {
      images.add(element.link);
    }

    return ProductModel(
      identifiant: intrant.identifiant,
      paymentMethod: intrant.paymentMethod,
      paymentId: intrant.paymentId,
      code: intrant.code,
      location: ProductLocationModel(
        locationId: intrant.location.id,
        regionId: intrant.location.regionId,
        departementId: intrant.location.departementId,
        sPrefectureId: intrant.location.sprefectureId,
        localiteId: intrant.location.localiteId,

      ),
      disponibilite: '',
      fournisseurId: intrant.fournisseurId,
      id: intrant.id,
      quantity: intrant.product.quantity,
      images: images,
      imagesWithId: intrant.images,
      rating: intrant.notation.toDouble(),
      title: intrant.product.intrant != null
          ? intrant.product.intrant!.libelle
          : "",
      price: intrant.product.priceU.toInt(),
      publicationDate: intrant.publicationDate.toString(),
      status: intrant.status,
      typeOffers: intrant.offerType,
      unite: intrant.product.unitOfMeasurment,
      typeVente: intrant.typeVente ?? "",
      description: intrant.description,
      speculation: SpeculationModel(
        imageUrl: "",
        name: intrant.product.intrant != null
            ? intrant.product.intrant!.libelle
            : "",
        speculationId:
            intrant.product.intrant != null ? intrant.product.intrant!.id : 1,
      ),
    );
  } else if (productItem["code"] == codeTransport.toString()) {
    List<String> images = [];
    InfoByIdModel info = InfoByIdModel();
    TransportModel transport = TransportModel.fromJson(productItem);
    if (transport.images != null) {
      for (var element in transport.images!) {
        images.add(element.link);
      }
    }

    UseCase useCase = instance<UseCase>();

    // imgs.addAll(element.images!);
    await useCase
        .getInfoById(InfoByIdRequest(
          userId: transport.fournisseurId,
          regionId: transport.location.regionId,
          departementId: transport.location.departementId != 0 &&
                  transport.location.departementId != null
              ? transport.location.departementId
              : null,
          sprefectureId: transport.location.sprefectureId != 0 &&
                  transport.location.sprefectureId != null
              ? transport.location.sprefectureId
              : null,
          localiteId: transport.location.localiteId != 0 &&
                  transport.location.localiteId != null
              ? transport.location.localiteId
              : null,
        ))
        .then((value) => value.fold((l) {}, (r) {
              info = r;
            }));

    ProductModel productReturn = ProductModel(
      paymentMethod: transport.paymentMethod,
      paymentId: transport.paymentId,
      code: transport.code,
      startTrajetInfo: TrajetInfoModel(
        region:productItem["product"]["startTrajet"]["region"] != null? productItem["product"]["startTrajet"]["region"]["name"]:"",
        departement:
            productItem["product"]["startTrajet"]["departement"] != null
                ? productItem["product"]["startTrajet"]["departement"]["name"]
                : null,
        sprefecture:
            productItem["product"]["startTrajet"]["sprefecture"] != null
                ? productItem["product"]["startTrajet"]["sprefecture"]["name"]
                : null,
        localite: productItem["product"]["startTrajet"]["localite"] != null
            ? productItem["product"]["startTrajet"]["localite"]["name"]
            : null,
      ),
      arrivalTrajetInfo: TrajetInfoModel(
        region: productItem["product"]["arrivalTrajet"]["region"]["name"],
        departement:
            productItem["product"]["arrivalTrajet"]["departement"] != null
                ? productItem["product"]["arrivalTrajet"]["departement"]["name"]
                : null,
        sprefecture:
            productItem["product"]["arrivalTrajet"]["sprefecture"] != null
                ? productItem["product"]["arrivalTrajet"]["sprefecture"]["name"]
                : null,
        localite: productItem["product"]["arrivalTrajet"]["localite"] != null
            ? productItem["product"]["arrivalTrajet"]["localite"]["name"]
            : null,
      ),
      location: ProductLocationModel(
        regionName: "",
      ),
      disponibilite: '',
      fournisseurId: transport.fournisseurId,
      id: transport.id,
      quantity: transport.product.quantity.toInt(),
      images: images,
      imagesWithId: transport.images,
      rating: 0,
      title: transport.product.speculation != null
          ? transport.product.speculation!.libelle
          : "",
      price: transport.product.priceU.toInt(),
      description: transport.description,
      speculation: SpeculationModel(
        imageUrl: transport.product.speculation != null
            ? transport.product.speculation!.image
            : "",
        name: transport.product.speculation != null
            ? transport.product.speculation!.libelle
            : "",
        speculationId: transport.product.speculationId,
      ),
      publicationDate: transport.publicationDate.toString(),
      status: transport.status,
      typeOffers: transport.offerType,
      typeVente: "",
      unite: transport.product.unitOfMeasurment,
    );
    if (info.user != null) {
      productReturn.user = UserModel(
          lastName: info.user!.laststname, firstName: info.user!.firstname);
    }
    //productReturn.alreadyNoted = isAlreadyRated;
    if (info.departement != null) {
      productReturn.location!.departementName = info.departement!.name;
    }
    if (info.region != null) {
      productReturn.location!.regionName = info.region!.name;
    }
    if (info.sousprefecture != null) {
      productReturn.location!.sPrefectureName = info.sousprefecture!.name;
    }
    if (info.localite != null) {
      productReturn.location!.localiteName = info.localite!.name;
    }
    return productReturn;
  } else if (productItem["code"] == codePrestation.toString()) {
    ProductResponse prestation = ProductResponse.fromJson(productItem);
    List<String> images = [];
    //InfoByIdModel info = InfoByIdModel();
    for (var element in prestation.images!) {
      images.add(element.link);
    }
    // UseCase useCase = instance<UseCase>();

    // // imgs.addAll(element.images!);
    // await useCase
    //     .getInfoById(InfoByIdRequest(
    //       userId: prestation.fournisseurId,
    //       regionId: prestation.location!.regionId,
    //       departementId: prestation.location!.departementId != 0 &&
    //               prestation.location!.departementId != null
    //           ? prestation.location!.departementId
    //           : null,
    //       sprefectureId: prestation.location!.sousprefectureId != 0 &&
    //               prestation.location!.sousprefectureId != null
    //           ? prestation.location!.sousprefectureId
    //           : null,
    //       localiteId: prestation.location!.localiteId != 0 &&
    //               prestation.location!.localiteId != null
    //           ? prestation.location!.localiteId
    //           : null,
    //     ))
    //     .then((value) => value.fold((l) {}, (r) {
    //           info = r;
    //         }));

    ProductModel productReturn = ProductModel(
      paymentMethod: prestation.paymentMethod,
      paymentId: prestation.paymentId,
      code: prestation.code,
      categorieProduct: CategorieProductModel(
          id: productItem["product"]["category"]["id"],
          name: productItem["product"]["category"]["name"]),
      location: ProductLocationModel(
          regionName: prestation.location!.departementId.toString()),
      disponibilite: '',
      fournisseurId: prestation.fournisseurId,
      id: prestation.id,
      quantity: prestation.product.quantity,
      images: images,
      imagesWithId: prestation.images,
      rating: prestation.rate.toDouble(),
      title: prestation.product.speculation != null
          ? prestation.product.speculation!.name!
          : "",
      price: prestation.product.price.toInt(),
      description: prestation.description,
      speculation: SpeculationModel(
        imageUrl: "",
        name: "",
        speculationId: 1,
      ),
      publicationDate: prestation.publicationDate,
      status: prestation.status,
      typeOffers: prestation.typeOffer,
      typeVente: "",
      unite: productItem["product"]["unitOfMeasurmentName"],
    );
    // if (info.user != null) {
    //   productReturn.user = UserModel(
    //       lastName: info.user!.laststname, firstName: info.user!.firstname);
    // }
    // //productReturn.alreadyNoted = isAlreadyRated;
    // if (info.departement != null) {
    //   productReturn.location!.departementName = info.departement!.name;
    // }
    // if (info.region != null) {
    //   productReturn.location!.regionName = info.region!.name;
    // }
    // if (info.sousprefecture != null) {
    //   productReturn.location!.sPrefectureName = info.sousprefecture!.name;
    // }
    // if (info.localite != null) {
    //   productReturn.location!.localiteName = info.localite!.name;
    // }
    return productReturn;
  } else {
    ProductResponse productAgricul = ProductResponse.fromJson(productItem);
    SpeculationModel productSpeculation = SpeculationModel();
    ProductModel prodData;
    SpeculationRepo speculationRepo = SpeculationRepo();
    bool isAlreadyRated = true;

    InfoByIdModel info = InfoByIdModel();
    List<String> imgs = [];
    if (productAgricul.images != null) {
      for (ImageProduct img in productAgricul.images ?? []) {
        imgs.add(img.link);
      }
    }

    // imgs.addAll(productAgricul.images!);
    if (productAgricul.product.speculationId != null) {
      await speculationRepo
          .getSpecificSpeculation(productAgricul.product.speculationId!)
          .then((speculation) async {
        if (speculation != null) productSpeculation = speculation;
      });
    }

    prodData = ProductModel(
        paymentMethod: productAgricul.paymentMethod,
        paymentId: productAgricul.paymentId,
        code: productAgricul.code,
        images: imgs.isNotEmpty
            ? imgs
            : productSpeculation.imageUrl != null
                ? [productSpeculation.imageUrl!]
                : [],
        // images: [speculation.imageUrl!],
        id: productAgricul.id,
        quantity: productAgricul.product.quantity,
        title: productSpeculation.name ?? "",
        price: productAgricul.product.price.toInt(),
        speculation: productSpeculation,
        unite: productAgricul.product.unitOfMeasurment??"",
        description: productAgricul.description,
        fournisseurId: productAgricul.fournisseurId,
        status: productAgricul.status,
        user: info.user != null
            ? UserModel(
                lastName: info.user!.laststname,
                firstName: info.user!.firstname,
              )
            : UserModel(),
        typeOffers: productAgricul.typeOffer,
        typeVente: productAgricul.typeVente ?? "",
        disponibilite: productAgricul.availability,
        dataDisponibilite: productAgricul.availabilityDate,
        publicationDate: productAgricul.publicationDate,
        rating: productAgricul.rate,
        alreadyNoted: isAlreadyRated,
        imagesWithId: productAgricul.images,
        identifiant: productItem["product"]["identifiant"],
        location: ProductLocationModel(
          regionId: productAgricul.location!.regionId,
          departementId: productAgricul.location!.departementId,
          sPrefectureId: productAgricul.location!.sousprefectureId,
          localiteId: productAgricul.location!.localiteId,
        ));
    return prodData;
  }
}

extension CartResponseMapper on CartResponseBody {
  CartModel toDomain() {
    List<CartItem> item = [];
    for (var element in cartItems) {
      item.add(element.toDomain());
    }
    return CartModel(
      id: id,
      cartItem: item,
      createdAt: createdAt,
      status: status,
      totalPrice: totalPrice.toInt(),
      userId: userId,
    );
  }
}

extension CartItemResponseMapper on CartItemReponse {
  CartItem toDomain() {
    return CartItem(
      id: id,
      fournisseurId: fournisseurId,
      idProduct: idProduct,
      price: price.toInt(),
      quantity: quantity,
    );
  }
}
