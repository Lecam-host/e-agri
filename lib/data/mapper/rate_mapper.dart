import 'package:eagri/data/request/request.dart';
import 'package:eagri/domain/model/user_model.dart';

import '../../domain/model/rate_model.dart';
import '../../domain/usecase/use_case.dart';
import 'package:eagri/app/di.dart';

extension RateAndCommentDataMapper on RateAndCommentItem? {
  Future<RateAndCommentItem> toDomain() async {
    UserModel userModel = UserModel();
    UseCase useCase = instance<UseCase>();
    await useCase
        .getInfoById(
          InfoByIdRequest(userId: this!.notePar),
        )
        .then((response) => response.fold((failure) => null, (data) {
              userModel.firstName = data.user!.firstname;
              userModel.lastName = data.user!.laststname;
              userModel.id = data.user!.id;
            }));
    return RateAndCommentItem(
      codeService: this!.codeService,
      comment: this!.comment,
      id: this!.id,
      note: this!.note,
      notePar: this!.notePar,
      resourceId: this!.resourceId,
      userId: this!.userId,
      user: userModel,
      createdAt: this!.createdAt,
    );
  }
}
