import 'package:dio/dio.dart';

import 'failure.dart';

enum DataSource {
  success,
  noContent,
  babRequest,
  forBiden,
  unauthorised,
  notFound,
  internalSeverError,
  connectTimeout,
  cancel,
  recieveTimeout,
  sendTimeout,
  cacheError,
  noInternetConnection,
  defaultResponse,
}

class ErrorHandler implements Exception {
  late Failure failure;
  ErrorHandler.handle(dynamic error) {
    if (error is DioError) {
      failure = _handlerError(error);
    } else {
      failure = DataSource.defaultResponse.getFailure();
    }
  }
  Failure _handlerError(DioError dioError) {
    switch (dioError.error) {
      case DioErrorType.connectionTimeout:
        return DataSource.connectTimeout.getFailure();
      case DioErrorType.sendTimeout:
        return DataSource.sendTimeout.getFailure();

      case DioErrorType.receiveTimeout:
        return DataSource.recieveTimeout.getFailure();

      case DioErrorType.badResponse:
        switch (dioError.response!.statusCode) {
          case ResponseCode.babRequest:
            return DataSource.babRequest.getFailure();
          case ResponseCode.forBiden:
            return DataSource.forBiden.getFailure();
          case ResponseCode.unauthorised:
            return DataSource.unauthorised.getFailure();
          case ResponseCode.notFound:
            return DataSource.noContent.getFailure();
          case ResponseCode.internalSeverError:
            return DataSource.internalSeverError.getFailure();
          default:
            return DataSource.defaultResponse.getFailure();
        }

      case DioErrorType.cancel:
        return DataSource.cancel.getFailure();

      case DioErrorType.unknown:
        return DataSource.defaultResponse.getFailure();

      default:
        return DataSource.defaultResponse.getFailure();
    }
  }
}

extension DataSourceExtension on DataSource {
  Failure getFailure() {
    switch (this) {
      case DataSource.babRequest:
        return Failure(ResponseCode.babRequest, ResponseMessage.babRequest);
      case DataSource.forBiden:
        return Failure(ResponseCode.forBiden, ResponseMessage.forBiden);

      case DataSource.unauthorised:
        return Failure(ResponseCode.unauthorised, ResponseMessage.unauthorised);

      case DataSource.notFound:
        return Failure(ResponseCode.notFound, ResponseMessage.notFound);

      case DataSource.internalSeverError:
        return Failure(ResponseCode.internalSeverError,
            ResponseMessage.internalSeverError);

      case DataSource.connectTimeout:
        return Failure(
            ResponseCode.connectTimeout, ResponseMessage.connectTimeout);
      case DataSource.cancel:
        return Failure(ResponseCode.cancel, ResponseMessage.cancel);
      case DataSource.recieveTimeout:
        return Failure(
            ResponseCode.recieveTimeout, ResponseMessage.recieveTimeout);

      case DataSource.sendTimeout:
        return Failure(ResponseCode.sendTimeout, ResponseMessage.sendTimeout);
      case DataSource.cacheError:
        return Failure(ResponseCode.cacheError, ResponseMessage.cacheError);

      case DataSource.noInternetConnection:
        return Failure(ResponseCode.noInternetConnection,
            ResponseMessage.noInternetConnection);
      case DataSource.defaultResponse:
        return Failure(
            ResponseCode.defaultResponse, ResponseMessage.defaultResponse);

      default:
        return Failure(
            ResponseCode.defaultResponse, ResponseMessage.defaultResponse);
    }
  }
}

class ResponseCode {
  static const int success = 200;
  static const int created = 201;
  static const int babRequest = 400;
  static const int forBiden = 403;
  static const int unauthorised = 401;
  static const int notFound = 404;
  static const int internalSeverError = 500;
  static const int parcelExist = 4001;

// local status code
  static const int defaultResponse = -1;
  static const int connectTimeout = -2;
  static const int cancel = -3;
  static const int recieveTimeout = -4;
  static const int sendTimeout = -5;
  static const int cacheError = -6;
  static const int noInternetConnection = -7;
}

class ResponseMessage {
  static const String success = "success";
  static const String noContent = "success with no content";
  static const String babRequest = "bad request, try again later";
  static const String forBiden = "forbiden request,try again later";
  static const String unauthorised = "user unauthorised,try again later";
  static const String notFound = "url is not found,try again later";
  static const String internalSeverError =
      "Oops,une erreur s'est produite, veuillez reessayer";

// local status code
  static const String defaultResponse =
      "Oops,une erreur s'est produite, veuillez reessayer ";
  static const String connectTimeout = "Reseau instable";
  static const String cancel = "request was cancelled, try again later";
  static const String recieveTimeout = "time out, try again later";
  static const String sendTimeout = "time out, try again later";
  static const String cacheError = "cache error , try again later";
  static const String noInternetConnection = "please check your connection";
}

class ApiInternalStatus {
  static const int success = 0;
  static const int failure = 1;
}
