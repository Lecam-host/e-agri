import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import '../../app/app_prefs.dart';
import '../../app/constant.dart';
import '../../app/di.dart';

const String applictionJson = "application/json";
const String contentType = "content-type";
const String accept = "accept";
const String authorization = "Authorization";
const String beareAuthorization = "Bearer Authorization";

const String defaultLanguage = "language";

class DioFactory {
  AppSharedPreference prefs;

  DioFactory(this.prefs);
  Future<Dio> getDio() async {
    Dio dio = Dio();

    String language = await prefs.getAppLanguage();
    String token = "";
    AppSharedPreference appSharedPreference = instance<AppSharedPreference>();
    token = await appSharedPreference.getUserToken();
    Duration timeOut = const Duration(seconds: 60 * 1000);
    dio.interceptors.add(InterceptorsWrapper(
      onRequest:
          (RequestOptions options, RequestInterceptorHandler handler) async {
        // Appeler votre fonction ici

        //  await verifyConnection.checkNetwork();

        // Poursuivre la requête
        return handler.next(options);
      },
    ));
    Map<String, String> headers = {
      contentType: applictionJson,
      accept: applictionJson,
      authorization: token,
      defaultLanguage: language,
    };
    dio.options = BaseOptions(
      baseUrl: Constant.baseUrl,
      connectTimeout: timeOut,
      receiveTimeout: timeOut,
      headers: headers,
    );
    if (kReleaseMode) {
    } else {
      // dio.interceptors.add(PrettyDioLogger(
      //   requestHeader: true,
      //   request: true,
      //   responseHeader: true,
      // ));
    }
    return dio;
  }
}
