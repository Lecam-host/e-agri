import 'dart:convert';

import 'dart:io';

import 'package:dio/dio.dart';
import 'package:eagri/data/responses/lieu_responses.dart';
import 'package:eagri/data/responses/local/local_responses.dart';
import 'package:eagri/domain/model/alerte_model.dart';
import 'package:eagri/domain/model/command_model.dart';
import 'package:eagri/domain/model/creditor_model.dart';
import 'package:eagri/domain/model/insurer_model.dart';
import 'package:eagri/domain/model/offre_insurance_model.dart';
import 'package:eagri/domain/model/intrant_model.dart';
import 'package:eagri/domain/model/methode_paiement_model.dart';
import 'package:eagri/domain/model/typeCulture_model.dart';
import 'package:eagri/domain/model/type_offre__insurance_model.dart';
import 'package:retrofit/error_logger.dart';
import 'package:retrofit/http.dart';

import '../../app/constant.dart';
import '../../domain/model/achat_or_preachat_model.dart';
import '../../domain/model/cart_model.dart';
import '../../domain/model/category_model.dart';

import '../../domain/model/mes_produits_commander_model.dart';
import '../../domain/model/model.dart';
import '../../domain/model/odered_product_credit_model.dart';
import '../../domain/model/offfre_item_model.dart';
import '../../domain/model/offre_credit_model.dart';
import '../../domain/model/favorite_model.dart';
import '../../domain/model/history_achat_model.dart';
import '../../domain/model/history_vente_model.dart';
import '../../domain/model/info_by_id_model.dart';
import '../../domain/model/insurance_by_insurer_model.dart';
import '../../domain/model/paiement_model.dart';
import '../../domain/model/prestation_model.dart';
import '../../domain/model/rate_model.dart';
import '../../domain/model/shop_model.dart';
import '../../domain/model/transport_model.dart';
import '../../domain/model/type_offre_credit_model.dart';
import '../../domain/model/user_stat_model.dart';
import '../request/intran_request.dart';
import '../request/request_object.dart';
import '../responses/account_response.dart';
import '../responses/advice_response.dart';
import '../responses/responses.dart';
import '../responses/shop_response.dart';
import '../responses/weather_response.dart';
part "app_api.g.dart";

@RestApi()
abstract class AppServiceClient {
  factory AppServiceClient(Dio dio, {String baseUrl}) = _AppServiceClient;
  @POST("${Constant.baseUrl}${Constant.urlMsSecurity}/auth/account")
  Future<AuthResponse> login(
    @Field("login") String number,
    @Field("password") String password,
  );
  @POST(
      "${Constant.baseUrl}${Constant.urlMsSecurity}/registration/push/addRegistrationId")
  Future<BaseResponse> saveRegistrationId(
    @Field("clientId") String clientId,
    @Field("registrationId") String registrationId,
  );
  @GET(
      "${Constant.baseUrl}${Constant.urlMsSecurity}/account-manage/getUserDetailsById/{id}")
  Future<GetUserInfoResponse> getUserInfo(@Path() int id);

  @GET("${Constant.baseUrl}${Constant.urlMsSecurity}/auth/get-profile")
  Future<AccountInfoResponse> getAccountInfoByToken();

  @GET("${Constant.baseUrl}${Constant.urlMsAdvice}/show?userId=1")
  Future<AllAdviceDataResponse> allConseils(
    @Query("perPage") int? perPage,
    @Query("page") int? pageNumber,
  );

  @GET("${Constant.baseUrl}${Constant.urlMsAdvice}/show/one/{id}")
  Future<DetailAdivceResponse> adviceDetails(@Path() int id);

  @GET("${Constant.baseUrl}${Constant.urlMsCategory}/category/get")
  Future<AllAdviceCategoryResponse> allCategoryAdvices();

  @POST("${Constant.baseUrl}${Constant.urlMsAdviceSocle}/search")
  Future<AllAdviceDataResponse> searchAdvice(
    @Field("categoriesId") List<int>? categoriesId,
    @Field("advicesType") String? adviceType,
    @Field("mediaType") List<String>? mediaType,
    @Field("keyword ") String? keyword,
    @Field("userId") int? userId,
  );

  @POST("${Constant.baseUrl}${Constant.urlMsAdvice}/ask")
  Future<AskAdviceResponse> askAdvice(
    @Field("entitiesId") List<int> entitiesId,
    @Field("categoriesId") List<int> categoriesId,
    @Field("adviceType") int adviceType,
    @Field("mediaType") List<int> mediaType,
    @Field("description") String description,
    @Field("userId") int userId,
  );

  @GET("${Constant.baseUrl}${Constant.urlMsAdvice}/ask/show")
  Future<ListAskAdviceResponse> getUserListAskAdvice(
    @Query("userId") int? userId,
    @Query("status") int? statusAsk,
    @Query("perPage") int? perPage,
    @Query("page") int? pageNumber,
  );

  @GET("${Constant.baseUrl}${Constant.urlMsWeather}/forecast")
  Future<WeatherResponse> getCurrentWeather(
      @Query("lat") double lat, @Query("lon") double lon);

  @GET("${Constant.baseUrl}${Constant.urlMsPrice}/official")
  Future<GetSpeculationPriceResponse> getListSpeculationPrice(
    @Query("pageNumber") int pageNumber,
    @Query("pageSize") int pageSize,
  );

  @GET("${Constant.baseUrl}${Constant.urlMsPrice}/non_official")
  Future<GetSpeculationPriceNonOfficielResponse>
      getListSpeculationPriceEnterByUser(
    @Query("pageNumber") int pageNumber,
    @Query("pageSize") int pageSize,
  );

  @POST("${Constant.baseUrl}${Constant.urlMsPrice}")
  Future<BaseResponse> addSpeculationPrice(
    @Field("speculationId") int? speculationId,
    @Field("userId") int? userId,
    @Field("from") String? from,
    @Field("to") String? to,
    @Field("unit") String? unit,
    @Field("source") String? source,
    @Field("official") bool? official,
    @Field("data") List<Map<String, dynamic>>? data,
  );

  @GET("${Constant.baseUrl}${Constant.urlMsLocation}/region")
  Future<ListRegionResponse> getListRegion();

  @GET(
      "${Constant.baseUrl}${Constant.urlMsLocation}/region/departement/{regionId}")
  Future<ListDepartementResponse> getListDepartementOfRegion(
    @Path() int regionId,
  );

  @GET(
      "${Constant.baseUrl}${Constant.urlMsLocation}/region/departement/sp/{departementId}")
  Future<ListSousPrefectureResponse> getListSousPrefectureOfDepartement(
    @Path() int departementId,
  );

  @GET(
      "${Constant.baseUrl}${Constant.urlMsLocation}/region/departement/sp/localite/{spId}")
  Future<ListLocaliteResponse> getListLocaliteOfSousPrefecture(
    @Path() int spId,
  );

  @GET("${Constant.baseUrl}${Constant.urlMsSpeculation}")
  Future<ListSpeculationResponse> getListSpeculation();

  @POST("${Constant.baseUrl}${Constant.urlMsPrice}/history")
  Future<GetSpeculationPriceResponse> getSpeculationPriceInAllRegion(
    @Field("speculationId") int? speculationId,
    @Field("userId") int? userId,
    @Field("regionId") int? regionId,
    @Field("from") String? from,
    @Field("to") String? to,
    @Field("userType") bool official,
  );

  @POST("${Constant.baseUrl}${Constant.urlMsPrice}/comparison")
  Future<GetSpeculationPriceResponse> compareSpeculationPrice(
    @Field("speculationId") int? speculationId,
    @Field("locationBodyId") List<Map<String, dynamic>>? locationBodyId,
    @Field("from") String? from,
    @Field("to") String? to,
    @Field("userType") bool? official,
  );

  @POST("${Constant.baseUrl}${Constant.urlMsWeather}/parcel/create")
  Future<AddParcelInfoResponse> createParcel(
    @Field("lat") double? lat,
    @Field("lon") double? lon,
    @Field("userId") int? userId,
    @Field("name") String? name,
  );

  @POST("${Constant.baseUrl}${Constant.urlMsWeather}/parcel/create-bulk")
  Future<CreateFullParcelResponse> createFullParcel(
      @Body() List<ParcelRequestJson> listParcel);

  @GET("${Constant.baseUrl}${Constant.urlMsWeather}/history")
  Future<ListHistoryWeatherInfoResponse> historyWeather(
    @Query("lat") double? lat,
    @Query("lon") double? lon,
    @Query("dateDeDebut") String? dateDeDebut,
    @Query("dateDeDeFin") String? dateDeDeFin,
  );

  @GET("${Constant.baseUrl}${Constant.urlMsWeather}/parcel/soil-data")
  Future<AddParcelInfoResponse> getParcelInfo(
      @Query("parcelId") String parcelId);

  @GET("${Constant.baseUrl}${Constant.urlMsConfig}/show")
  Future<AllConfigResponse> getConnfig();

  @POST("${Constant.baseUrl}${Constant.urlMsAlert}/reporting/add")
  Future<AddAlertResponse> addAlert(
    @Field("userId") int userId,
    @Field("regionId") int regionId,
    @Field("departementId") int departementId,
    @Field("sousprefectureId") int sousprefectureId,
    @Field("localiteId") int localiteId,
    @Field("speculationId") int speculationId,
    @Field("description") String description,
    @Field("domainId") int domain,
  );

  @GET("${Constant.baseUrl}${Constant.urlMsAlert}/get")
  Future<AlerteModel> getListAlert();

  @GET("${Constant.baseUrl}${Constant.urlMsAlert}/domain/get")
  Future<GetListDomaineResponse> getDomaineAlert();
  @PUT("${Constant.baseUrl}${Constant.urlMsAlert}/reporting/add-file/{id}")
  @MultiPart()
  Future<BaseResponse> addAlertFile(
      @Path() int id, @Part(name: "files") List<File> files);

  @GET("${Constant.baseUrl}${Constant.urlMsWeather}/locality/all")
  Future<ListLocationForWeatherResponse> getListLocationForWeather();

  @GET("${Constant.baseUrl}${Constant.urlMsWeather}/parcel/byUser")
  Future<ListParcelResponse> getUserParcel(
    @Query("userId") int? userId,
  );

  @POST("${Constant.baseUrl}${Constant.urlMsPrice}/min_maxOnRegion")
  Future<GetMinAndMaxSpeculationPriceResponse> getMinAndMaxSpeculationPrice(
    @Query("speculationId") int speculationId,
    @Query("official") bool official,
  );

  @GET("${Constant.baseUrl}${Constant.urlMsWeather}/recommendation")
  Future<GetListRecommandationResponse> getListRecommandation(
    @Query("speculationId") int? speculationId,
    @Query("temperature") double temperature,
    @Query("precipitation") double precipitation,
    @Query("windSpeed") double windSpeed,
    @Query("humidity") int humidity,
  );

  @GET("${Constant.baseUrl}${Constant.urlMsAdvice}/ask/answer/get")
  Future<AllAdviceDataResponse> getListAnswerAskAdvice(
    @Query("askId") int askId,
  );

  @POST("${Constant.baseUrl}${Constant.urlMsAdviceSocle}/add")
  @MultiPart()
  Future<AddAdviceResponse> addAdvice({
    @Part(name: "adviceType") required String adviceType,
    @Part(name: "title") required String title,
    @Part(name: "categoryId") required String categoryId,
    @Part(name: "adviceText") String? adviceText,
    @Part(name: "descriptionFiles") String? descriptionFiles,
    @Part(name: "userIdForCreation") required int userIdForCreation,
    @Part(name: "file1") File? file1,
    @Part(name: "file2") File? file2,
    @Part(name: "file3") File? file3,
    @Part(name: "illustrationImage") File? illustrationImage,
    @Part(name: "description") required String description,
  });

  @POST("${Constant.baseUrl}${Constant.urlMsCatalogs}/products")
  @MultiPart()
  Future<AddProductOnMarketResponse> appProductOnMarket({
    @Part(name: "fournisseurId") required int fournisseurId,
    @Part(name: "speculationId") required int speculationId,
    @Part(name: "price_u") required int price,
    @Part(name: "quantity") required int quantity,
    @Part(name: "typeVente") required String typeVente,
    @Part(name: "availability") required String availability,
    @Part(name: "description") required String description,
    @Part(name: "unitOfMeasurment") required String unitOfMeasurment,
    @Part(name: "location") required Map<String, dynamic>? location,
    @Part(name: "offerType") required String offerType,
    @Part(name: "availabilityDate") String? availabilityDate,
    @Part(name: "images") List<File>? images,
    @Part(name: "paymentId") required int paymentId,
    @Part(name: "categorieId") required int categorieId,
    @Part(name: "numberSlice") int? numberSlice,
    @Part(name: "unitTime") String? unitTime,
    @Part(name: "price_pre_order") double? pricePreOrder,
    @Part(name: "origin")required String origin,

  });

  @PUT(
      "${Constant.baseUrl}${Constant.urlMsCatalogs}/products/update/product/{id}")
  @MultiPart()
  Future<GetDetailProductResponse> updateProductOnMarket(
    @Path() int id,
    @Part(name: "productId") int productId,
    @Part(name: "fournisseurId") int? fournisseurId,
    @Part(name: "price_u") int? price,
    @Part(name: "quantity") int? quantity,
    @Part(name: "typeVente") String? typeVente,
    @Part(name: "availability") String? availability,
    @Part(name: "description") String? description,
    @Part(name: "unitOfMeasurment") String? unitOfMeasurment,
    @Part(name: "offerType") String? offerType,
    @Part(name: "availabilityDate") String? availabilityDate,
    // @Part(name:"images") List<File>? images,
    @Part(name: "locationId") int? locationId,
    @Part(name: "regionId") int? regionId,
    @Part(name: "departementId") int? departementId,
    @Part(name: "sprefectureId") int? sousPrefectureId,
    @Part(name: "numberSlice") int? numberSlice,
    @Part(name: "unitTime") String? unitTime,
  );
  @PUT("${Constant.baseUrl}${Constant.urlMsCatalogs}/products/{id}/disable")
  Future<GetDetailProductResponse> disableProduct(
    @Path("id") int id,
  );
  @PUT("${Constant.baseUrl}${Constant.urlMsCatalogs}/products/{id}/enable")
  Future<GetDetailProductResponse> enableProduct(@Path("id") int id);
  @GET("${Constant.baseUrl}${Constant.urlMsCatalogs}/products/typesVente")
  Future<ListStringResponse> getListTypesVente();
  @GET("${Constant.baseUrl}${Constant.urlMsCatalogs}/offerTypes")
  Future<ListStringResponse> getListOfferTypes();

  @GET("${Constant.baseUrl}${Constant.urlMsCatalogs}/products/all")
  Future<BodyGetListProductResponse> getlistProduct();

  @GET("${Constant.baseUrl}${Constant.urlMsCatalogs}/products/search")
  Future<BodyGetListProductResponse> searchProduct(
    @Query("regionId") int? regionId,
    @Query("departementId") int? departementId,
    @Query("locationId") int? locationId,
    @Query("sprefectureId") int? sprefectureId,
    @Query("speculationId") List<int>? speculationId,
    @Query("offerType") String? offerType,
    @Query("status") String? status,
    @Query("price_s") double? priceMin,
    @Query("price_e") double? priceMax,
    @Query("quantity_s") int? quantityMin,
    @Query("quantity_e") int? quantityMax,
    @Query("fournisseurId") int? fournisseurId,
    @Query("limit") int limit,
    @Query("page") int page,
    @Query("publicationDate_s") String? publicationDateMin,
    @Query("publicationDate_e") String? publicationDateMax,
    @Query("note_s") double? noteMin,
    @Query("note_e") double? noteMax,
    @Query("categorieId") int? categorieId,
    @Query("unitOfMeasurment") String? unitOfMeasurment,
    @Query("code") int? codeTypeProduct,
  );
  @GET("${Constant.baseUrl}${Constant.urlMsCatalogs}/products/search")
  Future<GetListIntrantResponse> searchProductIntrant(
    @Query("regionId") int? regionId,
    @Query("departementId") int? departementId,
    @Query("locationId") int? locationId,
    @Query("sprefectureId") int? sprefectureId,
    @Query("offerType") String? offerType,
    @Query("status") String? status,
    @Query("price_s") double? priceMin,
    @Query("price_e") double? priceMax,
    @Query("quantity_s") int? quantityMin,
    @Query("quantity_e") int? quantityMax,
    @Query("fournisseurId") int? fournisseurId,
    @Query("limit") int limit,
    @Query("page") int page,
    @Query("publicationDate_s") String? publicationDateMin,
    @Query("publicationDate_e") String? publicationDateMax,
    @Query("note_s") double? noteMin,
    @Query("note_e") double? noteMax,
    @Query("categorieId") int? categorieId,
    @Query("unitOfMeasurment") String? unitOfMeasurment,
    @Query("code") int? codeTypeProduct,
  );

  @GET("${Constant.baseUrl}${Constant.urlMsCatalogs}/products/{id}")
  Future<GetDetailProductResponse> getDetailsProduct(
    @Path("id") int idProduct,
  );

  @GET("${Constant.baseUrl}${Constant.urlMsCatalogs}/fournisseur/{id}")
  Future<DataProductUserResponse> getlistProductFournisseur(
    @Path() int id,
  );
  @GET("${Constant.baseUrl}${Constant.urlMsCatalogs}/fournisseur/{id}/{code}")
  Future<DataProductUserResponse> getlistProductFournisseurWithCode(
    @Path("id") int id,
    @Path("code") int code,
  );

  @DELETE("${Constant.baseUrl}${Constant.urlMsCatalogs}/products/{id}")
  Future<BaseResponse> deleteProduct(
    @Path() int id,
  );
  @POST("${Constant.baseUrl}${Constant.urlMsShoppingCart}")
  Future<AddProductInShoppingCartResponse> addProductInShoppingCart({
    @Field("userId") required int userId,
    @Field("items") required List<AddProductShoppingCartsItem> items,
    // @Part(value: "files") List<File>? images,
  });

  @GET("${Constant.baseUrl}/${Constant.urlMsMiddleware}")
  Future<InfoByIdModel> getListInfo(
    @Query("regionid") int? regionId,
    @Query("speculationid") int? speculationid,
    @Query("userid") int? userid,
    @Query("departementid") int? departementid,
    @Query("sousprefectureid") int? sousprefectureid,
    @Query("localiteid") int? localiteid,
  );

  @GET("${Constant.baseUrl}${Constant.urlMsShoppingCart}/users/{userId}")
  Future<ListCartResponse> getUserCart(
    @Path("userId") int userId,
  );

  @POST("${Constant.baseUrl}${Constant.urlMsNotation}")
  Future<BaseResponse> addNotation(
    @Field("userId") int userId,
    @Field("codeService") String codeService,
    @Field("resourceId") int resourceId,
    @Field("notePar") int noteurId,
    @Field("note") int note,
    @Field("comment") String? comment,
  );

  @GET(
      "${Constant.baseUrl}${Constant.urlMsNotation}/check/{userId}/{codeService}/{resourceId}")
  Future<CheckRateResponse> checkRating(
    @Path("userId") int userId,
    @Path("resourceId") int resourceId,
    @Path("codeService") int codeService,
  );

  @POST(
      "${Constant.baseUrl}${Constant.urlMsOrder}/initiate/users/{userId}/carts/{cartId}")
  Future<CreateCommandModel> createCommannd(
    @Path("userId") int userId,
    @Path("cartId") int cartId,
  );

  @GET("${Constant.baseUrl}${Constant.urlMsOrder}/users/{userId}")
  Future<GetUserCommands> getUserCommand(
    @Path("userId") int userId,
  );

  @DELETE(
      "${Constant.baseUrl}${Constant.urlMsShoppingCart}/{cartId}/items/{itemId}")
  Future<BaseResponse> deleteProdInCart(
    @Path("cartId") int cartId,
    @Path("itemId") int itemId,
  );

  @POST("${Constant.baseUrl}${Constant.urltMsPaiement}/create")
  Future<CreatePaiementResponse> createPaiement(
    @Field("orderId") int orderId,
    @Field("methodId") int methodId,
    @Field("phone") String? phone,
  );

  @POST("${Constant.baseUrl}${Constant.urltMsPaiement}/origination")
  Future<ConfirmPaymentResponse> confirmPaiement(
    @Query("signature") String signature,
    @Query("codeOtp") String codeOtp,
  );

  @GET("${Constant.baseUrl}${Constant.urltMsPaiement}/verification")
  Future<VerificationPaymentResponse> verificationPayment(
    @Query("signature") String signature,
  );

  @GET("${Constant.baseUrl}${Constant.urltMsPaiement}/invoice/order/{orderId}")
  Future<GetCommandFactureResponse> getCommandFacture(
    @Path("orderId") int orderId,
  );
  @GET("${Constant.baseUrl}${Constant.urltMsPaiement}/method/all")
  Future<MethodPaiementModel> getMethodPaiementAll();

  @POST("${Constant.baseUrl}${Constant.urlMsFavorite}/store")
  Future<AddUserFavoriteResponse> addFavorite(
    @Field("speculationId") List<int> speculationIds,
  );

  @GET("${Constant.baseUrl}${Constant.urlMsFavorite}/by-user")
  Future<ListUserFavoriteResponse> getUserFavorite();

  @GET("${Constant.baseUrl}${Constant.urlMsNotation}/search")
  Future<GetListCommentOnProductResponse> getListCommentOnProduct(
    @Query("resourceId") int resourceId,
    @Query("codeService") String codeService,
  );

  @POST("${Constant.baseUrl}${Constant.urlMsConsultation}/store")
  Future<BaseResponse> notifConsultationProduct(
    @Field("productId") int productId,
    @Field("date") String date,
  );

  @GET("${Constant.baseUrl}${Constant.urlMsLogs}/buy-seller")
  Future<HistoryAchatResponse> getUserHistoryAchat(
    @Query("userId") int userId,
    @Query("page") int page,
    @Query("perPage") int perPage,
  );
  @GET("${Constant.baseUrl}${Constant.urlMsLogs}/product-seller/user")
  Future<HistoryVenteResponse> getUserHistoryVente(
    @Query("userId") int userId,
    @Query("page") int page,
    @Query("perPage") int perPage,
  );

  @GET("${Constant.baseUrl}${Constant.urlMsSpeculation}/category")
  Future<GetListCategoryProductResponse> getListCategoryProduct();

  @GET(
      "${Constant.baseUrl}${Constant.urlMsCatalogs}/products/speculation/number")
  Future<GetNbOffreAchatBySpeculation> getListNbOfferAchatBySpeculation();

  @GET("${Constant.baseUrl}${Constant.urlMsDisponibilite}/get/offer-sell/{id}")
  Future<GetListOffreAchatFind> getListOffreAchatFind(
    @Path("id") int idProduct,
  );

  @DELETE("${Constant.baseUrl}${Constant.urlMsOrder}/{id}")
  Future<BaseResponse> deleteCommand(
    @Path("id") int commandId,
  );

  @DELETE("${Constant.baseUrl}${Constant.urlMsFavorite}/remove/{id}")
  Future<BaseResponse> deleteFavorite(
    @Path("id") int favoriteId,
  );

  @POST(
      "${Constant.baseUrl}${Constant.urlMsCatalogs}/intrants/products/products/lists")
  @MultiPart()
  Future<BaseResponse> addIntrant(
    @Part(name: "fournisseurId") int fournisseurId,
    @Part(name: "intrantId") int intrantId,
    @Part(name: "intrantCategorieId") int intrantCategorieId,
    @Part(name: "certification") String? certification,
    @Part(name: "availability") String availability,
    @Part(name: "availabilityDate") String? availabilityDate,
    @Part(name: "expirationDate") String? expirationDate,
    @Part(name: "description") String? description,
    @Part(name: "offerType") String offerType,
    @Part(name: "images") List<File>? images,
    @Part(name: "data") List<DataAddIntrant> data,
    @Part(name: "numberSlice") int? numberSlice,
    @Part(name: "unitTime") String? unitTime,
  );

  @GET(
      "${Constant.baseUrl}${Constant.urlMsCatalogs}/${Constant.urlIntrant}/${Constant.urlTypeCulture}/all")
  Future<TypeCultureModel> getTypeCulturesAll();

  @GET("${Constant.baseUrl}${Constant.urlMsCatalogs}${Constant.urlIntrant}/all")
  Future<IntrantModel> getIntrantAll();

  @GET("${Constant.baseUrl}${Constant.urlMsPrestation}/category-service/get")
  Future<GetListCategoryPrestation> getCategoryPrestation();

  @POST("${Constant.baseUrl}${Constant.urlMsPrestation}/purchase-service/add")
  @MultiPart()
  Future<BaseResponse> addPrestation(
    @Part(name: "fournisseurId") int fournisseurId,
    @Part(name: "categoryId") int categoryId,
    @Part(name: "speculationId") int speculationId,
    @Part(name: "unitOfMeasurment") String unitOfMeasurment,
    @Part(name: "price_u") int price,
    @Part(name: "paymentId") String paymentId,
    @Part(name: "availabilityDate") String? availabilityDate,
    @Part(name: "description") String? description,
    @Part(name: "location") String location,
    @Part(name: "images") List<File>? images,
    @Part(name: "availability") String availability,
    @Part(name: "numberSlice") int? numberSlice,
    @Part(name: "unitTime") String? unitTime,
  );

  @PUT(
      "${Constant.baseUrl}${Constant.urlMsPrestation}/purchase-service/service-request/{offerId}")
  Future<BaseResponse> demandePrestation(
    @Path("offerId") int prestationId,
    @Field("quantity") int quantity,
    @Field("userId") int userId,
    @Field("dateStart") String dateStart,
    @Field("dateEnd") String dateEnd,
    @Field("location") List<int> location,
  );

  @GET(
      "${Constant.baseUrl}${Constant.urlMsPrestation}/purchase-service/service-request/user/send/{userId}")
  Future<GetListPrestationResponse> getDemandePrestationSend(
    @Path("userId") int userId,
  );

  @GET(
      "${Constant.baseUrl}${Constant.urlMsPrestation}/purchase-service/service-request/user/receive/{userId}")
  Future<GetListPrestationResponse> getDemandePrestationRecu(
    @Path("userId") int userId,
  );

  @PUT(
      "${Constant.baseUrl}${Constant.urlMsPrestation}/purchase-service/service-request/validate/{id}")
  Future<BaseResponse> validateDemandePrestation(
    @Path("id") int prestationId,
  );

  @GET("${Constant.baseUrl}${Constant.urlMsPrestation}/unit-of-measurment/get")
  Future<GetUniteResponse> getListPrestationUnite();

  @PUT(
      "${Constant.baseUrl}${Constant.urlMsPrestation}/purchase-service/update/{id}")
  Future<BaseResponse> updatePrestation(
    @Path("id") int prestationId,
    @Field("price_u") int? priceU,
    @Field("availability") String? availability,
    @Field("availabilityDate") String? availabilityDate,
    @Field("description") String? description,
    @Field("location") List<int>? location,
  );

  @GET("${Constant.baseUrl}${Constant.urlMsCatalogs}/transports/vehicle/all")
  Future<GetListCarResponse> getListCar();

  @GET(
      "${Constant.baseUrl}${Constant.urlMsCatalogs}/transports/unit-of-measurment/get")
  Future<GetUniteResponse> getListTransportUnite();

  @POST("${Constant.baseUrl}${Constant.urlMsCatalogs}/transports/products")
  @MultiPart()
  Future<BaseResponse> addTransport({
    @Part(name: "fournisseurId") required int fournisseurId,
    @Part(name: "vehicleId") required int vehicleId,
    @Part(name: "categorieVehicleId") required int categorieVehicleId,
    @Part(name: "price") required double price,
    @Part(name: "capacite") required int capacite,
    @Part(name: "unitOfMeasurment") required String unitOfMeasurment,
    @Part(name: "availability") required String availability,
    @Part(name: "matriculeVehicle") required String matriculeVehicle,
    @Part(name: "availabilityDate") String? availabilityDate,
    @Part(name: "hour") String? hour,
    @Part(name: "expirationDate") required String expirationDate,
    @Part(name: "description") String? description,
    @Part(name: "images") List<File>? images,
    @Part(name: "start") required Map<String, dynamic> locationStart,
    @Part(name: "arrival") required Map<String, dynamic> locationEnd,
    @Part(name: "location") Map<String, dynamic>? location,
    @Part(name: "paymentId") required int paymentId,
    @Part(name: "numberSlice") int? numberSlice,
    @Part(name: "unitTime") String? unitTime,
  });

  @PUT(
      "${Constant.baseUrl}${Constant.urlMsCatalogs}/transports/update/products/{id}")
  Future<BaseResponse> updateTransport({
    @Path("id") required int idProduct,
    @Field("fournisseurId") required int fournisseurId,
    @Field("price") double? price,
    @Field("capacite") int? capacite,
    @Field("unitOfMeasurment") String? unitOfMeasurment,
    @Field("availability") String? availability,
    @Field("matriculeVehicle") String? matriculeVehicle,
    @Field("availabilityDate") String? availabilityDate,
    @Field("hour") String? hour,
    @Field("expirationDate") String? expirationDate,
    @Field("description") String? description,
    @Field("start") Map<String, dynamic>? locationStart,
    @Field("arrival") Map<String, dynamic>? locationEnd,
    @Field("location") Map<String, dynamic>? location,
    @Field("paymentId") int? paymentId,
  });

  @POST("${Constant.baseUrl}${Constant.urlMsCatalogs}/transports/reservations")
  Future<BaseResponse> reserveTransport(
    @Field("offerId") int offerId,
    @Field("capacity") int capacity,
    @Field("userId") int userId,
    @Field("unitOfMeasurment") String unitOfMeasurment,
    @Field("goods") String marchandise,
    @Field("description") String? description,
    @Field("date") String date,
    @Field("hour") String hour,
  );

  @GET(
      "${Constant.baseUrl}${Constant.urlMsCatalogs}/transports/reservations/fournisseur/{fournisseurId}")
  Future<GetDemandeTransport> getListDemandeTransportRecu(
    @Path("fournisseurId") int fournisseurId,
  );

  @GET(
      "${Constant.baseUrl}${Constant.urlMsCatalogs}/transports/reservations/user/{userId}")
  Future<GetDemandeTransport> getListDemandeTransportSend(
    @Path("userId") int userId,
  );

  @PUT(
      "${Constant.baseUrl}${Constant.urlMsCatalogs}/transports/reservations/valid/{fournisseurId}/{reservationId}")
  Future<BaseResponse> validDemandeTransport(
    @Path("fournisseurId") int fournisseurId,
    @Path("reservationId") int reservationId,
  );

  @PUT(
      "${Constant.baseUrl}${Constant.urlMsCatalogs}/transports/reservations/cancel/{reservationId}")
  Future<BaseResponse> cancelDemande(
    @Path("reservationId") int reservationId,
  );

  @PUT(
      "${Constant.baseUrl}${Constant.urlMsCatalogs}/transports/reservations/mission/{reservationId}/{personal}")
  Future<BaseResponse> validChargment(
    @Path("reservationId") int reservationId,
    @Path("personal") String personal,
    @Field("code") String code,
  );

  @GET(
      "${Constant.baseUrl}${Constant.urlMsCatalogs}/transports/category-vehicle/all")
  Future<GetCategoryVehicule> getCategorieVehicule();

  @POST("${Constant.baseUrl}${Constant.urltMsPaiement}/cash/accept-request")
  Future<BaseResponse> acceptPaiementCash(
    @Query("orderId") int orderId,
    @Query("productId") int productId,
    @Field("response") bool response,
    @Field("message") String? message,
  );

  @POST("${Constant.baseUrl}${Constant.urltMsPaiement}/cash/confirm-request")
  Future<BaseResponse> confirmPaiementCash(
    @Field("demandeId") int demandeId,
    @Field("confirmationCode") String confirmationCode,
  );

  @GET("${Constant.baseUrl}${Constant.urltMsPaiement}/cash/my-request")
  Future<GetListDemandePaiementCashItem> getListDemandePaimentCash(
      @Query("role") String role);

  @PUT(
      "${Constant.baseUrl}${Constant.urlMsPrestation}/purchase-service/service-request/accept/{userId}/{offerId}")
  Future<BaseResponse> validContractPdf(
    @Path("userId") int userId,
    @Path("offerId") int offerId,
    @Query("agreement") bool agreement,
  );

  @PUT(
      "${Constant.baseUrl}${Constant.urlMsCatalogs}/intrants/products/update/product/{id}")
  @MultiPart()
  Future<GetDetailProductResponse> updateProductIntrant(
    @Path() int id,
    @Part(name: "productId") int productId,
    @Part(name: "fournisseurId") int? fournisseurId,
    @Part(name: "price_u") int? price,
    @Part(name: "quantity") int? quantity,
    @Part(name: "typeVente") String? typeVente,
    @Part(name: "availability") String? availability,
    @Part(name: "description") String? description,
    @Part(name: "unitOfMeasurment") String? unitOfMeasurment,
    @Part(name: "offerType") String? offerType,
    @Part(name: "availabilityDate") String? availabilityDate,
    //@Part( name:"images") List<File>? images,
    @Part(name: "locationId") int? locationId,
    @Part(name: "regionId") int? regionId,
    @Part(name: "departementId") int? departementId,
    @Part(name: "sprefectureId") int? sousPrefectureId,
  );

  @GET(
      "${Constant.baseUrl}${Constant.urlMsCatalogs}/intrants/category-intrant/all")
  Future<GetIntrantCategoryResponse> getIntrantCategoris();

  @GET(
      "${Constant.baseUrl}${Constant.urlMsCatalogs}/intrants/unit-of-measurment/get")
  Future<GetUniteResponse> getIntrantUnite();

  @DELETE(
      "${Constant.baseUrl}${Constant.urlMsSecurity}/registration/push/deleteRegistrationId/{clientId}/{registrationId}")
  Future<BaseResponse> deleteFirebaseToken(
    @Path("clientId") String userName,
    @Path("registrationId") String registrationId,
  );

  @GET("${Constant.baseUrl}${Constant.urlMsAssuranceCredit}/insurance/get")
  Future<OffreInsuranceModel> getInsurance(
    @Query("page") int page,
    @Query("per_page") int perPage,
    @Query("sort_by") String sortBy,
    @Query("businessLineSlug") String? businessLineSlug,
    @Query("InsurerId") int? insurerId,
    @Query("typeInsuranceId") int? typeInsuranceId,
    @Query("insuranceId") int? insuranceId,
    @Query("insurerName") String? insurerName,
    @Query("typeInsuranceName") String? typeInsuranceName,
    @Query("insuranceName") String? insuranceName,
    @Query("priceMin") double? priceMin,
    @Query("priceMax") double? priceMax,
  );

  @GET(
      "${Constant.baseUrl}${Constant.urlMsAssuranceCredit}/insurance/get/insurer/{id}")
  Future<InsuranceByInsurerModel> getInsuranceByInsurer(
      @Path("id") int insurerId);

  @GET("${Constant.baseUrl}${Constant.urlMsAssuranceCredit}/credit/get")
  Future<OffreCreditModel> getCredit(
    @Query("page") int page,
    @Query("per_page") int perPage,
    @Query("sort_by") String sortBy,
    @Query("businessLineSlug") String? businessLineSlug,
    @Query("creditorId") int? creditorId,
    @Query("typeCreditId") int? typeCreditId,
    @Query("creditId") int? creditId,
    @Query("creditorName") String? creditorName,
    @Query("typeCreditName") String? typeCreditName,
    @Query("creditName") String? creditName,
    @Query("priceMin") double? priceMin,
    @Query("priceMax") double? priceMax,
  );

  // @GET(
  //     "${Constant.localBaseUrl}${Constant.portMsInsurance}${Constant.urlMsInsurance}/get/insurer/{id}")
  // Future<InsuranceByInsurerModel> getCreditByInsurer(
  //     @Path("id") int insurerId);
  @GET(
      "${Constant.baseUrl}${Constant.urlMsAssuranceCredit}/insurance/type-insurance/get")
  Future<TypeOffreInsuranceModel> getTypeInsurance();

  @GET(
      "${Constant.baseUrl}${Constant.urlMsAssuranceCredit}/credit/type-credit/get")
  Future<TypeOffreCreditModel> getTypeCredit();

  @GET(
      "${Constant.baseUrl}${Constant.urlMsAssuranceCredit}/credit/get/creditor/{id}")
  Future<OffreCreditModel> getCreditByCreditor(@Path("id") int creditorId);

  @GET(
      "${Constant.baseUrl}${Constant.urlMsAssuranceCredit}/credit/creditor/get")
  Future<CreditorModel> getCreditor();

  @GET(
      "${Constant.baseUrl}${Constant.urlMsAssuranceCredit}/insurance/insurer/get")
  Future<InsurerModel> getInsurer();

  //     @GET(
  //     "${Constant.baseUrl1}${Constant.portMsMicroCreditInsurance}/config/get/entreprise/get/business-line")
  // Future<InsurerModel> getBusinessLine();

  @DELETE("${Constant.baseUrl}${Constant.urlMsCatalogs}/image/delete/{id}")
  Future<BaseResponse> deleteImage(
    @Path("id") int imageId,
  );

  @PUT("${Constant.baseUrl}${Constant.urlMsCatalogs}/image/{offerId}")
  @MultiPart()
  Future<GetDetailProductResponse> addOtherImage(
    @Path("offerId") int productId,
    @Part(name: "images") List<File>? images,
  );

  //     @GET(
  //     "${Constant.baseUrl1}${Constant.portMsMicroCreditInsurance}/config/get/entreprise/get/business-line")
  // Future<InsurerModel> getBusinessLine();

  @GET(
      "${Constant.baseUrl}${Constant.urlMsAssuranceCredit}/insurance/insurer/get/{id}")
  Future<GetEntrepriseResponseModel> getEntreprise(
    @Path("id") int entrepriseId,
  );

  @GET(
      "${Constant.baseUrl}${Constant.urlMsAssuranceCredit}/credit/creditor/get/{id}")
  Future<GetEntrepriseResponseModel> getEntrepriseCredit(
    @Path("id") int entrepriseId,
  );

  @POST("${Constant.baseUrl}/broker/publish/emailt")
  Future<String?> sendNotif(
    @Field("resourceIds") String resourceIds,
    @Field("subject") String subject,
    @Field("receivers") List<String> receivers,
    @Field("title") String title,
    @Field("message") String message,
    @Field("messageDate") String messageDate,
  );

  @POST(
      "${Constant.baseUrl}${Constant.urlMsSecurity}/account-manage/updatePassword/{id}")
  Future<BaseResponse> updatePassword(
    @Path("id") int id,
    @Field("login") String login,
    @Field("password") String password,
    @Field("oldPassword") String oldPassword,
  );

  @GET("${Constant.baseUrl}/middleware/order/fournisseur/{fournisseurId}")
  Future<MesProduitsCommanderResponse> getMyProductsCommanded(
    @Path("fournisseurId") int fournisseurId,
    @Query("page") int page,
    @Query("perPage") int perPage,
    @Query("identifiant") String? identifiant,
    @Query("sortBy") String? sortBy,
  );

  @POST(
      "${Constant.baseUrl}${Constant.urlMsOrder}/reduction/{commandId}/{productId}/{priceReduction}")
  Future<BaseResponse> reductionPrice(
    @Path("commandId") int commandId,
    @Path("productId") int productId,
    @Path("priceReduction") double priceReduction,
  );
  @PUT(
      "${Constant.baseUrl}${Constant.urlMsSecurity}/account-manage/updateUser/{id}")
  Future<BaseResponse> updateUserInfo(
    @Path("id") int id,
    @Field("username") String? username,
    @Field("firstname") String? firstname,
    @Field("lastname") String? lastname,
    @Field("gender") String? gender,
    @Field("birthdate") String? birthdate,
    @Field("phone") String? phone,
    @Field("adress") String? adress,
    @Field("email") String? email,
    @Field("photo") String? photo,
    @Field("coordonates") String? coordonates,
    @Field("localisation") String? localisation,
    @Field("principalSpeculation") String? principalSpeculation,
    @Field("notation") int? notation,
  );
  @POST(
      "${Constant.baseUrl}${Constant.urlMsSecurity}/account-manage/reinit/{phoneNumber}")
  Future<BaseResponse> sendOtpResetPassword(
    @Path("phoneNumber") String phoneNumber,
  );
  @POST(
      "${Constant.baseUrl}${Constant.urlMsSecurity}/account-manage/resetPassword/{phoneNumber}")
  Future<BaseResponse> resetPassword(
    @Path("phoneNumber") String phoneNumber,
    @Field("password") String? password,
    @Field("token") String? token,
  );
 @GET("${Constant.baseUrl}/payment/credit/my-transactions")
  Future<GetProductOderedInCreditResponse> getProductOderedInCredit(
   @Query("role") String role,
  );
 
 @GET("${Constant.baseUrl}/payment/credit/unit-time")
  Future<GetListUnitTimeResponse> getUnitTime(

  );

    @POST("${Constant.baseUrl}${Constant.urltMsPaiement}/single-create")
  Future<CreatePaiementResponse> createPaiementCredit(
    @Field("orderId") int orderId,
    @Field("reference") String reference,
    @Field("phone") String? phone,
    @Field("motif") String? motif,

  
  );
 
 @POST("${Constant.baseUrl}${Constant.urlMsCatalogs}/prePurchaseRequest/add")
  Future<BaseResponse> demandePreAchat(
    @Field("fournisseurId") int fournisseurId, 
    @Field("offerId") int offerId,
  );


  @GET("${Constant.baseUrl}${Constant.urlMsCatalogs}/prePurchaseRequest/customer/{id}/{customer}/{type}")
  Future<GetDemandeAchatOrPreAchat> getDemandeAchatOrPreAchat(
      @Path("id") int id, @Path("customer") String customer, @Path("type") String type,
  );

 
  @PUT("${Constant.baseUrl}${Constant.urlMsCatalogs}/prePurchaseRequest/state/{id}/{state}")
  Future<BaseResponse> validedOrInvalidedDemandePreAchat(
    @Path("id") int id,
    @Path("state") bool state,
  );


 
  @GET("${Constant.baseUrl}${Constant.urlMsCatalogs}//stats-other-offer/{fournisseurId}")
  Future<GetUserStatResponse> getUserStat(
    @Path("fournisseurId") int id,
    
  );
}
