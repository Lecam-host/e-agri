// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HistoryWeatherInfo _$HistoryWeatherInfoFromJson(Map<String, dynamic> json) =>
    HistoryWeatherInfo()
      ..cityName = json['cityName'] as String?
      ..dt = (json['dt'] as num?)?.toInt()
      ..windDirection = (json['windDirection'] as num?)?.toInt()
      ..tempDay = (json['tempDay'] as num?)?.toDouble()
      ..tempMin = (json['tempMin'] as num?)?.toDouble()
      ..tempMax = (json['tempMax'] as num?)?.toDouble()
      ..dayName = json['dayName'] as String?
      ..date = json['date'] as String?
      ..hour = json['hour'] as String?
      ..dewPoint = (json['dewPoint'] as num?)?.toDouble()
      ..humidity = (json['humidity'] as num?)?.toInt()
      ..pressure = (json['pressure'] as num?)?.toInt()
      ..description = json['description'] as String?
      ..iconLink = json['icon'] as String?
      ..precipitation = (json['precipitation'] as num?)?.toDouble()
      ..precipitationProbability =
          (json['probprecipitation'] as num?)?.toDouble()
      ..clouds = (json['clouds'] as num?)?.toInt()
      ..windSpeed = (json['windSpeed'] as num?)?.toDouble()
      ..windDeg = (json['windDeg'] as num?)?.toDouble()
      ..windGust = (json['windGust'] as num?)?.toDouble()
      ..rain = (json['rain'] as num?)?.toDouble()
      ..pop = (json['pop'] as num?)?.toDouble();

Map<String, dynamic> _$HistoryWeatherInfoToJson(HistoryWeatherInfo instance) =>
    <String, dynamic>{
      'cityName': instance.cityName,
      'dt': instance.dt,
      'windDirection': instance.windDirection,
      'tempDay': instance.tempDay,
      'tempMin': instance.tempMin,
      'tempMax': instance.tempMax,
      'dayName': instance.dayName,
      'date': instance.date,
      'hour': instance.hour,
      'dewPoint': instance.dewPoint,
      'humidity': instance.humidity,
      'pressure': instance.pressure,
      'description': instance.description,
      'icon': instance.iconLink,
      'precipitation': instance.precipitation,
      'probprecipitation': instance.precipitationProbability,
      'clouds': instance.clouds,
      'windSpeed': instance.windSpeed,
      'windDeg': instance.windDeg,
      'windGust': instance.windGust,
      'rain': instance.rain,
      'pop': instance.pop,
    };

ListHistoryWeatherInfoResponse _$ListHistoryWeatherInfoResponseFromJson(
        Map<String, dynamic> json) =>
    ListHistoryWeatherInfoResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..listHistory = (json['data'] as List<dynamic>?)
          ?.map((e) => HistoryWeatherInfo.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$ListHistoryWeatherInfoResponseToJson(
        ListHistoryWeatherInfoResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.listHistory,
    };

AddParcelInfoResponse _$AddParcelInfoResponseFromJson(
        Map<String, dynamic> json) =>
    AddParcelInfoResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..parcelInfo = json['data'] == null
          ? null
          : ParcelInfoResponse.fromJson(json['data'] as Map<String, dynamic>);

Map<String, dynamic> _$AddParcelInfoResponseToJson(
        AddParcelInfoResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.parcelInfo,
    };

ParcelInfoResponse _$ParcelInfoResponseFromJson(Map<String, dynamic> json) =>
    ParcelInfoResponse()
      ..id = (json['id'] as num?)?.toInt()
      ..parcelId = json['parcelId'] as String?
      ..dt = (json['dt'] as num?)?.toInt()
      ..t10 = (json['t10'] as num?)?.toDouble()
      ..moisture = (json['moisture'] as num?)?.toDouble()
      ..t0 = (json['t0'] as num?)?.toDouble()
      ..centerLat = (json['centerLat'] as num?)?.toDouble()
      ..centerLon = (json['centerLon'] as num?)?.toDouble()
      ..name = json['name'] as String?
      ..createdAt = json['createdAt'] as String?
      ..coordinates = json['coordinates'] as String?;

Map<String, dynamic> _$ParcelInfoResponseToJson(ParcelInfoResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'parcelId': instance.parcelId,
      'dt': instance.dt,
      't10': instance.t10,
      'moisture': instance.moisture,
      't0': instance.t0,
      'centerLat': instance.centerLat,
      'centerLon': instance.centerLon,
      'name': instance.name,
      'createdAt': instance.createdAt,
      'coordinates': instance.coordinates,
    };

WeatherResponse _$WeatherResponseFromJson(Map<String, dynamic> json) =>
    WeatherResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..weather = json['data'] == null
          ? null
          : LocationWeatherResponse.fromJson(
              json['data'] as Map<String, dynamic>);

Map<String, dynamic> _$WeatherResponseToJson(WeatherResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.weather,
    };

LocationWeatherResponse _$LocationWeatherResponseFromJson(
        Map<String, dynamic> json) =>
    LocationWeatherResponse()
      ..listDayWeather = (json['forecast'] as List<dynamic>?)
          ?.map(
              (e) => DayWeatherDataResponse.fromJson(e as Map<String, dynamic>))
          .toList()
      ..cityName = json['placeName'] as String?;

Map<String, dynamic> _$LocationWeatherResponseToJson(
        LocationWeatherResponse instance) =>
    <String, dynamic>{
      'forecast': instance.listDayWeather,
      'placeName': instance.cityName,
    };

HoulyWeatherDataResponse _$HoulyWeatherDataResponseFromJson(
        Map<String, dynamic> json) =>
    HoulyWeatherDataResponse()
      ..hour = json['hour'] as String?
      ..temp = (json['temp'] as num?)?.toDouble()
      ..dewPoint = (json['dewPoint'] as num?)?.toInt()
      ..humidity = (json['humidity'] as num?)?.toInt()
      ..pressure = (json['pressure'] as num?)?.toInt()
      ..description = json['description'] as String?
      ..iconLink = json['icon'] as String?
      ..precipitation = (json['precipitation'] as num?)?.toDouble()
      ..precipitationProbability =
          (json['probprecipitation'] as num?)?.toDouble()
      ..clouds = (json['clouds'] as num?)?.toInt()
      ..windSpeed = (json['windSpeed'] as num?)?.toInt()
      ..windDeg = (json['windDeg'] as num?)?.toInt()
      ..windGust = (json['windGust'] as num?)?.toInt();

Map<String, dynamic> _$HoulyWeatherDataResponseToJson(
        HoulyWeatherDataResponse instance) =>
    <String, dynamic>{
      'hour': instance.hour,
      'temp': instance.temp,
      'dewPoint': instance.dewPoint,
      'humidity': instance.humidity,
      'pressure': instance.pressure,
      'description': instance.description,
      'icon': instance.iconLink,
      'precipitation': instance.precipitation,
      'probprecipitation': instance.precipitationProbability,
      'clouds': instance.clouds,
      'windSpeed': instance.windSpeed,
      'windDeg': instance.windDeg,
      'windGust': instance.windGust,
    };

DayWeatherDataResponse _$DayWeatherDataResponseFromJson(
        Map<String, dynamic> json) =>
    DayWeatherDataResponse()
      ..hour = json['hour'] as String?
      ..temp = (json['temp'] as num?)?.toDouble()
      ..dewPoint = (json['dewPoint'] as num?)?.toInt()
      ..humidity = (json['humidity'] as num?)?.toInt()
      ..pressure = (json['pressure'] as num?)?.toInt()
      ..description = json['description'] as String?
      ..iconLink = json['icon'] as String?
      ..precipitation = (json['precipitation'] as num?)?.toDouble()
      ..precipitationProbability =
          (json['probprecipitation'] as num?)?.toDouble()
      ..clouds = (json['clouds'] as num?)?.toInt()
      ..windSpeed = (json['windSpeed'] as num?)?.toInt()
      ..windDeg = (json['windDeg'] as num?)?.toInt()
      ..windGust = (json['windGust'] as num?)?.toInt()
      ..windDirection = (json['windDirection'] as num?)?.toInt()
      ..tempDay = (json['tempDay'] as num?)?.toDouble()
      ..tempMin = (json['tempMin'] as num?)?.toDouble()
      ..tempMax = (json['tempMax'] as num?)?.toDouble()
      ..dayName = json['daysName'] as String?
      ..date = json['date'] as String?
      ..houlyWeather = (json['houly'] as List<dynamic>?)
          ?.map((e) =>
              HoulyWeatherDataResponse.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$DayWeatherDataResponseToJson(
        DayWeatherDataResponse instance) =>
    <String, dynamic>{
      'hour': instance.hour,
      'temp': instance.temp,
      'dewPoint': instance.dewPoint,
      'humidity': instance.humidity,
      'pressure': instance.pressure,
      'description': instance.description,
      'icon': instance.iconLink,
      'precipitation': instance.precipitation,
      'probprecipitation': instance.precipitationProbability,
      'clouds': instance.clouds,
      'windSpeed': instance.windSpeed,
      'windDeg': instance.windDeg,
      'windGust': instance.windGust,
      'windDirection': instance.windDirection,
      'tempDay': instance.tempDay,
      'tempMin': instance.tempMin,
      'tempMax': instance.tempMax,
      'daysName': instance.dayName,
      'date': instance.date,
      'houly': instance.houlyWeather,
    };

ListParcelResponse _$ListParcelResponseFromJson(Map<String, dynamic> json) =>
    ListParcelResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..listParcel = (json['data'] as List<dynamic>?)
          ?.map((e) => ParcelInfoResponse.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$ListParcelResponseToJson(ListParcelResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.listParcel,
    };

CreateFullParcelResponse _$CreateFullParcelResponseFromJson(
        Map<String, dynamic> json) =>
    CreateFullParcelResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..listParcel = json['data'] == null
          ? null
          : CreateFullParcelDataResponse.fromJson(
              json['data'] as Map<String, dynamic>);

Map<String, dynamic> _$CreateFullParcelResponseToJson(
        CreateFullParcelResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.listParcel,
    };

CreateFullParcelDataResponse _$CreateFullParcelDataResponseFromJson(
        Map<String, dynamic> json) =>
    CreateFullParcelDataResponse()
      ..listCreated = (json['created'] as List<dynamic>?)
          ?.map((e) => ParcelInfoResponse.fromJson(e as Map<String, dynamic>))
          .toList()
      ..listAlreadyExists = (json['alreadyExists'] as List<dynamic>?)
          ?.map((e) => ParcelInfoResponse.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$CreateFullParcelDataResponseToJson(
        CreateFullParcelDataResponse instance) =>
    <String, dynamic>{
      'created': instance.listCreated,
      'alreadyExists': instance.listAlreadyExists,
    };
