// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shop_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddProductOnMarketResponse _$AddProductOnMarketResponseFromJson(
        Map<String, dynamic> json) =>
    AddProductOnMarketResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?;

Map<String, dynamic> _$AddProductOnMarketResponseToJson(
        AddProductOnMarketResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
    };

ProductResponse _$ProductResponseFromJson(Map<String, dynamic> json) =>
    ProductResponse(
      (json['id'] as num).toInt(),
      ProductElementResponse.fromJson(json['product'] as Map<String, dynamic>),
      json['publicationDate'] as String,
      json['description'] as String?,
      json['availability'] as String,
      json['status'] as String,
      (json['paymentId'] as num).toInt(),
      (json['fournisseurId'] as num).toInt(),
      json['offerType'] as String,
      json['typeVente'] as String?,
      (json['notation'] as num).toDouble(),
      json['code'] as String,
      json['paymentMethod'] == null
          ? null
          : PaymentMethodModel.fromJson(
              json['paymentMethod'] as Map<String, dynamic>),
      (json['locationId'] as num?)?.toInt(),
    )
      ..location = json['location'] == null
          ? null
          : LocationProductResponse.fromJson(
              json['location'] as Map<String, dynamic>)
      ..availabilityDate = json['availabilityDate'] as String?
      ..images = (json['images'] as List<dynamic>?)
          ?.map((e) => ImageProduct.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$ProductResponseToJson(ProductResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'location': instance.location,
      'locationId': instance.locationId,
      'product': instance.product,
      'publicationDate': instance.publicationDate,
      'code': instance.code,
      'description': instance.description,
      'availability': instance.availability,
      'availabilityDate': instance.availabilityDate,
      'status': instance.status,
      'paymentId': instance.paymentId,
      'fournisseurId': instance.fournisseurId,
      'typeVente': instance.typeVente,
      'offerType': instance.typeOffer,
      'notation': instance.rate,
      'images': instance.images,
      'paymentMethod': instance.paymentMethod,
    };

ListProductResponse _$ListProductResponseFromJson(Map<String, dynamic> json) =>
    ListProductResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..listProduct = (json['data'] as List<dynamic>?)
          ?.map((e) => ProductResponse.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$ListProductResponseToJson(
        ListProductResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.listProduct,
    };

GetDetailProductResponse _$GetDetailProductResponseFromJson(
        Map<String, dynamic> json) =>
    GetDetailProductResponse(
      json['data'] as Map<String, dynamic>,
    )
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?;

Map<String, dynamic> _$GetDetailProductResponseToJson(
        GetDetailProductResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.data,
    };

GetListProductResponse _$GetListProductResponseFromJson(
        Map<String, dynamic> json) =>
    GetListProductResponse(
      id: (json['id'] as num).toInt(),
      fournisseurId: (json['fournisseurId'] as num).toInt(),
    )
      ..createdAt = json['created_at'] as String?
      ..listProduct = (json['offers'] as List<dynamic>?)
          ?.map((e) => e as Map<String, dynamic>)
          .toList();

Map<String, dynamic> _$GetListProductResponseToJson(
        GetListProductResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'fournisseurId': instance.fournisseurId,
      'created_at': instance.createdAt,
      'offers': instance.listProduct,
    };

BodyGetListProductResponse _$BodyGetListProductResponseFromJson(
        Map<String, dynamic> json) =>
    BodyGetListProductResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..data = json['data'] == null
          ? null
          : GetAllProductResponse.fromJson(
              json['data'] as Map<String, dynamic>);

Map<String, dynamic> _$BodyGetListProductResponseToJson(
        BodyGetListProductResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.data,
    };

GetAllProductResponse _$GetAllProductResponseFromJson(
        Map<String, dynamic> json) =>
    GetAllProductResponse()
      ..totalObject = (json['total'] as num?)?.toInt()
      ..currentPage = (json['current_page'] as num?)?.toInt()
      ..totalOfPages = (json['total_of_pages'] as num?)?.toInt()
      ..nextPage = (json['next_page'] as num?)?.toInt()
      ..previousPage = (json['previous_page'] as num?)?.toInt()
      ..perPage = (json['per_page'] as num?)?.toInt()
      ..data = (json['items'] as List<dynamic>?)
          ?.map((e) => e as Map<String, dynamic>)
          .toList();

Map<String, dynamic> _$GetAllProductResponseToJson(
        GetAllProductResponse instance) =>
    <String, dynamic>{
      'total': instance.totalObject,
      'current_page': instance.currentPage,
      'total_of_pages': instance.totalOfPages,
      'next_page': instance.nextPage,
      'previous_page': instance.previousPage,
      'per_page': instance.perPage,
      'items': instance.data,
    };

DataProductUserResponse _$DataProductUserResponseFromJson(
        Map<String, dynamic> json) =>
    DataProductUserResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..data = json['data'] == null
          ? null
          : GetListProductResponse.fromJson(
              json['data'] as Map<String, dynamic>);

Map<String, dynamic> _$DataProductUserResponseToJson(
        DataProductUserResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.data,
    };

LocationProductResponse _$LocationProductResponseFromJson(
        Map<String, dynamic> json) =>
    LocationProductResponse(
      (json['regionId'] as num).toInt(),
    )
      ..locationId = (json['locationId'] as num?)?.toInt()
      ..departementId = (json['departementId'] as num?)?.toInt()
      ..sousprefectureId = (json['sprefectureId'] as num?)?.toInt()
      ..localiteId = (json['localiteId'] as num?)?.toInt();

Map<String, dynamic> _$LocationProductResponseToJson(
        LocationProductResponse instance) =>
    <String, dynamic>{
      'locationId': instance.locationId,
      'regionId': instance.regionId,
      'departementId': instance.departementId,
      'sprefectureId': instance.sousprefectureId,
      'localiteId': instance.localiteId,
    };

ProductElementResponse _$ProductElementResponseFromJson(
        Map<String, dynamic> json) =>
    ProductElementResponse(
      (json['id'] as num).toInt(),
      (json['quantity'] as num).toInt(),
      (json['price_u'] as num).toDouble(),
      json['unitOfMeasurment'] as String?,
      json['speculation'] == null
          ? null
          : SpeculationResponse.fromJson(
              json['speculation'] as Map<String, dynamic>),
    )
      ..speculationId = (json['speculationId'] as num?)?.toInt()
      ..pesticideId = (json['pesticideId'] as num?)?.toInt();

Map<String, dynamic> _$ProductElementResponseToJson(
        ProductElementResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'speculationId': instance.speculationId,
      'pesticideId': instance.pesticideId,
      'quantity': instance.quantity,
      'price_u': instance.price,
      'unitOfMeasurment': instance.unitOfMeasurment,
      'speculation': instance.speculation,
    };

AddProductInShoppingCartResponse _$AddProductInShoppingCartResponseFromJson(
        Map<String, dynamic> json) =>
    AddProductInShoppingCartResponse(
      DataProductInShoppingCartResponse.fromJson(
          json['data'] as Map<String, dynamic>),
    )
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?;

Map<String, dynamic> _$AddProductInShoppingCartResponseToJson(
        AddProductInShoppingCartResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.data,
    };

DataProductInShoppingCartResponse _$DataProductInShoppingCartResponseFromJson(
        Map<String, dynamic> json) =>
    DataProductInShoppingCartResponse(
      (json['id'] as num).toInt(),
      (json['cartItems'] as List<dynamic>)
          .map((e) =>
              ProductShoppingCartsItem.fromJson(e as Map<String, dynamic>))
          .toList(),
      (json['userId'] as num).toInt(),
    );

Map<String, dynamic> _$DataProductInShoppingCartResponseToJson(
        DataProductInShoppingCartResponse instance) =>
    <String, dynamic>{
      'cartItems': instance.cartItems,
      'id': instance.id,
      'userId': instance.userId,
    };

GetCartResponse _$GetCartResponseFromJson(Map<String, dynamic> json) =>
    GetCartResponse(
      (json['id'] as num).toInt(),
      (json['data'] as List<dynamic>)
          .map((e) =>
              ProductShoppingCartsItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    )
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?;

Map<String, dynamic> _$GetCartResponseToJson(GetCartResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.cartItems,
      'id': instance.id,
    };

GetCartResponseBody _$GetCartResponseBodyFromJson(Map<String, dynamic> json) =>
    GetCartResponseBody(
      (json['data'] as List<dynamic>)
          .map((e) => CartResponseBody.fromJson(e as Map<String, dynamic>))
          .toList(),
    )
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?;

Map<String, dynamic> _$GetCartResponseBodyToJson(
        GetCartResponseBody instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.listCart,
    };

CartResponseBody _$CartResponseBodyFromJson(Map<String, dynamic> json) =>
    CartResponseBody(
      (json['id'] as num).toInt(),
      (json['cartItems'] as List<dynamic>)
          .map((e) => CartItemReponse.fromJson(e as Map<String, dynamic>))
          .toList(),
      json['createdAt'] as String,
      json['status'] as String,
      (json['totalPrice'] as num).toDouble(),
      (json['userId'] as num).toInt(),
    );

Map<String, dynamic> _$CartResponseBodyToJson(CartResponseBody instance) =>
    <String, dynamic>{
      'cartItems': instance.cartItems,
      'id': instance.id,
      'userId': instance.userId,
      'createdAt': instance.createdAt,
      'status': instance.status,
      'totalPrice': instance.totalPrice,
    };

CartItemReponse _$CartItemReponseFromJson(Map<String, dynamic> json) =>
    CartItemReponse(
      (json['id'] as num).toInt(),
      (json['idProduct'] as num).toInt(),
      (json['quantity'] as num).toInt(),
      (json['price'] as num).toDouble(),
      (json['fournisseurId'] as num).toInt(),
    );

Map<String, dynamic> _$CartItemReponseToJson(CartItemReponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'idProduct': instance.idProduct,
      'quantity': instance.quantity,
      'price': instance.price,
      'fournisseurId': instance.fournisseurId,
    };
