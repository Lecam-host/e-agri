import 'package:eagri/data/responses/responses.dart';
import 'package:json_annotation/json_annotation.dart';

part 'weather_response.g.dart';

@JsonSerializable()
class HistoryWeatherInfo {
  HistoryWeatherInfo();

  factory HistoryWeatherInfo.fromJson(Map<String, dynamic> json) =>
      _$HistoryWeatherInfoFromJson(json);

  @JsonKey(name: "cityName")
  String? cityName;

  @JsonKey(name: "dt")
  int? dt;
  @JsonKey(name: "windDirection")
  int? windDirection;
  @JsonKey(name: "tempDay")
  double? tempDay;
  @JsonKey(name: "tempMin")
  double? tempMin;
  @JsonKey(name: "tempMax")
  double? tempMax;
  @JsonKey(name: "dayName")
  String? dayName;

  @JsonKey(name: "date")
  String? date;

  @JsonKey(name: "hour")
  String? hour;
  @JsonKey(name: "dewPoint")
  double? dewPoint;
  @JsonKey(name: "humidity")
  int? humidity;
  @JsonKey(name: "pressure")
  int? pressure;
  @JsonKey(name: "description")
  String? description;
  @JsonKey(name: "icon")
  String? iconLink;
  @JsonKey(name: "precipitation")
  double? precipitation;
  @JsonKey(name: "probprecipitation")
  double? precipitationProbability;
  @JsonKey(name: "clouds")
  int? clouds;
  @JsonKey(name: "windSpeed")
  double? windSpeed;
  @JsonKey(name: "windDeg")
  double? windDeg;
  @JsonKey(name: "windGust")
  double? windGust;

  @JsonKey(name: "rain")
  double? rain;

  @JsonKey(name: "pop")
  double? pop;

  Map<String, dynamic> toJson() => _$HistoryWeatherInfoToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class ListHistoryWeatherInfoResponse extends BaseResponse {
  ListHistoryWeatherInfoResponse();

  factory ListHistoryWeatherInfoResponse.fromJson(Map<String, dynamic> json) =>
      _$ListHistoryWeatherInfoResponseFromJson(json);
  @JsonKey(name: "data")
  List<HistoryWeatherInfo>? listHistory;

  @override
  Map<String, dynamic> toJson() => _$ListHistoryWeatherInfoResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class AddParcelInfoResponse extends BaseResponse {
  AddParcelInfoResponse();

  factory AddParcelInfoResponse.fromJson(Map<String, dynamic> json) =>
      _$AddParcelInfoResponseFromJson(json);
  @JsonKey(name: "data")
  ParcelInfoResponse? parcelInfo;

  @override
  Map<String, dynamic> toJson() => _$AddParcelInfoResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class ParcelInfoResponse {
  ParcelInfoResponse();

  factory ParcelInfoResponse.fromJson(Map<String, dynamic> json) =>
      _$ParcelInfoResponseFromJson(json);

  @JsonKey(name: "id")
  int? id;
  @JsonKey(name: "parcelId")
  String? parcelId;
  @JsonKey(name: "dt")
  int? dt;
  @JsonKey(name: "t10")
  double? t10;
  @JsonKey(name: "moisture")
  double? moisture;
  @JsonKey(name: "t0")
  double? t0;
  @JsonKey(name: "centerLat")
  double? centerLat;
  @JsonKey(name: "centerLon")
  double? centerLon;
  @JsonKey(name: "name")
  String? name;
  @JsonKey(name: "createdAt")
  String? createdAt;

  @JsonKey(name: "coordinates")
  String? coordinates;
  Map<String, dynamic> toJson() => _$ParcelInfoResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class WeatherResponse extends BaseResponse {
  WeatherResponse();
  factory WeatherResponse.fromJson(Map<String, dynamic> json) =>
      _$WeatherResponseFromJson(json);

  @JsonKey(name: "data")
  LocationWeatherResponse? weather;
  @override
  Map<String, dynamic> toJson() => _$WeatherResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class LocationWeatherResponse {
  LocationWeatherResponse();
  factory LocationWeatherResponse.fromJson(Map<String, dynamic> json) =>
      _$LocationWeatherResponseFromJson(json);

  @JsonKey(name: "forecast")
  List<DayWeatherDataResponse>? listDayWeather;

  @JsonKey(name: "placeName")
  String? cityName;
  Map<String, dynamic> toJson() => _$LocationWeatherResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class HoulyWeatherDataResponse {
  HoulyWeatherDataResponse();
  factory HoulyWeatherDataResponse.fromJson(Map<String, dynamic> json) =>
      _$HoulyWeatherDataResponseFromJson(json);

  @JsonKey(name: "hour")
  String? hour;
  @JsonKey(name: "temp")
  double? temp;
  @JsonKey(name: "dewPoint")
  int? dewPoint;
  @JsonKey(name: "humidity")
  int? humidity;
  @JsonKey(name: "pressure")
  int? pressure;
  @JsonKey(name: "description")
  String? description;
  @JsonKey(name: "icon")
  String? iconLink;
  @JsonKey(name: "precipitation")
  double? precipitation;
  @JsonKey(name: "probprecipitation")
  double? precipitationProbability;
  @JsonKey(name: "clouds")
  int? clouds;
  @JsonKey(name: "windSpeed")
  int? windSpeed;
  @JsonKey(name: "windDeg")
  int? windDeg;
  @JsonKey(name: "windGust")
  int? windGust;

  Map<String, dynamic> toJson() => _$HoulyWeatherDataResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class DayWeatherDataResponse extends HoulyWeatherDataResponse {
  DayWeatherDataResponse();
  factory DayWeatherDataResponse.fromJson(Map<String, dynamic> json) =>
      _$DayWeatherDataResponseFromJson(json);

  @JsonKey(name: "windDirection")
  int? windDirection;
  @JsonKey(name: "tempDay")
  double? tempDay;
  @JsonKey(name: "tempMin")
  double? tempMin;
  @JsonKey(name: "tempMax")
  double? tempMax;
  @JsonKey(name: "daysName")
  String? dayName;

  @JsonKey(name: "date")
  String? date;

  @JsonKey(name: "houly")
  List<HoulyWeatherDataResponse>? houlyWeather;

  @override
  Map<String, dynamic> toJson() => _$DayWeatherDataResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class ListParcelResponse extends BaseResponse {
  ListParcelResponse();
  factory ListParcelResponse.fromJson(Map<String, dynamic> json) =>
      _$ListParcelResponseFromJson(json);

  @JsonKey(name: "data")
  List<ParcelInfoResponse>? listParcel;

  @override
  Map<String, dynamic> toJson() => _$ListParcelResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class CreateFullParcelResponse extends BaseResponse {
  CreateFullParcelResponse();
  factory CreateFullParcelResponse.fromJson(Map<String, dynamic> json) =>
      _$CreateFullParcelResponseFromJson(json);

  @JsonKey(name: "data")
  CreateFullParcelDataResponse? listParcel;

  @override
  Map<String, dynamic> toJson() => _$CreateFullParcelResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class CreateFullParcelDataResponse {
  CreateFullParcelDataResponse();
  factory CreateFullParcelDataResponse.fromJson(Map<String, dynamic> json) =>
      _$CreateFullParcelDataResponseFromJson(json);

  @JsonKey(name: "created")
  List<ParcelInfoResponse>? listCreated;
  @JsonKey(name: "alreadyExists")
  List<ParcelInfoResponse>? listAlreadyExists;

  Map<String, dynamic> toJson() => _$CreateFullParcelDataResponseToJson(this);
  @override
  String toString() => toJson().toString();
}
