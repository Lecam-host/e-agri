// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'advice_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AllAdviceDataResponse _$AllAdviceDataResponseFromJson(
        Map<String, dynamic> json) =>
    AllAdviceDataResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..data = json['data'] == null
          ? null
          : AllAdviceItem.fromJson(json['data'] as Map<String, dynamic>);

Map<String, dynamic> _$AllAdviceDataResponseToJson(
        AllAdviceDataResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.data,
    };

AllAdviceItem _$AllAdviceItemFromJson(Map<String, dynamic> json) =>
    AllAdviceItem()
      ..totalObject = (json['total'] as num?)?.toInt()
      ..currentPage = (json['page'] as num?)?.toInt()
      ..totalOfPages = (json['totalPage'] as num?)?.toInt()
      ..nextPage = (json['nextPage'] as num?)?.toInt()
      ..previousPage = (json['lastPage'] as num?)?.toInt()
      ..perPage = (json['perPage'] as num?)?.toInt()
      ..listAdvice = (json['datas'] as List<dynamic>?)
          ?.map((e) => AdviceResponse.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$AllAdviceItemToJson(AllAdviceItem instance) =>
    <String, dynamic>{
      'total': instance.totalObject,
      'page': instance.currentPage,
      'totalPage': instance.totalOfPages,
      'nextPage': instance.nextPage,
      'lastPage': instance.previousPage,
      'perPage': instance.perPage,
      'datas': instance.listAdvice,
    };

AdviceResponse _$AdviceResponseFromJson(Map<String, dynamic> json) =>
    AdviceResponse(
      (json['userId'] as num).toInt(),
      (json['notation'] as num?)?.toDouble(),
    )
      ..id = (json['id'] as num?)?.toInt()
      ..title = json['title'] as String?
      ..description = json['description'] as String?
      ..createDate = json['createdAt'] as String?
      ..adviceType = json['adviceType'] as String?
      ..illustrationImage = json['illustration'] as String?
      ..files = (json['files'] as List<dynamic>?)
          ?.map((e) => FileResponse.fromJson(e as Map<String, dynamic>))
          .toList()
      ..categorys = (json['category'] as List<dynamic>?)
          ?.map((e) => ObjectResponse.fromJson(e as Map<String, dynamic>))
          .toList()
      ..numberView = (json['numberView'] as num?)?.toInt();

Map<String, dynamic> _$AdviceResponseToJson(AdviceResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'createdAt': instance.createDate,
      'adviceType': instance.adviceType,
      'illustration': instance.illustrationImage,
      'files': instance.files,
      'category': instance.categorys,
      'numberView': instance.numberView,
      'notation': instance.notation,
      'userId': instance.publicateurId,
    };

DetailAdivceResponse _$DetailAdivceResponseFromJson(
        Map<String, dynamic> json) =>
    DetailAdivceResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..advice = json['data'] == null
          ? null
          : AdviceResponse.fromJson(json['data'] as Map<String, dynamic>);

Map<String, dynamic> _$DetailAdivceResponseToJson(
        DetailAdivceResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.advice,
    };

RecommandationResponse _$RecommandationResponseFromJson(
        Map<String, dynamic> json) =>
    RecommandationResponse()
      ..speculationId = (json['speculationId'] as num?)?.toInt()
      ..treatmentRange = json['treatmentRange'] as String?
      ..message = json['message'] as String?
      ..speculation = json['speculation'] == null
          ? null
          : SpeculationResponse.fromJson(
              json['speculation'] as Map<String, dynamic>);

Map<String, dynamic> _$RecommandationResponseToJson(
        RecommandationResponse instance) =>
    <String, dynamic>{
      'speculationId': instance.speculationId,
      'treatmentRange': instance.treatmentRange,
      'message': instance.message,
      'speculation': instance.speculation,
    };

GetListRecommandationResponse _$GetListRecommandationResponseFromJson(
        Map<String, dynamic> json) =>
    GetListRecommandationResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..recommandations = (json['data'] as List<dynamic>?)
          ?.map(
              (e) => RecommandationResponse.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$GetListRecommandationResponseToJson(
        GetListRecommandationResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.recommandations,
    };

AddAdviceResponse _$AddAdviceResponseFromJson(Map<String, dynamic> json) =>
    AddAdviceResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?;

Map<String, dynamic> _$AddAdviceResponseToJson(AddAdviceResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
    };
