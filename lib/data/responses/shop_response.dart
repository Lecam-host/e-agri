import 'dart:convert';

import 'package:eagri/data/responses/responses.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../domain/model/image_model.dart';
import '../../domain/model/methode_paiement_model.dart';
import '../request/request_object.dart';

part 'shop_response.g.dart';

@JsonSerializable()
class AddProductOnMarketResponse extends BaseResponse {
  AddProductOnMarketResponse();
  factory AddProductOnMarketResponse.fromJson(Map<String, dynamic> json) =>
      _$AddProductOnMarketResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$AddProductOnMarketResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class ProductResponse {
  @JsonKey(name: "id")
  int id;
  @JsonKey(name: "location")
  LocationProductResponse? location;

  @JsonKey(name: "locationId")
  int? locationId;

  @JsonKey(name: "product")
  ProductElementResponse product;
  @JsonKey(name: "publicationDate")
  String publicationDate;
  @JsonKey(name: "code")
  String code;
  @JsonKey(name: "description")
  String? description;
  @JsonKey(name: "availability")
  String availability;
  @JsonKey(name: "availabilityDate")
  String? availabilityDate;
  @JsonKey(name: "status")
  String status;
  @JsonKey(name: "paymentId")
  int paymentId;
  @JsonKey(name: "fournisseurId")
  int fournisseurId;
  @JsonKey(name: "typeVente")
  String? typeVente;
  @JsonKey(name: "offerType")
  String typeOffer;
  @JsonKey(name: "notation")
  double rate;

  @JsonKey(name: "images")
  List<ImageProduct>? images;
  @JsonKey(name: "paymentMethod")
  PaymentMethodModel? paymentMethod;

  ProductResponse(
    this.id,
    this.product,
    this.publicationDate,
    this.description,
    this.availability,
    this.status,
    this.paymentId,
    this.fournisseurId,
    this.typeOffer,
    this.typeVente,
    this.rate,
    this.code,
    this.paymentMethod,
    this.locationId,
  );
  factory ProductResponse.fromJson(Map<String, dynamic> json) =>
      _$ProductResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ProductResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class ListProductResponse extends BaseResponse {
  @JsonKey(name: "data")
  List<ProductResponse>? listProduct;
  ListProductResponse();
  factory ListProductResponse.fromJson(Map<String, dynamic> json) =>
      _$ListProductResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$ListProductResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class GetDetailProductResponse extends BaseResponse {
  @JsonKey(name: "data")
  Map<String, dynamic> data;
  GetDetailProductResponse(this.data);
  factory GetDetailProductResponse.fromJson(Map<String, dynamic> json) =>
      _$GetDetailProductResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$GetDetailProductResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class GetListProductResponse {
  @JsonKey(name: "id")
  int id;

  @JsonKey(name: "fournisseurId")
  int fournisseurId;

  @JsonKey(name: "created_at")
  String? createdAt;

  @JsonKey(name: "offers")
  List<Map<String, dynamic>>? listProduct;
  GetListProductResponse({required this.id, required this.fournisseurId});
  factory GetListProductResponse.fromJson(Map<String, dynamic> json) =>
      _$GetListProductResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GetListProductResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class BodyGetListProductResponse extends BaseResponse {
  @JsonKey(name: "data")
  GetAllProductResponse? data;

  BodyGetListProductResponse();
  factory BodyGetListProductResponse.fromJson(Map<String, dynamic> json) =>
      _$BodyGetListProductResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$BodyGetListProductResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class GetAllProductResponse extends PaginateResponse {
  @JsonKey(name: "items")
  List<Map<String, dynamic>>? data;
  GetAllProductResponse();
  factory GetAllProductResponse.fromJson(Map<String, dynamic> json) =>
      _$GetAllProductResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$GetAllProductResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class DataProductUserResponse extends BaseResponse {
  @JsonKey(name: "data")
  GetListProductResponse? data;
  DataProductUserResponse();
  factory DataProductUserResponse.fromJson(Map<String, dynamic> json) =>
      _$DataProductUserResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$DataProductUserResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class LocationProductResponse {
  @JsonKey(name: "locationId")
  int? locationId;
  @JsonKey(name: "regionId")
  int regionId;
  @JsonKey(name: "departementId")
  int? departementId;
  @JsonKey(name: "sprefectureId")
  int? sousprefectureId;
  @JsonKey(name: "localiteId")
  int? localiteId;
  LocationProductResponse(this.regionId);
  factory LocationProductResponse.fromJson(Map<String, dynamic> json) =>
      _$LocationProductResponseFromJson(json);

  Map<String, dynamic> toJson() => _$LocationProductResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class ProductElementResponse {
  @JsonKey(name: "id")
  int id;
  @JsonKey(name: "speculationId")
  int? speculationId;
  @JsonKey(name: "pesticideId")
  int? pesticideId;
  @JsonKey(name: "quantity")
  int quantity;
  @JsonKey(name: "price_u")
  double price;
  @JsonKey(name: "unitOfMeasurment")
  String? unitOfMeasurment;
  @JsonKey(name: "speculation")
  SpeculationResponse? speculation;
  ProductElementResponse(this.id, this.quantity, this.price,
      this.unitOfMeasurment, this.speculation);
  factory ProductElementResponse.fromJson(Map<String, dynamic> json) =>
      _$ProductElementResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ProductElementResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class AddProductInShoppingCartResponse extends BaseResponse {
  @JsonKey(name: "data")
  DataProductInShoppingCartResponse data;

  AddProductInShoppingCartResponse(
    this.data,
  );
  factory AddProductInShoppingCartResponse.fromJson(
          Map<String, dynamic> json) =>
      _$AddProductInShoppingCartResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() =>
      _$AddProductInShoppingCartResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class DataProductInShoppingCartResponse {
  @JsonKey(name: "cartItems")
  List<ProductShoppingCartsItem> cartItems;

  @JsonKey(name: "id")
  int id;

  @JsonKey(name: "userId")
  int userId;

  DataProductInShoppingCartResponse(this.id, this.cartItems, this.userId);
  factory DataProductInShoppingCartResponse.fromJson(
          Map<String, dynamic> json) =>
      _$DataProductInShoppingCartResponseFromJson(json);

  Map<String, dynamic> toJson() =>
      _$DataProductInShoppingCartResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class GetCartResponse extends BaseResponse {
  @JsonKey(name: "data")
  List<ProductShoppingCartsItem> cartItems;

  @JsonKey(name: "id")
  int id;

  GetCartResponse(
    this.id,
    this.cartItems,
  );
  factory GetCartResponse.fromJson(Map<String, dynamic> json) =>
      _$GetCartResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$GetCartResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class GetCartResponseBody extends BaseResponse {
  @JsonKey(name: "data")
  List<CartResponseBody> listCart;

  GetCartResponseBody(
    this.listCart,
  );
  factory GetCartResponseBody.fromJson(Map<String, dynamic> json) =>
      _$GetCartResponseBodyFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$GetCartResponseBodyToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class CartResponseBody {
  @JsonKey(name: "cartItems")
  List<CartItemReponse> cartItems;

  @JsonKey(name: "id")
  int id;
  @JsonKey(name: "userId")
  int userId;

  @JsonKey(name: "createdAt")
  String createdAt;

  @JsonKey(name: "status")
  String status;

  @JsonKey(name: "totalPrice")
  double totalPrice;

  CartResponseBody(
    this.id,
    this.cartItems,
    this.createdAt,
    this.status,
    this.totalPrice,
    this.userId,
  );
  factory CartResponseBody.fromJson(Map<String, dynamic> json) =>
      _$CartResponseBodyFromJson(json);

  Map<String, dynamic> toJson() => _$CartResponseBodyToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class CartItemReponse {
  @JsonKey(name: "id")
  int id;

  @JsonKey(name: "idProduct")
  int idProduct;

  @JsonKey(name: "quantity")
  int quantity;
  @JsonKey(name: "price")
  double price;
  @JsonKey(name: "fournisseurId")
  int fournisseurId;

  CartItemReponse(
      this.id, this.idProduct, this.quantity, this.price, this.fournisseurId);
  factory CartItemReponse.fromJson(Map<String, dynamic> json) =>
      _$CartItemReponseFromJson(json);

  Map<String, dynamic> toJson() => _$CartItemReponseToJson(this);
  @override
  String toString() => toJson().toString();
}

GetListIntrantResponse getListIntrantResponseFromJson(String str) =>
    GetListIntrantResponse.fromJson(json.decode(str));

String getListIntrantResponseToJson(GetListIntrantResponse data) =>
    json.encode(data.toJson());

class GetListIntrantResponse {
  GetListIntrantResponse({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  int statusCode;
  String statusMessage;
  Data data;

  factory GetListIntrantResponse.fromJson(Map<String, dynamic> json) =>
      GetListIntrantResponse(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data.toJson(),
      };
}

class Data {
  Data({
    required this.total,
    required this.totalOfPages,
    required this.perPage,
    this.sorts,
    this.order,
    required this.currentPage,
    required this.nextPage,
    required this.previousPage,
    this.filters,
    required this.items,
  });

  int total;
  int totalOfPages;
  int perPage;
  dynamic sorts;
  dynamic order;
  int currentPage;
  int nextPage;
  int previousPage;
  dynamic filters;
  List<ProductIntrantItem> items;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        total: json["total"],
        totalOfPages: json["total_of_pages"],
        perPage: json["per_page"],
        sorts: json["sorts"],
        order: json["order"],
        currentPage: json["current_page"],
        nextPage: json["next_page"],
        previousPage: json["previous_page"],
        filters: json["filters"],
        items: List<ProductIntrantItem>.from(
            json["items"].map((x) => ProductIntrantItem.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "total_of_pages": totalOfPages,
        "per_page": perPage,
        "sorts": sorts,
        "order": order,
        "current_page": currentPage,
        "next_page": nextPage,
        "previous_page": previousPage,
        "filters": filters,
        "items": List<dynamic>.from(items.map((x) => x.toJson())),
      };
}

class ProductIntrantItem {
  ProductIntrantItem({
    required this.id,
    this.identifiant,
    required this.publicationDate,
    this.description,
    required this.code,
    required this.availability,
    this.availabilityDate,
    this.expirationDate,
    this.certification,
    this.typeVente,
    required this.location,
    required this.status,
    required this.paymentId,
    required this.product,
    required this.images,
    required this.offerType,
    required this.notation,
    this.paymentMethod,
    this.typeOrder,
    required this.createdAt,
    required this.updatedAt,
    required this.deleted,
    this.deletedAt,
    required this.fournisseurId,
  });

  int id;
  DateTime publicationDate;
  String? description;
  String code;
  String availability;
  dynamic availabilityDate;
  DateTime? expirationDate;
  String? certification;
  String? identifiant;

  String? typeVente;
  Location location;
  String status;
  int paymentId;
  Product product;
  List<ImageProduct> images;
  String offerType;
  double notation;
  dynamic typeOrder;
  DateTime createdAt;
  DateTime updatedAt;
  bool deleted;
  dynamic deletedAt;
  int fournisseurId;
  PaymentMethodModel? paymentMethod;

  factory ProductIntrantItem.fromJson(Map<String, dynamic> json) =>
      ProductIntrantItem(
        id: json["id"],
        identifiant: json["product"]["identifiant"],
        publicationDate: DateTime.parse(json["publicationDate"]),
        description: json["description"],
        code: json["code"],
        availability: json["availability"],
        availabilityDate: json["availabilityDate"],
        expirationDate: json["expirationDate"] == null
            ? null
            : DateTime.parse(json["expirationDate"]),
        certification: json["certification"],
        typeVente: json["typeVente"],
        location: Location.fromJson(json["location"]),
        status: json["status"],
        paymentId: json["paymentId"],
        product: Product.fromJson(json["product"]),
        images: List<ImageProduct>.from(
            json["images"].map((x) => ImageProduct.fromJson(x))),
        offerType: json["offerType"],
        notation: json["notation"],
        paymentMethod:json["paymentMethod"]!=null? PaymentMethodModel.fromJson(json["paymentMethod"]):null,
        typeOrder: json["typeOrder"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        deleted: json["deleted"],
        deletedAt: json["deleted_at"],
        fournisseurId: json["fournisseurId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "publicationDate": publicationDate.toIso8601String(),
        "description": description,
        "code": code,
        "availability": availability,
        "availabilityDate": availabilityDate,
        "expirationDate":
            "${expirationDate!.year.toString().padLeft(4, '0')}-${expirationDate!.month.toString().padLeft(2, '0')}-${expirationDate!.day.toString().padLeft(2, '0')}",
        "certification": certification,
        "typeVente": typeVente,
        "location": location.toJson(),
        "status": status,
        "paymentId": paymentId,
        "product": product.toJson(),
        "images": List<dynamic>.from(images.map((x) => x.toJson())),
        "offerType": offerType,
        "notation": notation,
        "paymentMethod": paymentMethod,
        "typeOrder": typeOrder,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "deleted": deleted,
        "deleted_at": deletedAt,
        "fournisseurId": fournisseurId,
      };
}

class Location {
  Location(
      {required this.id,
      required this.regionId,
      this.departementId,
      this.sprefectureId,
      this.localiteId,
      this.locationId});

  int? locationId;
  int id;
  int? regionId;
  int? departementId;
  int? sprefectureId;
  int? localiteId;

  factory Location.fromJson(Map<String, dynamic> json) => Location(
        id: json["id"],
        regionId: json["regionId"],
        departementId: json["departementId"],
        sprefectureId: json["sprefectureId"],
        localiteId: json["localiteId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "regionId": regionId,
        "departementId": departementId,
        "sprefectureId": sprefectureId,
        "localiteId": localiteId,
      };
}

class Product {
  Product({
    required this.id,
    required this.identifiant,
    this.intrantId,
    this.intrant,
    required this.quantity,
    required this.initialQuantity,
    required this.priceU,
    required this.createdAt,
    required this.unitOfMeasurment,
    this.categorieId,
    this.speculationId,
  });

  int id;
  String identifiant;
  int? intrantId;
  Intrant? intrant;
  int quantity;
  int initialQuantity;
  int priceU;
  DateTime createdAt;
  String unitOfMeasurment;
  int? categorieId;
  int? speculationId;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        id: json["id"],
        identifiant: json["identifiant"],
        intrantId: json["speculationId"],
        intrant: json["speculation"] == null
            ? null
            : Intrant.fromJson(json["speculation"]),
        quantity: json["quantity"],
        initialQuantity: json["initial_quantity"],
        priceU: json["price_u"].toInt(),
        createdAt: DateTime.parse(json["created_at"]),
        unitOfMeasurment: json["unitOfMeasurmentName"],
        categorieId: json["categorieId"],
        speculationId: json["speculationId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "identifiant": identifiant,
        "intrantId": intrantId,
        "intrant": intrant?.toJson(),
        "quantity": quantity,
        "initial_quantity": initialQuantity,
        "price_u": priceU,
        "created_at": createdAt.toIso8601String(),
        "unitOfMeasurment": unitOfMeasurment,
        "categorieId": categorieId,
        "speculationId": speculationId,
      };
}

class Intrant {
  Intrant({
    required this.id,
    required this.libelle,
    required this.typeCultures,
    required this.specifications,
    required this.nHomologation,
    required this.distributorAgree,
    this.description,
    required this.createdAt,
    required this.deleted,
    this.updatedAt,
    this.deletedAt,
  });

  int id;
  String libelle;
  List<TypeCulture> typeCultures;
  String specifications;
  String nHomologation;
  String distributorAgree;
  String? description;
  DateTime createdAt;
  bool deleted;
  dynamic updatedAt;
  dynamic deletedAt;

  factory Intrant.fromJson(Map<String, dynamic> json) => Intrant(
        id: json["id"],
        libelle: json["libelle"],
        typeCultures: List<TypeCulture>.from(
            json["typeCultures"].map((x) => TypeCulture.fromJson(x))),
        specifications: json["specifications"],
        nHomologation: json["n_homologation"],
        distributorAgree: json["distributorAgree"],
        description: json["description"],
        createdAt: DateTime.parse(json["createdAt"]),
        deleted: json["deleted"],
        updatedAt: json["updatedAt"],
        deletedAt: json["deletedAt"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "libelle": libelle,
        "typeCultures": List<dynamic>.from(typeCultures.map((x) => x.toJson())),
        "specifications": specifications,
        "n_homologation": nHomologation,
        "distributorAgree": distributorAgree,
        "description": description,
        "createdAt": createdAt.toIso8601String(),
        "deleted": deleted,
        "updatedAt": updatedAt,
        "deletedAt": deletedAt,
      };
}

class TypeCulture {
  TypeCulture({
    required this.id,
    required this.libelle,
  });

  int id;
  String libelle;

  factory TypeCulture.fromJson(Map<String, dynamic> json) => TypeCulture(
        id: json["id"],
        libelle: json["libelle"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "libelle": libelle,
      };
}
