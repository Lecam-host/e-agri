// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lieu_responses.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegionResponse _$RegionResponseFromJson(Map<String, dynamic> json) =>
    RegionResponse()
      ..regionId = (json['regionId'] as num?)?.toInt()
      ..name = json['name'] as String?;

Map<String, dynamic> _$RegionResponseToJson(RegionResponse instance) =>
    <String, dynamic>{
      'regionId': instance.regionId,
      'name': instance.name,
    };

DepartementResponse _$DepartementResponseFromJson(Map<String, dynamic> json) =>
    DepartementResponse()
      ..departementId = (json['departementId'] as num?)?.toInt()
      ..name = json['name'] as String?
      ..region = json['region'] == null
          ? null
          : RegionResponse.fromJson(json['region'] as Map<String, dynamic>);

Map<String, dynamic> _$DepartementResponseToJson(
        DepartementResponse instance) =>
    <String, dynamic>{
      'departementId': instance.departementId,
      'name': instance.name,
      'region': instance.region,
    };

SousPrefectureResponse _$SousPrefectureResponseFromJson(
        Map<String, dynamic> json) =>
    SousPrefectureResponse()
      ..sousPrefectureId = (json['sousPrefectureId'] as num?)?.toInt()
      ..name = json['name'] as String?
      ..departement = json['departement'] == null
          ? null
          : DepartementResponse.fromJson(
              json['departement'] as Map<String, dynamic>);

Map<String, dynamic> _$SousPrefectureResponseToJson(
        SousPrefectureResponse instance) =>
    <String, dynamic>{
      'sousPrefectureId': instance.sousPrefectureId,
      'name': instance.name,
      'departement': instance.departement,
    };

LocaliteResponse _$LocaliteResponseFromJson(Map<String, dynamic> json) =>
    LocaliteResponse()
      ..localiteId = (json['localiteId'] as num?)?.toInt()
      ..name = json['name'] as String?
      ..sousPrefecture = json['sousPrefecture'] == null
          ? null
          : SousPrefectureResponse.fromJson(
              json['sousPrefecture'] as Map<String, dynamic>);

Map<String, dynamic> _$LocaliteResponseToJson(LocaliteResponse instance) =>
    <String, dynamic>{
      'localiteId': instance.localiteId,
      'name': instance.name,
      'sousPrefecture': instance.sousPrefecture,
    };

LocationForWeather _$LocationForWeatherFromJson(Map<String, dynamic> json) =>
    LocationForWeather()
      ..cityName = json['cityName'] as String?
      ..lat = json['lat'] as String?
      ..lng = json['lng'] as String?;

Map<String, dynamic> _$LocationForWeatherToJson(LocationForWeather instance) =>
    <String, dynamic>{
      'cityName': instance.cityName,
      'lat': instance.lat,
      'lng': instance.lng,
    };

ListLocationForWeather _$ListLocationForWeatherFromJson(
        Map<String, dynamic> json) =>
    ListLocationForWeather()
      ..totalObject = (json['total'] as num?)?.toInt()
      ..currentPage = (json['current_page'] as num?)?.toInt()
      ..totalOfPages = (json['total_of_pages'] as num?)?.toInt()
      ..nextPage = (json['next_page'] as num?)?.toInt()
      ..previousPage = (json['previous_page'] as num?)?.toInt()
      ..perPage = (json['per_page'] as num?)?.toInt()
      ..listLocation = (json['items'] as List<dynamic>?)
          ?.map((e) => LocationForWeather.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$ListLocationForWeatherToJson(
        ListLocationForWeather instance) =>
    <String, dynamic>{
      'total': instance.totalObject,
      'current_page': instance.currentPage,
      'total_of_pages': instance.totalOfPages,
      'next_page': instance.nextPage,
      'previous_page': instance.previousPage,
      'per_page': instance.perPage,
      'items': instance.listLocation,
    };

ListLocationForWeatherResponse _$ListLocationForWeatherResponseFromJson(
        Map<String, dynamic> json) =>
    ListLocationForWeatherResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..data = json['data'] == null
          ? null
          : ListLocationForWeather.fromJson(
              json['data'] as Map<String, dynamic>);

Map<String, dynamic> _$ListLocationForWeatherResponseToJson(
        ListLocationForWeatherResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.data,
    };
