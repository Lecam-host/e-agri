import 'dart:convert';

import 'package:eagri/data/responses/responses.dart';
import 'package:json_annotation/json_annotation.dart';

part 'account_response.g.dart';

@JsonSerializable()
class UserInfoResponse {
  @JsonKey(name: "id")
  int? userId;

  @JsonKey(name: "token")
  String? token;

  @JsonKey(name: "username")
  String? username;
  @JsonKey(name: "lastname")
  String? lastName;
  @JsonKey(name: "firstname")
  String? firstName;
  @JsonKey(name: "phone")
  String? number;
  @JsonKey(name: "photo")
  String? photo;
  @JsonKey(name: "gender")
  String? gender;
  @JsonKey(name: "birthdate")
  String? birthdate;

  @JsonKey(name: "notation")
  double? notation;

  UserInfoResponse();
  factory UserInfoResponse.fromJson(Map<String, dynamic> json) =>
      _$UserInfoResponseFromJson(json);

  Map<String, dynamic> toJson() => _$UserInfoResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class AccountResponse {
  @JsonKey(name: "scopes")
  List<String>? scopes;

  AccountResponse();
  factory AccountResponse.fromJson(Map<String, dynamic> json) =>
      _$AccountResponseFromJson(json);

  Map<String, dynamic> toJson() => _$AccountResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class GetUserInfoResponse extends BaseResponse {
  @JsonKey(name: "data")
  UserInfoResponse? data;

  GetUserInfoResponse();
  factory GetUserInfoResponse.fromJson(Map<String, dynamic> json) =>
      _$GetUserInfoResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$GetUserInfoResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class AuthResponse extends BaseResponse {
  @JsonKey(name: "data")
  String? data;

  AuthResponse();
  factory AuthResponse.fromJson(Map<String, dynamic> json) =>
      _$AuthResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$AuthResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

AccountInfoResponse accountInfoResponseFromJson(String str) =>
    AccountInfoResponse.fromJson(json.decode(str));

String accountInfoResponseToJson(AccountInfoResponse data) =>
    json.encode(data.toJson());

class AccountInfoResponse {
  int statusCode;
  String statusMessage;
  AccountData data;

  AccountInfoResponse({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  factory AccountInfoResponse.fromJson(Map<String, dynamic> json) =>
      AccountInfoResponse(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: AccountData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data.toJson(),
      };
}

class AccountData {
  int id;
  List<String> scopes;
  List<String> groups;

  AccountData({
    required this.id,
    required this.scopes,
    required this.groups,
  });

  factory AccountData.fromJson(Map<String, dynamic> json) => AccountData(
        id: json["id"],
        scopes: List<String>.from(json["scopes"].map((x) => x)),
        groups: List<String>.from(json["groups"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "scopes": List<dynamic>.from(scopes.map((x) => x)),
        "groups": List<dynamic>.from(groups.map((x) => x)),
      };
}
