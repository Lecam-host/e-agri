// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'local_responses.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AdviceCategorieLocalResponse _$AdviceCategorieLocalResponseFromJson(
        Map<String, dynamic> json) =>
    AdviceCategorieLocalResponse()
      ..id = (json['id'] as num?)?.toInt()
      ..code = (json['code'] as num?)?.toInt()
      ..description = json['description'] as String?
      ..label = json['label'] as String?;

Map<String, dynamic> _$AdviceCategorieLocalResponseToJson(
        AdviceCategorieLocalResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'code': instance.code,
      'description': instance.description,
      'label': instance.label,
    };

CreateParcelRequestJson _$CreateParcelRequestJsonFromJson(
        Map<String, dynamic> json) =>
    CreateParcelRequestJson()..data = json['data'] as String?;

Map<String, dynamic> _$CreateParcelRequestJsonToJson(
        CreateParcelRequestJson instance) =>
    <String, dynamic>{
      'data': instance.data,
    };

ParcelRequestJson _$ParcelRequestJsonFromJson(Map<String, dynamic> json) =>
    ParcelRequestJson()
      ..name = json['name'] as String?
      ..lat = (json['lat'] as num?)?.toDouble()
      ..lon = (json['lon'] as num?)?.toDouble()
      ..createAt = json['createAt'] as String?;

Map<String, dynamic> _$ParcelRequestJsonToJson(ParcelRequestJson instance) =>
    <String, dynamic>{
      'name': instance.name,
      'lat': instance.lat,
      'lon': instance.lon,
      'createAt': instance.createAt,
    };
