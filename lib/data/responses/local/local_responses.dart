import 'package:json_annotation/json_annotation.dart';

import '../responses.dart';
part 'local_responses.g.dart';

@JsonSerializable()
class AdviceCategorieLocalResponse extends AdviceCategoryResponse {
  AdviceCategorieLocalResponse();
  factory AdviceCategorieLocalResponse.fromJson(Map<String, dynamic> json) =>
      _$AdviceCategorieLocalResponseFromJson(json);
  @override
  Map<String, dynamic> toJson() => _$AdviceCategorieLocalResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class CreateParcelRequestJson {
  @JsonKey(name: "data")
  String? data;

  CreateParcelRequestJson();
  factory CreateParcelRequestJson.fromJson(Map<String, dynamic> json) =>
      _$CreateParcelRequestJsonFromJson(json);
  Map<String, dynamic> toJson() => _$CreateParcelRequestJsonToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class ParcelRequestJson {
  @JsonKey(name: "name")
  String? name;
  @JsonKey(name: "lat")
  double? lat;
  @JsonKey(name: "lon")
  double? lon;

  @JsonKey(name: "createAt")
  String? createAt;

  ParcelRequestJson();
  factory ParcelRequestJson.fromJson(Map<String, dynamic> json) =>
      _$ParcelRequestJsonFromJson(json);
  Map<String, dynamic> toJson() => _$ParcelRequestJsonToJson(this);
  @override
  String toString() => toJson().toString();
}
