import 'package:eagri/data/responses/json_properties_name.dart';
import 'package:eagri/data/responses/lieu_responses.dart';
import 'package:json_annotation/json_annotation.dart';

part 'responses.g.dart';

@JsonSerializable()
class BaseResponse {
  @JsonKey(name: "statusCode")
  int? status;
  @JsonKey(name: "statusMessage")
  String? message;
  BaseResponse();
  factory BaseResponse.fromJson(Map<String, dynamic> json) =>
      _$BaseResponseFromJson(json);

  Map<String, dynamic> toJson() => _$BaseResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class ListStringResponse extends BaseResponse {
  @JsonKey(name: "data")
  List<String>? data;
  ListStringResponse();
  factory ListStringResponse.fromJson(Map<String, dynamic> json) =>
      _$ListStringResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$ListStringResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class PaginateResponse {
  @JsonKey(name: "total")
  int? totalObject;

  @JsonKey(name: "current_page")
  int? currentPage;
  @JsonKey(name: "total_of_pages")
  int? totalOfPages;
  @JsonKey(name: "next_page")
  int? nextPage;
  @JsonKey(name: "previous_page")
  int? previousPage;

  @JsonKey(name: "per_page")
  int? perPage;

  PaginateResponse();
  factory PaginateResponse.fromJson(Map<String, dynamic> json) =>
      _$PaginateResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PaginateResponseToJson(this);
  @override
  String toString() => toJson().toString();
}
@JsonSerializable()
class PaginateAdviceListResponse {
  @JsonKey(name: "total")
  int? totalObject;

  @JsonKey(name: "page")
  int? currentPage;
  @JsonKey(name: "totalPage")
  int? totalOfPages;
  @JsonKey(name: "nextPage")
  int? nextPage;
  @JsonKey(name: "lastPage")
  int? previousPage;

  @JsonKey(name: "perPage")
  int? perPage;

  PaginateAdviceListResponse();
  factory PaginateAdviceListResponse.fromJson(Map<String, dynamic> json) =>
      _$PaginateAdviceListResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PaginateAdviceListResponseToJson(this);
  @override
  String toString() => toJson().toString();
}
@JsonSerializable()
class FileResponse {
  @JsonKey(name: "id")
  int? id;
  @JsonKey(name: "title")
  String? title;

  @JsonKey(name: "description")
  String? description;

  @JsonKey(name: "typeFile")
  int? typeFile;

  @JsonKey(name: "url")
  String? fileLink;

  FileResponse();
  factory FileResponse.fromJson(Map<String, dynamic> json) =>
      _$FileResponseFromJson(json);

  Map<String, dynamic> toJson() => _$FileResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class AllAdviceCategoryResponse extends BaseResponse {
  @JsonKey(name: "data")
  List<AdviceCategoryResponse>? data;

  AllAdviceCategoryResponse();
  factory AllAdviceCategoryResponse.fromJson(Map<String, dynamic> json) =>
      _$AllAdviceCategoryResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$AllAdviceCategoryResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class AskAdviceResponse extends BaseResponse {
  AskAdviceResponse();
  factory AskAdviceResponse.fromJson(Map<String, dynamic> json) =>
      _$AskAdviceResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$AskAdviceResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class ObjectResponse {
  ObjectResponse();
  factory ObjectResponse.fromJson(Map<String, dynamic> json) =>
      _$ObjectResponseFromJson(json);

  @JsonKey(name: "id")
  int? id;
  @JsonKey(name: "label")
  String? label;
  Map<String, dynamic> toJson() => _$ObjectResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class AdviceCategoryResponse {
  @JsonKey(name: JsonPropertiesName.id)
  int? id;
  @JsonKey(name: JsonPropertiesName.code)
  int? code;

  @JsonKey(name: JsonPropertiesName.description)
  String? description;

  @JsonKey(name: JsonPropertiesName.label)
  String? label;

  AdviceCategoryResponse();
  factory AdviceCategoryResponse.fromJson(Map<String, dynamic> json) =>
      _$AdviceCategoryResponseFromJson(json);

  Map<String, dynamic> toJson() => _$AdviceCategoryResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class PriceResponse {
  PriceResponse();
  factory PriceResponse.fromJson(Map<String, dynamic> json) =>
      _$PriceResponseFromJson(json);
  @JsonKey(name: "wholesale")
  int? prixGros;
  @JsonKey(name: "detail")
  int? prixDetail;
  @JsonKey(name: "speculationId")
  int? speculationId;
  @JsonKey(name: "regionId")
  int? regionId;
  @JsonKey(name: "departementId")
  int? departementId;
  @JsonKey(name: "sousPrefectureId")
  int? sousPrefectureId;
  @JsonKey(name: "localiteId")
  int? localiteId;

  @JsonKey(name: "fromPeriod")
  String? fromPeriod;
  @JsonKey(name: "toPeriod")
  String? toPeriod;

  @JsonKey(name: "source")
  String? source;

  @JsonKey(name: "unit")
  String? unit;

  Map<String, dynamic> toJson() => _$PriceResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class SpeculationResponse {
  SpeculationResponse();
  factory SpeculationResponse.fromJson(Map<String, dynamic> json) =>
      _$SpeculationResponseFromJson(json);

  @JsonKey(name: "speculationId")
  int? speculationId;
  @JsonKey(name: "name")
  String? name;
  @JsonKey(name: "imageUrl")
  String? imageUrl;

  @JsonKey(name: "unitsOfMeasurement")
  List<String>? listUnit;

  @JsonKey(name: "category")
  CategorieSpeculationResponse? categorieSpeculation;

  Map<String, dynamic> toJson() => _$SpeculationResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class CategorieSpeculationResponse {
  CategorieSpeculationResponse();
  factory CategorieSpeculationResponse.fromJson(Map<String, dynamic> json) =>
      _$CategorieSpeculationResponseFromJson(json);

  @JsonKey(name: "categoryId")
  int? categoryId;
  @JsonKey(name: "name")
  String? name;

  Map<String, dynamic> toJson() => _$CategorieSpeculationResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class SpeculationPriceResponse {
  SpeculationPriceResponse();
  factory SpeculationPriceResponse.fromJson(Map<String, dynamic> json) =>
      _$SpeculationPriceResponseFromJson(json);

  @JsonKey(name: "price")
  PriceResponse? priceInfo;
  @JsonKey(name: "speculation")
  SpeculationResponse? speculationInfo;

  @JsonKey(name: "location")
  LocationPriceResponse? location;

  Map<String, dynamic> toJson() => _$SpeculationPriceResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class LocationPriceResponse extends LocaliteResponse {
  LocationPriceResponse();
  factory LocationPriceResponse.fromJson(Map<String, dynamic> json) =>
      _$LocationPriceResponseFromJson(json);

  @JsonKey(name: "region")
  RegionResponse? region;
  @JsonKey(name: "departement")
  DepartementResponse? departement;
  @override
  Map<String, dynamic> toJson() => _$LocationPriceResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class GetSpeculationPriceResponse extends BaseResponse {
  GetSpeculationPriceResponse();
  factory GetSpeculationPriceResponse.fromJson(Map<String, dynamic> json) =>
      _$GetSpeculationPriceResponseFromJson(json);

  @JsonKey(name: "data")
  List<SpeculationPriceResponse>? data;

  @override
  Map<String, dynamic> toJson() => _$GetSpeculationPriceResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class GetSpeculationPriceNonOfficielResponse extends BaseResponse {
  GetSpeculationPriceNonOfficielResponse();
  factory GetSpeculationPriceNonOfficielResponse.fromJson(
          Map<String, dynamic> json) =>
      _$GetSpeculationPriceNonOfficielResponseFromJson(json);

  @JsonKey(name: "data")
  GetSpeculationPriceNonOfficielDataResponse? data;

  @override
  Map<String, dynamic> toJson() =>
      _$GetSpeculationPriceNonOfficielResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class GetSpeculationPriceNonOfficielDataResponse {
  GetSpeculationPriceNonOfficielDataResponse();
  factory GetSpeculationPriceNonOfficielDataResponse.fromJson(
          Map<String, dynamic> json) =>
      _$GetSpeculationPriceNonOfficielDataResponseFromJson(json);

  @JsonKey(name: "datas")
  List<SpeculationPriceResponse>? listPrice;

  Map<String, dynamic> toJson() =>
      _$GetSpeculationPriceNonOfficielDataResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class ListSpeculationPriceResponse extends PaginateResponse {
  ListSpeculationPriceResponse();
  factory ListSpeculationPriceResponse.fromJson(Map<String, dynamic> json) =>
      _$ListSpeculationPriceResponseFromJson(json);

  @JsonKey(name: "data")
  List<SpeculationPriceResponse>? listSpeculationPrice;

  @override
  Map<String, dynamic> toJson() => _$ListSpeculationPriceResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class ListRegionResponse extends BaseResponse {
  ListRegionResponse();

  factory ListRegionResponse.fromJson(Map<String, dynamic> json) =>
      _$ListRegionResponseFromJson(json);
  @JsonKey(name: "data")
  List<RegionResponse>? data;

  @override
  Map<String, dynamic> toJson() => _$ListRegionResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class ListSpeculationResponse extends BaseResponse {
  ListSpeculationResponse();

  factory ListSpeculationResponse.fromJson(Map<String, dynamic> json) =>
      _$ListSpeculationResponseFromJson(json);
  @JsonKey(name: "data")
  List<SpeculationResponse>? data;

  @override
  Map<String, dynamic> toJson() => _$ListSpeculationResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class ListDepartementResponse extends BaseResponse {
  ListDepartementResponse();

  factory ListDepartementResponse.fromJson(Map<String, dynamic> json) =>
      _$ListDepartementResponseFromJson(json);
  @JsonKey(name: "data")
  List<DepartementResponse>? listDepartement;

  @override
  Map<String, dynamic> toJson() => _$ListDepartementResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class ListSousPrefectureResponse extends BaseResponse {
  ListSousPrefectureResponse();

  factory ListSousPrefectureResponse.fromJson(Map<String, dynamic> json) =>
      _$ListSousPrefectureResponseFromJson(json);
  @JsonKey(name: "data")
  List<SousPrefectureResponse>? listSousPrefecture;

  @override
  Map<String, dynamic> toJson() => _$ListSousPrefectureResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class ListLocaliteResponse extends BaseResponse {
  ListLocaliteResponse();

  factory ListLocaliteResponse.fromJson(Map<String, dynamic> json) =>
      _$ListLocaliteResponseFromJson(json);
  @JsonKey(name: "data")
  List<LocaliteResponse>? listLocalite;

  @override
  Map<String, dynamic> toJson() => _$ListLocaliteResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class ConfigResponse {
  ConfigResponse();

  factory ConfigResponse.fromJson(Map<String, dynamic> json) =>
      _$ConfigResponseFromJson(json);
  @JsonKey(name: "id")
  int? id;

  @JsonKey(name: "name")
  String? name;

  @JsonKey(name: "code")
  int? code;
  @JsonKey(name: "type")
  int? typeConfigId;
  @JsonKey(name: "isDisable")
  int? isDisable;
  Map<String, dynamic> toJson() => _$ConfigResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class GetListDomaineResponse extends BaseResponse {
  GetListDomaineResponse(this.listAlertDomain);

  factory GetListDomaineResponse.fromJson(Map<String, dynamic> json) =>
      _$GetListDomaineResponseFromJson(json);

  @JsonKey(name: "data")
  List<ConfigResponse> listAlertDomain;
  @override
  Map<String, dynamic> toJson() => _$GetListDomaineResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class GetDomaineAlertDataResponse extends BaseResponse {
  GetDomaineAlertDataResponse(this.data);

  factory GetDomaineAlertDataResponse.fromJson(Map<String, dynamic> json) =>
      _$GetDomaineAlertDataResponseFromJson(json);

  @JsonKey(name: "data")
  GetListDomaineResponse data;
  @override
  Map<String, dynamic> toJson() => _$GetDomaineAlertDataResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class ListConfigResponse {
  ListConfigResponse();

  factory ListConfigResponse.fromJson(Map<String, dynamic> json) =>
      _$ListConfigResponseFromJson(json);
  @JsonKey(name: "id")
  int? id;

  @JsonKey(name: "name")
  String? name;

  @JsonKey(name: "code")
  int? code;
  @JsonKey(name: "listConfig")
  List<ConfigResponse>? listConfig;
  Map<String, dynamic> toJson() => _$ListConfigResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class AllConfigResponse extends BaseResponse {
  AllConfigResponse();

  factory AllConfigResponse.fromJson(Map<String, dynamic> json) =>
      _$AllConfigResponseFromJson(json);

  @JsonKey(name: "data")
  List<ListConfigResponse>? listConfig;
  @override
  Map<String, dynamic> toJson() => _$AllConfigResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class AskAdviceDataResponse {
  AskAdviceDataResponse();

  factory AskAdviceDataResponse.fromJson(Map<String, dynamic> json) =>
      _$AskAdviceDataResponseFromJson(json);

  @JsonKey(name: "id")
  int? id;

  @JsonKey(name: "advicesType")
  String? advicesType;

  @JsonKey(name: "categories")
  List<AdviceCategoryResponse>? listCategorieAdvice;

  @JsonKey(name: "typeFile")
  List<String>? typeFiles;

  @JsonKey(name: "organization")
  List<String>? organization;

  @JsonKey(name: "description")
  String? description;

  @JsonKey(name: "isResolve")
  int? isResolve;

  @JsonKey(name: "createdAt")
  String? createdAt;

  Map<String, dynamic> toJson() => _$AskAdviceDataResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class ListAskAdviceResponse extends BaseResponse {
  ListAskAdviceResponse();

  factory ListAskAdviceResponse.fromJson(Map<String, dynamic> json) =>
      _$ListAskAdviceResponseFromJson(json);

  @JsonKey(name: "data")
  List<AskAdviceDataResponse>? listAskAdvice;
  @override
  Map<String, dynamic> toJson() => _$ListAskAdviceResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class MinAndMaxSpeculationPriceResponse {
  @JsonKey(name: "minPriceRegion")
  RegionResponse? minPriceRegion;

  @JsonKey(name: "maxPriceRegion")
  RegionResponse? maxPriceRegion;

  @JsonKey(name: "minPrice")
  int? minPrice;
  @JsonKey(name: "maxPrice")
  int? maxPrice;

  MinAndMaxSpeculationPriceResponse();
  factory MinAndMaxSpeculationPriceResponse.fromJson(
          Map<String, dynamic> json) =>
      _$MinAndMaxSpeculationPriceResponseFromJson(json);

  Map<String, dynamic> toJson() =>
      _$MinAndMaxSpeculationPriceResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class GetMinAndMaxSpeculationPriceResponse extends BaseResponse {
  @JsonKey(name: "data")
  MinAndMaxSpeculationPriceResponse? data;

  GetMinAndMaxSpeculationPriceResponse();
  factory GetMinAndMaxSpeculationPriceResponse.fromJson(
          Map<String, dynamic> json) =>
      _$GetMinAndMaxSpeculationPriceResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() =>
      _$GetMinAndMaxSpeculationPriceResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class AddAlertResponse extends BaseResponse {
  @JsonKey(name: "data")
  AlertResponse? alert;

  AddAlertResponse();
  factory AddAlertResponse.fromJson(Map<String, dynamic> json) =>
      _$AddAlertResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$AddAlertResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class AlertResponse {
  @JsonKey(name: "userId")
  int userId;

  @JsonKey(name: "id")
  int alertId;

  @JsonKey(name: "speculationId")
  int? speculationId;

  AlertResponse(
      {required this.alertId, this.speculationId, required this.userId});
  factory AlertResponse.fromJson(Map<String, dynamic> json) =>
      _$AlertResponseFromJson(json);

  Map<String, dynamic> toJson() => _$AlertResponseToJson(this);
  @override
  String toString() => toJson().toString();
}
