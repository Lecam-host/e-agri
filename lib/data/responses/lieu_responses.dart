import 'package:eagri/data/responses/responses.dart';
import 'package:json_annotation/json_annotation.dart';

part 'lieu_responses.g.dart';

@JsonSerializable()
class RegionResponse {
  @JsonKey(name: "regionId")
  int? regionId;
  @JsonKey(name: "name")
  String? name;
  RegionResponse();
  factory RegionResponse.fromJson(Map<String, dynamic> json) =>
      _$RegionResponseFromJson(json);

  Map<String, dynamic> toJson() => _$RegionResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class DepartementResponse {
  @JsonKey(name: "departementId")
  int? departementId;
  @JsonKey(name: "name")
  String? name;

  @JsonKey(name: "region")
  RegionResponse? region;
  DepartementResponse();
  factory DepartementResponse.fromJson(Map<String, dynamic> json) =>
      _$DepartementResponseFromJson(json);

  Map<String, dynamic> toJson() => _$DepartementResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class SousPrefectureResponse {
  @JsonKey(name: "sousPrefectureId")
  int? sousPrefectureId;
  @JsonKey(name: "name")
  String? name;

  @JsonKey(name: "departement")
  DepartementResponse? departement;
  SousPrefectureResponse();
  factory SousPrefectureResponse.fromJson(Map<String, dynamic> json) =>
      _$SousPrefectureResponseFromJson(json);

  Map<String, dynamic> toJson() => _$SousPrefectureResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class LocaliteResponse {
  @JsonKey(name: "localiteId")
  int? localiteId;
  @JsonKey(name: "name")
  String? name;

  @JsonKey(name: "sousPrefecture")
  SousPrefectureResponse? sousPrefecture;

  LocaliteResponse();
  factory LocaliteResponse.fromJson(Map<String, dynamic> json) =>
      _$LocaliteResponseFromJson(json);

  Map<String, dynamic> toJson() => _$LocaliteResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class LocationForWeather {
  @JsonKey(name: "cityName")
  String? cityName;
  @JsonKey(name: "lat")
  String? lat;

  @JsonKey(name: "lng")
  String? lng;

  LocationForWeather();
  factory LocationForWeather.fromJson(Map<String, dynamic> json) =>
      _$LocationForWeatherFromJson(json);

  Map<String, dynamic> toJson() => _$LocationForWeatherToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class ListLocationForWeather extends PaginateResponse {
  @JsonKey(name: "items")
  List<LocationForWeather>? listLocation;

  ListLocationForWeather();
  factory ListLocationForWeather.fromJson(Map<String, dynamic> json) =>
      _$ListLocationForWeatherFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$ListLocationForWeatherToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class ListLocationForWeatherResponse extends BaseResponse {
  @JsonKey(name: "data")
  ListLocationForWeather? data;

  ListLocationForWeatherResponse();
  factory ListLocationForWeatherResponse.fromJson(Map<String, dynamic> json) =>
      _$ListLocationForWeatherResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$ListLocationForWeatherResponseToJson(this);
  @override
  String toString() => toJson().toString();
}
