// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserInfoResponse _$UserInfoResponseFromJson(Map<String, dynamic> json) =>
    UserInfoResponse()
      ..userId = (json['id'] as num?)?.toInt()
      ..token = json['token'] as String?
      ..username = json['username'] as String?
      ..lastName = json['lastname'] as String?
      ..firstName = json['firstname'] as String?
      ..number = json['phone'] as String?
      ..photo = json['photo'] as String?
      ..gender = json['gender'] as String?
      ..birthdate = json['birthdate'] as String?
      ..notation = (json['notation'] as num?)?.toDouble();

Map<String, dynamic> _$UserInfoResponseToJson(UserInfoResponse instance) =>
    <String, dynamic>{
      'id': instance.userId,
      'token': instance.token,
      'username': instance.username,
      'lastname': instance.lastName,
      'firstname': instance.firstName,
      'phone': instance.number,
      'photo': instance.photo,
      'gender': instance.gender,
      'birthdate': instance.birthdate,
      'notation': instance.notation,
    };

AccountResponse _$AccountResponseFromJson(Map<String, dynamic> json) =>
    AccountResponse()
      ..scopes =
          (json['scopes'] as List<dynamic>?)?.map((e) => e as String).toList();

Map<String, dynamic> _$AccountResponseToJson(AccountResponse instance) =>
    <String, dynamic>{
      'scopes': instance.scopes,
    };

GetUserInfoResponse _$GetUserInfoResponseFromJson(Map<String, dynamic> json) =>
    GetUserInfoResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..data = json['data'] == null
          ? null
          : UserInfoResponse.fromJson(json['data'] as Map<String, dynamic>);

Map<String, dynamic> _$GetUserInfoResponseToJson(
        GetUserInfoResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.data,
    };

AuthResponse _$AuthResponseFromJson(Map<String, dynamic> json) => AuthResponse()
  ..status = (json['statusCode'] as num?)?.toInt()
  ..message = json['statusMessage'] as String?
  ..data = json['data'] as String?;

Map<String, dynamic> _$AuthResponseToJson(AuthResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.data,
    };
