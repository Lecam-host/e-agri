// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'responses.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BaseResponse _$BaseResponseFromJson(Map<String, dynamic> json) => BaseResponse()
  ..status = (json['statusCode'] as num?)?.toInt()
  ..message = json['statusMessage'] as String?;

Map<String, dynamic> _$BaseResponseToJson(BaseResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
    };

ListStringResponse _$ListStringResponseFromJson(Map<String, dynamic> json) =>
    ListStringResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..data =
          (json['data'] as List<dynamic>?)?.map((e) => e as String).toList();

Map<String, dynamic> _$ListStringResponseToJson(ListStringResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.data,
    };

PaginateResponse _$PaginateResponseFromJson(Map<String, dynamic> json) =>
    PaginateResponse()
      ..totalObject = (json['total'] as num?)?.toInt()
      ..currentPage = (json['current_page'] as num?)?.toInt()
      ..totalOfPages = (json['total_of_pages'] as num?)?.toInt()
      ..nextPage = (json['next_page'] as num?)?.toInt()
      ..previousPage = (json['previous_page'] as num?)?.toInt()
      ..perPage = (json['per_page'] as num?)?.toInt();

Map<String, dynamic> _$PaginateResponseToJson(PaginateResponse instance) =>
    <String, dynamic>{
      'total': instance.totalObject,
      'current_page': instance.currentPage,
      'total_of_pages': instance.totalOfPages,
      'next_page': instance.nextPage,
      'previous_page': instance.previousPage,
      'per_page': instance.perPage,
    };

PaginateAdviceListResponse _$PaginateAdviceListResponseFromJson(
        Map<String, dynamic> json) =>
    PaginateAdviceListResponse()
      ..totalObject = (json['total'] as num?)?.toInt()
      ..currentPage = (json['page'] as num?)?.toInt()
      ..totalOfPages = (json['totalPage'] as num?)?.toInt()
      ..nextPage = (json['nextPage'] as num?)?.toInt()
      ..previousPage = (json['lastPage'] as num?)?.toInt()
      ..perPage = (json['perPage'] as num?)?.toInt();

Map<String, dynamic> _$PaginateAdviceListResponseToJson(
        PaginateAdviceListResponse instance) =>
    <String, dynamic>{
      'total': instance.totalObject,
      'page': instance.currentPage,
      'totalPage': instance.totalOfPages,
      'nextPage': instance.nextPage,
      'lastPage': instance.previousPage,
      'perPage': instance.perPage,
    };

FileResponse _$FileResponseFromJson(Map<String, dynamic> json) => FileResponse()
  ..id = (json['id'] as num?)?.toInt()
  ..title = json['title'] as String?
  ..description = json['description'] as String?
  ..typeFile = (json['typeFile'] as num?)?.toInt()
  ..fileLink = json['url'] as String?;

Map<String, dynamic> _$FileResponseToJson(FileResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'typeFile': instance.typeFile,
      'url': instance.fileLink,
    };

AllAdviceCategoryResponse _$AllAdviceCategoryResponseFromJson(
        Map<String, dynamic> json) =>
    AllAdviceCategoryResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..data = (json['data'] as List<dynamic>?)
          ?.map(
              (e) => AdviceCategoryResponse.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$AllAdviceCategoryResponseToJson(
        AllAdviceCategoryResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.data,
    };

AskAdviceResponse _$AskAdviceResponseFromJson(Map<String, dynamic> json) =>
    AskAdviceResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?;

Map<String, dynamic> _$AskAdviceResponseToJson(AskAdviceResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
    };

ObjectResponse _$ObjectResponseFromJson(Map<String, dynamic> json) =>
    ObjectResponse()
      ..id = (json['id'] as num?)?.toInt()
      ..label = json['label'] as String?;

Map<String, dynamic> _$ObjectResponseToJson(ObjectResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'label': instance.label,
    };

AdviceCategoryResponse _$AdviceCategoryResponseFromJson(
        Map<String, dynamic> json) =>
    AdviceCategoryResponse()
      ..id = (json['id'] as num?)?.toInt()
      ..code = (json['code'] as num?)?.toInt()
      ..description = json['description'] as String?
      ..label = json['label'] as String?;

Map<String, dynamic> _$AdviceCategoryResponseToJson(
        AdviceCategoryResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'code': instance.code,
      'description': instance.description,
      'label': instance.label,
    };

PriceResponse _$PriceResponseFromJson(Map<String, dynamic> json) =>
    PriceResponse()
      ..prixGros = (json['wholesale'] as num?)?.toInt()
      ..prixDetail = (json['detail'] as num?)?.toInt()
      ..speculationId = (json['speculationId'] as num?)?.toInt()
      ..regionId = (json['regionId'] as num?)?.toInt()
      ..departementId = (json['departementId'] as num?)?.toInt()
      ..sousPrefectureId = (json['sousPrefectureId'] as num?)?.toInt()
      ..localiteId = (json['localiteId'] as num?)?.toInt()
      ..fromPeriod = json['fromPeriod'] as String?
      ..toPeriod = json['toPeriod'] as String?
      ..source = json['source'] as String?
      ..unit = json['unit'] as String?;

Map<String, dynamic> _$PriceResponseToJson(PriceResponse instance) =>
    <String, dynamic>{
      'wholesale': instance.prixGros,
      'detail': instance.prixDetail,
      'speculationId': instance.speculationId,
      'regionId': instance.regionId,
      'departementId': instance.departementId,
      'sousPrefectureId': instance.sousPrefectureId,
      'localiteId': instance.localiteId,
      'fromPeriod': instance.fromPeriod,
      'toPeriod': instance.toPeriod,
      'source': instance.source,
      'unit': instance.unit,
    };

SpeculationResponse _$SpeculationResponseFromJson(Map<String, dynamic> json) =>
    SpeculationResponse()
      ..speculationId = (json['speculationId'] as num?)?.toInt()
      ..name = json['name'] as String?
      ..imageUrl = json['imageUrl'] as String?
      ..listUnit = (json['unitsOfMeasurement'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList()
      ..categorieSpeculation = json['category'] == null
          ? null
          : CategorieSpeculationResponse.fromJson(
              json['category'] as Map<String, dynamic>);

Map<String, dynamic> _$SpeculationResponseToJson(
        SpeculationResponse instance) =>
    <String, dynamic>{
      'speculationId': instance.speculationId,
      'name': instance.name,
      'imageUrl': instance.imageUrl,
      'unitsOfMeasurement': instance.listUnit,
      'category': instance.categorieSpeculation,
    };

CategorieSpeculationResponse _$CategorieSpeculationResponseFromJson(
        Map<String, dynamic> json) =>
    CategorieSpeculationResponse()
      ..categoryId = (json['categoryId'] as num?)?.toInt()
      ..name = json['name'] as String?;

Map<String, dynamic> _$CategorieSpeculationResponseToJson(
        CategorieSpeculationResponse instance) =>
    <String, dynamic>{
      'categoryId': instance.categoryId,
      'name': instance.name,
    };

SpeculationPriceResponse _$SpeculationPriceResponseFromJson(
        Map<String, dynamic> json) =>
    SpeculationPriceResponse()
      ..priceInfo = json['price'] == null
          ? null
          : PriceResponse.fromJson(json['price'] as Map<String, dynamic>)
      ..speculationInfo = json['speculation'] == null
          ? null
          : SpeculationResponse.fromJson(
              json['speculation'] as Map<String, dynamic>)
      ..location = json['location'] == null
          ? null
          : LocationPriceResponse.fromJson(
              json['location'] as Map<String, dynamic>);

Map<String, dynamic> _$SpeculationPriceResponseToJson(
        SpeculationPriceResponse instance) =>
    <String, dynamic>{
      'price': instance.priceInfo,
      'speculation': instance.speculationInfo,
      'location': instance.location,
    };

LocationPriceResponse _$LocationPriceResponseFromJson(
        Map<String, dynamic> json) =>
    LocationPriceResponse()
      ..localiteId = (json['localiteId'] as num?)?.toInt()
      ..name = json['name'] as String?
      ..sousPrefecture = json['sousPrefecture'] == null
          ? null
          : SousPrefectureResponse.fromJson(
              json['sousPrefecture'] as Map<String, dynamic>)
      ..region = json['region'] == null
          ? null
          : RegionResponse.fromJson(json['region'] as Map<String, dynamic>)
      ..departement = json['departement'] == null
          ? null
          : DepartementResponse.fromJson(
              json['departement'] as Map<String, dynamic>);

Map<String, dynamic> _$LocationPriceResponseToJson(
        LocationPriceResponse instance) =>
    <String, dynamic>{
      'localiteId': instance.localiteId,
      'name': instance.name,
      'sousPrefecture': instance.sousPrefecture,
      'region': instance.region,
      'departement': instance.departement,
    };

GetSpeculationPriceResponse _$GetSpeculationPriceResponseFromJson(
        Map<String, dynamic> json) =>
    GetSpeculationPriceResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..data = (json['data'] as List<dynamic>?)
          ?.map((e) =>
              SpeculationPriceResponse.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$GetSpeculationPriceResponseToJson(
        GetSpeculationPriceResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.data,
    };

GetSpeculationPriceNonOfficielResponse
    _$GetSpeculationPriceNonOfficielResponseFromJson(
            Map<String, dynamic> json) =>
        GetSpeculationPriceNonOfficielResponse()
          ..status = (json['statusCode'] as num?)?.toInt()
          ..message = json['statusMessage'] as String?
          ..data = json['data'] == null
              ? null
              : GetSpeculationPriceNonOfficielDataResponse.fromJson(
                  json['data'] as Map<String, dynamic>);

Map<String, dynamic> _$GetSpeculationPriceNonOfficielResponseToJson(
        GetSpeculationPriceNonOfficielResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.data,
    };

GetSpeculationPriceNonOfficielDataResponse
    _$GetSpeculationPriceNonOfficielDataResponseFromJson(
            Map<String, dynamic> json) =>
        GetSpeculationPriceNonOfficielDataResponse()
          ..listPrice = (json['datas'] as List<dynamic>?)
              ?.map((e) =>
                  SpeculationPriceResponse.fromJson(e as Map<String, dynamic>))
              .toList();

Map<String, dynamic> _$GetSpeculationPriceNonOfficielDataResponseToJson(
        GetSpeculationPriceNonOfficielDataResponse instance) =>
    <String, dynamic>{
      'datas': instance.listPrice,
    };

ListSpeculationPriceResponse _$ListSpeculationPriceResponseFromJson(
        Map<String, dynamic> json) =>
    ListSpeculationPriceResponse()
      ..totalObject = (json['total'] as num?)?.toInt()
      ..currentPage = (json['current_page'] as num?)?.toInt()
      ..totalOfPages = (json['total_of_pages'] as num?)?.toInt()
      ..nextPage = (json['next_page'] as num?)?.toInt()
      ..previousPage = (json['previous_page'] as num?)?.toInt()
      ..perPage = (json['per_page'] as num?)?.toInt()
      ..listSpeculationPrice = (json['data'] as List<dynamic>?)
          ?.map((e) =>
              SpeculationPriceResponse.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$ListSpeculationPriceResponseToJson(
        ListSpeculationPriceResponse instance) =>
    <String, dynamic>{
      'total': instance.totalObject,
      'current_page': instance.currentPage,
      'total_of_pages': instance.totalOfPages,
      'next_page': instance.nextPage,
      'previous_page': instance.previousPage,
      'per_page': instance.perPage,
      'data': instance.listSpeculationPrice,
    };

ListRegionResponse _$ListRegionResponseFromJson(Map<String, dynamic> json) =>
    ListRegionResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..data = (json['data'] as List<dynamic>?)
          ?.map((e) => RegionResponse.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$ListRegionResponseToJson(ListRegionResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.data,
    };

ListSpeculationResponse _$ListSpeculationResponseFromJson(
        Map<String, dynamic> json) =>
    ListSpeculationResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..data = (json['data'] as List<dynamic>?)
          ?.map((e) => SpeculationResponse.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$ListSpeculationResponseToJson(
        ListSpeculationResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.data,
    };

ListDepartementResponse _$ListDepartementResponseFromJson(
        Map<String, dynamic> json) =>
    ListDepartementResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..listDepartement = (json['data'] as List<dynamic>?)
          ?.map((e) => DepartementResponse.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$ListDepartementResponseToJson(
        ListDepartementResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.listDepartement,
    };

ListSousPrefectureResponse _$ListSousPrefectureResponseFromJson(
        Map<String, dynamic> json) =>
    ListSousPrefectureResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..listSousPrefecture = (json['data'] as List<dynamic>?)
          ?.map(
              (e) => SousPrefectureResponse.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$ListSousPrefectureResponseToJson(
        ListSousPrefectureResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.listSousPrefecture,
    };

ListLocaliteResponse _$ListLocaliteResponseFromJson(
        Map<String, dynamic> json) =>
    ListLocaliteResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..listLocalite = (json['data'] as List<dynamic>?)
          ?.map((e) => LocaliteResponse.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$ListLocaliteResponseToJson(
        ListLocaliteResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.listLocalite,
    };

ConfigResponse _$ConfigResponseFromJson(Map<String, dynamic> json) =>
    ConfigResponse()
      ..id = (json['id'] as num?)?.toInt()
      ..name = json['name'] as String?
      ..code = (json['code'] as num?)?.toInt()
      ..typeConfigId = (json['type'] as num?)?.toInt()
      ..isDisable = (json['isDisable'] as num?)?.toInt();

Map<String, dynamic> _$ConfigResponseToJson(ConfigResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'code': instance.code,
      'type': instance.typeConfigId,
      'isDisable': instance.isDisable,
    };

GetListDomaineResponse _$GetListDomaineResponseFromJson(
        Map<String, dynamic> json) =>
    GetListDomaineResponse(
      (json['data'] as List<dynamic>)
          .map((e) => ConfigResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
    )
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?;

Map<String, dynamic> _$GetListDomaineResponseToJson(
        GetListDomaineResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.listAlertDomain,
    };

GetDomaineAlertDataResponse _$GetDomaineAlertDataResponseFromJson(
        Map<String, dynamic> json) =>
    GetDomaineAlertDataResponse(
      GetListDomaineResponse.fromJson(json['data'] as Map<String, dynamic>),
    )
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?;

Map<String, dynamic> _$GetDomaineAlertDataResponseToJson(
        GetDomaineAlertDataResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.data,
    };

ListConfigResponse _$ListConfigResponseFromJson(Map<String, dynamic> json) =>
    ListConfigResponse()
      ..id = (json['id'] as num?)?.toInt()
      ..name = json['name'] as String?
      ..code = (json['code'] as num?)?.toInt()
      ..listConfig = (json['listConfig'] as List<dynamic>?)
          ?.map((e) => ConfigResponse.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$ListConfigResponseToJson(ListConfigResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'code': instance.code,
      'listConfig': instance.listConfig,
    };

AllConfigResponse _$AllConfigResponseFromJson(Map<String, dynamic> json) =>
    AllConfigResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..listConfig = (json['data'] as List<dynamic>?)
          ?.map((e) => ListConfigResponse.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$AllConfigResponseToJson(AllConfigResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.listConfig,
    };

AskAdviceDataResponse _$AskAdviceDataResponseFromJson(
        Map<String, dynamic> json) =>
    AskAdviceDataResponse()
      ..id = (json['id'] as num?)?.toInt()
      ..advicesType = json['advicesType'] as String?
      ..listCategorieAdvice = (json['categories'] as List<dynamic>?)
          ?.map(
              (e) => AdviceCategoryResponse.fromJson(e as Map<String, dynamic>))
          .toList()
      ..typeFiles =
          (json['typeFile'] as List<dynamic>?)?.map((e) => e as String).toList()
      ..organization = (json['organization'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList()
      ..description = json['description'] as String?
      ..isResolve = (json['isResolve'] as num?)?.toInt()
      ..createdAt = json['createdAt'] as String?;

Map<String, dynamic> _$AskAdviceDataResponseToJson(
        AskAdviceDataResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'advicesType': instance.advicesType,
      'categories': instance.listCategorieAdvice,
      'typeFile': instance.typeFiles,
      'organization': instance.organization,
      'description': instance.description,
      'isResolve': instance.isResolve,
      'createdAt': instance.createdAt,
    };

ListAskAdviceResponse _$ListAskAdviceResponseFromJson(
        Map<String, dynamic> json) =>
    ListAskAdviceResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..listAskAdvice = (json['data'] as List<dynamic>?)
          ?.map(
              (e) => AskAdviceDataResponse.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$ListAskAdviceResponseToJson(
        ListAskAdviceResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.listAskAdvice,
    };

MinAndMaxSpeculationPriceResponse _$MinAndMaxSpeculationPriceResponseFromJson(
        Map<String, dynamic> json) =>
    MinAndMaxSpeculationPriceResponse()
      ..minPriceRegion = json['minPriceRegion'] == null
          ? null
          : RegionResponse.fromJson(
              json['minPriceRegion'] as Map<String, dynamic>)
      ..maxPriceRegion = json['maxPriceRegion'] == null
          ? null
          : RegionResponse.fromJson(
              json['maxPriceRegion'] as Map<String, dynamic>)
      ..minPrice = (json['minPrice'] as num?)?.toInt()
      ..maxPrice = (json['maxPrice'] as num?)?.toInt();

Map<String, dynamic> _$MinAndMaxSpeculationPriceResponseToJson(
        MinAndMaxSpeculationPriceResponse instance) =>
    <String, dynamic>{
      'minPriceRegion': instance.minPriceRegion,
      'maxPriceRegion': instance.maxPriceRegion,
      'minPrice': instance.minPrice,
      'maxPrice': instance.maxPrice,
    };

GetMinAndMaxSpeculationPriceResponse
    _$GetMinAndMaxSpeculationPriceResponseFromJson(Map<String, dynamic> json) =>
        GetMinAndMaxSpeculationPriceResponse()
          ..status = (json['statusCode'] as num?)?.toInt()
          ..message = json['statusMessage'] as String?
          ..data = json['data'] == null
              ? null
              : MinAndMaxSpeculationPriceResponse.fromJson(
                  json['data'] as Map<String, dynamic>);

Map<String, dynamic> _$GetMinAndMaxSpeculationPriceResponseToJson(
        GetMinAndMaxSpeculationPriceResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.data,
    };

AddAlertResponse _$AddAlertResponseFromJson(Map<String, dynamic> json) =>
    AddAlertResponse()
      ..status = (json['statusCode'] as num?)?.toInt()
      ..message = json['statusMessage'] as String?
      ..alert = json['data'] == null
          ? null
          : AlertResponse.fromJson(json['data'] as Map<String, dynamic>);

Map<String, dynamic> _$AddAlertResponseToJson(AddAlertResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.status,
      'statusMessage': instance.message,
      'data': instance.alert,
    };

AlertResponse _$AlertResponseFromJson(Map<String, dynamic> json) =>
    AlertResponse(
      alertId: (json['id'] as num).toInt(),
      speculationId: (json['speculationId'] as num?)?.toInt(),
      userId: (json['userId'] as num).toInt(),
    );

Map<String, dynamic> _$AlertResponseToJson(AlertResponse instance) =>
    <String, dynamic>{
      'userId': instance.userId,
      'id': instance.alertId,
      'speculationId': instance.speculationId,
    };
