import 'package:eagri/data/responses/responses.dart';
import 'package:json_annotation/json_annotation.dart';

part 'advice_response.g.dart';

@JsonSerializable()
class AllAdviceDataResponse extends BaseResponse {
  @JsonKey(name: "data")
  AllAdviceItem? data;

  AllAdviceDataResponse();
  factory AllAdviceDataResponse.fromJson(Map<String, dynamic> json) =>
      _$AllAdviceDataResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$AllAdviceDataResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class AllAdviceItem extends PaginateAdviceListResponse {
  @JsonKey(name: "datas")
  
  List<AdviceResponse>? listAdvice;

  AllAdviceItem();
  factory AllAdviceItem.fromJson(Map<String, dynamic> json) =>
      _$AllAdviceItemFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$AllAdviceItemToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class AdviceResponse {
  @JsonKey(name: "id")
  int? id;
  @JsonKey(name: "title")
  String? title;

  @JsonKey(name: "description")
  String? description;

  @JsonKey(name: "createdAt")
  String? createDate;

  @JsonKey(name: "adviceType")
  String? adviceType;

  @JsonKey(name: "illustration")
  String? illustrationImage;

  @JsonKey(name: "files")
  List<FileResponse>? files;

  @JsonKey(name: "category")
  List<ObjectResponse>? categorys;
  @JsonKey(name: "numberView")
  int? numberView;

  @JsonKey(name: "notation")
  double? notation;
  @JsonKey(name: "userId")
  int publicateurId;

  AdviceResponse(this.publicateurId, this.notation);
  factory AdviceResponse.fromJson(Map<String, dynamic> json) =>
      _$AdviceResponseFromJson(json);

  Map<String, dynamic> toJson() => _$AdviceResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class DetailAdivceResponse extends BaseResponse {
  @JsonKey(name: "data")
  AdviceResponse? advice;

  DetailAdivceResponse();
  factory DetailAdivceResponse.fromJson(Map<String, dynamic> json) =>
      _$DetailAdivceResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$DetailAdivceResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class RecommandationResponse {
  @JsonKey(name: "speculationId")
  int? speculationId;
  @JsonKey(name: "treatmentRange")
  String? treatmentRange;

  @JsonKey(name: "message")
  String? message;

  @JsonKey(name: "speculation")
  SpeculationResponse? speculation;

  RecommandationResponse();
  factory RecommandationResponse.fromJson(Map<String, dynamic> json) =>
      _$RecommandationResponseFromJson(json);

  Map<String, dynamic> toJson() => _$RecommandationResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class GetListRecommandationResponse extends BaseResponse {
  @JsonKey(name: "data")
  List<RecommandationResponse>? recommandations;

  GetListRecommandationResponse();
  factory GetListRecommandationResponse.fromJson(Map<String, dynamic> json) =>
      _$GetListRecommandationResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$GetListRecommandationResponseToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class AddAdviceResponse extends BaseResponse {
  AddAdviceResponse();
  factory AddAdviceResponse.fromJson(Map<String, dynamic> json) =>
      _$AddAdviceResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$AddAdviceResponseToJson(this);
  @override
  String toString() => toJson().toString();
}
