import 'dart:io';

class AddPrestationRequest {
  int fournisseurId, speculationId, price, paymentId, categorieId;
  String availability, description, unitOfMeasurment;

  List<int> location;
  String? availabilityDate, unitTime;
  List<File>? images;
  int? numberSlice;
  AddPrestationRequest({
    required this.fournisseurId,
    required this.speculationId,
    required this.price,
    required this.availability,
    required this.description,
    required this.unitOfMeasurment,
    required this.location,
    required this.categorieId,
    required this.availabilityDate,
    this.images,
    this.unitTime,
    this.numberSlice,
    required this.paymentId,
  });
}

class DemandePrestionRequest {
  int prestationId, quantity, userId;
  String dateStart, dateEnd;
  List<int> location;
  DemandePrestionRequest({
    required this.prestationId,
    required this.quantity,
    required this.userId,
    required this.dateStart,
    required this.dateEnd,
    required this.location,
  });
}

class UpdatePrestationRequest {
  int prestationId;

  List<int>? location;
  String? availabilityDate;
  String? availability;
  String? description;
  int? paymentId;
  int? priceU;

  UpdatePrestationRequest({
    required this.prestationId,
    this.location,
    this.availabilityDate,
    this.availability,
    this.paymentId,
    this.priceU,
    this.description,
  });
}
