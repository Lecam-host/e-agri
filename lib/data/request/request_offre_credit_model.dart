// To parse this JSON data, do
//
//     final requestOffreCreditModel = requestOffreCreditModelFromJson(jsonString);

import 'dart:convert';

RequestOffreCreditModel requestOffreCreditModelFromJson(String str) =>
    RequestOffreCreditModel.fromJson(json.decode(str));

String requestOffreCreditModelToJson(RequestOffreCreditModel data) =>
    json.encode(data.toJson());

class RequestOffreCreditModel {
  int page;
  int perPage;
  String sortBy;
  String? businessLineSlug;
  int? creditorId;
  int? typeCreditId;
  int? creditId;
  double? prixMax;
  double? prixMin;
  String? creditorName;
  String? typeCreditName;
  String? creditName;

  RequestOffreCreditModel({
    required this.page,
    required this.perPage,
    required this.sortBy,
    this.businessLineSlug,
    this.creditorId,
    this.typeCreditId,
    this.creditId,
    this.creditorName,
    this.typeCreditName,
    this.creditName,
    this.prixMin,
    this.prixMax,
  });

  factory RequestOffreCreditModel.fromJson(Map<String, dynamic> json) =>
      RequestOffreCreditModel(
        page: json["page"],
        perPage: json["perPage"],
        sortBy: json["sortBy"],
        businessLineSlug: json["businessLineSlug"],
        creditorId: json["creditorId"],
        typeCreditId: json["typeCreditId"],
        creditId: json["creditId"],
        creditorName: json["creditorName"],
        typeCreditName: json["typeCreditName"],
        creditName: json["creditName"],
      );

  Map<String, dynamic> toJson() => {
        "page": page,
        "perPage": perPage,
        "sortBy": sortBy,
        "businessLineSlug": businessLineSlug,
        "creditorId": creditorId,
        "typeCreditId": typeCreditId,
        "creditId": creditId,
        "creditorName": creditorName,
        "typeCreditName": typeCreditName,
        "creditName": creditName,
      };
}
