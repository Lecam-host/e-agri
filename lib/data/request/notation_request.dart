class AddRateRequest {
  int userId, resourceId, noteurId, note;
  String codeService;
  String? comment;

  AddRateRequest(
      {required this.userId,
      required this.resourceId,
      required this.noteurId,
      required this.note,
      required this.codeService,
      this.comment});
}

class CheckRateRequest {
  int userId;
  int ressourceId;
  int codeService;

  CheckRateRequest({
    required this.userId,
    required this.ressourceId,
    required this.codeService,
  });
}

class ProductListCommentRequest {
  int codeService;
  int resourceId;

  ProductListCommentRequest({
    required this.resourceId,
    required this.codeService,
  });
}
