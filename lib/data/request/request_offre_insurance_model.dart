// To parse this JSON data, do
//
//     final requestOffreCreditModel = requestOffreCreditModelFromJson(jsonString);

import 'dart:convert';

RequestOffreInsuranceModel requestOffreCreditModelFromJson(String str) =>
    RequestOffreInsuranceModel.fromJson(json.decode(str));

String requestOffreCreditModelToJson(RequestOffreInsuranceModel data) =>
    json.encode(data.toJson());

class RequestOffreInsuranceModel {
  int page;
  int perPage;
  String sortBy;
  String? businessLineSlug;
  int? insurerId;
  int? typeInsuranceId;
  int? insuranceId;
  String? insurerName;
  String? typeInsuranceName;
  String? insuranceName;
  double? prixMax;
  double? prixMin;

  RequestOffreInsuranceModel({
    required this.page,
    required this.perPage,
    required this.sortBy,
    this.businessLineSlug,
    this.insurerId,
    this.typeInsuranceId,
    this.insuranceId,
    this.insurerName,
    this.typeInsuranceName,
    this.insuranceName,
    this.prixMin,
    this.prixMax,
  });

  factory RequestOffreInsuranceModel.fromJson(Map<String, dynamic> json) =>
      RequestOffreInsuranceModel(
        page: json["page"],
        perPage: json["perPage"],
        sortBy: json["sortBy"],
        businessLineSlug: json["businessLineSlug"],
        insurerId: json["insurerId"],
        typeInsuranceId: json["typeInsuranceId"],
        insuranceId: json["insuranceId"],
        insurerName: json["insurerName"],
        typeInsuranceName: json["typeInsuranceName"],
        insuranceName: json["insuranceName"],
      );

  Map<String, dynamic> toJson() => {
        "page": page,
        "perPage": perPage,
        "sortBy": sortBy,
        "businessLineSlug": businessLineSlug,
        "insurerId": insurerId,
        "typeInsuranceId": typeInsuranceId,
        "insuranceId": insuranceId,
        "insurerName": insurerName,
        "typeInsuranceName": typeInsuranceName,
        "insuranceName": insuranceName,
      };
}
