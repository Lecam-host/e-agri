// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'request_object.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PriceSpeculationData _$PriceSpeculationDataFromJson(
        Map<String, dynamic> json) =>
    PriceSpeculationData(
      prixDetail: (json['detail'] as num?)?.toInt(),
      prixGros: (json['wholesale'] as num?)?.toInt(),
      regionId: (json['regionId'] as num?)?.toInt(),
      departementId: (json['departementId'] as num?)?.toInt(),
      sousPrefectureId: (json['sousPrefectureId'] as num?)?.toInt(),
      localiteId: (json['localiteId'] as num?)?.toInt(),
    );

Map<String, dynamic> _$PriceSpeculationDataToJson(
        PriceSpeculationData instance) =>
    <String, dynamic>{
      'detail': instance.prixDetail,
      'wholesale': instance.prixGros,
      'regionId': instance.regionId,
      'departementId': instance.departementId,
      'sousPrefectureId': instance.sousPrefectureId,
      'localiteId': instance.localiteId,
    };

AllPriceSpeculationData _$AllPriceSpeculationDataFromJson(
        Map<String, dynamic> json) =>
    AllPriceSpeculationData(
      from: json['from'] as String?,
      official: json['official'] as bool?,
      speculationId: (json['speculationId'] as num?)?.toInt(),
      source: json['source'] as String?,
      to: json['to'] as String?,
      unit: json['unit'] as String?,
      userId: (json['userId'] as num?)?.toInt(),
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => PriceSpeculationData.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$AllPriceSpeculationDataToJson(
        AllPriceSpeculationData instance) =>
    <String, dynamic>{
      'speculationId': instance.speculationId,
      'userId': instance.userId,
      'unit': instance.unit,
      'source': instance.source,
      'from': instance.from,
      'to': instance.to,
      'official': instance.official,
      'data': instance.data,
    };

LocationBodyId _$LocationBodyIdFromJson(Map<String, dynamic> json) =>
    LocationBodyId(
      regionId: (json['regionId'] as num?)?.toInt(),
      departementId: (json['departementId'] as num?)?.toInt(),
      sousPrefectureId: (json['sousPrefectureId'] as num?)?.toInt(),
      localiteId: (json['localiteId'] as num?)?.toInt(),
    );

Map<String, dynamic> _$LocationBodyIdToJson(LocationBodyId instance) =>
    <String, dynamic>{
      'regionId': instance.regionId,
      'departementId': instance.departementId,
      'sousPrefectureId': instance.sousPrefectureId,
      'localiteId': instance.localiteId,
    };

AddProductLocationRequest _$AddProductLocationRequestFromJson(
        Map<String, dynamic> json) =>
    AddProductLocationRequest(
      regionId: (json['regionId'] as num).toInt(),
      departementId: (json['departementId'] as num?)?.toInt() ?? 0,
      sousPrefectureId: (json['sprefectureId'] as num?)?.toInt() ?? 0,
      localiteId: (json['localiteId'] as num?)?.toInt() ?? 0,
    );

Map<String, dynamic> _$AddProductLocationRequestToJson(
        AddProductLocationRequest instance) =>
    <String, dynamic>{
      'regionId': instance.regionId,
      'departementId': instance.departementId,
      'sprefectureId': instance.sousPrefectureId,
      'localiteId': instance.localiteId,
    };

UpdateProductLocationRequest _$UpdateProductLocationRequestFromJson(
        Map<String, dynamic> json) =>
    UpdateProductLocationRequest()
      ..regionId = (json['regionId'] as num?)?.toInt()
      ..departementId = (json['departementId'] as num?)?.toInt()
      ..sousPrefectureId = (json['sprefectureId'] as num?)?.toInt()
      ..localiteId = (json['localiteId'] as num?)?.toInt();

Map<String, dynamic> _$UpdateProductLocationRequestToJson(
        UpdateProductLocationRequest instance) =>
    <String, dynamic>{
      'regionId': instance.regionId,
      'departementId': instance.departementId,
      'sprefectureId': instance.sousPrefectureId,
      'localiteId': instance.localiteId,
    };

DescriptionFile _$DescriptionFileFromJson(Map<String, dynamic> json) =>
    DescriptionFile(
      title: json['title'] as String?,
      description: json['description'] as String?,
    );

Map<String, dynamic> _$DescriptionFileToJson(DescriptionFile instance) =>
    <String, dynamic>{
      'title': instance.title,
      'description': instance.description,
    };

ProductShoppingCartsItem _$ProductShoppingCartsItemFromJson(
        Map<String, dynamic> json) =>
    ProductShoppingCartsItem(
      productId: (json['idProduct'] as num).toInt(),
      quantity: (json['quantity'] as num).toInt(),
      price: (json['price'] as num).toDouble(),
      fournisseurId: (json['fournisseurId'] as num).toInt(),
      speculationId: (json['speculationId'] as num).toInt(),
      productCode: json['productCode'] as String,
    );

Map<String, dynamic> _$ProductShoppingCartsItemToJson(
        ProductShoppingCartsItem instance) =>
    <String, dynamic>{
      'idProduct': instance.productId,
      'quantity': instance.quantity,
      'price': instance.price,
      'fournisseurId': instance.fournisseurId,
      'speculationId': instance.speculationId,
      'productCode': instance.productCode,
    };

AddProductShoppingCartsItem _$AddProductShoppingCartsItemFromJson(
        Map<String, dynamic> json) =>
    AddProductShoppingCartsItem(
      productId: (json['productId'] as num).toInt(),
      quantity: (json['quantity'] as num).toInt(),
      price: (json['price'] as num).toDouble(),
      fournisseurId: (json['fournisseurId'] as num).toInt(),
      speculationId: (json['speculationId'] as num).toInt(),
      productCode: json['productCode'] as String,
      paymentCode: json['paymentCode'] as String,
      unitTime: json['unitTime'] as String?,
      numberSlice: (json['numberSlice'] as num?)?.toInt(),
    );

Map<String, dynamic> _$AddProductShoppingCartsItemToJson(
        AddProductShoppingCartsItem instance) =>
    <String, dynamic>{
      'productId': instance.productId,
      'quantity': instance.quantity,
      'price': instance.price,
      'fournisseurId': instance.fournisseurId,
      'speculationId': instance.speculationId,
      'productCode': instance.productCode,
      'paymentCode': instance.paymentCode,
      'unitTime': instance.unitTime,
      'numberSlice': instance.numberSlice,
    };
