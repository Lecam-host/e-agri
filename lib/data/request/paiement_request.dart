class CreatePaiementRequest {
  int orderId, methodId;
  String? phone;

  CreatePaiementRequest({
    required this.orderId,
    required this.methodId,
    this.phone,
  });
}
