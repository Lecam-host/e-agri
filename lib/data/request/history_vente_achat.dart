class GetUserHistoryAchatVente {
  int userId;
  int page;
  int perPage;

  GetUserHistoryAchatVente({
    required this.userId,
    required this.page,
    required this.perPage,
  });
}
