// To parse this JSON data, do
//
//     final addIntrantModel = addIntrantModelFromJson(jsonString);

import 'dart:convert';
import 'dart:io';

AddIntrantRequestModel addIntrantModelFromJson(String str) =>
    AddIntrantRequestModel.fromJson(json.decode(str));

String addIntrantModelToJson(AddIntrantRequestModel data) =>
    json.encode(data.toJson());

class AddIntrantRequestModel {
  AddIntrantRequestModel({
    this.fournisseurId,
    this.intrantId,
    this.intrantCategorieId,
    this.certification,
    this.data,
    this.availability,
    this.availabilityDate,
    this.expirationDate,
    this.description,
    this.offerType,
    this.images,
  });

  int? fournisseurId;
  int? intrantId;
  int? intrantCategorieId, numberSlice;
  String? certification;
  List<DataAddIntrant>? data;
  String? availability;
  String? availabilityDate;
  String? expirationDate, unitTime;
  String? description;
  String? offerType;
  List<File>? images;

  factory AddIntrantRequestModel.fromJson(Map<String, dynamic> json) =>
      AddIntrantRequestModel(
        fournisseurId: json["fournisseurId"],
        intrantId: json["intrantId"],
        intrantCategorieId: json["intrantCategorieId"],
        certification: json["certification"],
        data: json["data"] == null
            ? []
            : List<DataAddIntrant>.from(
                json["data"]!.map((x) => DataAddIntrant.fromJson(x))),
        availability: json["availability"],
        availabilityDate: json["availabilityDate"],
        expirationDate: json["expirationDate"],
        description: json["description"],
        offerType: json["offerType"],
        images: json["images"] == null
            ? []
            : List<File>.from(json["images"]!.map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "fournisseurId": fournisseurId,
        "intrantId": intrantId,
        "intrantCategorieId": intrantCategorieId,
        "certification": certification,
        "data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
        "availability": availability,
        "availabilityDate": availabilityDate != null
            ? "${availabilityDate.toString().padLeft(4, '0')}-${availabilityDate.toString().padLeft(2, '0')}-${availabilityDate.toString().padLeft(2, '0')}"
            : null,
        "expirationDate": expirationDate != null
            ? "${expirationDate.toString().padLeft(4, '0')}-${expirationDate.toString().padLeft(2, '0')}-${expirationDate.toString().padLeft(2, '0')}"
            : null,
        "description": description,
        "offerType": offerType,
        "images":
            images == null ? [] : List<dynamic>.from(images!.map((x) => x)),
      };
}

class DataAddIntrant {
  DataAddIntrant({
    this.localisation,
    this.price,
    this.quantity,
    this.payment,
    this.unitOfMeasurment,
  });

  LocalisationAddIntrant? localisation;
  String? price;
  String? quantity;
  String? payment;
  int? unitOfMeasurment;

  factory DataAddIntrant.fromJson(Map<String, dynamic> json) => DataAddIntrant(
        localisation: json["localisation"] == null
            ? null
            : LocalisationAddIntrant.fromJson(json["localisation"]),
        price: json["price"],
        quantity: json["quantity"],
        payment: json["payment"],
        unitOfMeasurment: json["unitOfMeasurment"],
      );

  Map<String, dynamic> toJson() => {
        "localisation": localisation?.toJson(),
        "price": price,
        "quantity": quantity,
        "payment": payment,
        "unitOfMeasurment": unitOfMeasurment,
      };
}

class LocalisationAddIntrant {
  LocalisationAddIntrant({
    this.regionId,
    this.departementId,
    this.sprefectureId,
    this.localiteId,
  });

  int? regionId;
  int? departementId;
  int? sprefectureId;
  int? localiteId;

  factory LocalisationAddIntrant.fromJson(Map<String, dynamic> json) =>
      LocalisationAddIntrant(
        regionId: json["regionId"],
        departementId: json["departementId"],
        sprefectureId: json["sprefectureId"],
        localiteId: json["localiteId"],
      );

  Map<String, dynamic> toJson() => {
        "regionId": regionId,
        "departementId": departementId,
        "sprefectureId": sprefectureId,
        "localiteId": localiteId,
      };
}
