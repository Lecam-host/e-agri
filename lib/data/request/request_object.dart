import 'package:json_annotation/json_annotation.dart';

part 'request_object.g.dart';

@JsonSerializable()
class PriceSpeculationData {
  @JsonKey(name: "detail")
  int? prixDetail;
  @JsonKey(name: "wholesale")
  int? prixGros;
  @JsonKey(name: "regionId")
  int? regionId;

  @JsonKey(name: "departementId")
  int? departementId;
  @JsonKey(name: "sousPrefectureId")
  int? sousPrefectureId;
  @JsonKey(name: "localiteId")
  int? localiteId;
  PriceSpeculationData({
    this.prixDetail,
    this.prixGros,
    this.regionId,
    this.departementId,
    this.sousPrefectureId,
    this.localiteId,
  });
  factory PriceSpeculationData.fromJson(Map<String, dynamic> json) =>
      _$PriceSpeculationDataFromJson(json);

  Map<String, dynamic> toJson() => _$PriceSpeculationDataToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class AllPriceSpeculationData {
  @JsonKey(name: "speculationId")
  int? speculationId;

  @JsonKey(name: "userId")
  int? userId;

  @JsonKey(name: "unit")
  String? unit;

  @JsonKey(name: "source")
  String? source;

  @JsonKey(name: "from")
  String? from;

  @JsonKey(name: "to")
  String? to;
  @JsonKey(name: "official")
  bool? official;
  List<PriceSpeculationData>? data;
  AllPriceSpeculationData({
    this.from,
    this.official,
    this.speculationId,
    this.source,
    this.to,
    this.unit,
    this.userId,
    this.data,
  });
  factory AllPriceSpeculationData.fromJson(Map<String, dynamic> json) =>
      _$AllPriceSpeculationDataFromJson(json);

  Map<String, dynamic> toJson() => _$AllPriceSpeculationDataToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class LocationBodyId {
  @JsonKey(name: "regionId")
  int? regionId;

  @JsonKey(name: "departementId")
  int? departementId;

  @JsonKey(name: "sousPrefectureId")
  int? sousPrefectureId;

  @JsonKey(name: "localiteId")
  int? localiteId;

  LocationBodyId({
    this.regionId,
    this.departementId,
    this.sousPrefectureId,
    this.localiteId,
  });
  factory LocationBodyId.fromJson(Map<String, dynamic> json) =>
      _$LocationBodyIdFromJson(json);

  Map<String, dynamic> toJson() => _$LocationBodyIdToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class AddProductLocationRequest {
  @JsonKey(name: "regionId")
  int regionId;

  @JsonKey(name: "departementId")
  int? departementId;

  @JsonKey(name: "sprefectureId")
  int? sousPrefectureId;

  @JsonKey(name: "localiteId")
  int? localiteId;

  AddProductLocationRequest({
    required this.regionId,
    this.departementId = 0,
    this.sousPrefectureId = 0,
    this.localiteId = 0,
  });
  factory AddProductLocationRequest.fromJson(Map<String, dynamic> json) =>
      _$AddProductLocationRequestFromJson(json);

  Map<String, dynamic> toJson() => _$AddProductLocationRequestToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class UpdateProductLocationRequest {
  @JsonKey(name: "regionId")
  int? regionId;

  @JsonKey(name: "departementId")
  int? departementId;

  @JsonKey(name: "sprefectureId")
  int? sousPrefectureId;

  @JsonKey(name: "localiteId")
  int? localiteId;

  UpdateProductLocationRequest();
  factory UpdateProductLocationRequest.fromJson(Map<String, dynamic> json) =>
      _$UpdateProductLocationRequestFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateProductLocationRequestToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class DescriptionFile {
  @JsonKey(name: "title")
  String? title;

  @JsonKey(name: "description")
  String? description;

  DescriptionFile({
    this.title,
    this.description,
  });
  factory DescriptionFile.fromJson(Map<String, dynamic> json) =>
      _$DescriptionFileFromJson(json);

  Map<String, dynamic> toJson() => _$DescriptionFileToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class ProductShoppingCartsItem {
  @JsonKey(name: "idProduct")
  int productId;

  @JsonKey(name: "quantity")
  int quantity;

  @JsonKey(name: "price")
  double price;

  @JsonKey(name: "fournisseurId")
  int fournisseurId;

  @JsonKey(name: "speculationId")
  int speculationId;

  @JsonKey(name: "productCode")
  String productCode;

  ProductShoppingCartsItem({
    required this.productId,
    required this.quantity,
    required this.price,
    required this.fournisseurId,
    required this.speculationId,
    required this.productCode,
  });
  factory ProductShoppingCartsItem.fromJson(Map<String, dynamic> json) =>
      _$ProductShoppingCartsItemFromJson(json);

  Map<String, dynamic> toJson() => _$ProductShoppingCartsItemToJson(this);
  @override
  String toString() => toJson().toString();
}

@JsonSerializable()
class AddProductShoppingCartsItem {
  @JsonKey(name: "productId")
  int productId;

  @JsonKey(name: "quantity")
  int quantity;

  @JsonKey(name: "price")
  double price;

  @JsonKey(name: "fournisseurId")
  int fournisseurId;

  @JsonKey(name: "speculationId")
  int speculationId;

  @JsonKey(name: "productCode")
  String productCode;
  @JsonKey(name: "paymentCode")
  String paymentCode;
  @JsonKey(name: "unitTime")
  String? unitTime;
  @JsonKey(name: "numberSlice")
  int? numberSlice;

  AddProductShoppingCartsItem({
    required this.productId,
    required this.quantity,
    required this.price,
    required this.fournisseurId,
    required this.speculationId,
    required this.productCode,
    required this.paymentCode,
    this.unitTime,
    this.numberSlice,
  });
  factory AddProductShoppingCartsItem.fromJson(Map<String, dynamic> json) =>
      _$AddProductShoppingCartsItemFromJson(json);

  Map<String, dynamic> toJson() => _$AddProductShoppingCartsItemToJson(this);
  @override
  String toString() => toJson().toString();
}
// @JsonSerializable()
// class AddIntrantRequest {
//   @JsonKey(name: "fournisseurId")
//   int fournisseurId;

//   @JsonKey(name: "quantity")
//   int quantity;

//   @JsonKey(name: "price_u")
//   double price;

//   @JsonKey(name: "speculationId")
//   int speculationId;

//   @JsonKey(name: "description")
//   String description;

//   @JsonKey(name: "images")
//   List<File>? images;

//   @JsonKey(name: "pesticideId")
//   int pesticideId;
//   @JsonKey(name: "location")
//   Map<String, dynamic> location;

//   @JsonKey(name: "typeCulture")
//   int typeCulture;
//   @JsonKey(name: "paymentId")
//   int paymentId;

//   @JsonKey(name: "unitOfMeasurment")
//   String unitOfMeasurment;

//   @JsonKey(name: "offerType")
//   String offerType;

//   @JsonKey(name: "availabilityDate")
//   String? availabilityDate;
//   @JsonKey(name: "expirationDate")
//   String expirationDate;
//   @JsonKey(name: "certification")
//   String certification;
//   AddIntrantRequest({
//     required this.fournisseurId,
//     required this.quantity,
//     required this.price,
//     required this.speculationId,
//     required this.description,
//     this.images,
//     required this.location,
//     required this.pesticideId,
//     required this.typeCulture,
//     required this.paymentId,
//     required this.unitOfMeasurment,
//     required this.offerType,
//     this.availabilityDate,
//     required this.expirationDate,
//     required this.certification,
//   });
//   factory AddIntrantRequest.fromJson(Map<String, dynamic> json) =>
//       _$AddIntrantRequestFromJson(json);

//   Map<String, dynamic> toJson() => _$AddIntrantRequestToJson(this);
//   @override
//   String toString() => toJson().toString();
// }
