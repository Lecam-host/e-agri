import 'dart:io';

import 'package:eagri/data/request/request_object.dart';

class PaginateRequest {
  int? page;
  int? perPage;

  PaginateRequest({this.page, this.perPage});
}

class LoginRequest {
  String number;
  String password;

  LoginRequest(
    this.number,
    this.password,
  );
}

class SaveRegistrationIdRequest {
  String clientId;
  String registrationId;

  SaveRegistrationIdRequest(
    this.clientId,
    this.registrationId,
  );
}

class SearchAdviceRequest {
  List<int>? categoriesId;
  String? adviceType;
  List<String>? mediaType;
  String? keyword;
  int? userId;

  SearchAdviceRequest({
    this.categoriesId,
    this.adviceType,
    this.mediaType,
    this.keyword,
    this.userId,
  });
}

class AskAdviceRequest {
  List<int> entitiesId;
  List<int> categoriesId;
  int adviceType;
  List<int> mediaType;
  String description;
  int userId;

  AskAdviceRequest(
    this.entitiesId,
    this.categoriesId,
    this.adviceType,
    this.mediaType,
    this.description,
    this.userId,
  );
}

class GetWeatherRequest {
  double lat;
  double lon;

  GetWeatherRequest(
    this.lat,
    this.lon,
  );
}

class GetListAdviceRequest {
  int perPage;
  int pageNumber;

  GetListAdviceRequest(
    this.perPage,
    this.pageNumber,
  );
}

class GetListSpeculationPriceRequest {
  int pageNumber;
  int pageSize;

  GetListSpeculationPriceRequest(
    this.pageNumber,
    this.pageSize,
  );
}

class AddSpeculationPriceRequest {
  int? speculationId;
  int? userId;
  String? from;
  String? source;
  String? unit;
  String? to;
  bool? official;
  List<Map<String, dynamic>>? data;

  AddSpeculationPriceRequest({
    this.speculationId,
    this.userId,
    this.from,
    this.to,
    this.official,
    this.data,
    this.source,
    this.unit,
  });
}

class SpeculationPriceRequest {
  int? speculationId;
  int? userId;
  int? regionId;

  String? from;
  String? to;
  bool? official;

  SpeculationPriceRequest({
    this.speculationId,
    this.userId,
    this.regionId,
    this.from,
    this.to,
    this.official,
  });
}

class CompareSpeculationPriceRequest {
  int? speculationId;
  String? from;
  String? to;
  List<Map<String, dynamic>>? locationByid;

  bool? userType;

  CompareSpeculationPriceRequest({
    this.speculationId,
    this.from,
    this.to,
    this.userType,
    this.locationByid,
  });
}

class CreateParcelRequest {
  int? userId;
  double? lat;
  double? lon;
  String? name;
  String? createAt;

  CreateParcelRequest({
    this.userId,
    this.lat,
    this.lon,
    this.name,
    this.createAt,
  });
}

class HistoryWeatherRequest {
  String? dateDeDebut;
  String? dateDeDeFin;
  double? lat;
  double? lon;

  HistoryWeatherRequest({
    this.dateDeDeFin,
    this.dateDeDebut,
    this.lat,
    this.lon,
  });
}

class AddAlertRequest {
  int userId;
  int regionId;
  int departementId;
  int sousprefectureId;
  int localiteId;

  int speculationId;

  String description;
  int domain;
  List<File> files;

  AddAlertRequest({
    required this.userId,
    required this.description,
    required this.speculationId,
    required this.domain,
    required this.files,
    required this.regionId,
    required this.departementId,
    required this.sousprefectureId,
    required this.localiteId,
  });
}

class GetMinAndMaxSpeculationnPriceRequest {
  int speculationId;
  bool official;

  GetMinAndMaxSpeculationnPriceRequest({
    required this.speculationId,
    required this.official,
  });
}

class GetRecommandationRequest {
  int? speculationId;
  double temperature;
  double windSpeed;
  double precipitation;
  int humidity;

  GetRecommandationRequest({
    this.speculationId,
    required this.temperature,
    required this.windSpeed,
    required this.precipitation,
    required this.humidity,
  });
}

class AddAdviceRequest {
  String adviceType;
  String title;
  String categoryId;
  String? adviceText;
  File? file1;
  File? file2;
  File? file3;
  File? illustrationImage;
  List<DescriptionFile>? descriptionFiles;
  String description;
  int userIdForCreation;
  AddAdviceRequest({
    required this.adviceType,
    required this.title,
    required this.categoryId,
    this.adviceText,
    this.file1,
    this.file2,
    this.file3,
    this.illustrationImage,
    required this.description,
    required this.userIdForCreation,
  });
}

class GetUserAskAdviceRequest extends PaginateRequest {
  int? userId;
  int? statutAsk;

  GetUserAskAdviceRequest({
    this.userId,
    this.statutAsk,
  });
}

class GetUserAskAdviceAnswerRequest {
  int askId;

  GetUserAskAdviceAnswerRequest({
    required this.askId,
  });
}

class InfoByIdRequest {
  int? regionId;
  int? departementId;
  int? localiteId;
  int? sprefectureId;
  int? userId;
  int? speculation;

  InfoByIdRequest({
    this.regionId,
    this.departementId,
    this.localiteId,
    this.sprefectureId,
    this.userId,
    this.speculation,
  });
}
