import 'dart:io';

import 'request_object.dart';

class AddProductOnMarketrequest {
  int fournisseurId, speculationId, price, quantity, paymentId, categorieId;
  String typeVente, availability, description, unitOfMeasurment, offerType;

  AddProductLocationRequest location;
  String? availabilityDate;
  String? unitTime;
  int? numberSlice;
  String origin;
double? pricePreOrder;
  List<File>? images;
  AddProductOnMarketrequest({
    required this.fournisseurId,
    required this.speculationId,
    required this.price,
    required this.quantity,
    required this.typeVente,
    required this.availability,
    required this.description,
    required this.unitOfMeasurment,
    required this.location,
    required this.offerType,
    required this.categorieId,
    this.availabilityDate,
    this.images,
    this.unitTime,
    this.numberSlice,
    required this.paymentId,
    this.origin="MOBILE",
    this.pricePreOrder,


  });
}

class UpdateProductOnMarketrequest {
  int idProduct;
  int? fournisseurId,
      speculationId,
      price,
      quantity,
      paymentId,
      departementId,
      regionId,
      sousPrefectureId,
      locationId;
  String? typeVente, availability, description, unitOfMeasurment, offerType;
  String? unitTime;
  int? numberSlice;
  UpdateProductLocationRequest? location;
  String? availabilityDate;
  List<File>? images;
  UpdateProductOnMarketrequest({
    this.fournisseurId,
    this.speculationId,
    this.price,
    this.quantity,
    this.typeVente,
    this.availability,
    this.description,
    this.unitOfMeasurment,
    this.departementId,
    this.regionId,
    this.sousPrefectureId,
    this.locationId,
    this.offerType,
    this.availabilityDate,
    this.images,
    this.paymentId,
    required this.idProduct,
    this.unitTime,
    this.numberSlice,
  });
}

class AddProductInShoppingCartsRequest {
  int userId;
  List<AddProductShoppingCartsItem> items;
  AddProductInShoppingCartsRequest({
    required this.userId,
    required this.items,
  });
}

class SearchProductRequest {
  int? regionId;
  int? departementId;
  int? locationId;
  int? sprefectureId;
  List<int>? speculationId;
  String? offerType;
  String? status;
  double? priceMin;
  double? priceMax;
  double? noteMax;
  double? noteMin;
  int? codeTypeProduit;
  int? quantityMax;
  int? quantityMin;

  int? fournisseurId;
  int limit;
  int page;

  String? publicationDateMin;
  String? publicationDateMax;
  int? categorieId;
  String? unitOfMeasurment;

  SearchProductRequest({
    this.regionId,
    this.departementId,
    this.locationId,
    this.sprefectureId,
    this.speculationId,
    this.priceMin,
    this.priceMax,
    this.noteMax,
    this.noteMin,
    this.quantityMax,
    this.quantityMin,
    this.fournisseurId,
    this.offerType,
    this.status,
    this.publicationDateMin,
    this.publicationDateMax,
    this.codeTypeProduit,
    required this.limit,
    required this.page,
    this.categorieId,
    this.unitOfMeasurment,
  });
}

class ConsultationProductRequest {
  int productId;
  DateTime date;

  ConsultationProductRequest({
    required this.date,
    required this.productId,
  });
}



class GetProductCommanderRequest {
  int fournisseurId;
  int page;
  int perPage;
  String? sortBy;
  String? identifiant;




  GetProductCommanderRequest({

    required this.fournisseurId,
    required this.page,
    required this.perPage,
    this.sortBy,
    this.identifiant,
  });
}
