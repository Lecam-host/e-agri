import 'dart:io';

import 'package:eagri/data/request/request_object.dart';

class AddTransportRequest {
  int fournisseurId, vehiculeId, price, paymentId, categorieId, capacite;
  String availability, description, unitOfMeasurment;
  AddProductLocationRequest locationStart;
  AddProductLocationRequest locationEnd;

  String? availabilityDate;
  String expirationDate, matricule;
  String? hour;
  List<File>? images;
  AddTransportRequest({
    required this.fournisseurId,
    required this.vehiculeId,
    required this.price,
    required this.availability,
    required this.description,
    required this.unitOfMeasurment,
    required this.locationStart,
    required this.locationEnd,
    required this.categorieId,
    required this.matricule,
    this.availabilityDate,
    required this.capacite,
    required this.expirationDate,
    this.images,
    this.hour,
    required this.paymentId,
  });
}

class UpdateTransportRequest {
  int fournisseurId;
  int idProduct;

  int? price;
  int? paymentId;
  int? capacite;
  String? availability;
  String? description;
  String? unitOfMeasurment;
  AddProductLocationRequest? locationStart;
  AddProductLocationRequest? locationEnd;

  String? availabilityDate;
  String? expirationDate, matricule;
  String? hour;
  UpdateTransportRequest({
    required this.fournisseurId,
    required this.idProduct,
    this.price,
    this.availability,
    this.description,
    this.unitOfMeasurment,
    this.locationStart,
    this.locationEnd,
    this.matricule,
    this.availabilityDate,
    this.capacite,
    this.expirationDate,
    this.hour,
    this.paymentId,
  });
}

class DemandeTransportRequest {
  int userId;
  int idProduct;

  int capacity;

  String date;
  String unitOfMeasurment;
  String? description;
  String hour;
  String marchandise;

  DemandeTransportRequest({
    required this.userId,
    required this.idProduct,
    required this.marchandise,
    required this.capacity,
    required this.date,
    required this.unitOfMeasurment,
    this.description,
    required this.hour,
  });
}
