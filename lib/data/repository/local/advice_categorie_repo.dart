import 'package:eagri/data/responses/local/local_responses.dart';
import 'package:eagri/data/responses/responses.dart';
import 'package:sqflite/sql.dart';

import '../../db/data_base_helpers.dart';
import '../../db/table_name.dart';
import '../../responses/json_properties_name.dart';

class AdviceCategoriesRepo {
  AdviceCategorieLocalResponse getEntity() {
    return AdviceCategorieLocalResponse();
  }

  Future<bool> delete() async {
    var dbClient = await DatabaseHelper.database;

    await dbClient.delete(
      TableName.adviceCategorieTable,
    );
    return true;
  }

  Future<bool> insert(AdviceCategoryResponse item) async {
    var dbClient = await DatabaseHelper.database;

    await dbClient.insert(
      TableName.adviceCategorieTable,
      item.toJson(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return true;
  }

  insertMore(List<AdviceCategoryResponse> items) async {
    if (items.isNotEmpty) {
      await delete();
    }
    for (var element in items) {
      insert(element);
    }
  }

  Future<List<AdviceCategorieLocalResponse>> getAllItem() async {
    var dbClient = await DatabaseHelper.database;
    const sql =
        '''SELECT  DISTINCT ${JsonPropertiesName.id},${JsonPropertiesName.description},${JsonPropertiesName.label},${JsonPropertiesName.code} FROM  ${TableName.adviceCategorieTable}  ORDER BY  ${JsonPropertiesName.label} DESC''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return AdviceCategorieLocalResponse.fromJson(data[i]);
    });
  }
}
