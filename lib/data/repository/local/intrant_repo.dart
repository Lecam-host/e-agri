import 'dart:convert';

import 'package:eagri/data/mapper/mapper.dart';
import 'package:eagri/domain/model/intrant_model.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:sqflite/sqflite.dart';

import '../../db/data_base_helpers.dart';
import '../../db/helpers_db.dart';
import '../../db/table_name.dart';
import '../../responses/responses.dart';

class IntrantRepo {
  IntrantRepo getEntity() {
    return IntrantRepo();
  }

  Future<bool> deleteAll() async {
    var dbClient = await DatabaseHelper.database;

    await dbClient.delete(
      TableName.speculationTable,
    );
    return true;
  }

  insertAll(List<ItemIntrant> items) async {
    // print("ooooooooooooooooo");
    // inspect(items);
    // print("aaaaaaaaaaaaaaaaaa");

    for (var element in items) {
      saveItem(element);
    }
  }

  Future<bool> isExist(int id) async {
    var dbClient = await DatabaseHelper.database;
    String sql =
        '''SELECT  * from ${TableName.speculationTable} where  ${ColumnsPropertiesName.apiId} = $id''';

    final data = await dbClient.rawQuery(sql);

    return data.isEmpty ? false : true;
  }

  saveItem(ItemIntrant item) async {
    isExist(item.id!).then((value) {
      if (value == false) {
        insertItem(item);
      } else {
        update(item);
      }
    });
  }

  insertItem(ItemIntrant item) async {
    var dbClient = await DatabaseHelper.database;

    Map<String, Object?> data = {
      ColumnsPropertiesName.apiId: item.id,
      ColumnsPropertiesName.data: json.encode(item.toJson()),
    };
    await dbClient
        .insert(
          TableName.speculationTable,
          data,
          conflictAlgorithm: ConflictAlgorithm.replace,
        )
        .then((value) {});
  }

  update(ItemIntrant item) async {
    var dbClient = await DatabaseHelper.database;

    Map<String, Object?> data = {
      ColumnsPropertiesName.apiId: item.id,
      ColumnsPropertiesName.data: json.encode(item.toJson())
    };
    await dbClient
        .update(TableName.speculationTable, data,
            conflictAlgorithm: ConflictAlgorithm.replace,
            where: "${ColumnsPropertiesName.apiId} = ${item.id}")
        .then((value) {});
  }

  Future<List<SpeculationModel>> getAllItem() async {
    var dbClient = await DatabaseHelper.database;
    const sql = '''SELECT  * from ${TableName.speculationTable}''';
    final data = await dbClient.rawQuery(sql);
    if (data.isNotEmpty) {
      return List.generate(data.length, (i) {
        SpeculationResponse item = SpeculationResponse.fromJson(
          json.decode(data[i][ColumnsPropertiesName.data] as String),
        );

        return item.toDomain();
      });
    } else {
      return [];
    }
  }

  Future<SpeculationModel?> getSpecificSpeculation(int id) async {
    var dbClient = await DatabaseHelper.database;
    var sql =
        '''SELECT  * from ${TableName.speculationTable} where  ${ColumnsPropertiesName.apiId} = $id''';
    final data = await dbClient.rawQuery(sql);

    List<SpeculationModel> listSpec = List.generate(
      data.length,
      (i) {
        SpeculationResponse item = SpeculationResponse.fromJson(
            json.decode(data[i][ColumnsPropertiesName.data] as String));

        return item.toDomain();
      },
    );

    return listSpec.isNotEmpty ? listSpec[0] : null;
  }
}
