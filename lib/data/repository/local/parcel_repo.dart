import 'dart:convert';

import 'package:eagri/data/db/table_name.dart';
import 'package:eagri/data/request/request.dart';
import 'package:sqflite/sqflite.dart';

import '../../db/data_base_helpers.dart';
import '../../responses/local/local_responses.dart';

class ParcelRepo {
  ParcelRepo getEntity() {
    return ParcelRepo();
  }

  Future<bool> deleteAll() async {
    var dbClient = await DatabaseHelper.database;

    await dbClient.delete(
      TableName.parcelTable,
    );
    return true;
  }

  Future<bool> deleteSpecificElement() async {
    var dbClient = await DatabaseHelper.database;

    await dbClient.delete(
      TableName.parcelTable,
    );
    return true;
  }

  Future<bool> insert(CreateParcelRequest item) async {
    var dbClient = await DatabaseHelper.database;
    ParcelRequestJson parcelRequest = ParcelRequestJson();

    parcelRequest.lat = item.lat;
    parcelRequest.lon = item.lon;
    parcelRequest.name = item.name;
    parcelRequest.createAt = DateTime.now().toString();

    CreateParcelRequestJson data = CreateParcelRequestJson();
    data.data = json.encode(parcelRequest);
    await dbClient
        .insert(
          TableName.parcelTable,
          data.toJson(),
          conflictAlgorithm: ConflictAlgorithm.replace,
        )
        .then((value) {});
    return true;
  }

  Future<bool> findParcelWithCoordonnates(CreateParcelRequest item) async {
    bool find = false;

    await getAllItem().then((value) {
      for (var element in value) {
        if (element.lat == item.lat && element.lon == item.lon) {
          find = true;
          break;
        }
      }
    });
    return find;
  }

  Future<List<CreateParcelRequest>> getAllItem() async {
    var dbClient = await DatabaseHelper.database;
    const sql = '''SELECT  * from ${TableName.parcelTable}''';
    final data = await dbClient.rawQuery(sql);

    List<CreateParcelRequestJson> list = List.generate(data.length, (i) {
      return CreateParcelRequestJson.fromJson(data[i]);
    });
    return List.generate(list.length, (i) {
      ParcelRequestJson parcelInfo =
          ParcelRequestJson.fromJson(json.decode(list[i].data!));
      return CreateParcelRequest(
          lat: parcelInfo.lat,
          lon: parcelInfo.lon,
          name: parcelInfo.name,
          userId: 1,
          createAt: parcelInfo.createAt);
    });
  }
}
