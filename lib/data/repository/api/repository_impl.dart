import 'dart:developer';
import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:eagri/data/mapper/mapper.dart';
import 'package:eagri/data/mapper/location_mapper.dart';
import 'package:eagri/data/mapper/weather_mapper.dart';
import 'package:eagri/data/mapper/advice_mapper.dart';
import 'package:eagri/data/mapper/alert_mapper.dart';
import 'package:eagri/data/mapper/shop_mapper.dart';
import 'package:eagri/data/request/history_vente_achat.dart';
import 'package:eagri/data/request/notation_request.dart';
import 'package:eagri/data/request/paiement_request.dart';
import 'package:eagri/data/request/prestation_request.dart';
import 'package:eagri/data/request/request_object.dart';
import 'package:eagri/data/request/request_offre_credit_model.dart';

import 'package:eagri/data/request/shop_resquests.dart';
import 'package:eagri/data/request/transport_resquest.dart';

import 'package:eagri/data/responses/responses.dart';
import 'package:eagri/domain/model/Product.dart';

import 'package:eagri/domain/model/advice_model.dart';
import 'package:eagri/domain/model/alert_model.dart';
import 'package:eagri/domain/model/alerte_model.dart';
import 'package:eagri/domain/model/category_model.dart';
import 'package:eagri/domain/model/command_model.dart';

import 'package:eagri/domain/model/creditor_model.dart';
import 'package:eagri/domain/model/insurer_model.dart';
import 'package:eagri/domain/model/offre_credit_model.dart';
import 'package:eagri/domain/model/favorite_model.dart';
import 'package:eagri/domain/model/info_by_id_model.dart';
import 'package:eagri/domain/model/offre_insurance_model.dart';
import 'package:eagri/domain/model/intrant_model.dart';
import 'package:eagri/domain/model/location_model.dart';
import 'package:eagri/domain/model/paiement_model.dart';
import 'package:eagri/domain/model/prestation_model.dart';
import 'package:eagri/domain/model/shop_model.dart';
import 'package:eagri/domain/model/transport_model.dart';
import 'package:eagri/domain/model/typeCulture_model.dart';
import 'package:eagri/domain/model/type_offre__insurance_model.dart';
import 'package:eagri/domain/model/type_offre_credit_model.dart';

import '../../../domain/model/achat_or_preachat_model.dart';
import '../../../domain/model/cart_model.dart';
import '../../../domain/model/history_achat_model.dart';
import '../../../domain/model/history_vente_model.dart';
import '../../../domain/model/insurance_by_insurer_model.dart';
import '../../../domain/model/mes_produits_commander_model.dart';
import '../../../domain/model/methode_paiement_model.dart';
import '../../../domain/model/model.dart';
import '../../../domain/model/odered_product_credit_model.dart';
import '../../../domain/model/offfre_item_model.dart';
import '../../../domain/model/rate_model.dart';
import '../../../domain/model/user_info_model.dart';
import '../../../domain/model/user_model.dart';
import '../../../domain/model/user_stat_model.dart';
import '../../../domain/model/weather_model.dart';
import '../../data_source/remote_data_source.dart';
import '../../network/error_handler.dart';
import '../../network/failure.dart';
import '../../network/network_info.dart';
import '../../request/intran_request.dart';
import '../../request/request.dart';
import '../../request/request_offre_insurance_model.dart';
import '../../responses/account_response.dart';
import '../../responses/shop_response.dart';
import '../local/advice_categorie_repo.dart';

class RepositoryImpl {
  RepositoryImpl(this.remoteDataSource, this.networkInfo);
  AdviceCategoriesRepo adviceCategoriesRepo = AdviceCategoriesRepo();
  RemoteDataSource remoteDataSource;
  NetworkInfo networkInfo;
  // sendNotifFn(
  //   String error,
  //   String message,
  //   String title,
  // ) {
  //   sendNotif("8,9,3", error, ["lecam.kedja@mobisoft.ci"], title, message,
  //       DateTime.now().toString());
  // }

  Future<Either<Failure, AuthModel>> login(LoginRequest loginRequest) async {
    //  sendNotifFn("test", "login", "login title");
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.login(loginRequest);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, ListAdviceModel>> getAllAdvice(
      GetListAdviceRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getAllAdvice(request);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, AdviceModel>> detailAdvice(int adviceId) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.detailAdvice(adviceId);
        if (response.status == ResponseCode.success) {
          return Right(response.advice!.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, ListAdviceCategoryModel>>
      getAllAdviceCategory() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getAllAdviceCategorie();
        if (response.status == ResponseCode.success) {
          // adviceCategoriesRepo.insertMore(response.data!);
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, ListAdviceModel>> searchAdvice(
      SearchAdviceRequest searchAdviceRequest) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.searchAdvice(searchAdviceRequest);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, AskAdviceResponse>> askAdvice(
      AskAdviceRequest askAdviceRequest) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.askAdvice(askAdviceRequest);
        if (response.status == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, CityWeatherModel>> getCurrentWeather(
      GetWeatherRequest location) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getCurrentWeather(location);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, ListSpeculationPriceModel>> getListSpeculationPrice(
      GetListSpeculationPriceRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.getListSpeculationPrice(request);

        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, ListSpeculationPriceModel>>
      getListSpeculationPriceEnterByUser(
          GetListSpeculationPriceRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.getListSpeculationPriceEnterByUser(request);
        if (response.status == ResponseCode.success) {
          return Right(response.data!.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> addSpeculationPrice(
      AddSpeculationPriceRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.addSpeculationPrice(request);

        if (response.status == ResponseCode.created) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, ListRegionModel>> getListRegion() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getListRegion();
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, ListSpeculationModel>> getListSpeculation() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getListSpeculation();
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, ListSpeculationPriceModel>> getSpeculationPriceHistory(
      SpeculationPriceRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.getSpeculationPriceHistory(request);

        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, ListDepartementModel>> getListDepartementOfRegion(
      int idregion) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.getListDepartementOfRegion(idregion);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, ListLocaliteModel>> getListLocaliteOfSp(
      int idSp) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.getListLocaliteOfSousPrefecture(idSp);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, ListSousPrefectureModel>> getListSpOfDepartement(
      int idDepartement) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource
            .getListSousPrefectureOfDepartement(idDepartement);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, ListSpeculationPriceModel>> compareSpeculationPrice(
      CompareSpeculationPriceRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.compareSpeculationPrice(request);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, ParcelInfoModel>> createParcel(
      CreateParcelRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.createParcel(request);
        if (response.status == ResponseCode.success ||
            response.status == ResponseCode.parcelExist) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, CreateFullParcelResponseModel>> createFullParcel(
      List<CreateParcelRequest> request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.createFullParcel(request);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, ParcelInfoModel>> getParcelInfo(
      String parcelId) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getParcelInfo(parcelId);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, ListHistoryWeatherModel>> historyWeather(
      HistoryWeatherRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.historyWeather(request);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, AllConfigModel>> getAllConfig() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getAllConfig();
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, AlertModel>> addAlert(AddAlertRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.addAlert(request);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  //get alerte
  Future<Either<Failure, AlerteModel>> getListAlert() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getListAlert();
        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode ?? ApiInternalStatus.failure,
              response.statusMessage ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        return (Left(ErrorHandler.handle(error).failure));
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, ListAskAdviceModel>> getListAskAdvice(
      GetUserAskAdviceRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getListAskAdviceOfUser(request);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, ListLocationForWeatherModel>>
      getListLocationForWeather() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getListLocationForWeather();
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, ListParcelModel>> getUserListParcel(int userId) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getUserParcelle(userId);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, UserModel>> getUserInfo(int id) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getUserInfo(id);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, AccountInfoResponse>> getAccountInfoByToken() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getAccountInfoByToken();
        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, MinAndMaxSpeculationPriceModel?>> getMinMaxPrice(
      GetMinAndMaxSpeculationnPriceRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.getMinAndMaxSpeculationPrice(request);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, List<RecommandationModel>?>> getListRecommandation(
      GetRecommandationRequest request) async {
    if (await networkInfo.isConnected) {
      final response = await remoteDataSource.getListRecommandation(request);
      if (response.status == ResponseCode.success) {
        return Right(response.toDomain());
      } else {
        return Left(
          Failure(
            response.status ?? ApiInternalStatus.failure,
            response.message ?? ResponseMessage.defaultResponse,
          ),
        );
      }
      // try {

      // } catch (error) {
      //   if (error is DioException) {
      //     DioException erreur = error;
      //     if (erreur.response != null) {
      //       if (erreur.response!.data != null) {
      //         return (Left(
      //           ErrorHandler.handle(error).failure
      //             ..message = erreur.response!.data!["statusMessage"] ??
      //                 ResponseMessage.defaultResponse
      //             ..code = erreur.response!.data!["statusCode"] ??
      //                 ApiInternalStatus.failure,
      //         ));
      //       } else {
      //         return (Left(ErrorHandler.handle(error).failure));
      //       }
      //     } else {
      //       return (Left(ErrorHandler.handle(error).failure));
      //     }
      //   } else {
      //     return (Left(ErrorHandler.handle(error).failure));
      //   }
      // }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> addAdvice(
      AddAdviceRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.addAdVice(request);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, ListAdviceModel>> getUserAskAdviceAnswer(
      GetUserAskAdviceAnswerRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getUserAskAdviceAnswer(request);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> addProductOnMarket(
      AddProductOnMarketrequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.addProductOnMarket(request);
        if (response.status == ResponseCode.created) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, Future<ProductModel?>>> updateProductOnMarket(
      UpdateProductOnMarketrequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.updateProductOnMarket(request);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, List<String>>> getListOffersType() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getListOffersType();
        if (response.status == ResponseCode.success) {
          return Right(response.data!);
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, List<String>>> getListTypeVente() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getListTypeVente();
        if (response.status == ResponseCode.success) {
          return Right(response.data!);
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> addAlertFiles(
      int idAlert, List<File> file) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.addAlertFiles(idAlert, file);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, Future<List<ProductModel>>>> getListProducts() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getListProduct();
        if (response.status == ResponseCode.success) {
          return Right(response.data.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, List<ConfigModel>>> getAlertDomain() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getAlertDomain();
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, DataProductUserResponse>> getListProductsFournisseur(
      int id) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getListProductFournisseur(id);
        if (response.status == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetAllProductResponse>> searchProduct(
      SearchProductRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.searchProduct(request);
        if (response.status == ResponseCode.success) {
          return Right(response.data!);
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetListIntrantResponse>> searchProductIntrant(
      SearchProductRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.searchProductIntrant(request);
        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(response.statusCode, response.statusMessage),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, List<ProductShoppingCartsItem>>>
      addProductInShoppingCart(AddProductInShoppingCartsRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.addProductInShoppingCart(request);
        if (response.status == ResponseCode.created) {
          return Right(response.data.cartItems);
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, InfoByIdModel>> getInfoById(
      InfoByIdRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getInfoById(request);
        return Right(response);
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, ListCartResponse>> getUserCart(int userId) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getUserCart(userId);

        if (response.status == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> deleteCatalogueProduct(
      int idProduct) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.deleteCatalogueProduct(idProduct);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> addRate(
      AddRateRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.addRate(request);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, CheckRateResponse>> checkRating(
      CheckRateRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.checkRating(request);

        return Right(response);
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, CreateCommandModel>> createCommand(
      int userId, int cartId) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.createCommand(userId, cartId);
        if (response.statusCode == ResponseCode.created) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetUserCommands>> getUserCommand(int userId) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getUserCommand(userId);
        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> deleteProdInCart(
      int cartId, int itemId) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.deleteProdIncart(cartId, itemId);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, CreatePaiementResponse>> createPaiement(
      CreatePaiementRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.createPaiement(request);
        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, CreatePaiementResponse>> createPaiementCredit(
      String reference, int orderId, String motif, String phone) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.createPaiementCredit(
          reference,
          orderId,
          motif,
          phone,
        );
        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, ConfirmPaymentResponse>> confirmPaiement(
      String signature, otp) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.confirmPaiement(signature, otp);
        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, Future<ProductModel?>>> getDetailProduct(
      int idProduct) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getDetailProduct(idProduct);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, VerificationPaymentResponse>> verificationPayment(
      String signature) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.verificationPayment(signature);
        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, MethodPaiementModel>> getMethodPaiementAll() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getMethodPaiementAll();
        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode ?? ApiInternalStatus.failure,
              response.statusMessage ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, AddUserFavoriteResponse>> addFavorite(
      List<int> speculationIds) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.addFavorite(speculationIds);
        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, ListUserFavoriteResponse>> getUserFavorite() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getUserFavorite();
        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetCommandFactureResponse>> getCommanFacture(
      int orderId) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getCommandFacture(orderId);
        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetListCommentOnProductResponse>> getProductComment(
      ProductListCommentRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getProductComment(request);
        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseResponse>> notifyConsultationProduct(
      ConsultationProductRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.notifyConsultaionProduct(request);
        if (response.status == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.status ?? 1,
              response.message ?? "",
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, HistoryAchatResponse>> getUserHistoryAchat(
      GetUserHistoryAchatVente request) async {
    if (await networkInfo.isConnected) {
      final response = await remoteDataSource.getUserHistoryAchat(request);
      if (response.statusCode == ResponseCode.success) {
        return Right(response);
      } else {
        return Left(
          Failure(
            response.statusCode,
            response.statusMessage,
          ),
        );
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, HistoryVenteResponse>> getUserHistoryVente(
      GetUserHistoryAchatVente request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getUserHistoryVente(request);
        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        inspect(error);
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetListCategoryProductResponse>>
      getListCategoryProduct() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getListCategoryProduct();
        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetNbOffreAchatBySpeculation>>
      getListNbOfferAchatBySpeculation() async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.getListNbOfferAchatBySpeculation();
        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetListOffreAchatFind>> getListOffreAchatFind(
      int idProduct) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.getListOffreAchatFind(idProduct);
        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> deleteCommand(
      int commandId) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.deleteCommand(commandId);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? 1,
              response.message ?? "",
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> deleteFavorite(
      int favoriteId) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.deleteFavorite(favoriteId);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? 1,
              response.message ?? "",
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, Future<ProductModel>>> disableProduct(
      int idProduct) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.disableProduct(idProduct);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? 1,
              response.message ?? "",
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, Future<ProductModel>>> enableProduct(
      int idProduct) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.enableProduct(idProduct);
        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? 1,
              response.message ?? "",
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> addIntrant(
      AddIntrantRequestModel request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.addIntrant(request);
        if (response.status == ResponseCode.created) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? 1,
              response.message ?? "",
            ),
          );
        }
      } catch (error) {
        log(error.toString());
        if (error is DioException) {
          log(error.toString());
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, TypeCultureModel>> getTypeCulturesAll() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getTypeCulturesAll();
        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode ?? 1,
              response.statusMessage ?? "",
            ),
          );
        }
      } catch (error) {
        log(error.toString());
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, IntrantModel>> getIntrantAll() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getIntrantAll();
        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(Failure(
            response.statusCode ?? 1,
            response.statusMessage ?? "",
          ));
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, DataProductUserResponse>>
      getListProductsFournisseurWithCode(int id, int code) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.getListProductFournisseurWithCode(id, code);
        if (response.status == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetListCategoryPrestation>>
      getListCategoryPrestation() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getListCategoryPrestation();
        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> addPrestion(
      AddPrestationRequest addPrestationRequest) async {
    inspect(addPrestationRequest);
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.addPrestation(addPrestationRequest);

        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? 1,
              response.message ?? "",
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> demandePrestion(
      DemandePrestionRequest request) async {
    inspect(request);
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.demandePrestation(request);

        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? 1,
              response.message ?? "",
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetListPrestationResponse>> getDemandePrestationRecu(
      int userId) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.getDemandePrestationRecu(userId);

        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        inspect(error);

        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetListPrestationResponse>> getDemandePrestationSend(
      int userId) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.getDemandePrestationSend(userId);

        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        inspect(error);
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> validateDemandePrestation(
      int prestationId) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.validateDemandePrestation(prestationId);

        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? 1,
              response.message ?? "",
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetUniteResponse>> getListPrestationUnite() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getListPrestationUnite();

        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> updatePrestation(
      UpdatePrestationRequest updatePrestationRequest) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.updatePrestation(updatePrestationRequest);

        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? 1,
              response.message ?? "",
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetListCarResponse>> getListCar() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getListCar();

        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        inspect(error);
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetUniteResponse>> getTransportUnits() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getListTransportUnite();

        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> addTransport(
      AddTransportRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.addTransport(request);

        if (response.status == ResponseCode.created) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? 1,
              response.message ?? "",
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> updateTransport(
      UpdateTransportRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.updateTransport(request);

        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? 1,
              response.message ?? "",
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> reserveTransport(
      DemandeTransportRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.reserveTransport(request);

        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? 1,
              response.message ?? "",
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> saveRegistrationId(
      SaveRegistrationIdRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.saveRegistrationId(request);

        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? 1,
              response.message ?? "",
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetDemandeTransport>> getListDemandeTransportSend(
      int userId) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.getListDemandeTransportEnvoye(userId);

        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(response.statusCode, response.statusMessage),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetDemandeTransport>> getListDemandeTransportRecu(
      int userId) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.getListDemandeTransportRecu(userId);

        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(response.statusCode, response.statusMessage),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> cancelDemandeTransport(
      int reservationId) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.cancelDemandeTransport(reservationId);

        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? 1,
              response.message ?? "",
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> validDemandeTransport(
      int fournisseurId, int reservationId) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.validDemandeTransport(
            fournisseurId, reservationId);

        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? 1,
              response.message ?? "",
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> validChargement(
      int reservationId, String personal, String code) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.validChargement(
            reservationId, personal, code);

        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? 1,
              response.message ?? "",
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetCategoryVehicule>>
      getListCategorieVehicule() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getListCategorieVehicule();

        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> acceptPaiementCash(
      int offerId, int productId, bool typeResponse, String? message) async {
    inspect(offerId);
    inspect(productId);
    inspect(typeResponse);
    inspect(message);

    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.acceptPaiementCash(
            offerId, productId, typeResponse, message);

        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? 1,
              response.message ?? "",
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> confirmPaiementCash(
      int demandeId, String confirmationCode) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.confirmPaiementCash(
            demandeId, confirmationCode);

        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? 1,
              response.message ?? "",
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetListDemandePaiementCashItem>>
      getListDemandePaimentCash(String role) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getListDemandePaimentCash(role);

        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(response.statusCode, response.statusMessage),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> validContractPdf(
      int userId, int offerId, bool agreement) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.validContractPdf(userId, offerId, agreement);

        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? 1,
              response.message ?? "",
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, Future<ProductModel?>>> updateProductIntrant(
      UpdateProductOnMarketrequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.updateProductIntrant(request);

        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? 1,
              response.message ?? "",
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetIntrantCategoryResponse>>
      getIntrantCategories() async {
    if (await networkInfo.isConnected) {
      final response = await remoteDataSource.getIntrantCategorie();

      if (response.statusCode == ResponseCode.success) {
        return Right(response);
      } else {
        return Left(
          Failure(
            response.statusCode,
            response.statusMessage,
          ),
        );
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetUniteResponse>> geIntrantUnits() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getIntrantUnite();

        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.statusCode,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> deleteFirebaseToken(
      String userName, String firebaseToken) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.deleteFirebaseToken(userName, firebaseToken);

        if (response.status == ResponseCode.success) {
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              response.status ?? 1,
              response.message ?? "",
            ),
          );
        }
      } catch (error) {
        //sendNotifFn("test",  error, "deleteFirebaseToken title");
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, OffreInsuranceModel>> getInsurance(
    RequestOffreInsuranceModel request,
  ) async {
    if (await networkInfo.isConnected) {
      // try {
      final response = await remoteDataSource.getInsurance(request);
      if (response.statusCode == ResponseCode.success) {
        return Right(response);
      } else {
        return Left(Failure(
          response.statusCode ?? 1,
          response.statusMessage ?? "",
        ));
      }
      // } catch (error) {
      //   inspect(error);
      //   if (error is DioException) {
      //     DioException erreur = error;
      //     if (erreur.response != null) {
      //       if (erreur.response!.data != null) {
      //         return (Left(
      //           ErrorHandler.handle(error).failure
      //             ..message = erreur.response!.data!["statusMessage"] ??
      //                 ResponseMessage.defaultResponse
      //             ..code = erreur.response!.data!["statusCode"] ??
      //                 ApiInternalStatus.failure,
      //         ));
      //       } else {
      //         return (Left(ErrorHandler.handle(error).failure));
      //       }
      //     } else {
      //       return (Left(ErrorHandler.handle(error).failure));
      //     }
      //   } else {
      //     return (Left(ErrorHandler.handle(error).failure));
      //   }
      // }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, InsuranceByInsurerModel>> getInsuranceByInsurer(
      int insurerId) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.getInsuranceByInsurer(insurerId);
        if (response.statusCode == ResponseCode.success) {
          // log("ici");
          return Right(response);
        } else {
          return Left(Failure(
            response.statusCode ?? 1,
            response.statusMessage ?? "fsd",
          ));
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, OffreCreditModel>> getCredit(
    RequestOffreCreditModel request,
  ) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getCredit(request);
        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(Failure(
            response.statusCode ?? 1,
            response.statusMessage ?? "",
          ));
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, TypeOffreInsuranceModel>> getTypeInsurance() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getTypeInsurance();
        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(Failure(
            response.statusCode ?? 1,
            response.statusMessage ?? "",
          ));
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, TypeOffreCreditModel>> getTypeCredit() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getTypeCredit();
        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(Failure(
            response.statusCode ?? 1,
            response.statusMessage ?? "",
          ));
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, OffreCreditModel>> getCreditByCreditor(
      int creditorId) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getCreditByCreditor(creditorId);
        // try {

        // final response =
        //     await remoteDataSource.getCreditByCreditor(creditorId);
        if (response.statusCode == ResponseCode.success) {
          // log("ici");
          return Right(response);
        } else {
          return Left(Failure(
            response.statusCode ?? 1,
            response.statusMessage ?? "",
          ));
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, CreditorModel>> getCreditor() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getCreditor();
        if (response.statusCode == ResponseCode.success) {
          // log("ici");
          return Right(response);
        } else {
          return Left(Failure(
            response.statusCode ?? 1,
            response.statusMessage ?? "",
          ));
        }
      } catch (error) {
        log(error.toString());
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, InsurerModel>> getInsurer() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getInsurer();
        if (response.statusCode == ResponseCode.success) {
          // log("ici");
          return Right(response);
        } else {
          return Left(Failure(
            response.statusCode ?? 1,
            response.statusMessage ?? "",
          ));
        }
      } catch (error) {
        log(error.toString());
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> deleteImage(int imageId) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.deleteImage(imageId);
        if (response.status == ResponseCode.success) {
          // log("ici");
          return Right(response.toDomain());
        } else {
          return Left(Failure(
            response.status ?? 1,
            response.message ?? "",
          ));
        }
      } catch (error) {
        log(error.toString());
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, Future<ProductModel?>>> addOtherImages(
      int productId, List<File> images) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.addOtherImage(productId, images);
        if (response.status == ResponseCode.success) {
          // log("ici");
          return Right(response.toDomain());
        } else {
          return Left(Failure(
            response.status ?? 1,
            response.message ?? "",
          ));
        }
      } catch (error) {
        log(error.toString());
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, EntrepriseModel>> getDetailsEntreprise(
    int entrepriseId,
  ) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.getEntrepriseDetails(entrepriseId);
        if (response.statusCode == ResponseCode.success) {
          // log("ici");
          return Right(response.entreprise);
        } else {
          return Left(Failure(response.statusCode, response.statusMessage));
        }
      } catch (error) {
        log(error.toString());
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, EntrepriseModel>> getDetailsEntrepriseCredit(
    int entrepriseId,
  ) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.getEntrepriseCredit(entrepriseId);
        if (response.statusCode == ResponseCode.success) {
          // log("ici");
          return Right(response.entreprise);
        } else {
          return Left(Failure(response.statusCode, response.statusMessage));
        }
      } catch (error) {
        log(error.toString());
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, String?>> sendNotif(
    String resourceIds,
    String subject,
    List<String> receivers,
    String title,
    String message,
    String messageDate,
  ) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.sendNotif(
          resourceIds,
          subject,
          receivers,
          title,
          message,
          messageDate,
        );
        if (response != "") {
          // log("ici");
          return Right(response);
        } else {
          return Left(Failure(0, ""));
        }
      } catch (error) {
        log(error.toString());
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseResponse?>> updatePassword(
    int id,
    String login,
    String password,
    String oldPassword,
  ) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.updatePassword(
          id,
          login,
          password,
          oldPassword,
        );
        if (response.status == ResponseCode.success) {
          // log("ici");
          return Right(response);
        } else {
          return Left(Failure(0, ""));
        }
      } catch (error) {
        log(error.toString());
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, MesProduitsCommanderResponse>> getMyProductOdered(
      GetProductCommanderRequest request) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getMyProductOrdered(request);
        print(response);
        if (response.statusCode == ResponseCode.success.toString()) {
          // log("ici");
          return Right(response);
        } else {
          return Left(
              Failure(int.parse(response.statusCode), response.statusMessage));
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              );
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> reductPrice(
      int commandId, int productId, double priceReduction) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.reductionPrice(
            commandId, productId, priceReduction);
        if (response.status == ResponseCode.created) {
          // log("ici");
          return Right(response.toDomain());
        } else {
          return Left(Failure(
            response.status ?? ApiInternalStatus.failure,
            response.message ?? ResponseMessage.defaultResponse,
          ));
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;

          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseResponse?>> updateUserInfo(
    int id,
    UserInfoModel userInfoModel,
  ) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.updateUserInfo(id, userInfoModel);
        if (response.status == ResponseCode.success) {
          // log("ici");
          return Right(response);
        } else {
          return Left(Failure(0, ""));
        }
      } catch (error) {
        log(error.toString());
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseResponse?>> sendOtpResetPassword(
    String phoneNumber,
  ) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.sendOtpResetPassword(
          phoneNumber,
        );
        if (response.status == ResponseCode.success) {
          // log("ici");
          return Right(response);
        } else {
          return Left(Failure(0, ""));
        }
      } catch (error) {
        log(error.toString());
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseResponse?>> resetPassword(
    String phoneNumber,
    String password,
    String token,
  ) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.resetPassword(phoneNumber, password, token);
        if (response.status == ResponseCode.success) {
          // log("ici");
          return Right(response);
        } else {
          return Left(Failure(0, ""));
        }
      } catch (error) {
        log(error.toString());
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetProductOderedInCreditResponse>>
      getProductOderedInCredit(
    String role,
  ) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getProductOderedInCredit(
          role,
        );
        if (response.statusCode == ResponseCode.success) {
          // log("ici");
          return Right(response);
        } else {
          return Left(Failure(0, ""));
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetListUnitTimeResponse>> getUnitTime() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getUnitTime();
        if (response.statusCode == ResponseCode.success) {
          // log("ici");
          return Right(response);
        } else {
          return Left(Failure(0, ""));
        }
      } catch (error) {
        log(error.toString());
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>> demandePreAchat(
      int fournisseurId, int offerId) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.demandePreAchat(fournisseurId, offerId);
        if (response.status == ResponseCode.success) {
          // log("ici");
          return Right(response.toDomain());
        } else {
          return Left(Failure(0, ""));
        }
      } catch (error) {
        log(error.toString());
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetDemandeAchatOrPreAchat>> getDemandeAchatOrPreAchat(
    int id,
    String customer,
    String type,
  ) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getDemandeAchatOrPreAchat(
            id, customer, type);
        if (response.statusCode == ResponseCode.success) {
          // log("ici");
          return Right(response);
        } else {
          return Left(
            Failure(
              ApiInternalStatus.failure,
              ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        log(error.toString());
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseApiResponseModel>>
      validedOrInvalidedDemandePreAchat(int id, bool state) async {
    if (await networkInfo.isConnected) {
      try {
        final response =
            await remoteDataSource.validedOrInvalidedDemandePreAchat(id, state);
        if (response.status == ResponseCode.success) {
          // log("ici");
          return Right(response.toDomain());
        } else {
          return Left(
            Failure(
              ApiInternalStatus.failure,
              ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        log(error.toString());
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetUserStatResponse>> getUserStat(
    int id,
  ) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await remoteDataSource.getUserStat(
          id,
        );
        if (response.statusCode == ResponseCode.success) {
          // log("ici");
          return Right(response);
        } else {
          return Left(
            Failure(
              ApiInternalStatus.failure,
              ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        log(error.toString());
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }
}
