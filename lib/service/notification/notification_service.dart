import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:retrofit/retrofit.dart';

import '../../app/constant.dart';
import '../../data/network/error_handler.dart';
import '../../data/network/failure.dart';
import '../../data/responses/responses.dart';
import '../../domain/model/notification_model.dart';
part "notification_service.g.dart";

@RestApi()
abstract class NotificationService {
  factory NotificationService(Dio dio, {String baseUrl}) = _NotificationService;
  @GET(
      "${Constant.baseUrl}${Constant.urlMsNotification}/notificatrions-receive/{userId}")
  Future<GetNotificationsResponse> getUserNotification(@Path() int userId);

  @DELETE(
      "${Constant.baseUrl}${Constant.urlMsNotification}/notificatrions-receive/delete/{notificationId}")
  Future<BaseResponse> deleteNotification(@Path() String notificationId);
  @PUT(
      "${Constant.baseUrl}${Constant.urlMsNotification}/notificatrions-receive/read/{id}")
  Future<BaseResponse> readNotification(@Path() String id);
}

class NotificationServiceImpl {
  NotificationServiceImpl(this.notificationService);
  final NotificationService notificationService;
  InternetConnectionChecker networkInfo = InternetConnectionChecker();
  Future<Either<Failure, BaseResponse>> readNotification(String id) async {
    if (await networkInfo.hasConnection) {
      try {
        final response = await notificationService.readNotification(id);

        if (response.status == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, GetNotificationsResponse>> getUserNotification(
      int userId) async {
    if (await networkInfo.hasConnection) {
      try {
        final response = await notificationService.getUserNotification(userId);

        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(response.statusCode, response.statusMessage),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  Future<Either<Failure, BaseResponse>> deleteNotification(
      String notificationId) async {
    if (await networkInfo.hasConnection) {
      try {
        final response =
            await notificationService.deleteNotification(notificationId);

        if (response.status == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(
              response.status ?? ApiInternalStatus.failure,
              response.message ?? ResponseMessage.defaultResponse,
            ),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }
}
