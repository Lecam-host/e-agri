import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:retrofit/retrofit.dart';

import '../../app/constant.dart';
import '../../data/network/error_handler.dart';
import '../../data/network/failure.dart';
import '../../domain/model/product_paid_model.dart';
part "vos_avis_service.g.dart";

@RestApi()
abstract class VosAvisService {
  factory VosAvisService(Dio dio, {String baseUrl}) = _VosAvisService;
  @GET("${Constant.baseUrl}/middleware/paidProduct/{userId}")
  Future<GetListProductPaidResponse> getProductPaid(
    @Path() int userId,
    @Query("page") int pageNumber,
    @Query("perPage") int perPage,
    @Query("sortBy") String sortBy,
  );
}

class VosAvisServiceImpl {
  VosAvisServiceImpl(this.service);
  final VosAvisService service;
  InternetConnectionChecker networkInfo = InternetConnectionChecker();
  Future<Either<Failure, GetListProductPaidResponse>> getProductPaid(
    int userId,
    int pageNumber,
    int perPage,
    String sortBy,
  ) async {
    if (await networkInfo.hasConnection) {
      try {
        final response = await service.getProductPaid(userId,pageNumber,perPage,sortBy);

        if (response.statusCode == ResponseCode.success) {
          return Right(response);
        } else {
          return Left(
            Failure(response.statusCode, response.statusMessage),
          );
        }
      } catch (error) {
        if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
      }
    } else {
      return Left(DataSource.noInternetConnection.getFailure());
    }
  }

  
}
