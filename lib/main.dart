import 'dart:io';

import 'package:eagri/app/app_params.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

import 'app/app.dart';
import 'app/app_prefs.dart';
import 'app/di.dart';
import 'app/notification_controller.dart';
import 'controllers/user_controller.dart';
import 'data/repository/local/speculation_repo.dart';
import 'domain/model/user_model.dart';
import 'presentations/ressources/routes_manager.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
}

Future<void> main() async {
  String initialRoute = "";
  WidgetsFlutterBinding.ensureInitialized();
  HttpOverrides.global = MyHttpOverrides();
  await Firebase.initializeApp();
  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  NotificationFactory notificationController = NotificationFactory();
  notificationController.init();
  await initAppModule();
  if ((await SpeculationRepo().getAllItem()).isEmpty) {
    await getAppParams();
  } else {
    getAppParams();
  }

  // FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  // await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
  //   alert: true,
  //   badge: true,
  //   sound: true,
  // );
  UserModel user = UserModel();
  AppSharedPreference appSharedPreference = instance<AppSharedPreference>();
  appSharedPreference.openAppGetInfo();

  await appSharedPreference.verifyOpenApp().then((value) {
    if (value == true) {
      initialRoute = Routes.onBoardingRoute;
    } else {
      initialRoute = Routes.homeRoute;
    }
  });
  await appSharedPreference.getUserInformation().then((value) => user = value);
  Get.put(UserController());

  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitDown,
    DeviceOrientation.portraitUp,
  ]).then(
    (value) => runApp(
      MultiProvider(
        providers: [
          Provider.value(value: appSharedPreference),
          ChangeNotifierProvider(create: (context) => user),
        ],
        child: MyApp(
          initialRoute: initialRoute,
        ),
      ),
    ),
  );
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
