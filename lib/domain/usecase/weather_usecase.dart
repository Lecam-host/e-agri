import 'package:dartz/dartz.dart';
import 'package:eagri/data/request/request.dart';

import '../../data/network/failure.dart';
import '../../data/repository/api/repository_impl.dart';
import '../model/advice_model.dart';
import '../model/weather_model.dart';

class WeatherUseCase {
  RepositoryImpl repository;
  WeatherUseCase(this.repository);
  Future<Either<Failure, CityWeatherModel>> getCurrentWeather(
      GetWeatherRequest location) async {
    return await repository
        .getCurrentWeather(GetWeatherRequest(location.lat, location.lon));
  }

  Future<Either<Failure, ParcelInfoModel>> createParcel(
      CreateParcelRequest request) async {
    return await repository.createParcel(request);
  }

  Future<Either<Failure, ParcelInfoModel>> getParcelInfo(
      String parcelId) async {
    return await repository.getParcelInfo(parcelId);
  }

  Future<Either<Failure, ListHistoryWeatherModel>> getHistory(
      HistoryWeatherRequest request) async {
    return await repository.historyWeather(request);
  }

  Future<Either<Failure, ListParcelModel>> getUserListParcel(
      int userdId) async {
    return await repository.getUserListParcel(userdId);
  }

  Future<Either<Failure, CreateFullParcelResponseModel>> createFullParcelle(
      List<CreateParcelRequest> request) async {
    return await repository.createFullParcel(request);
  }

  Future<Either<Failure, List<RecommandationModel>?>> getListRecommandation(
      GetRecommandationRequest request) async {
    return await repository.getListRecommandation(request);
  }
}
