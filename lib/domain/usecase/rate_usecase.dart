import 'package:dartz/dartz.dart';
import 'package:eagri/data/request/notation_request.dart';
import 'package:eagri/domain/model/rate_model.dart';

import '../../data/network/failure.dart';
import '../../data/repository/api/repository_impl.dart';
import '../model/model.dart';

class RateUseCase {
  RepositoryImpl repository;
  RateUseCase(this.repository);

  Future<Either<Failure, BaseApiResponseModel>> addRate(
      AddRateRequest request) async {
    return await repository.addRate(request);
  }

  Future<Either<Failure, CheckRateResponse>> checkAlreadyRated(
      CheckRateRequest request) async {
    return await repository.checkRating(request);
  }

  Future<Either<Failure, GetListCommentOnProductResponse>> getProductComment(
      ProductListCommentRequest request) async {
    return await repository.getProductComment(request);
  }
}
