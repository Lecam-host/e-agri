import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:eagri/data/request/request.dart';
import 'package:eagri/domain/model/alert_model.dart';
import 'package:eagri/domain/usecase/config_usecase.dart';
import 'package:eagri/domain/usecase/location_usecase.dart';

import '../../data/network/failure.dart';
import '../../data/repository/api/repository_impl.dart';
import '../model/location_model.dart';
import '../model/model.dart';

class AlertUserCase {
  RepositoryImpl repository;
  ConfigUsecase configUsecase;
  LocationUsecase locationUsecase;
  AlertUserCase(this.repository, this.configUsecase, this.locationUsecase);

  Future<Either<Failure, AllConfigModel>> getAllConfig() async {
    return await configUsecase.getAllConfig();
  }

  Future<Either<Failure, ListRegionModel>> getListRegion() async {
    return await locationUsecase.getListRegion();
  }

  Future<Either<Failure, AlertModel>> addAlert(AddAlertRequest request) async {
    return await repository.addAlert(request);
  }

  Future<Either<Failure, BaseApiResponseModel>> addAlertFiles(
      int idAlert, List<File> file) async {
    return await repository.addAlertFiles(idAlert, file);
  }
}
// 