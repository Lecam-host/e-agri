import 'package:dartz/dartz.dart';

import '../../data/network/failure.dart';
import '../../data/repository/api/repository_impl.dart';
import '../model/model.dart';

class ConfigUsecase {
  RepositoryImpl repository;
  ConfigUsecase(this.repository);

  Future<Either<Failure, AllConfigModel>> getAllConfig() async {
    return await repository.getAllConfig();
  }

  Future<Either<Failure, List<ConfigModel>>> getAlertDomain() async {
    return await repository.getAlertDomain();
  }
}
// 