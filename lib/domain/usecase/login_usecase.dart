import 'package:dartz/dartz.dart';
import 'package:eagri/domain/model/model.dart';

import '../../data/network/failure.dart';
import '../../data/repository/api/repository_impl.dart';
import '../../data/request/request.dart';
import '../../data/responses/account_response.dart';
import '../model/user_model.dart';

class LoginUseCase {
  RepositoryImpl repository;
  LoginUseCase(this.repository);
  Future<Either<Failure, AuthModel>> login(LoginRequest loginRequest) async {
    return await repository.login(loginRequest);
  }

  Future<Either<Failure, UserModel>> getUserInfo(int id) async {
    return await repository.getUserInfo(id);
  }

  Future<Either<Failure, AccountInfoResponse>> getAccountInfoByToken() async {
    return await repository.getAccountInfoByToken();
  }

  Future<Either<Failure, BaseApiResponseModel>> saveRegistrationId(
      SaveRegistrationIdRequest request) async {
    return await repository.saveRegistrationId(request);
  }
}

class LoginUseCaseInput {
  String number;
  String password;
  LoginUseCaseInput(this.number, this.password);
}
