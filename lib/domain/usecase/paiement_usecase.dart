import 'package:dartz/dartz.dart';
import 'package:eagri/data/request/paiement_request.dart';
import 'package:eagri/domain/model/paiement_model.dart';

import '../../data/network/failure.dart';
import '../../data/repository/api/repository_impl.dart';

class PaiementUsecase {
  RepositoryImpl repository;
  PaiementUsecase(this.repository);

  Future<Either<Failure, CreatePaiementResponse>> createPaiement(
      CreatePaiementRequest request) async {
    return await repository.createPaiement(request);
  }

  Future<Either<Failure, ConfirmPaymentResponse>> confirPaiement(
      String signature, otp) async {
    return await repository.confirmPaiement(signature, otp);
  }

  Future<Either<Failure, VerificationPaymentResponse>> verificationPayment(
      String signature) async {
    return await repository.verificationPayment(signature);
  }
}
