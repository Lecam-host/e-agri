import 'package:dartz/dartz.dart';
import 'package:eagri/data/repository/api/repository_impl.dart';
import 'package:eagri/data/responses/responses.dart';

import '../../data/network/failure.dart';
import '../../data/request/request.dart';
import '../model/model.dart';
import 'config_usecase.dart';

class AdviceUseCase {
  RepositoryImpl repository;
  ConfigUsecase configUsecase;
  AdviceUseCase(this.repository, this.configUsecase);
  Future<Either<Failure, AskAdviceResponse>> askAdvice(
      AskAdviceRequest input) async {
    return await repository.askAdvice(
      AskAdviceRequest(
        input.entitiesId,
        input.categoriesId,
        input.adviceType,
        input.mediaType,
        input.description,
        input.userId,
      ),
    );
  }

  Future<Either<Failure, ListAdviceModel>> getListAdvice(
      GetListAdviceRequest request) async {
    return await repository.getAllAdvice(request);
  }

  Future<Either<Failure, AdviceModel>> detailsAdvice(int adviceId) async {
    return await repository.detailAdvice(adviceId);
  }

  Future<Either<Failure, ListAdviceCategoryModel>> getAdviceCategory() async {
    return await repository.getAllAdviceCategory();
  }

  Future<Either<Failure, ListAdviceCategoryModel>> execute() async {
    return await repository.getAllAdviceCategory();
  }

  Future<Either<Failure, ListAdviceModel>> searchAdvices(
      SearchAdviceRequest request) async {
    return await repository.searchAdvice(request);
  }

  Future<Either<Failure, AllConfigModel>> getAllConfig() async {
    return await configUsecase.getAllConfig();
  }

  Future<Either<Failure, ListAskAdviceModel>> getUserAsk(
      GetUserAskAdviceRequest request) async {
    return await repository.getListAskAdvice(request);
  }

  Future<Either<Failure, BaseApiResponseModel>> addAdvice(
      AddAdviceRequest addAdviceRequest) async {
    return await repository.addAdvice(addAdviceRequest);
  }

  Future<Either<Failure, ListAdviceModel>> getUserAskAdviceAnswer(
      GetUserAskAdviceAnswerRequest request) async {
    return await repository.getUserAskAdviceAnswer(request);
  }
}
