import 'package:dartz/dartz.dart';

import '../../data/network/failure.dart';
import '../../data/repository/api/repository_impl.dart';

class ParamsUseCase {
  RepositoryImpl repository;
  ParamsUseCase(this.repository);

  Future<Either<Failure, List<String>>> getListTypeVente() async {
    return await repository.getListTypeVente();
  }

  Future<Either<Failure, List<String>>> getListOffersType() async {
    return await repository.getListOffersType();
  }
}
