import 'package:dartz/dartz.dart';
import 'package:eagri/data/request/shop_resquests.dart';
import 'package:eagri/domain/model/command_model.dart';
import 'package:eagri/domain/model/model.dart';
import '../../data/network/failure.dart';
import '../../data/repository/api/repository_impl.dart';
import '../../data/request/request_object.dart';
import '../../data/responses/shop_response.dart';
import '../model/Product.dart';
import '../model/cart_model.dart';

class ShopUseCase {
  RepositoryImpl repository;
  ShopUseCase(this.repository);

  Future<Either<Failure, BaseApiResponseModel>> addProductOnMarket(
      AddProductOnMarketrequest request) async {
    return await repository.addProductOnMarket(request);
  }

  Future<Either<Failure, Future<List<ProductModel>>>> getListProducts() async {
    return await repository.getListProducts();
  }

  Future<Either<Failure, GetAllProductResponse>> searchProduct(
      SearchProductRequest request) async {
    return await repository.searchProduct(request);
  }

  Future<Either<Failure, DataProductUserResponse>> getListProductsFournisseur(
      int id) async {
    return await repository.getListProductsFournisseur(id);
  }

  Future<Either<Failure, List<ProductShoppingCartsItem>>>
      addProductShoppingCart(AddProductInShoppingCartsRequest request) async {
    return await repository.addProductInShoppingCart(request);
  }

  Future<Either<Failure, ListCartResponse>> getUserCart(int userId) async {
    return await repository.getUserCart(userId);
  }

  Future<Either<Failure, BaseApiResponseModel>> deleteProduct(
      int idProduct) async {
    return await repository.deleteCatalogueProduct(idProduct);
  }

  Future<Either<Failure, CreateCommandModel>> createCommand(
      int userId, int cartId) async {
    return await repository.createCommand(userId, cartId);
  }

  Future<Either<Failure, GetUserCommands>> getUserCommand(
    int userId,
  ) async {
    return await repository.getUserCommand(userId);
  }

  Future<Either<Failure, BaseApiResponseModel>> deleteProdInCart(
    int cartId,
    int itemId,
  ) async {
    return await repository.deleteProdInCart(
      cartId,
      itemId,
    );
  }

  Future<Either<Failure, Future<ProductModel?>>> getDetailProduct(
      int idProduct) async {
    return await repository.getDetailProduct(idProduct);
  }
}
