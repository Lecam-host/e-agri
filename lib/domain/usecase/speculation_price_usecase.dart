import 'package:dartz/dartz.dart';
import 'package:eagri/data/request/request.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/domain/usecase/location_usecase.dart';

import '../../data/network/failure.dart';
import '../../data/repository/api/repository_impl.dart';
import '../model/location_model.dart';

class SpeculationPriceUseCase {
  LocationUsecase locationUsecase;
  RepositoryImpl repository;
  SpeculationPriceUseCase(this.repository, this.locationUsecase);
  Future<Either<Failure, ListSpeculationPriceModel>> getListSpeculationPrice(
      GetListSpeculationPriceRequest request) async {
    return await repository.getListSpeculationPrice(
        GetListSpeculationPriceRequest(request.pageNumber, request.pageSize));
  }

  Future<Either<Failure, ListSpeculationPriceModel>>
      getListSpeculationPriceEnterByUser(
          GetListSpeculationPriceRequest request) async {
    return await repository.getListSpeculationPriceEnterByUser(
        GetListSpeculationPriceRequest(request.pageNumber, request.pageSize));
  }

  Future<Either<Failure, BaseApiResponseModel>> addSpeculationPrice(
      AddSpeculationPriceRequest request) async {
    return await repository.addSpeculationPrice(AddSpeculationPriceRequest(
      speculationId: request.speculationId,
      userId: request.userId,
      from: request.from,
      to: request.to,
      official: request.official,
      data: request.data,
      source: request.source,
      unit: request.unit,
    ));
  }

  Future<Either<Failure, ListRegionModel>> getListRegion() async {
    return await locationUsecase.getListRegion();
  }

  Future<Either<Failure, ListDepartementModel>> getListDepartementOfRegion(
      int idregion) async {
    return await repository.getListDepartementOfRegion(idregion);
  }

  Future<Either<Failure, ListSousPrefectureModel>> getListSpOfDepartment(
      int idDepartement) async {
    return await repository.getListSpOfDepartement(idDepartement);
  }

  Future<Either<Failure, ListLocaliteModel>> getListLocaliteOfsp(
      int idSousPrefecture) async {
    return await repository.getListLocaliteOfSp(idSousPrefecture);
  }

  Future<Either<Failure, ListSpeculationModel>> getListSpeculation() async {
    return await repository.getListSpeculation();
  }

  Future<Either<Failure, ListSpeculationPriceModel>> getSpeculationPriceHistory(
      SpeculationPriceRequest request) async {
    return await repository.getSpeculationPriceHistory(request);
  }

  Future<Either<Failure, ListSpeculationPriceModel>> compareSpeculationPrice(
      CompareSpeculationPriceRequest request) async {
    return await repository.compareSpeculationPrice(request);
  }

  Future<Either<Failure, MinAndMaxSpeculationPriceModel?>> getMinMaxPrice(
      GetMinAndMaxSpeculationnPriceRequest request) async {
    return await repository.getMinMaxPrice(request);
  }
}
