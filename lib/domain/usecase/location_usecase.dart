import 'package:dartz/dartz.dart';
import 'package:eagri/domain/model/location_model.dart';
import 'package:eagri/domain/model/model.dart';

import '../../data/network/failure.dart';
import '../../data/repository/api/repository_impl.dart';

class LocationUsecase {
  RepositoryImpl repository;
  LocationUsecase(this.repository);
  Future<Either<Failure, ListRegionModel>> getListRegion() async {
    return await repository.getListRegion();
  }

  Future<Either<Failure, ListLocationForWeatherModel>>
      getLocationForWeather() async {
    return await repository.getListLocationForWeather();
  }

  Future<Either<Failure, ListDepartementModel>> getListDepartement(
      int idregion) async {
    return await repository.getListDepartementOfRegion(idregion);
  }

  Future<Either<Failure, ListSousPrefectureModel>> getListSpOfDepartement(
      int idDepartement) async {
    return await repository.getListSpOfDepartement(idDepartement);
  }

  Future<Either<Failure, ListLocaliteModel>> getListLocaliteOfSp(
      int idSp) async {
    return await repository.getListLocaliteOfSp(idSp);
  }
}
// 