import 'package:dartz/dartz.dart';
import 'package:eagri/data/request/request.dart';
import 'package:eagri/domain/model/info_by_id_model.dart';

import '../../data/network/failure.dart';
import '../../data/repository/api/repository_impl.dart';

class UseCase {
  RepositoryImpl repository;
  UseCase(this.repository);
  Future<Either<Failure, InfoByIdModel>> getInfoById(
      InfoByIdRequest request) async {
    return await repository.getInfoById(request);
  }
}
