import 'dart:convert';

import 'package:eagri/domain/model/user_model.dart';

CheckRateResponse checkRateResponseFromJson(String str) =>
    CheckRateResponse.fromJson(json.decode(str));

String checkRateResponseToJson(CheckRateResponse data) =>
    json.encode(data.toJson());

class CheckRateResponse {
  CheckRateResponse({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  int statusCode;
  String statusMessage;
  bool data;

  factory CheckRateResponse.fromJson(Map<String, dynamic> json) =>
      CheckRateResponse(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: json["data"],
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data,
      };
}

GetListCommentOnProductResponse getListCommentOnProductResponseFromJson(
        String str) =>
    GetListCommentOnProductResponse.fromJson(json.decode(str));

String getListCommentOnProductResponseToJson(
        GetListCommentOnProductResponse data) =>
    json.encode(data.toJson());

class GetListCommentOnProductResponse {
  GetListCommentOnProductResponse({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  int statusCode;
  String statusMessage;
  RateAndCommentData data;

  factory GetListCommentOnProductResponse.fromJson(Map<String, dynamic> json) =>
      GetListCommentOnProductResponse(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: RateAndCommentData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data.toJson(),
      };
}

class RateAndCommentData {
  RateAndCommentData({
    required this.total,
    required this.totalOfPages,
    this.perPage,
    this.sorts,
    this.order,
    required this.currentPage,
    required this.nextPage,
    required this.previousPage,
    this.filters,
    required this.items,
  });

  int total;
  int totalOfPages;
  dynamic perPage;
  dynamic sorts;
  dynamic order;
  int currentPage;
  int nextPage;
  int previousPage;
  dynamic filters;
  List<RateAndCommentItem> items;

  factory RateAndCommentData.fromJson(Map<String, dynamic> json) =>
      RateAndCommentData(
        total: json["total"],
        totalOfPages: json["total_of_pages"],
        perPage: json["per_page"],
        sorts: json["sorts"],
        order: json["order"],
        currentPage: json["current_page"],
        nextPage: json["next_page"],
        previousPage: json["previous_page"],
        filters: json["filters"],
        items: List<RateAndCommentItem>.from(
            json["items"].map((x) => RateAndCommentItem.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "total_of_pages": totalOfPages,
        "per_page": perPage,
        "sorts": sorts,
        "order": order,
        "current_page": currentPage,
        "next_page": nextPage,
        "previous_page": previousPage,
        "filters": filters,
        "items": List<dynamic>.from(items.map((x) => x.toJson())),
      };
}

class RateAndCommentItem {
  RateAndCommentItem({
    required this.id,
    required this.note,
    required this.codeService,
    required this.resourceId,
    required this.userId,
    required this.notePar,
    required this.comment,
    this.user,
    required this.createdAt,
  });

  int id;
  int note;
  int codeService;
  int resourceId;
  int userId;
  int notePar;
  UserModel? user;
  String comment;
  DateTime createdAt;

  factory RateAndCommentItem.fromJson(Map<String, dynamic> json) =>
      RateAndCommentItem(
        id: json["id"],
        note: json["note"],
        codeService: json["codeService"],
        resourceId: json["resourceId"],
        userId: json["userId"],
        notePar: json["notePar"],
        comment: json["comment"],
        createdAt: DateTime.parse(json["createdAt"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "note": note,
        "codeService": codeService,
        "resourceId": resourceId,
        "userId": userId,
        "notePar": notePar,
        "comment": comment,
      };
}
