
import 'dart:convert';

import 'package:eagri/data/responses/responses.dart';

import 'image_model.dart';

GetDemandeAchatOrPreAchat getDemandeAchatOrPreAchatFromJson(String str) => GetDemandeAchatOrPreAchat.fromJson(json.decode(str));

String getDemandeAchatOrPreAchatToJson(GetDemandeAchatOrPreAchat data) => json.encode(data.toJson());

class GetDemandeAchatOrPreAchat {
    int statusCode;
    String statusMessage;
    Data data;

    GetDemandeAchatOrPreAchat({
        required this.statusCode,
        required this.statusMessage,
        required this.data,
    });

    factory GetDemandeAchatOrPreAchat.fromJson(Map<String, dynamic> json) => GetDemandeAchatOrPreAchat(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data.toJson(),
    };
}

class Data {
    int total;
    int totalOfPages;
    int perPage;
    dynamic sorts;
    dynamic order;
    int currentPage;
    int nextPage;
    int previousPage;
    dynamic filters;
    List<DemandePreachatModel> items;

    Data({
        required this.total,
        required this.totalOfPages,
        required this.perPage,
        this.sorts,
        this.order,
        required this.currentPage,
        required this.nextPage,
        required this.previousPage,
        this.filters,
        required this.items,
    });

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        total: json["total"],
        totalOfPages: json["total_of_pages"],
        perPage: json["per_page"],
        sorts: json["sorts"],
        order: json["order"],
        currentPage: json["current_page"],
        nextPage: json["next_page"],
        previousPage: json["previous_page"],
        filters: json["filters"],
        items: List<DemandePreachatModel>.from(json["items"].map((x) => DemandePreachatModel.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "total": total,
        "total_of_pages": totalOfPages,
        "per_page": perPage,
        "sorts": sorts,
        "order": order,
        "current_page": currentPage,
        "next_page": nextPage,
        "previous_page": previousPage,
        "filters": filters,
        "items": List<dynamic>.from(items.map((x) => x.toJson())),
    };
}
class DemandePreachatResponse extends BaseResponse {
  DemandePreachatModel data;

  DemandePreachatResponse({required this.data});

  factory DemandePreachatResponse.fromJson(Map<String, dynamic> json) => DemandePreachatResponse(
        
        data: DemandePreachatModel.fromJson(json["data"]),
       
    );

}
class DemandePreachatModel {
    int id;
    int fournisseurId;
    Buyer fournisseur;
    int buyerId;
    Buyer buyer;
    int offerId;
    Offer offer;
    String prePurchaseRequestType;
    bool accepted;
    bool refused;
    DateTime createdAt;

    DemandePreachatModel({
        required this.id,
        required this.fournisseurId,
        required this.fournisseur,
        required this.buyerId,
        required this.buyer,
        required this.offerId,
        required this.offer,
        required this.prePurchaseRequestType,
        required this.accepted,
        required this.refused,
        required this.createdAt,
    });

    factory DemandePreachatModel.fromJson(Map<String, dynamic> json) => DemandePreachatModel(
        id: json["id"],
        fournisseurId: json["fournisseurId"],
        fournisseur: Buyer.fromJson(json["fournisseur"]),
        buyerId: json["buyerId"],
        buyer: Buyer.fromJson(json["buyer"]),
        offerId: json["offerId"],
        offer: Offer.fromJson(json["offer"]),
        prePurchaseRequestType: json["prePurchaseRequestType"],
        accepted: json["accepted"],
        refused: json["refused"],
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "fournisseurId": fournisseurId,
        "fournisseur": fournisseur.toJson(),
        "buyerId": buyerId,
        "buyer": buyer.toJson(),
        "offerId": offerId,
        "offer": offer.toJson(),
        "prePurchaseRequestType": prePurchaseRequestType,
        "accepted": accepted,
        "refused": refused,
        "created_at": createdAt.toIso8601String(),
    };
}

class Buyer {
    int id;
    String firstname;
    String lastname;
    String phone;
    String email;
    String gender;
    String? localisation;
    DateTime birthdate;
    String username;

    Buyer({
        required this.id,
        required this.firstname,
        required this.lastname,
        required this.phone,
        required this.email,
        required this.gender,
        this.localisation,
        required this.birthdate,
        required this.username,
    });

    factory Buyer.fromJson(Map<String, dynamic> json) => Buyer(
        id: json["id"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        phone: json["phone"],
        email: json["email"],
        gender: json["gender"],
        localisation: json["localisation"],
        birthdate: DateTime.parse(json["birthdate"]),
        username: json["username"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "firstname": firstname,
        "lastname": lastname,
        "phone": phone,
        "email": email,
        "gender": gender,
        "localisation": localisation,
        "birthdate": birthdate.toIso8601String(),
        "username": username,
    };
}

class Offer {
    int id;
    DateTime publicationDate;
    String description;
    String code;
    String availability;
    dynamic availabilityDate;
    dynamic expirationDate;
    dynamic certification;
    String typeVente;
    Location location;
    String status;
    int paymentId;
    Product product;
    List<ImageProduct> images;
    String offerType;
    double notation;
    PaymentMethod ?paymentMethod;
    dynamic unitTime;
    dynamic numberSlice;
    dynamic origin;
    DateTime createdAt;
   
    int fournisseurId;

    Offer({
        required this.id,
        required this.publicationDate,
        required this.description,
        required this.code,
        required this.availability,
        this.availabilityDate,
        this.expirationDate,
        this.certification,
        required this.typeVente,
        required this.location,
        required this.status,
        required this.paymentId,
        required this.product,
        required this.images,
        required this.offerType,
        required this.notation,
         this.paymentMethod,
        this.unitTime,
        this.numberSlice,
        this.origin,
        required this.createdAt,
        required this.fournisseurId,
    });

    factory Offer.fromJson(Map<String, dynamic> json) => Offer(
        id: json["id"],
        publicationDate: DateTime.parse(json["publicationDate"]),
        description: json["description"],
        code: json["code"],
        availability: json["availability"],
        availabilityDate: json["availabilityDate"],
        expirationDate: json["expirationDate"],
        certification: json["certification"],
        typeVente: json["typeVente"],
        location: Location.fromJson(json["location"]),
        status: json["status"],
        paymentId: json["paymentId"],
        product: Product.fromJson(json["product"]),
        images: List<ImageProduct>.from(json["images"].map((x) => x)),
        offerType: json["offerType"],
        notation: json["notation"],
        paymentMethod:json["paymentMethod"]!=null? PaymentMethod.fromJson(json["paymentMethod"]):null,
        unitTime: json["unitTime"],
        numberSlice: json["numberSlice"],
        origin: json["origin"],
        createdAt: DateTime.parse(json["created_at"]),
        fournisseurId: json["fournisseurId"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "publicationDate": publicationDate.toIso8601String(),
        "description": description,
        "code": code,
        "availability": availability,
        "availabilityDate": availabilityDate,
        "expirationDate": expirationDate,
        "certification": certification,
        "typeVente": typeVente,
        "location": location.toJson(),
        "status": status,
        "paymentId": paymentId,
        "product": product.toJson(),
        "images": List<dynamic>.from(images.map((x) => x)),
        "offerType": offerType,
        "notation": notation,
        "paymentMethod":paymentMethod!=null? paymentMethod!.toJson():null,
        "unitTime": unitTime,
        "numberSlice": numberSlice,
        "origin": origin,
        "created_at": createdAt.toIso8601String(),
        "fournisseurId": fournisseurId,
    };
}

class Location {
    int id;
    int regionId;
    dynamic departementId;
    dynamic sprefectureId;
    dynamic localiteId;

    Location({
        required this.id,
        required this.regionId,
        this.departementId,
        this.sprefectureId,
        this.localiteId,
    });

    factory Location.fromJson(Map<String, dynamic> json) => Location(
        id: json["id"],
        regionId: json["regionId"],
        departementId: json["departementId"],
        sprefectureId: json["sprefectureId"],
        localiteId: json["localiteId"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "regionId": regionId,
        "departementId": departementId,
        "sprefectureId": sprefectureId,
        "localiteId": localiteId,
    };
}

class PaymentMethod {
    int id;
    String code;
    int priority;
    String name;

    PaymentMethod({
        required this.id,
        required this.code,
        required this.priority,
        required this.name,
    });

    factory PaymentMethod.fromJson(Map<String, dynamic> json) => PaymentMethod(
        id: json["id"],
        code: json["code"],
        priority: json["priority"],
        name: json["name"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "code": code,
        "priority": priority,
        "name": name,
    };
}

class Product {
    int id;
    String identifiant;
    int categorieId;
    int speculationId;
    int quantity;
    int initialQuantity;
    double priceU;
    DateTime createdAt;
    String unitOfMeasurment;

    Product({
        required this.id,
        required this.identifiant,
        required this.categorieId,
        required this.speculationId,
        required this.quantity,
        required this.initialQuantity,
        required this.priceU,
        required this.createdAt,
        required this.unitOfMeasurment,
    });

    factory Product.fromJson(Map<String, dynamic> json) => Product(
        id: json["id"],
        identifiant: json["identifiant"],
        categorieId: json["categorieId"],
        speculationId: json["speculationId"],
        quantity: json["quantity"],
        initialQuantity: json["initial_quantity"],
        priceU: json["price_u"],
        createdAt: DateTime.parse(json["created_at"]),
        unitOfMeasurment: json["unitOfMeasurment"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "identifiant": identifiant,
        "categorieId": categorieId,
        "speculationId": speculationId,
        "quantity": quantity,
        "initial_quantity": initialQuantity,
        "price_u": priceU,
        "created_at": createdAt.toIso8601String(),
        "unitOfMeasurment": unitOfMeasurment,
    };
}
