// To parse this JSON data, do
//
//     final historyVenteResponse = historyVenteResponseFromJson(jsonString);

import 'dart:convert';

HistoryVenteResponse historyVenteResponseFromJson(String str) =>
    HistoryVenteResponse.fromJson(json.decode(str));

String historyVenteResponseToJson(HistoryVenteResponse data) =>
    json.encode(data.toJson());

class HistoryVenteResponse {
  int statusCode;
  String statusMessage;
  VenteElement data;

  HistoryVenteResponse({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  factory HistoryVenteResponse.fromJson(Map<String, dynamic> json) =>
      HistoryVenteResponse(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: VenteElement.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data.toJson(),
      };
}

class VenteElement {
  String typePaiement;
  String typeVente;
  int fournisseurId;
  Fournisseur fournisseur;
  int amount;
  List<VenteProductElement> products;

  VenteElement({
    required this.typePaiement,
    required this.typeVente,
    required this.fournisseurId,
    required this.fournisseur,
    required this.amount,
    required this.products,
  });

  factory VenteElement.fromJson(Map<String, dynamic> json) => VenteElement(
        typePaiement: json["typePaiement"],
        typeVente: json["typeVente"],
        fournisseurId: json["fournisseurId"],
        fournisseur: Fournisseur.fromJson(json["fournisseur"]),
        amount: json["amount"],
        products: List<VenteProductElement>.from(
            json["products"].map((x) => VenteProductElement.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "typePaiement": typePaiement,
        "typeVente": typeVente,
        "fournisseurId": fournisseurId,
        "fournisseur": fournisseur.toJson(),
        "amount": amount,
        "products": List<dynamic>.from(products.map((x) => x.toJson())),
      };
}

class Fournisseur {
  int id;
  String username;
  String firstname;
  String lastname;
  String gender;
  String phone;
  String adress;
  String email;

  Fournisseur({
    required this.id,
    required this.username,
    required this.firstname,
    required this.lastname,
    required this.gender,
    required this.phone,
    required this.adress,
    required this.email,
  });

  factory Fournisseur.fromJson(Map<String, dynamic> json) => Fournisseur(
        id: json["id"],
        username: json["username"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        gender: json["gender"],
        phone: json["phone"],
        adress: json["adress"],
        email: json["email"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "username": username,
        "firstname": firstname,
        "lastname": lastname,
        "gender": gender,
        "phone": phone,
        "adress": adress,
        "email": email,
      };
}

class VenteProductElement {
  int id;
  int idProduct;
  int quantity;
  int price;
  int customerId;
  DateTime datePaiement;
  ProductProduct product;
  Fournisseur customer;

  VenteProductElement({
    required this.id,
    required this.idProduct,
    required this.quantity,
    required this.price,
    required this.customerId,
    required this.datePaiement,
    required this.product,
    required this.customer,
  });

  factory VenteProductElement.fromJson(Map<String, dynamic> json) =>
      VenteProductElement(
        id: json["id"],
        idProduct: json["idProduct"],
        quantity: json["quantity"],
        price: json["price"],
        customerId: json["customerId"],
        datePaiement: DateTime.parse(json["datePaiement"]),
        product: ProductProduct.fromJson(json["product"]),
        customer: Fournisseur.fromJson(json["customer"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "idProduct": idProduct,
        "quantity": quantity,
        "price": price,
        "customerId": customerId,
        "datePaiement": datePaiement.toIso8601String(),
        "product": product.toJson(),
        "customer": customer.toJson(),
      };
}

class ProductProduct {
  int id;
  String code;
  String description;
  String availability;
  String availabilityDate;
  String typeVente;
  String offerType;
  double notation;
  String status;
  DateTime publicationDate;
  Location location;
  List<Image> images;
  int categoryId;
  //Category category;
  int ?speculationId;
  Category? speculation;
  String unityName;
  ProductProduct({
    required this.id,
    required this.code,
    required this.description,
    required this.availability,
    required this.availabilityDate,
    required this.typeVente,
    required this.offerType,
    required this.notation,
    required this.status,
    required this.publicationDate,
    required this.location,
    required this.images,
    required this.categoryId,
    //required this.category,
    required this.unityName,
     this.speculationId,
     this.speculation,
  });

  factory ProductProduct.fromJson(Map<String, dynamic> json) => ProductProduct(
        id: json["id"],
        code: json["code"],
        description: json["description"],
        availability: json["availability"],
        availabilityDate: json["availabilityDate"],
        typeVente: json["typeVente"],
        offerType: json["offerType"],
        notation: json["notation"],
        status: json["status"],
        unityName: json["unityName"],
        publicationDate: DateTime.parse(json["publicationDate"]),
        location: Location.fromJson(json["location"]),
        images: List<Image>.from(json["images"].map((x) => Image.fromJson(x))),
        categoryId: json["categoryId"],
        //category: Category.fromJson(json["category"]),
        speculationId: json["speculationId"],
        speculation:json["speculation"]!=null? Category.fromJson(json["speculation"]):null,
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "code": code,
        "description": description,
        "availability": availability,
        "availabilityDate": availabilityDate,
        "typeVente": typeVente,
        "offerType": offerType,
        "notation": notation,
        "status": status,
        "publicationDate": publicationDate.toIso8601String(),
        "location": location.toJson(),
        "images": List<dynamic>.from(images.map((x) => x.toJson())),
        "categoryId": categoryId,
        //"category": category.toJson(),
        "speculationId": speculationId,
        //"speculation": speculation.toJson(),
      };
}

class Category {
  int id;
  String libelle;

  Category({
    required this.id,
    required this.libelle,
  });

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"],
        libelle: json["libelle"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "libelle": libelle,
      };
}

class Image {
  int id;
  String link;

  Image({
    required this.id,
    required this.link,
  });

  factory Image.fromJson(Map<String, dynamic> json) => Image(
        id: json["id"],
        link: json["link"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "link": link,
      };
}

class Location {
  int id;
  int regionId;
  dynamic departementId;
  dynamic sprefectureId;
  dynamic localiteId;

  Location({
    required this.id,
    required this.regionId,
    this.departementId,
    this.sprefectureId,
    this.localiteId,
  });

  factory Location.fromJson(Map<String, dynamic> json) => Location(
        id: json["id"],
        regionId: json["regionId"],
        departementId: json["departementId"],
        sprefectureId: json["sprefectureId"],
        localiteId: json["localiteId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "regionId": regionId,
        "departementId": departementId,
        "sprefectureId": sprefectureId,
        "localiteId": localiteId,
      };
}
