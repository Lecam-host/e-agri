import 'package:eagri/domain/model/model.dart';

class RecommandationModel {
  int? speculationId;
  String? treatmentRange;
  String? message;
  SpeculationModel? speculation;

  RecommandationModel({
    this.speculationId,
    this.treatmentRange,
    this.message,
    this.speculation,
  });
}
