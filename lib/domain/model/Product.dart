// ignore_for_file: file_names

import 'package:eagri/app/constant.dart';
import 'package:flutter/material.dart';

import 'package:eagri/domain/model/image_model.dart';
import 'package:eagri/domain/model/user_model.dart';
import 'package:eagri/presentations/ressources/strings_manager.dart';

import '../../data/responses/shop_response.dart';
import 'methode_paiement_model.dart';
import 'model.dart';
import 'transport_model.dart';

class ProductModel {
  TrajetInfoModel? startTrajetInfo;
  TrajetInfoModel? arrivalTrajetInfo;

  final int quantity, id, fournisseurId;
  final String title, unite;
  String? description;
  List<String>? images;
  List<ImageProduct>? imagesWithId;

  List<Color>? colors;
  final double rating;
  final int price;
  final int paymentId;

  final bool isFavourite, isPopular;
  final String typeVente;
  final String typeOffers;
  final String code;

  String publicationDate, status;
  final String disponibilite;
  String? identifiant;

  final String? dataDisponibilite;

  final String? defaultImg;

  final SpeculationModel speculation;
  UserModel? user;
  bool alreadyNoted;
  ProductLocationModel? location;
  Intrant? itrantInfo;
  Trajet? trajet;

  CategorieProductModel? categorieProduct;
  PaymentMethodModel? paymentMethod;

  ProductModel({
    required this.id,
    this.identifiant,
    required this.quantity,
    required this.paymentId,
    this.images,
    this.startTrajetInfo,
    this.arrivalTrajetInfo,
    this.categorieProduct,
    this.colors,
    this.rating = 0.0,
    this.isFavourite = false,
    this.isPopular = false,
    required this.title,
    required this.price,
    this.description,
    required this.unite,
    required this.typeVente,
    required this.speculation,
    required this.fournisseurId,
    required this.code,
    this.user,
    this.defaultImg,
    required this.typeOffers,
    this.dataDisponibilite,
    required this.disponibilite,
    required this.publicationDate,
    required this.status,
    this.alreadyNoted = false,
    this.location,
    this.itrantInfo,
    this.trajet,
    this.imagesWithId,
    this.paymentMethod,
  });

  factory ProductModel.fromJson(Map<String, dynamic> json) => ProductModel(
        id: json["id"],
        title: "",
        publicationDate: json["publicationDate"].toString(),
        description: json["description"],
        code: json["code"],
        disponibilite: json["availability"],
        dataDisponibilite: json["availabilityDate"],
        typeVente: json["typeVente"] ?? '',
        location: ProductLocationModel.fromJson(json["location"]),
        status: json["status"],
        paymentId: json["paymentId"],
        // product: Product.fromJson(json["product"]),
        speculation: SpeculationModel(),
        price: json["product"]["price_u"].toInt(),
        imagesWithId: List<ImageProduct>.from(
            json["images"].map((x) => ImageProduct.fromJson(x))),
        typeOffers: json["offerType"],
        rating: json["notation"],
        paymentMethod: PaymentMethodModel.fromJson(json["paymentMethod"]),
        unite: json["code"] == codeIntrant.toString()
            ? json["product"]["unitOfMeasurmentName"]
            : json["product"]["unitOfMeasurment"],
        quantity: json["product"]["quantity"],
        fournisseurId: json["fournisseurId"],
        identifiant: json["identifiant"],
      );
}

class ProductLocationModel {
  String? regionName;
  String? departementName;
  String? sPrefectureName;
  String? localiteName;
  final int? regionId;
  final int? locationId;
  final int? departementId;
  final int? sPrefectureId;
  final int? localiteId;

  ProductLocationModel({
    this.locationId,
    this.regionName,
    this.departementName,
    this.sPrefectureName,
    this.localiteName,
    this.regionId,
    this.departementId,
    this.sPrefectureId,
    this.localiteId,
  });
  factory ProductLocationModel.fromJson(Map<String, dynamic> json) =>
      ProductLocationModel(
        regionId: json["regionId"],
        departementId: json["departementId"],
        sPrefectureId: json["sprefectureId"],
        localiteId: json["localiteId"],
      );
}

// Our demo Products
class TrajetInfoModel {
  String? region;
  String? departement;
  String? sprefecture;
  String? localite;

  TrajetInfoModel(
      {this.region, this.departement, this.sprefecture, this.localite});
}

// Our demo Products
class CategorieProductModel {
  int id;
  String name;

  CategorieProductModel({required this.id, required this.name});
}

List<ProductModel> demoProducts = [
  ProductModel(
    id: 1,
    images: [
      "https://images.pexels.com/photos/1093038/pexels-photo-1093038.jpeg?auto=compress&cs=tinysrgb&w=1200",
      "https://images.pexels.com/photos/68170/paprika-green-red-vegetables-68170.jpeg?auto=compress&cs=tinysrgb&w=1200",
      "https://images.pexels.com/photos/7543155/pexels-photo-7543155.jpeg?auto=compress&cs=tinysrgb&w=1200",
      "https://images.pexels.com/photos/321551/pexels-photo-321551.jpeg?auto=compress&cs=tinysrgb&w=1200",
    ],
    quantity: 10,
    colors: [
      const Color(0xFFF6625E),
      const Color(0xFF836DB8),
      const Color(0xFFDECB9C),
      Colors.white,
    ],
    paymentId: 1,
    title: "Banane",
    code: "100",
    price: 1500,
    description: description,
    rating: 4.8,
    isFavourite: true,
    isPopular: true,
    unite: "Kg",
    typeVente: "BLOC",
    fournisseurId: 1,
    status: "",
    speculation: SpeculationModel(
      speculationId: 1,
      imageUrl:
          "https://images.pexels.com/photos/68170/paprika-green-red-vegetables-68170.jpeg?auto=compress&cs=tinysrgb&w=1200",
      name: "Banane",
      listUnit: ["kg"],
    ),
    disponibilite: "IMMEDIAT",
    typeOffers: "VENTE",
    publicationDate: "2023-03-13T15:25:07.323Z",
  ),
  ProductModel(
    id: 2,
    images: [
      "https://images.pexels.com/photos/68170/paprika-green-red-vegetables-68170.jpeg?auto=compress&cs=tinysrgb&w=1200",
    ],
    quantity: 167,
    colors: [
      const Color(0xFFF6625E),
      const Color(0xFF836DB8),
      const Color(0xFFDECB9C),
      Colors.white,
    ],
    paymentId: 1,
    title: "Piment",
    price: 2500,
    status: "",
    fournisseurId: 1,
    description: description,
    rating: 2.1,
    code: "100",
    isPopular: true,
    unite: "Sac",
    typeVente: "BLOC",
    speculation: SpeculationModel(
      speculationId: 1,
      imageUrl:
          "https://images.pexels.com/photos/68170/paprika-green-red-vegetables-68170.jpeg?auto=compress&cs=tinysrgb&w=1200",
      name: "Banane",
      listUnit: ["kg"],
    ),
    disponibilite: "IMMEDIAT",
    typeOffers: "VENTE",
    publicationDate: "2023-03-13T15:25:07.323Z",
  ),
  ProductModel(
    id: 3,
    images: [
      "https://images.pexels.com/photos/321551/pexels-photo-321551.jpeg?auto=compress&cs=tinysrgb&w=1200",
    ],
    quantity: 17,
    colors: [
      const Color(0xFFF6625E),
      const Color(0xFF836DB8),
      const Color(0xFFDECB9C),
      Colors.white,
    ],
    paymentId: 1,
    title: "Manioc",
    price: 500,
    description: description,
    rating: 3.1,
    isFavourite: true,
    isPopular: true,
    status: "",
    code: "100",
    fournisseurId: 1,
    unite: "Kg",
    typeVente: "DETAIL",
    speculation: SpeculationModel(
      speculationId: 1,
      imageUrl:
          "https://images.pexels.com/photos/68170/paprika-green-red-vegetables-68170.jpeg?auto=compress&cs=tinysrgb&w=1200",
      name: "Banane",
      listUnit: ["kg"],
    ),
    disponibilite: "IMMEDIAT",
    typeOffers: "VENTE",
    publicationDate: "2023-03-13T15:25:07.323Z",
  ),
  ProductModel(
    id: 4,
    images: [
      "https://images.pexels.com/photos/321551/pexels-photo-321551.jpeg?auto=compress&cs=tinysrgb&w=1200",
    ],
    quantity: 16,
    colors: [
      const Color(0xFFF6625E),
      const Color(0xFF836DB8),
      const Color(0xFFDECB9C),
      Colors.white,
    ],
    paymentId: 1,
    title: "Aubergine",
    price: 2200,
    fournisseurId: 1,
    description: description,
    rating: 3,
    isFavourite: true,
    unite: "Tonnes",
    code: "100",
    typeVente: "DETAILS",
    speculation: SpeculationModel(
      speculationId: 1,
      imageUrl:
          "https://images.pexels.com/photos/68170/paprika-green-red-vegetables-68170.jpeg?auto=compress&cs=tinysrgb&w=1200",
      name: "Banane",
      listUnit: ["kg"],
    ),
    status: "",
    disponibilite: "IMMEDIAT",
    typeOffers: "VENTE",
    publicationDate: "2023-03-13T15:25:07.323Z",
  ),
];

const String description = AppStrings.lorem;
