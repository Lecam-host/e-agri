// To parse this JSON data, do
//
//     final alerteModel = alerteModelFromJson(jsonString);

import 'dart:convert';

AlerteModel alerteModelFromJson(String str) =>
    AlerteModel.fromJson(json.decode(str));

String alerteModelToJson(AlerteModel data) => json.encode(data.toJson());

class AlerteModel {
  int? statusCode;
  String? statusMessage;
  List<DataAlerte>? data;

  AlerteModel({
    this.statusCode,
    this.statusMessage,
    this.data,
  });

  factory AlerteModel.fromJson(Map<String, dynamic> json) => AlerteModel(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: json["data"] == null
            ? []
            : List<DataAlerte>.from(
                json["data"]!.map((x) => DataAlerte.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class DataAlerte {
  int? id;
  dynamic message;
  int? reportingId;
  Reporting reporting;
  int? isSend;
  int? isEnded;
  DateTime? createdAt;
  DateTime? updatedAt;
  dynamic sentAt;
  dynamic endedAt;

  DataAlerte({
    this.id,
    this.message,
    this.reportingId,
    required this.reporting,
    this.isSend,
    this.isEnded,
    this.createdAt,
    this.updatedAt,
    this.sentAt,
    this.endedAt,
  });

  factory DataAlerte.fromJson(Map<String, dynamic> json) => DataAlerte(
        id: json["id"],
        message: json["message"],
        reportingId: json["reportingId"],
        reporting: Reporting.fromJson(json["reporting"]),
        isSend: json["isSend"],
        isEnded: json["isEnded"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        sentAt: json["sentAt"],
        endedAt: json["endedAt"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "message": message,
        "reportingId": reportingId,
        "reporting": reporting.toJson(),
        "isSend": isSend,
        "isEnded": isEnded,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "sentAt": sentAt,
        "endedAt": endedAt,
      };
}

class Reporting {
  int? id;
  int? userId;
  int? speculationId;
  int? regionId;
  int? departementId;
  int? sousprefectureId;
  int? localiteId;
  String? description;
  DateTime? sendDate;
  Domain? qualification;
  Domain? domain;
  List<FileElement>? files;
  Location? location;
  Speculation? speculation;
  User? user;
  bool? validate;
  bool? rejected;
  bool? active;
  bool? manage;
  DateTime? createdAt;
  dynamic deleted;
  DateTime? deletedAt;
  DateTime? updatedAt;
  dynamic validatedAt;
  dynamic rejectedAt;

  Reporting({
    this.id,
    this.userId,
    this.speculationId,
    this.regionId,
    this.departementId,
    this.sousprefectureId,
    this.localiteId,
    this.description,
    this.sendDate,
    this.qualification,
    this.domain,
    this.files,
    this.location,
    this.speculation,
    this.user,
    this.validate,
    this.rejected,
    this.active,
    this.manage,
    this.createdAt,
    this.deleted,
    this.deletedAt,
    this.updatedAt,
    this.validatedAt,
    this.rejectedAt,
  });

  factory Reporting.fromJson(Map<String, dynamic> json) => Reporting(
        id: json["id"],
        userId: json["userId"],
        speculationId: json["speculationId"],
        regionId: json["regionId"],
        departementId: json["departementId"],
        sousprefectureId: json["sousprefectureId"],
        localiteId: json["localiteId"],
        description: json["description"],
        sendDate:
            json["sendDate"] == null ? null : DateTime.parse(json["sendDate"]),
        qualification: json["qualification"] == null
            ? null
            : Domain.fromJson(json["qualification"]),
        domain: json["domain"] == null ? null : Domain.fromJson(json["domain"]),
        files: json["files"] == null
            ? []
            : List<FileElement>.from(
                json["files"]!.map((x) => FileElement.fromJson(x))),
        location: json["location"] == null
            ? null
            : Location.fromJson(json["location"]),
        speculation: json["speculation"] == null
            ? null
            : Speculation.fromJson(json["speculation"]),
        user: json["user"] == null ? null : User.fromJson(json["user"]),
        validate: json["validate"],
        rejected: json["rejected"],
        active: json["active"],
        manage: json["manage"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        deleted: json["deleted"],
        deletedAt: json["deletedAt"] == null
            ? null
            : DateTime.parse(json["deletedAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        validatedAt: json["validatedAt"],
        rejectedAt: json["rejectedAt"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "userId": userId,
        "speculationId": speculationId,
        "regionId": regionId,
        "departementId": departementId,
        "sousprefectureId": sousprefectureId,
        "localiteId": localiteId,
        "description": description,
        "sendDate": sendDate?.toIso8601String(),
        "qualification": qualification?.toJson(),
        "domain": domain?.toJson(),
        "files": files == null
            ? []
            : List<dynamic>.from(files!.map((x) => x.toJson())),
        "location": location?.toJson(),
        "speculation": speculation?.toJson(),
        "user": user?.toJson(),
        "validate": validate,
        "rejected": rejected,
        "active": active,
        "manage": manage,
        "createdAt": createdAt?.toIso8601String(),
        "deleted": deleted,
        "deletedAt": deletedAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "validatedAt": validatedAt,
        "rejectedAt": rejectedAt,
      };
}

class Domain {
  int? id;
  String? name;
  DateTime? createdAt;
  DateTime? updatedAt;
  String? description;
  int? categoryId;

  Domain({
    this.id,
    this.name,
    this.createdAt,
    this.updatedAt,
    this.description,
    this.categoryId,
  });

  factory Domain.fromJson(Map<String, dynamic> json) => Domain(
        id: json["id"],
        name: json["name"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        description: json["description"],
        categoryId: json["categoryId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "description": description,
        "categoryId": categoryId,
      };
}

class FileElement {
  int? id;
  int? realId;
  String? fileId;
  String? fileUrl;
  String? fileType;
  DateTime? createdAt;

  FileElement({
    this.id,
    this.realId,
    this.fileId,
    this.fileUrl,
    this.fileType,
    this.createdAt,
  });

  factory FileElement.fromJson(Map<String, dynamic> json) => FileElement(
        id: json["id"],
        realId: json["realId"],
        fileId: json["fileId"],
        fileUrl: json["fileUrl"],
        fileType: json["fileType"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "realId": realId,
        "fileId": fileId,
        "fileUrl": fileUrl,
        "fileType": fileType,
        "createdAt": createdAt?.toIso8601String(),
      };
}

class Location {
  int? regionId;
  String? name;

  Location({
    this.regionId,
    this.name,
  });

  factory Location.fromJson(Map<String, dynamic> json) => Location(
        regionId: json["regionId"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "regionId": regionId,
        "name": name,
      };
}

class Speculation {
  DateTime? createdAt;
  DateTime? updatedAt;
  int? speculationId;
  String? name;
  String? imageUrl;
  int? duration;
  bool? active;
  List<String>? unitsOfMeasurement;
  Domain? category;

  Speculation({
    this.createdAt,
    this.updatedAt,
    this.speculationId,
    this.name,
    this.imageUrl,
    this.duration,
    this.active,
    this.unitsOfMeasurement,
    this.category,
  });

  factory Speculation.fromJson(Map<String, dynamic> json) => Speculation(
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        speculationId: json["speculationId"],
        name: json["name"],
        imageUrl: json["imageUrl"],
        duration: json["duration"],
        active: json["active"],
        unitsOfMeasurement: json["unitsOfMeasurement"] == null
            ? []
            : List<String>.from(json["unitsOfMeasurement"]!.map((x) => x)),
        category:
            json["category"] == null ? null : Domain.fromJson(json["category"]),
      );

  Map<String, dynamic> toJson() => {
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "speculationId": speculationId,
        "name": name,
        "imageUrl": imageUrl,
        "duration": duration,
        "active": active,
        "unitsOfMeasurement": unitsOfMeasurement == null
            ? []
            : List<dynamic>.from(unitsOfMeasurement!.map((x) => x)),
        "category": category?.toJson(),
      };
}

class User {
  int? id;
  String? username;
  String? firstname;
  String? lastname;
  String? gender;
  DateTime? birthdate;
  String? phone;
  String? adress;
  String? title;
  String? email;
  dynamic photo;
  String? coordonates;
  String? localisation;
  String? principalSpeculation;
  double? notation;
  Account? account;

  User({
    this.id,
    this.username,
    this.firstname,
    this.lastname,
    this.gender,
    this.birthdate,
    this.phone,
    this.adress,
    this.title,
    this.email,
    this.photo,
    this.coordonates,
    this.localisation,
    this.principalSpeculation,
    this.notation,
    this.account,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        username: json["username"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        gender: json["gender"],
        birthdate: json["birthdate"] == null
            ? null
            : DateTime.parse(json["birthdate"]),
        phone: json["phone"],
        adress: json["adress"],
        title: json["title"],
        email: json["email"],
        photo: json["photo"],
        coordonates: json["coordonates"],
        localisation: json["localisation"],
        principalSpeculation: json["principalSpeculation"],
        notation: json["notation"],
        account:
            json["account"] == null ? null : Account.fromJson(json["account"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "username": username,
        "firstname": firstname,
        "lastname": lastname,
        "gender": gender,
        "birthdate": birthdate?.toIso8601String(),
        "phone": phone,
        "adress": adress,
        "title": title,
        "email": email,
        "photo": photo,
        "coordonates": coordonates,
        "localisation": localisation,
        "principalSpeculation": principalSpeculation,
        "notation": notation,
        "account": account?.toJson(),
      };
}

class Account {
  int? id;
  String? identifier;
  bool? enabledFactAuth;
  dynamic factAuthChanel;
  dynamic factAuthLine;
  bool? active;
  DateTime? createdAt;
  dynamic deleted;
  dynamic deletedAt;
  dynamic updatedAt;
  List<dynamic>? scopes;

  Account({
    this.id,
    this.identifier,
    this.enabledFactAuth,
    this.factAuthChanel,
    this.factAuthLine,
    this.active,
    this.createdAt,
    this.deleted,
    this.deletedAt,
    this.updatedAt,
    this.scopes,
  });

  factory Account.fromJson(Map<String, dynamic> json) => Account(
        id: json["id"],
        identifier: json["identifier"],
        enabledFactAuth: json["enabledFactAuth"],
        factAuthChanel: json["factAuthChanel"],
        factAuthLine: json["factAuthLine"],
        active: json["active"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        deleted: json["deleted"],
        deletedAt: json["deletedAt"],
        updatedAt: json["updatedAt"],
        scopes: json["scopes"] == null
            ? []
            : List<dynamic>.from(json["scopes"]!.map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "identifier": identifier,
        "enabledFactAuth": enabledFactAuth,
        "factAuthChanel": factAuthChanel,
        "factAuthLine": factAuthLine,
        "active": active,
        "createdAt": createdAt?.toIso8601String(),
        "deleted": deleted,
        "deletedAt": deletedAt,
        "updatedAt": updatedAt,
        "scopes":
            scopes == null ? [] : List<dynamic>.from(scopes!.map((x) => x)),
      };
}
