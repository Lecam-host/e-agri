// To parse this JSON data, do
//
//     final typeInsuranceModel = typeInsuranceModelFromJson(jsonString);

import 'dart:convert';

TypeOffreInsuranceModel typeInsuranceModelFromJson(String str) => TypeOffreInsuranceModel.fromJson(json.decode(str));

String typeInsuranceModelToJson(TypeOffreInsuranceModel data) => json.encode(data.toJson());

class TypeOffreInsuranceModel {
    int? statusCode;
    String? statusMessage;
    List<DataTypeOffreInsuranceModel>? data;

    TypeOffreInsuranceModel({
        this.statusCode,
        this.statusMessage,
        this.data,
    });

    factory TypeOffreInsuranceModel.fromJson(Map<String, dynamic> json) => TypeOffreInsuranceModel(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: json["data"] == null ? [] : List<DataTypeOffreInsuranceModel>.from(json["data"]!.map((x) => DataTypeOffreInsuranceModel.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data == null ? [] : List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class DataTypeOffreInsuranceModel {
    int? id;
    String? name;
    String? description;
    dynamic illustrationImage;
    dynamic insurances;
    bool? isActive;
    bool? isDeleted;
    DateTime? createdAt;
    DateTime? updatedAt;
    dynamic deletedAt;

    DataTypeOffreInsuranceModel({
        this.id,
        this.name,
        this.description,
        this.illustrationImage,
        this.insurances,
        this.isActive,
        this.isDeleted,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
    });

    factory DataTypeOffreInsuranceModel.fromJson(Map<String, dynamic> json) => DataTypeOffreInsuranceModel(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        illustrationImage: json["illustrationImage"],
        insurances: json["insurances"],
        isActive: json["isActive"],
        isDeleted: json["isDeleted"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        deletedAt: json["deletedAt"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "illustrationImage": illustrationImage,
        "insurances": insurances,
        "isActive": isActive,
        "isDeleted": isDeleted,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "deletedAt": deletedAt,
    };
}
