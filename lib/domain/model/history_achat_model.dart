import 'dart:convert';

HistoryAchatResponse historyAchatResponseFromJson(String str) =>
    HistoryAchatResponse.fromJson(json.decode(str));

String historyAchatResponseToJson(HistoryAchatResponse data) =>
    json.encode(data.toJson());

class HistoryAchatResponse {
  int statusCode;
  String statusMessage;
  List<AchatElement> data;

  HistoryAchatResponse({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  factory HistoryAchatResponse.fromJson(Map<String, dynamic> json) =>
      HistoryAchatResponse(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: List<AchatElement>.from(
            json["data"].map((x) => AchatElement.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class AchatElement {
  String id;
  String ip;
  String service;
  String action;
  // dynamic user;
  Data data;
  // DateTime date;

  AchatElement({
    required this.id,
    required this.ip,
    required this.service,
    required this.action,
    // this.user,
    required this.data,
    // required this.date,
  });

  factory AchatElement.fromJson(Map<String, dynamic> json) => AchatElement(
        id: json["id"],
        ip: json["ip"],
        service: json["service"],
        action: json["action"],
        // user: json["user"],
        data: Data.fromJson(json["data"]),
        // date: DateTime.parse(json["date"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "ip": ip,
        "service": service,
        "action": action,
        // "user": user,
        "data": data.toJson(),
        // "date": date.toIso8601String(),
      };
}

class Data {
  int amount;
  DateTime datePaiement;
  String typePaiement;
  String typeVente;
  int customer;
  CustomerInfo customerInfo;
  List<ProductElement> products;

  Data({
    required this.amount,
    required this.datePaiement,
    required this.typePaiement,
    required this.typeVente,
    required this.customer,
    required this.customerInfo,
    required this.products,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        amount: json["amount"],
        datePaiement: DateTime.parse(json["datePaiement"]),
        typePaiement: json["typePaiement"],
        typeVente: json["typeVente"],
        customer: json["customer"],
        customerInfo: CustomerInfo.fromJson(json["customerInfo"]),
        products: List<ProductElement>.from(
            json["products"].map((x) => ProductElement.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "amount": amount,
        "datePaiement": datePaiement.toIso8601String(),
        "typePaiement": typePaiement,
        "typeVente": typeVente,
        "customer": customer,
        "customerInfo": customerInfo.toJson(),
        "products": List<dynamic>.from(products.map((x) => x.toJson())),
      };
}

class CustomerInfo {
  int id;
  String username;
  String firstname;
  String lastname;
  String gender;
  String phone;
  String adress;
  String email;

  CustomerInfo({
    required this.id,
    required this.username,
    required this.firstname,
    required this.lastname,
    required this.gender,
    required this.phone,
    required this.adress,
    required this.email,
  });

  factory CustomerInfo.fromJson(Map<String, dynamic> json) => CustomerInfo(
        id: json["id"],
        username: json["username"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        gender: json["gender"],
        phone: json["phone"],
        adress: json["adress"],
        email: json["email"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "username": username,
        "firstname": firstname,
        "lastname": lastname,
        "gender": gender,
        "phone": phone,
        "adress": adress,
        "email": email,
      };
}

class ProductElement {
  int id;
  int idProduct;
  int quantity;
  int price;
  int fournisseurId;
  ProductProduct product;
  CustomerInfo fournisseur;

  ProductElement({
    required this.id,
    required this.idProduct,
    required this.quantity,
    required this.price,
    required this.fournisseurId,
    required this.product,
    required this.fournisseur,
  });

  factory ProductElement.fromJson(Map<String, dynamic> json) => ProductElement(
        id: json["id"],
        idProduct: json["idProduct"],
        quantity: json["quantity"],
        price: json["price"],
        fournisseurId: json["fournisseurId"],
        product: ProductProduct.fromJson(json["product"]),
        fournisseur: CustomerInfo.fromJson(json["fournisseur"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "idProduct": idProduct,
        "quantity": quantity,
        "price": price,
        "fournisseurId": fournisseurId,
        "product": product.toJson(),
        "fournisseur": fournisseur.toJson(),
      };
}

class ProductProduct {
  int id;
  String code;
  String description;
  String availability;
  String availabilityDate;
  String typeVente;
  String offerType;
  double notation;
  String status;
  DateTime publicationDate;
  Location location;
  List<Image> images;
  int categoryId;
  //Category? category;
  int? speculationId;
  Category ? speculation;

  ProductProduct({
    required this.id,
    required this.code,
    required this.description,
    required this.availability,
    required this.availabilityDate,
    required this.typeVente,
    required this.offerType,
    required this.notation,
    required this.status,
    required this.publicationDate,
    required this.location,
    required this.images,
    required this.categoryId,
    // required this.category,
     this.speculationId,
     this.speculation,
  });

  factory ProductProduct.fromJson(Map<String, dynamic> json) => ProductProduct(
        id: json["id"],
        code: json["code"],
        description: json["description"],
        availability: json["availability"],
        availabilityDate: json["availabilityDate"],
        typeVente: json["typeVente"],
        offerType: json["offerType"],
        notation: json["notation"],
        status: json["status"],
        publicationDate: DateTime.parse(json["publicationDate"]),
        location: Location.fromJson(json["location"]),
        images: List<Image>.from(json["images"].map((x) => Image.fromJson(x))),
        categoryId: json["categoryId"],
        // category: json["category"] != null
        //     ? Category.fromJson(json["category"])
        //     : null,
        speculationId: json["speculationId"],
        speculation:json["speculation"]!=null? Category.fromJson(json["speculation"]):null,
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "code": code,
        "description": description,
        "availability": availability,
        "availabilityDate": availabilityDate,
        "typeVente": typeVente,
        "offerType": offerType,
        "notation": notation,
        "status": status,
        "publicationDate": publicationDate.toIso8601String(),
        "location": location.toJson(),
        "images": List<dynamic>.from(images.map((x) => x.toJson())),
        "categoryId": categoryId,
        // "category": category.toJson(),
        "speculationId": speculationId,
        //"speculation": speculation.toJson(),
      };
}

class Category {
  int id;
  String libelle;

  Category({
    required this.id,
    required this.libelle,
  });

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"],
        libelle: json["libelle"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "libelle": libelle,
      };
}

class Image {
  int id;
  String link;

  Image({
    required this.id,
    required this.link,
  });

  factory Image.fromJson(Map<String, dynamic> json) => Image(
        id: json["id"],
        link: json["link"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "link": link,
      };
}

class Location {
  int id;
  int regionId;
  int? departementId;
  int? sprefectureId;
  int? localiteId;

  Location({
    required this.id,
    required this.regionId,
    this.departementId,
    this.sprefectureId,
    this.localiteId,
  });

  factory Location.fromJson(Map<String, dynamic> json) => Location(
        id: json["id"],
        regionId: json["regionId"],
        departementId: json["departementId"],
        sprefectureId: json["sprefectureId"],
        localiteId: json["localiteId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "regionId": regionId,
        "departementId": departementId,
        "sprefectureId": sprefectureId,
        "localiteId": localiteId,
      };
}
