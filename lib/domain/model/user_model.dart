import 'package:flutter/material.dart';

class AuthModel {
  UserModel? userInfo;

  AuthModel(
    this.userInfo,
  );
}

class UserModel with ChangeNotifier {
  int? id;
  String? token;
  String? userName;
  String? firebaseToken;
  String? lastName;
  String? firstName;
  String? number;
  String? photo;
  bool? isConnected;
  String? gender;
  String? birthdate;
  List<String>? scopes=[];
  double? rate;


  UserModel({
    this.userName,
    this.lastName,
    this.firstName,
    this.number,
    this.id,
    this.token,
    this.firebaseToken,
    this.isConnected,
    this.photo,
    this.gender,
    this.birthdate,
    this.scopes,
    this.rate,

  });

  void signalUpdated(UserModel userInfo) {
    userName = userInfo.userName;
    id = userInfo.id;
    token = userInfo.token;
    lastName = userInfo.lastName;
    firstName = userInfo.firstName;
    number = userInfo.number;
    isConnected = userInfo.isConnected;
    photo = userInfo.photo;
    firebaseToken = userInfo.firebaseToken;
    gender = userInfo.gender;
    birthdate = userInfo.birthdate;
    scopes = userInfo.scopes;
    rate = userInfo.rate;

    notifyListeners();
  }
}
