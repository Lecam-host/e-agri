import 'dart:convert';

import 'cart_model.dart';

GetUserCommands getUserCommandsFromJson(String str) =>
    GetUserCommands.fromJson(json.decode(str));

String getUserCommandsToJson(GetUserCommands data) =>
    json.encode(data.toJson());

class GetUserCommands {
  GetUserCommands({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  int statusCode;
  String statusMessage;
  List<CommandModel> data;

  factory GetUserCommands.fromJson(Map<String, dynamic> json) {
    return GetUserCommands(
      statusCode: json["statusCode"],
      statusMessage: json["statusMessage"],
      data: List<CommandModel>.from(
          json["data"]["items"].map((x) => CommandModel.fromJson(x))),
    );
  }

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class CommandModel {
  CommandModel({
    required this.id,
    required this.createdAt,
    required this.numberCommande,
    required this.shoppingCart,
    required this.status,
  });

  int id;
  DateTime createdAt;
  ShoppingCart shoppingCart;
  Status status;
  String numberCommande;

  factory CommandModel.fromJson(Map<String, dynamic> json) => CommandModel(
        id: json["id"],
        createdAt: DateTime.parse(json["createdAt"]),
        shoppingCart: ShoppingCart.fromJson(json["shoppingCart"]),
        status: Status.fromJson(json["status"]),
        numberCommande: json["numberCommande"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "createdAt": createdAt.toIso8601String(),
        "shoppingCart": shoppingCart.toJson(),
        "status": status.toJson(),
        "numberCommande": numberCommande,
      };
}

class ShoppingCart {
  ShoppingCart({
    required this.id,
    required this.userId,
    required this.cartItems,
    required this.createdAt,
    required this.status,
    required this.totalPrice,
  });

  int id;
  int userId;
  List<CartItem> cartItems;

  DateTime createdAt;
  String status;
  int totalPrice;

  factory ShoppingCart.fromJson(Map<String, dynamic> json) => ShoppingCart(
        id: json["id"],
        userId: json["userId"],
        cartItems: List<CartItem>.from(
            json["cartItems"].map((x) => CartItem.fromJson(x))),
        createdAt: DateTime.parse(json["createdAt"]),
        status: json["status"],
        totalPrice: json["totalPrice"].toInt(),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "userId": userId,
        "cartItems": List<dynamic>.from(cartItems.map((x) => x.toJson())),
        "createdAt": createdAt.toIso8601String(),
        "status": status,
        "totalPrice": totalPrice,
      };
}

class Status {
  Status({
    required this.id,
    required this.libelle,
  });

  int id;
  String libelle;

  factory Status.fromJson(Map<String, dynamic> json) => Status(
        id: json["id"],
        libelle: json["libelle"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "libelle": libelle,
      };
}

CreateCommandModel createCommandModelFromJson(String str) =>
    CreateCommandModel.fromJson(json.decode(str));

String createCommandModelToJson(CreateCommandModel data) =>
    json.encode(data.toJson());

class CreateCommandModel {
  CreateCommandModel({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  int statusCode;
  String statusMessage;
  CommandModel data;

  factory CreateCommandModel.fromJson(Map<String, dynamic> json) =>
      CreateCommandModel(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: CommandModel.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data.toJson(),
      };
}

GetCommandFactureResponse getCommandFactureResponseFromJson(String str) =>
    GetCommandFactureResponse.fromJson(json.decode(str));

String getCommandFactureResponseToJson(GetCommandFactureResponse data) =>
    json.encode(data.toJson());

class GetCommandFactureResponse {
  GetCommandFactureResponse({
    required this.statusCode,
    required this.statusMessage,
    required this.factureData,
  });

  int statusCode;
  String statusMessage;
  FactureData factureData;

  factory GetCommandFactureResponse.fromJson(Map<String, dynamic> json) =>
      GetCommandFactureResponse(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        factureData: FactureData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": factureData.toJson(),
      };
}

class FactureData {
  FactureData({
    required this.id,
    required this.orderId,
    required this.user,
    required this.invoiceUrl,
    required this.createdAt,
  });

  int id;
  int orderId;
  int user;
  String invoiceUrl;
  DateTime createdAt;

  factory FactureData.fromJson(Map<String, dynamic> json) => FactureData(
        id: json["id"],
        orderId: json["orderId"],
        user: json["user"],
        invoiceUrl: json["invoiceUrl"],
        createdAt: DateTime.parse(json["createdAt"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "orderId": orderId,
        "user": user,
        "invoiceUrl": invoiceUrl,
        "createdAt": createdAt.toIso8601String(),
      };
}
