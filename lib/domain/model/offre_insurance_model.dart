// To parse this JSON data, do
//
//     final offreInsuranceModel = offreInsuranceModelFromJson(jsonString);

import 'dart:convert';

OffreInsuranceModel offreInsuranceModelFromJson(String str) => OffreInsuranceModel.fromJson(json.decode(str));

String offreInsuranceModelToJson(OffreInsuranceModel data) => json.encode(data.toJson());

class OffreInsuranceModel {
    int? statusCode;
    String? statusMessage;
    DataOffreInsuranceModel? data;

    OffreInsuranceModel({
        this.statusCode,
        this.statusMessage,
        this.data,
    });

    factory OffreInsuranceModel.fromJson(Map<String, dynamic> json) => OffreInsuranceModel(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: json["data"] == null ? null : DataOffreInsuranceModel.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data?.toJson(),
    };
}

class DataOffreInsuranceModel {
    int? total;
    int? totalOfPages;
    int? perPage;
    dynamic sorts;
    dynamic order;
    int? currentPage;
    int? nextPage;
    int? previousPage;
    dynamic filters;
    List<ItemOffreInsuranceModel>? items;

    DataOffreInsuranceModel({
        this.total,
        this.totalOfPages,
        this.perPage,
        this.sorts,
        this.order,
        this.currentPage,
        this.nextPage,
        this.previousPage,
        this.filters,
        this.items,
    });

    factory DataOffreInsuranceModel.fromJson(Map<String, dynamic> json) => DataOffreInsuranceModel(
        total: json["total"],
        totalOfPages: json["total_of_pages"],
        perPage: json["per_page"],
        sorts: json["sorts"],
        order: json["order"],
        currentPage: json["current_page"],
        nextPage: json["next_page"],
        previousPage: json["previous_page"],
        filters: json["filters"],
        items: json["items"] == null ? [] : List<ItemOffreInsuranceModel>.from(json["items"]!.map((x) => ItemOffreInsuranceModel.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "total": total,
        "total_of_pages": totalOfPages,
        "per_page": perPage,
        "sorts": sorts,
        "order": order,
        "current_page": currentPage,
        "next_page": nextPage,
        "previous_page": previousPage,
        "filters": filters,
        "items": items == null ? [] : List<dynamic>.from(items!.map((x) => x.toJson())),
    };
}

class ItemOffreInsuranceModel {
    int? id;
    String? name;
    String? description;
    String? illustrationImage;
    List<FileElement>? files;
    String? webLink;
    List<String>? contacts;
    Insurer? insurer;
    Insurer? typeInsurance;
    List<ItemOffreInsuranceModel>? subInsurances;
    bool? isActive;
    bool? isDeleted;
    DateTime? createdAt;
    DateTime? updatedAt;
    dynamic deletedAt;
    double? price;

    ItemOffreInsuranceModel({
        this.id,
        this.name,
        this.description,
        this.illustrationImage,
        this.files,
        this.webLink,
        this.contacts,
        this.insurer,
        this.typeInsurance,
        this.subInsurances,
        this.isActive,
        this.isDeleted,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
        this.price,
    });

    factory ItemOffreInsuranceModel.fromJson(Map<String, dynamic> json) => ItemOffreInsuranceModel(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        illustrationImage: json["illustrationImage"],
       files: json["files"] == null ? [] : List<FileElement>.from(json["files"]!.map((x) => FileElement.fromJson(x))),
        webLink: json["webLink"],
        contacts: json["contacts"] == null ? [] : List<String>.from(json["contacts"]!.map((x) => x)),
        insurer: json["insurer"] == null ? null : Insurer.fromJson(json["insurer"]),
        typeInsurance: json["typeInsurance"] == null ? null : Insurer.fromJson(json["typeInsurance"]),
        subInsurances: json["subInsurances"] == null ? [] : List<ItemOffreInsuranceModel>.from(json["subInsurances"]!.map((x) => ItemOffreInsuranceModel.fromJson(x))),
        isActive: json["isActive"],
        isDeleted: json["isDeleted"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        deletedAt: json["deletedAt"],
        price: json["price"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "illustrationImage": illustrationImage,
        "files": files == null ? [] : List<FileElement>.from(files!.map((x) => x)),
        "webLink": webLink,
        "contacts": contacts == null ? [] : List<dynamic>.from(contacts!.map((x) => x)),
        "insurer": insurer?.toJson(),
        "typeInsurance": typeInsurance?.toJson(),
        "subInsurances": subInsurances == null ? [] : List<dynamic>.from(subInsurances!.map((x) => x.toJson())),
        "isActive": isActive,
        "isDeleted": isDeleted,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "deletedAt": deletedAt,
        "price": price,
    };
}
class FileElement {
    String? link;
    int? id;
    String? type;

    FileElement({
        this.link,
        this.id,
        this.type,
    });

    factory FileElement.fromJson(Map<String, dynamic> json) => FileElement(
        link: json["link"],
        id: json["id"],
        type: json["type"],
    );

    Map<String, dynamic> toJson() => {
        "link": link,
        "id": id,
        "type": type,
    };
}
class Insurer {
    String? name;
    int? id;

    Insurer({
        this.name,
        this.id,
    });

    factory Insurer.fromJson(Map<String, dynamic> json) => Insurer(
        name: json["name"],
        id: json["id"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "id": id,
    };
}
