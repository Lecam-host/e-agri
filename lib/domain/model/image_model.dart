class ImageProduct {
  ImageProduct({
    required this.id,
    required this.link,
  });

  int id;
  String link;

  factory ImageProduct.fromJson(Map<String, dynamic> json) => ImageProduct(
        id: json["id"],
        link: json["link"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "link": link,
      };
}
