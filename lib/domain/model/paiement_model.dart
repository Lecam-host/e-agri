import 'dart:convert';

import 'package:eagri/domain/model/Product.dart';

import 'user_model.dart';

CreatePaiementResponse createPaiemenrResponseFromJson(String str) =>
    CreatePaiementResponse.fromJson(json.decode(str));

String createPaiemenrResponseToJson(CreatePaiementResponse data) =>
    json.encode(data.toJson());

class CreatePaiementResponse {
  CreatePaiementResponse({
    required this.statusCode,
    required this.statusMessage,
    this.data,
  });

  int statusCode;
  String statusMessage;
  Data? data;

  factory CreatePaiementResponse.fromJson(Map<String, dynamic> json) =>
      CreatePaiementResponse(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: json["data"] != null ? Data.fromJson(json["data"]) : null,
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data != null ? data!.toJson() : null,
      };
}

class Data {
  Data({
    this.signature,
    this.paymentMethod,
    this.instruction,
    this.otpLength,
    this.otp,
  });

  String? signature;
  String? paymentMethod;
  String? instruction;
  String? otpLength;
  String? otp;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        signature: json["signature"],
        paymentMethod: json["payment_method"],
        instruction: json["instruction"],
        otpLength: json["otp_length"],
        otp: json["otp"],
      );

  Map<String, dynamic> toJson() => {
        "signature": signature,
        "payment_method": paymentMethod,
        "instruction": instruction,
        "otp_length": otpLength,
        "otp": otp,
      };
}

VerificationPaymentResponse verificationPaymentResponseFromJson(String str) =>
    VerificationPaymentResponse.fromJson(json.decode(str));

String verificationPaymentResponseToJson(VerificationPaymentResponse data) =>
    json.encode(data.toJson());

class VerificationPaymentResponse {
  VerificationPaymentResponse({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  int statusCode;
  String statusMessage;
  VerificationPaymentResponseData data;

  factory VerificationPaymentResponse.fromJson(Map<String, dynamic> json) =>
      VerificationPaymentResponse(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: VerificationPaymentResponseData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data.toJson(),
      };
}

class VerificationPaymentResponseData {
  VerificationPaymentResponseData({
    required this.signature,
    required this.message,
    required this.amount,
    required this.phone,
    required this.status,
    required this.invoiceUrl,
  });

  String signature;
  String message;
  int amount;
  String phone;
  String status;
  String invoiceUrl;

  factory VerificationPaymentResponseData.fromJson(Map<String, dynamic> json) =>
      VerificationPaymentResponseData(
        signature: json["signature"],
        message: json["message"],
        amount: json["amount"],
        phone: json["phone"],
        status: json["status"],
        invoiceUrl: json["invoiceUrl"],
      );

  Map<String, dynamic> toJson() => {
        "signature": signature,
        "message": message,
        "amount": amount,
        "phone": phone,
        "status": status,
        "invoiceUrl": invoiceUrl,
      };
}

ConfirmPaymentResponse confirmPaymentResponseFromJson(String str) =>
    ConfirmPaymentResponse.fromJson(json.decode(str));

String confirmPaymentResponseToJson(ConfirmPaymentResponse data) =>
    json.encode(data.toJson());

class ConfirmPaymentResponse {
  ConfirmPaymentResponse({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  int statusCode;
  String statusMessage;
  ConfirmPaymentResponseData data;

  factory ConfirmPaymentResponse.fromJson(Map<String, dynamic> json) =>
      ConfirmPaymentResponse(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: ConfirmPaymentResponseData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data.toJson(),
      };
}

class ConfirmPaymentResponseData {
  ConfirmPaymentResponseData({
    required this.transactionId,
    required this.signature,
    required this.message,
  });

  String transactionId;
  String signature;
  String message;

  factory ConfirmPaymentResponseData.fromJson(Map<String, dynamic> json) =>
      ConfirmPaymentResponseData(
        transactionId: json["transactionId"],
        signature: json["signature"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "transactionId": transactionId,
        "signature": signature,
        "message": message,
      };
}

GetListDemandePaiementCashItem getListDemandePaiementCashItemFromJson(
        String str) =>
    GetListDemandePaiementCashItem.fromJson(json.decode(str));

String getListDemandePaiementCashItemToJson(
        GetListDemandePaiementCashItem data) =>
    json.encode(data.toJson());

class GetListDemandePaiementCashItem {
  GetListDemandePaiementCashItem({
    required this.statusCode,
    required this.statusMessage,
    required this.items,
  });

  int statusCode;
  String statusMessage;
  List<DemandePaiementCashItem> items;

  factory GetListDemandePaiementCashItem.fromJson(Map<String, dynamic> json) =>
      GetListDemandePaiementCashItem(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        items: List<DemandePaiementCashItem>.from(json["data"]["items"]
            .map((x) => DemandePaiementCashItem.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "items": items,
      };
}

class DemandePaiementCashItem {
  DemandePaiementCashItem({
    required this.id,
    required this.transactionId,
    required this.orderId,
    required this.methodId,
    required this.productId,
    required this.productCode,
    required this.quantity,
    required this.unitPrice,
    required this.speculationId,
    required this.buyerId,
    required this.sellerId,
    this.confirmationCode,
    this.sellerAgrees,
    this.isConfirmed,
    this.message,
    this.vendeur,
    this.acheteur,
    this.product,
    this.timeUnit,

    this.isReceive = false,
    required this.createdAt,
  });

  int id;
  String transactionId;
  int orderId;
  String methodId;
  int productId;
  String productCode;
  int quantity;
  int unitPrice;
  int speculationId;
  int buyerId;
  int sellerId;
  UserModel? vendeur;
  UserModel? acheteur;
  ProductModel? product;
  bool isReceive;
  dynamic confirmationCode;
  bool? sellerAgrees;
  dynamic isConfirmed;
  String? timeUnit;

  dynamic message;
  DateTime createdAt;

  factory DemandePaiementCashItem.fromJson(Map<String, dynamic> json) =>
      DemandePaiementCashItem(
        id: json["id"],
        transactionId: json["transactionId"],
        orderId: json["orderId"],
        methodId: json["methodId"],
        productId: json["productId"],
        productCode: json["productCode"],
        quantity: json["quantity"],
        unitPrice: json["unitPrice"],
        speculationId: json["speculationId"],
        buyerId: json["buyerId"],
        sellerId: json["sellerId"],
        confirmationCode: json["confirmationCode"],
        sellerAgrees: json["sellerAgrees"],
        isConfirmed: json["isConfirmed"],
        message: json["message"],
        timeUnit: json["timeUnit"],

        createdAt: DateTime.parse(json["createdAt"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "transactionId": transactionId,
        "orderId": orderId,
        "methodId": methodId,
        "productId": productId,
        "productCode": productCode,
        "quantity": quantity,
        "unitPrice": unitPrice,
        "speculationId": speculationId,
        "buyerId": buyerId,
        "sellerId": sellerId,
        "confirmationCode": confirmationCode,
        "sellerAgrees": sellerAgrees,
        "isConfirmed": isConfirmed,
        "message": message,
        "timeUnit": timeUnit,

        "createdAt": createdAt.toIso8601String(),
      };
}
