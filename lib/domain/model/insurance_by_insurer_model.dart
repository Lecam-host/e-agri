// To parse this JSON data, do
//
//     final insuranceByInsurerModel = insuranceByInsurerModelFromJson(jsonString);

import 'dart:convert';


import 'offre_insurance_model.dart';

InsuranceByInsurerModel insuranceByInsurerModelFromJson(String str) =>
    InsuranceByInsurerModel.fromJson(json.decode(str));

String insuranceByInsurerModelToJson(InsuranceByInsurerModel data) =>
    json.encode(data.toJson());

class InsuranceByInsurerModel {
  int? statusCode;
  String? statusMessage;
  List<ItemOffreInsuranceModel>? data;
  InsuranceByInsurerModel({
    this.statusCode,
    this.statusMessage,
    this.data,
  });

  factory InsuranceByInsurerModel.fromJson(Map<String, dynamic> json) =>
      InsuranceByInsurerModel(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: json["data"] == null
            ? []
            : List<ItemOffreInsuranceModel>.from(
                json["data"]!.map((x) => ItemOffreInsuranceModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}