import 'dart:convert';

AddUserFavoriteResponse addUserFavoriteResponseFromJson(String str) =>
    AddUserFavoriteResponse.fromJson(json.decode(str));

String addUserFavoriteResponseToJson(AddUserFavoriteResponse data) =>
    json.encode(data.toJson());

class AddUserFavoriteResponse {
  AddUserFavoriteResponse({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  int statusCode;
  String statusMessage;
  List<FavoriteModel> data;

  factory AddUserFavoriteResponse.fromJson(Map<String, dynamic> json) =>
      AddUserFavoriteResponse(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: List<FavoriteModel>.from(
            json["data"].map((x) => FavoriteModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class AddUserFavoriteResponseData {
  AddUserFavoriteResponseData({
    required this.createdAt,
    required this.deleted,
    required this.deletedAt,
    required this.updatedAt,
    required this.id,
    required this.userId,
    required this.speculationId,
  });

  DateTime createdAt;
  bool deleted;
  DateTime deletedAt;
  DateTime updatedAt;
  int id;
  int userId;
  int speculationId;

  factory AddUserFavoriteResponseData.fromJson(Map<String, dynamic> json) =>
      AddUserFavoriteResponseData(
        createdAt: DateTime.parse(json["createdAt"]),
        deleted: json["deleted"],
        deletedAt: DateTime.parse(json["deletedAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        id: json["id"],
        userId: json["userId"],
        speculationId: json["speculationId"],
      );

  Map<String, dynamic> toJson() => {
        "createdAt": createdAt.toIso8601String(),
        "deleted": deleted,
        "deletedAt": deletedAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "id": id,
        "userId": userId,
        "speculationId": speculationId,
      };
}

ListUserFavoriteResponse listUserFavoriteResponseFromJson(String str) =>
    ListUserFavoriteResponse.fromJson(json.decode(str));

String listUserFavoriteResponseToJson(ListUserFavoriteResponse data) =>
    json.encode(data.toJson());

class ListUserFavoriteResponse {
  ListUserFavoriteResponse({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  int statusCode;
  String statusMessage;
  List<FavoriteModel> data;

  factory ListUserFavoriteResponse.fromJson(Map<String, dynamic> json) =>
      ListUserFavoriteResponse(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: List<FavoriteModel>.from(
            json["data"].map((x) => FavoriteModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class FavoriteModel {
  FavoriteModel({
    required this.speculationId,
    required this.idFavorite,
  });

  int speculationId;
  int idFavorite;
  factory FavoriteModel.fromJson(Map<String, dynamic> json) => FavoriteModel(
        speculationId: json["speculationId"],
        idFavorite: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "speculationId": speculationId,
        "id": idFavorite,
      };
}

class Category {
  Category({
    required this.categoryId,
    required this.name,
  });

  int categoryId;
  String name;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        categoryId: json["categoryId"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "categoryId": categoryId,
        "name": name,
      };
}
