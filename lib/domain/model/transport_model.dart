import 'dart:convert';

import 'Product.dart';
import 'image_model.dart';
import 'methode_paiement_model.dart';
import 'user_model.dart';

GetListCarResponse getListCarResponseFromJson(String str) =>
    GetListCarResponse.fromJson(json.decode(str));

String getListCarResponseToJson(GetListCarResponse data) =>
    json.encode(data.toJson());

class GetListCarResponse {
  GetListCarResponse({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  int statusCode;
  String statusMessage;
  Data data;

  factory GetListCarResponse.fromJson(Map<String, dynamic> json) =>
      GetListCarResponse(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data.toJson(),
      };
}

class Data {
  Data({
    required this.total,
    required this.totalOfPages,
    this.perPage,
    this.sorts,
    this.order,
    required this.currentPage,
    required this.nextPage,
    required this.previousPage,
    required this.listCars,
  });

  int total;
  int totalOfPages;
  dynamic perPage;
  dynamic sorts;
  dynamic order;
  int currentPage;
  int nextPage;
  int previousPage;
  List<CarModel> listCars;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        total: json["total"],
        totalOfPages: json["total_of_pages"],
        perPage: json["per_page"],
        sorts: json["sorts"],
        order: json["order"],
        currentPage: json["current_page"],
        nextPage: json["next_page"],
        previousPage: json["previous_page"],
        listCars:
            List<CarModel>.from(json["items"].map((x) => CarModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "total_of_pages": totalOfPages,
        "per_page": perPage,
        "sorts": sorts,
        "order": order,
        "current_page": currentPage,
        "next_page": nextPage,
        "previous_page": previousPage,
        "items": List<dynamic>.from(listCars.map((x) => x.toJson())),
      };
}

class CarModel {
  CarModel({
    required this.id,
    required this.libelle,
    required this.image,
    required this.categoryCar,
    required this.capacity,
    required this.unitOfMeasurment,
  });

  int id;
  String libelle;
  String image;
  CategoryCar categoryCar;
  int capacity;
  String unitOfMeasurment;

  factory CarModel.fromJson(Map<String, dynamic> json) => CarModel(
        id: json["id"],
        libelle: json["libelle"],
        image: json["image"],
        categoryCar: CategoryCar.fromJson(json["categoryVehicle"]),
        capacity: json["capacity"],
        unitOfMeasurment: json["unitOfMeasurment"]["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "libelle": libelle,
        "image": image,
        "categoryVehicle": categoryCar.toJson(),
        "capacity": capacity,
        "unitOfMeasurment": unitOfMeasurment,
      };
}

class CategoryCar {
  CategoryCar({
    required this.id,
    required this.libelle,
    required this.createdAt,
    required this.deleted,
    this.updatedAt,
    this.deletedAt,
  });

  int id;
  String libelle;
  DateTime createdAt;
  bool deleted;
  dynamic updatedAt;
  dynamic deletedAt;

  factory CategoryCar.fromJson(Map<String, dynamic> json) => CategoryCar(
        id: json["id"],
        libelle: json["libelle"],
        createdAt: DateTime.parse(json["createdAt"]),
        deleted: json["deleted"],
        updatedAt: json["updatedAt"],
        deletedAt: json["deletedAt"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "libelle": libelle,
        "createdAt": createdAt.toIso8601String(),
        "deleted": deleted,
        "updatedAt": updatedAt,
        "deletedAt": deletedAt,
      };
}

GetListTransportResponse getListTransportResponseFromJson(String str) =>
    GetListTransportResponse.fromJson(json.decode(str));

String getListTransportResponseToJson(GetListTransportResponse data) =>
    json.encode(data.toJson());

class GetListTransportResponse {
  GetListTransportResponse({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  int statusCode;
  String statusMessage;
  Data data;

  factory GetListTransportResponse.fromJson(Map<String, dynamic> json) =>
      GetListTransportResponse(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data.toJson(),
      };
}

class ListData {
  ListData({
    required this.total,
    required this.totalOfPages,
    required this.perPage,
    this.sorts,
    this.order,
    required this.currentPage,
    required this.nextPage,
    required this.previousPage,
    this.filters,
    required this.items,
  });

  int total;
  int totalOfPages;
  int perPage;
  dynamic sorts;
  dynamic order;
  int currentPage;
  int nextPage;
  int previousPage;
  dynamic filters;
  List<TransportModel> items;

  factory ListData.fromJson(Map<String, dynamic> json) => ListData(
        total: json["total"],
        totalOfPages: json["total_of_pages"],
        perPage: json["per_page"],
        sorts: json["sorts"],
        order: json["order"],
        currentPage: json["current_page"],
        nextPage: json["next_page"],
        previousPage: json["previous_page"],
        filters: json["filters"],
        items: List<TransportModel>.from(
            json["items"].map((x) => TransportModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "total_of_pages": totalOfPages,
        "per_page": perPage,
        "sorts": sorts,
        "order": order,
        "current_page": currentPage,
        "next_page": nextPage,
        "previous_page": previousPage,
        "filters": filters,
        "items": List<dynamic>.from(items.map((x) => x.toJson())),
      };
}

class TransportModel {
  TransportModel({
    required this.id,
    required this.publicationDate,
    required this.description,
    required this.code,
    required this.availability,
    this.availabilityDate,
    this.expirationDate,
    this.certification,
    this.typeVente,
    required this.location,
    required this.status,
    required this.paymentId,
    required this.product,
    required this.images,
    required this.offerType,
    required this.notation,
    this.paymentMethod,
    required this.createdAt,
    required this.updatedAt,
    required this.deleted,
    required this.fournisseurId,
  });

  int id;
  DateTime publicationDate;
  String description;
  String code;
  String availability;
  dynamic availabilityDate;
  DateTime? expirationDate;
  dynamic certification;
  dynamic typeVente;
  Location location;
  String status;
  int paymentId;
  Product product;
  List<ImageProduct>? images;
  String offerType;
  double notation;
  PaymentMethodModel? paymentMethod;
  DateTime createdAt;
  DateTime updatedAt;
  bool deleted;

  int fournisseurId;

  factory TransportModel.fromJson(Map<String, dynamic> json) => TransportModel(
        id: json["id"],
        publicationDate: DateTime.parse(json["publicationDate"]),
        description: json["description"],
        code: json["code"],
        availability: json["availability"],
        availabilityDate: json["availabilityDate"],
        expirationDate: json["expirationDate"] != null
            ? DateTime.parse(json["expirationDate"])
            : null,
        certification: json["certification"],
        typeVente: json["typeVente"],
        location: Location.fromJson(json["location"]),
        status: json["status"],
        paymentId: json["paymentId"],
        product: Product.fromJson(json["product"]),
        images: List<ImageProduct>.from(
            json["images"].map((x) => ImageProduct.fromJson(x))),
        offerType: json["offerType"],
        notation: json["notation"],
        paymentMethod:json["paymentMethod"]!=null? PaymentMethodModel.fromJson( json["paymentMethod"]):null,
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        deleted: json["deleted"],
        fournisseurId: json["fournisseurId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "publicationDate": publicationDate.toIso8601String(),
        "description": description,
        "code": code,
        "availability": availability,
        "availabilityDate": availabilityDate,
        "expirationDate": expirationDate != null
            ? "${expirationDate!.year.toString().padLeft(4, '0')}-${expirationDate!.month.toString().padLeft(2, '0')}-${expirationDate!.day.toString().padLeft(2, '0')}"
            : null,
        "certification": certification,
        "typeVente": typeVente,
        "location": location.toJson(),
        "status": status,
        "paymentId": paymentId,
        "product": product.toJson(),
        "images": images != null
            ? List<dynamic>.from(images!.map((x) => x.toJson()))
            : null,
        "offerType": offerType,
        "notation": notation,
        "paymentMethod": paymentMethod,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "deleted": deleted,
        "fournisseurId": fournisseurId,
      };
}

class Location {
  Location({
    required this.id,
    required this.regionId,
    this.departementId,
    this.sprefectureId,
    this.localiteId,
    this.nameRegion,
    this.nameDepartemnt,
    this.nameSp,
    this.nameLocalite,
  });

  int id;
  int regionId;
  int? departementId;
  int? sprefectureId;
  int? localiteId;
  String? nameRegion;
  String? nameDepartemnt;
  String? nameSp;
  String? nameLocalite;

  factory Location.fromJson(Map<String, dynamic> json) => Location(
        id: json["id"],
        regionId: json["regionId"],
        departementId: json["departementId"],
        sprefectureId: json["sprefectureId"],
        localiteId: json["localiteId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "regionId": regionId,
        "departementId": departementId,
        "sprefectureId": sprefectureId,
        "localiteId": localiteId,
      };
}

class Product {
  Product({
    required this.id,
    required this.identifiant,
    required this.categorieId,
    required this.speculationId,
    this.speculation,
    required this.trajet,
    required this.matriculeVehicle,
    required this.quantity,
    required this.initialQuantity,
    required this.priceU,
    required this.createdAt,
    required this.unitOfMeasurment,
  });

  int id;
  String identifiant;
  int categorieId;
  int speculationId;
  Speculation? speculation;
  Trajet trajet;
  String matriculeVehicle;
  int quantity;
  int initialQuantity;
  int priceU;
  DateTime createdAt;
  String unitOfMeasurment;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        id: json["id"],
        identifiant: json["identifiant"],
        categorieId: json["categorieId"],
        speculationId: json["speculationId"],
        speculation: json["speculation"] != null
            ? Speculation.fromJson(json["speculation"])
            : null,
        trajet: Trajet.fromJson(json["trajet"]),
        matriculeVehicle: json["matriculeVehicle"],
        quantity: json["quantity"],
        initialQuantity: json["initial_quantity"],
        priceU: json["price_u"].toInt(),
        createdAt: DateTime.parse(json["created_at"]),
        unitOfMeasurment: json["unitOfMeasurmentName"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "identifiant": identifiant,
        "categorieId": categorieId,
        "speculationId": speculationId,
        "speculation": speculation != null ? speculation!.toJson() : null,
        "trajet": trajet.toJson(),
        "matriculeVehicle": matriculeVehicle,
        "quantity": quantity,
        "initial_quantity": initialQuantity,
        "price_u": priceU,
        "created_at": createdAt.toIso8601String(),
        "unitOfMeasurmentName": unitOfMeasurment,
      };
}

class Speculation {
  Speculation({
    required this.id,
    required this.libelle,
    required this.image,
    required this.categoryVehicle,
    required this.capacity,
    // required this.unitOfMeasurment,
    required this.createdAt,
    required this.deleted,
  });

  int id;
  String libelle;
  String image;
  CategoryVehicle categoryVehicle;
  int capacity;
  // String unitOfMeasurment;
  DateTime createdAt;
  bool deleted;

  factory Speculation.fromJson(Map<String, dynamic> json) => Speculation(
        id: json["id"],
        libelle: json["libelle"],
        image: json["image"],
        categoryVehicle: CategoryVehicle.fromJson(json["categoryVehicle"]),
        capacity: json["capacity"],
        // unitOfMeasurment: json["unitOfMeasurmentName"],
        createdAt: DateTime.parse(json["createdAt"]),
        deleted: json["deleted"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "libelle": libelle,
        "image": image,
        "categoryVehicle": categoryVehicle.toJson(),
        "capacity": capacity,
        // "unitOfMeasurment": unitOfMeasurment,
        "createdAt": createdAt.toIso8601String(),
        "deleted": deleted,
      };
}

class CategoryVehicle {
  CategoryVehicle({
    required this.id,
    required this.libelle,
  });

  int id;
  String libelle;

  factory CategoryVehicle.fromJson(Map<String, dynamic> json) =>
      CategoryVehicle(
        id: json["id"],
        libelle: json["libelle"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "libelle": libelle,
      };
}

class Trajet {
  Trajet({
    required this.id,
    required this.start,
    required this.arrival,
    required this.fournisseurId,
  });

  int id;
  Location start;
  Location arrival;
  int fournisseurId;

  factory Trajet.fromJson(Map<String, dynamic> json) => Trajet(
        id: json["id"],
        start: Location.fromJson(json["start"]),
        arrival: Location.fromJson(json["arrival"]),
        fournisseurId: json["fournisseurId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "start": start.toJson(),
        "arrival": arrival.toJson(),
        "fournisseurId": fournisseurId,
      };
}

GetDemandeTransport getDemandeTransportRecuFromJson(String str) =>
    GetDemandeTransport.fromJson(json.decode(str));

String getDemandeTransportRecuToJson(GetDemandeTransport data) =>
    json.encode(data.toJson());

class GetDemandeTransport {
  GetDemandeTransport({
    required this.statusCode,
    required this.statusMessage,
    required this.dataDemandeTransport,
  });

  int statusCode;
  String statusMessage;
  DataDemandeTransport dataDemandeTransport;

  factory GetDemandeTransport.fromJson(Map<String, dynamic> json) =>
      GetDemandeTransport(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        dataDemandeTransport: DataDemandeTransport.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": dataDemandeTransport.toJson(),
      };
}

class DataDemandeTransport {
  DataDemandeTransport({
    required this.total,
    required this.totalOfPages,
    required this.perPage,
    this.sorts,
    this.order,
    required this.currentPage,
    required this.nextPage,
    required this.previousPage,
    this.filters,
    required this.items,
  });

  int total;
  int totalOfPages;
  int perPage;
  dynamic sorts;
  dynamic order;
  int currentPage;
  int nextPage;
  int previousPage;
  dynamic filters;
  List<ItemDemandeTransport> items;

  factory DataDemandeTransport.fromJson(Map<String, dynamic> json) =>
      DataDemandeTransport(
        total: json["total"],
        totalOfPages: json["total_of_pages"],
        perPage: json["per_page"],
        sorts: json["sorts"],
        order: json["order"],
        currentPage: json["current_page"],
        nextPage: json["next_page"],
        previousPage: json["previous_page"],
        filters: json["filters"],
        items: List<ItemDemandeTransport>.from(
            json["items"].map((x) => ItemDemandeTransport.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "total_of_pages": totalOfPages,
        "per_page": perPage,
        "sorts": sorts,
        "order": order,
        "current_page": currentPage,
        "next_page": nextPage,
        "previous_page": previousPage,
        "filters": filters,
        "items": List<dynamic>.from(items.map((x) => x.toJson())),
      };
}

class ItemDemandeTransport {
  ItemDemandeTransport({
    required this.id,
    required this.offerId,
    required this.date,
    required this.hour,
    required this.capacity,
    required this.unitOfMeasurment,
    required this.description,
    required this.userId,
    required this.fournisseurId,
    required this.isValid,
    required this.terminate,
    required this.status,
    this.demandeur,
    this.receptionStatus,
    this.removalStatus,
    this.isReceive = false,
  });

  int id;
  int offerId;
  DateTime date;
  String hour;
  int capacity;
  String unitOfMeasurment;
  String description;
  int userId;
  int fournisseurId;
  bool isValid;
  bool isReceive;
  UserModel? demandeur;
  bool? removalStatus;
  bool? receptionStatus;
  bool terminate;
  String status;

  factory ItemDemandeTransport.fromJson(Map<String, dynamic> json) =>
      ItemDemandeTransport(
        id: json["id"],
        offerId: json["offerId"],
        date: DateTime.parse(json["date"]),
        hour: json["hour"],
        capacity: json["capacity"],
        unitOfMeasurment: json["unitOfMeasurment"],
        description: json["description"],
        userId: json["userId"],
        fournisseurId: json["fournisseurId"],
        isValid: json["isValid"],
        terminate: json["terminate"],
        status: json["status"],
        removalStatus: json["removalStatus"],
        receptionStatus: json["receptionStatus"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "offerId": offerId,
        "date":
            "${date.year.toString().padLeft(4, '0')}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}",
        "hour": hour,
        "capacity": capacity,
        "unitOfMeasurment": unitOfMeasurment,
        "description": description,
        "userId": userId,
        "fournisseurId": fournisseurId,
        "isValid": isValid,
        "terminate": terminate,
        "status": status,
        "removalStatus": removalStatus,
        "receptionStatus": receptionStatus,
      };
}

class DataDemandeForDetailModel {
  ProductModel prestation;
  UserModel user;
  ItemDemandeTransport prestationDemandeData;
  bool isReceive;
  DataDemandeForDetailModel({
    required this.prestation,
    required this.user,
    required this.prestationDemandeData,
    required this.isReceive,
  });
}

GetCategoryVehicule getCategoryVehiculeFromJson(String str) =>
    GetCategoryVehicule.fromJson(json.decode(str));

String getCategoryVehiculeToJson(GetCategoryVehicule data) =>
    json.encode(data.toJson());

class GetCategoryVehicule {
  GetCategoryVehicule({
    required this.statusCode,
    required this.statusMessage,
    required this.listCategorie,
  });

  int statusCode;
  String statusMessage;
  List<CategoryVehicle> listCategorie;

  factory GetCategoryVehicule.fromJson(Map<String, dynamic> json) =>
      GetCategoryVehicule(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        listCategorie: List<CategoryVehicle>.from(
            json["data"]["items"].map((x) => CategoryVehicle.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        // "data": listCategorie.toJson(),
      };
}
