import '../../data/responses/responses.dart';
import 'Product.dart';

class CartModel {
  int id, userId, totalPrice;
  List<CartItem> cartItem;
  String status, createdAt;
  CartModel({
    required this.id,
    required this.userId,
    required this.cartItem,
    required this.status,
    required this.createdAt,
    required this.totalPrice,
  });
}

// class Cart {
//   ProductModel? product;
//   int numOfItem;
//   int productPrice;

//   String id;
//   Cart(
//       {this.product,
//       this.numOfItem = 0,
//       required this.id,
//       required this.productPrice});

//   Cart copyWith({
//     required String id,
//     ProductModel? product,
//   }) =>
//       Cart(
//           id: id,
//           product: product ?? this.product,
//           numOfItem: numOfItem = 0,
//           productPrice: 0);

//   // @override
//   // List<Object> get props => [id, qty, product];

//   void toggleDone() {
//     numOfItem = numOfItem + 1;
//   }

//   void decreaseDown() {
//     numOfItem = numOfItem - 1;
//   }
// }

// // Demo data for our cart

// List<Cart> demoCarts = [
//   Cart(product: demoProducts[0], numOfItem: 2, id: "1", productPrice: 110),
//   Cart(product: demoProducts[1], numOfItem: 1, id: "2", productPrice: 110),
//   Cart(product: demoProducts[3], numOfItem: 1, id: "3", productPrice: 110),
// ];

class ListCartResponse extends BaseResponse {
  ListCartResponse({
    required this.data,
  });

  List<Cart> data;

  factory ListCartResponse.fromJson(Map<String, dynamic> json) =>
      ListCartResponse(
        data: List<Cart>.from(json["data"].map((x) => Cart.fromJson(x))),
      )..status = json["statusCode"];

  @override
  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Cart {
  Cart({
    required this.id,
    required this.userId,
    required this.cartItems,
    required this.createdAt,
    required this.status,
    required this.totalPrice,
  });

  int id;
  int userId;
  List<CartItem> cartItems;
  DateTime createdAt;
  String status;
  int totalPrice;

  factory Cart.fromJson(Map<String, dynamic> json) => Cart(
        id: json["id"],
        userId: json["userId"],
        cartItems: List<CartItem>.from(
            json["cartItems"].map((x) => CartItem.fromJson(x))),
        createdAt: DateTime.parse(json["createdAt"]),
        status: json["status"],
        totalPrice: json["totalPrice"].toInt(),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "userId": userId,
        "cartItems": List<dynamic>.from(cartItems.map((x) => x.toJson())),
        "createdAt": createdAt.toIso8601String(),
        "status": status,
        "totalPrice": totalPrice,
      };
}

class CartItem {
  CartItem({
    required this.id,
    required this.idProduct,
    required this.quantity,
    required this.price,
    required this.fournisseurId,
     this.paymentCode,
    this.status,
    this.product,
  });

  int id;
  int idProduct;
  int quantity;
  int price;
  int fournisseurId;
  String? status;
  ProductModel? product;
  String? paymentCode ;

  factory CartItem.fromJson(Map<String, dynamic> json) {
    return CartItem(
      id: json["id"],
      idProduct: json["idProduct"],
      quantity: json["quantity"],
      price: json["price"].toInt(),
      fournisseurId: json["fournisseurId"],
      status: json["status"],
      paymentCode: json["paymentCode"],

    );
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "idProduct": idProduct,
        "quantity": quantity,
        "price": price,
        "fournisseurId": fournisseurId,
      };

  void toggleDone() {
    quantity = quantity + 1;
  }

  void decreaseDown() {
    quantity = quantity - 1;
  }
}
