import 'dart:convert';

class OffreItemModel {
  int? id;
  String? name;
  String? description;
  String? illustrationImage;
  String? webLink;
  List<String>? contacts;
  Creditor? creditor;
  Creditor? typeCredit;
  List<dynamic>? subCredits;
  bool? isActive;
  bool? isDeleted;
  DateTime? createdAt;
  DateTime? updatedAt;
  dynamic deletedAt;

  OffreItemModel({
    this.id,
    this.name,
    this.description,
    this.illustrationImage,
    this.webLink,
    this.contacts,
    this.creditor,
    this.typeCredit,
    this.subCredits,
    this.isActive,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
  });

  factory OffreItemModel.fromJson(Map<String, dynamic> json) => OffreItemModel(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        illustrationImage: json["illustrationImage"],
        webLink: json["webLink"],
        contacts: json["contacts"] == null
            ? []
            : List<String>.from(json["contacts"]!.map((x) => x)),
        creditor: json["creditor"] == null
            ? null
            : Creditor.fromJson(json["creditor"]),
        typeCredit: json["typeCredit"] == null
            ? null
            : Creditor.fromJson(json["typeCredit"]),
        subCredits: json["subCredits"] == null
            ? []
            : List<dynamic>.from(json["subCredits"]!.map((x) => x)),
        isActive: json["isActive"],
        isDeleted: json["isDeleted"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        deletedAt: json["deletedAt"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "illustrationImage": illustrationImage,
        "webLink": webLink,
        "contacts":
            contacts == null ? [] : List<dynamic>.from(contacts!.map((x) => x)),
        "creditor": creditor?.toJson(),
        "typeCredit": typeCredit?.toJson(),
        "subCredits": subCredits == null
            ? []
            : List<dynamic>.from(subCredits!.map((x) => x)),
        "isActive": isActive,
        "isDeleted": isDeleted,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "deletedAt": deletedAt,
      };
}

class Creditor {
  String? name;
  int? id;

  Creditor({
    this.name,
    this.id,
  });

  factory Creditor.fromJson(Map<String, dynamic> json) => Creditor(
        name: json["name"],
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "id": id,
      };
}

// To parse this JSON data, do
//
//     final entrepriseModel = entrepriseModelFromJson(jsonString);

EntrepriseModel entrepriseModelFromJson(String str) =>
    EntrepriseModel.fromJson(json.decode(str));

String entrepriseModelToJson(EntrepriseModel data) =>
    json.encode(data.toJson());

class GetEntrepriseResponseModel {
  int statusCode;
  String statusMessage;
  EntrepriseModel entreprise;

  GetEntrepriseResponseModel({
    required this.statusCode,
    required this.statusMessage,
    required this.entreprise,
  });

  factory GetEntrepriseResponseModel.fromJson(Map<String, dynamic> json) =>
      GetEntrepriseResponseModel(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        entreprise: EntrepriseModel.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": entreprise.toJson(),
      };
}

class EntrepriseModel {
  int id;
  String name;
  String? description;
  String? email;
  String? address;

  List<String> contacts;
  String webLink;

  String? logo;

  EntrepriseModel({
    required this.id,
    required this.name,
    //required this.slug,
    required this.contacts,
    required this.webLink,
    this.email,
    this.description,
    this.address,
    this.logo,
  });

  factory EntrepriseModel.fromJson(Map<String, dynamic> json) =>
      EntrepriseModel(
        id: json["id"],
        name: json["name"],
        email: json["email"],
        address: json["address"],
        description: json["description"],
        contacts: List<String>.from(json["phone"].map((x) => x)),
        webLink: json["webLink"],
        logo: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        // "slug": slug,
        "contacts": List<dynamic>.from(contacts.map((x) => x)),
        "webLink": webLink,
        "illustrationImage": logo,
      };
}
