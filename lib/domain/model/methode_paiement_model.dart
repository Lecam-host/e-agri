// To parse this JSON data, do
//
//     final methodPaiementModel = methodPaiementModelFromJson(jsonString);

import 'dart:convert';

MethodPaiementModel methodPaiementModelFromJson(String str) =>
    MethodPaiementModel.fromJson(json.decode(str));

String methodPaiementModelToJson(MethodPaiementModel data) =>
    json.encode(data.toJson());

class MethodPaiementModel {
  MethodPaiementModel({
    this.statusCode,
    this.statusMessage,
    this.data,
  });

  int? statusCode;
  String? statusMessage;
  Data? data;

  factory MethodPaiementModel.fromJson(Map<String, dynamic> json) =>
      MethodPaiementModel(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data?.toJson(),
      };
}

class Data {
  Data({
    this.total,
    this.totalOfPages,
    this.perPage,
    this.sorts,
    this.order,
    this.currentPage,
    this.nextPage,
    this.previousPage,
    this.filters,
    this.items,
  });

  int? total;
  int? totalOfPages;
  dynamic perPage;
  dynamic sorts;
  dynamic order;
  int? currentPage;
  int? nextPage;
  int? previousPage;
  dynamic filters;
  List<PaymentMethodModel>? items;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        total: json["total"],
        totalOfPages: json["total_of_pages"],
        perPage: json["per_page"],
        sorts: json["sorts"],
        order: json["order"],
        currentPage: json["current_page"],
        nextPage: json["next_page"],
        previousPage: json["previous_page"],
        filters: json["filters"],
        items: json["items"] == null
            ? []
            : List<PaymentMethodModel>.from(
                json["items"]!.map((x) => PaymentMethodModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "total_of_pages": totalOfPages,
        "per_page": perPage,
        "sorts": sorts,
        "order": order,
        "current_page": currentPage,
        "next_page": nextPage,
        "previous_page": previousPage,
        "filters": filters,
        "items": items == null
            ? []
            : List<dynamic>.from(items!.map((x) => x.toJson())),
      };
}

class PaymentMethodModel {
  PaymentMethodModel({
    this.id,
    this.name,
    this.priority,
    this.code,
  });

  int? id;
  String? name;
  String? code;

  int? priority;
  factory PaymentMethodModel.fromJson(Map<String, dynamic> json) =>
      PaymentMethodModel(
        id: json["id"],
        name: json["name"],
        code: json["code"],
        priority: json["priority"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "priority": priority,
      };
}
