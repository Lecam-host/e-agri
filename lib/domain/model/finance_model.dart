class FinanceProductModel {
  int id;
  String title;
  String description;
  String webLink;
  String entreprise;

  String img;

  FinanceProductModel({
    required this.id,
    required this.title,
    required this.description,
    required this.img,
    required this.entreprise,
    required this.webLink,
  });
}

List<FinanceProductModel> listProductFinance = [
  FinanceProductModel(
    id: 1,
    description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
    title: "Az Microassurance",
    img:
        'https://images.pexels.com/photos/4386467/pexels-photo-4386467.jpeg?auto=compress&cs=tinysrgb&w=800',
    entreprise: "Allianz",
    webLink: "https://www.allianzcare.com/en.html",
  ),
  FinanceProductModel(
    id: 2,
    description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
    title: "Assurance auto",
    img:
        'https://images.pexels.com/photos/7164498/pexels-photo-7164498.jpeg?auto=compress&cs=tinysrgb&w=800',
    entreprise: "Allianz",
    webLink:
        "https://www.pexels.com/fr-fr/photo/homme-costume-rue-voiture-7164498/",
  )
];
