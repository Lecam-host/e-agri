import 'dart:convert';

GetListCategoryProductResponse getListCategoryProductFromJson(String str) =>
    GetListCategoryProductResponse.fromJson(json.decode(str));

String getListCategoryProductToJson(GetListCategoryProductResponse data) =>
    json.encode(data.toJson());

class GetListCategoryProductResponse {
  GetListCategoryProductResponse({
    required this.message,
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  String message;
  int statusCode;
  String statusMessage;
  List<CategoryModel> data;

  factory GetListCategoryProductResponse.fromJson(Map<String, dynamic> json) =>
      GetListCategoryProductResponse(
        message: json["message"],
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: List<CategoryModel>.from(
            json["data"].map((x) => CategoryModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class CategoryModel {
  CategoryModel({
    required this.categoryId,
    required this.name,
  });

  int categoryId;
  String name;

  factory CategoryModel.fromJson(Map<String, dynamic> json) => CategoryModel(
        categoryId: json["categoryId"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "categoryId": categoryId,
        "name": name,
      };
}

class IntrantCategoryModel {
  IntrantCategoryModel({
    required this.categoryId,
    required this.libelle,
  });

  int categoryId;
  String libelle;

  factory IntrantCategoryModel.fromJson(Map<String, dynamic> json) =>
      IntrantCategoryModel(
        categoryId: json["id"],
        libelle: json["libelle"],
      );

  Map<String, dynamic> toJson() => {
        "categoryId": categoryId,
        "libelle": libelle,
      };
}

class GetIntrantCategoryResponse {
  GetIntrantCategoryResponse({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  int statusCode;
  String statusMessage;
  List<IntrantCategoryModel> data;

  factory GetIntrantCategoryResponse.fromJson(Map<String, dynamic> json) =>
      GetIntrantCategoryResponse(
       
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: List<IntrantCategoryModel>.from(
            json["data"].map((x) => IntrantCategoryModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}
