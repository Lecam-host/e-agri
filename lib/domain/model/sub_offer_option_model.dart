// To parse this JSON data, do
//
//     final subOfferOptionModel = subOfferOptionModelFromJson(jsonString);

import 'dart:convert';

SubOfferOptionModel subOfferOptionModelFromJson(String str) => SubOfferOptionModel.fromJson(json.decode(str));

String subOfferOptionModelToJson(SubOfferOptionModel data) => json.encode(data.toJson());

class SubOfferOptionModel {
    int? id;
    String? name;
    String? description;
    String? webLink;
    int? price;
    List<String>? contacts;
    String? illustrationImage;
    List<dynamic>? files;
    bool? isActive;
    bool? isDeleted;
    DateTime? createdAt;
    DateTime? updatedAt;
    dynamic deletedAt;

    SubOfferOptionModel({
        this.id,
        this.name,
        this.description,
        this.webLink,
        this.price,
        this.contacts,
        this.illustrationImage,
        this.files,
        this.isActive,
        this.isDeleted,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
    });

    factory SubOfferOptionModel.fromJson(Map<String, dynamic> json) => SubOfferOptionModel(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        webLink: json["webLink"],
        price: json["price"],
        contacts: json["contacts"] == null ? [] : List<String>.from(json["contacts"]!.map((x) => x)),
        illustrationImage: json["illustrationImage"],
        files: json["files"] == null ? [] : List<dynamic>.from(json["files"]!.map((x) => x)),
        isActive: json["isActive"],
        isDeleted: json["isDeleted"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        deletedAt: json["deletedAt"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "webLink": webLink,
        "price": price,
        "contacts": contacts == null ? [] : List<dynamic>.from(contacts!.map((x) => x)),
        "illustrationImage": illustrationImage,
        "files": files == null ? [] : List<dynamic>.from(files!.map((x) => x)),
        "isActive": isActive,
        "isDeleted": isDeleted,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "deletedAt": deletedAt,
    };
}
