// To parse this JSON data, do
//
//     final intrantModel = intrantModelFromJson(jsonString);

import 'dart:convert';

IntrantModel intrantModelFromJson(String str) => IntrantModel.fromJson(json.decode(str));

String intrantModelToJson(IntrantModel data) => json.encode(data.toJson());

class IntrantModel {
    IntrantModel({
        this.statusCode,
        this.statusMessage,
        this.data,
    });

    int? statusCode;
    String? statusMessage;
    DataIntrant? data;

    factory IntrantModel.fromJson(Map<String, dynamic> json) => IntrantModel(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: json["data"] == null ? null : DataIntrant.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data?.toJson(),
    };
}

class DataIntrant {
    DataIntrant({
        this.total,
        this.totalOfPages,
        this.perPage,
        this.sorts,
        this.order,
        this.currentPage,
        this.nextPage,
        this.previousPage,
        this.filters,
        this.items,
    });

    int? total;
    int? totalOfPages;
    dynamic perPage;
    dynamic sorts;
    dynamic order;
    int? currentPage;
    int? nextPage;
    int? previousPage;
    dynamic filters;
    List<ItemIntrant>? items;

    factory DataIntrant.fromJson(Map<String, dynamic> json) => DataIntrant(
        total: json["total"],
        totalOfPages: json["total_of_pages"],
        perPage: json["per_page"],
        sorts: json["sorts"],
        order: json["order"],
        currentPage: json["current_page"],
        nextPage: json["next_page"],
        previousPage: json["previous_page"],
        filters: json["filters"],
        items: json["items"] == null ? [] : List<ItemIntrant>.from(json["items"]!.map((x) => ItemIntrant.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "total": total,
        "total_of_pages": totalOfPages,
        "per_page": perPage,
        "sorts": sorts,
        "order": order,
        "current_page": currentPage,
        "next_page": nextPage,
        "previous_page": previousPage,
        "filters": filters,
        "items": items == null ? [] : List<dynamic>.from(items!.map((x) => x.toJson())),
    };
}

class ItemIntrant {
    ItemIntrant({
        this.id,
         this.categorieId,
        this.libelle,
        this.typeCultures,
        this.specifications,
        this.nHomologation,
        this.distributorAgree,
        this.description,
        this.createdAt,
        this.deleted,
        this.updatedAt,
        this.deletedAt,
    });

    int? id;
    int? categorieId;
    String? libelle;
    List<TypeCulture>? typeCultures;
    String? specifications;
    String? nHomologation;
    String? distributorAgree;
    String? description;
    DateTime? createdAt;
    bool? deleted;
    dynamic updatedAt;
    dynamic deletedAt;

    factory ItemIntrant.fromJson(Map<String, dynamic> json) => ItemIntrant(
        id: json["id"],
        libelle: json["libelle"],
        typeCultures: json["typeCultures"] == null ? [] : List<TypeCulture>.from(json["typeCultures"]!.map((x) => TypeCulture.fromJson(x))),
        specifications: json["specifications"],
        nHomologation: json["n_homologation"],
        distributorAgree: json["distributorAgree"],
        description: json["description"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        deleted: json["deleted"],
        updatedAt: json["updatedAt"],
      categorieId: json["categoryId"]!=null? json["categoryId"]["id"]:null,
        deletedAt: json["deletedAt"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "libelle": libelle,
        "typeCultures": typeCultures == null ? [] : List<dynamic>.from(typeCultures!.map((x) => x.toJson())),
        "specifications": specifications,
        "n_homologation": nHomologation,
        "distributorAgree": distributorAgree,
        "description": description,
        "createdAt": createdAt?.toIso8601String(),
        "deleted": deleted,
        "updatedAt": updatedAt,
        "deletedAt": deletedAt,
    };
}

class TypeCulture {
    TypeCulture({
        this.id,
        this.libelle,
        this.createdAt,
        this.deleted,
        this.updatedAt,
        this.deletedAt,
    });

    int? id;
    String? libelle;
    DateTime? createdAt;
    bool? deleted;
    DateTime? updatedAt;
    dynamic deletedAt;

    factory TypeCulture.fromJson(Map<String, dynamic> json) => TypeCulture(
        id: json["id"],
        libelle: json["libelle"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        deleted: json["deleted"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        deletedAt: json["deletedAt"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "libelle": libelle,
        "createdAt": createdAt?.toIso8601String(),
        "deleted": deleted,
        "updatedAt": updatedAt?.toIso8601String(),
        "deletedAt": deletedAt,
    };
}
