// To parse this JSON data, do
//
//     final insurerModel = insurerModelFromJson(jsonString);

import 'dart:convert';

InsurerModel insurerModelFromJson(String str) => InsurerModel.fromJson(json.decode(str));

String insurerModelToJson(InsurerModel data) => json.encode(data.toJson());

class InsurerModel {
    int? statusCode;
    String? statusMessage;
    DataInsurerModel? data;

    InsurerModel({
        this.statusCode,
        this.statusMessage,
        this.data,
    });

    factory InsurerModel.fromJson(Map<String, dynamic> json) => InsurerModel(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: json["data"] == null ? null : DataInsurerModel.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data?.toJson(),
    };
}

class DataInsurerModel {
    int? total;
    int? totalOfPages;
    int? perPage;
    dynamic sorts;
    dynamic order;
    int? currentPage;
    int? nextPage;
    int? previousPage;
    dynamic filters;
    List<ItemInsurerModel>? items;

    DataInsurerModel({
        this.total,
        this.totalOfPages,
        this.perPage,
        this.sorts,
        this.order,
        this.currentPage,
        this.nextPage,
        this.previousPage,
        this.filters,
        this.items,
    });

    factory DataInsurerModel.fromJson(Map<String, dynamic> json) => DataInsurerModel(
        total: json["total"],
        totalOfPages: json["total_of_pages"],
        perPage: json["per_page"],
        sorts: json["sorts"],
        order: json["order"],
        currentPage: json["current_page"],
        nextPage: json["next_page"],
        previousPage: json["previous_page"],
        filters: json["filters"],
        items: json["items"] == null ? [] : List<ItemInsurerModel>.from(json["items"]!.map((x) => ItemInsurerModel.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "total": total,
        "total_of_pages": totalOfPages,
        "per_page": perPage,
        "sorts": sorts,
        "order": order,
        "current_page": currentPage,
        "next_page": nextPage,
        "previous_page": previousPage,
        "filters": filters,
        "items": items == null ? [] : List<dynamic>.from(items!.map((x) => x.toJson())),
    };
}

class ItemInsurerModel {
    int? id;
    String? name;
    String? entreprise;
    String? businessLine;
    String? email;
    String? address;
    List<String>? phone;
    String? webLink;
    String? image;
    String? description;
    List<TypeInsurance>? typeInsurance;
    bool? isActive;
    bool? isDeleted;
    DateTime? createdAt;
    DateTime? updatedAt;
    dynamic deletedAt;

    ItemInsurerModel({
        this.id,
        this.name,
        this.entreprise,
        this.businessLine,
        this.email,
        this.address,
        this.phone,
        this.webLink,
        this.image,
        this.description,
        this.typeInsurance,
        this.isActive,
        this.isDeleted,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
    });

    factory ItemInsurerModel.fromJson(Map<String, dynamic> json) => ItemInsurerModel(
        id: json["id"],
        name: json["name"],
        entreprise: json["entreprise"],
        businessLine: json["businessLine"],
        email: json["email"],
        address: json["address"],
        phone: json["phone"] == null ? [] : List<String>.from(json["phone"]!.map((x) => x)),
        webLink: json["webLink"],
        image: json["image"],
        description: json["description"],
        typeInsurance: json["typeInsurance"] == null ? [] : List<TypeInsurance>.from(json["typeInsurance"]!.map((x) => TypeInsurance.fromJson(x))),
        isActive: json["isActive"],
        isDeleted: json["isDeleted"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        deletedAt: json["deletedAt"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "entreprise": entreprise,
        "businessLine": businessLine,
        "email": email,
        "address": address,
        "phone": phone == null ? [] : List<dynamic>.from(phone!.map((x) => x)),
        "webLink": webLink,
        "image": image,
        "description": description,
        "typeInsurance": typeInsurance == null ? [] : List<dynamic>.from(typeInsurance!.map((x) => x.toJson())),
        "isActive": isActive,
        "isDeleted": isDeleted,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "deletedAt": deletedAt,
    };
}

class TypeInsurance {
    int? id;
    String? name;
    String? description;
    String? illustrationImage;
    List<Insurance>? insurances;
    bool? isActive;
    bool? isDeleted;
    DateTime? createdAt;
    DateTime? updatedAt;
    dynamic deletedAt;

    TypeInsurance({
        this.id,
        this.name,
        this.description,
        this.illustrationImage,
        this.insurances,
        this.isActive,
        this.isDeleted,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
    });

    factory TypeInsurance.fromJson(Map<String, dynamic> json) => TypeInsurance(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        illustrationImage: json["illustrationImage"],
        insurances: json["insurances"] == null ? [] : List<Insurance>.from(json["insurances"]!.map((x) => Insurance.fromJson(x))),
        isActive: json["isActive"],
        isDeleted: json["isDeleted"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        deletedAt: json["deletedAt"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "illustrationImage": illustrationImage,
        "insurances": insurances == null ? [] : List<dynamic>.from(insurances!.map((x) => x.toJson())),
        "isActive": isActive,
        "isDeleted": isDeleted,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "deletedAt": deletedAt,
    };
}

class Insurance {
    int? id;
    String? name;
    String? description;
    String? illustrationImage;
    String? webLink;
    List<String>? contacts;
    dynamic insurer;
    dynamic typeInsurance;
    List<Insurance>? subInsurances;
    bool? isActive;
    bool? isDeleted;
    DateTime? createdAt;
    DateTime? updatedAt;
    dynamic deletedAt;
    double? price;

    Insurance({
        this.id,
        this.name,
        this.description,
        this.illustrationImage,
        this.webLink,
        this.contacts,
        this.insurer,
        this.typeInsurance,
        this.subInsurances,
        this.isActive,
        this.isDeleted,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
        this.price,
    });

    factory Insurance.fromJson(Map<String, dynamic> json) => Insurance(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        illustrationImage: json["illustrationImage"],
        webLink: json["webLink"],
        contacts: json["contacts"] == null ? [] : List<String>.from(json["contacts"]!.map((x) => x)),
        insurer: json["insurer"],
        typeInsurance: json["typeInsurance"],
        subInsurances: json["subInsurances"] == null ? [] : List<Insurance>.from(json["subInsurances"]!.map((x) => Insurance.fromJson(x))),
        isActive: json["isActive"],
        isDeleted: json["isDeleted"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        deletedAt: json["deletedAt"],
        price: json["price"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "illustrationImage": illustrationImage,
        "webLink": webLink,
        "contacts": contacts == null ? [] : List<dynamic>.from(contacts!.map((x) => x)),
        "insurer": insurer,
        "typeInsurance": typeInsurance,
        "subInsurances": subInsurances == null ? [] : List<dynamic>.from(subInsurances!.map((x) => x.toJson())),
        "isActive": isActive,
        "isDeleted": isDeleted,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "deletedAt": deletedAt,
        "price": price,
    };
}
