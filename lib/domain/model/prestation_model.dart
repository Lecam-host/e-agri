import 'dart:convert';

import 'Product.dart';
import 'user_model.dart';

GetListCategoryPrestation getListCategoryPrestationFromJson(String str) =>
    GetListCategoryPrestation.fromJson(json.decode(str));

String getListCategoryPrestationToJson(GetListCategoryPrestation data) =>
    json.encode(data.toJson());

class GetListCategoryPrestation {
  GetListCategoryPrestation({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  int statusCode;
  String statusMessage;
  List<CategoryPrestation> data;

  factory GetListCategoryPrestation.fromJson(Map<String, dynamic> json) =>
      GetListCategoryPrestation(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: List<CategoryPrestation>.from(
            json["data"].map((x) => CategoryPrestation.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class CategoryPrestation {
  CategoryPrestation({
    required this.id,
    required this.name,
    required this.description,
    this.sousCategoryPurchases,
    this.idCategory,
  });

  int id;
  String name;
  String description;
  List<SousCategoryPrestation>? sousCategoryPurchases;
  int? idCategory;

  factory CategoryPrestation.fromJson(Map<String, dynamic> json) =>
      CategoryPrestation(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        sousCategoryPurchases: json["sousCategoryPurchases"] == null
            ? []
            : List<SousCategoryPrestation>.from(json["sousCategoryPurchases"]!
                .map((x) => SousCategoryPrestation.fromJson(x))),
        idCategory: json["idCategory"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "sousCategoryPurchases": sousCategoryPurchases == null
            ? []
            : List<dynamic>.from(sousCategoryPurchases!.map((x) => x.toJson())),
        "idCategory": idCategory,
      };
}

class SousCategoryPrestation {
  SousCategoryPrestation({
    required this.id,
    required this.name,
    required this.description,
    this.idCategory,
  });

  int id;
  String name;
  String description;

  int? idCategory;

  factory SousCategoryPrestation.fromJson(Map<String, dynamic> json) =>
      SousCategoryPrestation(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        idCategory: json["idCategory"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "idCategory": idCategory,
      };
}

GetListPrestationResponse getListPrestationResponseFromJson(String str) =>
    GetListPrestationResponse.fromJson(json.decode(str));

String getListPrestationResponseToJson(GetListPrestationResponse data) =>
    json.encode(data.toJson());

class GetListPrestationResponse {
  GetListPrestationResponse({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  int statusCode;
  String statusMessage;
  List<PrestationData> data;

  factory GetListPrestationResponse.fromJson(Map<String, dynamic> json) =>
      GetListPrestationResponse(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: List<PrestationData>.from(
            json["data"].map((x) => PrestationData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class PrestationData {
  PrestationData({
    required this.id,
    required this.offerId,
    required this.quantity,
    required this.userId,
    required this.total,
    required this.isValid,
    required this.dateStart,
    required this.dateEnd,
    this.contractDate,
    this.contract,
    this.isAccept,
  });

  int id;
  int offerId;
  int quantity;
  int userId;
  int total;
  String dateStart;
  String dateEnd;
  String? contract;
  String? contractDate;
  bool isValid;
  bool? isAccept;

  factory PrestationData.fromJson(Map<String, dynamic> json) => PrestationData(
        id: json["id"],
        offerId: json["offerId"],
        quantity: json["quantity"],
        userId: json["userId"],
        total: json["total"].toInt(),
        isValid: json["isValid"],
        dateStart: json["dateStart"],
        dateEnd: json["dateEnd"],
        contractDate: json["contractDate"],
        contract: json["contract"],
        isAccept: json["accept"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "offerId": offerId,
        "quantity": quantity,
        "userId": userId,
        "total": total,
        "isValid": isValid,
        "dateStart": dateStart,
        "dateEnd": dateEnd,
        "isAccept": isAccept,
      };
}

class DataDemande {
  ProductModel? prestation;
  UserModel user;
  PrestationData prestationDemandeData;
  bool isReceive;
  DataDemande({
    this.prestation,
    required this.user,
    required this.prestationDemandeData,
    required this.isReceive,
  });
}

GetUniteResponse getPrestationServiceUniteFromJson(String str) =>
    GetUniteResponse.fromJson(json.decode(str));

String getPrestationServiceUniteToJson(GetUniteResponse data) =>
    json.encode(data.toJson());

class GetUniteResponse {
  GetUniteResponse({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  int statusCode;
  String statusMessage;
  List<UniteModel> data;

  factory GetUniteResponse.fromJson(Map<String, dynamic> json) =>
      GetUniteResponse(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: List<UniteModel>.from(
            json["data"].map((x) => UniteModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class UniteModel {
  UniteModel({
    required this.id,
    required this.name,
    required this.shortName,
  });

  int id;
  String name;
  String shortName;

  factory UniteModel.fromJson(Map<String, dynamic> json) => UniteModel(
        id: json["id"],
        name: json["name"],
        shortName: json["shortName"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "shortName": shortName,
      };
}
