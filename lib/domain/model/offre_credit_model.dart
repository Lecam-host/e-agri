// To parse this JSON data, do
//
//     final offreCreditModel = offreCreditModelFromJson(jsonString);

import 'dart:convert';

import 'package:eagri/domain/model/offre_insurance_model.dart';

OffreCreditModel offreCreditModelFromJson(String str) =>
    OffreCreditModel.fromJson(json.decode(str));

String offreCreditModelToJson(OffreCreditModel data) =>
    json.encode(data.toJson());

class OffreCreditModel {
  int? statusCode;
  String? statusMessage;
  DataOffreCreditModel? data;

  OffreCreditModel({
    this.statusCode,
    this.statusMessage,
    this.data,
  });

  factory OffreCreditModel.fromJson(Map<String, dynamic> json) =>
      OffreCreditModel(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: json["data"] == null
            ? null
            : DataOffreCreditModel.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data?.toJson(),
      };
}

class DataOffreCreditModel {
  int? total;
  int? totalOfPages;
  int? perPage;
  dynamic sorts;
  dynamic order;
  int? currentPage;
  int? nextPage;
  int? previousPage;
  dynamic filters;
  List<ItemOffreCreditModel>? items;

  DataOffreCreditModel({
    this.total,
    this.totalOfPages,
    this.perPage,
    this.sorts,
    this.order,
    this.currentPage,
    this.nextPage,
    this.previousPage,
    this.filters,
    this.items,
  });

  factory DataOffreCreditModel.fromJson(Map<String, dynamic> json) =>
      DataOffreCreditModel(
        total: json["total"],
        totalOfPages: json["total_of_pages"],
        perPage: json["per_page"],
        sorts: json["sorts"],
        order: json["order"],
        currentPage: json["current_page"],
        nextPage: json["next_page"],
        previousPage: json["previous_page"],
        filters: json["filters"],
        items: json["items"] == null
            ? []
            : List<ItemOffreCreditModel>.from(
                json["items"]!.map((x) => ItemOffreCreditModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "total_of_pages": totalOfPages,
        "per_page": perPage,
        "sorts": sorts,
        "order": order,
        "current_page": currentPage,
        "next_page": nextPage,
        "previous_page": previousPage,
        "filters": filters,
        "items": items == null
            ? []
            : List<dynamic>.from(items!.map((x) => x.toJson())),
      };
}

class ItemOffreCreditModel {
  int? id;
  String? name;
  String? description;
  String? illustrationImage;
  List<FileElement>? files;
  String? webLink;
  List<String>? contacts;
  Creditor? creditor;
  Creditor? typeCredit;
  List<ItemOffreCreditModel>? subCredits;
  bool? isActive;
  bool? isDeleted;
  DateTime? createdAt;
  DateTime? updatedAt;
  dynamic deletedAt;
  double? price;

  ItemOffreCreditModel({
    this.id,
    this.name,
    this.description,
    this.illustrationImage,
    this.files,
    this.webLink,
    this.contacts,
    this.creditor,
    this.typeCredit,
    this.subCredits,
    this.isActive,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
    this.price,
  });

  factory ItemOffreCreditModel.fromJson(Map<String, dynamic> json) =>
      ItemOffreCreditModel(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        illustrationImage: json["illustrationImage"],
        files: json["files"] == null
            ? []
            : List<FileElement>.from(
                json["files"]!.map((x) => FileElement.fromJson(x))),
        webLink: json["webLink"],
        contacts: json["contacts"] == null
            ? []
            : List<String>.from(json["contacts"]!.map((x) => x)),
        creditor: json["creditor"] == null
            ? null
            : Creditor.fromJson(json["creditor"]),
        typeCredit: json["typeCredit"] == null
            ? null
            : Creditor.fromJson(json["typeCredit"]),
        subCredits: json["subCredits"] == null
            ? []
            : List<ItemOffreCreditModel>.from(json["subCredits"]!
                .map((x) => ItemOffreCreditModel.fromJson(x))),
        isActive: json["isActive"],
        isDeleted: json["isDeleted"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        deletedAt: json["deletedAt"],
        price: json["price"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "illustrationImage": illustrationImage,
        "files":
            files == null ? [] : List<FileElement>.from(files!.map((x) => x)),
        "webLink": webLink,
        "contacts":
            contacts == null ? [] : List<dynamic>.from(contacts!.map((x) => x)),
        "creditor": creditor?.toJson(),
        "typeCredit": typeCredit?.toJson(),
        "subCredits": subCredits == null
            ? []
            : List<dynamic>.from(subCredits!.map((x) => x.toJson())),
        "isActive": isActive,
        "isDeleted": isDeleted,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "deletedAt": deletedAt,
        "price": price,
      };
}

class Creditor {
  String? name;
  int? id;

  Creditor({
    this.name,
    this.id,
  });

  factory Creditor.fromJson(Map<String, dynamic> json) => Creditor(
        name: json["name"]!,
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "id": id,
      };
}
