// To parse this JSON data, do
//
//     final typeCultureModel = typeCultureModelFromJson(jsonString);

// ignore_for_file: file_names

import 'dart:convert';

TypeCultureModel typeCultureModelFromJson(String str) =>
    TypeCultureModel.fromJson(json.decode(str));

String typeCultureModelToJson(TypeCultureModel data) =>
    json.encode(data.toJson());

class TypeCultureModel {
  TypeCultureModel({
    this.statusCode,
    this.statusMessage,
    this.data,
  });

  int? statusCode;
  String? statusMessage;
  Data? data;

  factory TypeCultureModel.fromJson(Map<String, dynamic> json) =>
      TypeCultureModel(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data?.toJson(),
      };
}

class Data {
  Data({
    this.total,
    this.totalOfPages,
    this.perPage,
    this.sorts,
    this.order,
    this.currentPage,
    this.nextPage,
    this.previousPage,
    this.filters,
    this.items,
  });

  int? total;
  int? totalOfPages;
  dynamic perPage;
  dynamic sorts;
  dynamic order;
  int? currentPage;
  int? nextPage;
  int? previousPage;
  dynamic filters;
  List<ItemTypeCulture>? items;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        total: json["total"],
        totalOfPages: json["total_of_pages"],
        perPage: json["per_page"],
        sorts: json["sorts"],
        order: json["order"],
        currentPage: json["current_page"],
        nextPage: json["next_page"],
        previousPage: json["previous_page"],
        filters: json["filters"],
        items: json["items"] == null
            ? []
            : List<ItemTypeCulture>.from(
                json["items"]!.map((x) => ItemTypeCulture.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "total_of_pages": totalOfPages,
        "per_page": perPage,
        "sorts": sorts,
        "order": order,
        "current_page": currentPage,
        "next_page": nextPage,
        "previous_page": previousPage,
        "filters": filters,
        "items": items == null
            ? []
            : List<dynamic>.from(items!.map((x) => x.toJson())),
      };
}

class ItemTypeCulture {
  ItemTypeCulture({
    this.id,
    this.libelle,
    this.createdAt,
    this.deleted,
    this.updatedAt,
    this.deletedAt,
  });

  int? id;
  String? libelle;
  DateTime? createdAt;
  bool? deleted;
  DateTime? updatedAt;
  dynamic deletedAt;

  factory ItemTypeCulture.fromJson(Map<String, dynamic> json) =>
      ItemTypeCulture(
        id: json["id"],
        libelle: json["libelle"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        deleted: json["deleted"],
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        deletedAt: json["deletedAt"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "libelle": libelle,
        "createdAt": createdAt?.toIso8601String(),
        "deleted": deleted,
        "updatedAt": updatedAt?.toIso8601String(),
        "deletedAt": deletedAt,
      };
}
