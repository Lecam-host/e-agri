// To parse this JSON data, do
//
//     final getProductOderedInCreditResponse = getProductOderedInCreditResponseFromJson(jsonString);

import 'dart:convert';

import 'info_by_id_model.dart';

GetProductOderedInCreditResponse getProductOderedInCreditResponseFromJson(String str) => GetProductOderedInCreditResponse.fromJson(json.decode(str));

String getProductOderedInCreditResponseToJson(GetProductOderedInCreditResponse data) => json.encode(data.toJson());

class GetProductOderedInCreditResponse {
    int statusCode;
    String statusMessage;
    Data data;

    GetProductOderedInCreditResponse({
        required this.statusCode,
        required this.statusMessage,
        required this.data,
    });

    factory GetProductOderedInCreditResponse.fromJson(Map<String, dynamic> json) => GetProductOderedInCreditResponse(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data.toJson(),
    };
}

class Data {
    int total;
    int totalOfPages;
    int perPage;
    dynamic sorts;
    dynamic order;
    int currentPage;
    int nextPage;
    int previousPage;
    dynamic filters;
    List<OderedProductCredit> items;

    Data({
        required this.total,
        required this.totalOfPages,
        required this.perPage,
        this.sorts,
        this.order,
        required this.currentPage,
        required this.nextPage,
        required this.previousPage,
        this.filters,
        required this.items,
    });

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        total: json["total"],
        totalOfPages: json["total_of_pages"],
        perPage: json["per_page"],
        sorts: json["sorts"],
        order: json["order"],
        currentPage: json["current_page"],
        nextPage: json["next_page"],
        previousPage: json["previous_page"],
        filters: json["filters"],
        items: List<OderedProductCredit>.from(json["items"].map((x) => OderedProductCredit.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "total": total,
        "total_of_pages": totalOfPages,
        "per_page": perPage,
        "sorts": sorts,
        "order": order,
        "current_page": currentPage,
        "next_page": nextPage,
        "previous_page": previousPage,
        "filters": filters,
        "items": List<dynamic>.from(items.map((x) => x.toJson())),
    };
}

class OderedProductCredit {
    int id;
    int userId;
    int vendorId;
    int productId;
    int speculationId;
    dynamic orderId;
    String productCode;
    int quantity;
    int price;
    int totalAmount;
    int paidAmount;
    int numberOfSlice;
    String paymentFrequency;
    String transactionId;
    String status;
    List<PaymentDate> paymentDates;
    bool isInArrears;
    DateTime? createdAt;
    ClientModel ?acheteur;
   ClientModel ?vendeur;

    OderedProductCredit({
        required this.id,
        required this.userId,
        required this.vendorId,
        required this.productId,
        required this.speculationId,
        this.orderId,
        required this.productCode,
        required this.quantity,
        required this.price,
        required this.totalAmount,
        required this.paidAmount,
        required this.numberOfSlice,
        required this.paymentFrequency,
        required this.transactionId,
        required this.status,
        required this.paymentDates,
        required this.isInArrears,
         this.createdAt,
         this.acheteur,
         this.vendeur,
       
    });

    factory OderedProductCredit.fromJson(Map<String, dynamic> json) => OderedProductCredit(
        id: json["id"],
        userId: json["userId"],
        vendorId: json["vendorId"],
        productId: json["productId"],
        speculationId: json["speculationId"],
        orderId: json["orderId"],
        productCode: json["productCode"],
        quantity: json["quantity"],
        price: json["price"],
        totalAmount: json["totalAmount"],
        paidAmount: json["paidAmount"],
        numberOfSlice: json["numberOfSlice"],
        paymentFrequency: json["paymentFrequency"],
        transactionId: json["transactionId"],
        status: json["status"],
        paymentDates: List<PaymentDate>.from(json["paymentDates"].map((x) => PaymentDate.fromJson(x))),
        isInArrears: json["isInArrears"],
        createdAt: DateTime.parse(json["createdAt"]),
       
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "userId": userId,
        "vendorId": vendorId,
        "productId": productId,
        "speculationId": speculationId,
        "orderId": orderId,
        "productCode": productCode,
        "quantity": quantity,
        "price": price,
        "totalAmount": totalAmount,
        "paidAmount": paidAmount,
        "numberOfSlice": numberOfSlice,
        "paymentFrequency": paymentFrequency,
        "transactionId": transactionId,
        "status": status,
        "paymentDates": List<dynamic>.from(paymentDates.map((x) => x.toJson())),
        "isInArrears": isInArrears,
        
    };
}

class PaymentDate {
    int id;
    dynamic notification;
    DateTime date;
    int amount;
    bool isOk;
    DateTime createdAt;
    bool deleted;
   

    PaymentDate({
        required this.id,
        this.notification,
        required this.date,
        required this.amount,
        required this.isOk,
        required this.createdAt,
        required this.deleted,
       
    });

    factory PaymentDate.fromJson(Map<String, dynamic> json) => PaymentDate(
        id: json["id"],
        notification: json["notification"],
        date: DateTime.parse(json["date"]),
        amount: json["amount"],
        isOk: json["isOk"],
        createdAt: DateTime.parse(json["createdAt"]),
        deleted: json["deleted"],
       
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "notification": notification,
        "date": "${date.year.toString().padLeft(4, '0')}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}",
        "amount": amount,
        "isOk": isOk,
        "createdAt": createdAt.toIso8601String(),
        "deleted": deleted,
       
    };
}
