// To parse this JSON data, do
//
//     final typeOffreCreditModel = typeOffreCreditModelFromJson(jsonString);

import 'dart:convert';

TypeOffreCreditModel typeOffreCreditModelFromJson(String str) => TypeOffreCreditModel.fromJson(json.decode(str));

String typeOffreCreditModelToJson(TypeOffreCreditModel data) => json.encode(data.toJson());

class TypeOffreCreditModel {
    int? statusCode;
    String? statusMessage;
    List<DataTypeOffreCreditModel>? data;

    TypeOffreCreditModel({
        this.statusCode,
        this.statusMessage,
        this.data,
    });

    factory TypeOffreCreditModel.fromJson(Map<String, dynamic> json) => TypeOffreCreditModel(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: json["data"] == null ? [] : List<DataTypeOffreCreditModel>.from(json["data"]!.map((x) => DataTypeOffreCreditModel.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data == null ? [] : List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class DataTypeOffreCreditModel {
    int? id;
    String? name;
    String? description;
    String? illustrationImage;
    dynamic credits;
    bool? isActive;
    bool? isDeleted;
    DateTime? createdAt;
    DateTime? updatedAt;
    dynamic deletedAt;

    DataTypeOffreCreditModel({
        this.id,
        this.name,
        this.description,
        this.illustrationImage,
        this.credits,
        this.isActive,
        this.isDeleted,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
    });

    factory DataTypeOffreCreditModel.fromJson(Map<String, dynamic> json) => DataTypeOffreCreditModel(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        illustrationImage: json["illustrationImage"],
        credits: json["credits"],
        isActive: json["isActive"],
        isDeleted: json["isDeleted"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        deletedAt: json["deletedAt"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "illustrationImage": illustrationImage,
        "credits": credits,
        "isActive": isActive,
        "isDeleted": isDeleted,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "deletedAt": deletedAt,
    };
}
