// To parse this JSON data, do
//
//     final getListProductPaidResponse = getListProductPaidResponseFromJson(jsonString);

import 'dart:convert';

import 'image_model.dart';

GetListProductPaidResponse getListProductPaidResponseFromJson(String str) =>
    GetListProductPaidResponse.fromJson(json.decode(str));

String getListProductPaidResponseToJson(GetListProductPaidResponse data) =>
    json.encode(data.toJson());

class GetListProductPaidResponse {
  int statusCode;
  String statusMessage;
  GetListProductPaidResponseData data;

  GetListProductPaidResponse({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  factory GetListProductPaidResponse.fromJson(Map<String, dynamic> json) =>
      GetListProductPaidResponse(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: GetListProductPaidResponseData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data.toJson(),
      };
}

class GetListProductPaidResponseData {
  int total;
  int totalPage;
  int perPage;
  int page;
  int? nextPage;
  int lastPage;
  List<ProductPaidModel> items;

  GetListProductPaidResponseData({
    required this.total,
    required this.totalPage,
    required this.perPage,
    required this.page,
    this.nextPage,
    required this.lastPage,
    required this.items,
  });

  factory GetListProductPaidResponseData.fromJson(Map<String, dynamic> json) =>
      GetListProductPaidResponseData(
        total: json["total"],
        totalPage: json["totalPage"],
        perPage: json["perPage"],
        page: json["page"],
        nextPage: json["nextPage"],
        lastPage: json["lastPage"],
        items: List<ProductPaidModel>.from(
            json["items"].map((x) => ProductPaidModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "totalPage": totalPage,
        "perPage": perPage,
        "page": page,
        "nextPage": nextPage,
        "lastPage": lastPage,
        "items": List<dynamic>.from(items.map((x) => x.toJson())),
      };
}

class ProductPaidModel {
  int productId;
  int paidUser;
  ItemProduct product;
  dynamic notation;
  String createdAt;

  Speculation speculation;

  ProductPaidModel({
    required this.productId,
    required this.paidUser,
    required this.product,
    required this.createdAt,
    this.notation,
    required this.speculation,
  });

  factory ProductPaidModel.fromJson(Map<String, dynamic> json) =>
      ProductPaidModel(
        productId: json["product_id"],
        paidUser: json["paid_user"],
        product: ItemProduct.fromJson(json["product"]),
        notation: json["notation"],
        createdAt: json["created_at"].toString(),
        speculation: Speculation.fromJson(json["speculation"]),
      );

  Map<String, dynamic> toJson() => {
        "product_id": productId,
        "paid_user": paidUser,
        "product": product.toJson(),
        "notation": notation,
        "speculation": speculation.toJson(),
      };
}

class ItemProduct {
  int statusCode;
  String statusMessage;
  ProductData data;

  ItemProduct({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  factory ItemProduct.fromJson(Map<String, dynamic> json) => ItemProduct(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: ProductData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data.toJson(),
      };
}

class ProductData {
  int id;
  DateTime publicationDate;
  String description;
  String code;
  String availability;
  dynamic availabilityDate;
  DateTime expirationDate;
  dynamic certification;
  String typeVente;
  Location location;
  String status;
  int paymentId;
  DataProduct product;
  List<ImageProduct>? images;
  String offerType;
  double notation;
  PaymentMethod paymentMethod;
  dynamic unitTime;
  dynamic numberSlice;
  dynamic origin;
  DateTime createdAt;
  DateTime updatedAt;
  bool deleted;
  dynamic deletedAt;
  int fournisseurId;
  dynamic atAbout;

  ProductData({
    required this.id,
    required this.publicationDate,
    required this.description,
    required this.code,
    required this.availability,
    this.availabilityDate,
    required this.expirationDate,
    this.certification,
    required this.typeVente,
    required this.location,
    required this.status,
    required this.paymentId,
    required this.product,
    this.images,
    required this.offerType,
    required this.notation,
    required this.paymentMethod,
    this.unitTime,
    this.numberSlice,
    this.origin,
    required this.createdAt,
    required this.updatedAt,
    required this.deleted,
    this.deletedAt,
    required this.fournisseurId,
    this.atAbout,
  });

  factory ProductData.fromJson(Map<String, dynamic> json) => ProductData(
        id: json["id"],
        publicationDate: DateTime.parse(json["publicationDate"]),
        description: json["description"],
        code: json["code"],
        availability: json["availability"],
        availabilityDate: json["availabilityDate"],
        expirationDate: DateTime.parse(json["expirationDate"]),
        certification: json["certification"],
        typeVente: json["typeVente"],
        location: Location.fromJson(json["location"]),
        status: json["status"],
        paymentId: json["paymentId"],
        product: DataProduct.fromJson(json["product"]),
        images: json["images"] != null
            ? List<ImageProduct>.from(
                json["images"].map((x) => ImageProduct.fromJson(x)))
            : null,
        offerType: json["offerType"],
        notation: json["notation"],
        paymentMethod: PaymentMethod.fromJson(json["paymentMethod"]),
        unitTime: json["unitTime"],
        numberSlice: json["numberSlice"],
        origin: json["origin"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        deleted: json["deleted"],
        deletedAt: json["deleted_at"],
        fournisseurId: json["fournisseurId"],
        atAbout: json["atAbout"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "publicationDate": publicationDate.toIso8601String(),
        "description": description,
        "code": code,
        "availability": availability,
        "availabilityDate": availabilityDate,
        "expirationDate":
            "${expirationDate.year.toString().padLeft(4, '0')}-${expirationDate.month.toString().padLeft(2, '0')}-${expirationDate.day.toString().padLeft(2, '0')}",
        "certification": certification,
        "typeVente": typeVente,
        "location": location.toJson(),
        "status": status,
        "paymentId": paymentId,
        "product": product.toJson(),
        "images":
            images != null ? List<dynamic>.from(images!.map((x) => x)) : null,
        "offerType": offerType,
        "notation": notation,
        "paymentMethod": paymentMethod.toJson(),
        "unitTime": unitTime,
        "numberSlice": numberSlice,
        "origin": origin,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "deleted": deleted,
        "deleted_at": deletedAt,
        "fournisseurId": fournisseurId,
        "atAbout": atAbout,
      };
}

class Location {
  int id;
  int regionId;
  dynamic departementId;
  dynamic sprefectureId;
  dynamic localiteId;

  Location({
    required this.id,
    required this.regionId,
    this.departementId,
    this.sprefectureId,
    this.localiteId,
  });

  factory Location.fromJson(Map<String, dynamic> json) => Location(
        id: json["id"],
        regionId: json["regionId"],
        departementId: json["departementId"],
        sprefectureId: json["sprefectureId"],
        localiteId: json["localiteId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "regionId": regionId,
        "departementId": departementId,
        "sprefectureId": sprefectureId,
        "localiteId": localiteId,
      };
}

class PaymentMethod {
  int id;
  String code;
  int priority;
  String name;

  PaymentMethod({
    required this.id,
    required this.code,
    required this.priority,
    required this.name,
  });

  factory PaymentMethod.fromJson(Map<String, dynamic> json) => PaymentMethod(
        id: json["id"],
        code: json["code"],
        priority: json["priority"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "code": code,
        "priority": priority,
        "name": name,
      };
}

class DataProduct {
  int id;
  String identifiant;
  int categorieId;
  int speculationId;
  int quantity;
  int initialQuantity;
  double priceU;
  DateTime createdAt;
  String unitOfMeasurment;

  DataProduct({
    required this.id,
    required this.identifiant,
    required this.categorieId,
    required this.speculationId,
    required this.quantity,
    required this.initialQuantity,
    required this.priceU,
    required this.createdAt,
    required this.unitOfMeasurment,
  });

  factory DataProduct.fromJson(Map<String, dynamic> json) => DataProduct(
        id: json["id"],
        identifiant: json["identifiant"],
        categorieId: json["categorieId"],
        speculationId: json["speculationId"],
        quantity: json["quantity"],
        initialQuantity: json["initial_quantity"],
        priceU: json["price_u"],
        createdAt: DateTime.parse(json["created_at"]),
        unitOfMeasurment: json["unitOfMeasurment"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "identifiant": identifiant,
        "categorieId": categorieId,
        "speculationId": speculationId,
        "quantity": quantity,
        "initial_quantity": initialQuantity,
        "price_u": priceU,
        "created_at": createdAt.toIso8601String(),
        "unitOfMeasurment": unitOfMeasurment,
      };
}

class Speculation {
  int speculationId;
  String name;
  String imageUrl;

  Speculation({
    required this.speculationId,
    required this.name,
    required this.imageUrl,
  });

  factory Speculation.fromJson(Map<String, dynamic> json) => Speculation(
        speculationId: json["speculationId"],
        name: json["name"],
        imageUrl: json["imageUrl"],
      );

  Map<String, dynamic> toJson() => {
        "speculationId": speculationId,
        "name": name,
        "imageUrl": imageUrl,
      };
}
