// To parse this JSON data, do
//
//     final notificationModel = notificationModelFromJson(jsonString);

import 'dart:convert';

GetNotificationsResponse getNotificationsResponseFromJson(String str) =>
    GetNotificationsResponse.fromJson(json.decode(str));

String getNotificationsResponseToJson(GetNotificationsResponse data) =>
    json.encode(data.toJson());

class GetNotificationsResponse {
  GetNotificationsResponse({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  int statusCode;
  String statusMessage;
  List<NotificationModel> data;

  factory GetNotificationsResponse.fromJson(Map<String, dynamic> json) =>
      GetNotificationsResponse(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: List<NotificationModel>.from(
            json["data"].map((x) => NotificationModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

NotificationModel notificationModelFromJson(String str) =>
    NotificationModel.fromJson(json.decode(str));

String notificationModelToJson(NotificationModel data) =>
    json.encode(data.toJson());

class NotificationModel {
  String id;
  int userId;
  int? isRead;

  String registrationId;
  String title;
  String message;
  DateTime createdAt;

  NotificationModel({
    required this.id,
    this.isRead,
    required this.userId,
    required this.registrationId,
    required this.title,
    required this.message,
    required this.createdAt,
  });

  factory NotificationModel.fromJson(Map<String, dynamic> json) =>
      NotificationModel(
        id: json["id"].toString(),
        userId: json["userId"],
        isRead: json["isRead"],
        registrationId: json["registrationId"],
        title: json["title"],
        message: json["message"],
        createdAt: DateTime.parse(json["createdAt"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "userId": userId,
        "registrationId": registrationId,
        "title": title,
        "message": message,
        "isRead": isRead,
        "createdAt": createdAt.toIso8601String(),
      };
}
