class RegionModel {
  int? regionId;
  String? name;
  RegionModel({
    this.regionId,
    this.name,
  });
}

class DepartementModel {
  int? departementId;
  String? name;
  RegionModel? region;
  DepartementModel({
    this.departementId,
    this.name,
    this.region,
  });
}

class SousPrefectureModel {
  int? sousPrefectureId;
  String? name;
  DepartementModel? departement;
  SousPrefectureModel({
    this.sousPrefectureId,
    this.name,
    this.departement,
  });
}

class LocaliteModel {
  int? localiteId;
  String? name;
  SousPrefectureModel? sousPrefecture;
  LocaliteModel({
    this.localiteId,
    this.name,
    this.sousPrefecture,
  });
}

class ListRegionModel {
  List<RegionModel> data;
  ListRegionModel(
    this.data,
  );
}

class LocationForWeatherModel {
  String? cityName;
  String? lat;
  String? lng;

  LocationForWeatherModel({
    this.lng,
    this.lat,
    this.cityName,
  });
}

class ListLocationForWeatherModel {
  List<LocationForWeatherModel>? listLocation;

  ListLocationForWeatherModel({
    this.listLocation,
  });
}
