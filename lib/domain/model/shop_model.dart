import 'dart:convert';

GetNbOffreAchatBySpeculation getNbOffreAchatBySpeculationFromJson(String str) =>
    GetNbOffreAchatBySpeculation.fromJson(json.decode(str));

String getNbOffreAchatBySpeculationToJson(GetNbOffreAchatBySpeculation data) =>
    json.encode(data.toJson());

class GetNbOffreAchatBySpeculation {
  GetNbOffreAchatBySpeculation({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  int statusCode;
  String statusMessage;
  List<ItemNbOffreAchatBySpeculation> data;

  factory GetNbOffreAchatBySpeculation.fromJson(Map<String, dynamic> json) =>
      GetNbOffreAchatBySpeculation(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: List<ItemNbOffreAchatBySpeculation>.from(
            json["data"].map((x) => ItemNbOffreAchatBySpeculation.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class ItemNbOffreAchatBySpeculation {
  ItemNbOffreAchatBySpeculation({
    required this.speculationId,
    required this.nombreOffer,
  });

  int speculationId;
  int nombreOffer;

  factory ItemNbOffreAchatBySpeculation.fromJson(Map<String, dynamic> json) =>
      ItemNbOffreAchatBySpeculation(
        speculationId: json["speculationId"],
        nombreOffer: json["nombreOffer"],
      );

  Map<String, dynamic> toJson() => {
        "speculationId": speculationId,
        "nombreOffer": nombreOffer,
      };
}

GetListOffreAchatFind getListOffreAchatFindFromJson(String str) =>
    GetListOffreAchatFind.fromJson(json.decode(str));

String getListOffreAchatFindToJson(GetListOffreAchatFind data) =>
    json.encode(data.toJson());

class GetListOffreAchatFind {
  GetListOffreAchatFind({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  int statusCode;
  String statusMessage;
  List<int> data;

  factory GetListOffreAchatFind.fromJson(Map<String, dynamic> json) =>
      GetListOffreAchatFind(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: List<int>.from(json["data"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": List<dynamic>.from(data.map((x) => x)),
      };
}
