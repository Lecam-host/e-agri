// To parse this JSON data, do
//
//     final creditorModel = creditorModelFromJson(jsonString);

import 'dart:convert';

CreditorModel creditorModelFromJson(String str) => CreditorModel.fromJson(json.decode(str));

String creditorModelToJson(CreditorModel data) => json.encode(data.toJson());

class CreditorModel {
    int? statusCode;
    String? statusMessage;
    DataCreditorModel? data;

    CreditorModel({
        this.statusCode,
        this.statusMessage,
        this.data,
    });

    factory CreditorModel.fromJson(Map<String, dynamic> json) => CreditorModel(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: json["data"] == null ? null : DataCreditorModel.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data?.toJson(),
    };
}

class DataCreditorModel {
    int? total;
    int? totalOfPages;
    int? perPage;
    dynamic sorts;
    dynamic order;
    int? currentPage;
    int? nextPage;
    int? previousPage;
    dynamic filters;
    List<ItemCreditorModel>? items;

    DataCreditorModel({
        this.total,
        this.totalOfPages,
        this.perPage,
        this.sorts,
        this.order,
        this.currentPage,
        this.nextPage,
        this.previousPage,
        this.filters,
        this.items,
    });

    factory DataCreditorModel.fromJson(Map<String, dynamic> json) => DataCreditorModel(
        total: json["total"],
        totalOfPages: json["total_of_pages"],
        perPage: json["per_page"],
        sorts: json["sorts"],
        order: json["order"],
        currentPage: json["current_page"],
        nextPage: json["next_page"],
        previousPage: json["previous_page"],
        filters: json["filters"],
        items: json["items"] == null ? [] : List<ItemCreditorModel>.from(json["items"]!.map((x) => ItemCreditorModel.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "total": total,
        "total_of_pages": totalOfPages,
        "per_page": perPage,
        "sorts": sorts,
        "order": order,
        "current_page": currentPage,
        "next_page": nextPage,
        "previous_page": previousPage,
        "filters": filters,
        "items": items == null ? [] : List<dynamic>.from(items!.map((x) => x.toJson())),
    };
}

class ItemCreditorModel {
    int? id;
    String? name;
    String? entreprise;
    String? businessLine;
    String? email;
    String? address;
    List<String>? phone;
    String? webLink;
    String? image;
    dynamic description;
    List<Credit>? typeCredits;
    bool? isActive;
    bool? isDeleted;
    DateTime? createdAt;
    DateTime? updatedAt;
    dynamic deletedAt;

    ItemCreditorModel({
        this.id,
        this.name,
        this.entreprise,
        this.businessLine,
        this.email,
        this.address,
        this.phone,
        this.webLink,
        this.image,
        this.description,
        this.typeCredits,
        this.isActive,
        this.isDeleted,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
    });

    factory ItemCreditorModel.fromJson(Map<String, dynamic> json) => ItemCreditorModel(
        id: json["id"],
        name: json["name"],
        entreprise: json["entreprise"],
        businessLine: json["businessLine"],
        email: json["email"],
        address: json["address"],
        phone: json["phone"] == null ? [] : List<String>.from(json["phone"]!.map((x) => x)),
        webLink: json["webLink"],
        image: json["image"],
        description: json["description"],
        typeCredits: json["typeCredits"] == null ? [] : List<Credit>.from(json["typeCredits"]!.map((x) => Credit.fromJson(x))),
        isActive: json["isActive"],
        isDeleted: json["isDeleted"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        deletedAt: json["deletedAt"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "entreprise": entreprise,
        "businessLine": businessLine,
        "email": email,
        "address": address,
        "phone": phone == null ? [] : List<dynamic>.from(phone!.map((x) => x)),
        "webLink": webLink,
        "image": image,
        "description": description,
        "typeCredits": typeCredits == null ? [] : List<dynamic>.from(typeCredits!.map((x) => x.toJson())),
        "isActive": isActive,
        "isDeleted": isDeleted,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "deletedAt": deletedAt,
    };
}

class CreditElement {
    int? id;
    String? name;
    String? description;
    String? illustrationImage;
    String? webLink;
    List<String>? contacts;
    Creditor? creditor;
    Creditor? typeCredit;
    List<Credit>? subCredits;
    bool? isActive;
    bool? isDeleted;
    DateTime? createdAt;
    DateTime? updatedAt;
    dynamic deletedAt;

    CreditElement({
        this.id,
        this.name,
        this.description,
        this.illustrationImage,
        this.webLink,
        this.contacts,
        this.creditor,
        this.typeCredit,
        this.subCredits,
        this.isActive,
        this.isDeleted,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
    });

    factory CreditElement.fromJson(Map<String, dynamic> json) => CreditElement(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        illustrationImage: json["illustrationImage"],
        webLink: json["webLink"],
        contacts: json["contacts"] == null ? [] : List<String>.from(json["contacts"]!.map((x) => x)),
        creditor: json["creditor"] == null ? null : Creditor.fromJson(json["creditor"]),
        typeCredit: json["typeCredit"] == null ? null : Creditor.fromJson(json["typeCredit"]),
        subCredits: json["subCredits"] == null ? [] : List<Credit>.from(json["subCredits"]!.map((x) => Credit.fromJson(x))),
        isActive: json["isActive"],
        isDeleted: json["isDeleted"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        deletedAt: json["deletedAt"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "illustrationImage": illustrationImage,
        "webLink": webLink,
        "contacts": contacts == null ? [] : List<dynamic>.from(contacts!.map((x) => x)),
        "creditor": creditor?.toJson(),
        "typeCredit": typeCredit?.toJson(),
        "subCredits": subCredits == null ? [] : List<dynamic>.from(subCredits!.map((x) => x.toJson())),
        "isActive": isActive,
        "isDeleted": isDeleted,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "deletedAt": deletedAt,
    };
}

class Credit {
    int? id;
    String? name;
    String? description;
    String? illustrationImage;
    List<CreditElement>? credits;
    bool? isActive;
    bool? isDeleted;
    DateTime? createdAt;
    DateTime? updatedAt;
    dynamic deletedAt;
    String? webLink;
    double? price;
    List<String>? contacts;

    Credit({
        this.id,
        this.name,
        this.description,
        this.illustrationImage,
        this.credits,
        this.isActive,
        this.isDeleted,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
        this.webLink,
        this.price,
        this.contacts,
    });

    factory Credit.fromJson(Map<String, dynamic> json) => Credit(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        illustrationImage: json["illustrationImage"],
        credits: json["credits"] == null ? [] : List<CreditElement>.from(json["credits"]!.map((x) => CreditElement.fromJson(x))),
        isActive: json["isActive"],
        isDeleted: json["isDeleted"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        deletedAt: json["deletedAt"],
        webLink: json["webLink"],
        price: json["price"],
        contacts: json["contacts"] == null ? [] : List<String>.from(json["contacts"]!.map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "illustrationImage": illustrationImage,
        "credits": credits == null ? [] : List<dynamic>.from(credits!.map((x) => x.toJson())),
        "isActive": isActive,
        "isDeleted": isDeleted,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "deletedAt": deletedAt,
        "webLink": webLink,
        "price": price,
        "contacts": contacts == null ? [] : List<dynamic>.from(contacts!.map((x) => x)),
    };
}

class Creditor {
    String? name;
    int? id;

    Creditor({
        this.name,
        this.id,
    });

    factory Creditor.fromJson(Map<String, dynamic> json) => Creditor(
        name: json["name"],
        id: json["id"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "id": id,
    };
}
