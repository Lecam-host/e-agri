// To parse this JSON data, do
//
//     final userInfo = userInfoFromJson(jsonString);

import 'dart:convert';

UserInfoModel userInfoFromJson(String str) => UserInfoModel.fromJson(json.decode(str));

String userInfoToJson(UserInfoModel data) => json.encode(data.toJson());

class UserInfoModel {
    int? id;
    String? username;
    String? firstname;
    String? lastname;
    String? gender;
    String? birthdate;
    String? phone;
    String? adress;
    String? email;
    String? photo;
    String? coordonates;
    String? localisation;
    String? principalSpeculation;
    int? notation;

    UserInfoModel({
        this.id,
        this.username,
        this.firstname,
        this.lastname,
        this.gender,
        this.birthdate,
        this.phone,
        this.adress,
        this.email,
        this.photo,
        this.coordonates,
        this.localisation,
        this.principalSpeculation,
        this.notation,
    });

    factory UserInfoModel.fromJson(Map<String, dynamic> json) => UserInfoModel(
        id: json["id"],
        username: json["username"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        gender: json["gender"],
        birthdate: json["birthdate"],
        phone: json["phone"],
        adress: json["adress"],
        email: json["email"],
        photo: json["photo"],
        coordonates: json["coordonates"],
        localisation: json["localisation"],
        principalSpeculation: json["principalSpeculation"],
        notation: json["notation"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "username": username,
        "firstname": firstname,
        "lastname": lastname,
        "gender": gender,
        "birthdate": birthdate,
        "phone": phone,
        "adress": adress,
        "email": email,
        "photo": photo,
        "coordonates": coordonates,
        "localisation": localisation,
        "principalSpeculation": principalSpeculation,
        "notation": notation,
    };
}
