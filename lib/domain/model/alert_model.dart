class AlertModel {
  int id;
  int userId;
  int speculationId;

  AlertModel({
    required this.id,
    required this.userId,
    required this.speculationId,
  });
}
