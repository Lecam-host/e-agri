// To parse this JSON data, do
//
//     final mesProduitsCommanderResponse = mesProduitsCommanderResponseFromJson(jsonString);

import 'dart:convert';

import '../../../domain/model/info_by_id_model.dart';

import 'Product.dart';
import 'image_model.dart';

MesProduitsCommanderResponse mesProduitsCommanderResponseFromJson(String str) =>
    MesProduitsCommanderResponse.fromJson(json.decode(str));

String mesProduitsCommanderResponseToJson(MesProduitsCommanderResponse data) =>
    json.encode(data.toJson());

class MesProduitsCommanderResponse {
  String statusCode;
  String statusMessage;
  Data data;

  MesProduitsCommanderResponse({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  factory MesProduitsCommanderResponse.fromJson(Map<String, dynamic> json) =>
      MesProduitsCommanderResponse(
          statusCode: json["statusCode"].toString(),
          statusMessage: json["statusMessage"],
        data: Data.fromJson(json["data"]),
      );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data.toJson(),
      };
}

class Data {
  int total;
  int totalPage;
  int perPage;
  int page;
  dynamic nextPage;
  int lastPage;
  List<ProductCommanderItemModel> items;

  Data({
    required this.total,
    required this.totalPage,
    required this.perPage,
    required this.page,
    this.nextPage,
    required this.lastPage,
    required this.items,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        total: json["total"],
        totalPage: json["totalPage"],
        perPage: json["perPage"],
        page: json["page"],
        nextPage: json["nextPage"],
        lastPage: json["lastPage"],
        items: List<ProductCommanderItemModel>.from(
            json["items"].map((x) => ProductCommanderItemModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "totalPage": totalPage,
        "perPage": perPage,
        "page": page,
        "nextPage": nextPage,
        "lastPage": lastPage,
        "items": List<dynamic>.from(items.map((x) => x.toJson())),
      };
}

class ProductCommanderItemModel {
  int quantity;
  double price;
  int commandId;
  int? shoppingCartId;
  int? shoppingCartItemId;
  String numberCommande;
  int userId;
  int idProduct;
  String productCode;
  String paymentCode;
  ProductModel? productInfo;
  ClientModel? client;

  int fournisseurId;
  DateTime createdAt;

  ProductCommanderItemModel({
    required this.quantity,
    required this.price,
    required this.commandId,
    this.shoppingCartId,
    this.shoppingCartItemId,
    required this.numberCommande,
    required this.userId,
    required this.idProduct,
    required this.productCode,
    required this.paymentCode,
    required this.fournisseurId,
    required this.createdAt,
    this.productInfo,
    this.client,
  });

  factory ProductCommanderItemModel.fromJson(Map<String, dynamic> json) =>
      ProductCommanderItemModel(
        quantity: json["quantity"],
        price: json["price"],
        commandId: json["command_id"],
        shoppingCartId: json["shopping_cart_id"],
        shoppingCartItemId: json["shopping_cart_item_id"],
        numberCommande: json["number_commande"],
        userId: json["user_id"],
        idProduct: json["id_product"],
        productCode: json["product_code"],
        paymentCode: json["payment_code"],
        fournisseurId: json["fournisseur_id"],
        client:json["user"]!=null? ClientModel.fromJson(json["user"]):null,
        productInfo: json["product"] != null ? ProductModel.fromJson(json["product"]["data"]) : null,
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "quantity": quantity,
        "price": price,
        "command_id": commandId,
        "shopping_cart_id": shoppingCartId,
        "shopping_cart_item_id": shoppingCartItemId,
        "number_commande": numberCommande,
        "user_id": userId,
        "id_product": idProduct,
        "product_code": productCode,
        "payment_code": paymentCode,
        "fournisseur_id": fournisseurId,
        "created_at": createdAt.toIso8601String(),
      };
}

class ProductInfo {
  int id;
  String code;
  String description;
  String availability;
  String availabilityDate;
  String typeVente;
  String offerType;
  double notation;
  String status;
  DateTime publicationDate;
  // Location location;
  List<ImageProduct> images;
  int categoryId;
  //Category? category;
  int speculationId;
//  Category speculation;

  ProductInfo({
    required this.id,
    required this.code,
    required this.description,
    required this.availability,
    required this.availabilityDate,
    required this.typeVente,
    required this.offerType,
    required this.notation,
    required this.status,
    required this.publicationDate,
    // required this.location,
    required this.images,
    required this.categoryId,
    // required this.category,
    required this.speculationId,
    // required this.speculation,
  });

  factory ProductInfo.fromJson(Map<String, dynamic> json) => ProductInfo(
        id: json["id"],
        code: json["code"],
        description: json["description"],
        availability: json["availability"],
        availabilityDate: json["availabilityDate"],
        typeVente: json["typeVente"],
        offerType: json["offerType"],
        notation: json["notation"],
        status: json["status"],
        publicationDate: DateTime.parse(json["publicationDate"]),
        // location: Location.fromJson(json["location"]),
        images: List<ImageProduct>.from(
            json["images"].map((x) => ImageProduct.fromJson(x))),
        categoryId: json["categoryId"],
        // category: json["category"] != null
        //     ? Category.fromJson(json["category"])
        //     : null,
        speculationId: json["speculationId"],
        //speculation: Category.fromJson(json["speculation"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "code": code,
        "description": description,
        "availability": availability,
        "availabilityDate": availabilityDate,
        "typeVente": typeVente,
        "offerType": offerType,
        "notation": notation,
        "status": status,
        "publicationDate": publicationDate.toIso8601String(),
        // "location": location.toJson(),
        "images": List<dynamic>.from(images.map((x) => x.toJson())),
        "categoryId": categoryId,
        // "category": category.toJson(),
        "speculationId": speculationId,
        //"speculation": speculation.toJson(),
      };
}
