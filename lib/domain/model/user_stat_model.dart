// To parse this JSON data, do
//
//     final getUserStatResponse = getUserStatResponseFromJson(jsonString);

import 'dart:convert';

GetUserStatResponse getUserStatResponseFromJson(String str) => GetUserStatResponse.fromJson(json.decode(str));

String getUserStatResponseToJson(GetUserStatResponse data) => json.encode(data.toJson());

class GetUserStatResponse {
    int statusCode;
    String statusMessage;
    UserStatModel data;

    GetUserStatResponse({
        required this.statusCode,
        required this.statusMessage,
        required this.data,
    });

    factory GetUserStatResponse.fromJson(Map<String, dynamic> json) => GetUserStatResponse(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: UserStatModel.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": data.toJson(),
    };
}

class UserStatModel {
    int? preAchatAvailable;
    int? preAchatUnavailable;
    int? achatAvailable;
    int? achatUnavailable;

    UserStatModel({
         this.preAchatAvailable,
         this.preAchatUnavailable,
         this.achatAvailable,
         this.achatUnavailable,
    });

    factory UserStatModel.fromJson(Map<String, dynamic> json) => UserStatModel(
        preAchatAvailable: json["preAchatAvailable"],
        preAchatUnavailable: json["preAchatUnavailable"],
        achatAvailable: json["achatAvailable"],
        achatUnavailable: json["achatUnavailable"],
    );

    Map<String, dynamic> toJson() => {
        "preAchatAvailable": preAchatAvailable,
        "preAchatUnavailable": preAchatUnavailable,
        "achatAvailable": achatAvailable,
        "achatUnavailable": achatUnavailable,
    };
}
