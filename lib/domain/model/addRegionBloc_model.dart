// ignore_for_file: file_names

import 'dart:convert';

import 'package:eagri/data/request/request_object.dart';

AddIntrantModel addRegionBlocModelFromJson(String str) =>
    AddIntrantModel.fromJson(json.decode(str));

String addRegionBlocModelToJson(AddIntrantModel data) =>
    json.encode(data.toJson());

class AddIntrantModel {
  AddIntrantModel({
    this.fournisseurId,
    this.categorieId,
    this.speculationId,
    this.typeCulture,
    this.pesticideId,
    this.priceU,
    this.quantity,
    this.typeVente,
    this.certification,
    this.availability,
    this.availabilityDate,
    this.expirationDate,
    this.description,
    this.unitOfMeasurment,
    this.location,
    this.offerType,
    this.paymentId,
    this.images,
  });

  int? fournisseurId;
  int? categorieId;
  int? speculationId;
  int? typeCulture;
  int? pesticideId;
  int? priceU;
  int? quantity;
  String? typeVente;
  String? certification;
  String? availability;
  DateTime? availabilityDate;
  DateTime? expirationDate;
  String? description;
  String? unitOfMeasurment;
  AddProductLocationRequest? location;
  String? offerType;
  int? paymentId;
  List<String>? images;
  int? departementId;
  int? localiteId;
  int? sousPrefectureId;
  int? regionId;

  factory AddIntrantModel.fromJson(Map<String, dynamic> json) =>
      AddIntrantModel(
        fournisseurId: json["fournisseurId"],
        categorieId: json["categorieId"],
        speculationId: json["speculationId"],
        typeCulture: json["typeCulture"],
        pesticideId: json["pesticideId"],
        priceU: json["price_u"],
        quantity: json["quantity"],
        typeVente: json["typeVente"],
        certification: json["certification"],
        availability: json["availability"],
        availabilityDate: json["availabilityDate"] == null
            ? null
            : DateTime.parse(json["availabilityDate"]),
        expirationDate: json["expirationDate"] == null
            ? null
            : DateTime.parse(json["expirationDate"]),
        description: json["description"],
        unitOfMeasurment: json["unitOfMeasurment"],
        location: json["location"],
        offerType: json["offerType"],
        paymentId: json["paymentId"],
        images: json["images"] == null
            ? []
            : List<String>.from(json["images"]!.map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "fournisseurId": fournisseurId,
        "categorieId": categorieId,
        "speculationId": speculationId,
        "typeCulture": typeCulture,
        "pesticideId": pesticideId,
        "price_u": priceU,
        "quantity": quantity,
        "typeVente": typeVente,
        "certification": certification,
        "availability": availability,
        "availabilityDate":
            "${availabilityDate!.year.toString().padLeft(4, '0')}-${availabilityDate!.month.toString().padLeft(2, '0')}-${availabilityDate!.day.toString().padLeft(2, '0')}",
        "expirationDate":
            "${expirationDate!.year.toString().padLeft(4, '0')}-${expirationDate!.month.toString().padLeft(2, '0')}-${expirationDate!.day.toString().padLeft(2, '0')}",
        "description": description,
        "unitOfMeasurment": unitOfMeasurment,
        "location": location,
        "offerType": offerType,
        "paymentId": paymentId,
        "images":
            images == null ? [] : List<dynamic>.from(images!.map((x) => x)),
      };
}
