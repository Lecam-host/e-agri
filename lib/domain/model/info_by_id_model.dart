class InfoByIdModel {
  SpeculationInfoModel? speculation;
  Sousprefecture? sousprefecture;
  Departement? departement;
  Localite? localite;
  Region? region;
  ClientModel? user;

  InfoByIdModel(
      {this.speculation,
      this.sousprefecture,
      this.departement,
      this.localite,
      this.region,
      this.user});

  InfoByIdModel.fromJson(Map<String, dynamic> json) {
    speculation = json['speculation'] != null
        ? SpeculationInfoModel.fromJson(json['speculation'])
        : null;
    sousprefecture = json['sousprefecture'] != null
        ? Sousprefecture.fromJson(json['sousprefecture'])
        : null;
    departement = json['departement'] != null
        ? Departement.fromJson(json['departement'])
        : null;
    localite =
        json['localite'] != null ? Localite.fromJson(json['localite']) : null;
    region = json['region'] != null ? Region.fromJson(json['region']) : null;
    user = json['user'] != null ? ClientModel.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (speculation != null) {
      data['speculation'] = speculation!.toJson();
    }
    if (sousprefecture != null) {
      data['sousprefecture'] = sousprefecture!.toJson();
    }
    if (departement != null) {
      data['departement'] = departement!.toJson();
    }
    if (localite != null) {
      data['localite'] = localite!.toJson();
    }
    if (region != null) {
      data['region'] = region!.toJson();
    }
    if (user != null) {
      data['user'] = user!.toJson();
    }
    return data;
  }
}

class SpeculationInfoModel {
  int speculationId;
  String name;
  String imageUrl;

  SpeculationInfoModel(
      {required this.speculationId,
      required this.name,
      required this.imageUrl});

  factory SpeculationInfoModel.fromJson(Map<String, dynamic> json) {
    return SpeculationInfoModel(
      speculationId: json['speculationId'],
      name: json['name'],
      imageUrl: json['imageUrl'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['speculationId'] = speculationId;
    data['name'] = name;
    data['imageUrl'] = imageUrl;
    return data;
  }
}

class Sousprefecture {
  int sousPrefectureId;
  String name;

  Sousprefecture({required this.sousPrefectureId, required this.name});

  factory Sousprefecture.fromJson(Map<String, dynamic> json) {
    return Sousprefecture(
      sousPrefectureId: json['sousPrefectureId'],
      name: json['name'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['sousPrefectureId'] = sousPrefectureId;
    data['name'] = name;
    return data;
  }
}

class Departement {
  int departementId;
  String name;

  Departement({required this.departementId, required this.name});

  factory Departement.fromJson(Map<String, dynamic> json) {
    return Departement(
      departementId: json['departementId'],
      name: json['name'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['departementId'] = departementId;
    data['name'] = name;
    return data;
  }
}

class Localite {
  int localiteId;
  String name;

  Localite({required this.localiteId, required this.name});

  factory Localite.fromJson(Map<String, dynamic> json) {
    return Localite(
      localiteId: json['localiteId'],
      name: json['name'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['localiteId'] = localiteId;
    data['name'] = name;
    return data;
  }
}

class Region {
  int regionId;
  String name;

  Region({required this.regionId, required this.name});

  factory Region.fromJson(Map<String, dynamic> json) {
    return Region(
      regionId: json['regionId'],
      name: json['name'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['regionId'] = regionId;
    data['name'] = name;
    return data;
  }
}

class ClientModel {
  int id;
  String firstname;
  String? laststname;
  String? phone;
  String? email;

  ClientModel(
      {required this.id,
      required this.firstname,
      this.laststname,
      required this.phone,
      required this.email});

  factory ClientModel.fromJson(Map<String, dynamic> json) {
    return ClientModel(
      id: json['id'],
      firstname: json['firstname'],
      laststname: json['lastname'],
      phone: json['phone'],
      email: json['email'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['firstname'] = firstname;
    data['lastname'] = laststname;
    data['phone'] = phone;
    data['email'] = email;
    return data;
  }
}
