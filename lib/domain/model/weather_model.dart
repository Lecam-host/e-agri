class HistoryWeatherModel {
  String? date;
  double? tempMin;
  double? tempMax;
  double? probprecipitation;
  int? clouds;
  String? dayName;
  String? hour;
  double? tempDay;
  double? dewPoint;
  int? humidity;
  int? pressure;
  String? description;
  String? iconLink;
  double? precipitation;
  double? precipitationProbability;
  int? cloudCover;
  double? windSpeed;
  double? windDeg;
  int? dt;

  double? windGust;
  double? pop;
  double? rain;

  String? cityName;

  HistoryWeatherModel({
    this.date,
    this.tempMin,
    this.tempMax,
    this.probprecipitation,
    this.clouds,
    this.dayName,
    this.cityName,
    this.hour,
    this.tempDay,
    this.dewPoint,
    this.humidity,
    this.pressure,
    this.description,
    this.iconLink,
    this.precipitation,
    this.precipitationProbability,
    this.cloudCover,
    this.windSpeed,
    this.windDeg,
    this.windGust,
    this.pop,
    this.rain,
    this.dt,
  });
}

class ListHistoryWeatherModel {
  List<HistoryWeatherModel>? listHistory;
  ListHistoryWeatherModel({this.listHistory});
}

class HoulyWeatherModel {
  String? hour;
  double? temp;
  int? dewPoint;
  int? humidity;
  int? pressure;
  String? description;
  String? iconLink;
  double? precipitation;
  double? precipitationProbability;
  int? cloudCover;
  int? windSpeed;
  int? windDeg;
  int? windGust;

  HoulyWeatherModel({
    this.hour,
    this.temp,
    this.dewPoint,
    this.humidity,
    this.pressure,
    this.description,
    this.iconLink,
    this.precipitation,
    this.precipitationProbability,
    this.cloudCover,
    this.windSpeed,
    this.windDeg,
    this.windGust,
  });
}

class DayWeatherModel {
  String? date;
  double? tempMin;
  double? tempMax;
  double? probprecipitation;
  int? clouds;
  String? dayName;
  String? hour;
  double? temp;
  int? dewPoint;
  int? humidity;
  int? pressure;
  String? description;
  String? iconLink;
  double? precipitation;
  double? precipitationProbability;
  int? cloudCover;
  int? windSpeed;
  int? windDeg;
  int? windGust;
  List<HoulyWeatherModel>? houlyWeather;
  String? cityName;

  DayWeatherModel({
    this.date,
    this.tempMin,
    this.tempMax,
    this.probprecipitation,
    this.clouds,
    this.dayName,
    this.houlyWeather,
    this.cityName,
    this.hour,
    this.temp,
    this.dewPoint,
    this.humidity,
    this.pressure,
    this.description,
    this.iconLink,
    this.precipitation,
    this.precipitationProbability,
    this.cloudCover,
    this.windSpeed,
    this.windDeg,
    this.windGust,
  });
}

class CityWeatherModel {
  List<DayWeatherModel>? listDayWeather;
  String? cityName;
  CityWeatherModel({
    this.cityName,
    this.listDayWeather,
  });
}

class ParcelInfoModel {
  int id;
  int? statusCode;

  int? dt;
  String? parcelId;

  double? t10;
  double? moisture;
  double? t0;
  String? coordinates;
  String? createdAt;
  String? name;

  double? centerLat;
  double? centerLon;

  ParcelInfoModel({
    this.statusCode,
    this.dt,
    this.t10,
    this.moisture,
    this.t0,
    this.parcelId,
    required this.id,
    this.name,
    this.centerLat,
    this.centerLon,
    this.coordinates,
    this.createdAt,
  });
}

class ListParcelModel {
  List<ParcelInfoModel>? listParcel;
  ListParcelModel({
    this.listParcel,
  });
}

class CreateFullParcelResponseModel {
  ListParcelModel? listParcelExist;
  ListParcelModel? listParcelCreated;

  CreateFullParcelResponseModel({
    this.listParcelExist,
    this.listParcelCreated,
  });
}
