import 'package:eagri/domain/model/location_model.dart';
import 'dart:convert';

class BaseApiResponseModel {
  final int? statusCode;
  final String? statusMessage;

  BaseApiResponseModel({
    this.statusCode,
    this.statusMessage,
  });
}

class SelectObjectModel {
  final int? id;
  final String? name;

  SelectObjectModel({
    this.id,
    this.name,
  });
}

class SliderObject {
  String title;
  String subTitle;
  String image;

  SliderObject(this.title, this.subTitle, this.image);
}

class DeviceInfoModel {
  String id;
  String name;

  String version;
  String systemVersion;
  DeviceInfoModel(this.name, this.version, this.systemVersion, this.id);
}

class PaginationModel {
  int totalObject;
  int currentPage;

  int totalOfPages;
  int perPage;
  int nextPage;
  int previousPage;
  PaginationModel(
    this.totalObject,
    this.currentPage,
    this.totalOfPages,
    this.perPage,
    this.nextPage,
    this.previousPage,
  );
}

class AdviceModel {
  int id;
  String title;
  String description;

  String createDate;
  List<FileModel> files;
  int numberView;

  String adviceType;
  List<ObjectModel> categorys;
  String illustration;

  double note;
  int publicateurId;

  AdviceModel(
    this.id,
    this.title,
    this.description,
    this.createDate,
    this.files,
    this.adviceType,
    this.categorys,
    this.numberView,
    this.illustration,
    this.note,
    this.publicateurId,
  );
}

class FileModel {
  int id;
  String title;
  String description;
  int typeFile;
  String fileLink;

  FileModel(
    this.id,
    this.title,
    this.description,
    this.typeFile,
    this.fileLink,
  );
}

class ListAdviceModel {
  List<AdviceModel> listAdvice;
  int totalObject;
  int currentPage;

  int totalOfPages;
  int perPage;
  int nextPage;
  int previousPage;
  ListAdviceModel(
    this.listAdvice,
    this.totalObject,
    this.currentPage,
    this.totalOfPages,
    this.perPage,
    this.nextPage,
    this.previousPage,
  );
}

class AdviceCategoryModel {
  int id;
  int code;
  String description;
  String label;

  AdviceCategoryModel(
    this.id,
    this.code,
    this.description,
    this.label,
  );
}

class ListAdviceCategoryModel {
  List<AdviceCategoryModel> listAdviceCategory;
  ListAdviceCategoryModel(this.listAdviceCategory);
}

class ObjectModel {
  int id;
  String label;

  ObjectModel(this.id, this.label);
}

class SpeculationModel {
  int? speculationId;
  String? name;
  String? imageUrl;
  List<String>? listUnit;
  CategorieSpeculationModel? categoryInfo;

  SpeculationModel(
      {this.speculationId,
      this.name,
      this.imageUrl,
      this.listUnit,
      this.categoryInfo});
}

class CategorieSpeculationModel {
  int? categorieId;
  String? name;

  CategorieSpeculationModel({
    this.categorieId,
    this.name,
  });
}

class LocationModel {
  RegionModel? region;
  DepartementModel? departement;
  SousPrefectureModel? sousPrefecture;
  int? localiteId;
  String? localiteName;
  LocationModel({
    this.region,
    this.departement,
    this.sousPrefecture,
    this.localiteId,
    this.localiteName,
  });
}

class PriceModel {
  int? prixGros;
  int? prixDetails;

  int? speculationId;
  int? regionId;
  int? departementId;
  int? sousPrefectureId;
  int? localiteId;
  String? fromPeriod;
  String? toPeriod;

  String? unit;
  String? source;

  PriceModel({
    this.prixGros,
    this.prixDetails,
    this.speculationId,
    this.regionId,
    this.departementId,
    this.sousPrefectureId,
    this.localiteId,
    this.fromPeriod,
    this.toPeriod,
    this.source,
    this.unit,
  });
}

class ListSpeculationPriceModel {
  List<SpeculationPriceModel>? listSpeculationPrice;

  ListSpeculationPriceModel({
    this.listSpeculationPrice,
  });
}

class SpeculationPriceModel {
  PriceModel? priceInfo;
  SpeculationModel? speculationInfo;
  LocationModel? locationInfo;

  SpeculationPriceModel({
    this.priceInfo,
    this.speculationInfo,
    this.locationInfo,
  });
}

class ListSpeculationModel {
  List<SpeculationModel>? data;

  ListSpeculationModel({
    this.data,
  });
}

class ListDepartementModel {
  List<DepartementModel>? listDepartement;

  ListDepartementModel({
    this.listDepartement,
  });
}

class ListSousPrefectureModel {
  List<SousPrefectureModel>? listSousPrefecture;

  ListSousPrefectureModel({
    this.listSousPrefecture,
  });
}

class ListLocaliteModel {
  List<LocaliteModel>? listLocalite;

  ListLocaliteModel({
    this.listLocalite,
  });
}

class ConfigModel {
  int? id;
  int? code;
  int? isDisable;
  int? typeConfigId;

  String? name;

  ConfigModel({
    this.id,
    this.code,
    this.isDisable,
    this.typeConfigId,
    this.name,
  });
}

class ListConfigModel {
  int? id;
  int? code;
  List<ConfigModel>? listConfig;
  int? typeConfigId;

  String? name;

  ListConfigModel({
    this.id,
    this.code,
    this.listConfig,
    this.typeConfigId,
    this.name,
  });
}

class GetListConfigModel {
  List<ConfigModel>? allConfig;

  GetListConfigModel({
    this.allConfig,
  });
}

class AllConfigModel {
  List<ListConfigModel>? allConfig;

  AllConfigModel({
    this.allConfig,
  });
}

class ListAskAdviceModel {
  List<AskAdviceModel>? listAskAdvice;

  ListAskAdviceModel({
    this.listAskAdvice,
  });
}

class AskAdviceModel {
  int id;
  String description;

  String? advicesType;
  List<String>? typeFiles;
  List<String>? organization;
  List<AdviceCategoryModel>? categoriesAdvice;

  int isResolve;
  String creationDate;

  AskAdviceModel({
    required this.id,
    required this.description,
    this.advicesType,
    this.typeFiles,
    this.organization,
    required this.isResolve,
    required this.creationDate,
    this.categoriesAdvice,
  });
}

class PositionModel {
  double lat;
  double lon;

  PositionModel({
    required this.lon,
    required this.lat,
  });
}

class MinAndMaxSpeculationPriceModel {
  RegionModel minPriceRegion;
  RegionModel maxPriceRegion;

  int minPrice;
  int maxPrice;
  MinAndMaxSpeculationPriceModel({
    required this.minPriceRegion,
    required this.maxPriceRegion,
    required this.maxPrice,
    required this.minPrice,
  });
}

GetListUnitTimeResponse getListUnitTimeResponseFromJson(String str) =>
    GetListUnitTimeResponse.fromJson(json.decode(str));

String getListUnitTimeResponseToJson(GetListUnitTimeResponse data) =>
    json.encode(data.toJson());

class GetListUnitTimeResponse {
  int statusCode;
  String statusMessage;
  List<String> data;

  GetListUnitTimeResponse({
    required this.statusCode,
    required this.statusMessage,
    required this.data,
  });

  factory GetListUnitTimeResponse.fromJson(Map<String, dynamic> json) =>
      GetListUnitTimeResponse(
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
        data: List<String>.from(json["data"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "statusMessage": statusMessage,
        "data": List<dynamic>.from(data.map((x) => x)),
      };
}
