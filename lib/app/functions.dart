import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:dio/dio.dart';
import 'package:eagri/app/constant.dart';
import 'package:eagri/domain/model/Product.dart';
import 'package:eagri/domain/model/user_model.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';

import '../data/network/error_handler.dart';
import '../data/network/failure.dart';
import '../domain/model/model.dart';
import '../presentations/details_produit/details_produit_screen.dart';
import '../presentations/ressources/assets_manager.dart';
import 'app_prefs.dart';
import 'dart:async';




Left<Failure, dynamic> getDioError( error){
   if (error is DioException) {
          DioException erreur = error;
          if (erreur.response != null) {
            if (erreur.response!.data != null) {
              return (Left(
                ErrorHandler.handle(error).failure
                  ..message = erreur.response!.data!["statusMessage"] ??
                      ResponseMessage.defaultResponse
                  ..code = erreur.response!.data!["statusCode"] ??
                      ApiInternalStatus.failure,
              ));
            } else {
              return (Left(ErrorHandler.handle(error).failure));
            }
          } else {
            return (Left(ErrorHandler.handle(error).failure));
          }
        } else {
          return (Left(ErrorHandler.handle(error).failure));
        }
}
extension StringExtension on String {
  String toCapitalize() {
    if (isNotEmpty) {
      if (length > 1) {
        return "${this[0].toUpperCase()}${substring(1).toLowerCase()}";
      } else {
        return toUpperCase();
      }
    } else {
      return "";
    }
  }
}

bool isInScopes(String permission, List<String>? userScopes) {
  // return userScopes.contains(permission);
  return true;
}

bool hasAtLeastOneScope(List<String> permissions, List<String>? userScopes) {
  List<String> communs = [];
  if(userScopes!=null){
     for (String element in permissions) { 
    if (isInScopes(element, userScopes) == true) {
      communs.add(element);
      break;
    }
  }
  }
 
  return communs.isNotEmpty?true  :false;
}

void makePhoneCall(String phoneNumber) async {
  //String url = 'tel:$phoneNumber';
  Get.bottomSheet(
      isScrollControlled: true,
      Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(
              height: 10,
            ),
            Text(
              "Voulez-vous vraiment appeler le $phoneNumber ?",
              style: getMeduimTextStyle(
                fontSize: 14,
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 10,
            ),
            TextButton(
              onPressed: () {
                Get.back();

                FlutterPhoneDirectCaller.callNumber(phoneNumber);
              },
              child: Text(
                'Appeler',
                style: getMeduimTextStyle(
                  color: ColorManager.blue,
                ),
              ),
            ),
            TextButton(
              onPressed: () {
                Get.back();
              },
              child: Text(
                'Annuler',
                style: getRegularTextStyle(color: ColorManager.grey),
              ),
            ),
          ],
        ),
      ));
}

// Future<bool> checkInternetStability() async {
//   var connectivityResult = await (Connectivity().checkConnectivity());
//   if (connectivityResult == ConnectivityResult.none) {
//     return false;
//   }

//   final stopwatch = Stopwatch()..start();
//   final response = await get('https://www.google.com');
//   stopwatch.stop();

//   if (response.statusCode == 200) {
//     final speed = response.contentLength / stopwatch.elapsedMilliseconds;
//     // Si la vitesse de téléchargement est supérieure à un seuil de stabilité
//     // alors on considère la connexion comme stable.
//     return speed > 1024; // 1024 octets par milliseconde (1 Mbps)
//   } else {
//     return false;
//   }
// }
bool estEntier(num nombre) {
  return nombre.isNaN;
}

bool estMultipleDeCinq(int nombre) {
  if (nombre % 5 == 0) {
    return true;
  } else {
    return false;
  }
}

bool verifPrice(int nombre) {
  if (estMultipleDeCinq(nombre) == false) {
    return false;
  } else if (nombre <= 0) {
    return false;
  } else {
    return true;
  }
}

bool validQuantity(int nombre) {
  if (nombre <= 0) {
    return false;
  } else {
    return true;
  }
}

Reseau determinateReseau(String number) {
  String twoFirstLetter = number[0] + number[1];
  if (twoFirstLetter == '07') {
    return Reseau.orange;
  } else if (twoFirstLetter == '01') {
    return Reseau.moov;
  } else {
    return Reseau.mtn;
  }
}

Future<bool> createCatalogue() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  UserModel userInfo =
      await AppSharedPreference(sharedPreferences).getUserInformation();

  try {
    await Dio().post(
      "${Constant.baseUrl}${Constant.urlMsCatalogs}",
      data: {"fournisseurId": userInfo.id},
    );
    return true;
  } catch (e) {
    return false;
  }
}

bool valideNumber(
  String numero,
) {
  bool numberValide = true;
  if (numero.length == 10) {
    String twoFirstLetter = numero[0] + numero[1];
    if (twoFirstLetter != "07" &&
        twoFirstLetter != "05" &&
        twoFirstLetter != "01") {
      numberValide = false;
    }
  } else {
    numberValide = false;
  }

  return numberValide;
}

String createTableInBD(String tableName, String colunmsSql) {
  return "CREATE TABLE $tableName (localId 'INTEGER PRIMARY KEY AUTOINCREMENT',$colunmsSql)";
}

dynamic sendProdDetailArgument(ProductModel data, bool isIntrant) {
  return {
    "produit": ProductDetailsArguments(
      product: data,
      isIntrant: isIntrant,
    )
  };
}

String separateur(double nbre) {
  final oCcy = NumberFormat("#,##0.00", "en_US");
  String fullConvert = oCcy.format(nbre);
  return fullConvert.replaceAll(".00", "").replaceAll(",", " ");
}

String returLinkImageFnHeure() {
  int hour = DateTime.now().hour;
   if (15 <= hour || hour <= 18) {
    return ImageAssets.connexionBg;
  } else {
    return ImageAssets.humidity;
  }
}

Future<DeviceInfoModel> getDeviceInfo() async {
  DeviceInfoModel deviceInfo = DeviceInfoModel("", "", "", "");
  DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();

  try {
    if (Platform.isAndroid) {
      var build = await deviceInfoPlugin.androidInfo;
      deviceInfo.name = "${build.brand} ${build.model}";
      deviceInfo.id = build.id;
      deviceInfo.version = build.version.codename;
    } else if (Platform.isIOS) {
      var build = await deviceInfoPlugin.iosInfo;
      deviceInfo.name = "${build.name} ${build.model}";
      deviceInfo.id = build.identifierForVendor!;
      deviceInfo.version = build.systemVersion;
    }
  } on PlatformException {
    deviceInfo;
  }

  return deviceInfo;
}

String formatPlayTime(Duration duration) {
  String twoDigits(int n) => n.toString().padLeft(2, '0');
  final hours = twoDigits(duration.inHours.remainder(60));
  final minutes = twoDigits(duration.inMinutes.remainder(60));
  final seconds = twoDigits(duration.inSeconds.remainder(60));

  return [if (duration.inHours > 0) hours, minutes, seconds].join(":");
}

String convertDateWithTime(String theDate) {
  return convertHourAndDate(theDate);
}

String convertHourAndDate(String theDate) {
  return "${convertDate(theDate)} - ${convertHourMinite(DateTime.parse(theDate))}";
}

String convertDate(String theDate) {
  String dateConvert = "";
  if(theDate.isNotEmpty){
    DateTime dt = DateTime.parse(theDate);

  
  String mois = "";
  String jour = dt.day.toString();
  if (jour.length < 2) {
    jour = "0$jour";
  }

  String annee = dt.year.toString();

  mois = Constant.listMonth[dt.month - 1];

  dateConvert = "$jour $mois $annee";
  }
  
  return dateConvert;
}

String convertHourMinite(DateTime theDate) {
  String heure = theDate.hour.toString();
  String minute = theDate.minute.toString();

  if (heure.length < 2) {
    heure = "0${theDate.hour}";
  }
  if (minute.length < 2) {
    minute = "0${theDate.minute}";
  }
  return "$heure:$minute";
}

// Future checkGps() async {
//   if (!(await Geolocator.isLocationServiceEnabled())) {
//     if (Theme.of(context).platform == TargetPlatform.android) {
//       showDialog(
//         context: context,
//         builder: (BuildContext context) {
//           return AlertDialog(
//             title: const Text("Accerder à votre postion"),
//             content:
//                 const Text('Please make sure you enable GPS and try again'),
//             actions: <Widget>[
//               ElevatedButton(
//                 child: const Text('Ok'),
//                 onPressed: () {
//                   AndroidIntent intent = const AndroidIntent(
//                       action: 'android.settings.LOCATION_SOURCE_SETTINGS');

//                   intent.launch();
//                   Navigator.of(context, rootNavigator: true).pop();
//                 },
//               ),
//             ],
//           );
//         },
//       );
//     }
//   }
// }

Future<Position> determinePosition() async {
  LocationPermission permission;

  // if (!serviceEnabled) {
  //   checkGps();
  //   //return Future.error('Location services are disabled.');
  // }

  permission = await Geolocator.checkPermission();
  if (permission == LocationPermission.denied) {
    permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.denied) {
      return Future.error('Location permissions are denied');
    }
  }

  if (permission == LocationPermission.deniedForever) {
    return Future.error(
        'Location permissions are permanently denied, we cannot request permissions.');
  }

  return await Geolocator.getCurrentPosition();
}

// import 'package:ping_discover_network/ping_discover_network.dart';
class SnackBarService {
  static final scaffoldKey = GlobalKey<ScaffoldMessengerState>();

  static void showSnackBar({required String content}) {
    scaffoldKey.currentState?.showSnackBar(SnackBar(content: Text(content)));
  }
}


// yaml
// Copy code
// dependencies:
//   telephony: ^1.2.0
// import 'package:telephony/telephony.dart';

// Telephony telephony = Telephony.instance;
// bool permissionGranted = await telephony.requestPhoneStatePermissions;
// int signalStrength = await telephony.signalStrength;