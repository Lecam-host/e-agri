
import 'package:eagri/presentations/cart/controller/cart_controller.dart';
import 'package:eagri/presentations/command/controller/command_controller.dart';
import 'package:eagri/presentations/shop/controller/shop_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/connection_controller.dart';
import '../controllers/location_controller.dart';
import '../controllers/speculation_controller.dart';
import '../presentations/notifications/controller/notification_controller.dart';
import '../presentations/ressources/routes_manager.dart';
import '../presentations/ressources/theme_manager.dart';

class MyApp extends StatefulWidget {
  const MyApp({Key? key, required this.initialRoute}) : super(key: key);
  final String initialRoute;
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  ConnectionController connectionController = Get.put(ConnectionController());

  @override
  void initState() {
    connectionController.checkNetworkQuality();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      onGenerateRoute: RouteGenerator.getRoute,
      initialRoute: widget.initialRoute,
      theme: getThemeDataApp(context),
      getPages: PageGenerator.listPage(),
      initialBinding: BindingsBuilder(() {
        Get.put(SpeculationController());
        Get.put(ShopController());
        Get.put(CartController());
        Get.put(LocationController());
        Get.put(ConnectionController());
        Get.put(CommandCrontroller());
        Get.put(NotificationController());

      }),
    );
  }
}
