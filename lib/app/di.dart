import 'package:eagri/domain/usecase/advice_usecase.dart';
import 'package:eagri/domain/usecase/config_usecase.dart';
import 'package:eagri/domain/usecase/paiement_usecase.dart';
import 'package:eagri/domain/usecase/params_usecase.dart';
import 'package:eagri/domain/usecase/shop_usecase.dart';

import 'package:eagri/domain/usecase/weather_usecase.dart';
import 'package:eagri/presentations/conseils/conseil_view_model.dart';
import 'package:eagri/service/notification/notification_service.dart';
import 'package:get_it/get_it.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../data/data_source/remote_data_source.dart';
import '../data/db/data_base_helpers.dart';
import '../data/network/app_api.dart';
import '../data/network/dio_factory.dart';
import '../data/network/network_info.dart';
import '../data/repository/api/repository_impl.dart';
import '../domain/usecase/alert_usecase.dart';
import '../domain/usecase/location_usecase.dart';
import '../domain/usecase/login_usecase.dart';
import '../domain/usecase/rate_usecase.dart';
import '../domain/usecase/speculation_price_usecase.dart';
import '../domain/usecase/use_case.dart';
import '../presentations/login/login_view_model.dart';
import '../presentations/market_price/market_price_view_model.dart';
import '../presentations/meteo/weather_view_model.dart';
import '../service/avis/vos_avis_service.dart';
import 'app_prefs.dart';

final instance = GetIt.instance;

Future<void> initAppModule() async {
  await DatabaseHelper.initDb();
  final sharedPrefs = await SharedPreferences.getInstance();

  // shared prefs instance
  instance.registerLazySingleton<SharedPreferences>(() => sharedPrefs);

  // app prefs instance
  instance.registerLazySingleton<AppSharedPreference>(
      () => AppSharedPreference(instance()));
// alret usecase
  instance.registerLazySingleton<AlertUserCase>(
      () => AlertUserCase(instance(), instance(), instance()));

  instance.registerLazySingleton<LocationUsecase>(
      () => LocationUsecase(instance()));

  instance.registerLazySingleton<PaiementUsecase>(
      () => PaiementUsecase(instance()));

  instance.registerLazySingleton<RateUseCase>(() => RateUseCase(instance()));

  // ConfigUsecaseConfigUsecaseConfigUsecaseConfigUsecase

  instance
      .registerLazySingleton<ConfigUsecase>(() => ConfigUsecase(instance()));
//UseCase
  instance.registerLazySingleton<UseCase>(() => UseCase(instance()));

  instance
      .registerLazySingleton<ParamsUseCase>(() => ParamsUseCase(instance()));
  instance.registerLazySingleton<ShopUseCase>(() => ShopUseCase(instance()));

  // network info
  instance.registerLazySingleton<NetworkInfo>(
      () => NetworkInfoImpl(InternetConnectionChecker()));

  // dio factory
  instance.registerLazySingleton<DioFactory>(() => DioFactory(instance()));
  instance.registerLazySingleton<RepositoryImpl>(
      () => RepositoryImpl(instance(), instance()));

  // app  service client
  final dio = await instance<DioFactory>().getDio();
  instance.registerLazySingleton<AppServiceClient>(() => AppServiceClient(
        dio,
      ));

  instance.registerLazySingleton<NotificationService>(() => NotificationService(
        dio,
      ));

  instance.registerLazySingleton<NotificationServiceImpl>(
      () => NotificationServiceImpl(
            instance(),
          ));
  instance.registerLazySingleton<VosAvisService>(() => VosAvisService(
        dio,
      ));
  instance.registerLazySingleton<VosAvisServiceImpl>(() => VosAvisServiceImpl(
        instance(),
      ));
  // remote data source
  instance.registerLazySingleton<RemoteDataSource>(
      () => RemoteDataSourceImplement(instance()));
//SpeculationPriceUseCase
  instance.registerLazySingleton<SpeculationPriceUseCase>(
      () => SpeculationPriceUseCase(instance(), instance()));
  //MarketPriceViewModel
  instance.registerLazySingleton<MarketPriceViewModel>(
      () => MarketPriceViewModel(instance()));

  instance.registerLazySingleton<AdviceUseCase>(
      () => AdviceUseCase(instance(), instance()));
  instance.registerLazySingleton<WeatherViewModel>(
      () => WeatherViewModel(instance()));
//conseil view model
  instance.registerLazySingleton<ConseilViewModel>(
      () => ConseilViewModel(instance()));
//WeatherUseCase
  instance
      .registerLazySingleton<WeatherUseCase>(() => WeatherUseCase(instance()));
  //login view
  instance.registerFactory<LoginUseCase>(() => LoginUseCase(instance()));
  instance.registerFactory<LoginViewModel>(() => LoginViewModel(instance()));
  // repository
  // instance.registerLazySingleton<Repository>(
  //     () => RepositoryImpl(instance(), instance()));
}

initLoginModule() {
  if (!GetIt.I.isRegistered<LoginUseCase>()) {}
}
