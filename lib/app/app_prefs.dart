// ignore_for_file: use_build_context_synchronously

import 'package:eagri/domain/usecase/params_usecase.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../domain/model/model.dart';
import '../domain/model/user_model.dart';
import '../presentations/ressources/language_manager.dart';
import 'di.dart';

const String prefKeyLang = "language";
const String prefKeyuserId = "userId";
const String prefKeyuserNumber = "userNumber";
const String prefKeyuserToken = "userToken";
const String prefKeyuserFirstName = "userFirstName";
const String prefKeyuserLastName = "userFirstLastName";
const String prefKeyFirstUseApp = "firstUseApp";
const String prefKeyCurrentLongitude = "currentLongitude";
const String prefKeyCurrentLatitude = "currentLagitude";
const String prefKeyFirebaseToken = "firebaseToken";
const String prefKeyUserName = "userName";
const String prefKeyUserGender = "userGender";
const String prefKeyUserBirthdate = "userBirthdate";
const String prefKeyScopes = "userScopes";
const String prefKeyConnected = "connected";
const String typesVente = "typesVente";
const String offersType = "offersType";
const String prefKeyUserRate = "userRate";

class AppSharedPreference {
  SharedPreferences sharedPreferences;
  AppSharedPreference(
    this.sharedPreferences,
  );
  Future<String> getAppLanguage() async {
    String? language = sharedPreferences.getString(prefKeyLang);
    if (language != null && language.isNotEmpty) {
      return language;
    } else {
      return LanguageType.english.getValue();
    }
  }

  Future<void> createLoginSession(
      BuildContext context, UserModel userInfo) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt(prefKeyuserId, userInfo.id ?? -1);
    await prefs.setString(prefKeyuserNumber, userInfo.number ?? "");
    await prefs.setString(prefKeyuserFirstName, userInfo.firstName ?? "");
    await prefs.setString(prefKeyuserLastName, userInfo.lastName ?? "");
    await prefs.setString(prefKeyuserToken, "Bearer ${userInfo.token ?? ""}");
    await prefs.setString(prefKeyFirebaseToken, userInfo.firebaseToken ?? "");
    await prefs.setString(prefKeyUserName, userInfo.userName ?? "");
    await prefs.setString(prefKeyUserGender, userInfo.gender ?? "");

    await prefs.setString(prefKeyUserBirthdate, userInfo.birthdate ?? "");
    await prefs.setDouble(prefKeyUserRate, userInfo.rate ?? 0);

    await prefs.setBool(prefKeyConnected, true);

    userInfo.isConnected = true;
    Provider.of<UserModel>(context, listen: false).signalUpdated(userInfo);

    // return prefs.getString(routeConnected) ?? "";
  }

  Future<void> updateLoginSession(
      BuildContext context, UserModel userInfo) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt(prefKeyuserId, userInfo.id ?? -1);
    await prefs.setString(prefKeyuserNumber, userInfo.number ?? "");
    await prefs.setString(prefKeyuserFirstName, userInfo.firstName ?? "");
    await prefs.setString(prefKeyuserLastName, userInfo.lastName ?? "");
    await prefs.setString(prefKeyUserName, userInfo.userName ?? "");
    await prefs.setString(prefKeyUserGender, userInfo.gender ?? "");

    await prefs.setDouble(prefKeyUserRate, userInfo.rate ?? 0);
    await prefs.setString(prefKeyUserBirthdate, userInfo.birthdate ?? "");

    await prefs.setBool(prefKeyConnected, true);

    userInfo.isConnected = true;
    await getUserInformation().then((value) {
      Provider.of<UserModel>(context, listen: false).signalUpdated(value);
    });

    // return prefs.getString(routeConnected) ?? "";
  }

  Future<void> updateScope(BuildContext context, List<String> scopes) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setStringList(prefKeyScopes, scopes);

    await getUserInformation().then((value) {
      Provider.of<UserModel>(context, listen: false).signalUpdated(value);
    });
    // return prefs.getString(routeConnected) ?? "";
  }

  Future<void> logoutUser(
    BuildContext context,
  ) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt(prefKeyuserId, 0);

    prefs.setBool(prefKeyConnected, false);
    prefs.setString(prefKeyuserToken, "");
    prefs.setString(prefKeyuserNumber, "");
    prefs.setString(prefKeyuserFirstName, "");
    prefs.setString(prefKeyuserLastName, "");
    prefs.setString(prefKeyFirebaseToken, "");
    prefs.setString(prefKeyUserName, "");
    prefs.setString(prefKeyUserGender, "");
    prefs.setString(prefKeyUserBirthdate, "");
    prefs.setStringList(prefKeyScopes, []);
    prefs.setDouble(prefKeyUserRate, 0);

    UserModel userInfo = UserModel();
    userInfo.isConnected = false;
    userInfo.id = 0;
    userInfo.photo = "";
    userInfo.firstName = "";
    userInfo.lastName = "";
    userInfo.token = "";
    userInfo.photo = "";
    userInfo.firebaseToken = "";
    userInfo.userName = "";
    userInfo.gender = "";
    userInfo.birthdate = "";
    userInfo.scopes = [];
    userInfo.rate = 0;

    Provider.of<UserModel>(context, listen: false).signalUpdated(userInfo);
  }

  Future<UserModel> getUserInformation() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    UserModel userInfo = UserModel();

    userInfo.id = prefs.getInt(prefKeyuserId) ?? -1;
    userInfo.firstName = prefs.getString(prefKeyuserFirstName) ?? "";
    userInfo.lastName = prefs.getString(prefKeyuserLastName) ?? "";
    userInfo.token = prefs.getString(prefKeyuserToken) ?? "";
    userInfo.isConnected = prefs.getBool(prefKeyConnected) ?? false;
    userInfo.number = prefs.getString(prefKeyuserNumber) ?? "";
    userInfo.firebaseToken = prefs.getString(prefKeyFirebaseToken) ?? "";
    userInfo.userName = prefs.getString(prefKeyUserName) ?? "";
    userInfo.gender = prefs.getString(prefKeyUserGender) ?? "";
    userInfo.birthdate = prefs.getString(prefKeyUserBirthdate) ?? "";
    userInfo.scopes = prefs.getStringList(prefKeyScopes) ?? [];
    userInfo.rate = prefs.getDouble(prefKeyUserRate) ?? 0;

    

    return userInfo;
  }

  isFirstOpenApp() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(prefKeyFirstUseApp, false);
  }

  setDefaultPosition() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setDouble(prefKeyCurrentLongitude, -4.033333);
    prefs.setDouble(prefKeyCurrentLatitude, 5.316667);
  }

  determinePosition() async {
    bool serviceEnabled;

    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      setDefaultPosition();
      //checkGps();
      //return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      setDefaultPosition();
      if (permission == LocationPermission.denied) {
      } else {
        setCurrentPosition();
      }
    } else {
      setCurrentPosition();
    }

    if (permission == LocationPermission.deniedForever) {
      setDefaultPosition();
    }
  }

  setCurrentPosition() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    Geolocator.getCurrentPosition().then((value) {
      prefs.setDouble(prefKeyCurrentLongitude, value.longitude);
      prefs.setDouble(prefKeyCurrentLatitude, value.latitude);
    });
  }

  openAppGetInfo() async {
    determinePosition();
  }

  Future<PositionModel> getCurrentPosition() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return PositionModel(
        lat: prefs.getDouble(prefKeyCurrentLatitude) ?? 0,
        lon: prefs.getDouble(prefKeyCurrentLongitude) ?? 0);
  }

  Future<bool> verifyOpenApp() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getBool(prefKeyFirstUseApp) ?? true;
  }

  Future<String> getUserToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(prefKeyuserToken) ?? "";
  }

  saveListTypeventeAndOffer() async {
    ParamsUseCase paramsUseCase = instance<ParamsUseCase>();

    final SharedPreferences prefs = await SharedPreferences.getInstance();
    paramsUseCase.getListOffersType().then((response) {
      response.fold(
          (l) => null, (result) => prefs.setStringList(offersType, result));
    });
    paramsUseCase.getListTypeVente().then((response) {
      response.fold(
          (l) => null, (result) => prefs.setStringList(typesVente, result));
    });
    // prefs.setStringList(typesVente, value);
  }

  Future<List<String>> getListTypeOffers() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList(offersType) ?? [];
  }

  Future<List<String>> getListTypeVente() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList(typesVente) ?? [];
  }
}
