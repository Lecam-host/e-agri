// ignore_for_file: unnecessary_null_comparison
int zero = 0;
double zero1 = 0;

String empty = "";
List listEmpty = [];

extension NonNullString on String? {
  String orEmpty() {
    if (this == null) {
      return empty;
    } else {
      return this!;
    }
  }

  String? cleanHtml() {
    return this?.replaceAll(RegExp(r'<[^>]*>'), '');
  }
}

extension NonNullList on List? {
  List orListEmpty() {
    if (this == null) {
      return listEmpty;
    } else {
      return this!;
    }
  }
}

extension NonNullInt on int? {
  int orZero() {
    if (this == null) {
      return zero;
    } else {
      return this!;
    }
  }
}

extension NonNullDouble on double? {
  double orZeroDouble() {
    if (this == null) {
      return zero1;
    } else {
      return this!;
    }
  }
}
