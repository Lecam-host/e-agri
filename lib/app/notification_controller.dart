import 'package:firebase_messaging/firebase_messaging.dart';
import 'dart:async';

import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';

import '../presentations/ressources/routes_manager.dart';

class NotificationFactory {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;

  void init() async {
    await initNotifications();
    // Demander l'autorisation de recevoir des notifications si elle n'est pas déjà accordée
    _firebaseMessaging.requestPermission();

    // Écouter les événements de message entrant
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      // Afficher une notification à l'utilisateur
      onMessage(message);
      onResume(message);
      onLaunch(message);
    });
  }

  Future<void> onMessage(RemoteMessage message) async {
    _showLocalNotification(message);
  }

  Future<void> onResume(RemoteMessage message) async {
    _showLocalNotification(message);
  }

  Future<void> onLaunch(RemoteMessage message) async {
    _showLocalNotification(message);
  }

  void _showLocalNotification(RemoteMessage message) async {
    // Créer une notification locale en utilisant Flutter Local Notification
    // Vous pouvez personnaliser la notification en fonction de vos besoins
    const iosNotification = DarwinNotificationDetails();

    const AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails(
      'your channel id',
      'your channel name',
      importance: Importance.max,
      priority: Priority.high,
      showWhen: false,
    );
    const NotificationDetails platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics, iOS: iosNotification);
    await flutterLocalNotificationsPlugin.show(
      0,
      message.notification!.title,
      message.notification!.body,
      platformChannelSpecifics,
      payload: Routes.detailsProduct,
    );
  }

  Future<void> initNotifications() async {
    AndroidInitializationSettings initializationSettingsAndroid =
        const AndroidInitializationSettings('app_icon');
    final DarwinInitializationSettings initializationSettingsDarwin =
        DarwinInitializationSettings(
      onDidReceiveLocalNotification:
          (int id, String? title, String? body, String? payload) {
        Get.to("/${Routes.cart}");

        // showDialog(
        //   context: context,
        //   builder: (BuildContext context) => CupertinoAlertDialog(
        //     title: Text(title!),
        //     content: Text(body!),
        //     actions: [
        //       CupertinoDialogAction(
        //         isDefaultAction: true,
        //         child: Text('Ok'),
        //         onPressed: () async {
        //           Get.offAll(HomePage());
        //         },
        //       )
        //     ],
        //   ),
        // );
      },
    );

    final InitializationSettings initializationSettings =
        InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsDarwin,
    );
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onDidReceiveNotificationResponse: (res) {
      Get.to("/${Routes.cart}");
    });
    await flutterLocalNotificationsPlugin.initialize(initializationSettings);
  }
}
