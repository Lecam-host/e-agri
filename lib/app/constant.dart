// ignore_for_file: constant_identifier_names

import 'package:animations/animations.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import '../presentations/ressources/size_config.dart';

class Constant {
  static const String confidentialiteUrl =
      "https://eagrici.org/termes-conditions/CGU";

  static const String baseUrlOrdi = "http://192.168.4.39:";
  static const String versionApi = "api/v1";
  static const String localBaseUrl = "http://192.168.4.220:";
  static const String awsBaseUrl = "https://ms-gateway.eagrici.org/$versionApi";

  //static const String baseUrl = "http://84.247.170.134:30248/$versionApi";
  static const String baseUrl = baseUrlLigne;

  static const String baseUrl1 = "https://api.eagri.mobisoft.ci/$versionApi";
  static const String baseUrlO = "http://38.242.148.221:8001/$versionApi";
  static const String urlHono =
      "http://192.168.4.57:9999/api/v1/middleware/custom";

  static const String baseUrlLigne =
      "https://api.eagri.mobisoft.ci/$versionApi";

  static const String portMsAdvice = "8091/$versionApi/advices";
  static const String portMsSecurity = "8083";
  static const String portMsConfig = "8091/$versionApi/advices/type-config";
  static const String portMsCategory = "8091/$versionApi/advices";
  static const String portMsWeather = "8070/$versionApi/weather";
  static const String portMsAlert = "8090/$versionApi/alert";
  static const String portMsPrice = "8093/$versionApi/price";
  static const String portMsLocation = "8094/$versionApi/location";
  static const String portMsSpeculation = "8092/$versionApi/";
  static const String portMsMiddleware = ":9999/$versionApi/middleware/custom";
  static const String portMsOrder = ":9003/$versionApi/orders";
  static const String portMsPaiement = "8074/$versionApi/payment";
  static const String portMsFavorite = ":8076/$versionApi/customer/favorite";

  static const String portMsCatalogs = ":9002/$versionApi/catalogs";

  static const String portMsMicroCreditInsurance =
      "9010/$versionApi/micro-credit-insurance";
  static const String portMsMicroInsurance = "9010/$versionApi/micro-insurance";

  static const String portMsNotation = ":8051/$versionApi/notation";
  static const String portMCatalogs = ":9002/$versionApi/catalogs";
  static const String portMsDisponibilite =
      ":8061/$versionApi/product_availability/subscription";

  static const String portMLogs = ":8020/$versionApi/logs";

  // "http://192.168.4.220:";

  static const String urlMsSpeculation = "/speculation";
  static const String urlMsNotation = "/notation";

  static const String urlMsSecurity = "/security";
  static const String urlMsAssuranceCredit = "/micro-credit-insurance";

  static const String urlMsConsultation = "/customer/consultation";

  static const String urlMsAdvice = "/advices";
  static const String urlMsMiddleware = "middleware/custom";
  static const String urlMsFavorite = "/customer/favorite";

  static const String urlMsAdviceSocle = "/advices-socle";
  static const String urlMsCatalogs = "/catalogs";
  static const String urlMsIntrant = "/catalogs/intrant";

  static const String urlMsPrestation = "/catalogs/purchase";

  static const String urlMsInsurance = "/insurance";
  static const String urlMsCredit = "/credit";

  static const String urlMsProducts = ":9002/$versionApi/catalogs/products";
  static const String urlMsShoppingCart = "/orders/shoppingCarts";
  static const String urltMsPaiement = "/payment";

  static const String urlMsConfig = "/advices/type-config";

  static const String urlMsCategory = "/advices";

  static const String urlMsWeather = "/weather";
  static const String urlMsAlert = "/alert";
  static const String urlMsPrice = "/price";
  static const String urlMsLocation = "/location";
  static const String urlMsLogs = "/logs";
  static const String urlMsNotification = "/notification";

  static const String urlTypeCulture = "/typeCultures";
  static const String urlIntrant = "/intrants";
  static const String urlMsOrder = "/orders";
  static const String urlMsDisponibilite = "/product_availability/subscription";
  static const String token = "api token ";
  static const String localBDName = "eagribd";
  static const double defaultLetterSpacing = 0.5;
  static const String pdfDefault =
      "http://africau.edu/images/default/sample.pdf";

  static List<String> listMonth = [
    "Janvier",
    "Février",
    "Mars",
    "Avril",
    "Mai",
    "Juin",
    "Juillet",
    "Aout",
    "Septembre",
    "Octobre",
    "Novembre",
    "Décembre"
  ];

  static const double widthPoint = 220.0;

  static const int navigatorDuration = 200;

  static const int offset = 10;
  static const int nbImageMax = 4;

  static const List<String> imagesExtension = ["png", "jpg", "jpeg"];
  static const List<String> videoExtension = ["mp4", "mkv"];
  static const List<String> audioExtension = ["mp3", "opus", "ogg"];
}

enum Reseau { orange, moov, mtn }

class CodeConfig {
  static const int organizationCode = 101;
  static const int formatFileCode = 102;
  static const int typeAdviceCode = 103;

  static const int alertDomaineCode = 104;
//_______________________________________-
  static const int videoCode = 101;
  static const int audioCode = 102;
  static const int pdfCode = 103;
  static const int textCode = 104;
  static const int imageCode = 105;
}

class ServiceCode {
  static const int prixMarchee = 100;
  static const int weather = 200;
  static const int advice = 300;
  static const int alert = 400;
  static const int marketPlace = 500;
  static const int intrant = 110;
}

const int codePrestation = 102;
const int codeTransport = 103;

const int codeIntrant = 101;
const int codeProductAgricultural = 100;
const String codePaiementCash = "MP01";
const String codePaiementOnligne = "MP02";
const String codePaiementCredit = "MP03";
const String partialPay = "PARTIALLY_PAID";
const String PAID = "PAID";

Color kPrimaryColor = ColorManager.primary;
const kPrimaryLightColor = Color(0xFFFFECDF);
const kPrimaryGradientColor = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [Color(0xFFFFA53E), Color(0xFFFF7643)],
);
const kSecondaryColor = Color(0xFF979797);
const kTextColor = Color(0xFF757575);

const kAnimationDuration = Duration(milliseconds: 200);

final headingStyle = TextStyle(
  fontSize: getProportionateScreenWidth(28),
  fontWeight: FontWeight.bold,
  color: Colors.black,
  height: 1.5,
);

const defaultDuration = Duration(milliseconds: 250);

// Form Error
final RegExp emailValidatorRegExp =
    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");

final otpInputDecoration = InputDecoration(
  contentPadding:
      EdgeInsets.symmetric(vertical: getProportionateScreenWidth(15)),
  border: outlineInputBorder(),
  focusedBorder: outlineInputBorder(),
  enabledBorder: outlineInputBorder(),
);

OutlineInputBorder outlineInputBorder() {
  return OutlineInputBorder(
    borderRadius: BorderRadius.circular(getProportionateScreenWidth(15)),
    borderSide: const BorderSide(color: kTextColor),
  );
}

const transitionType = ContainerTransitionType.fade;
const transitionDuration = Duration(milliseconds: 500);

class UserPermissions {
  static const String ADDCART = "PLACEDEMARCHE_PRODUIT_AGRICOLE_AJOUTER_PANIER";
  static const String SHOW_PRODUCT_AGRICOLE =
      "PLACEDEMARCHE_PRODUIT_AGRICOLE_VOIR";
  static const String SHOW_PRODUCT_INTRANT = "PLACEDEMARCHE_INTRANT_VOIR";
  static const String SHOW_PRIX_OFFICIEL_PRODUIT =
      "PRIXDEMARCHE_PRIX_OFFICIEL_PRODUIT_AGRICOLE_VOIR";
  static const String SHOW_PRIX_NON_OFFICIEL_PRODUIT =
      "PRIXDEMARCHE_PRIX_NON_OFFICIEL";
  static const String ADD_PRIX = "PRIXDEMARCHE_AJOUTER_PRIX";
  static const String SHOW_HISTORY_PRIX =
      "PRIXDEMARCHE_CONSULTER_HISTORIQUE_PRIX";
  static const String COMPARE_PRIX = "PRIXDEMARCHE_COMPARER_PRIX";
  static const String SHOW_PRESTATION = "PLACEDEMARCHE_PRESTATION_VOIR";
  static const String SHOW_TRANSPORT = "PLACEDEMARCHE_TRANSPORT_VOIR";
  static const String RESERVATION_PRESTATION =
      "PLACEDEMARCHE_PRESTATION_DEMANDE";
  static const String RESERVATION_FILTER = "PLACEDEMARCHE_PRESTATION_FILTRER";
  static const String RESERVATION_TRANSPORT =
      "PLACEDEMARCHE_TRANSPORT_RESERVATION";

  static const String FILTER_TRANSPORT = "PLACEDEMARCHE_TRANSPORT_FILTRER";

  static const String HISTORIQUETRANSCATION_HISTORIQUEVENTE_VOIR =
      "HISTORIQUETRANSCATION_HISTORIQUEVENTE_VOIR";
  static const String HISTORIQUETRANSCATION_HISTORIQUEACHAT_VOIR =
      "HISTORIQUETRANSCATION_HISTORIQUEACHAT_VOIR";

  static const String ADD_INTRANT = "PUBLIEROFFRE_INTRANT_AJOUTER";
  static const String ADD_PROD_AGRICOL =
      "PUBLIEROFFRE_PRODUIT_AGRICOLE_AJOUTER";
  static const String ADD_TRANSPORT = "PUBLIEROFFRE_TRANSPORT_AJOUTER";
  static const String ADD_PRESTATION = "PUBLIEROFFRE_PRESTATION_AJOUTER";
  static const String CREDIT_SHOW = "CREDIT_VOIR";
  static const String CREDIT_DETAIL_SHOW = "CREDIT_VOIR_DETAIL";
  static const String CREDIT_FILTRER = "CREDIT_FILTRER";
  static const String ASSURANCE_VOIR = "ASSURANCE_VOIR";

  static const String CATALOGUE_PRODUIT_AGRICOLE_VOIR =
      "CATALOGUE_PRODUIT_AGRICOLE_VOIR";
  static const String CATALOGUE_PRESTATION_VOIR = "CATALOGUE_PRESTATION_VOIR";
  static const String CATALOGUE_TRANSPORT_VOIR = "CATALOGUE_TRANSPORT_VOIR";
  static const String CATALOGUE_INTRANT_VOIR = "CATALOGUE_INTRANT_VOIR";
  static const String PLACEDEMARCHE_PRODUIT_AGRICOLE_VENTE =
      "PLACEDEMARCHE_PRODUIT_AGRICOLE_VENTE";
  static const String PLACEDEMARCHE_PRODUIT_AGRICOLE_ACHAT =
      "PLACEDEMARCHE_PRODUIT_AGRICOLE_ACHAT";

  static const String PUBLIEROFFRE_PRODUIT_AGRICOLE_AJOUTER =
      "PUBLIEROFFRE_PRODUIT_AGRICOLE_AJOUTER";
  static const String PUBLIEROFFRE_INTRANT_AJOUTER =
      "PUBLIEROFFRE_INTRANT_AJOUTER";
  static const String PUBLIEROFFRE_PRESTATION_AJOUTER =
      "PUBLIEROFFRE_PRESTATION_AJOUTER";
  static const String PUBLIEROFFRE_TRANSPORT_AJOUTER =
      "PUBLIEROFFRE_TRANSPORT_AJOUTER";
  static const String CONSEIL_AGRICOLE_VOIR = "CONSEIL_AGRICOLE_VOIR";
  static const String METEO_RECOMMANDATION_VOIR = "METEO_RECOMMANDATION_VOIR";

  static const String ALERT_VOIR = "ALERT_VOIR";
  static const String ALERT_AJOUTER = "ALERT_AJOUTER";
  static const String FAVORIS_VOIR = "FAVORIS_VOIR";
  static const String FAVORIS_AJOUTER = "FAVORIS_AJOUTER";
  static const String COMMANDE_VOIR = "COMMANDE_VOIR";
  static const String COMMANDE_VOIR_DETAIL = "COMMANDE_VOIR_DETAIL";
  static const String COMMANDE_VOIR_FACTURE = "COMMANDE_VOIR_FACTURE";
}
