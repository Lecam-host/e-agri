import 'package:eagri/controllers/speculation_controller.dart';
import 'package:get/get.dart';

import '../data/repository/local/speculation_repo.dart';
import '../domain/usecase/speculation_price_usecase.dart';
import 'app_prefs.dart';
import 'di.dart';

getAppParams() async {
  AppSharedPreference appSharedPreference = instance<AppSharedPreference>();
  await getListSpeculation();

  appSharedPreference.saveListTypeventeAndOffer();
}

getListSpeculation() async {
  SpeculationController speculationController =
      Get.put(SpeculationController());
  SpeculationPriceUseCase speculationPriceUseCase =
      instance<SpeculationPriceUseCase>();
  SpeculationRepo speculationRepo = SpeculationRepo();

  await speculationPriceUseCase
      .getListSpeculation()
      .then((value) => value.fold((l) {}, (r) async {
            speculationController.listSpeculationOnline.value = r.data!;

            await speculationRepo.insertAll(r.data!);
          }));
}
