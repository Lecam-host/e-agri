import 'package:eagri/app/functions.dart';

import 'package:eagri/presentations/command/controller/command_controller.dart';

import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/icon_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

import '../../app/constant.dart';

import '../../domain/model/command_model.dart';
import '../../domain/model/user_model.dart';
import '../common/state/empty_component.dart';
import '../ressources/assets_manager.dart';
import 'command_screen.dart';

class ListCommand extends StatefulWidget {
  const ListCommand({
    Key? key,
    required this.listCommand,
  }) : super(key: key);

  final List<CommandModel> listCommand;

  @override
  State<ListCommand> createState() => _ListCommandState();
}

class _ListCommandState extends State<ListCommand> {
  CommandCrontroller commandCrontroller = Get.put(CommandCrontroller());

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
        builder: (context, user, child) => Scaffold(
              body: widget.listCommand.isNotEmpty
                  ? Column(
                      children: [
                        Expanded(
                          child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: widget.listCommand.length,
                            itemBuilder: (context, index) => Container(
                              color: ColorManager.white,
                              margin: const EdgeInsets.only(top: 10),
                              padding: const EdgeInsets.all(10),
                              child: ListTile(
                                onTap: () async {
                                  if (isInScopes(
                                      UserPermissions.COMMANDE_VOIR_DETAIL,
                                      user.scopes!)) {
                                    Get.to(
                                      CommandScreen(
                                        commandModel: widget.listCommand[index],
                                      ),
                                    );
                                  }
                                },
                                title: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "N° ${widget.listCommand[index].numberCommande}",
                                      style: getBoldTextStyle(
                                          color: ColorManager.blue),
                                    ),
                                    Text(
                                      "${widget.listCommand[index].shoppingCart.cartItems.length} produits",
                                    ),
                                    Row(
                                      children: [
                                        const Text('Total : '),
                                        Text(
                                          "${widget.listCommand[index].shoppingCart.totalPrice.toString()} FCFA",
                                          style: getBoldTextStyle(
                                            color: Colors.orange,
                                            fontSize: 15,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                subtitle: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      convertDate(
                                        widget.listCommand[index].createdAt
                                            .toString(),
                                      ),
                                      style: getRegularTextStyle(
                                        color: ColorManager.grey,
                                      ),
                                    ),
                                    if (widget.listCommand[index].status
                                            .libelle ==
                                        "PAID")
                                      Container(
                                        padding: const EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                          color: ColorManager.primary
                                              .withOpacity(0.2),
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                        child: Text(
                                          'Payée',
                                          style: getMeduimTextStyle(
                                            color: ColorManager.primary,
                                            fontSize: 10,
                                          ),
                                        ),
                                      ),
                                    if (widget.listCommand[index].status
                                            .libelle ==
                                        partialPay)
                                      Container(
                                        padding: const EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                          color: ColorManager.jaune
                                              .withOpacity(0.2),
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                        child: Text(
                                          'Partiellement payée',
                                          style: getMeduimTextStyle(
                                            color: ColorManager.primary,
                                            fontSize: 10,
                                          ),
                                        ),
                                      ),
                                  ],
                                ),
                                trailing: const Icon(IconManager.arrowRight),
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  : EmptyComponenent(
                      icon: JsonAssets.empty,
                      message: "Vous n'avez aucune commande",
                    ),
            ));
  }
}
