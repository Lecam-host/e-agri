import 'dart:developer';

import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/data/repository/api/repository_impl.dart';
import 'package:eagri/domain/model/command_model.dart';
import 'package:eagri/presentations/command/command_screen.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:get/get.dart';

import '../../../app/app_prefs.dart';
import '../../../app/di.dart';
import '../../../domain/model/user_model.dart';
import '../../../domain/usecase/shop_usecase.dart';
import '../../cart/controller/cart_controller.dart';
import '../../common/flushAlert_componenent.dart';
import '../../ressources/color_manager.dart';
import '../command_facture.dart';

class CommandCrontroller extends GetxController {
  var userInfo = UserModel().obs;

  ShopUseCase shopUseCase = instance<ShopUseCase>();
  CartController cartController = Get.put(CartController());
  List<CommandModel> listCommandWait = <CommandModel>[].obs;
  List<CommandModel> allCommand = <CommandModel>[].obs;
  List<CommandModel> listCommandPartial = <CommandModel>[].obs;

  List<CommandModel> listCommandPayed = <CommandModel>[].obs;
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  AppSharedPreference appSharedPreference = instance<AppSharedPreference>();
  var isLoadListCommand = true.obs;
  var commandId = 0.obs;

  init() async {
    await getUserInfo();
  //  await cartController.init();
    await getUserCommand();
    // await createCommand(cartController.listCart.value.data[0].id);
  }

  getCommandFacture(int orderId) async {
    Get.dialog(const LoaderComponent());
    await repositoryImpl
        .getCommanFacture(orderId)
        .then((response) => response.fold((failure) {
              inspect(failure);
              Get.back();
            }, (data) {
              Get.back();

              Get.to(CommandFacture(
                factureData: data.factureData,
              ));
              cartController.getUserCart();
            }));
  }

  getUserInfo() async {
    await appSharedPreference
        .getUserInformation()
        .then((value) => userInfo.value = value);
  }

  createCommand(int cartId) async {
    Get.dialog(const LoaderComponent());

    await shopUseCase
        .createCommand(cartController.userInfo.value.id!, cartId)
        .then((value) => value.fold((l) {
              Get.back();
              showCustomFlushbar(Get.context!, "Echec de la commande",
                  ColorManager.error, FlushbarPosition.TOP);
            }, (command) async {
               
              

              // Get.back();

              Get.off(CommandScreen(commandModel: command.data));
                showCustomFlushbar(Get.context!, "Commande éffectuée",
                  ColorManager.succes, FlushbarPosition.TOP);
                cartController.cartItems.value = [];
                cartController.  listCart.value.data = [];
            }));
  }

  getUserCommand() async {
    isLoadListCommand.value = true;
    listCommandPayed = [];
    listCommandWait = [];
    listCommandPartial = [];
    allCommand = [];

    await appSharedPreference.getUserInformation().then((user) async {
      await shopUseCase
          .getUserCommand(user.id!)
          .then((value) => value.fold((l) {
                inspect(l);
              }, (data) async {
                allCommand = data.data;

                await Future.forEach(data.data, (CommandModel element) {
                  if (element.status.libelle == "CONFIRMED") {
                    listCommandWait.add(element);
                  } else if (element.status.libelle == "PAID") {
                    listCommandPayed.add(element);
                  } else if (element.status.libelle == "PARTIALLY_PAID") {
                    listCommandPartial.add(element);
                  }
                });
              }));
    });
    isLoadListCommand.value = false;
  }

  deleteCommand(int commandId) async {
    Get.dialog(const LoaderComponent());
    await repositoryImpl.deleteCommand(commandId).then((response) async {
      response.fold((l) {
        Get.back();
      }, (data) async {
        Get.back();
        Get.back();
        showCustomFlushbar(Get.context!, "Commande annulée",
            ColorManager.primary, FlushbarPosition.TOP);
        await getUserCommand();
      });
    });
  }
}
