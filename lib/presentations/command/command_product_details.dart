import 'package:eagri/domain/model/Product.dart';
import 'package:eagri/presentations/common/default_button.dart';
import 'package:eagri/presentations/shop/controller/shop_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import '../../app/constant.dart';
import '../common/rate/controller/rate_controller.dart';
import '../details_produit/components/product_description.dart';
import '../details_produit/components/product_images.dart';
import '../ressources/color_manager.dart';
import '../ressources/styles_manager.dart';
import 'controller/command_controller.dart';

class CommandProductDetails extends StatefulWidget {
  const CommandProductDetails(
      {Key? key, required this.product, required this.isPayed})
      : super(key: key);
  final ProductModel product;
  final bool isPayed;

  @override
  State<CommandProductDetails> createState() => _CommandProductDetailsState();
}

class _CommandProductDetailsState extends State<CommandProductDetails> {
  var rateController = Get.put(RateController());
  ShopController shopController = Get.find();
  CommandCrontroller commandCrontroller = Get.find();
  final _focusNode = FocusNode();
  late ProductModel productShow;
  @override
  void initState() {
    productShow = widget.product;
    rateController.personneNoteId.value = widget.product.fournisseurId;
    rateController.resourceIdObs.value = widget.product.id;
    super.initState();
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          ProductImages(
            product: productShow,
          ),
          ProductDescription(
            product: productShow,
            pressOnSeeMore: () {},
          ),
          if (productShow.alreadyNoted == false && widget.isPayed)
            Container(
              margin: const EdgeInsets.all(10),
              child: DefaultButton(
                text: "Noté",
                press: () {
                  rate();
                },
              ),
            ),
        ],
      ),
    );
  }

  rate() {
    Get.bottomSheet(
      isScrollControlled: false,
      Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10)),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Entrer votre note",
                style: getBoldTextStyle(color: Colors.black, fontSize: 20),
              ),
              RatingBar.builder(
                initialRating: productShow.rating,
                minRating: 0,
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 5,
                itemSize: 50,
                itemPadding: const EdgeInsets.symmetric(horizontal: 1),
                itemBuilder: (context, _) => const Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                onRatingUpdate: (rating) {
                  rateController.rating.value = rating.toInt();
                },
              ),
              const SizedBox(
                height: 30,
              ),
              Text(
                "Entrer un commentaire",
                style: getRegularTextStyle(color: Colors.black, fontSize: 12),
              ),
              const SizedBox(
                height: 10,
              ),
              TextField(
                controller: rateController.commantaire.value,
                maxLines: null,
                focusNode: _focusNode,
                decoration: InputDecoration(
                  // hintText:"La ",
                  label: Text(
                    "Commentaire",
                    style: getRegularTextStyle(color: Colors.grey),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 100,
                    child: DefaultButton(
                      color: ColorManager.grey,
                      text: "Annuler",
                      press: () {
                        Get.back();
                      },
                    ),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  if (productShow.alreadyNoted == false)
                    SizedBox(
                      width: 100,
                      child: DefaultButton(
                        text: "Envoyer",
                        press: () async {
                          if (rateController.rating.value > 0) {
                            productShow.alreadyNoted =
                                await rateController.ratingRessource(
                                    ServiceCode.marketPlace.toString());
                            setState(() {
                              productShow;
                            });
                            commandCrontroller.getUserCommand();
                          }
                        },
                      ),
                    ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
