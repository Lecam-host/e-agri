import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:flutter/material.dart';

import '../../domain/model/command_model.dart';
import '../common/pdf_screen.dart';

class CommandFacture extends StatefulWidget {
  const CommandFacture({Key? key, required this.factureData}) : super(key: key);

  final FactureData factureData;
  @override
  State<CommandFacture> createState() => _CommandFactureState();
}

class _CommandFactureState extends State<CommandFacture> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: const BackButtonCustom(),
        ),
        body: PdfScreenn(
          pdfLink: widget.factureData.invoiceUrl,
        ));
  }
}
