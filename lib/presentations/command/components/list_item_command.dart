import 'package:eagri/app/constant.dart';
import 'package:eagri/presentations/command/components/command_card.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../domain/model/cart_model.dart';
import '../../../domain/model/command_model.dart';
import '../../cart/controller/cart_controller.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/styles_manager.dart';

class ListItemCommand extends StatefulWidget {
  const ListItemCommand(
      {Key? key, required this.commandModel, required this.isPayed})
      : super(key: key);
  final CommandModel commandModel;
  final bool isPayed;

  @override
  BodyState createState() => BodyState();
}

class BodyState extends State<ListItemCommand> {
  CartController cartController = Get.put(CartController());
  List<CartItem> cartItemsMobileMoney = [];
  List<CartItem> cartItemsCash = [];
  List<CartItem> cartItemsCredit = [];
  String currentCodeList = codePaiementOnligne;

  @override
  void initState() {
    for (CartItem element in widget.commandModel.shoppingCart.cartItems) {
      if (element.product != null) {
        if (element.product!.paymentMethod!.code == codePaiementOnligne) {
          cartItemsMobileMoney.add(element);
        } else if (element.product!.paymentMethod!.code == codePaiementCash) {
          cartItemsCash.add(element);
        } else {
          cartItemsCredit.add(element);
        }
      }
      if (cartItemsMobileMoney.isNotEmpty) {
        currentCodeList = codePaiementOnligne;
      } else if (cartItemsCash.isNotEmpty) {
        currentCodeList = codePaiementCash;
      } else {
        currentCodeList = codePaiementCredit;
      }

      super.initState();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // SingleChildScrollView(
        //   scrollDirection: Axis.horizontal,
        //   child: Row(
        //     children: [
        //       if (cartItemsMobileMoney.isNotEmpty)
        //         categorieBoutton("Mobile money", codePaiementOnligne),
        //       if (cartItemsCash.isNotEmpty)
        //         categorieBoutton("Cash", codePaiementCash),
        //       if (cartItemsCredit.isNotEmpty)
        //         categorieBoutton("Credit", codePaiementCredit)
        //     ],
        //   ),
        // ),
        // if (cartItemsMobileMoney.isNotEmpty &&
        //     currentCodeList == codePaiementOnligne)
        //   Column(
        //     crossAxisAlignment: CrossAxisAlignment.start,
        //     children: [
        //       ...List.generate(
        //         cartItemsMobileMoney.length,
        //         (index) => CommandCard(
        //           cardItem: cartItemsMobileMoney[index],
        //           isPayed: widget.isPayed,
        //         ),
        //       ),
        //     ],
        //   ),

        ...List.generate(
          widget.commandModel.shoppingCart.cartItems.length,
          (index) => CommandCard(
            cardItem: widget.commandModel.shoppingCart.cartItems[index],
            isPayed:widget.commandModel.shoppingCart.cartItems[index].status == "CONFIRMED",
          ),
        ),
        // if (cartItemsCredit.isNotEmpty && currentCodeList == codePaiementCredit)
        //   Column(
        //     children: [
        //       Text("Credit"),
        //       ...List.generate(
        //         cartItemsCredit.length,
        //         (index) => CommandCard(
        //           cardItem: cartItemsCredit[index],
        //           isPayed: widget.isPayed,
        //         ),
        //       )
        //     ],
        //   ),
      ],
    );
  }

  Widget categorieBoutton(String titre, String code) {
    return InkWell(
      onTap: () {
        setState(() {
          currentCodeList = code;
        });
      },
      child: Container(
        width: 110,
        margin: const EdgeInsets.only(
          left: 5,
          bottom: 5,
        ),
        padding: const EdgeInsets.all(5),
        decoration: BoxDecoration(
            color: currentCodeList == code
                ? ColorManager.primary
                : ColorManager.grey1,
            borderRadius: const BorderRadius.all(Radius.circular(10))),
        child: Center(
          child: Text(
            titre,
            style: getRegularTextStyle(
              color: ColorManager.white,
            ),
          ),
        ),
      ),
    );
  }
}
