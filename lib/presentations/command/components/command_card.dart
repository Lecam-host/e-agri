import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/presentations/details_produit/details_produit_screen.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:shimmer/shimmer.dart';

import '../../../app/di.dart';
import '../../../domain/model/Product.dart';
import '../../../domain/model/cart_model.dart';
import '../../../domain/usecase/shop_usecase.dart';
import '../../ressources/color_manager.dart';

class CommandCard extends StatefulWidget {
  const CommandCard({Key? key, required this.cardItem, required this.isPayed})
      : super(key: key);
  final CartItem cardItem;
  final bool isPayed;

  @override
  State<CommandCard> createState() => _CommandCardState();
}

class _CommandCardState extends State<CommandCard> {
  ShopUseCase shopUseCase = instance<ShopUseCase>();
  ProductModel? productShow;
  bool isLoadProduct = true;
  getProductInCommand() async {
    await shopUseCase
        .getDetailProduct(widget.cardItem.idProduct)
        .then((value) => value.fold((l) {
              setState(() {
                isLoadProduct = false;
              });
            }, (prod) async {
              ProductModel pr = (await prod)!;
              setState(() {
                productShow = pr;
                isLoadProduct = false;
              });
            }));
  }

  @override
  void initState() {
    getProductInCommand();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return isLoadProduct == false
        ? productShow != null
            ? InkWell(
                onTap: () {
                  Get.to(
                    DetailsProduitScreen(
                        product:ProductDetailsArguments(product: productShow!,isPay: widget.isPayed) ),
                  );
                },
                child: Container(
                  width: double.infinity,
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: ColorManager.white,
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      if (productShow != null)
                        CachedNetworkImage(
                          imageUrl: productShow!.images != null
                              ? productShow!.images![0]
                              : productShow!.defaultImg!,
                          imageBuilder: (context, imageProvider) => Container(
                            width: 50,
                            height: 60,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                scale: 1,
                                image: imageProvider,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          placeholder: (context, url) => Container(
                            color: ColorManager.grey.withOpacity(0.2),
                            width: 50,
                            height: 60,
                          ),
                          errorWidget: (context, url, error) =>
                              const Icon(Icons.error),
                        ),
                      const SizedBox(
                        width: 5,
                      ),
                      if (productShow != null)
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              productShow!.title,
                              style: getSemiBoldTextStyle(
                                  color: ColorManager.black, fontSize: 12),
                            ),
                            Text(
                              "QTE: ${widget.cardItem.quantity} ${productShow!.unite} ",
                              style: getSemiBoldTextStyle(
                                  color: ColorManager.grey, fontSize: 12),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Text(
                              "Total: ${widget.cardItem.price * widget.cardItem.quantity} FCFA",
                              style: getBoldTextStyle(
                                  color: ColorManager.black, fontSize: 12),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                     if(  productShow!.paymentMethod!=null)     
                            RichText(
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                    text: "Methode paiement : ",
                                    style: getLightTextStyle(
                                        fontSize: 12, color: ColorManager.grey),
                                  ),
                                  TextSpan(
                                    text: productShow!.paymentMethod!.name!,
                                    style: getBoldTextStyle(
                                        fontSize: 12,
                                        color: ColorManager.jaune),
                                  )
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            if (widget.cardItem.status != null)
                              Text(
                                widget.cardItem.status == "PENDING"
                                    ? "Pas encore payé"
                                    : widget.cardItem.status == "CONFIRMED"
                                        ? 'Payé'
                                        : "",
                                style: getBoldTextStyle(
                                    color: widget.cardItem.status == "CONFIRMED"
                                        ? ColorManager.primary
                                        : ColorManager.red,
                                    fontSize: 12),
                              ),
                            const SizedBox(
                              height: 5,
                            ),
                          ],
                        ),
                    ],
                  ),
                ),
              )
            : Container(
                width: double.infinity,
                margin: const EdgeInsets.all(10),
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: ColorManager.white,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Total: ${widget.cardItem.price * widget.cardItem.quantity} FCFA",
                      style: getBoldTextStyle(
                          color: ColorManager.black, fontSize: 12),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Ce produit n'est plus disponible",
                      style: getRegularTextStyle(
                          color: ColorManager.red, fontSize: 14),
                    ),
                  ],
                ))
        : shimmerProductCustom();
  }

  Shimmer shimmerProductCustom() {
    return Shimmer.fromColors(
      baseColor: ColorManager.shimmerColorBaseColor,
      highlightColor: ColorManager.shimmerColorHighlightColor,
      child: Container(
        margin: const EdgeInsets.only(top: 10),
        height: 60,
        child: Row(
          children: [
            Container(
              height: 60,
              margin: const EdgeInsets.only(bottom: 10, left: 10),
              width: 40,
              decoration: BoxDecoration(
                color: ColorManager.white,
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  color: ColorManager.white,
                  height: 10,
                  width: 150,
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  color: ColorManager.white,
                  height: 10,
                  width: 200,
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  color: ColorManager.white,
                  height: 10,
                  width: 100,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
