import 'package:eagri/app/constant.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

import '../../../app/functions.dart';
import '../../../domain/model/cart_model.dart';
import '../../../domain/model/command_model.dart';
import '../../../domain/model/user_model.dart';
import '../controller/command_controller.dart';
import 'list_item_command.dart';

class CommanndBody extends StatefulWidget {
  const CommanndBody({Key? key, required this.commandModel}) : super(key: key);
  final CommandModel commandModel;

  @override
  BodyState createState() => BodyState();
}

class BodyState extends State<CommanndBody> {
  CommandCrontroller commandCrontroller = Get.find();
  int amountPay = 0;
  getamountNotPay() {
    for (CartItem element in widget.commandModel.shoppingCart.cartItems) {
      if (element.status == "CONFIRMED") {
        setState(() {
          amountPay += element.price;
        });
      }
    }
  }

  @override
  void initState() {
    getamountNotPay();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
        builder: (context, user, child) => SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.all(10),
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(color: ColorManager.white),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Nombre de produit : ${widget.commandModel.shoppingCart.cartItems.length}',
                          style: getRegularTextStyle(
                            color: ColorManager.black,
                            fontSize: 13,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Total : ${widget.commandModel.shoppingCart.totalPrice} FCFA',
                              style: getBoldTextStyle(
                                color: ColorManager.black,
                                fontSize: 14,
                              ),
                            ),
                            if (widget.commandModel.status.libelle == "PAID")
                              Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  color: ColorManager.primary.withOpacity(0.2),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: Text(
                                  'Payée',
                                  style: getMeduimTextStyle(
                                    color: ColorManager.primary,
                                    fontSize: 10,
                                  ),
                                ),
                              ),
                            if (widget.commandModel.status.libelle ==
                                partialPay)
                              Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  color: ColorManager.jaune.withOpacity(0.2),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: Text(
                                  'Partiellement payé',
                                  style: getMeduimTextStyle(
                                    color: ColorManager.primary,
                                    fontSize: 10,
                                  ),
                                ),
                              ),
                          ],
                        ),
                        if (widget.commandModel.status.libelle == partialPay)
                          Text(
                            'Total payé :$amountPay FCFA',
                            style: getBoldTextStyle(
                              color: ColorManager.black,
                              fontSize: 14,
                            ),
                          ),
                        Text(
                          convertDateWithTime(
                              widget.commandModel.createdAt.toString()),
                          style: getRegularTextStyle(
                            color: ColorManager.grey,
                            // fontSize: 10,
                          ),
                        ),
                        if ((widget.commandModel.status.libelle == "PAID" ||
                                widget.commandModel.status.libelle ==
                                    partialPay) &&
                            isInScopes(UserPermissions.COMMANDE_VOIR_FACTURE,
                                user.scopes!))
                          InkWell(
                            onTap: () {
                              commandCrontroller
                                  .getCommandFacture(widget.commandModel.id);
                              //  Get.to(CommandFacture());
                            },
                            child: Text(
                              "Voir la facture",
                              style: getBoldTextStyle(color: ColorManager.blue),
                            ),
                          ),
                      ],
                    ),
                  ),
                  ListItemCommand(
                    commandModel: widget.commandModel,
                    isPayed: widget.commandModel.status.libelle == "PAID",
                  ),
                  const SizedBox(
                    height: 50,
                  )
                ],
              ),
            ));
  }
}
