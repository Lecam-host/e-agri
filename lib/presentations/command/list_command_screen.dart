import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../ressources/color_manager.dart';
import '../ressources/styles_manager.dart';
import 'controller/command_controller.dart';
import 'list_command.dart';

class ListCommandScreen extends StatefulWidget {
  const ListCommandScreen({Key? key}) : super(key: key);

  @override
  State<ListCommandScreen> createState() => _ListCommandScreenState();
}

class _ListCommandScreenState extends State<ListCommandScreen> {
  CommandCrontroller commandCrontroller = Get.put(CommandCrontroller());
  @override
  void initState() {
    commandCrontroller.init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
          appBar: AppBar(
            leading: const BackButtonCustom(),
            title: const Text('Mes commandes'),
            bottom: TabBar(
              isScrollable: true,
              indicatorColor: ColorManager.primary,
              tabs: [
                Tab(
                  child: Text(
                    "Tout",
                    style: getMeduimTextStyle(color: ColorManager.black),
                  ),
                ),
                Tab(
                  child: Text(
                    "En attente",
                    style: getMeduimTextStyle(color: ColorManager.black),
                  ),
                ),
                Tab(
                  child: Text(
                    "Partiellement",
                    style: getMeduimTextStyle(color: ColorManager.black),
                  ),
                ),
                Tab(
                  child: Text(
                    "Payer",
                    style: getMeduimTextStyle(color: ColorManager.black),
                  ),
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              Obx(() => commandCrontroller.isLoadListCommand.value == false
                  ? ListCommand(
                      listCommand: commandCrontroller.allCommand,
                    )
                  : const LoaderComponent()),
              Obx(() => commandCrontroller.isLoadListCommand.value == false
                  ? ListCommand(
                      listCommand: commandCrontroller.listCommandWait,
                    )
                  : const LoaderComponent()),
              Obx(() => commandCrontroller.isLoadListCommand.value == false
                  ? ListCommand(
                      listCommand: commandCrontroller.listCommandPartial,
                    )
                  : const LoaderComponent()),
              Obx(() => commandCrontroller.isLoadListCommand.value == false
                  ? ListCommand(
                      listCommand: commandCrontroller.listCommandPayed,
                    )
                  : const LoaderComponent()),
            ],
          )),
    );
  }
}
