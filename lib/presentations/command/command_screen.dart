
import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/domain/model/command_model.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/paiement/controller/paiement_controller.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../app/constant.dart';
import '../../app/di.dart';
import '../../app/functions.dart';
import '../../data/request/paiement_request.dart';
import '../../domain/usecase/paiement_usecase.dart';
import '../../domain/usecase/shop_usecase.dart';
import '../cart/controller/cart_controller.dart';
import '../common/default_button.dart';
import '../common/flushAlert_componenent.dart';
import '../common/state/loader_component.dart';
import '../paiement/after_resquest_payment_cash.dart';
import 'components/command_body.dart';
import 'controller/command_controller.dart';

class CommandScreen extends StatefulWidget {
  const CommandScreen({Key? key, required this.commandModel}) : super(key: key);
  final CommandModel commandModel;
  @override
  State<CommandScreen> createState() => _CommandScreenState();
}

class _CommandScreenState extends State<CommandScreen> {
  Reseau typeReseau = Reseau.orange;

  CartController cartController = Get.put(CartController());
  CommandCrontroller commandCrontroller = Get.put(CommandCrontroller());
  PaiementController paiementController = Get.put(PaiementController());
  ShopUseCase shopUseCase = instance<ShopUseCase>();
  int cashMontant = 0;
  int enLigneMontant = 0;
  int creditMontant = 0;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Future<void> init() async {
    calculAmount();
  }

  calculAmount() {
    setState(() {
      for (var cart in widget.commandModel.shoppingCart.cartItems) {
        if (cart.paymentCode  == codePaiementCash) {
          cashMontant = cashMontant+cart.price;
        }
        if (cart.paymentCode == codePaiementOnligne) {
          enLigneMontant = enLigneMontant+cart.price;
        }
        if (cart.paymentCode == codePaiementCredit) {
          creditMontant =creditMontant+ cart.price;
        }
    }
    });
    
    commandCrontroller.init();
  }

  @override
  void initState() {
    calculAmount();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Details de ma commande',
          style: getBoldTextStyle(color: ColorManager.black),
        ),
        leading: const BackButtonCustom(),
        actions: [
          if (widget.commandModel.status.libelle == "CONFIRMED")
            TextButton(
              child: const Text(
                "Annuler",
              ),
              onPressed: () {
                showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (context) {
                      return AlertDialog(
                        backgroundColor: Colors.white,
                        contentPadding: const EdgeInsets.all(12),
                        title: const Text(
                          "Veuillez confirmer l'annulation",
                          textAlign: TextAlign.center,
                        ),
                        actions: [
                          TextButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: const Text('Annuler',
                                style: TextStyle(color: Colors.grey)),
                          ),
                          TextButton(
                            onPressed: () async {
                              Navigator.pop(context);
                              commandCrontroller
                                  .deleteCommand(widget.commandModel.id);
                            },
                            child: Text('Confirmer',
                                style: TextStyle(color: ColorManager.red)),
                          ),
                        ],
                      );
                    });
              },
            )
        ],
      ),
      body: ListView(
        children: [
          CommanndBody(
            commandModel: widget.commandModel,
          )
        ],
      ),
      bottomSheet: widget.commandModel.status.libelle == "CONFIRMED"
          ? Hero(
              tag: "commandTag",
              child: Container(
                decoration: BoxDecoration(
                    color: ColorManager.lightGrey.withOpacity(0.5)),
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    if (enLigneMontant > 0)
                      Row(
                        children: [
                          Text(
                            "Mobile money",
                            style:
                                getRegularTextStyle(color: ColorManager.black),
                          ),
                          const Spacer(),
                          Text(
                            "${enLigneMontant.toString()} FCFA",
                            style: getSemiBoldTextStyle(
                                color: Colors.black, fontSize: 14),
                          ),
                        ],
                      ),
                    if (cashMontant > 0)
                      Row(
                        children: [
                          Text(
                            "Cash",
                            style:
                                getRegularTextStyle(color: ColorManager.black),
                          ),
                          const Spacer(),
                          Text(
                            "${cashMontant.toString()} FCFA",
                            style: getSemiBoldTextStyle(
                                color: Colors.black, fontSize: 14),
                          ),
                        ],
                      ),
                    // if (creditMontant > 0)
                    //   Row(
                    //     children: [
                    //       Text(
                    //         "Credit",
                    //         style:
                    //             getRegularTextStyle(color: ColorManager.black),
                    //       ),
                    //       const Spacer(),
                    //       Text(
                    //         "${creditMontant.toString()} FCFA",
                    //         style: getSemiBoldTextStyle(
                    //             color: Colors.black, fontSize: 14),
                    //       ),
                    //     ],
                    //   ),
                    // Row(
                    //   children: [
                    //     Text(
                    //       "Frais",
                    //       style: getRegularTextStyle(color: ColorManager.black),
                    //     ),
                    //     const Spacer(),
                    //     Text(
                    //       "${(widget.commandModel.shoppingCart.totalPrice * cartController.fraisPercent) ~/ 100} FCFA",
                    //       style: getSemiBoldTextStyle(
                    //           color: Colors.black, fontSize: 14),
                    //     ),
                    //   ],
                    // ),
                    const Divider(
                      color: Colors.black,
                    ),
                    Row(
                      children: [
                        Text(
                          "Total",
                          style: getSemiBoldTextStyle(
                            color: ColorManager.black,
                            fontSize: 17,
                          ),
                        ),
                        const Spacer(),
                        Text(
                          "${widget.commandModel.shoppingCart.totalPrice} FCFA",
                          style: getBoldTextStyle(
                              color: Colors.orange, fontSize: 19),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    DefaultButton(
                      text: "Payer",
                      press: () {
                        if (enLigneMontant > 0) {
                          numberInput();
                        } else if (cashMontant > 0) {
                          Get.bottomSheet(
                            Container(
                              padding: const EdgeInsets.all(10),
                              margin: const EdgeInsets.symmetric(
                                  horizontal: 7, vertical: 7),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  const Text(
                                    'Vous êtes sur le point de faire unne demande de paiement cash',
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ), 
                                  TextButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    child: Text("Annuler",
                                        style: getBoldTextStyle(
                                            color: ColorManager.blue)),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  SizedBox(
                                    height: 50,
                                    child: DefaultButton(
                                      text: "Valider",
                                      press: () async {
                                        PaiementUsecase paiementUsecase =
                                            instance<PaiementUsecase>();
                                        Get.dialog(const LoaderComponent());
                                        await paiementUsecase
                                            .createPaiement(
                                              CreatePaiementRequest(
                                                  methodId: paiementController
                                                      .listMethodPaiement
                                                      .firstWhere((element) =>
                                                          element.code ==
                                                          codePaiementCash)
                                                      .id!,
                                                  orderId:
                                                      widget.commandModel.id,
                                                  phone: null),
                                            )
                                            .then(
                                              (response) => response.fold(
                                                (failure) {
                                                  Get.back();
                                                  Get.back();
                                                  showCustomFlushbar(
                                                      context,
                                                      failure.message
                                                          .toString(),
                                                      ColorManager.error,
                                                      FlushbarPosition.TOP,
                                                      duration: 5);
                                                },
                                                (r) {
                                                  commandCrontroller
                                                      .getUserCommand();
                                                  Get.back();
                                                  Get.back();
                                                  Get.off(
                                                    AfterRequestPaymentCash(
                                                      message:
                                                          r.data!.instruction ??
                                                               "Demande paiement cash envoyée",
                                                    ),
                                                  );
                                                },
                                              ),
                                            );
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        }else{
                            Get.bottomSheet(
                            Container(
                              padding: const EdgeInsets.all(10),
                              margin: const EdgeInsets.symmetric(
                                  horizontal: 7, vertical: 7),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  const Text(
                                    'Vous êtes sur le point de faire unne demande de paiement à credit',
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    child: Text("Annuler",
                                        style: getBoldTextStyle(
                                            color: ColorManager.blue)),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  SizedBox(
                                    height: 50,
                                    child: DefaultButton(
                                      text: "Valider",
                                      press: () async {
                                        PaiementUsecase paiementUsecase =
                                            instance<PaiementUsecase>();
                                        Get.dialog(const LoaderComponent());
                                        await paiementUsecase
                                            .createPaiement(
                                              CreatePaiementRequest(
                                                  methodId: paiementController
                                                      .listMethodPaiement
                                                      .firstWhere((element) =>
                                                          element.code ==
                                                          codePaiementCredit)
                                                      .id!,
                                                  orderId:
                                                      widget.commandModel.id,
                                                  phone: null),
                                            )
                                            .then(
                                              (response) => response.fold(
                                                (failure) {
                                                  Get.back();
                                                  Get.back();
                                                  showCustomFlushbar(
                                                      context,
                                                      failure.message
                                                          .toString(),
                                                      ColorManager.error,
                                                      FlushbarPosition.TOP,
                                                      duration: 5);
                                                },
                                                (r) {
                                                  commandCrontroller
                                                      .getUserCommand();
                                                  Get.back();
                                                  Get.back();
                                                  Get.off(
                                                    AfterRequestPaymentCash(
                                                      message:
                                                          r.data!.instruction ??
                                                              "Demande paiement à credit envoyée",
                                                    ),
                                                  );
                                                },
                                              ),
                                            );
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );

                        }

                        // if (enLigneMontant <= 0) {
                        //   Get.bottomSheet(Container(
                        //     padding: const EdgeInsets.all(10),
                        //     margin: const EdgeInsets.symmetric(
                        //         horizontal: 7, vertical: 7),
                        //     decoration: BoxDecoration(
                        //         color: Colors.white,
                        //         borderRadius: BorderRadius.circular(10)),
                        //     child: Column(
                        //       crossAxisAlignment: CrossAxisAlignment.center,
                        //       children: [
                        //         Text(
                        //           "Veuillez choisir le moyen de paiement",
                        //           style: getRegularTextStyle(
                        //             color: ColorManager.black,
                        //           ),
                        //         ),
                        //         const SizedBox(
                        //           height: 10,
                        //         ),
                        //         TypePaiementScreen(
                        //           commandData: widget.commandModel,
                        //         ),
                        //       ],
                        //     ),
                        //   ));
                        // }

                        //  Get.to(const PaiementScreen());
                      },
                    ),
                  ],
                ),
              ),
            )
          : const SizedBox(),
    );
  }

  numberInput() {
    Get.bottomSheet(
      Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Vous devez payer maintenant $enLigneMontant FCFA pour les produits avec type de payement par mobile money",
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              'Montant : $enLigneMontant FCFA',
              style: getBoldTextStyle(color: ColorManager.black, fontSize: 14),
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              "Entrer votre numéro de téléphone",
              style: getBoldTextStyle(color: ColorManager.black, fontSize: 14),
            ),
            const SizedBox(
              height: 10,
            ),
            Form(
              key: _formKey,
              child: TextFormField(
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Entrer votre numero';
                  } else if (valideNumber(
                        value,
                      ) ==
                      false) {
                    return 'Numero invalide';
                  } else {
                    return null;
                  }
                },
                controller: paiementController.phoneController.value,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.grey.withOpacity(0.2),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide:
                        const BorderSide(width: 0, style: BorderStyle.none),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide:
                        const BorderSide(width: 0, style: BorderStyle.none),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide:
                        const BorderSide(width: 0, style: BorderStyle.none),
                  ),
                  hintText: "Entrer votre numéro",
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: 150,
              child: DefaultButton(
                text: "Suivant >",
                press: () async {
                  if (_formKey.currentState!.validate()) {
                    Get.back();

                    await paiementController.createPaiement(
                        determinateReseau(
                            paiementController.phoneController.value.text),
                        widget.commandModel.id);
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
