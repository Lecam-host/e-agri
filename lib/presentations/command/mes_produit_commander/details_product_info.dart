import 'package:flutter/material.dart';

import '../../../app/functions.dart';
import '../../../domain/model/mes_produits_commander_model.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/styles_manager.dart';

class DetailsInfoProductOdered extends StatelessWidget {
  final ProductCommanderItemModel product;

  const DetailsInfoProductOdered({Key? key, required this.product})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            product.productInfo!.title,
            style: getBoldTextStyle(
              fontSize: 14,
              color: ColorManager.primary,
            ),
          ),
          Text(
            "Produit",
            style: getRegularTextStyle(
              color: ColorManager.grey,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            "Info sur la commande",
            style: getMeduimTextStyle(
              color: ColorManager.black,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            color: ColorManager.white,
            width: double.infinity,
            padding: const EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                cardInfo(
                  title: "Date de la commande",
                  value: convertDateWithTime(product.createdAt.toString()),
                ),
                const SizedBox(
                  height: 10,
                ),
                if (product.client != null)
                  cardInfo(
                    title: "Client",
                    value:
                        "${product.client!.firstname} ${product.client!.laststname}",
                  ),
                const SizedBox(
                  height: 10,
                ),
                if (product.client != null)
                if (product.client!.phone!= null)
                  cardInfo(
                    title: "Numero du client",
                    value: product.client!.phone!,
                  ),
                const SizedBox(
                  height: 10,
                ),
                cardInfo(
                  title: "Prix",
                  value: "${separateur(product.price)} FCFA",
                ),
                const SizedBox(
                  height: 10,
                ),
                if (product.productInfo != null)
                  cardInfo(
                    title: "Quantité commandée",
                    value: "${product.quantity} ${product.productInfo!.unite}",
                  ),
                const SizedBox(
                  height: 10,
                ),
                cardInfo(
                  title: "Total",
                  value: "${separateur(product.price * product.quantity)} FCFA",
                ),
                const SizedBox(
                  height: 10,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

Widget cardInfo({required String title, required String value, Color? color}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        value,
        style: getBoldTextStyle(
          fontSize: 12,
          color: color ?? ColorManager.black,
        ),
      ),
      Text(
        title,
        style: getRegularTextStyle(
          color: ColorManager.grey,
        ),
      ),
    ],
  );
}
