import 'dart:developer';

import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../app/app_prefs.dart';
import '../../../app/di.dart';
import '../../../data/repository/api/repository_impl.dart';
import '../../../data/request/shop_resquests.dart';
import '../../../domain/model/mes_produits_commander_model.dart';

class MyProdcutOderedController extends GetxController {
  var listProductOdored = <ProductCommanderItemModel>[].obs;
  var isLoadProduct = true.obs;
  int perPage = 50;
  var page = 0.obs;
  var newPrice = TextEditingController().obs;
  AppSharedPreference appSharedPreference = instance<AppSharedPreference>();
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();

  Future<bool> reductPrice(ProductCommanderItemModel product) async {
    bool status = false;
    await repositoryImpl
        .reductPrice(
      product.commandId,
      product.idProduct,
      double.parse(newPrice.value.text),
    )
        .then((response) async {
      await response.fold(
        (failure) async {
          Get.back();
          showCustomFlushbar(Get.context!, failure.message, ColorManager.error,
              FlushbarPosition.TOP);
          status = false;
        },
        (data) async {
          Get.back();
          showCustomFlushbar(Get.context!, "Prix modifié avec succes",
              ColorManager.succes, FlushbarPosition.TOP);
          getListProduct();
          status = true;
        },
      );
    });
    return status;
  }

  getListProduct() async {
    await appSharedPreference.getUserInformation().then((value) async {
      await repositoryImpl
          .getMyProductOdered(
        GetProductCommanderRequest(
          perPage: perPage,
          page: page.value,
          fournisseurId: value.id!,
        ),
      )
          .then((response) async {
        response.fold((failure) {
          
          showCustomFlushbar(Get.context!, failure.message, ColorManager.error, FlushbarPosition.TOP);
          isLoadProduct.value = false;
        }, (data) {
          listProductOdored.value = data.data.items;
          inspect(listProductOdored);
          isLoadProduct.value = false;
        });
      });
    });
  }
}
