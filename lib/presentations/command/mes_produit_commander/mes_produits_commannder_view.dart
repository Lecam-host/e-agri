// ignore_for_file: use_build_context_synchronously

import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../app/constant.dart';
import '../../../app/functions.dart';
import '../../../domain/model/mes_produits_commander_model.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/styles_manager.dart';
import 'details_product_odered.dart';
import 'mes_product_odered_controller.dart';

class MesProduitsCommanderView extends StatefulWidget {
  const MesProduitsCommanderView({Key? key}) : super(key: key);

  @override
  State<MesProduitsCommanderView> createState() =>
      _MesProduitsCommanderViewState();
}

class _MesProduitsCommanderViewState extends State<MesProduitsCommanderView> {
  MyProdcutOderedController myProdcutOderedController =
      Get.put(MyProdcutOderedController());

  @override
  void initState() {
    myProdcutOderedController.getListProduct();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Mes produits commandés"),
        leading: const BackButtonCustom(),
      ),
      body: Obx(
        () => myProdcutOderedController.isLoadProduct.value
            ? const LoaderComponent()
            : myProdcutOderedController.listProductOdored.isEmpty
                ? const EmptyComponenent(message: 'Aucun produit',)
                : ListView.builder(
                    shrinkWrap: true,
                    itemCount:
                        myProdcutOderedController.listProductOdored.length,
                    itemBuilder: (context, index) => productCard(
                      myProdcutOderedController.listProductOdored[index],
                    ),
                  ),
      ),
    );
  }

  Widget productCard(ProductCommanderItemModel productOdored) {
    return GestureDetector(
      onTap: () {
        Get.to(DetailsProductOdoredScreen(
          product: productOdored,
        ));
      },
      child: Container(
        width: double.infinity,
        margin: const EdgeInsets.all(10),
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: ColorManager.white,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            if (productOdored.productInfo!.imagesWithId!.isNotEmpty)
              CachedNetworkImage(
                imageUrl: productOdored.productInfo!.imagesWithId![0].link,
                imageBuilder: (context, imageProvider) => Container(
                  width: 50,
                  height: 60,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                      scale: 1,
                      image: imageProvider,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                placeholder: (context, url) => Container(
                  color: ColorManager.grey.withOpacity(0.2),
                  width: 50,
                  height: 60,
                ),
                errorWidget: (context, url, error) => const Icon(Icons.error),
              ),
            if (productOdored.productInfo!.imagesWithId!.isEmpty)
              Container(
                color: ColorManager.grey.withOpacity(0.2),
                width: 50,
                height: 60,
                child: const Icon(Icons.image),
              ),
            const SizedBox(
              width: 5,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  productOdored.numberCommande,
                  style: getSemiBoldTextStyle(
                      color: ColorManager.black, fontSize: 12),
                ),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  "QTE: ${productOdored.quantity}  ${productOdored.productInfo != null ? productOdored.productInfo!.unite : ""}",
                  style: getSemiBoldTextStyle(
                      color: ColorManager.grey, fontSize: 12),
                ),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  "Total: ${separateur(productOdored.price)} FCFA",
                  style:
                      getBoldTextStyle(color: ColorManager.black, fontSize: 12),
                ),
                const SizedBox(
                  height: 5,
                ),
                if (productOdored.client != null)
                  Text(
                    "${productOdored.client!.firstname} ${productOdored.client!.laststname}",
                    style: getSemiBoldTextStyle(
                        color: ColorManager.black, fontSize: 12),
                  ),
                Text(
                  "Client",
                  style:
                      getLightTextStyle(fontSize: 10, color: ColorManager.grey),
                ),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  productOdored.paymentCode == codePaiementOnligne
                      ? "Mobile money"
                      : productOdored.paymentCode == codePaiementCash
                          ? "Cash"
                          : "Credit",
                  style:
                      getBoldTextStyle(fontSize: 12, color: ColorManager.jaune),
                ),
                Text(
                  "Methode paiement",
                  style:
                      getLightTextStyle(fontSize: 10, color: ColorManager.grey),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
