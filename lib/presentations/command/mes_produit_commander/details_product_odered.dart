import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/default_button.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../app/di.dart';
import '../../../app/functions.dart';
import '../../../data/repository/api/repository_impl.dart';
import '../../../data/request/request.dart';
import '../../../domain/model/mes_produits_commander_model.dart';
import '../../common/state/loader.dart';
import '../../ressources/styles_manager.dart';
import 'details_image.dart';
import 'details_product_info.dart';
import 'mes_product_odered_controller.dart';

class DetailsProductOdoredScreen extends StatefulWidget {
  const DetailsProductOdoredScreen({Key? key, required this.product})
      : super(key: key);

  final ProductCommanderItemModel product;
  @override
  State<DetailsProductOdoredScreen> createState() =>
      _DetailsProductOdoredScreenState();
}

class _DetailsProductOdoredScreenState
    extends State<DetailsProductOdoredScreen> {
  TextEditingController codeController = TextEditingController();

  MyProdcutOderedController myProdcutOderedController = Get.find();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool isload = true;
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  late ProductCommanderItemModel productShow;
  getClientDetails() async {
    await repositoryImpl
        .getInfoById(InfoByIdRequest(userId: productShow.userId))
        .then((response) {
      response.fold((failure) {}, (data) {
        if (mounted) {
          setState(() {
            productShow.client = data.user;
          });
        }
      });
    });
  }

  getDetailsProduct() async {
    setState(() {
      isload = true;
    });

    await repositoryImpl
        .getDetailProduct(productShow.idProduct)
        .then((response) async {
      await response.fold((failure) {}, (data) async {
        productShow.productInfo = (await data);
        if (mounted) {
          setState(() {
            productShow;
          });
        }
      });
    });
    setState(() {
      isload = false;
    });
  }

  @override
  void initState() {
    productShow = widget.product;
    getClientDetails();
    getDetailsProduct();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: isload == false
            ? Container(
                margin: const EdgeInsets.all(10),
                child: DefaultButton(
                  text: "Réduire le prix",
                  press: () {
                    newProduct(productShow);
                  },
                ),
              )
            : const SizedBox(),
        backgroundColor: ColorManager.backgroundColor,
        appBar: AppBar(
          leading: const BackButtonCustom(),
          title: const Text("Details de la commande"),
        ),
        // appBar: PreferredSize(
        //   preferredSize: Size.fromHeight(AppBar().preferredSize.height),
        //   child: DetailsDemandeCashAppBar(
        //     demandeData: productShow,
        //   ),
        // ),
        body: isload == false
            ? productShow.productInfo != null
                ? Hero(
                    tag: "demande",
                    child: ListView(
                      shrinkWrap: true,
                      children: [
                        DetailsProductOderedImages(
                          product: productShow,
                        ),
                        DetailsInfoProductOdered(
                          product: productShow,
                        )
                      ],
                    ),
                  )
                : const EmptyComponenent(
                    message: "Ce produit n'est plus disponible",
                  )
            : const LoaderComponent());
  }

  newProduct(ProductCommanderItemModel productOdored) {
    Get.bottomSheet(
      Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Entrer le nouveau prix",
              style: getBoldTextStyle(color: ColorManager.black, fontSize: 14),
            ),
            const SizedBox(
              height: 10,
            ),
            Form(
              key: _formKey,
              child: TextFormField(
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Entrer le prix';
                  } else if (verifPrice(
                        int.parse(value),
                      ) ==
                      false) {
                    return 'Prix incorrect';
                  } else {
                    return null;
                  }
                },
                controller: myProdcutOderedController.newPrice.value,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.grey.withOpacity(0.2),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide:
                        const BorderSide(width: 0, style: BorderStyle.none),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide:
                        const BorderSide(width: 0, style: BorderStyle.none),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide:
                        const BorderSide(width: 0, style: BorderStyle.none),
                  ),
                  hintText: "Entrer le prix",
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: 150,
              child: DefaultButton(
                text: "Valider",
                press: () async {
                  bool retour = false;

                  if (_formKey.currentState!.validate()) {
                    Get.back();
                    loaderDialog(context, text: "Veuillez patientez");
                    retour = await myProdcutOderedController
                        .reductPrice(productOdored);
                    if (retour == true) {
                      setState(() {
                        productShow.price = double.parse(
                            myProdcutOderedController.newPrice.value.text);
                      });
                    }
                    // Navigator.pop(context);
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
