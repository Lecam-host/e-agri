import 'dart:async';

import 'package:eagri/data/request/request.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/domain/usecase/advice_usecase.dart';
import 'package:rxdart/rxdart.dart';
import '../../data/repository/local/advice_categorie_repo.dart';

import '../base/base_view_model.dart';
import '../common/state_renderer/state_render_impl.dart';
import '../common/state_renderer/state_renderer.dart';

class ConseilViewModel extends BaseViewModel {
  StreamController listDataisEmpty = StreamController<bool>.broadcast();
  StreamController isLoadControler =
      StreamController<bool>.broadcast(sync: true);
  final listAdvice = BehaviorSubject<ListAdviceModel>();
  StreamController listAdviceCategory =
      StreamController<ListAdviceCategoryModel>.broadcast();

  final listAdviceSearch = BehaviorSubject<ListAdviceModel>();

  AdviceCategoriesRepo adviceCategoriesRepo = AdviceCategoriesRepo();

  AdviceUseCase adviceUseCase;
  ConseilViewModel(this.adviceUseCase);
  getListAdvice(int page) async {
    inputState.add(
        LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE));
    (await adviceUseCase.getListAdvice(GetListAdviceRequest(20, page))).fold(
        (failure) => {
              inputState.add(ErrorState(
                  StateRendererType.FULL_SCREEN_ERROR_STATE, failure.message))
            }, (data) {
      inputState.add(ContentState());
      listAdvice.add(data);
    });
  }

  askAdvice(AskAdviceRequest adviceRequest) async {
    inputState.add(
        LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE));
    (await adviceUseCase.askAdvice(adviceRequest)).fold(
        (failure) => {
              // left -> failure
              inputState.add(ErrorState(
                  StateRendererType.FULL_SCREEN_ERROR_STATE, failure.message))
            }, (data) {
      // right -> success (data)
      // listAdvice.add(data);
      inputState.add(ContentState());
    });
  }

  // getUserAsk() async {
  //   inputState.add(
  //       LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE));
  //   (await adviceUseCase.getUserAsk(1)).fold(
  //       (failure) => {
  //             // left -> failure
  //             inputState.add(ErrorState(
  //                 StateRendererType.FULL_SCREEN_ERROR_STATE, failure.message))
  //           }, (data) {
  //     // right -> success (data)
  //     // listAdvice.add(data);
  //     inputState.add(ContentState());
  //   });
  // }

//rechercher des conseils
  searchAdvice(SearchAdviceRequest request) async {
    inputState.add(
        LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE));
    (await adviceUseCase.searchAdvices(request)).fold(
        (failure) => {
              // left -> failure
              inputState.add(ErrorState(
                  StateRendererType.FULL_SCREEN_ERROR_STATE, failure.message))
            }, (data) {
      listAdviceSearch.add(data);
      listAdvice.add(data);
      inputState.add(ContentState());
    });
  }

  getListCategoryAdvice() async {
    // adviceCategoriesRepo
    //     .getAllItem()
    //     .then((value) => listAdviceCategory.add(value));
    inputState.add(
        LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE));
    (await adviceUseCase.getAdviceCategory()).fold(
        (failure) => {
              // left -> failure
              inputState.add(ErrorState(
                  StateRendererType.FULL_SCREEN_ERROR_STATE, failure.message))
            }, (data) {
      listAdviceCategory.add(data);
      // listAdviceCategory
      //     .add(data.listAdviceCategory as List<AdviceCategorieLocalResponse>);
      // adviceCategoriesRepo
      //     .getAllItem()
      //     .then((value) => listAdviceCategory.add(value));
      inputState.add(ContentState());
    });
  }

  Stream<ListAdviceModel> get outputIsListAdvice =>
      listAdvice.stream.map((advices) => advices);
  Stream<ListAdviceModel> get outputIsListAdviceSearch =>
      listAdviceSearch.stream.map((advices) => advices);
  Stream<ListAdviceCategoryModel> get outPutIsListCategoryAdvice =>
      listAdviceCategory.stream.map((categoryAdvices) => categoryAdvices);

  @override
  void start() async {
    inputState.add(ContentState());
  }

  @override
  void dispose() {
    listDataisEmpty.close();
    isLoadControler.close();
    super.dispose();
  }

  Stream<bool> get outPutisLoadControler =>
      isLoadControler.stream.map((event) => true);
}

abstract class ConseilViewModelOutput {
  Stream<bool> get outPutisLoadControler;
}
