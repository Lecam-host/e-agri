// ignore_for_file: use_build_context_synchronously

import 'dart:io';

import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/data/request/request.dart';
import 'package:eagri/data/request/request_object.dart';
import 'package:eagri/domain/model/audio_model.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/domain/usecase/advice_usecase.dart';
import 'package:eagri/presentations/common/audio/app_audio.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/state/loader.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

import '../../app/constant.dart';
import '../../app/di.dart';
import '../../data/repository/local/advice_categorie_repo.dart';
import '../../domain/model/user_model.dart';
import '../common/state/succes_page_view.dart';

import '../common/video/play_video.dart';
import '../ressources/assets_manager.dart';
import '../ressources/color_manager.dart';
import '../ressources/icon_manager.dart';
import '../ressources/strings_manager.dart';
import '../ressources/styles_manager.dart';
import 'add_advice_file_view.dart';

class AddAdviceView extends StatefulWidget {
  const AddAdviceView({Key? key}) : super(key: key);

  @override
  State<AddAdviceView> createState() => _AddAdviceViewState();
}

enum TypeConseil { conseil, formation }

TypeConseil typeConseil = TypeConseil.conseil;

class _AddAdviceViewState extends State<AddAdviceView> {
  AdviceUseCase adviceUseCase = instance<AdviceUseCase>();
  File? imageIllustrative;
  final ImagePicker imagePicker = ImagePicker();
  TextEditingController descriptionAdviceController = TextEditingController();
  TextEditingController titleAdviceController = TextEditingController();
  AdviceCategoriesRepo adviceCategoriesRepo = AdviceCategoriesRepo();
  final _formKeyTitre = GlobalKey<FormState>();
  final _formKeyImgIllistrative = GlobalKey<FormState>();
  List<AdviceCategoryModel> listAdviceCategory = [];

  List<DescriptionFile> listDescription = [];
  List<AdviceCategoryModel> advicesCategories = [];
  List<String> listTypeAdvice = ["Formation", "Conseil"];

  String adviceType = "Conseil";
  String categoriesId = "";
  String directoryPath = "";

  bool addFile = false;
  int titreStepNumber = 0;
  int imageIllustrativeStepNumber = 1;
  int fichierStepNumber = 2;
  int resumeStepNumber = 3;

  List<Map<String, dynamic>> listFiles = [];

  getListAdviceCategory() {
    adviceUseCase.getAdviceCategory().then((value) {
      value.fold((l) => null, (response) {
        setState(() {
          advicesCategories = response.listAdviceCategory;
        });
      });
    });
  }

  tapped(int step) {
    setState(() => currentStep = step);
  }

  continued() {
    verifForm();
  }

  cancel() {
    currentStep > 0 ? setState(() => currentStep -= 1) : null;
  }

  @override
  void initState() {
    super.initState();
    getListAdviceCategory();
  }

  List<Step> listStep() => [
        titreStep(),
        imageIllustrativeStep(),
        fichierStep(),
        //  resumeStep(),
      ];
  int currentStep = 0;
  StepperType stepperType = StepperType.horizontal;
  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
        builder: (context, user, child) => Scaffold(
              appBar: AppBar(
                title: const Text('Ajouter un conseil'),
                leading: const BackButtonCustom(),
              ),
              body: Column(
                children: [
                  Expanded(
                    child: Stepper(
                      elevation: 1,
                      type: stepperType,
                      controlsBuilder: (context, controlsDetails) {
                        final isLastStep = currentStep == listStep().length - 1;
                        return Row(
                          children: [
                            Expanded(
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  elevation: 0,
                                  backgroundColor: Colors.grey,
                                ),
                                onPressed: controlsDetails.onStepCancel,
                                child: const Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Icon(IconManager.arrowLef),
                                    Text(AppStrings.previous),
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  elevation: 0,
                                  backgroundColor: ColorManager.primary,
                                ),
                                onPressed: isLastStep == true
                                    ? () {
                                        loaderDialog(context,
                                            text: "Veuillez patienter");
                                        AddAdviceRequest addAdviceRequest =
                                            AddAdviceRequest(
                                          adviceType: adviceType,
                                          categoryId: categoriesId,
                                          title: titleAdviceController.text,
                                          userIdForCreation: user.id!,
                                          description:
                                              descriptionAdviceController.text,
                                        );
                                        if (imageIllustrative != null) {
                                          addAdviceRequest.illustrationImage =
                                              imageIllustrative!;
                                        }

                                        List<DescriptionFile> descFile = [];
                                        if (listFiles.isNotEmpty) {
                                          for (var element in listFiles) {
                                            descFile.add(element["info"]);
                                          }

                                          addAdviceRequest.descriptionFiles =
                                              descFile;

                                          addAdviceRequest.file1 =
                                              listFiles[0]["file"];
                                          if (listFiles.length > 1) {
                                            addAdviceRequest.file2 =
                                                listFiles[1]["file"];
                                          }
                                          if (listFiles.length > 2) {
                                            addAdviceRequest.file3 =
                                                listFiles[2]["file"];
                                          }
                                        }

                                        adviceUseCase
                                            .addAdvice(addAdviceRequest)
                                            .then((value) {
                                          value.fold((l) {}, (r) {
                                            Navigator.pop(context);
                                            Navigator.pushReplacement(
                                              context,
                                              PageTransition(
                                                type: PageTransitionType
                                                    .rightToLeft,
                                                child: const SuccessPageView(
                                                    message:
                                                        "Votre conseil a été rémonté avec succes"),
                                                isIos: true,
                                                duration: const Duration(
                                                    milliseconds: 400),
                                              ),
                                            );
                                          });
                                        });
                                        // addAdviceRequest!.file1 = listFiles[0].files.;
                                      }
                                    : controlsDetails.onStepContinue,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(isLastStep == true
                                        ? AppStrings.submit
                                        : AppStrings.next),
                                    const Icon(IconManager.arrowRight)
                                  ],
                                ),
                              ),
                            ),
                          ],
                        );
                      },
                      physics: const ScrollPhysics(),
                      currentStep: currentStep,
                      onStepTapped: (step) => tapped(step),
                      onStepContinue: continued,
                      onStepCancel: cancel,
                      steps: listStep(),
                    ),
                  ),
                ],
              ),
            ));
  }

  showCustomFlushbar() {
    return Flushbar(
      maxWidth: 200,
      flushbarPosition: FlushbarPosition.BOTTOM,
      duration: const Duration(seconds: 5),
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.all(10),
      borderRadius: BorderRadius.circular(15),
      backgroundColor: Colors.black,
      icon: Icon(Icons.info, color: ColorManager.white),
      boxShadows: const [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],
      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      // title: '',
      message: "Remplir tout les champs",
    ).show(context);
  }

  Step titreStep() {
    return Step(
        isActive: currentStep >= titreStepNumber,
        state: currentStep >= titreStepNumber
            ? StepState.complete
            : StepState.disabled,
        content: Form(
          key: _formKeyTitre,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              const Text("Que voulez-vous ajouter ?"),
              Container(
                decoration: BoxDecoration(
                  color: ColorManager.white,
                ),
                child: Column(children: [
                  ListTile(
                    title: const Text('Conseil'),
                    leading: Radio<TypeConseil>(
                      activeColor: ColorManager.primary,
                      value: TypeConseil.conseil,
                      groupValue: typeConseil,
                      onChanged: (TypeConseil? value) {
                        setState(() {
                          typeConseil = value!;
                          adviceType = "Conseil";
                        });
                      },
                    ),
                  ),
                  ListTile(
                    title: const Text('Formation'),
                    leading: Radio<TypeConseil>(
                      activeColor: ColorManager.primary,
                      value: TypeConseil.formation,
                      groupValue: typeConseil,
                      onChanged: (TypeConseil? value) {
                        setState(() {
                          typeConseil = value!;
                          adviceType = "Formation";
                        });
                      },
                    ),
                  ),
                ]),
              ),
              const SizedBox(
                height: 40,
              ),
              const Text('Titre du conseil *'),
              TextFormField(
                //   maxLines: null,
                controller: titleAdviceController,
                decoration: InputDecoration(
                  filled: true,
                  hintText: "Entrer le titre",
                  fillColor: ColorManager.formFieldBackgroundColor,
                  // labelText: "Titre",
                ),
              ),
              const SizedBox(
                height: 40,
              ),
              const Text('Catégories du conseil *'),
              Container(
                decoration: containerBoxDecoration(),
                child: MultiSelectDialogField(
                  searchHint: "Réchercher",
                  searchable: true,
                  buttonIcon: const Icon(Icons.keyboard_arrow_down),
                  buttonText: Text(
                    "Catégorie du conseil",
                    style: getRegularTextStyle(
                        color: ColorManager.grey, fontSize: 12),
                  ),
                  confirmText: const Text(AppStrings.ok),
                  cancelText: const Text(AppStrings.cancel),
                  title: Text(
                    "Choisir la catégorie ",
                    style: getRegularTextStyle(
                        color: ColorManager.black, fontSize: 12),
                  ),
                  items: advicesCategories
                      .map((e) => MultiSelectItem(e, e.label))
                      .toList(),
                  listType: MultiSelectListType.LIST,
                  onConfirm: (List<AdviceCategoryModel> values) {
                    categoriesId = values[0].id.toString();
                  },
                  selectedColor: ColorManager.primary,
                ),
              ),
              const SizedBox(
                height: 40,
              ),
              const Text('Description du conseil *'),
              TextFormField(
                //   maxLines: null,
                controller: descriptionAdviceController,
                decoration: InputDecoration(
                  filled: true,
                  hintText: AppStrings.adviceDescriptionQuestion,
                  fillColor: ColorManager.formFieldBackgroundColor,
                  // labelText: AppStrings.adviceDescription,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
        title: const Text(""));
  }

  Step imageIllustrativeStep() {
    return Step(
      isActive: currentStep >= imageIllustrativeStepNumber,
      state: currentStep >= imageIllustrativeStepNumber
          ? StepState.complete
          : StepState.disabled,
      content: Form(
        key: _formKeyImgIllistrative,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Image illustrative",
              style: getBoldTextStyle(color: ColorManager.black, fontSize: 14),
            ),
            const SizedBox(
              height: 10,
            ),
            if (imageIllustrative == null)
              Text(
                "Ajouter une image illustrative du conseil",
                style: getRegularTextStyle(
                    color: ColorManager.black, fontSize: 14),
              ),
            if (imageIllustrative == null)
              GestureDetector(
                onTap: () {
                  pictureDialod();
                },
                child: Card(
                  margin: const EdgeInsets.all(10),
                  child: Container(
                    padding: const EdgeInsets.all(10),
                    child: Column(children: [
                      SvgPicture.asset(
                        SvgManager.media,
                        width: 50,
                      ),
                      const Text("Ajouter une photo"),
                    ]),
                  ),
                ),
              ),
            if (imageIllustrative != null)
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  TextButton(
                      onPressed: () {
                        pictureDialod();
                      },
                      child: Text(
                        "Modifier l'image",
                        style: getBoldTextStyle(color: Colors.blue),
                      )),
                  Image.file(
                    File(imageIllustrative!.path),
                    fit: BoxFit.cover,
                    width: 250,
                  ),
                ],
              ),
          ],
        ),
      ),
      title: const Text(''),
    );
  }

  Step fichierStep() {
    return Step(
      isActive: currentStep >= fichierStepNumber,
      state: currentStep >= fichierStepNumber
          ? StepState.complete
          : StepState.disabled,
      content: Column(
        children: [
          if (listFiles.isNotEmpty) renderListFile(),
          const SizedBox(
            height: 40,
          ),
          ElevatedButton.icon(
            style: ElevatedButton.styleFrom(backgroundColor: Colors.black),
            onPressed: () {
              Navigator.push(
                context,
                PageTransition(
                  type: PageTransitionType.rightToLeft,
                  child: const AddFileAdviceView(),
                  isIos: true,
                  duration:
                      const Duration(milliseconds: Constant.navigatorDuration),
                ),
              ).then((value) {
                if (value != null) {
                  setState(() {
                    listFiles.add(value);
                  });
                }
              });
            },
            icon: const Icon(Icons.file_download),
            label: const Text("Ajouter un fichier"),
          ),
          const SizedBox(
            height: 20,
          ),
        ],
      ),
      title: const Text(''),
    );
  }

  Step resumeStep() {
    return Step(
      isActive: currentStep >= resumeStepNumber,
      state: currentStep >= resumeStepNumber
          ? StepState.complete
          : StepState.disabled,
      content: const Column(
        mainAxisSize: MainAxisSize.min,
        children: [Text('ee')],
      ),
      title: const Text(''),
    );
  }

  pictureDialod() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text(""),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text(
                'Annuler',
                style: getBoldTextStyle(color: Colors.black),
              ),
            ),
          ],
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextButton(
                onPressed: () {
                  selectImagesInGalery();
                },
                child: Text(
                  "Gallerie",
                  style: getBoldTextStyle(color: Colors.blue),
                ),
              ),
              TextButton(
                onPressed: () {
                  takePicture();
                },
                child: Text(
                  "Camera",
                  style: getBoldTextStyle(color: Colors.blue),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void selectImagesInGalery() async {
    XFile? selectedImages =
        await imagePicker.pickImage(source: ImageSource.gallery);
    if (selectedImages != null) {
      setState(() {
        imageIllustrative = File(selectedImages.path);
      });
      Navigator.pop(context);
    }
  }

  void takePicture() async {
    XFile? selectedImages =
        await imagePicker.pickImage(source: ImageSource.camera);
    if (selectedImages != null) {
      setState(() {
        imageIllustrative = File(selectedImages.path);
      });
      Navigator.pop(context);
    }
  }

  renderListFile() {
    return Container(
      color: ColorManager.grey.withOpacity(0.2),
      child: ListView.builder(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: listFiles.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            margin: const EdgeInsets.only(
              bottom: 10,
            ),
            color: ColorManager.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 10,
                ),
                RichText(
                  textAlign: TextAlign.end,
                  textDirection: TextDirection.rtl,
                  softWrap: true,
                  textScaleFactor: 1,
                  text: TextSpan(
                    text: 'Titre :',
                    style: DefaultTextStyle.of(context).style,
                    children: <TextSpan>[
                      TextSpan(
                          text: " ${listFiles[index]["info"].title}",
                          style: const TextStyle(fontWeight: FontWeight.bold)),
                    ],
                  ),
                ),
                RichText(
                  textAlign: TextAlign.end,
                  textDirection: TextDirection.rtl,
                  softWrap: true,
                  textScaleFactor: 1,
                  text: TextSpan(
                    text: 'Description :',
                    style: DefaultTextStyle.of(context).style,
                    children: <TextSpan>[
                      TextSpan(
                          text: " ${listFiles[index]["info"].description}",
                          style: const TextStyle(fontWeight: FontWeight.bold)),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                if (Constant.imagesExtension
                    .contains(listFiles[index]["extension"])) ...[
                  Container(
                    margin: const EdgeInsets.only(bottom: 10),
                    decoration: const BoxDecoration(color: Colors.white),
                    child: Image.file(
                      File(listFiles[index]["file"].path),
                      fit: BoxFit.contain,
                    ),
                  ),
                ],
                if (Constant.videoExtension
                    .contains(listFiles[index]["extension"])) ...[
                  AppVideoPlay(
                      isNetworkVideo: false,
                      link: '',
                      file: listFiles[index]["file"])
                ],
                if (Constant.audioExtension
                    .contains(listFiles[index]["extension"])) ...[
                  AppAudio(
                    audio: AudioModel(link: listFiles[index]["file"].path),
                  )
                ],
              ],
            ),
          );
        },
      ),
    );
  }

  verifForm() {
    if (currentStep == titreStepNumber &&
        (titleAdviceController.text == "" ||
            adviceType == "" ||
            categoriesId == "" ||
            descriptionAdviceController.text == "")) {
      showCustomFlushbar();
    } else {
      currentStep < listStep().length - 1
          ? setState(() => currentStep += 1)
          : null;
    }
  }
}
