import 'package:eagri/data/request/request.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

import '../../app/di.dart';
import '../../app/functions.dart';
import '../../domain/model/model.dart';
import '../../domain/usecase/advice_usecase.dart';
import '../common/state/empty_component.dart';
import '../ressources/color_manager.dart';
import '../ressources/styles_manager.dart';
import 'controller/answer_ask_controller.dart';

class ListAskAdviceAnswerView extends StatefulWidget {
  const ListAskAdviceAnswerView({Key? key, this.statutAsk}) : super(key: key);
  final int? statutAsk; //if statutAsk== null get AllAdive

  @override
  State<ListAskAdviceAnswerView> createState() =>
      _ListAskAdviceAnswerViewState();
}

class _ListAskAdviceAnswerViewState extends State<ListAskAdviceAnswerView> {
  List<AskAdviceModel> listAsk = [];
  bool isError = false;
  int statutCode = -2;
  bool isLoad = true;
  AdviceUseCase adviceUseCase = instance<AdviceUseCase>();
  AnswerAskConntroller answerAskConntroller = AnswerAskConntroller();

  @override
  void initState() {
    // getListAsk();
    answerAskConntroller
        .getUserAsk(GetUserAskAdviceRequest(statutAsk: widget.statutAsk));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => answerAskConntroller.isLoad.value
        ? const LoaderComponent()
        : Obx(() =>
            answerAskConntroller.listAskAdvice.value.listAskAdvice!.isNotEmpty
                ? ListView.builder(
                    itemCount: answerAskConntroller
                        .listAskAdvice.value.listAskAdvice!.length,
                    itemBuilder: (BuildContext context, int index) {
                      return AnimationConfiguration.staggeredList(
                        position: index,
                        duration: const Duration(milliseconds: 100),
                        child: SlideAnimation(
                          verticalOffset: 50.0,
                          child: FadeInAnimation(
                            child: askWidget(answerAskConntroller
                                .listAskAdvice.value.listAskAdvice![index]),
                          ),
                        ),
                      );
                    },
                  )
                : const EmptyComponenent(
                    message: "Votre liste de demande conseil est vide",
                  )));
    // return isLoad == true
    //     ?
    //     : listAsk.isNotEmpty

    //         : const EmptyComponenent(
    //             message: "Votre liste de demande conseil est vide",
    //           );
  }

  Widget askWidget(AskAdviceModel ask) {
    return InkWell(
      // onTap: () {
      //   Navigator.push(
      //     context,
      //     PageTransition(
      //       type: PageTransitionType.bottomToTop,
      //       child: AnswerAskDemandeView(
      //         askId: ask.id,
      //       ),
      //       isIos: true,
      //       duration: const Duration(milliseconds: 400),
      //     ),
      //   );
      // },
      child: Card(
        elevation: 1,
        margin: const EdgeInsets.all(10),
        child: Container(
          margin: const EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                ask.description,
                style:
                    getMeduimTextStyle(color: ColorManager.black, fontSize: 15),
              ),
              Row(
                children: [
                  Text(
                    convertDate(ask.creationDate),
                    style: getLightTextStyle(
                        color: ColorManager.grey, fontSize: 12),
                  ),
                  const Spacer(),
                  Container(
                    padding: const EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        color: ask.isResolve == 0
                            ? ColorManager.blue.withOpacity(0.1)
                            : ColorManager.primary.withOpacity(0.1),
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      ask.isResolve == 0 ? "En attente" : "Traité",
                      style: getMeduimTextStyle(
                          color: ask.isResolve == 0
                              ? ColorManager.blue
                              : ColorManager.primary,
                          fontSize: 12),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
