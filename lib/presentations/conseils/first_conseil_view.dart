import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/state/loader.dart';
import 'package:eagri/presentations/conseils/conseil_view_model.dart';
import 'package:eagri/presentations/ressources/strings_manager.dart';
import 'package:eagri/presentations/ressources/values_manager.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shimmer/shimmer.dart';

import '../../app/di.dart';
import '../../data/repository/api/repository_impl.dart';
import '../../data/request/request.dart';
import '../../domain/model/model.dart';

import '../../domain/usecase/advice_usecase.dart';
import '../ressources/color_manager.dart';
import '../ressources/styles_manager.dart';
import 'add_advice_view.dart';
import 'components/app_bar_actions.dart';
import 'components/categorie_boutton.dart';

import 'components/list_last_advice.dart';
import 'demande_conseil_view.dart';
import 'list_mes_demanndes_view.dart';

class FirstConseilView extends StatefulWidget {
  const FirstConseilView({Key? key}) : super(key: key);

  @override
  State<FirstConseilView> createState() => _FirstConseilViewState();
}

class _FirstConseilViewState extends State<FirstConseilView> {
  ConseilViewModel viewModel = instance<ConseilViewModel>();
  ListAdviceCategoryModel? data;
  ListAdviceCategoryModel? listAdviceCategoryData;
  RepositoryImpl apiRepo = instance<RepositoryImpl>();
  AdviceUseCase adviceUseCase = instance<AdviceUseCase>();

  List<AdviceModel> listLastAdvice = [];
  getListLastAdvice() async {
    WidgetsBinding.instance.addPostFrameCallback(
        (_) => loaderDialog(context, text: "Veuillez patienter ..."));
    await getListAdvice().then((value) => Navigator.pop(context));
  }

  Future<void> getListAdvice() async {
    await adviceUseCase
        .getListAdvice(GetListAdviceRequest(4, 0))
        .then((response) {
      response.fold((l) {}, (listAdviceResponse) {
        setState(() {
          listLastAdvice = listAdviceResponse.listAdvice;
        });
      });
    });
  }

  Future<void> _refresh() async {
    await getListAdvice();
    await viewModel.getListCategoryAdvice();
    // await viewModel.getListAdvice();
  }

  
  // getListAdvice() async {
  //   print('-----lastttlasll------');
  //   await apiRepo.getAllAdviceCategory().then((value) {
  //     print('-----------------');

  //     value.all((a) {
  //       print('--sqsqdsdsd---------');
  //       setState(() {
  //         listAdviceCategoryData = a;
  //       });

  //       return true;
  //     });
  //   });
  // }

  getStart() async {
    await getListLastAdvice();
    await viewModel.getListCategoryAdvice();

    // viewModel.outPutIsListCategoryAdvice.listen((event) {
    //   SchedulerBinding.instance.addPostFrameCallback((_) {
    //     if (mounted) {
    //       setState(() {
    //         listAdviceCategoryData = event;
    //       });
    //     }
    //   });
    // });
  }

  @override
  void initState() {
    getStart();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButtonCustom(),
        title: const Text(AppStrings.conseils),
        actions: actions(context),
      ),
      body: RefreshIndicator(
        onRefresh: () => _refresh(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              listAction(),
              const SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  AppStrings.categoryConseil,
                  style: getBoldTextStyle(
                    color: ColorManager.black,
                    fontSize: 15,
                  ),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              listAdviceCategoryContentWidget(),
              const SizedBox(
                height: 30,
              ),
              ListLastAdvice(
                list: listLastAdvice.isNotEmpty ? listLastAdvice : null,
              ),
             // const Divider(),
              const SizedBox(
                height: 10,
              ),
              // GestureDetector(
              //   onTap: () {
              //     // Navigator.push(
              //     //   context,
              //     //   PageTransition(
              //     //     type: PageTransitionType.rightToLeft,
              //     //     child: widget.child,
              //     //     isIos: true,
              //     //     duration: const Duration(milliseconds: 400),
              //     //   ),
              //     // );
              //   },
              //   child: Container(
              //       padding: const EdgeInsets.all(AppPadding.p5),
              //       height: AppSize.s150,
              //       width: double.infinity,
              //       decoration: BoxDecoration(
              //         color: ColorManager.lightGrey,
              //         borderRadius: BorderRadius.circular(AppSize.s5),
              //       ),
              //       child: const AbonnementCardComponent()),
              // ),
            ],
          ),
        ),
      ),
    );
  }

  Widget listAction() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: [
          const SizedBox(
            width: 5,
          ),
          actionCard(
              "Ajouter un conseil",
              Icons.add_comment,
              ColorManager.primary2.withOpacity(0.8),
              const Color.fromARGB(255, 32, 80, 33),
              const AddAdviceView()),
          const SizedBox(
            width: 5,
          ),
          actionCard(
              "Demande de conseil",
              Icons.question_answer,
              ColorManager.primary3.withOpacity(0.8),
              const Color.fromARGB(255, 153, 124, 29),
              const DemandeConveilView()),
          const SizedBox(
            width: 5,
          ),
          actionCard(
            "Voir mes demandes",
            Icons.list,
            ColorManager.primary.withOpacity(0.8),
            const Color.fromARGB(255, 33, 104, 35),
            const ListMesDemandes(),
          ),
          const SizedBox(
            width: 5,
          ),
          // actionCard(
          //     "Demande de conseil",
          //     Icons.request_page,
          //     Color.fromARGB(255, 150, 72, 0),
          //     Color.fromARGB(255, 223, 144, 54)),
          const SizedBox(
            width: 5,
          ),
        ],
      ),
    );
  }

  Widget actionCard(
    String titre,
    IconData icon,
    Color color,
    Color iconColor,
    Widget nextPage,
  ) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          PageTransition(
            type: PageTransitionType.leftToRight,
            child: nextPage,
            duration: const Duration(milliseconds: 200),
          ),
        );
      },
      child: Container(
        padding: const EdgeInsets.only(
          left: 10,
          right: 10,
        ),
        decoration:
            BoxDecoration(color: color, borderRadius: BorderRadius.circular(5)),
        height: 55,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              width: 70,
              child: Text(
                titre,
                style: getBoldTextStyle(color: ColorManager.white),
              ),
            ),
            const SizedBox(
              width: 5,
            ),
            Icon(
              icon,
              color: iconColor,
            )
          ],
        ),
      ),
    );
  }

  Wrap listShimmer() {
    return Wrap(
      children: [
        shimmerCustom(65),
        shimmerCustom(75),
        shimmerCustom(70),
        shimmerCustom(80),
        shimmerCustom(76),
        shimmerCustom(67),
        shimmerCustom(80),
        shimmerCustom(78),
      ],
    );
  }

  Shimmer shimmerCustom(double width) {
    return Shimmer.fromColors(
      baseColor: const Color.fromARGB(255, 115, 115, 115),
      highlightColor: const Color.fromARGB(255, 181, 181, 181),
      child: Container(
        margin: const EdgeInsets.only(bottom: 10, left: 10),
        width: width,
        height: 25.0,
        decoration: BoxDecoration(
          color: ColorManager.white,
          borderRadius: BorderRadius.circular(15),
        ),
      ),
    );
  }

  Widget formatCard(String image, String titre, Widget nextWidget) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          PageTransition(
            type: PageTransitionType.bottomToTop,
            child: nextWidget,
            isIos: true,
            duration: const Duration(milliseconds: 400),
          ),
        );
      },
      child: Container(
        padding: const EdgeInsets.all(AppPadding.p2),
        height: AppSize.s100,
        width: MediaQuery.of(context).size.width / 2.2,
        decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.circular(15),
          image: DecorationImage(
            onError: (Object exception, StackTrace? stackTrace) {},
            colorFilter: ColorFilter.mode(
              Colors.black.withOpacity(0.35),
              BlendMode.multiply,
            ),
            image: AssetImage(image),
            fit: BoxFit.cover,
          ),
        ),
        child: Align(
          alignment: Alignment.bottomLeft,
          child: Container(
            padding: const EdgeInsets.only(
              left: AppMargin.m10,
              right: AppMargin.m10,
              top: AppMargin.m5,
              bottom: AppMargin.m5,
            ),
            margin: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.4),
              borderRadius: BorderRadius.circular(15),
            ),
            //width: double.infinity / 2,
            child: Text(
              titre,
              style: const TextStyle(
                color: Color.fromARGB(255, 255, 255, 255),
                fontSize: 13,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget listAdviceCategoryContentWidget() {
    return StreamBuilder<ListAdviceCategoryModel>(
        stream: viewModel.outPutIsListCategoryAdvice,
        builder: (context, listCategory) {
          return listCategory.data == null
              ? listShimmer()
              : Wrap(
                  children: [
                    CategorieButton(
                      advice: AdviceCategoryModel(0, 0, "", "Tout"),
                    ),
                    for (AdviceCategoryModel category
                        in listCategory.data!.listAdviceCategory) ...[
                      CategorieButton(
                        advice: category,
                      ),
                    ],
                  ],
                );
        });
  }

  Container categoryButton() {
    return Container(
      margin: const EdgeInsets.only(
        left: 5,
        bottom: 5,
      ),
      padding: const EdgeInsets.all(5),
      decoration: BoxDecoration(
          color: ColorManager.lightGrey,
          borderRadius: const BorderRadius.all(Radius.circular(10))),
      child: Text(
        'Formation',
        style: getRegularTextStyle(color: ColorManager.black),
      ),
    );
  }
}
