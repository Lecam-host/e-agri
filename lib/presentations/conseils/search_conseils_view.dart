import 'package:eagri/data/request/request.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/presentations/conseils/after_search_view.dart';
import 'package:eagri/presentations/conseils/conseil_view_model.dart';
import 'package:eagri/presentations/ressources/font_manager.dart';
import 'package:eagri/presentations/ressources/icon_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:eagri/presentations/ressources/values_manager.dart';
import 'package:flutter/material.dart';
import 'package:multi_select_flutter/dialog/multi_select_dialog_field.dart';
import 'package:multi_select_flutter/util/multi_select_item.dart';
import 'package:multi_select_flutter/util/multi_select_list_type.dart';
import 'package:page_transition/page_transition.dart';

import '../../app/di.dart';
import '../../data/repository/api/repository_impl.dart';
import '../common/buttons/back_button.dart';
import '../ressources/color_manager.dart';
import '../ressources/strings_manager.dart';

class SearchConseilView extends StatefulWidget {
  const SearchConseilView({Key? key}) : super(key: key);

  @override
  State<SearchConseilView> createState() => _SearchConseilViewState();
}

class _SearchConseilViewState extends State<SearchConseilView> {
  SearchAdviceRequest searchAdviceRequest = SearchAdviceRequest();
  ConseilViewModel viewModel = instance<ConseilViewModel>();
  List<AdviceModel> list = [];
  TextEditingController searchInputController = TextEditingController();
  String? adviceTypeOutput;

  static List<String> adviceTypes = [
    "Formation",
    "Conseil",
  ];

  static List<SelectObjectModel> listTypeMedia = [
    SelectObjectModel(id: 1, name: "Audio"),
    SelectObjectModel(id: 2, name: "Fichier"),
    SelectObjectModel(id: 3, name: "Video"),
  ];
  static List<SelectObjectModel> categoryAdviceList = [];

  List<AdviceCategoryModel> listAdviceCategory = [
    AdviceCategoryModel(1, 1, "", "Agricole"),
    AdviceCategoryModel(2, 2, "", "Elevage"),
    AdviceCategoryModel(3, 3, "", "Assurance"),
    AdviceCategoryModel(4, 4, "", "Autre")
  ];
  List<int> listAdviceCategoryChoisi = [];
  List<int> listAdviceTypeChoisi = [];
  List<String> listAdviceTypeMediChoisi = [];

  @override
  void initState() {
    getListCategoryAdvice();
    super.initState();
  }

  ListAdviceCategoryModel? listAdviceCategoryData;
  RepositoryImpl apiRepo = instance<RepositoryImpl>();

  getListCategoryAdvice() async {
    await apiRepo.getAllAdviceCategory().then((value) {
      value.all((list) {
        categoryAdviceList = [];
        for (var element in list.listAdviceCategory) {
          categoryAdviceList
              .add(SelectObjectModel(id: element.id, name: element.label));
        }
        setState(() {
          categoryAdviceList;
        });

        return true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButtonCustom(),
        title: Text(
          AppStrings.searchConseil,
          style: getRegularTextStyle(
            color: ColorManager.black,
            fontSize: FontSize.s14,
          ),
        ),
      ),
      body: Container(
        margin:
            const EdgeInsets.only(left: AppMargin.m10, right: AppMargin.m10),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextFormField(
                keyboardType: TextInputType.visiblePassword,
                controller: searchInputController,
                decoration: InputDecoration(
                  prefixIcon: const Icon(IconManager.search),
                  filled: true,
                  fillColor: ColorManager.grey.withOpacity(0.1),
                  hintText: AppStrings.enterKeyWord,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                color: ColorManager.white,
                child: DropdownButton(
                  dropdownColor: ColorManager.white,
                  isExpanded: true,
                  hint: const Text(AppStrings.adviceTypeQuestion),
                  alignment: AlignmentDirectional.center,
                  value: adviceTypeOutput,
                  icon: const Icon(Icons.keyboard_arrow_down),
                  items: adviceTypes.map((String items) {
                    return DropdownMenuItem(
                      value: items,
                      child: Text(items),
                    );
                  }).toList(),
                  onChanged: (String? newValue) {
                    setState(() {
                      adviceTypeOutput = newValue!;
                    });
                  },
                ),
              ),
              // const SizedBox(
              //   height: 20,
              // ),
              // Container(
              //   color: ColorManager.white,
              //   child: MultiSelectDialogField(
              //     buttonIcon: const Icon(Icons.keyboard_arrow_down),
              //     isDismissible: false,
              //     confirmText: const Text(AppStrings.ok),
              //     cancelText: const Text(AppStrings.cancel),
              //     title: const Text(AppStrings.adviceTypeQuestion),
              //     buttonText: const Text(AppStrings.adviceTypeQuestion),
              //     items: adviceType
              //         .map((e) => MultiSelectItem(e, e.name!))
              //         .toList(),
              //     listType: MultiSelectListType.LIST,
              //     onConfirm: (List<SelectObjectModel> values) {
              //       for (var element in values) {
              //         listAdviceTypeChoisi.add(element.id!);
              //       }
              //       setState(() {
              //         listAdviceTypeChoisi;
              //       });
              //     },
              //     selectedColor: ColorManager.primary,
              //   ),
              // ),
              const SizedBox(
                height: 20,
              ),
              Container(
                color: ColorManager.white,
                child: MultiSelectDialogField(
                  buttonIcon: const Icon(Icons.keyboard_arrow_down),
                  isDismissible: false,
                  confirmText: const Text(AppStrings.ok),
                  cancelText: const Text(AppStrings.cancel),
                  title: const Text(AppStrings.categoryAdviceQuestion),
                  buttonText: const Text(AppStrings.categoryAdviceQuestion),
                  items: categoryAdviceList
                      .map((e) => MultiSelectItem(e, e.name!))
                      .toList(),
                  listType: MultiSelectListType.LIST,
                  onConfirm: (List<SelectObjectModel> values) {
                    for (var element in values) {
                      listAdviceCategoryChoisi.add(element.id!);
                    }
                    setState(() {
                      listAdviceCategoryChoisi;
                    });
                  },
                  selectedColor: ColorManager.primary,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                color: ColorManager.white,
                child: MultiSelectDialogField(
                  buttonIcon: const Icon(Icons.keyboard_arrow_down),
                  isDismissible: false,
                  confirmText: const Text(AppStrings.ok),
                  cancelText: const Text(AppStrings.cancel),
                  title: const Text(AppStrings.typeDeMedia),
                  buttonText: const Text(AppStrings.typeDeMedia),
                  items: listTypeMedia
                      .map((e) => MultiSelectItem(e, e.name!))
                      .toList(),
                  listType: MultiSelectListType.LIST,
                  onConfirm: (List<SelectObjectModel> values) {
                    for (var element in values) {
                      listAdviceTypeMediChoisi.add(element.name!);
                    }
                    setState(() {
                      listAdviceTypeMediChoisi;
                    });
                  },
                  selectedColor: ColorManager.primary,
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Center(
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      elevation: 0,
                      backgroundColor: ColorManager.black,
                      textStyle: getRegularTextStyle(
                        color: ColorManager.white,
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(
                          AppSize.s12,
                        ),
                      ),
                    ),
                    child: const Text(AppStrings.submit),
                    onPressed: () {
                      if (adviceTypeOutput != null) {
                        searchAdviceRequest.adviceType = adviceTypeOutput;
                      }
                      if (listAdviceCategoryChoisi.isNotEmpty) {
                        searchAdviceRequest.categoriesId =
                            listAdviceCategoryChoisi;
                      }
                      if (listAdviceTypeMediChoisi.isNotEmpty) {
                        searchAdviceRequest.mediaType =
                            listAdviceTypeMediChoisi;
                      }
                      if (searchInputController.text != "") {
                        searchAdviceRequest.keyword =
                            searchInputController.text;
                      }

                      searchAdviceRequest.userId = 1;

                      Navigator.push(
                        context,
                        PageTransition(
                          type: PageTransitionType.rightToLeft,
                          child: AfterSearchView(
                            searchAdviceRequest: searchAdviceRequest,
                          ),
                          isIos: true,
                          duration: const Duration(milliseconds: 400),
                        ),
                      );
                    }),
              ),
              // const Divider(),
              // const SizedBox(
              //   height: 10,
              // ),
              // Text(
              //   AppStrings.recentSearch,
              //   style: getBoldTextStyle(
              //       color: ColorManager.black, fontSize: FontSize.s16),
              // ),
              // recentSearchListTile("Mettre de l'engrais"),
              // recentSearchListTile('Comment arroser vos palntes'),
              // recentSearchListTile('Comment arroser vos palntes')
            ],
          ),
        ),
      ),
    );
  }

  ListTile recentSearchListTile(String titre) {
    return ListTile(
      title: Text(
        titre,
        style: getSemiBoldTextStyle(color: ColorManager.black),
      ),
      trailing: const Icon(IconManager.arrowRight),
    );
  }
}
