import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/strings_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';

import 'list_ask_advice_answer.dart';

class ListMesDemandes extends StatefulWidget {
  const ListMesDemandes({Key? key}) : super(key: key);

  @override
  State<ListMesDemandes> createState() => _ListMesDemandesState();
}

class _ListMesDemandesState extends State<ListMesDemandes> {
  listAskIsEmpty() {}
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          leading: const BackButtonCustom(),
          title: const Row(
            children: [
              Text("Mes demandes"),
            ],
          ),
          bottom: TabBar(
            indicatorColor: ColorManager.primary,
            tabs: [
              Tab(
                child: Text(
                  AppStrings.all,
                  style: getMeduimTextStyle(color: ColorManager.black),
                ),
              ),
              Tab(
                child: Text(
                  AppStrings.treated,
                  style: getMeduimTextStyle(color: ColorManager.black),
                ),
              ),
              Tab(
                child: Text(
                  AppStrings.pending,
                  style: getMeduimTextStyle(color: ColorManager.black),
                ),
              ),
            ],
          ),
        ),
        body: const TabBarView(
          children: [
            ListAskAdviceAnswerView(),
            ListAskAdviceAnswerView(
              statutAsk: 1,
            ),
            ListAskAdviceAnswerView(
              statutAsk: 0,
            ),
          ],
        ),
      ),
    );
  }
}
