import 'package:eagri/domain/model/model.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/conseils/conseil_view_model.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/strings_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';

import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import '../../app/di.dart';
import '../../data/request/request.dart';
import '../../domain/usecase/advice_usecase.dart';
import 'components/advice_card.dart';
import 'controller/advice_controller.dart';

class AllAdviceView extends StatefulWidget {
  const AllAdviceView(
      {Key? key, this.searchAdviceRequest, this.advice, this.isAllAdvice})
      : super(key: key);
  final SearchAdviceRequest? searchAdviceRequest;
  final AdviceCategoryModel? advice;
  final bool? isAllAdvice;

  @override
  State<AllAdviceView> createState() => _AllAdviceViewState();
}

class _AllAdviceViewState extends State<AllAdviceView> {
  ConseilViewModel viewModel = instance<ConseilViewModel>();
  List<AdviceModel> list = [];
  AdviceController adviceController = AdviceController();
  ScrollController scrollController = ScrollController();
  AdviceUseCase adviceUseCase = instance<AdviceUseCase>();
  void scrollListener() async {
    if (scrollController.offset >= scrollController.position.maxScrollExtent &&
        !scrollController.position.outOfRange) {
        if( adviceController.currentPageIndexPage<=adviceController.totalPage.value) {
           await adviceController.getListAdvice();
        }
     
    }
  }

  bind() {
    if (widget.isAllAdvice == true) {
      adviceController.getListAdvice();
    } else {
      adviceController.searchAdvice(widget.searchAdviceRequest!);
    }
  }

  @override
  void initState() {
    scrollController.addListener(() => scrollListener());
    bind();
    super.initState();
  }

  @override
  void dispose() {
    viewModel.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: ColorManager.black,
          title: Text(
            widget.advice != null ? widget.advice!.label : AppStrings.conseils,
            style: getBoldTextStyle(
              color: ColorManager.white,
            ),
          ),
        ),
        body: Obx(() => showListAdviceWidget()));
  }

  Widget showListAdviceWidget() {
    return adviceController.isLoadInitAdviceListe.value == false
        ? adviceController.listAdvice.isNotEmpty
            ? ListView.builder(
                controller: scrollController,
                itemCount: adviceController.listAdvice.length,
                physics:const AlwaysScrollableScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  return AnimationConfiguration.staggeredList(
                    position: index,
                    duration: const Duration(milliseconds: 100),
                    child: SlideAnimation(
                      verticalOffset: 50.0,
                      child: FadeInAnimation(
                        child: AdviceCard(
                            advice: adviceController.listAdvice[index]),
                      ),
                    ),
                  );
                },
              )
            : const EmptyComponenent(message: "Liste vide",)
        : const LoaderComponent();
  }
}
