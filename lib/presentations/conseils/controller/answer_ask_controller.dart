import 'package:eagri/domain/usecase/advice_usecase.dart';
import 'package:get/get.dart';

import '../../../app/di.dart';
import '../../../data/request/request.dart';
import '../../../domain/model/model.dart';

class AnswerAskConntroller extends GetxController {
  final isLoad = true.obs;
  AdviceUseCase adviceUseCase = instance<AdviceUseCase>();
  final listAskAdvice = Rx(ListAskAdviceModel());
  getUserAsk(GetUserAskAdviceRequest request) async {
    (await adviceUseCase.getUserAsk(request)).fold(
        (failure) => {
              // left -> failure
            }, (data) {
      listAskAdvice.value = data;
      // right -> success (data)
      // listAdvice.add(data);
    });
    isLoad.value = false;
  }
}
