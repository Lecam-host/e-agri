import 'dart:developer';

import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:get/get.dart';

import '../../../app/di.dart';
import '../../../data/request/request.dart';
import '../../../domain/model/model.dart';
import '../../../domain/usecase/advice_usecase.dart';

class AdviceController extends GetxController {
  var isLoadInitAdviceListe = true.obs;
  final listAdvice = <AdviceModel>[].obs;
  var listAdviceCategory = <AdviceCategoryModel>[].obs;

  var nextIndexPage = 0.obs;
  var previousIndexPage = 0.obs;
  var currentPageIndexPage = 0.obs;
  var totalAdvice = 0.obs;
  var totalPage = 0.obs;

  AdviceUseCase adviceUseCase = instance<AdviceUseCase>();
  searchAdvice(SearchAdviceRequest request) async {
   inspect(request);
    (await adviceUseCase.searchAdvices(request)).fold(
        (failure) {
              // left -> failure
             isLoadInitAdviceListe.value = false;
            }, (data) {
              isLoadInitAdviceListe.value = false;
      listAdvice.addAll(data.listAdvice);
    });
  }
  getListAdvice() async {
    (await adviceUseCase
            .getListAdvice(GetListAdviceRequest(10, nextIndexPage.value)))
        .fold((failure) {
          showCustomFlushbar(Get.context!, failure.message, ColorManager.error, FlushbarPosition.TOP);
      isLoadInitAdviceListe.value = false;
    }, (data) {
      nextIndexPage.value = data.nextPage;
      previousIndexPage.value = data.previousPage;
      currentPageIndexPage.value = data.currentPage;
      nextIndexPage.value = data.nextPage;
      totalAdvice.value = data.totalObject;
      totalPage.value = data.totalOfPages;
      isLoadInitAdviceListe.value = false;
      listAdvice.addAll(data.listAdvice);
    });
  }
}
