import 'package:eagri/app/constant.dart';
import 'package:eagri/data/request/request.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/conseils/conseil_view_model.dart';
import 'package:eagri/presentations/conseils/details_advice_view.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/font_manager.dart';
import 'package:eagri/presentations/ressources/strings_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:eagri/presentations/ressources/values_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

import '../../app/di.dart';
import '../../app/functions.dart';
import '../common/state/search_empty_component.dart';
import '../common/state_renderer/state_render_impl.dart';
import '../ressources/icon_manager.dart';

class AfterSearchView extends StatefulWidget {
  const AfterSearchView({Key? key, required this.searchAdviceRequest})
      : super(key: key);
  final SearchAdviceRequest searchAdviceRequest;

  @override
  State<AfterSearchView> createState() => _AfterSearchViewState();
}

class _AfterSearchViewState extends State<AfterSearchView> {
  ConseilViewModel viewModel = instance<ConseilViewModel>();
  List<AdviceModel> list = [];

  bind() {
    viewModel.searchAdvice(widget.searchAdviceRequest);
  }

  @override
  void initState() {
    bind();
    super.initState();
  }

  @override
  void dispose() {
    viewModel.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<FlowState>(
      stream: viewModel.outputState,
      builder: (context, snapshot) {
        return Scaffold(
            appBar: AppBar(
              title: const Text("Récherche"),
              leading: const BackButtonCustom(),
            ),
            body: snapshot.data?.getScreenWidget(context, getContent(), () {
                  bind();
                }) ??
                getContent());
      },
    );
  }

  Widget getContent() {
    return StreamBuilder<ListAdviceModel>(
      stream: viewModel.outputIsListAdviceSearch,
      builder: (context, snapshot) {
        return showListAdviceWidget(snapshot.data);
      },
    );
  }

  Widget showListAdviceWidget(ListAdviceModel? data) {
    return data != null
        ? ListView.builder(
            itemCount: data.listAdvice.length,
            itemBuilder: (BuildContext context, int index) {
              return AnimationConfiguration.staggeredList(
                position: index,
                duration: const Duration(milliseconds: 100),
                child: SlideAnimation(
                  verticalOffset: 50.0,
                  child: FadeInAnimation(
                    child: conseilCard(data.listAdvice[index]),
                  ),
                ),
              );
            },
          )
        : const SearchEmptyComponent();
  }

  Widget conseilCard(AdviceModel advice) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => DetailsAdviceView(advice: advice)));
      },
      child: Card(
        elevation: 0.2,
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.only(
              top: AppPadding.p5,
              bottom: AppPadding.p5,
              left: AppPadding.p10,
              right: AppPadding.p10),
          margin: const EdgeInsets.only(bottom: AppMargin.m10),
          decoration: BoxDecoration(color: ColorManager.white),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                advice.title.toUpperCase(),
                style: getBoldTextStyle(
                    color: ColorManager.black, fontSize: FontSize.s15),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                advice.description,
                style: getRegularTextStyle(
                  color: ColorManager.black,
                  fontSize: FontSize.s15,
                ),
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Text(
                    "ANADER",
                    style: getRegularTextStyle(
                      color: ColorManager.blue,
                      fontSize: FontSize.s12,
                    ),
                  ),
                  Text(
                    ", le ${convertDate(advice.createDate)}",
                    style: getRegularTextStyle(
                      color: ColorManager.grey,
                      fontSize: FontSize.s12,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Text(
                    "0 ${AppStrings.view}",
                    style: getLightTextStyle(
                      color: ColorManager.black,
                    ),
                  ),
                  const Spacer(),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        for (FileModel file in advice.files) ...[
                          if (file.typeFile == CodeConfig.pdfCode)
                            Icon(
                              IconManager.doc,
                              color: ColorManager.blue,
                              size: 14,
                            ),
                          if (file.typeFile == CodeConfig.audioCode)
                            const Icon(
                              IconManager.musique,
                              size: 14,
                            ),
                          if (file.typeFile == CodeConfig.videoCode)
                            const Icon(
                              IconManager.video,
                              size: 14,
                            ),
                        ],
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
