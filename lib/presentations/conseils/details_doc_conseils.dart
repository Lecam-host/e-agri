import 'package:eagri/domain/model/model.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/ressources/icon_manager.dart';
import 'package:flutter/material.dart';

import '../common/pdf_screen.dart';
import '../ressources/color_manager.dart';

class DetailsDocConseilsView extends StatefulWidget {
  const DetailsDocConseilsView({Key? key, this.file}) : super(key: key);
  final FileModel? file;
  @override
  State<DetailsDocConseilsView> createState() => _DetailsDocConseilsViewState();
}

class _DetailsDocConseilsViewState extends State<DetailsDocConseilsView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: const BackButtonCustom(),
          actions: [
            IconButton(
              onPressed: () {},
              icon: Icon(
                IconManager.share,
                color: ColorManager.black,
              ),
            ),
            IconButton(
              onPressed: () {
                // Navigator.of(context).push(
                //   MaterialPageRoute(
                //       builder: (context) => DocComponentView(
                //             link: widget.file!.fileLink,
                //           )),
                // );
              },
              icon: Icon(
                IconManager.doc,
                color: ColorManager.black,
              ),
            )
          ],
        ),
        body: PdfScreenn(
          pdfLink: widget.file!.fileLink,
        ));
  }
}
