import 'dart:io';

import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/data/request/request_object.dart';
import 'package:eagri/domain/model/audio_model.dart';
import 'package:eagri/presentations/common/audio/app_audio.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../app/constant.dart';
import '../../domain/model/model.dart';
import '../common/video/play_video.dart';
import '../ressources/color_manager.dart';

class AddFileAdviceView extends StatefulWidget {
  const AddFileAdviceView({Key? key}) : super(key: key);

  @override
  State<AddFileAdviceView> createState() => _AddFileAdviceViewState();
}

class _AddFileAdviceViewState extends State<AddFileAdviceView> {
  List<FileModel> listTypeFile = [];
  List<Map<String, dynamic>> listFiles = [];
  List<AdviceCategoryModel> advicesCategories = [];
  Map<String, dynamic>? fileInfo;
  TextEditingController titreFileController = TextEditingController();
  TextEditingController descriptionFileController = TextEditingController();
  @override
  void initState() {
    chooseFile();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButtonCustom(icon: Icons.clear),
        title: const Text('Ajouter un fichier'),
        actions: [
          if (fileInfo != null)
            SizedBox(
              height: 30,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(),
                onPressed: () {
                  if (fileInfo == null) {
                    showCustomFlushbar();
                  } else {
                    DescriptionFile descriptionFile = DescriptionFile(
                        title: titreFileController.text,
                        description: descriptionFileController.text);
                    var info = <String, dynamic>{"info": descriptionFile};

                    fileInfo!.addEntries(info.entries);
                    Navigator.of(context).pop(fileInfo);
                  }
                },
                child: const Text('Ajouter'),
              ),
            )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.all(10),
          child: Column(children: [
            const SizedBox(
              height: 20,
            ),
            TextFormField(
              //   maxLines: null,
              controller: titreFileController,
              decoration: InputDecoration(
                filled: true,
                hintText: "Entrer le titre du fichier",
                fillColor: ColorManager.formFieldBackgroundColor,
                labelText: "Titre du fichier",
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            TextFormField(
              //   maxLines: null,
              controller: descriptionFileController,
              decoration: InputDecoration(
                filled: true,
                hintText: "Entrer la description du fichier",
                fillColor: ColorManager.formFieldBackgroundColor,
                labelText: "Description du fichier",
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            if (fileInfo == null)
              ElevatedButton.icon(
                style: ElevatedButton.styleFrom(backgroundColor: Colors.black),
                onPressed: () {
                  chooseFile();
                },
                icon: const Icon(Icons.file_download),
                label: const Text("Ajouter un fichier"),
              ),
            if (fileInfo != null) showFile(),
          ]),
        ),
      ),
    );
  }

  showCustomFlushbar() {
    return Flushbar(
      maxWidth: 200,
      flushbarPosition: FlushbarPosition.BOTTOM,
      duration: const Duration(seconds: 3),
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.all(10),
      borderRadius: BorderRadius.circular(15),
      backgroundColor: Colors.red,
      icon: Icon(Icons.info, color: ColorManager.white),
      boxShadows: const [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],
      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      // title: '',
      message: "Ajouter un fichier",
    ).show(context);
  }

  Widget showFile() {
    return Column(
      children: [
        if (Constant.imagesExtension.contains(fileInfo!["extension"])) ...[
          Image.file(
            File(fileInfo!["file"].path),
            fit: BoxFit.contain,
          ),
        ],
        if (Constant.videoExtension.contains(fileInfo!["extension"])) ...[
          AppVideoPlay(isNetworkVideo: false, link: '', file: fileInfo!["file"])
        ],
        if (Constant.audioExtension.contains(fileInfo!["extension"])) ...[
          AppAudio(
            audio: AudioModel(link: fileInfo!["file"].path),
          )
        ]
      ],
    );
  }

  chooseFile() async {
    var status = await Permission.storage.isDenied;
    if (status) {
      await Permission.storage.request();
      status = await Permission.storage.isDenied;
      // We didn't ask for permission yet or the permission has been denied before but not permanently.
      if (status) {
        await Permission.storage.request();
      } else {
        getFile();
      }
    } else {
      getFile();
    }
  }

  getFile() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles();
    if (result != null) {
      File file = File(result.files.single.path!);

      setState(() {
        fileInfo = {
          "file": file,
          "extension": result.files.single.extension,
        };
      });
    } else {}

    // var status = await Permission.storage.isDenied;
  }
}
