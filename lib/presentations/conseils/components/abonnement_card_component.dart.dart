import 'package:flutter/material.dart';

import '../../ressources/assets_manager.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/font_manager.dart';
import '../../ressources/strings_manager.dart';
import '../../ressources/styles_manager.dart';
import '../../ressources/values_manager.dart';

class AbonnementCardComponent extends StatelessWidget {
  const AbonnementCardComponent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: MediaQuery.of(context).size.width / 2,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Abonnez-vous pour plus de contenu",
                style: getBoldTextStyle(
                    color: ColorManager.black, fontSize: FontSize.s20),
              ),
              const Spacer(),
              SizedBox(
                height: AppSize.s40,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      elevation: 0,
                      backgroundColor: ColorManager.primary,
                      textStyle: getRegularTextStyle(
                        color: ColorManager.white,
                      ),
                    ),
                    child: const Text(AppStrings.iSubscribe),
                    onPressed: () {
                      // if (snapshot.data ?? false) {
                      //   _viewModel.login();
                      // }
                    }),
              )
            ],
          ),
        ),
        Image.asset(
          ImageAssets.souscription,
          width: 150,
        ),
      ],
    );
  }
}
