import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/app/functions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../app/constant.dart';
import '../../../app/di.dart';
import '../../../data/repository/api/repository_impl.dart';
import '../../../data/request/request.dart';
import '../../../domain/model/model.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/styles_manager.dart';

class AdviceCard extends StatefulWidget {
  const AdviceCard({Key? key, required this.advice}) : super(key: key);
  final AdviceModel advice;

  @override
  State<AdviceCard> createState() => _AdviceCardState();
}

class _AdviceCardState extends State<AdviceCard> {
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  String publicateur = "";
  getPublicateurInfo() {
    repositoryImpl
        .getInfoById(InfoByIdRequest(userId: widget.advice.publicateurId))
        .then((response) {
      response.fold((l) => null, (data) {
        if (data.user != null) {
          if (mounted) {
            setState(() {
              publicateur = "${data.user!.firstname} ${data.user!.laststname}";
            });
          }
        }
      });
    });
  }

  @override
  void initState() {
    getPublicateurInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 1.1,
      height: 120,
      padding: const EdgeInsets.all(10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (widget.advice.illustration.isNotEmpty) ...[
            Container(
              height: 120,
              decoration: BoxDecoration(
                color: kSecondaryColor.withOpacity(0.1),
                borderRadius: BorderRadius.circular(5.0),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(5.0),
                child: CachedNetworkImage(
                  imageUrl: widget.advice.illustration,
                  imageBuilder: (context, imageProvider) => Container(
                    width: 100,
                    height: 120,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.0),
                      image: DecorationImage(
                          scale: 1,
                          image: imageProvider,
                          fit: BoxFit.cover,
                          colorFilter: const ColorFilter.mode(
                              Colors.grey, BlendMode.colorBurn)),
                    ),
                  ),
                  placeholder: (context, url) => Container(
                    decoration: BoxDecoration(
                      color: kSecondaryColor.withOpacity(0.1),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    width: 100,
                    height: 120,
                  ),
                  errorWidget: (context, url, error) => Container(
                    decoration: BoxDecoration(
                      color: kSecondaryColor.withOpacity(0.1),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    width: 100,
                    height: 120,
                    child: const Icon(Icons.error),
                  ),
                ),
              ),
            ),
          ] else ...[
            Container(
              color: ColorManager.grey.withOpacity(0.2),
              width: 100,
              height: 120,
              child: const Icon(Icons.image),
            ),
          ],
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 50,
                    child: Text(
                      widget.advice.title,
                      style: getMeduimTextStyle(
                        color: ColorManager.black,
                      ),
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  const Spacer(),
                  Text(
                    publicateur,
                    style: getRegularTextStyle(
                        color: ColorManager.grey, fontSize: 10),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Row(
                    children: [
                      Text(
                        "${widget.advice.numberView} vues . ",
                        style: getRegularTextStyle(
                          color: ColorManager.grey,
                          fontSize: 10,
                        ),
                      ),
                      Row(
                        children: [
                          Text(
                            "${widget.advice.note}/5",
                            style: getRegularTextStyle(
                              color: ColorManager.grey,
                              fontSize: 8,
                            ),
                          ),
                          SvgPicture.asset(
                            "assets/icons/Star Icon.svg",
                            width: 10,
                          ),
                        ],
                      ),
                      const Spacer(),
                      Text(
                        convertDate(widget.advice.createDate),
                        style: getRegularTextStyle(
                          color: ColorManager.grey,
                          fontSize: 10,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
