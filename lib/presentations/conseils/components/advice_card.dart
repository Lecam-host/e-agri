import 'package:animations/animations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/presentations/conseils/details_advice_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import '../../../app/constant.dart';
import '../../../app/functions.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/font_manager.dart';
import '../../ressources/size_config.dart';
import '../../ressources/strings_manager.dart';
import '../../ressources/styles_manager.dart';
import '../../ressources/values_manager.dart';

class AdviceCard extends StatelessWidget {
  const AdviceCard({Key? key, required this.advice}) : super(key: key);
  final AdviceModel advice;
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openElevation: 0,
      closedElevation: 0,
      transitionType: transitionType,
      transitionDuration: transitionDuration,
      openBuilder: (context, _) => DetailsAdviceView(advice: advice),
      closedBuilder: (context, VoidCallback openContainer) => adivceItem(),
    );
  }

  Card adivceItem() {
    return Card(
      elevation: 0.2,
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.only(
            top: AppPadding.p5,
            bottom: AppPadding.p5,
            left: AppPadding.p10,
            right: AppPadding.p10),
        margin: const EdgeInsets.only(bottom: AppMargin.m10),
        decoration: BoxDecoration(color: ColorManager.grey.withOpacity(0.1)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                if (advice.illustration.isNotEmpty)
                  SizedBox(
                    height: getProportionateScreenHeight(100),
                    width: getProportionateScreenWidth(100),
                    child: CachedNetworkImage(
                      imageUrl: advice.illustration,
                      imageBuilder: (context, imageProvider) => Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            scale: 1,
                            image: imageProvider,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      placeholder: (context, url) => Container(
                        width: getProportionateScreenWidth(30),
                        height: getProportionateScreenHeight(30),
                        color: ColorManager.grey,
                      ),
                      errorWidget: (context, url, error) =>
                          const Icon(Icons.error),
                      width: getProportionateScreenHeight(30),
                    ),
                  ),
                Container(
                  margin: EdgeInsets.only(
                    left: getProportionateScreenWidth(10),
                  ),
                  width: getProportionateScreenWidth(230),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        child: Text(
                          advice.title,
                          style: getSemiBoldTextStyle(
                              color: ColorManager.black,
                              fontSize: FontSize.s15),
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "${advice.numberView} ${AppStrings.view} |",
                            style: getLightTextStyle(
                              color: ColorManager.black,
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          RatingBar.builder(
                            initialRating: 2.2,
                            minRating: 1,
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 5,
                            itemSize: 15,
                            itemPadding:
                                const EdgeInsets.symmetric(horizontal: 1),
                            itemBuilder: (context, _) => const Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                            onRatingUpdate: (rating) {},
                          ),
                          // Align(
                          //   alignment: Alignment.bottomRight,
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.end,
                          //     mainAxisAlignment: MainAxisAlignment.end,
                          //     children: [
                          //       for (FileModel file in advice.files) ...[
                          //         if (file.typeFile == CodeConfig.pdfCode)
                          //           const Icon(
                          //             IconManager.doc,
                          //             size: 14,
                          //           ),
                          //         if (file.typeFile == CodeConfig.audioCode)
                          //           const Icon(
                          //             IconManager.musique,
                          //             size: 14,
                          //           ),
                          //         if (file.typeFile == CodeConfig.videoCode)
                          //           const Icon(
                          //             IconManager.video,
                          //             size: 14,
                          //           ),
                          //       ],
                          //     ],
                          //   ),
                          // ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "ANADER",
                            style: getRegularTextStyle(
                              color: ColorManager.blue,
                              fontSize: FontSize.s12,
                            ),
                          ),
                          Text(
                            " | le ${convertDate(advice.createDate)}",
                            style: getRegularTextStyle(
                              color: ColorManager.grey,
                              fontSize: FontSize.s12,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
