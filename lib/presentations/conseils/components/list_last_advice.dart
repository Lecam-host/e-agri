import 'package:animations/animations.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/presentations/conseils/details_advice_view.dart';
import 'package:eagri/presentations/home/components/section_title.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import '../../../app/constant.dart';
import '../../../app/di.dart';
import '../../../data/request/request.dart';
import '../../../domain/usecase/advice_usecase.dart';
import '../../conseils/components/last_advice_card.dart';
import '../../ressources/size_config.dart';
import '../../ressources/strings_manager.dart';
import '../../ressources/styles_manager.dart';
import '../all_advice_view.dart';

class ListLastAdvice extends StatefulWidget {
  const ListLastAdvice({
    Key? key,
    this.list,
  }) : super(key: key);
  final List<AdviceModel>? list;
  @override
  State<ListLastAdvice> createState() => _ListLastAdviceState();
}

class _ListLastAdviceState extends State<ListLastAdvice> {
  AdviceUseCase adviceUseCase = instance<AdviceUseCase>();
  List<AdviceModel> listAdvice = [];
  getListLastAdvice() {
    adviceUseCase.getListAdvice(GetListAdviceRequest(4, 0)).then((response) {
      response.fold((l) {}, (listAdviceResponse) {
        if (mounted) {
          setState(() {
            listAdvice = listAdviceResponse.listAdvice;
          });
        }
      });
    });
  }

  getStart() {
    if (widget.list != null) {
      if (mounted) {
        setState(() {
          listAdvice = widget.list!;
        });
      }
    }
  }

  @override
  void initState() {
    getStart();
    getListLastAdvice();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return listAdvice.isNotEmpty
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: SectionTitle(
                    title: AppStrings.lastConseil,
                    style: getMeduimTextStyle(
                        color: ColorManager.black, fontSize: 18),
                    press: () {
                      Navigator.push(
                        context,
                        PageTransition(
                          type: PageTransitionType.bottomToTop,
                          child: const AllAdviceView(
                            isAllAdvice: true,
                          ),
                          isIos: true,
                          duration: const Duration(milliseconds: 400),
                        ),
                      );
                    }),
              ),
              SizedBox(height: getProportionateScreenWidth(10)),
              SingleChildScrollView(
                padding:
                    EdgeInsets.only(bottom: getProportionateScreenWidth(20)),
                scrollDirection: Axis.horizontal,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ...List.generate(
                      listAdvice.length,
                      (index) {
                        return Container(
                          margin: const EdgeInsets.only(left: 10,bottom: 10),
                          child: OpenContainer(
                            openElevation: 0,
                            closedElevation: 0,
                            transitionType: transitionType,
                            transitionDuration: transitionDuration,
                            openBuilder: (context, _) =>
                                DetailsAdviceView(advice: listAdvice[index]),
                            closedBuilder:
                                (context, VoidCallback openContainer) =>
                                    AdviceCard(advice: listAdvice[index]),
                          ),
                        );
                        // here by default width and height is 0
                      },
                    ),
                    SizedBox(width: getProportionateScreenWidth(20)),
                  ],
                ),
              ),
            ],
          )
        : const SizedBox();
  }
}
