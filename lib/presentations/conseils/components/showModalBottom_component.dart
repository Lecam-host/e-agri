// ignore_for_file: file_names

import 'package:flutter/material.dart';

import '../../common/showModalBottomSheetListTile.dart';
import '../../ressources/icon_manager.dart';
import '../../ressources/strings_manager.dart';
import '../demande_conseil_view.dart';
import '../list_mes_demanndes_view.dart';

Future<dynamic> showModalBottomCustom(BuildContext context) {
  return showModalBottomSheet(
      context: context,
      builder: (context) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            showModalBottomSheetListTile(
                titre: AppStrings.demandeConseil,
                icon: IconManager.question,
                widget: const DemandeConveilView(),
                context: context),
            showModalBottomSheetListTile(
                titre: AppStrings.forumQuestion,
                icon: IconManager.forumQuestiom,
                widget: const ListMesDemandes(),
                context: context),
          ],
        );
      });
}
