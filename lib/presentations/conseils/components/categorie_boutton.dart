import 'package:eagri/data/request/request.dart';
import 'package:eagri/presentations/conseils/all_advice_view.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import '../../../domain/model/model.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/styles_manager.dart';
import '../../ressources/values_manager.dart';

class CategorieButton extends StatelessWidget {
  const CategorieButton({Key? key, required this.advice}) : super(key: key);
  final AdviceCategoryModel advice;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          PageTransition(
            type: PageTransitionType.bottomToTop,
            child: AllAdviceView(
              isAllAdvice: advice.id == 0 ? true : false,
              advice: advice,
              searchAdviceRequest: SearchAdviceRequest(
                categoriesId: [advice.id],
                userId: 1,
              ),
            ),
            isIos: true,
            duration: const Duration(milliseconds: 400),
          ),
        );
      },
      child: Container(
        decoration: BoxDecoration(
          color: ColorManager.primary,
          borderRadius: BorderRadius.circular(5),
        ),
        padding: const EdgeInsets.all(AppPadding.p8),
        margin: const EdgeInsets.only(
          left: AppMargin.m5,
          bottom: AppMargin.m5,
        ),
        child: Text(
          advice.label,
          style: getMeduimTextStyle(color: ColorManager.white),
        ),
      ),
    );
  }
}
