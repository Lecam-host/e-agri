import 'package:flutter/material.dart';

import '../../ressources/color_manager.dart';
import '../../ressources/styles_manager.dart';
import '../../ressources/values_manager.dart';

class FilterButtonComponent extends StatelessWidget {
  const FilterButtonComponent({
    Key? key,
    required this.titre,
    required this.isActif,
  }) : super(key: key);
  final String titre;
  final bool? isActif;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: isActif == true ? ColorManager.primary : ColorManager.grey,
          borderRadius: BorderRadius.circular(15)),
      padding: const EdgeInsets.all(AppPadding.p8),
      margin: const EdgeInsets.only(left: AppMargin.m5),
      child: Text(
        titre,
        style: getRegularTextStyle(
          color: ColorManager.white,
        ),
      ),
    );
  }
}
