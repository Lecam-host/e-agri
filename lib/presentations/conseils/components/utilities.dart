import '../../../domain/model/model.dart';

List<int> getListInt(List<SelectObjectModel> data) {
  List<int> dataOutPut = [];
  for (var element in data) {
    dataOutPut.add(element.id!);
  }
  return dataOutPut;
}
