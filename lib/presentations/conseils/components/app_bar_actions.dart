import 'package:eagri/presentations/conseils/components/showModalBottom_component.dart';
import 'package:flutter/material.dart';

import '../../common/buttons/search_button.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/icon_manager.dart';
import '../../ressources/routes_manager.dart';

List<Widget> actions(BuildContext context) {
  return [
    SearchIconButton(
      onPressed: () {
        Navigator.pushNamed(context, Routes.searchConseil);
      },
    ),
    IconButton(
      onPressed: () {
        showModalBottomCustom(context);
      },
      icon: Icon(
        IconManager.moreMenu,
        color: ColorManager.black,
      ),
    )
  ];
}
