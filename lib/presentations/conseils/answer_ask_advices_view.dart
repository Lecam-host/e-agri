import 'package:eagri/app/di.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/domain/usecase/advice_usecase.dart';
import 'package:eagri/presentations/conseils/components/advice_card.dart';
import 'package:flutter/material.dart';

import '../../app/constant.dart';
import '../../data/request/request.dart';

class AnswerAskDemandeView extends StatefulWidget {
  const AnswerAskDemandeView({Key? key, required this.askId}) : super(key: key);
  final int askId;

  @override
  State<AnswerAskDemandeView> createState() => _AnswerAskDemandeViewState();
}

class _AnswerAskDemandeViewState extends State<AnswerAskDemandeView> {
  List<AdviceModel> listAdvice = [];
  AdviceUseCase adviceUseCase = instance<AdviceUseCase>();

  @override
  void initState() {
    getListAnswer();
    super.initState();
  }

  getListAnswer() {
    adviceUseCase
        .getUserAskAdviceAnswer(
      GetUserAskAdviceAnswerRequest(askId: widget.askId),
    )
        .then((value) {
      value.fold((l) => null, (r) {
        setState(() {
          listAdvice = r.listAdvice;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    const padding = EdgeInsets.fromLTRB(10, 10, 10, 0);

    return Scaffold(
        body: CustomScrollView(
      slivers: [
        const SliverAppBar(
          floating: true,
          pinned: true,
          snap: false,
          centerTitle: false,
          title: Text('Place du marché'),
          actions: [],
        ),
        SliverPadding(
          padding: padding,
          sliver: buildProducts(),
        ),
      ],
    ));
  }

  Widget buildProducts() {
    return SliverGrid(
      gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: Constant.widthPoint * 1.5,
        mainAxisSpacing: 10,
        crossAxisSpacing: 10,
        mainAxisExtent: 150,
      ),
      delegate: SliverChildBuilderDelegate(
        (context, index) => _buildProductItem(context, index),
        childCount: listAdvice.length,
      ),
    );
  }

  Widget _buildProductItem(BuildContext context, int index) {
    return AdviceCard(
      advice: listAdvice[index],
    );
  }
}
