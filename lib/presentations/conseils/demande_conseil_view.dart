import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/app/constant.dart';
import 'package:eagri/data/request/request.dart';
import 'package:eagri/domain/usecase/advice_usecase.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/ressources/values_manager.dart';
import 'package:flutter/material.dart';
import 'package:multi_select_flutter/dialog/multi_select_dialog_field.dart';
import 'package:multi_select_flutter/util/multi_select_item.dart';
import 'package:multi_select_flutter/util/multi_select_list_type.dart';
import 'package:page_transition/page_transition.dart';

import '../../app/di.dart';
import '../../domain/model/model.dart';
import '../common/state/succes_page_view.dart';
import '../common/state_renderer/state_render_impl.dart';
import '../common/state_renderer/state_renderer.dart';
import '../ressources/color_manager.dart';
import '../ressources/strings_manager.dart';
import '../ressources/styles_manager.dart';
import 'components/app_bar_actions.dart';
import 'conseil_view_model.dart';

class DemandeConveilView extends StatefulWidget {
  const DemandeConveilView({Key? key}) : super(key: key);

  @override
  State<DemandeConveilView> createState() => _DemandeConveilViewState();
}

class _DemandeConveilViewState extends State<DemandeConveilView> {
  askAdvice(AskAdviceRequest adviceRequest) async {
    LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE);
    (await adviceUseCase.askAdvice(adviceRequest)).fold((failure) => {},
        (data) {
      Navigator.pushReplacement(
        context,
        PageTransition(
          type: PageTransitionType.bottomToTop,
          child: const SuccessPageView(
            message:
                "Votre demande a été pris en compte et est en cours de traitement",
          ),
          isIos: true,
          duration: const Duration(milliseconds: 400),
        ),
      );
    });
  }

  AdviceUseCase adviceUseCase = instance<AdviceUseCase>();

  ConseilViewModel viewModel = instance<ConseilViewModel>();
  AskAdviceRequest adviceRequest = AskAdviceRequest([], [], 1, [], "", 1);
  TextEditingController questionInputController = TextEditingController();
  List<int> entitiesIdOutput = [];
  List<ConfigModel> listTypeFormat = [];
  List<int> categoriesIdOutput = [];
  ConfigModel? adviceTypeOutput;
  List<int> mediaTypeOutput = [];
  String descriptionOutput = "";
  int userIdInput = 1;
  static List<ConfigModel> entities = [];
  static List<ConfigModel> typeAdvices = [];
  List<AdviceCategoryModel> listCategorieAdvice = [];
  AdviceCategoryModel? adviceOutput;
  getListCategoryAdvice() async {
    // adviceCategoriesRepo
    //     .getAllItem()
    //     .then((value) => listAdviceCategory.add(value));

    (await adviceUseCase.getAdviceCategory()).fold((failure) => {}, (data) {
      setState(() {
        listCategorieAdvice = data.listAdviceCategory;
      });
    });
  }

  getConfig() {
    adviceUseCase.getAllConfig().then((value) {
      value.fold((l) => null, (result) {
        listTypeFormat = listTypeFormat =
            getListSelect(result.allConfig!, CodeConfig.formatFileCode);
        entities =
            getListSelect(result.allConfig!, CodeConfig.organizationCode);
        typeAdvices =
            getListSelect(result.allConfig!, CodeConfig.typeAdviceCode);
      });
    });
  }

  List<ConfigModel> getListSelect(List<ListConfigModel> inputList, int code) {
    List<ConfigModel> output = [];
    for (var element in inputList) {
      if (element.code == code) {
        if (element.listConfig != null) {
          setState(() {
            output = element.listConfig!;
          });
        }
      }
    }
    return output;
  }

  List<String> getListString(List<SelectObjectModel> data) {
    List<String> dataOutPut = [];
    for (var element in data) {
      dataOutPut.add(element.name!);
    }
    return dataOutPut;
  }

  @override
  void initState() {
    getListCategoryAdvice();
    getConfig();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButtonCustom(),
        title: const Text('Demander un conseil'),
        actions: actions(context),
      ),
      body: StreamBuilder<FlowState>(
        stream: viewModel.outputState,
        builder: (context, snapshot) {
          return snapshot.data?.getScreenWidget(context, getContent(), () {
                viewModel.askAdvice(adviceRequest);
              }) ??
              getContent();
        },
      ),
    );
  }

  Container getContent() {
    return Container(
      margin: const EdgeInsets.all(
        AppMargin.m10,
      ),
      child: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                decoration: containerBoxDecoration(),
                child: MultiSelectDialogField(
                  buttonIcon: const Icon(Icons.keyboard_arrow_down),
                  confirmText: const Text(AppStrings.ok),
                  cancelText: const Text(AppStrings.cancel),
                  title: const Text(AppStrings.addressRequest),
                  buttonText: const Text(AppStrings.addressRequest),
                  items:
                      entities.map((e) => MultiSelectItem(e, e.name!)).toList(),
                  listType: MultiSelectListType.LIST,
                  onConfirm: (List<ConfigModel> values) {
                    for (var element in values) {
                      entitiesIdOutput.add(element.id!);
                    }
                    setState(() {
                      entitiesIdOutput;
                    });
                  },
                  selectedColor: ColorManager.primary,
                ),
              ),
              const SizedBox(
                height: AppMargin.m20,
              ),
              Container(
                decoration: containerBoxDecoration(),
                child: MultiSelectDialogField(
                  buttonIcon: const Icon(Icons.keyboard_arrow_down),
                  buttonText: const Text(AppStrings.adviceCategoryQuestion),
                  confirmText: const Text(AppStrings.ok),
                  cancelText: const Text(AppStrings.cancel),
                  title: const Text(AppStrings.adviceCategoryQuestion),
                  items: listCategorieAdvice
                      .map((e) => MultiSelectItem(e, e.label))
                      .toList(),
                  listType: MultiSelectListType.LIST,
                  onConfirm: (List<AdviceCategoryModel> values) {
                    for (var element in values) {
                      categoriesIdOutput.add(element.id);
                    }
                    setState(() {
                      categoriesIdOutput;
                    });
                  },
                  selectedColor: ColorManager.primary,
                ),
              ),
              const SizedBox(
                height: AppMargin.m20,
              ),
              Container(
                decoration: containerBoxDecoration(),
                child: DropdownButton(
                  hint: const Text(AppStrings.adviceTypeQuestion),
                  dropdownColor: ColorManager.white,
                  isExpanded: true,
                  alignment: AlignmentDirectional.centerEnd,
                  value: adviceTypeOutput,
                  icon: const Icon(Icons.keyboard_arrow_down),
                  items: typeAdvices.map((ConfigModel items) {
                    return DropdownMenuItem(
                      value: items,
                      child: Text(items.name!),
                    );
                  }).toList(),
                  onChanged: (ConfigModel? newValue) {
                    setState(() {
                      adviceTypeOutput = newValue!;
                    });
                  },
                ),
              ),
              const SizedBox(
                height: AppMargin.m20,
              ),
              Container(
                decoration: containerBoxDecoration(),
                child: MultiSelectDialogField(
                  buttonIcon: const Icon(Icons.keyboard_arrow_down),
                  buttonText: const Text(
                    AppStrings.typeDeMedia,
                  ),
                  confirmText: const Text(AppStrings.ok),
                  cancelText: const Text(AppStrings.cancel),
                  title: const Text(AppStrings.addressRequest),
                  items: listTypeFormat
                      .map((e) => MultiSelectItem(e, e.name!))
                      .toList(),
                  listType: MultiSelectListType.LIST,
                  onConfirm: (List<ConfigModel> values) {
                    for (var element in values) {
                      mediaTypeOutput.add(element.id!);
                    }
                    setState(() {
                      mediaTypeOutput;
                    });
                  },
                  selectedColor: ColorManager.primary,
                ),
              ),
              const SizedBox(
                height: AppMargin.m20,
              ),
              TextFormField(
                maxLines: null,
                keyboardType: TextInputType.visiblePassword,
                controller: questionInputController,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: ColorManager.white,
                  hintText: AppStrings.enterDemande,
                ),
              ),
              const SizedBox(
                height: AppMargin.m40,
              ),
              SizedBox(
                height: AppSize.s40,
                width: double.infinity,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      elevation: 0,
                      backgroundColor: ColorManager.primary,
                      textStyle: getRegularTextStyle(
                        color: ColorManager.white,
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(
                          AppSize.s12,
                        ),
                      ),
                    ),
                    child: const Text(AppStrings.send),
                    onPressed: () {
                      if (adviceTypeOutput != null ||
                          categoriesIdOutput.isNotEmpty ||
                          entitiesIdOutput.isNotEmpty ||
                          mediaTypeOutput.isNotEmpty ||
                          questionInputController.text != "") {
                        adviceRequest.adviceType = adviceTypeOutput!.id!;
                        adviceRequest.categoriesId = categoriesIdOutput;
                        adviceRequest.entitiesId = entitiesIdOutput;
                        adviceRequest.mediaType = mediaTypeOutput;
                        adviceRequest.userId = userIdInput;
                        adviceRequest.description =
                            questionInputController.text;

                        askAdvice(adviceRequest);
                      } else {
                        showCustomFlushbar();
                      }

                      //Navigator.pushReplacementNamed(context, Routes.homeRoute);
                      // if (snapshot.data ?? false) {
                      //   _viewModel.login();
                      // }
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }

  BoxDecoration containerBoxDecoration() {
    return BoxDecoration(
      color: ColorManager.lightGrey.withOpacity(0.3),
    );
  }

  showCustomFlushbar() {
    return Flushbar(
      maxWidth: 200,
      flushbarPosition: FlushbarPosition.BOTTOM,
      duration: const Duration(seconds: 5),
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.all(10),
      borderRadius: BorderRadius.circular(15),
      backgroundColor: Colors.black,
      icon: Icon(Icons.info, color: ColorManager.white),
      boxShadows: const [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],
      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      // title: '',
      message: "Remplir tout les champs",
    ).show(context);
  }
}
