import 'dart:developer';

import 'package:another_flushbar/flushbar.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/app/app_prefs.dart';
import 'package:eagri/app/constant.dart';
import 'package:eagri/data/repository/api/repository_impl.dart';
import 'package:eagri/domain/model/audio_model.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/domain/usecase/advice_usecase.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/font_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:eagri/presentations/ressources/values_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:video_player/video_player.dart';

import '../../app/di.dart';
import '../../app/functions.dart';
import '../../data/request/notation_request.dart';
import '../../data/request/request.dart';
import '../../domain/model/user_model.dart';
import '../common/audio/app_audio.dart';
import '../common/default_button.dart';
import '../ressources/assets_manager.dart';
import '../ressources/icon_manager.dart';

import 'details_doc_conseils.dart';

class DetailsAdviceView extends StatefulWidget {
  const DetailsAdviceView({Key? key, required this.advice}) : super(key: key);

  final AdviceModel advice;

  @override
  State<DetailsAdviceView> createState() => _DetailsAdviceViewState();
}

class _DetailsAdviceViewState extends State<DetailsAdviceView> {
  final _focusNode = FocusNode();
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  AdviceUseCase adviceUseCase = instance<AdviceUseCase>();
  String audioPlay = "";
  AudioPlayer audioPlayer = AudioPlayer();
  Duration duration = Duration.zero;
  bool findVideo = false;
  int indexPlay = -1;
  bool isPlaying = false;
  bool loadVideo = true;
  bool isAlredyNoted = true;
  AppSharedPreference appSharedPreference = instance<AppSharedPreference>();

  Duration position = Duration.zero;
  int selectedPlayerIdx = 0;

  TextEditingController rateController = TextEditingController();
  TextEditingController commantaireController = TextEditingController();

  String timeVideo = "";
  String titre = "";
  String publicateur = "";
  checkIfIsAlredyNoted() {
    appSharedPreference.getUserInformation().then((value) {
      repositoryImpl
          .checkRating(CheckRateRequest(
              codeService: ServiceCode.advice,
              ressourceId: widget.advice.id,
              userId: value.id!))
          .then((response) {
        response.fold((l) => null, (data) {
          setState(() {
            isAlredyNoted = data.data;
          });
        });
      });
    });
  }

  sendRate(int userId) async {
    Get.dialog(
      const LoaderComponent(),
      barrierDismissible: false,
    );
    inspect(AddRateRequest(
      userId: widget.advice.publicateurId,
      codeService: ServiceCode.advice.toString(),
      note: int.parse(
        rateController.text,
      ),
      noteurId: userId,
      resourceId: widget.advice.id,
    ));
    await repositoryImpl
        .addRate(
      AddRateRequest(
        userId: widget.advice.publicateurId,
        codeService: ServiceCode.advice.toString(),
        note: int.parse(
          rateController.text,
        ),
        noteurId: userId,
        resourceId: widget.advice.id,
        comment: commantaireController.text.isNotEmpty
            ? commantaireController.text
            : null,
      ),
    )
        .then((response) async {
      await response.fold((l) {
        Get.back();
        showCustomFlushbar(
            context, "Echec", ColorManager.error, FlushbarPosition.TOP);
      }, (r) async {
        setState(() {
          isAlredyNoted = true;
        });
        Get.back();
        showCustomFlushbar(context, "Note ajoutée", ColorManager.primary,
            FlushbarPosition.TOP);
      });
    });
  }

  getPublicateurInfo() {
    repositoryImpl
        .getInfoById(InfoByIdRequest(userId: widget.advice.publicateurId))
        .then((response) {
      response.fold((l) => null, (data) {
        setState(() {
          publicateur = "${data.user!.firstname} ${data.user!.laststname}";
        });
      });
    });
  }

  late VideoPlayerController _controllerVideo;

  @override
  void dispose() {
    if (findVideo == true) {
      if (_controllerVideo.value.isInitialized) {
        _controllerVideo.dispose();
      }
    }
    _focusNode.dispose();
    audioPlayer.dispose();
    super.dispose();
  }

  @override
  void initState() {
    getPublicateurInfo();
    checkIfIsAlredyNoted();
    getMedia();
    getDeatilsAdive();
    super.initState();
  }

  getDeatilsAdive() {
    adviceUseCase.detailsAdvice(widget.advice.id);
  }

  Future setAudio() async {
    audioPlayer.setReleaseMode(ReleaseMode.loop);
  }

  getAudio(String linkAudio) {
    audioPlayer = AudioPlayer(playerId: widget.advice.id.toString());
    audioPlayer.setSourceUrl(linkAudio).then((value) {
      audioPlayer.onPlayerStateChanged.listen((event) {
        if (mounted) {
          setState(() {
            isPlaying = event == PlayerState.playing;
          });
        }
      });
      audioPlayer.onDurationChanged.listen((newDuration) {
        if (mounted) {
          setState(() {
            duration = newDuration;
          });
        }
      });
      audioPlayer.onPositionChanged.listen((newposition) {
        if (mounted) {
          setState(() {
            position = newposition;
          });
        }
      });
      setState(() {
        isPlaying = false;
      });
    });
  }

  readerVideo(String linkVideo) {
    setState(() {
      findVideo = true;
    });
    _controllerVideo = VideoPlayerController.network(
      // widget.file?.fileLink ??
      linkVideo,
    )..initialize().then((_) {
        if (mounted) {
          setState(() {
            loadVideo = false;
            timeVideo = formatPlayTime(
                Duration(seconds: _controllerVideo.value.duration.inSeconds));
          });
        }

        _controllerVideo.pause();
      });
  }

  getMedia() {
    for (var element in widget.advice.files) {
      if (element.fileLink != "") {
        if (element.typeFile == CodeConfig.videoCode) {
          readerVideo(element.fileLink);
        }
        if (element.typeFile == CodeConfig.audioCode) {
          getAudio(element.fileLink);
        }
      }
    }
  }

  Container categoryButton(String titre) {
    return Container(
      margin: const EdgeInsets.only(
        left: 5,
        bottom: 5,
      ),
      padding: const EdgeInsets.all(5),
      decoration: BoxDecoration(
          color: ColorManager.lightGrey,
          borderRadius: const BorderRadius.all(Radius.circular(10))),
      child: Text(
        titre,
        style: getRegularTextStyle(color: ColorManager.black),
      ),
    );
  }

  ListTile documentCard({FileModel? file}) {
    return ListTile(
      contentPadding: const EdgeInsets.all(10),
      onTap: () {
        Navigator.push(
          context,
          PageTransition(
            type: PageTransitionType.rightToLeft,
            child: DetailsDocConseilsView(
              file: file,
            ),
            isIos: true,
            duration: const Duration(milliseconds: 400),
          ),
        );
      },
      tileColor: ColorManager.grey.withOpacity(0.2),
      leading: Image.asset(
        ImageAssets.lectureDoc,
      ),
      title: const Text("Voir le fichier"),
      trailing: const Icon(IconManager.arrowRight),
    );
    // Container(
    //   decoration: const BoxDecoration(),
    //   width: double.infinity,
    //   // height: AppSize.s50,
    //   child: Column(children: [
    //     Image.asset(
    //       image ?? ImageAssets.lectureDoc,
    //       width: 150,
    //     )
    //   ]),
    // );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
      builder: (context, user, child) => Scaffold(
        appBar: AppBar(
          leading: const BackButtonCustom(),
          title: const Text('Détails du conseil'),
          actions: [
            if (isAlredyNoted == false && user.isConnected == true)
              TextButton(
                onPressed: () {
                  rateBottomSheet(user.id!);
                },
                child: const Text("Noté"),
              )
          ],
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.all(AppMargin.m10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.advice.title.toUpperCase(),
                  style: getBoldTextStyle(
                    color: ColorManager.black,
                    fontSize: FontSize.s15,
                  ),
                  maxLines: 3,
                ),
                const SizedBox(
                  height: AppSize.s10,
                ),
                Row(
                  children: [
                    Text(
                      publicateur,
                      style: getRegularTextStyle(
                        color: ColorManager.blue,
                        fontSize: FontSize.s12,
                      ),
                    ),
                    Text(
                      " , le ${convertDate(widget.advice.createDate)}",
                      style: getRegularTextStyle(
                        color: ColorManager.grey,
                        fontSize: FontSize.s12,
                      ),
                    ),
                    const Spacer(),
                    RatingBar.builder(
                      initialRating: widget.advice.note,
                      minRating: 0,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemSize: 15,
                      itemPadding: const EdgeInsets.symmetric(horizontal: 1),
                      itemBuilder: (context, _) => const Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      onRatingUpdate: (rating) {},
                    ),
                  ],
                ),
                const SizedBox(
                  height: AppSize.s10,
                ),
                if (widget.advice.illustration.isNotEmpty)
                  SizedBox(
                    height: 200,
                    width: MediaQuery.of(context).size.width,
                    child: CachedNetworkImage(
                      imageUrl: widget.advice.illustration,
                      imageBuilder: (context, imageProvider) => Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            scale: 2,
                            image: imageProvider,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      placeholder: (context, url) => Container(
                        width: double.infinity,
                        height: 150,
                        color: ColorManager.grey,
                      ),
                      errorWidget: (context, url, error) =>
                          const Icon(Icons.error),
                      width: 150,
                    ),
                  ),
                const SizedBox(
                  height: AppSize.s10,
                ),
                Wrap(
                  children: [
                    for (ObjectModel category in widget.advice.categorys) ...[
                      categoryButton(
                        category.label,
                      ),
                    ],
                  ],
                ),
                const SizedBox(
                  height: AppSize.s20,
                ),
                Text(
                  widget.advice.description,
                  style: getRegularTextStyle(
                    color: ColorManager.black,
                    fontSize: FontSize.s14,
                  ),
                ),
                const SizedBox(
                  height: AppSize.s20,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: List.generate(widget.advice.files.length, (index) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Divider(),
                        Text(
                          widget.advice.files[index].title.toUpperCase(),
                          style: getMeduimTextStyle(
                              color: ColorManager.black,
                              fontSize: FontSize.s15),
                        ),
                        const SizedBox(
                          height: AppSize.s10,
                        ),
                        Text(
                          widget.advice.files[index].description,
                          style: getRegularTextStyle(
                            color: ColorManager.black,
                            fontSize: FontSize.s14,
                          ),
                        ),
                        if (widget.advice.files[index].typeFile ==
                            CodeConfig.videoCode) ...[
                          const SizedBox(
                            height: AppSize.s10,
                          ),
                          Center(
                            child: _controllerVideo.value.isInitialized
                                ? GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        audioPlayer.pause();
                                        _controllerVideo.value.isPlaying
                                            ? _controllerVideo.pause()
                                            : _controllerVideo.play();
                                      });
                                    },
                                    child: Stack(
                                      alignment: AlignmentDirectional.center,
                                      children: [
                                        SizedBox(
                                          height: 200,
                                          child: AspectRatio(
                                            aspectRatio: _controllerVideo
                                                .value.aspectRatio,
                                            child:
                                                VideoPlayer(_controllerVideo),
                                          ),
                                        ),
                                        if (!_controllerVideo.value.isPlaying)
                                          CircleAvatar(
                                            backgroundColor:
                                                Colors.black.withOpacity(0.4),
                                            child: Center(
                                              child: Icon(
                                                IconManager.play,
                                                size: AppSize.s30,
                                                color: ColorManager.white,
                                              ),
                                            ),
                                          )
                                      ],
                                    ),
                                  )
                                : Container(
                                    height: 200,
                                    width: double.infinity,
                                    color: ColorManager.black,
                                  ),
                          ),
                          VideoProgressIndicator(
                              _controllerVideo, //video player controller
                              allowScrubbing: true,
                              colors: VideoProgressColors(
                                //video player progress bar
                                backgroundColor: ColorManager.grey,
                                playedColor: ColorManager.red,
                                bufferedColor: ColorManager.grey1,
                              )),
                          const SizedBox(
                            height: AppSize.s20,
                          ),
                        ],
                        if (widget.advice.files[index].typeFile ==
                            CodeConfig.pdfCode) ...[
                          const SizedBox(
                            height: AppSize.s10,
                          ),
                          GestureDetector(
                            onTap: () {},
                            child:
                                documentCard(file: widget.advice.files[index]),
                          ),
                          const SizedBox(
                            height: AppSize.s20,
                          ),
                        ],
                        if (widget.advice.files[index].typeFile ==
                            CodeConfig.audioCode) ...[
                          Card(
                            child: AppAudio(
                              audio: AudioModel(
                                  link: widget.advice.files[index].fileLink),
                            ),
                          ),
                          const SizedBox(
                            height: AppSize.s10,
                          ),
                        ],
                      ],
                    );
                  }),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  rateBottomSheet(int userId) {
    Get.bottomSheet(
      isScrollControlled: false,
      Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10)),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Entrer votre note",
                style: getBoldTextStyle(color: Colors.black, fontSize: 20),
              ),
              RatingBar.builder(
                initialRating: 0,
                minRating: 0,
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 5,
                itemSize: 50,
                itemPadding: const EdgeInsets.symmetric(horizontal: 1),
                itemBuilder: (context, _) => const Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                onRatingUpdate: (rating) {
                  rateController.text = (rating.toInt()).toString();
                },
              ),
              const SizedBox(
                height: 30,
              ),
              Text(
                "Entrer un commentaire",
                style: getRegularTextStyle(color: Colors.black, fontSize: 12),
              ),
              const SizedBox(
                height: 10,
              ),
              TextField(
                controller: commantaireController,
                maxLines: null,
                focusNode: _focusNode,
                decoration: InputDecoration(
                  // hintText:"La ",
                  label: Text(
                    "Commentaire",
                    style: getRegularTextStyle(color: Colors.grey),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 100,
                    child: DefaultButton(
                      color: ColorManager.grey,
                      text: "Annuler",
                      press: () {
                        Get.back();
                      },
                    ),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  SizedBox(
                    width: 100,
                    child: DefaultButton(
                      text: "Envoyer",
                      press: () async {
                        Get.back();
                        sendRate(userId);
                      },
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
