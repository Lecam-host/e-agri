import 'package:flutter/material.dart';

import '../common/buttons/back_button.dart';
import '../ressources/color_manager.dart';
import '../ressources/strings_manager.dart';
import '../ressources/styles_manager.dart';
import 'all_advice_view.dart';
import 'components/app_bar_actions.dart';

class ConseilView extends StatefulWidget {
  const ConseilView({Key? key, this.titre}) : super(key: key);
  final String? titre;
  @override
  State<ConseilView> createState() => _ConseilViewState();
}

class _ConseilViewState extends State<ConseilView> {
  String dropdownvalue = "";
  List<Widget> tabs = [
    Tab(
        child: Text(
      AppStrings.all,
      style: getSemiBoldTextStyle(color: ColorManager.black),
    )),
    Tab(
        child: Text(
      AppStrings.video,
      style: getSemiBoldTextStyle(color: ColorManager.black),
    )),
    Tab(
        child: Text(
      AppStrings.document,
      style: getSemiBoldTextStyle(color: ColorManager.black),
    )),
    Tab(
        child: Text(
      AppStrings.audio,
      style: getSemiBoldTextStyle(color: ColorManager.black),
    )),
  ];
  // List of items in our dropdown menu
  var items = [
    AppStrings.all,
    AppStrings.breeding,
    AppStrings.agricultural,
  ];
  @override
  void initState() {
    super.initState();
    dropdownvalue = AppStrings.all;
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: tabs.length,
      child: Scaffold(
        body: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return [
              SliverAppBar(
                leading: const BackButtonCustom(),
                title: Text(
                  "${AppStrings.conseils} ${widget.titre}",
                ),
                actions: actions(context),
                elevation: 0.0,
                pinned: true,
                floating: true,
              ),
            ];
          },
          body: const Column(
            children: [
              Expanded(child: AllAdviceView()),
              // Expanded(
              //   child: TabBarView(
              //     children: [
              //       // ListConseilVideo(),
              //       // ListConseilDoc(),
              //       // ListConseilAudio(),
              //     ],
              //   ),
              // ),
            ],
          ),
          // body: SafeArea(
          //   child: Column(
          //     crossAxisAlignment: CrossAxisAlignment.start,
          //     children: [
          //       const SizedBox(
          //         height: 10,
          //       ),
          //       SingleChildScrollView(
          //         scrollDirection: Axis.horizontal,
          //         child: Row(
          //           children: const [
          //             FilterButtonComponent(
          //               titre: AppStrings.all,
          //               isActif: true,
          //             ),
          //             FilterButtonComponent(
          //               titre: AppStrings.video,
          //             ),
          //             FilterButtonComponent(
          //               titre: AppStrings.audio,
          //             ),
          //             FilterButtonComponent(
          //               titre: AppStrings.document,
          //             ),
          //           ],
          //         ),
          //       ),
          //       const ConseilVideoCardComponenent(),
          //       const ConseilVideoCardComponenent(),
          //       const ConseilVideoCardComponenent(),
          //       const ConseilVideoCardComponenent(),
          //     ],
          //   ),
          // ),
        ),
      ),
    );
  }
}
