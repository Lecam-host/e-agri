import 'package:eagri/app/functions.dart';
import 'package:eagri/presentations/common/rate/controller/rate_controller.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';

class ListProductComments extends StatelessWidget {
  const ListProductComments({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    RateController rateController = Get.put(RateController());
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ListView.builder(
          physics: const NeverScrollableScrollPhysics(),
          padding: const EdgeInsets.only(bottom: 10),
          shrinkWrap: true,
          itemCount: rateController.listComments.length,
          itemBuilder: (context, index) {
            return Container(
              margin: const EdgeInsets.only(top: 10),
              padding: const EdgeInsets.all(10),
              decoration: const BoxDecoration(
                  color: Color.fromARGB(255, 239, 239, 239)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      RatingBar.builder(
                        initialRating:
                            rateController.listComments[index].note.toDouble(),
                        minRating: 0,
                        direction: Axis.horizontal,
                        allowHalfRating: true,
                        itemCount: 5,
                        itemSize: 13,
                        itemPadding: const EdgeInsets.symmetric(horizontal: 1),
                        itemBuilder: (context, _) => const Icon(
                          Icons.star,
                          color: Colors.amber,
                        ),
                        onRatingUpdate: (rating) {
                          rateController.rating.value = rating.toInt();
                        },
                      ),
                      const Spacer(),
                      Text(
                        convertDateWithTime(rateController
                            .listComments[index].createdAt
                            .toString()),
                        style: getSemiBoldTextStyle(color: ColorManager.grey),
                      ),
                    ],
                  ),
                  Text(rateController.listComments[index].comment),
                  Text(
                    "Posté par ${rateController.listComments[index].user == null ? "Aucun" : rateController.listComments[index].user!.firstName}",
                    style: getSemiBoldTextStyle(color: ColorManager.grey),
                  ),
                ],
              ),
            );
          },
        ),
      ],
    );
  }
}
