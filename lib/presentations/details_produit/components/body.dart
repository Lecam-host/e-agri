import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/presentations/details_produit/components/product_description.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../app/di.dart';
import '../../../data/repository/api/repository_impl.dart';
import '../../../domain/model/Product.dart';
import '../../../domain/model/user_model.dart';
import '../../ressources/color_manager.dart';
import 'product_images.dart';

class Body extends StatefulWidget {
  final ProductModel product;

  const Body({Key? key, required this.product, this.isCatalogueProduct=false}) : super(key: key);
final bool isCatalogueProduct;
  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  late ProductModel productShow;
  getDetailsProduct() async {
    if (mounted) {
      await repositoryImpl
          .getDetailProduct(widget.product.id)
          .then((response) async {
        await response.fold((failure) {}, (data) async {
          productShow = (await data)!;
        });
        if (mounted) {
          setState(() {
            productShow;
          });
        }
      });
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
      builder: (context, user, child) => Stack(
        children: [
          ProductImages(product: widget.product,isCatalogueProduct: widget.isCatalogueProduct,),
          Container(
            margin:
                EdgeInsets.only(top: MediaQuery.of(context).size.height / 2.2),
            child: Column(
              children: [
                ProductDescription(
                  product: widget.product,
                  pressOnSeeMore: () {},
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 100,
          )
        ],
      ),
    );
  }

  showCustomFlushbar(BuildContext ctx, bool isExist, String message) {
    return Flushbar(
      flushbarPosition: FlushbarPosition.TOP,
      duration: const Duration(seconds: 3),
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.all(10),
      borderRadius: BorderRadius.circular(15),
      backgroundColor:
          isExist == true ? ColorManager.blue : ColorManager.primary,
      icon: Icon(Icons.info, color: ColorManager.grey),
      boxShadows: const [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],
      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      // title: '',
      message: message,
    ).show(ctx);
  }
}
