import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';

import '../../ressources/color_manager.dart';

Shimmer loadDetailProduct() {
  double width = MediaQuery.of(Get.context!).size.width;
  BorderRadius borderRadius = BorderRadius.circular(5);
  return Shimmer.fromColors(
    baseColor: const Color.fromARGB(255, 183, 182, 182),
    highlightColor: const Color.fromARGB(255, 214, 213, 213),
    child: Container(
      margin: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                margin: const EdgeInsets.only(bottom: 10, left: 10),
                width: width / 2.5,
                height: 15,
                decoration: BoxDecoration(
                  borderRadius: borderRadius,
                  color: ColorManager.white,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 10, left: 10),
                width: width / 6,
                height: 15,
                decoration: BoxDecoration(
                  borderRadius: borderRadius,
                  color: ColorManager.white,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                margin: const EdgeInsets.only(bottom: 10, left: 10),
                width: width / 6,
                height: 15,
                decoration: BoxDecoration(
                  borderRadius: borderRadius,
                  color: ColorManager.white,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 10, left: 10),
                width: width / 6,
                height: 15,
                decoration: BoxDecoration(
                  borderRadius: borderRadius,
                  color: ColorManager.white,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 10, left: 10),
                width: width / 6,
                height: 15,
                decoration: BoxDecoration(
                  borderRadius: borderRadius,
                  color: ColorManager.white,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 10, left: 10),
                width: width / 6,
                height: 15,
                decoration: BoxDecoration(
                  borderRadius: borderRadius,
                  color: ColorManager.white,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 10, left: 10),
            width: width / 1.3,
            height: 15,
            decoration: BoxDecoration(
              borderRadius: borderRadius,
              color: ColorManager.white,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 10, left: 10),
            width: width / 5,
            height: 15,
            decoration: BoxDecoration(
              borderRadius: borderRadius,
              color: ColorManager.white,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 10, left: 10),
            width: width / 1.5,
            height: 15,
            decoration: BoxDecoration(
              borderRadius: borderRadius,
              color: ColorManager.white,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 10, left: 10),
            width: width / 1.1,
            height: 15,
            decoration: BoxDecoration(
              borderRadius: borderRadius,
              color: ColorManager.white,
            ),
          ),
          const SizedBox(
            height: 15,
          ),
        ],
      ),
    ),
  );
}
