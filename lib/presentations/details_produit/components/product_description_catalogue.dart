import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';

import '../../../app/constant.dart';
import '../../../app/functions.dart';
import '../../../domain/model/Product.dart';
import '../../ressources/strings_manager.dart';

class ProductDescriptionSelft extends StatelessWidget {
  const ProductDescriptionSelft({
    Key? key,
    required this.product,
    this.pressOnSeeMore,
  }) : super(key: key);

  final ProductModel product;
  final GestureTapCallback? pressOnSeeMore;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            height: 10,
          ),
          Text(product.title,
              style: getBoldTextStyle(color: ColorManager.black, fontSize: 19)),
          Text("${product.price} ${AppStrings.moneyDevise} / ${product.unite}",
              style: getBoldTextStyle(color: Colors.orange, fontSize: 18)),
          const SizedBox(
            height: 10,
          ),
          if (product.dataDisponibilite != null)
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Date Disponibilité ",
                  style:
                      getSemiBoldTextStyle(color: Colors.black, fontSize: 14),
                ),
                Text("${convertDate(product.dataDisponibilite!)} ",
                    style:
                        getRegularTextStyle(color: Colors.black, fontSize: 12)),
                const SizedBox(
                  height: 10,
                ),
              ],
            ),
          if (product.code != codePrestation.toString())
            Text("Qte :  ${product.quantity}  ${product.unite}",
                style: getBoldTextStyle(color: Colors.black, fontSize: 12)),
          const SizedBox(
            height: 10,
          ),
          if (product.location != null)
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Lieu  ",
                    style: getBoldTextStyle(color: Colors.black, fontSize: 12)),
                Text("Region :  ${product.location!.regionName} ",
                    style:
                        getRegularTextStyle(color: Colors.black, fontSize: 12)),
                if (product.location!.departementName != null)
                  Text("Département :  ${product.location!.departementName} ",
                      style: getRegularTextStyle(
                          color: Colors.black, fontSize: 12)),
                if (product.location!.sPrefectureName != null)
                  Text(
                      "Sous-prefecture :  ${product.location!.sPrefectureName} ",
                      style: getRegularTextStyle(
                          color: Colors.black, fontSize: 12)),
                if (product.location!.localiteName != null)
                  Text("Localité :  ${product.location!.localiteName} ",
                      style: getRegularTextStyle(
                          color: Colors.black, fontSize: 12)),
              ],
            ),
          const SizedBox(
            height: 10,
          ),
          if (product.description != "") ...[
            Text(
              "Description ",
              style: getSemiBoldTextStyle(color: Colors.black, fontSize: 14),
            ),
            Text(
              product.description!,
              maxLines: 3,
            ),
            const SizedBox(
              height: 10,
            ),
          ],
          Text(
            "Date de publication",
            style: getSemiBoldTextStyle(color: Colors.black, fontSize: 14),
          ),
          Text(
            "${convertDate(product.publicationDate)} ",
            style: getRegularTextStyle(color: Colors.black, fontSize: 12),
          ),
          const SizedBox(
            height: 30,
          ),
          if (product.status == "DELETED")
            Center(
              child: Text(
                'Ce produit a été rétiré',
                style:
                    getSemiBoldTextStyle(color: ColorManager.red, fontSize: 15),
              ),
            ),
        ],
      ),
    );
  }
}
