import 'package:eagri/app/constant.dart';
import 'package:eagri/domain/model/Product.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../catalogue/controllers/catalogue_controller.dart';
import '../../common/buttons/cart_bottom_component.dart';
import '../../ressources/size_config.dart';

class CustomAppBar extends StatelessWidget {
  final ProductModel product;
  final bool isCatalogueProduct;
  const CustomAppBar({
    Key? key,
    required this.product,
    this.isCatalogueProduct = false,
  }) : super(key: key);

  // AppBar().preferredSize.height provide us the height that appy on our app bar
  Size get preferredSize => Size.fromHeight(AppBar().preferredSize.height);

  @override
  Widget build(BuildContext context) {
    CatalogueController catalogueController = Get.put(CatalogueController());

    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: getProportionateScreenWidth(20), vertical: 30),
      child: Row(
        children: [
          SizedBox(
            height: getProportionateScreenWidth(40),
            width: getProportionateScreenWidth(40),
            child: TextButton(
              style: TextButton.styleFrom(
                foregroundColor: kPrimaryColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(60),
                ),
                backgroundColor: Colors.white,
                padding: EdgeInsets.zero,
              ),
              onPressed: () => Navigator.pop(context),
              child: SvgPicture.asset(
                "assets/icons/Back ICon.svg",
                height: 15,
              ),
            ),
          ),
          const Spacer(),
          if (product.code == codeProductAgricultural.toString())
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 10),
              decoration: BoxDecoration(
                color: ColorManager.white,
                borderRadius: BorderRadius.circular(14),
              ),
              child: Row(
                children: [
                  Text(
                      "${product.typeOffers.toLowerCase()} en ${product.typeVente == "BLOC" ? "gros" : "détails"}",
                      style: getBoldTextStyle(
                          color: product.typeVente == "BLOC"
                              ? ColorManager.red
                              : ColorManager.primary)),
                  const SizedBox(width: 5),
                ],
              ),
            ),
          // const SizedBox(width: 5),
          // Container(
          //   padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 5),
          //   decoration: BoxDecoration(
          //     color: Colors.white,
          //     borderRadius: BorderRadius.circular(14),
          //   ),
          //   child: Row(
          //     children: [
          //       Text(
          //         "${product.rating}",
          //         style: const TextStyle(
          //           fontSize: 14,
          //           fontWeight: FontWeight.w600,
          //         ),
          //       ),
          //       const SizedBox(width: 5),
          //       SvgPicture.asset("assets/icons/Star Icon.svg"),
          //     ],
          //   ),
          // ),
          const SizedBox(width: 5),
          if (isCatalogueProduct == false) CartButtomComponent(),
          if (isCatalogueProduct == true)
            GestureDetector(
              onTap: () {
                catalogueController.deleteProduct(product);
              },
              child: Container(
                padding: const EdgeInsets.all(10),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.circle,
                ),
                child: Icon(
                  Icons.delete,
                  color: ColorManager.red,
                ),
              ),
            ),
        ],
      ),
    );
  }
}
