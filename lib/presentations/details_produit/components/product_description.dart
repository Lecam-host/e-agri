import 'package:eagri/app/constant.dart';
import 'package:eagri/presentations/common/rate/controller/rate_controller.dart';
import 'package:eagri/presentations/details_produit/components/product_simillaire.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:eagri/presentations/shop/controller/shop_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import '../../../app/functions.dart';
import '../../../domain/model/Product.dart';
import '../../ressources/assets_manager.dart';
import '../list_product_comment.dart';

class ProductDescription extends StatefulWidget {
  const ProductDescription({
    Key? key,
    required this.product,
    this.pressOnSeeMore,
  }) : super(key: key);

  final ProductModel product;
  final GestureTapCallback? pressOnSeeMore;

  @override
  State<ProductDescription> createState() => _ProductDescriptionState();
}

class _ProductDescriptionState extends State<ProductDescription> {
  int currentIndex = 0;
  ShopController shopController = Get.find();
  RateController rateController = Get.put(RateController());
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: const Color.fromARGB(255, 214, 214, 214).withOpacity(0.2),
            spreadRadius: 2,
            blurRadius: 2,
            offset: const Offset(0, -3), // décale l'ombre vers le haut
          ),
        ],
        borderRadius: const BorderRadius.vertical(top: Radius.circular(20)),
        color: ColorManager.white,
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.product.title,
                        style: getSemiBoldTextStyle(
                            fontSize: 18, color: ColorManager.primary),
                      ),
                      if( widget.product.identifiant!=null)...[
                      const  SizedBox(height: 5,),
                          Text(
                        widget.product.identifiant!,
                        style: getSemiBoldTextStyle(
                            fontSize: 12, color: ColorManager.darkGrey),
                      ),
                      ],
                     

                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          if (widget.product.user != null)
                            Text(
                                "${widget.product.user!.lastName} ${widget.product.user!.firstName} ",
                                style: getRegularTextStyle(
                                  color: ColorManager.grey,
                                  fontSize: 10,
                                )),
                          Text(
                            "| ${convertDate(widget.product.publicationDate)} ",
                            style: getRegularTextStyle(
                              fontSize: 10,
                              color: ColorManager.grey,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  width: 70,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 14, vertical: 5),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(14),
                  ),
                  child: Row(
                    children: [
                      SvgPicture.asset("assets/icons/Star Icon.svg"),
                      const SizedBox(width: 5),
                      Text(
                        "${widget.product.rating}",
                        style: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  cardDes("Description", 0),
                  if (widget.product.code == codeIntrant.toString())
                    cardDes("Cultures appropriées", 1),
                  cardDes("Produits similaires", 2),
                  if (widget.product.code != codeTransport.toString())
                    cardDes("Avis des clients", 3),
                  if (widget.product.code == codeTransport.toString())
                    cardDes("Trajet", 4),
                ],
              ),
            ),
            const SizedBox(height: 10),
            if (currentIndex == 0) ...[
            if  ( widget.product.code != codePrestation.toString())
              RichText(
                text: TextSpan(
                  text: widget.product.code == codeTransport.toString()
                      ? "Capacité : "
                      : "Quantité : ",
                  style: getRegularTextStyle(color: ColorManager.grey),
                  children: [
                    TextSpan(
                      text:
                          "${widget.product.quantity}  ${widget.product.unite}",
                      style: getSemiBoldTextStyle(fontSize: 12),
                    )
                  ],
                ),
              ),
             
              if( widget.product.paymentMethod!=null)...[
                 const SizedBox(
                height: 10,
              ),
               RichText(
                text: TextSpan(
                  text: "Methode de paiement : ",
                  style: getRegularTextStyle(color: ColorManager.grey),
                  children: [
                    TextSpan(
                      text: widget.product.paymentMethod!.name!.toLowerCase(),
                      style: getSemiBoldTextStyle(fontSize: 12),
                    )
                  ],
                ),
              ),
              ],
             
             
              if (widget.product.code != codeTransport.toString())...[
            const SizedBox(height: 10),
             RichText(
                  text: TextSpan(
                    text: "Localisation : ",
                    style: getRegularTextStyle(
                        color: ColorManager.grey, fontSize: 12),
                    children: [
                      TextSpan(
                        text: widget.product.location!.regionName != null
                            ? "${widget.product.location!.regionName}"
                            : "[Region]",
                        style: getSemiBoldTextStyle(fontSize: 12),
                      ),
                      TextSpan(
                        text: widget.product.location!.sPrefectureName != null
                            ? " ,${widget.product.location!.departementName} "
                            : "",
                        style: getSemiBoldTextStyle(fontSize: 12),
                      ),
                      TextSpan(
                        text: widget.product.location!.sPrefectureName != null
                            ? " ,${widget.product.location!.sPrefectureName}"
                            : "",
                        style: getSemiBoldTextStyle(fontSize: 12),
                      ),
                      TextSpan(
                        text: widget.product.location!.localiteName != null
                            ? " ,${widget.product.location!.localiteName}"
                            : "",
                        style: getSemiBoldTextStyle(fontSize: 12),
                      ),
                    ],
                  ),
                ),

              ],
               
              const SizedBox(height: 10),
              if (widget.product.description != null)
                RichText(
                  text: TextSpan(
                    text: "Commentaire : ",
                    style: getRegularTextStyle(color: ColorManager.grey),
                    children: [
                      TextSpan(
                        text: widget.product.description!
                            .toLowerCase()
                            .toString(),
                        style: getRegularTextStyle(fontSize: 12),
                      )
                    ],
                  ),
                ),
            ],
            if (currentIndex == 1) ...[
              if (widget.product.code == codeIntrant.toString())
                if (widget.product.itrantInfo != null)
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Cultures appropriées",
                        style: getSemiBoldTextStyle(
                          color: ColorManager.black,
                          fontSize: 14,
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ...List.generate(
                            widget.product.itrantInfo!.typeCultures.length,
                            (index) {
                              return Text(
                                " - ${widget.product.itrantInfo!.typeCultures[index].libelle}",
                                maxLines: 3,
                              );
                              // here by default width and height is 0
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
            ],
            if (currentIndex == 2) ...[
              Obx(() => shopController.similarProduct.isNotEmpty
                  ? Column(
                      children: [
                        const Divider(),
                        ProductSimilaire(
                          similarProducts: shopController.similarProduct,
                        ),
                      ],
                    )
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                          child: Image.asset(
                            IconAssetManager.emptyIcon,
                            scale: 0.5,
                          ),
                        ),
                        const Text('Aucun produit similiare trouvé')
                      ],
                    )),
            ],
            if (currentIndex == 3) ...[
              Obx(() => rateController.listComments.isNotEmpty
                  ? const ListProductComments()
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                          child: Image.asset(
                            IconAssetManager.commentIcon,
                            scale: 1,
                          ),
                        ),
                        const Text('Aucun avis sur ce produit')
                      ],
                    )),
            ],
            if (currentIndex == 4) ...[
              if (widget.product.code == codeTransport.toString()) ...[
                if (widget.product.startTrajetInfo != null)
                  RichText(
                    text: TextSpan(
                      text: "Départ : ",
                      style: getRegularTextStyle(
                          color: ColorManager.grey, fontSize: 12),
                      children: [
                        TextSpan(
                          text: widget.product.startTrajetInfo!.region,
                          style: getSemiBoldTextStyle(fontSize: 12),
                        ),
                        if (widget.product.startTrajetInfo!.departement != null)
                          TextSpan(
                            text:
                                ", ${widget.product.startTrajetInfo!.departement}",
                            style: getSemiBoldTextStyle(fontSize: 12),
                          ),
                        if (widget.product.startTrajetInfo!.sprefecture != null)
                          TextSpan(
                            text:
                                ", ${widget.product.startTrajetInfo!.sprefecture}",
                            style: getSemiBoldTextStyle(fontSize: 12),
                          ),
                        if (widget.product.startTrajetInfo!.localite != null)
                          TextSpan(
                            text:
                                ", ${widget.product.startTrajetInfo!.localite}",
                            style: getSemiBoldTextStyle(fontSize: 12),
                          ),
                      ],
                    ),
                  ),
                const SizedBox(
                  height: 10,
                ),
                if (widget.product.arrivalTrajetInfo != null)
                  RichText(
                    text: TextSpan(
                      text: "Arrivé : ",
                      style: getRegularTextStyle(
                          color: ColorManager.grey, fontSize: 12),
                      children: [
                        TextSpan(
                          text: widget.product.arrivalTrajetInfo!.region,
                          style: getSemiBoldTextStyle(fontSize: 12),
                        ),
                        if (widget.product.arrivalTrajetInfo!.departement !=
                            null)
                          TextSpan(
                            text:
                                ", ${widget.product.arrivalTrajetInfo!.departement}",
                            style: getSemiBoldTextStyle(fontSize: 12),
                          ),
                        if (widget.product.arrivalTrajetInfo!.sprefecture !=
                            null)
                          TextSpan(
                            text:
                                ", ${widget.product.arrivalTrajetInfo!.sprefecture}",
                            style: getSemiBoldTextStyle(fontSize: 12),
                          ),
                        if (widget.product.arrivalTrajetInfo!.localite != null)
                          TextSpan(
                            text:
                                ", ${widget.product.arrivalTrajetInfo!.localite}",
                            style: getSemiBoldTextStyle(fontSize: 12),
                          ),
                      ],
                    ),
                  ),
              ],
            ]
          ],
        ),
      ),
    );
  }

  GestureDetector cardDes(String titre, int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          currentIndex = index;
        });
      },
      child: Container(
        //  padding: const EdgeInsets.only(right: 20),
        margin: const EdgeInsets.only(right: 20),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: currentIndex == index
                  ? ColorManager.jaune
                  : ColorManager.lightGrey,
              width: 2.0,
            ),
          ),
        ),
        child: Text(
          titre,
          style: currentIndex == index
              ? getSemiBoldTextStyle(color: ColorManager.black, fontSize: 12)
              : getRegularTextStyle(color: ColorManager.grey, fontSize: 12),
        ),
      ),
    );
  }
}
