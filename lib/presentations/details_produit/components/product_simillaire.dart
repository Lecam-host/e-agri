import 'package:flutter/material.dart';

import '../../../domain/model/Product.dart';
import '../../common/product_card.dart';

class ProductSimilaire extends StatelessWidget {
  const ProductSimilaire({Key? key, required this.similarProducts})
      : super(key: key);
  final List<ProductModel> similarProducts;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GridView.builder(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: similarProducts.length,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisSpacing: 10,
            crossAxisSpacing: 10,
            childAspectRatio: 1,
            mainAxisExtent: 300,
          ),
          itemBuilder: (BuildContext context, int index) {
            return ProductCard(data: similarProducts[index]);
          },
        ),
      ],
    );
  }
}
