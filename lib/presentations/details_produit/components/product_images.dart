import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import '../../../app/constant.dart';
import '../../../domain/model/Product.dart';
import '../../ressources/size_config.dart';
import 'custom_details_app_bar.dart';

class ProductImages extends StatefulWidget {
  const ProductImages({
    Key? key,
    required this.product,
    this.isCatalogueProduct=false,
  }) : super(key: key);

  final ProductModel product;
  final bool isCatalogueProduct ;
  @override
  ProductImagesState createState() => ProductImagesState();
}

class ProductImagesState extends State<ProductImages> {
  int selectedImage = 0;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        
        Stack(children: [
           if (widget.product.images!.isNotEmpty) ...[
          Stack(
            children: [
              SizedBox(
                // width: getProportionateScreenWidth(238),
                // height: MediaQuery.of(context).size.height / 2.2,
                child: AspectRatio(
                  aspectRatio: 1,
                  child: CachedNetworkImage(
                    imageUrl: widget.product.images![selectedImage],
                    imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                        // borderRadius: BorderRadius.circular(15),
                        image: DecorationImage(
                          scale: 1,
                          image: imageProvider,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    placeholder: (context, url) => SizedBox(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                    ),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                  ),
                ),
              ),
              
              Positioned(
              bottom: 20,
              right: 0,
              left: 0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ...List.generate(
                      widget.product.images!.length,
                      (index) => buildSmallProductPreview(index),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: getProportionateScreenWidth(20)),
        ],
        if (widget.product.images!.isEmpty) ...[
          SizedBox(
            width: getProportionateScreenWidth(238),
            child: AspectRatio(
              aspectRatio: 1,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                ),
                child: const Icon(Icons.image),
              ),
            ),
          ),
          AnimatedContainer(
            duration: defaultDuration,
            margin: const EdgeInsets.only(right: 15),
            height: getProportionateScreenWidth(48),
            width: getProportionateScreenWidth(48),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.white,
              border: Border.all(color: kPrimaryColor.withOpacity(0)),
            ),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
              ),
              child: const Icon(Icons.image),
            ),
          ),
        ],
        Container(margin:EdgeInsets.only(top: MediaQuery.of(context).size.height/100) ,child:CustomAppBar(product: widget.product,isCatalogueProduct: widget.isCatalogueProduct,) ,)
         

        ],),
       
      ],
    );
  }

  GestureDetector buildSmallProductPreview(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedImage = index;
        });
      },
      child: AnimatedContainer(
        duration: defaultDuration,
        margin: const EdgeInsets.only(right: 15),
        height: getProportionateScreenWidth(48),
        width: getProportionateScreenWidth(48),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          border: Border.all(
              color: kPrimaryColor.withOpacity(selectedImage == index ? 1 : 0)),
        ),
        child: CachedNetworkImage(
          imageUrl: widget.product.images![index],
          imageBuilder: (context, imageProvider) => Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                scale: 1,
                image: imageProvider,
                fit: BoxFit.cover,
              ),
            ),
          ),
          placeholder: (context, url) => SizedBox(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
          ),
          errorWidget: (context, url, error) => const Icon(Icons.error),
        ),
      ),
    );
  }
}
