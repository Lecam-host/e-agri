import 'package:eagri/app/constant.dart';
import 'package:eagri/domain/model/Product.dart';
import 'package:eagri/domain/model/user_model.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

import '../../../app/functions.dart';
import '../../../domain/model/cart_model.dart';
import '../../cart/controller/cart_controller.dart';
import '../../common/default_button.dart';
import '../../ressources/assets_manager.dart';
import '../../ressources/styles_manager.dart';


class AddCartButton extends StatefulWidget {
  const AddCartButton({Key? key, required this.product}) : super(key: key);
  final ProductModel product;

  @override
  State<AddCartButton> createState() => _AddCartButtonState();
}

class _AddCartButtonState extends State<AddCartButton> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  CartController cartController = Get.find();
  late CartItem cartItem;
  bool isLoadAddcart = false;
  bool isLoadChangeQuantity = false;

  @override
  void initState() {
    //cartItem = cartController.getSpecificCartItem(widget.product);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
      builder: (context, user, child) => GetBuilder<CartController>(
        // specify type as Controller
        init: CartController(), // intialize with the Controller
        builder: (value) => widget.product.typeOffers == "VENTE"
            ? user.isConnected != true
                ? const SizedBox()
                : !cartController.isAlredyAdded(widget.product)
                    ? isInScopes(UserPermissions.ADDCART, user.scopes!) == true
                        ? Center(
                            child: Container(
                              margin: const EdgeInsets.all(10),
                              child: Visibility(
                                visible: isLoadAddcart == false,
                                replacement: SizedBox(
                                  height: 50,
                                  width: 50,
                                  child: Lottie.asset(JsonAssets.loader),
                                ),
                                child: DefaultButton(
                                  isActive: !cartController
                                      .isAlredyAdded(widget.product),
                                  text: "Ajouter au panier",
                                  press: () async {
                                    quantityInput(widget.product);
                                  },
                                ),
                              ),
                            ),
                          )
                        : const SizedBox()
                    : widget.product.typeVente == "DETAIL"
                        ? Container(
                            width: double.infinity,
                            padding: const EdgeInsets.all(10),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                if (widget.product.typeVente == "DETAIL")
                                  Visibility(
                                    visible: isLoadChangeQuantity == false,
                                    replacement: SizedBox(
                                      height: 50,
                                      width: 50,
                                      child: Lottie.asset(JsonAssets.loader),
                                    ),
                                    child: CircleAvatar(
                                      backgroundColor: cartController
                                                  .getSpecificCartItem(
                                                      widget.product)
                                                  .quantity ==
                                              1
                                          ? ColorManager.grey
                                          : ColorManager.primary,
                                      maxRadius: 25,
                                      child: Center(
                                          child: IconButton(
                                              onPressed: () async {
                                                setState(() {
                                                  isLoadChangeQuantity = true;
                                                });
                                                await value
                                                    .decreasqty(cartController
                                                        .getSpecificCartItem(
                                                            widget.product))
                                                    .then((value) {
                                                  setState(() {
                                                    isLoadChangeQuantity =
                                                        false;
                                                  });
                                                });
                                              },
                                              icon: Icon(
                                                Icons.remove,
                                                size: 15,
                                                color: ColorManager.white,
                                              ))),
                                    ),
                                  ),
                                const SizedBox(
                                  width: 20,
                                ),
                                if (widget.product.typeVente == "DETAIL")
                                  Text(
                                      " x${cartController.getSpecificCartItem(widget.product).quantity}",
                                      style: getBoldTextStyle(fontSize: 15)),
                                if (widget.product.typeVente == "DETAIL")
                                  const SizedBox(
                                    width: 20,
                                  ),
                                Visibility(
                                  visible: isLoadChangeQuantity == false,
                                  replacement: SizedBox(
                                    height: 50,
                                    width: 50,
                                    child: Lottie.asset(JsonAssets.loader),
                                  ),
                                  child: CircleAvatar(
                                    backgroundColor: cartController
                                                .getSpecificCartItem(
                                                    widget.product)
                                                .quantity <
                                            widget.product.quantity
                                        ? ColorManager.primary
                                        : ColorManager.grey,
                                    maxRadius: 25,
                                    child: Center(
                                        child: IconButton(
                                            onPressed: () async {
                                              setState(() {
                                                isLoadChangeQuantity = true;
                                              });

                                              await value
                                                  .increasQty(cartController
                                                      .getSpecificCartItem(
                                                          widget.product))
                                                  .then((value) {
                                                setState(() {
                                                  isLoadChangeQuantity = false;
                                                });
                                              });
                                            },
                                            icon: Icon(
                                              Icons.add,
                                              size: 15,
                                              color: ColorManager.white,
                                            ))),
                                  ),
                                ),
                              ],
                            ),
                          )
                        : Container(
                            margin: const EdgeInsets.all(10),
                            child: DefaultButton(
                              isActive: false,
                              text: "Déja dans le panier",
                              press: () {},
                            ),
                          )
            : const SizedBox()
      ),
    );
  }

  quantityInput(ProductModel product) {
    if (product.typeVente == "BLOC") {
      cartController.quantityCommand.value.text = product.quantity.toString();
    } else {
      cartController.quantityCommand.value.text = "1";
    }
    Get.bottomSheet(
      
      StatefulBuilder(builder: (cxt, setStateBottomSheet) {
        return Container(
          padding: const EdgeInsets.all(10),
          margin: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(10)),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (product.typeVente == "BLOC") ...[
                Text(
                  'Ce produit est vendu en bloc donc vous ne pouvez pas changer la quantité',
                  style: getRegularTextStyle(),
                  textAlign: TextAlign.center,
                ),
              ] else ...[
                Text(
                  "Entrer la quantité",
                  style:
                      getBoldTextStyle(color: ColorManager.black, fontSize: 14),
                ),
                const SizedBox(
                  height: 10,
                ),
                Form(
                  key: _formKey,
                  child: TextFormField(
                    initialValue: "1",
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Entrer la quantité';
                      } else if (validQuantity(
                            int.parse(value),
                          ) ==
                          false) {
                        return 'Quantité invalide';
                      } else if (int.parse(value) > product.quantity) {
                        return "Quantité indisponible";
                      } else {
                        return null;
                      }
                    },
                    onChanged: (value) {
                      if (_formKey.currentState!.validate()) {
                        setStateBottomSheet(() {
                          cartController.quantityCommand.value.text = value;
                        });
                      }
                    },
                    // controller: cartController.quantityCommand.value,
                    keyboardType: TextInputType.number,

                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.grey.withOpacity(0.2),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15.0),
                        borderSide:
                            const BorderSide(width: 0, style: BorderStyle.none),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15.0),
                        borderSide:
                            const BorderSide(width: 0, style: BorderStyle.none),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15.0),
                        borderSide:
                            const BorderSide(width: 0, style: BorderStyle.none),
                      ),
                      hintText: 'Entrer la quantité',
                    ),
                  ),
                ),
              ],
              const SizedBox(
                height: 20,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RichText(
                    text: TextSpan(
                      text: 'Quantité disponible : ',
                      style: getRegularTextStyle(color: ColorManager.black),
                      children: [
                        if (cartController
                            .quantityCommand.value.text.isNotEmpty)
                          TextSpan(
                            text:
                                "${product.quantity} ${product.unite}",
                            style: getMeduimTextStyle(
                              color: ColorManager.jaune,
                              fontSize: 16,
                            ),
                          ),
                        if (cartController.quantityCommand.value.text.isEmpty)
                          TextSpan(
                            text: "_ _ _",
                            style: getMeduimTextStyle(
                              color: ColorManager.jaune,
                              fontSize: 16,
                            ),
                          ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  RichText(
                    text: TextSpan(
                      text: 'Quantité démandée : ',
                      style: getRegularTextStyle(color: ColorManager.black),
                      children: [
                        if (cartController
                            .quantityCommand.value.text.isNotEmpty)
                          TextSpan(
                            text:
                                "${cartController.quantityCommand.value.text} ${product.unite}",
                            style: getMeduimTextStyle(
                              color: ColorManager.jaune,
                              fontSize: 16,
                            ),
                          ),
                        if (cartController.quantityCommand.value.text.isEmpty)
                          TextSpan(
                            text: "_ _ _",
                            style: getMeduimTextStyle(
                              color: ColorManager.jaune,
                              fontSize: 16,
                            ),
                          ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  RichText(
                    text: TextSpan(
                      text: 'Prix total : ',
                      style: getRegularTextStyle(color: ColorManager.black),
                      children: [
                        if (cartController
                            .quantityCommand.value.text.isNotEmpty)
                          TextSpan(
                            text: " ${separateur((int.parse(
                                  cartController.quantityCommand.value.text,
                                ) * product.price).toDouble())} FCFA",
                            style: getMeduimTextStyle(
                              color: ColorManager.jaune,
                              fontSize: 16,
                            ),
                          ),
                        if (cartController.quantityCommand.value.text.isEmpty)
                          TextSpan(
                            text: "_ _ _",
                            style: getMeduimTextStyle(
                              color: ColorManager.jaune,
                              fontSize: 16,
                            ),
                          ),
                      ],
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              SizedBox(
                width: 150,
                child: DefaultButton(
                  text: "Ajouter",
                  press: () async {
                    if (product.typeVente == "BLOC") {
                      Get.back();

                      setState(() {
                        isLoadAddcart = true;
                      });

                      if (cartController.isAlredyAdded(widget.product) ==
                          false) {
                        await cartController.addToCart(
                          widget.product,
                          int.parse(
                            cartController.quantityCommand.value.text,
                          ),
                        );
                        setState(() {
                          isLoadAddcart = false;
                        });
                      }
                    } else {
                      if (_formKey.currentState!.validate()) {
                        Get.back();

                        setState(() {
                          isLoadAddcart = true;
                        });

                        if (cartController.isAlredyAdded(widget.product) ==
                            false) {
                          await cartController.addToCart(
                            widget.product,
                            int.parse(
                              cartController.quantityCommand.value.text,
                            ),
                          );
                          setState(() {
                            isLoadAddcart = false;
                          });
                        }
                      }
                    }
                  },
                ),
              ),
            ],
          ),
          
        );
        
      },),
       isScrollControlled : true,
    );
  }
}
