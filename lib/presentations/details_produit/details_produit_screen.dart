// ignore_for_file: use_build_context_synchronously

import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/app/app_prefs.dart';
import 'package:eagri/app/constant.dart';
import 'package:eagri/presentations/common/rate/controller/rate_controller.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/routes_manager.dart';
import 'package:eagri/presentations/shop/controller/shop_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:select_form_field/select_form_field.dart';
import '../../app/di.dart';
import '../../app/functions.dart';
import '../../data/repository/api/repository_impl.dart';
import '../../data/request/notation_request.dart';
import '../../data/request/request.dart';
import '../../data/request/shop_resquests.dart';
import '../../domain/model/Product.dart';
import '../../domain/model/user_model.dart';
import '../../domain/usecase/rate_usecase.dart';
import '../../domain/usecase/use_case.dart';
import '../catalogue/controllers/catalogue_controller.dart';
import '../catalogue/updateProdcut/controller/update_product_controller.dart';
import '../common/default_button.dart';
import '../common/flushAlert_componenent.dart';
import '../common/state/loader.dart';
import '../image/update_image_screen.dart';
import '../prestations/demande_prestation.dart';
import '../ressources/assets_manager.dart';
import '../ressources/strings_manager.dart';
import '../ressources/styles_manager.dart';
import '../transport/demande_transport_screen.dart';
import 'components/add_cart_button.dart';
import 'components/body.dart';
import 'components/loader_details_produc.dart';
import 'components/product_images.dart';

class DetailsProduitScreen extends StatefulWidget {
  static String routeName = Routes.detailsProduct;

  const DetailsProduitScreen({Key? key, required this.product})
      : super(key: key);
  final ProductDetailsArguments product;
  @override
  State<DetailsProduitScreen> createState() => _DetailsProduitScreenState();
}

class _DetailsProduitScreenState extends State<DetailsProduitScreen> {
  final _focusNode = FocusNode();
  RateUseCase rateUseCase = instance<RateUseCase>();

  ShopController shopController = Get.put(ShopController());
  RateController rateController = Get.put(RateController());
  UseCase useCase = instance<UseCase>();
  late ProductDetailsArguments productArg;
  bool isLoadDetails = true;
  late ProductModel productShow;
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  CatalogueController catalogueController = Get.put(CatalogueController());
  final UpdateProductController updateProductController =
      Get.put(UpdateProductController());
  AppSharedPreference appSharedPreference = instance<AppSharedPreference>();
  Future<bool> changeDisponibilite(bool isEnable) async {
    if (isEnable == true) {
      await repositoryImpl
          .disableProduct(productShow.id)
          .then((response) => response.fold((l) {
                showCustomFlushbar(
                    context, "Echec", ColorManager.error, FlushbarPosition.TOP);
              }, (data) async {
                // catalogueController.fetchProductFournisseur();
                productShow.status = (await data).status;
                if (mounted) {
                  setState(() {
                    catalogueController.selectedProduct.value!.status =
                        productShow.status;
                    productShow;
                  });
                }
                showCustomFlushbar(context, "Disponibilté modifiée",
                    ColorManager.primary, FlushbarPosition.TOP);
              }));
    } else {
      await repositoryImpl
          .enableProduct(productShow.id)
          .then((response) => response.fold((l) {
                showCustomFlushbar(
                    context, "Echec", ColorManager.error, FlushbarPosition.TOP);
              }, (data) async {
                //catalogueController.fetchProductFournisseur();
                productShow.status = (await data).status;
                if (mounted) {
                  setState(() {
                    catalogueController.selectedProduct.value!.status =
                        productShow.status;
                    productShow;
                  });
                }
                showCustomFlushbar(context, "Disponibilté modifiée",
                    ColorManager.primary, FlushbarPosition.TOP);
              }));
    }

    return productShow.status != "UNAVAILABLE";
  }

  checkRate() async {
    await appSharedPreference.getUserInformation().then((response) async {
      await rateUseCase
          .checkAlreadyRated(
        CheckRateRequest(
            userId: response.id!,
            ressourceId: widget.product.product.id,
            codeService: ServiceCode.marketPlace),
      )
          .then((checkAlreadyRatedResponse) async {
        checkAlreadyRatedResponse.fold((l) {
          setState(() {
            productShow.alreadyNoted = true;
          });
        }, (checkAlreadyRatedResult) async {
          setState(() {
            productShow.alreadyNoted = checkAlreadyRatedResult.data;
          });
        });
      });
    });
  }

  getMoreInfo() async {
    await repositoryImpl
        .getInfoById(InfoByIdRequest(
          userId: widget.product.product.fournisseurId,
          regionId: widget.product.product.location!.regionId,
          departementId: widget.product.product.location!.departementId != 0 &&
                  widget.product.product.location!.departementId != null
              ? widget.product.product.location!.departementId
              : null,
          sprefectureId: widget.product.product.location!.sPrefectureId != 0 &&
                  widget.product.product.location!.sPrefectureId != null
              ? widget.product.product.location!.sPrefectureId
              : null,
          localiteId: widget.product.product.location!.localiteId != 0 &&
                  widget.product.product.location!.localiteId != null
              ? widget.product.product.location!.localiteId
              : null,
        ))
        .then((value) => value.fold((l) {
              if (mounted) {
                setState(() {
                  isLoadDetails = false;
                });
              }
            }, (info) {
              productShow.location!.regionName =
                  info.region != null ? info.region!.name : null;
              productShow.location!.departementName =
                  info.departement != null ? info.departement!.name : null;
              productShow.location!.sPrefectureName =
                  info.sousprefecture != null
                      ? info.sousprefecture!.name
                      : null;
              productShow.location!.localiteName =
                  info.localite != null ? info.localite!.name : null;
              if (info.user != null) {
                productShow.user = UserModel(
                    lastName: info.user!.laststname,
                    firstName: info.user!.firstname,
                    number: info.user!.phone);
              }

              infos(productShow);
              if (mounted) {
                setState(() {
                  isLoadDetails = false;
                  productShow;
                });
              }
            }));
  }

  infos(ProductModel product) {
    setState(() {
      // log("details product : $value");
      // inspect(value);

      isLoadDetails = false;

      updateProductController.productDescription.text =
          product.description == null ? "" : productShow.description!;
      updateProductController.productPrice.text = productShow.price.toString();

      updateProductController.productQuatity.text =
          productShow.quantity.toString();
      updateProductController.productId = productShow.id;

      updateProductController.productRegion.text =
          productShow.location!.regionId.toString();

      // updateProductController.productLocalite.text =
      //     productShow.id.toString();

      updateProductController.productDepartement.text =
          productShow.location!.departementId.toString();
      updateProductController.productSousPrefecture.text =
          productShow.location!.sPrefectureId.toString();

      updateProductController.productLocalite.text =
          productShow.location!.localiteId.toString();
      if (productShow.location!.departementId != null) {
        updateProductController
            .fetchDepartement(productShow.location!.regionId!);
      }
    });
  }

  Future<void> getInfoProduct() async {
    await getMoreInfo();
  }

  @override
  void dispose() {
    updateProductController.productRegion.clear();
    super.dispose();
  }

  @override
  void initState() {
    productArg = widget.product;
    productShow = productArg.product;
    if (widget.product.isPay == true) {
      checkRate();
    }

    getMoreInfo();

    shopController.notifyConsultationProduct(
      ConsultationProductRequest(
        date: DateTime.now(),
        productId: productArg.product.id,
      ),
    );
    rateController.getProductComment(ProductListCommentRequest(
        codeService: ServiceCode.marketPlace,
        resourceId: productArg.product.id));
    shopController.getSimilarProduct(productArg.product);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
      builder: (context, user, child) => Scaffold(
        backgroundColor: ColorManager.white,
        extendBody: true,
        // appBar: PreferredSize(
        //   preferredSize: Size.fromHeight(
        //       AppBar(backgroundColor: Colors.transparent).preferredSize.height),
        //   child: CustomAppBar(product: productArg.product),
        // ),

        bottomNavigationBar: isLoadDetails == false
            ? Container(
                padding: const EdgeInsets.only(top: 5),
                color: const Color.fromARGB(255, 243, 243, 243),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(left: 10, bottom: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            "Prix unitaire",
                            style: getRegularTextStyle(
                                color: ColorManager.grey, fontSize: 12),
                          ),
                          if (productShow.code != codePrestation.toString())
                            Text(
                              "${productShow.price} ${AppStrings.moneyDevise} ",
                              style: getBoldTextStyle(
                                  color: ColorManager.black, fontSize: 17),
                            ),
                          if (productShow.code == codePrestation.toString())
                            Text(
                              "${productShow.price} ${AppStrings.moneyDevise} / ${productShow.unite}",
                              style: getBoldTextStyle(
                                  color: ColorManager.black, fontSize: 17),
                            ),
                          // const SizedBox(height: 10),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    if (user.id != productShow.fournisseurId &&
                        widget.product.isPay == false) ...[
                      AddCartButton(
                        product: productShow,
                      )
                    ] else if (widget.product.isPay == true &&
                        productShow.alreadyNoted == false) ...[
                      Container(
                        margin: const EdgeInsets.all(10),
                        child: DefaultButton(
                          color: ColorManager.blue,
                          text: "Noter",
                          press: () {
                            rate();
                          },
                        ),
                      ),
                    ] else if (isInScopes(UserPermissions.RESERVATION_TRANSPORT,
                                user.scopes!) ==
                            true &&
                        productShow.code == codeTransport.toString()) ...[
                      Container(
                        margin: const EdgeInsets.all(10),
                        child: DefaultButton(
                          text: "Reserver",
                          press: () {
                            Get.to(DemandeTransportScreen(
                              product: productShow,
                            ));
                          },
                        ),
                      )
                    ] else if (isInScopes(
                                UserPermissions.RESERVATION_PRESTATION,
                                user.scopes!) ==
                            true &&
                        productShow.code == codePrestation.toString()) ...[
                      Container(
                        margin: const EdgeInsets.all(10),
                        child: DefaultButton(
                          text: "Reserver",
                          press: () {
                            Get.to(DemandePrestation(
                              prestation: productShow,
                            ));
                          },
                        ),
                      )
                    ] else if (productShow.fournisseurId != user.id! &&
                        productShow.code ==
                            codeProductAgricultural.toString() &&
                        (productShow.typeOffers == "ACHAT" ||
                            productShow.typeOffers == "PREACHAT")) ...[
                      Container(
                        margin: const EdgeInsets.all(10),
                        child: DefaultButton(
                          text: "Contacter",
                          press: () async {
                            loaderDialog(context, text: "Veuillez patienter");
                            await repositoryImpl
                                .demandePreAchat(user.id!, productShow.id)
                                .then((response) async {
                              await response.fold((failure) async {
                                Get.back();
                                showCustomFlushbar(context, failure.message,
                                    ColorManager.error, FlushbarPosition.TOP);
                              }, (data) {
                                Get.back();

                                showCustomFlushbar(context, "Contact établi",
                                    ColorManager.succes, FlushbarPosition.TOP);
                              });
                            });
                          },
                        ),
                      )
                    ] else if (user.id == productShow.fournisseurId)
                      if (true == widget.product.isMyproduct) ...[
                        Container(
                          margin: const EdgeInsets.all(10),
                          child: DefaultButton(
                            color: ColorManager.blue,
                            text: "Modifier",
                            press: () {
                              modifProduct(catalogueController);
                            },
                          ),
                        )
                      ]
                  ],
                ),
              )
            : const SizedBox(),

        body: Hero(
          tag: "product",
          child: isLoadDetails == true
              ? ListView(
                  children: [
                    ProductImages(
                      isCatalogueProduct: productArg.isMyproduct,
                      product: productShow,
                    ),
                    loadDetailProduct()
                  ],
                )
              : RefreshIndicator(
                  onRefresh: () => getInfoProduct(),
                  child: SingleChildScrollView(
                    physics: const AlwaysScrollableScrollPhysics(),
                    child: Body(
                      isCatalogueProduct: productArg.isMyproduct,
                      product: productShow,
                    ),
                  ),
                ),
        ),
      ),
    );
  }

  modifProduct(CatalogueController catalogueController) {
    bool isDispo =
        catalogueController.selectedProduct.value!.status != "UNAVAILABLE";
    bool isLoadDispo = false;
    Get.bottomSheet(
      isScrollControlled: true,
      StatefulBuilder(
        builder: (context, setInnerState) => Container(
          padding: const EdgeInsets.all(10),
          margin: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
          height: MediaQuery.of(context).size.height - 50,
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(10)),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Text(
                  "Modification de produit",
                  style: getBoldTextStyle(color: Colors.black, fontSize: 20),
                ),
                const SizedBox(
                  height: 10,
                ),
                TextButton(
                    onPressed: () {
                      Get.to(UpdateImage(
                        product: productShow,
                      ));
                    },
                    child: const Text("Modifier image")),
                Row(
                  children: [
                    const Text("Disponibilité"),
                    const Spacer(),
                    if (isLoadDispo == true)
                      SizedBox(
                        height: 50,
                        width: 50,
                        child: Lottie.asset(JsonAssets.loader),
                      ),
                    Switch(
                      value: isDispo,
                      onChanged: (value) async {
                        setInnerState(() {
                          isLoadDispo = true;
                        });
                        isDispo = await changeDisponibilite(isDispo);
                        setInnerState(() {
                          isDispo;
                          isLoadDispo = false;
                        });
                      },
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Informations supplementaire",
                    style:
                        getRegularTextStyle(color: Colors.black, fontSize: 15),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                TextFormField(
                  controller: updateProductController.productDescription,
                  decoration: InputDecoration(
                    // hintText:"La ",
                    label: Text(
                      "Description",
                      style: getRegularTextStyle(color: Colors.grey),
                    ),
                  ),
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: updateProductController.productPrice,
                  decoration: InputDecoration(
                    // hintText:"La ",
                    label: Text(
                      productShow.code == codeTransport.toString()
                          ? "Prix du transport"
                          : productShow.code == codePrestation.toString()
                              ? "Le coût de votre service"
                              : "Prix du produit",
                      style: getRegularTextStyle(color: Colors.grey),
                    ),
                  ),
                  keyboardType: TextInputType.number,
                ),
                const SizedBox(
                  height: 20,
                ),
                if (productShow.code != codePrestation.toString()) ...[
                  TextFormField(
                    controller: updateProductController.productQuatity,
                    decoration: InputDecoration(
                      // hintText:"La ",
                      label: Text(
                        productShow.code == codeTransport.toString()
                            ? "Capacité de votre camion"
                            : "Quantité du produit",
                        style: getRegularTextStyle(color: Colors.grey),
                      ),
                    ),
                    keyboardType: TextInputType.number,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                ],
                if (productShow.code != codeTransport.toString()) ...[
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Localisation du produit",
                      style: getRegularTextStyle(
                          color: Colors.black, fontSize: 15),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  SelectFormField(
                    type: SelectFormFieldType.dialog,
                    enableSearch: true, // or can be dialog
                    //initialValue: 'circle',
                    controller: updateProductController.productRegion,
                    labelText: 'Choisir la region',
                    items: updateProductController.listRegionMap,
                    onChanged: (val) => updateProductController
                        .fetchDepartement(int.parse(val)),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Obx(() => updateProductController.listDepartementMap.isEmpty
                      ? Container()
                      : Column(
                          children: [
                            SelectFormField(
                              controller:
                                  updateProductController.productDepartement,
                              type: SelectFormFieldType
                                  .dropdown, // or can be dialog
                              labelText: "Choisir le departement",
                              items: updateProductController.listDepartementMap,
                              onChanged: (val) {
                                // print("******************************");
                                // log(val);
                                // print("******************************");
                                updateProductController
                                    .fetchSpFectureOfDepartement(
                                        int.parse(val));
                              },
                            ),
                            const SizedBox(
                              height: 30,
                            ),
                          ],
                        )),
                  Obx(
                    () => updateProductController.listSousPrefectureMap.isEmpty
                        ? Container()
                        : Column(
                            children: [
                              SelectFormField(
                                controller: updateProductController
                                    .productSousPrefecture,
                                type: SelectFormFieldType
                                    .dropdown, // or can be dialog
                                labelText: "Choisir la sous-prefercture",
                                items: updateProductController
                                    .listSousPrefectureMap,
                                onChanged: (val) => updateProductController
                                    .fecthLocalite(int.parse(val)),
                              ),
                              const SizedBox(
                                height: 30,
                              )
                            ],
                          ),
                  ),
                  Obx(
                    () => updateProductController.listLocaliteMap.isEmpty
                        ? Container()
                        : SelectFormField(
                            controller: updateProductController.productLocalite,
                            type: SelectFormFieldType
                                .dropdown, // or can be dialog
                            labelText: "Choisir la localité",
                            items: updateProductController.listLocaliteMap,
                          ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                ],
                DefaultButton(
                  color: ColorManager.blue,
                  text: "Enregistrer",
                  press: () async {
                    productShow =
                        await updateProductController.modifProduct(productShow);
                    setState(() {
                      productShow;
                    });
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  rate() {
    Get.bottomSheet(
      isScrollControlled: false,
      Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10)),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Entrer votre note",
                style: getBoldTextStyle(color: Colors.black, fontSize: 20),
              ),
              RatingBar.builder(
                initialRating: 0,
                minRating: 0,
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 5,
                itemSize: 50,
                itemPadding: const EdgeInsets.symmetric(horizontal: 1),
                itemBuilder: (context, _) => const Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                onRatingUpdate: (rating) {
                  rateController.rating.value = rating.toInt();
                },
              ),
              const SizedBox(
                height: 30,
              ),
              Text(
                "Entrer un commentaire",
                style: getRegularTextStyle(color: Colors.black, fontSize: 12),
              ),
              const SizedBox(
                height: 10,
              ),
              TextField(
                controller: rateController.commantaire.value,
                maxLines: null,
                focusNode: _focusNode,
                decoration: InputDecoration(
                  // hintText:"La ",
                  label: Text(
                    "Commentaire",
                    style: getRegularTextStyle(color: Colors.grey),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 100,
                    child: DefaultButton(
                      color: ColorManager.grey,
                      text: "Annuler",
                      press: () {
                        Get.back();
                      },
                    ),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  if (productShow.alreadyNoted == false)
                    SizedBox(
                      width: 100,
                      child: DefaultButton(
                        text: "Envoyer",
                        press: () async {
                          if (rateController.rating.value > 0) {
                            rateController.resourceIdObs.value = productShow.id;
                            rateController.personneNoteId.value =
                                productShow.fournisseurId;
                            productShow.alreadyNoted =
                                await rateController.ratingRessource(
                                    ServiceCode.marketPlace.toString());
                            setState(() {
                              productShow;
                            });
                          }
                        },
                      ),
                    ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ProductDetailsArguments {
  final ProductModel product;
  final bool isMyproduct;
  final bool isIntrant;
  final bool isPay;

  ProductDetailsArguments(
      {required this.product,
      this.isIntrant = false,
      this.isMyproduct = false,
      this.isPay = false});
}
