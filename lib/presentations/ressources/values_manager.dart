class AppMargin {
  static const double m2 = 2;
  static const double m5 = 5;
  static const double m8 = 8;

  static const double m10 = 10;
  static const double m12 = 12;
  static const double m14 = 14;
  static const double m16 = 16;
  static const double m18 = 18;
  static const double m20 = 20;
  static const double m30 = 30;
  static const double m40 = 40;
  static const double m50 = 50;

  static const double m60 = 60;
}

class AppPadding {
  static const double p5 = 5;
  static const double p2 = 2;

  static const double p8 = 8;
  static const double p10 = 10;
  static const double p12 = 12;
  static const double p14 = 14;
  static const double p16 = 16;
  static const double p18 = 18;
  static const double p20 = 20;
  static const double p28 = 28;
  static const double p30 = 30;
  static const double p40 = 40;
  static const double p70 = 70;
  static const double p100 = 100;
  static const double p120 = 120;
}

class AppSize {
  static const double s0 = 0;
  static const double s5 = 5;

  static const double s8 = 8;
  static const double s1_5 = 1.5;

  static const double s4 = 4;
  static const double s10 = 10;
  static const double s12 = 12;
  static const double s14 = 14;
  static const double s16 = 16;
  static const double s18 = 18;
  static const double s20 = 20;
  static const double s28 = 28;
  static const double s30 = 30;

  static const double s40 = 40;
  static const double s50 = 50;

  static const double s60 = 60;
  static const double s55 = 55;

  static const double s100 = 100;
  static const double s180 = 180;
  static const double s150 = 150;
}

class DurationConstant {
  static const int d300 = 300;
}
