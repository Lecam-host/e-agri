enum LanguageType { english, french }
const String french = "fr";
const String english = "en";

extension LanguageTypeExtension on LanguageType {
  String getValue() {
    switch (this) {
      case LanguageType.english:
        return english;
      case LanguageType.french:
        return french;
    }
  }
}
