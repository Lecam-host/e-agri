import 'package:flutter/material.dart';

import '../../app/constant.dart';
import 'color_manager.dart';
import 'font_manager.dart';

double getDefaultFontSize(BuildContext context) {
  final mediaQueryData = MediaQuery.of(context);
  return mediaQueryData.textScaleFactor * Theme.of(context).textTheme. bodyLarge!.fontSize!;
}
TextStyle _getTextStyle(
  double fontSize,
  String fontFamily,
  FontWeight fontWeight,
  Color color,
  double letterSpacing,
) {
  return TextStyle(
    fontFamily: fontFamily,
    fontSize: fontSize,
    fontWeight: fontWeight,
    color: color,
    // height: 1.2,
    letterSpacing: letterSpacing,
  );
}

//regular textstyle

TextStyle getRegularTextStyle(
    {double fontSize = FontSize.s12,
    Color? color,
    FontWeight? fontWeight,
    double letterSpacing = Constant.defaultLetterSpacing}) {
  return _getTextStyle(
    fontSize,
    FontConstants.fontFamily,
    fontWeight ?? FontWeightManager.regular,
    color ?? ColorManager.black,
    letterSpacing,
  );
}

//Light  textstyle
TextStyle getLightTextStyle(
    {double fontSize = FontSize.s12,
    Color? color,
    double letterSpacing = Constant.defaultLetterSpacing}) {
  return _getTextStyle(fontSize, FontConstants.fontFamily,
      FontWeightManager.light, color ?? ColorManager.black, letterSpacing);
}

//bold  textstyle
TextStyle getBoldTextStyle(
    {double fontSize = FontSize.s12,
    Color? color,
    double letterSpacing = Constant.defaultLetterSpacing}) {
  return _getTextStyle(fontSize, FontConstants.fontFamily,
      FontWeightManager.bold, color ?? ColorManager.black, letterSpacing);
}

//semibold  textstyle
TextStyle getSemiBoldTextStyle(
    {double fontSize = FontSize.s12,
    Color? color,
    double letterSpacing = Constant.defaultLetterSpacing}) {
  return _getTextStyle(fontSize, FontConstants.fontFamily,
      FontWeightManager.semiBold, color ?? ColorManager.black, letterSpacing);
}

//medium  textstyle

TextStyle getMeduimTextStyle(
    {double fontSize = FontSize.s12,
    Color? color,
    double letterSpacing = Constant.defaultLetterSpacing}) {
  return _getTextStyle(
    fontSize,
    FontConstants.fontFamily,
    FontWeightManager.medium,
    color ?? ColorManager.black,
    letterSpacing,
  );
}

BoxShadow shadow() {
  return BoxShadow(
    color: Colors.grey.withOpacity(0.5),
    spreadRadius: 0,
    blurRadius: 0,
    offset: const Offset(0, 0), // changes position of shadow
  );
}

BoxDecoration containerBoxDecoration() {
  return BoxDecoration(
    color: ColorManager.lightGrey.withOpacity(0.3),
  );
}
