import 'package:flutter/cupertino.dart';

class FontConstants {
  static const String fontFamily = "Poppins";
}

class FontWeightManager {
  static const FontWeight light = FontWeight.w300;
  static const FontWeight regular = FontWeight.w400;
  static const FontWeight medium = FontWeight.w600;
  static const FontWeight semiBold = FontWeight.w800;
  static const FontWeight bold = FontWeight.w900;
}

class FontSize {
  static const double s8 = 8;
  static const double s10 = 10;

  static const double s12 = 12;
  static const double s13 = 13;
  static const double s14 = 14;
  static const double s15 = 15;
  static const double s16 = 16;
  static const double s17 = 17;
  static const double s18 = 18;
  static const double s20 = 20;
  static const double s25 = 25;

  static const double s30 = 30;
}
