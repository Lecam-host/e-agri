import 'package:flutter/material.dart';

class ColorManager {
  static Color primary = HexColor.fromHex("#588157");
  static Color primary2 = HexColor.fromHex("#4E6C50");
  static Color primary3 = HexColor.fromHex("#AA8B56");
  static Color primary4 = HexColor.fromHex("#CA965C");

  static Color formFieldBackgroundColor = HexColor.fromHex("#F1F1F1");

  static Color black = HexColor.fromHex("#000000");

  static Color primaryOpacity70 = HexColor.fromHex("#B3588157");

  static Color error = HexColor.fromHex("#C30000");
  static Color red = HexColor.fromHex("#E70303");

  static Color white = HexColor.fromHex("#FFFFFF");
  static Color succes = HexColor.fromHex("#05C700");
  static Color darkPrimary = HexColor.fromHex("#2E492D");
  static Color darkGrey = HexColor.fromHex("#777676");

  static Color grey1 = HexColor.fromHex("#A8A8A8");
  static Color grey = HexColor.fromHex("#888888");
  static Color lightGrey = HexColor.fromHex("#DAD7CD");
  static Color blue = HexColor.fromHex("#006C9E");
  static Color blueLight = HexColor.fromHex("#004BC6");

  static Color jaune = HexColor.fromHex("#E6A23C");
  static Color backgroundColor = HexColor.fromHex("#F3F3F3");
  static Color shimmerColorBaseColor = const Color.fromARGB(255, 115, 115, 115);
  static Color shimmerColorHighlightColor =
      const Color.fromARGB(255, 181, 181, 181);
}

extension HexColor on Color {
  static fromHex(String hexColorString) {
    hexColorString = hexColorString.replaceAll("#", "");
    if (hexColorString.length == 6) {
      hexColorString = "FF$hexColorString";
    }
    return Color(int.parse(hexColorString, radix: 16));
  }
}
