const String imagePath = "assets/images";
const String jsonPath = "assets/json";
const String svgPath = "assets/icons";

class ImageAssets {
  static String longLogo = "$imagePath/logo.png";

  static String onboardingLogo1 = "$imagePath/previous.svg";
  static String onboardingLogo2 = "$imagePath/previous.svg";
  static String onboardingLogo3 = "$imagePath/previous.svg";
  static String onboardingLogo4 = "$imagePath/previous.svg";
  static String hollowCircleIc = "$imagePath/circle.svg";
  static String leftArrowIc = "$imagePath/previous.svg";
  static String rightarrowIc = "$imagePath/next.svg";
  static String connexionBg = "$imagePath/connexionBgImg.jpeg";

  static String solidCircleIc = "$imagePath/solid_circle.svg";
  static String alertCloche = "$imagePath/alert_cloche.png";
  static String priceMarque = "$imagePath/marque.png";

  static String meteo = "$imagePath/meteo.png";
  static String allWeather = "$imagePath/allweather.jpg";
  static String price = "$imagePath/price.jpg";

  static String weather = "$imagePath/weather.svg";
  static String marketPrice = "$imagePath/marketPrice.jpg";

  static String pin = "$imagePath/pin.png";
  static String vue = "$imagePath/vue.png";
  static String conseil = "$imagePath/conseils.png";

  static String downArrow = "$imagePath/down-arrow.png";
  static String upArrow = "$imagePath/en-haut.png";
  static String meteoBg = "$imagePath/meteoBg.jpg";

  static String video = "$imagePath/video.png";
  static String audio = "$imagePath/audio.jpg";
  static String document = "$imagePath/document.jpeg";
  static String desdocument = "$imagePath/desdocuments.png";

  static String souscription = "$imagePath/souscription.png";
  static String lectureDoc = "$imagePath/lectureDoc.png";

  static String nuage = "$imagePath/nuage.jpg";
  static String mettreEngrais = "$imagePath/mettreEngrais.jpg";
  static String orateur = "$imagePath/orateur.png";
  static String humidity = "$imagePath/humidity.png";
  static String search = "$imagePath/search.png";
  static String filter = "$imagePath/filter.png";
  static String map = "$imagePath/map.png";
  static String ocpvLogo = "$imagePath/ocpv_logo.png";
  static String splash1 = "$imagePath/1.png";
  static String splash2 = "$imagePath/2.png";
  static String splash3 = "$imagePath/3.png";
  static String alert = "$imagePath/alert.jpg";
  static String transport = "$imagePath/transport.jpg";
  static String agricole = "$imagePath/agricole.jpg";
  static String prestation = "$imagePath/prestation.jpg";


}

class JsonAssets {
  static String loading = "$jsonPath/loading.json";
  static String error = "$jsonPath/error.json";
  static String empty = "$jsonPath/empty.json";
  static String success = "$jsonPath/success.json";
  static String save = "$jsonPath/save.json";
  static String earthload = "$jsonPath/earth_load.json";
  static String loader = "$jsonPath/loader.json";
  static String cart = "$jsonPath/cart.json";
  static String cartEmpty = "$jsonPath/cart_empty.json";
  static String deleteSuccess = "$jsonPath/delete_succes.json";
  static String waitingPaiement = "$jsonPath/waiting_paiement.json";
  static String waitingPassword = "$jsonPath/enter_password.json";

  static String agri = "$jsonPath/agri.json";
  static String advice = "$jsonPath/advice.json";
  static String weather = "$jsonPath/weather.json";

}

class SvgManager {
  static String userIcon = "$svgPath/User Icon.svg";
  static String weather = "$svgPath/weather.svg";
  static String price = "$svgPath/price.svg";
  static String alert = "$svgPath/alert.svg";
  static String advice = "$svgPath/advice.svg";
  static String audio = "$svgPath/audio.svg";

  static String draft = "$svgPath/draft.svg";
  static String official = "$svgPath/official.svg";
  static String verify = "$svgPath/verify.svg";
  static String customers = "$svgPath/customer.svg";
  static String history = "$svgPath/history.svg";
  static String comparison = "$svgPath/comparison.svg";
  static String add = "$svgPath/add.svg";
  static String media = "$svgPath/media.svg";
  static String cart = "$svgPath/Cart Icon.svg";
  static String like = "$svgPath/Heart Icon.svg";
  static String search = "$svgPath/Search Icon.svg";
  static String shop = "$svgPath/Shop Icon.svg";

  static String home = "$svgPath/home.svg";
  static String user = "$svgPath/User Icon.svg";
  static String star = "$svgPath/Star Icon.svg";
  static String filter = "$svgPath/filter.svg";

  static String cartIsNotEmpty = "$svgPath/cartIsNotEmpty.svg";

  static String userOutl = "$svgPath/user_outl.svg";
  static String homeOutl = "$svgPath/home_outl.svg";
  static String shopOutl = "$svgPath/shop_outl.svg";
  static String priceTagOutl = "$svgPath/price_tag_outl.svg";
  static String priceTag = "$svgPath/price_tag.svg";
  static String transport = "$svgPath/transport_svg.svg";

}

class IconAssetManager {
  static String adviceIcon = "$svgPath/adviceIcon.png";
  static String alertIcon = "$svgPath/alertIcon.png";
  static String serviceIcon = "$svgPath/serviceIcon.png";
  static String weatherIcon = "$svgPath/weatherIcon.png";
  static String sendIcon = "$svgPath/sendIcon.png";
  static String receiveIcon = "$svgPath/receiveIcon.png";
  static String transportIcon = "$svgPath/transportIcon.png";
  static String payIcon = "$svgPath/pay.png";
  static String tractorIcon = "$svgPath/tractor.png";
  static String commentIcon = "$svgPath/comment.png";
  static String emptyIcon = "$svgPath/empty.png";
  static String coinsIcon = "$svgPath/coins.png";
}
