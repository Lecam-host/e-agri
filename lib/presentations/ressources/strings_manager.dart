class AppStrings {
  static const String noRouteFound = "No Route Found";
  static const String login = "Se connecter";
  static const String success = "Success";
  static const String history = "Historique";

  static const String titleMarketPlace = "Place de marché";

  static const String marketPrice = "Prix du marché";
  static const String meteo = "Météo";
  static const String alert = "Alerte";

  static const String comparaison = "Comparaison";

  static const String gros = "En gros";
  static const String details = "En détails";
  static const String moneyUnit = "FCFA";
  static const String recommandations = "Recommandations";
  static const String poserQuestion = "Demander un conseil";
  static const String demandeConseil = "Demander un conseil";
  static const String description = "Description";

  static const String orderAsc = "Par ordre croissant";
  static const String historyPrice = "Historique des prix";
  static const String comparePrice = "Comparer des prix";

  static const String orderDesc = "Par ordre décroissant";
  static const String orderAlpha = "Par ordre décroissant";

  static const String moneyDevise = "FCFA";
  static const String seeMore = "Voir plus";
  static const String infoSpeculation = "Information sur la spéculation";
  static const String infoLieu = "Information sur le lieu";

  static const String searchEmpty = "Désolé aucun élemnet n'a été trouvé";

  static const String addressRequest =
      "A qui voulez-vous adresser la demande ?";

  static const String choisirProduit = "Choisir le produit";
  static const String choisirRegion = "Choisir region";

  static const String adviceTypeQuestion = "Quel est le type de conseil?";
  static const String precisezDomaine = "Préciser le domaine?";

  static const String speculationQuestion = "Choisir le produit";
  static const String uniteSpeculationQuestion = "Choisir l'unité du produit";
  static const String regionQuestion = "Choisir la region";
  static const String departementQuestion = "Choisir le département";
  static const String sousPrefectureQuestion = "Choisir la sous-prefecture";
  static const String localiteQuestion = "Choisir la localité";

  static const String categoryAdviceQuestion =
      "Quel est la categorie de conseil?";

  static const String typeDeMedia = "Comment souhaitez-vous avoir le conseil ?";
  static const String adviceCategoryQuestion =
      "Quelle est la catégorie du conseil ?";
  static const String choiceCity = "Choisir la ville";

  static const String forumQuestion = "Mes démandes";
  static const String lastConseil = "Derniers conseils";
  static const String lastAlert = "Dernieres alertes";

  static const String otherService = "Autres services";
  static const String conseils = "Conseils";
  static const String all = "Tout";
  static const String treated = "Traité";
  static const String notTreated = "Non traité";
  static const String pending = "En attente";

  static const String quit = "Quitter";
  static const String cancel = "Annuler";

  static const String view = "Vue";
  static const String humidity = "Humidité";
  static const String pressure = "Pression";

  static const String agricultural = "Agricole";
  static const String breeding = "Elevage";
  static const String insurance = "Assurance";
  static const String finance = "Financier";
  static const String category = "Categories";
  static const String categoryConseil = "Catégories de conseil";

  static const String format = "Formats";

  static const String audio = "Audio";
  static const String document = "Document";
  static const String domain = "Domaine";

  static const String video = "Video";
  static const String categoryPrice = "Catégories de prix";

  static const String lorem =
      "Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups";

  static const String onBoardingTitle1 = "SEE THE BEST COURSE #1";
  static const String onBoardingTitle2 = "SEE THE BEST COURSE #2";
  static const String onBoardingTitle3 = "SEE THE BEST COURSE #3";
  static const String onBoardingTitle4 = "SEE THE BEST COURSE #4";

  static const String onBoardingSubTitle1 =
      "Tut app is an awesome flutter application using clean architecture #1";
  static const String onBoardingSubTitle2 =
      "Tut app is an awesome flutter application using clean architecture #2";
  static const String onBoardingSubTitle3 =
      "Tut app is an awesome flutter application using clean architecture #3";
  static const String onBoardingSubTitle4 =
      "Tut app is an awesome flutter application using clean architecture #4";
  static const String skip = "Skip";
  static const String number = "Numéro";
  static const String password = "Mot de passe";
  static const String submit = "Soumettre";
  static const String send = "Envoyer";

  static const String iSubscribe = "Je m'abonne";
  static const String addPrice = "Ajouter un prix";
  static const String searchSomething = "Rechercher quelque chose";

  static const String enterLieu = "Entrer le lieu";
  static const String weather = "Meteo";

  static const String myParcel = "Mes parcelles";

  static const String periodeQuestion = "Préciser la période";

  static const String speculation = "Spéculation";
  static const String priceDetail = "Prix en détail";
  static const String priceGros = "Prix en gros";

  static const String region = "Region";
  static const String lieu = "lieu";
  static const String adviceDescriptionQuestion =
      "Entrer la description du conseil";
  static const String adviceDescription = "Description du conseil";

  static const String preciseLieu =
      "Precisez le lieu dans le lequel vous vous trouvez";

  static const String departement = "Departemennt";

  static const String sousPrefecture = "Sous-prefecture";
  static const String localite = "Localité";
  static const String unite = "Unité";

  static const String forgetPassword = "Mot de passe oublié ?";
  static const String signUp = "Créer un compte";
  static const String loading = "Loading...";
  static const String retryAgain = "Réesseyer";
  static const String toSearch = "Réchercher";
  static const String enterKeyWord = "Entrez un mot clé";

  static const String enterSearch = "Veuillez entrer ce que vous recherchez";
  static const String recentSearch = "Rechercher récement";
  static const String categoryChoice = "Catégories choisies";

  static const String searchConseil = "Récherche de conseil";
  static const String enterDemande = "Entrez votre démande";
  static const String enterNameParcelle = "Entrer un nom pour votre parcelle";

  static const String enterPrice = "Entrer le prix";

  static const String adviceCategory = "Catégories de conseils";
  static const String dateDebut = "Date de debut";
  static const String dateFin = "Date de fin";

  static const String ok = "ok";
  static const String profil = "Profil";
  static const String home = "Accueil";
  static const String next = "Suivant";
  static const String previous = "Précédent";
  static const String noConnection = "Aucune ou mauvaise connexion";

  static const String indiqueLieu =
      "Indiquer le lieu dans lequel vous vous trouvez";

  static const String verification = "Verification";

  static const String fakeText = "2023-01-18T15:30:07.813052";

  static const String kEmailNullError = "Please Enter your email";
  static const String kInvalidEmailError = "Please Enter Valid Email";
  static const String kPassNullError = "Please Enter your password";
  static const String kShortPassError = "Password is too short";
  static const String kMatchPassError = "Passwords don't match";
  static const String kNamelNullError = "Please Enter your name";
  static const String kPhoneNumberNullError = "Please Enter your phone number";
  static const String kAddressNullError = "Please Enter your address";
}
