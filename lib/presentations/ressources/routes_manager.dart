import 'package:eagri/presentations/account/account_view.dart';
import 'package:eagri/presentations/alert/add_alert_view.dart';
import 'package:eagri/presentations/cart/cart_screen.dart';
import 'package:eagri/presentations/cart/controller/cart_controller.dart';
import 'package:eagri/presentations/catalogue/catalogue_screen.dart';
import 'package:eagri/presentations/common/state/succes_page_view.dart';
import 'package:eagri/presentations/finance/finance_srceen.dart';

import 'package:eagri/presentations/market_price/market_price_view.dart';
import 'package:eagri/presentations/prestations/prestations_home.dart';
import 'package:eagri/presentations/shop/shop_home.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../app/di.dart';

import '../alert/alert_view.dart';
import '../catalogue/catalogue_home.dart';
import '../conseils/first_conseil_view.dart';
import '../conseils/search_conseils_view.dart';
import '../home/home_screen.dart';
import '../login/login_view.dart';
import '../market_price/market_price_details.dart';
import '../meteo/details_meteo_view.dart';
import '../meteo/list_location_meteo.dart';
import '../meteo/recommandation_view.dart';
import '../meteo/type_recommandation_view.dart';
import '../notifications/details_notification_view.dart';
import '../notifications/notifications_view.dart';
import '../sliders/slider_view.dart';
import '../splash/splach_view.dart';
import '../transport/transport_screen.dart';
import 'strings_manager.dart';

class Routes {
  static const String onBoardingRoute = "/onBoardingRoute";
  static const String homeRoute = "/home";
  static const String loginRoute = "/login";
  static const String marketRoute = "/market";
  static const String slider = "/slider";
  static const String profil = "/profil";

  static const String notification = "/notification";
  static const String detailsNotification = "/detailsNotification";
  static const String detailsMarketPrice = "/detailsMarketPrice";
  static const String historyPrice = "/historyPrice";
  static const String meteo = "/meteo";
  static const String detailsMeteo = "/detailsMeteo";
  static const String recommandationMeteoView = "/recommandationMeteoView";
  static const String typeRecommandationMeteoView =
      "/typeRecommandationMeteoView";
  static const String conseils = "/conseils";

  static const String searchConseil = "/searchConseil";
  static const String docView = "/docView";
  static const String alert = "/alert";
  static const String cart = "/cart";
  static const String shop = "/shop";
  static const String detailsProduct = "/detailsProduct";
  static const String catalogueProduit = "/catalogueProduit";
  static const String catalogueDetailProduit = "/catalogueDetailProduit";
  static const String succesPage = "/succesPage";
  static const String prestationHome = "/prestationHome";
  static const String transport = "/transport";
  static const String finance = "/financeHome";
}

class PageGenerator {
  static List<GetPage<dynamic>> listPage() => [
        // GetPage(
        //     name: Routes.detailsProduct,
        //     page: () => const DetailsProduitScreen()),
        GetPage(
          name: Routes.homeRoute,
          page: () => const HomeScreen(),
        ),
        GetPage(
          name: Routes.shop,
          page: () => const ShopHome(),
        ),
        GetPage(
          name: Routes.onBoardingRoute,
          page: () => const OnboardingScreen(),
        ),
        GetPage(
          name: Routes.loginRoute,
          page: () => const LoginView(),
        ),
        GetPage(
          name: Routes.marketRoute,
          page: () => const MarketPriceView(),
        ),
        GetPage(
          name: Routes.profil,
          page: () => const AccountPageView(),
        ),
        GetPage(
          name: Routes.notification,
          page: () => const NotificationsView(),
        ),
        GetPage(
          name: Routes.detailsNotification,
          page: () => const DetailsNotificationView(),
        ),
        GetPage(
          name: Routes.detailsMarketPrice,
          page: () => const MarketPriceDetails(),
        ),
        GetPage(
          name: Routes.meteo,
          page: () => const ListLocationMeteoView(),
        ),
        GetPage(
          name: Routes.detailsMeteo,
          page: () => const DetailsMeteoView(),
        ),
        GetPage(
          name: Routes.recommandationMeteoView,
          page: () => const RecommandationView(),
        ),
        GetPage(
          name: Routes.typeRecommandationMeteoView,
          page: () => const TypeRecommandationView(),
        ),
        GetPage(
          name: Routes.conseils,
          page: () => const FirstConseilView(),
        ),
        GetPage(
          name: Routes.searchConseil,
          page: () => const SearchConseilView(),
        ),
        GetPage(
          name: Routes.alert,
          page: () => const AlertView(),
        ),
        GetPage(
          name: Routes.cart,
          page: () => const CartScreen(),
        ),
        GetPage(
          name: Routes.catalogueProduit,
         
          page: () => const CatalogueHome()
         // page: () => const CatalogueView(),
        ),
        // GetPage(
        //   name: Routes.catalogueDetailProduit,
        //   page: () =>  CatalogueDetailsProduitScreen(),
        // ),
        GetPage(
          name: Routes.succesPage,
          page: () => const SuccessPageView(),
        ),

        GetPage(
          name: Routes.prestationHome,
          page: () => const PrestationHome(),
        ),
        GetPage(
          name: Routes.transport,
          page: () => const TransportScreen(),
        ),
        GetPage(
          name: Routes.finance,
          page: () => const FinanceScreen(),
        ),
      ];
}

class RouteGenerator {
  static Route<dynamic> getRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case Routes.onBoardingRoute:
        return MaterialPageRoute(builder: (_) => const SplashView());
      case Routes.detailsMeteo:
        return MaterialPageRoute(builder: (_) => const DetailsMeteoView());
      case Routes.homeRoute:
        return MaterialPageRoute(builder: (_) => const HomeScreen());
      case Routes.searchConseil:
        return MaterialPageRoute(builder: (_) => const SearchConseilView());
      case Routes.notification:
        return MaterialPageRoute(builder: (_) => const NotificationsView());
      case Routes.detailsMarketPrice:
        return MaterialPageRoute(builder: (_) => const MarketPriceDetails());
      case Routes.recommandationMeteoView:
        return MaterialPageRoute(builder: (_) => const RecommandationView());
      case Routes.detailsNotification:
        return MaterialPageRoute(
            builder: (_) => const DetailsNotificationView());
      case Routes.meteo:
        return MaterialPageRoute(builder: (_) => const ListLocationMeteoView());
      case Routes.conseils:
        return MaterialPageRoute(builder: (_) => const FirstConseilView());
      case Routes.profil:
        return MaterialPageRoute(builder: (_) => const AccountPageView());
      case Routes.cart:
        return MaterialPageRoute(builder: (_) => const CartScreen());
      case Routes.shop:
        return MaterialPageRoute(builder: (_) => const ShopHome());
      // case Routes.detailsProduct:
      //   return MaterialPageRoute(builder: (_) => const DetailsProduitScreen());

      case Routes.typeRecommandationMeteoView:
        return MaterialPageRoute(
            builder: (_) => const TypeRecommandationView());
      case Routes.marketRoute:
        return MaterialPageRoute(builder: (_) => const MarketPriceView());

      case Routes.alert:
        return MaterialPageRoute(builder: (_) => const AddAlertView());
      case Routes.catalogueProduit:
        return MaterialPageRoute(builder: (_) => const CatalogueView());
      case Routes.prestationHome:
        return MaterialPageRoute(builder: (_) => const PrestationHome());
      case Routes.transport:
        return MaterialPageRoute(builder: (_) => const TransportScreen());

      case Routes.loginRoute:
        initLoginModule();
        return MaterialPageRoute(builder: (_) => const LoginView());
      case Routes.slider:
        return MaterialPageRoute(builder: (_) => const OnboardingScreen());

      default:
        return unDefinedRoute();
    }
  }

  static Route<dynamic> unDefinedRoute() {
    return MaterialPageRoute(
        builder: (_) => Scaffold(
              appBar: AppBar(
                title: const Text(AppStrings.noRouteFound),
              ),
              body: const Center(child: Text(AppStrings.noRouteFound)),
            ));
  }
}

class HomeBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<CartController>(CartController());
  }
}
