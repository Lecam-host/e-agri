import 'package:flutter/material.dart';

import 'color_manager.dart';
import 'font_manager.dart';
import 'styles_manager.dart';
import 'values_manager.dart';

ThemeData getThemeDataApp(BuildContext context) {
  return ThemeData(
// useMaterial3: true,
// inputDecorationTheme: InputDecorationTheme(
//       border: OutlineInputBorder(),
//       labelStyle: TextStyle(
//         fontSize: 24.0
//       ),
//     ),
    useMaterial3: false,
    primaryColor: ColorManager.primary,
    primaryColorLight: ColorManager.primaryOpacity70,
    primaryColorDark: ColorManager.darkPrimary,
    disabledColor: ColorManager.grey,
    splashColor: ColorManager.primaryOpacity70,
    fontFamily: "Poppins",

    // cardthme
    cardTheme: CardTheme(
      color: ColorManager.white,
      shadowColor: ColorManager.grey,
      elevation: AppSize.s4,
    ),
    //appbar themea
    appBarTheme: AppBarTheme(
      color: ColorManager.white,
      centerTitle: false,
      elevation: 0,
      shadowColor: ColorManager.primaryOpacity70,
      titleTextStyle: getRegularTextStyle(
        color: ColorManager.black,
        fontSize: FontSize.s16,
      ),
    ),
    buttonTheme: ButtonThemeData(
      shape: const StadiumBorder(),
      disabledColor: ColorManager.black,
      buttonColor: ColorManager.primary,
      splashColor: ColorManager.primaryOpacity70,
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        backgroundColor: ColorManager.primary,
        textStyle: getRegularTextStyle(
          color: ColorManager.black,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
            AppSize.s8,
          ),
        ),
      ),
    ),
    textTheme: Theme.of(context).textTheme.copyWith(
          displayLarge: getSemiBoldTextStyle(
            color: ColorManager.black,
            fontSize: FontSize.s16,
          ),
          titleMedium: getMeduimTextStyle(
            color: ColorManager.black,
            fontSize: FontSize.s14,
          ),
          bodySmall: getRegularTextStyle(
            color: ColorManager.black,
            fontSize: FontSize.s12,
          ),
          bodyLarge: getRegularTextStyle(
            color: ColorManager.black,
            fontSize: FontSize.s12,
          ),
        ),
    inputDecorationTheme: InputDecorationTheme(
      prefixIconColor: ColorManager.primary,
      contentPadding: const EdgeInsets.all(
        AppPadding.p8,
      ),
      hintStyle: getRegularTextStyle(
        color: ColorManager.grey1,
      ),
      labelStyle: getMeduimTextStyle(
        color: ColorManager.darkGrey,
      ),
      errorStyle: getRegularTextStyle(
        color: ColorManager.error,
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: ColorManager.grey,
          width: AppSize.s1_5,
        ),
        borderRadius: const BorderRadius.all(
          Radius.circular(AppSize.s8),
        ),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: ColorManager.primary,
          width: AppSize.s1_5,
        ),
        borderRadius: const BorderRadius.all(
          Radius.circular(AppSize.s8),
        ),
      ),
      errorBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: ColorManager.error,
          width: AppSize.s1_5,
        ),
        borderRadius: const BorderRadius.all(
          Radius.circular(AppSize.s8),
        ),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: ColorManager.primary,
          width: AppSize.s1_5,
        ),
        borderRadius: const BorderRadius.all(
          Radius.circular(AppSize.s8),
        ),
      ),
    ),

    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(
        foregroundColor: ColorManager.primary,
      ),
    ),
    colorScheme: ColorScheme.light(primary: ColorManager.primary)
        .copyWith(background: ColorManager.backgroundColor),
  );
}
