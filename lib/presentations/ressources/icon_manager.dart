import 'package:flutter/material.dart';

class IconManager {
  static const IconData share = Icons.share;
  static const IconData photoCamera = Icons.photo_camera;
  static const IconData gallery = Icons.photo_album;

  static const IconData date = Icons.date_range;
  static const IconData locationPin = Icons.location_on;

  static const IconData clear = Icons.clear;
  static const IconData eye = Icons.remove_red_eye;

  static const IconData search = Icons.search;
  static const IconData sort = Icons.sort_by_alpha;

  static const IconData arrowLef = Icons.arrow_left;
  static const IconData temperature = Icons.thermostat;

  static const IconData arrowRight = Icons.arrow_right;
  static const IconData play = Icons.play_arrow;
  static const IconData playCircle = Icons.play_circle;
  static const IconData pauseCircle = Icons.pause_circle;
  static const IconData video = Icons.video_file;

  static const IconData musique = Icons.music_note;
  static const IconData doc = Icons.file_present;
  static const IconData moreMenu = Icons.menu;
  static const IconData filter = Icons.menu;

  static const IconData question = Icons.question_mark;
  static const IconData orderAlpha = Icons.sort_by_alpha;

  static const IconData request = Icons.request_quote;

  static const IconData forumQuestiom = Icons.forum;
  static const IconData orderDesc = Icons.arrow_downward_sharp;
  static const IconData history = Icons.history;
  static const IconData compare = Icons.compare;

  static const IconData orderAsc = Icons.arrow_upward;
}
