import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';

import '../../app/constant.dart';
import '../../app/functions.dart';
import '../../domain/model/product_paid_model.dart';
import '../common/cache_network_image.dart';
import '../common/default_button.dart';
import '../common/rate/controller/rate_controller.dart';
import '../ressources/color_manager.dart';
import '../ressources/styles_manager.dart';
import 'controllers/vos_avis_constrollers.dart';

class VosAvisScreen extends StatefulWidget {
  const VosAvisScreen({super.key});

  @override
  State<VosAvisScreen> createState() => _VosAvisScreenState();
}

class _VosAvisScreenState extends State<VosAvisScreen> {
  VosAvisController vosAvisController = Get.put(VosAvisController());
  @override
  void initState() {
    vosAvisController.getListProductPaid();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorManager.backgroundColor,
        appBar: AppBar(
          title: const Text("Notes en attente"),
          leading: const BackButtonCustom(),
        ),
        body: Obx(
          () => vosAvisController.initLoadData.value == true
              ? const LoaderComponent()
              : ListView.builder(
                  shrinkWrap: true,
                  itemCount: vosAvisController.listProduct.length,
                  itemBuilder: (context, index) => Padding(
                      padding: const EdgeInsets.symmetric(vertical: 2),
                      child: DataInfoCard(
                          productPaid: vosAvisController.listProduct[index])),
                ),
        ));
  }
}

class DataInfoCard extends StatefulWidget {
  const DataInfoCard({super.key, required this.productPaid});
  final ProductPaidModel productPaid;
  @override
  State<DataInfoCard> createState() => _DataInfoCardState();
}

class _DataInfoCardState extends State<DataInfoCard> {
  final _focusNode = FocusNode();

  getProductInfo() {}
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _focusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: ColorManager.primary4,
      onTap: () {
        // Get.to(
        //   DetailsProduitScreen(
        //     product: ProductDetailsArguments(
        //       product: product,
        //     ),
        //   ),
        // );
      },
      child: Container(
        margin: const EdgeInsets.only(top: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: ColorManager.white,
        ),
        padding: const EdgeInsets.all(5),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (widget.productPaid.product.data.images != null)
              if (widget.productPaid.product.data.images!.isNotEmpty)
                Container(
                  width: 60.0,
                  height: 80,
                  decoration: BoxDecoration(
                    color: ColorManager.grey,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5.0),
                    child: CacheNetworkImage(
                      image: widget.productPaid.product.data.images![0].link,
                    ),
                  ),
                ),
            if (widget.productPaid.product.data.images == null ||
                widget.productPaid.product.data.images!.isEmpty)
              Container(
                width: 60.0,
                height: 80,
                decoration: BoxDecoration(
                  color: ColorManager.grey,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(5.0),
                    child: const Icon(Icons.image)),
              ),
            const SizedBox(
              width: 5,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    widget.productPaid.speculation.name,
                    style: getRegularTextStyle(
                      color: ColorManager.black,
                      fontSize: 14,
                    ),
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Le : ${convertDate(widget.productPaid.createdAt)}",
                        style: getRegularTextStyle(
                          color: ColorManager.grey,
                          fontSize: 12,
                        ),
                      ),
                      SizedBox(
                        width: 120,
                        height: 40,
                        child: TextButton(
                          style: TextButton.styleFrom(
                              foregroundColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5)),
                              backgroundColor: Colors.orange),
                          onPressed: () {
                            rate();
                          },
                          child: Text("Noter et evaluer",
                              style: getRegularTextStyle(
                                  color: ColorManager.white)),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  rate() {
    RateController rateController = Get.put(RateController());

    Get.bottomSheet(
      isScrollControlled: false,
      Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10)),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Entrer votre note",
                style: getBoldTextStyle(color: Colors.black, fontSize: 20),
              ),
              RatingBar.builder(
                initialRating: 0,
                minRating: 0,
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 5,
                itemSize: 50,
                itemPadding: const EdgeInsets.symmetric(horizontal: 1),
                itemBuilder: (context, _) => const Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                onRatingUpdate: (rating) {
                  rateController.rating.value = rating.toInt();
                },
              ),
              const SizedBox(
                height: 30,
              ),
              Text(
                "Entrer un commentaire",
                style: getRegularTextStyle(color: Colors.black, fontSize: 12),
              ),
              const SizedBox(
                height: 10,
              ),
              TextField(
                controller: rateController.commantaire.value,
                maxLines: null,
                focusNode: _focusNode,
                decoration: InputDecoration(
                  // hintText:"La ",
                  label: Text(
                    "Commentaire",
                    style: getRegularTextStyle(color: Colors.grey),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 100,
                    child: DefaultButton(
                      color: ColorManager.grey,
                      text: "Annuler",
                      press: () {
                        Get.back();
                      },
                    ),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  // if (productShow.alreadyNoted == false)
                  SizedBox(
                    width: 100,
                    child: DefaultButton(
                      text: "Envoyer",
                      press: () async {
                        if (rateController.rating.value > 0) {
                          rateController.resourceIdObs.value =  widget.productPaid.product.data.id;
                          rateController.personneNoteId.value =
                              widget.productPaid.product.data.fournisseurId;

                          await rateController.ratingRessource(
                              ServiceCode.marketPlace.toString());

                          // setState(() {
                          //   productShow;
                          // });
                        }
                      },
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
