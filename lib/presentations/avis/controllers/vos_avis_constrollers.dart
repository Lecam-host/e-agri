import 'package:get/get.dart';

import '../../../app/di.dart';
import '../../../controllers/user_controller.dart';
import '../../../domain/model/product_paid_model.dart';
import '../../../service/avis/vos_avis_service.dart';

class VosAvisController extends GetxController {
  VosAvisServiceImpl vosAvisServiceImpl = instance<VosAvisServiceImpl>();
  UserController userController = Get.find();
  var listProduct = <ProductPaidModel>[].obs;
  var initLoadData =true.obs;
  getListProductPaid() async {
     initLoadData.value =true;
    await vosAvisServiceImpl
        .getProductPaid(userController.userInfo.value.id!, 0, 100, "id")
        .then((response) => response.fold((failure) => null, (data) {
              listProduct.value = data.data.items;
            }));

            initLoadData.value=false;
  }
}
