import 'package:eagri/presentations/catalogue/addPrduct/controllers/add_product_controller.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddIntrantChoiseOffreBloc extends StatefulWidget {
  const AddIntrantChoiseOffreBloc({super.key});

  @override
  State<AddIntrantChoiseOffreBloc> createState() =>
      _AddIntrantChoiseOffreBlocState();
}

class _AddIntrantChoiseOffreBlocState extends State<AddIntrantChoiseOffreBloc> {
  @override
  Widget build(BuildContext context) {
    final AddProductController addProductController =
        Get.put(AddProductController());
    return Obx(
      () {
        return addProductController.isLoadTypeOffre.value
            ? const LoaderComponent()
            : Column(
                children: [
                  Text(
                    "Choisssez votre type d'offre",
                    style: getBoldTextStyle(
                      color: Colors.black,
                      fontSize: 20,
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Wrap(
                    children: [
                      ...addProductController.listeTypeOffre.map(
                        (item) {
                          return GestureDetector(
                            onTap: () {
                              addProductController
                                  .productTypeOffreValue.value.text = item;
                              setState(() {});
                            },
                            child: Card(
                              child: Container(
                                alignment: Alignment.center,
                                color: addProductController
                                            .productTypeOffreValue.value.text ==
                                        item
                                    ? Colors.green[200]
                                    : Colors.white,
                                height: 100,
                                width: 100,
                                child: Text(
                                  item,
                                  style: getSemiBoldTextStyle(
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                            ),
                          );
                        },
                      )
                    ],
                  ),
                ],
              );
      },
    );
  }
}
