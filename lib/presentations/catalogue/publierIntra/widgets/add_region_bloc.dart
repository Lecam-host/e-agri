import 'package:eagri/domain/model/addRegionBloc_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:select_form_field/select_form_field.dart';

import '../../../ressources/styles_manager.dart';
import '../../addPrduct/controllers/add_product_controller.dart';

class AddRegionBloc extends StatelessWidget {
  final Function()? callback;
  final AddIntrantModel addIntrantModel = AddIntrantModel();
  final TextEditingController priceController = TextEditingController();
  final TextEditingController quantityController = TextEditingController();
  final TextEditingController regionController = TextEditingController();
  final TextEditingController departementController = TextEditingController();
  final TextEditingController sousPrefectureController =
      TextEditingController();
  final TextEditingController localiteController = TextEditingController();

  AddRegionBloc({
    super.key,
    this.callback,
  });

  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    final AddProductController addProductController =
        Get.put(AddProductController());

    return Form(
      key: formKey,
      child: Column(
        children: [
          // Align(
          //   alignment: Alignment.centerLeft,
          //   child: GestureDetector(
          //     onTap: () {
          //       log(index.toString());
          //       addIntraController.listeRegiomBloc.removeAt(this.);
          //     },
          //     child: Icon(
          //       Icons.remove_circle,
          //       color: Colors.red,
          //       size: 30,
          //     ),
          //   ),
          // ),
          TextFormField(
            // controller: price,
            onChanged: (value) {
              if (value.isNotEmpty) {
                addIntrantModel.priceU = int.parse(value);
              }
            },
            controller: priceController,
            decoration: InputDecoration(
              hintText: "Entrez le prix",
              label: Text(
                "Prix",
                style: getRegularTextStyle(color: Colors.grey),
              ),
            ),
            keyboardType: TextInputType.number,
          ),
          const SizedBox(height: 20),
          TextFormField(
            onChanged: (value) {
              if (value.isNotEmpty) {
                addIntrantModel.quantity = int.parse(value);
              }
            },
            controller: quantityController,
            // controller: addProductController.productQuantityValue.value,
            decoration: InputDecoration(
              hintText: "Entrez la quantité",
              label: Text(
                "Quantité",
                style: getRegularTextStyle(color: Colors.grey),
              ),
            ),
            keyboardType: TextInputType.number,
          ),
          Column(
            children: [
              const SizedBox(
                height: 30,
              ),
              SelectFormField(
                type: SelectFormFieldType.dialog,
                enableSearch: true, // or can be dialog
                //initialValue: 'circle',
                controller: regionController,
                labelText: 'Choisir la region',
                items: addProductController.listRegionMap,
                onChanged: (val) {
                  if (val.isNotEmpty) {
                    addIntrantModel.regionId = int.parse(val);
                    addProductController.fetchDepartement(int.parse(val));
                  }
                },
              ),
              const SizedBox(
                height: 30,
              ),
              Obx(() => addProductController.listeDepartementMap.isEmpty
                  ? Container()
                  : Column(
                      children: [
                        SelectFormField(
                          controller: departementController,
                          type:
                              SelectFormFieldType.dropdown, // or can be dialog
                          labelText: "Choisir le departement",

                          items: addProductController.listeDepartementMap,
                          onChanged: (val) {
                            if (val.isNotEmpty) {
                              // departement = val;
                              addIntrantModel.departementId = int.parse(val);
                              addProductController
                                  .fetchSpFectureOfDepartement(int.parse(val));
                            }
                          },
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                      ],
                    )),
              Obx(
                () => addProductController.listeSousPrefectureMap.isEmpty
                    ? Container()
                    : Column(
                        children: [
                          SelectFormField(
                            controller: sousPrefectureController,
                            type: SelectFormFieldType
                                .dropdown, // or can be dialog
                            labelText: "Choisir la sous-prefercture",
                            items: addProductController.listeSousPrefectureMap,
                            onChanged: (val) {
                              if (val.isNotEmpty) {
                                // sousPrefecture = val;
                                addIntrantModel.sousPrefectureId =
                                    int.parse(val);
                                addProductController
                                    .fecthLocalite(int.parse(val));
                              }
                            },
                          ),
                          const SizedBox(
                            height: 30,
                          )
                        ],
                      ),
              ),
              Obx(
                () => addProductController.listLocaliteMap.isEmpty
                    ? Container()
                    : SelectFormField(
                        controller: localiteController,
                        onChanged: (val) {
                          if (val.isNotEmpty) {
                            addIntrantModel.localiteId = int.parse(val);
                          }
                          // localite = val;
                        },
                        type: SelectFormFieldType.dropdown, // or can be dialog
                        labelText: "Choisir la localité",
                        items: addProductController.listLocaliteMap,
                      ),
              )
            ],
          ),
          //  AddProductLocationBloc(title: false),
          const SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}
