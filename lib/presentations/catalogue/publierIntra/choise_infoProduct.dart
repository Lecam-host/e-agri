// ignore_for_file: file_names

import 'package:eagri/presentations/catalogue/addPrduct/add_picture.dart';
import 'package:eagri/presentations/catalogue/publierIntra/controllers/Add_intra_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:select_form_field/select_form_field.dart';

import '../../common/date_field.dart';
import '../../ressources/styles_manager.dart';
import '../addPrduct/add_product_info_bloc.dart';
import '../addPrduct/controllers/add_product_controller.dart';

class ChoiseInfoProductBloc extends StatefulWidget {
  const ChoiseInfoProductBloc({super.key});

  @override
  State<ChoiseInfoProductBloc> createState() => _ChoiseInfoProductBlocState();
}

class _ChoiseInfoProductBlocState extends State<ChoiseInfoProductBloc> {
  Disponibility disponibilityValue = Disponibility.IMMEDIAT;

  @override
  Widget build(BuildContext context) {
    AddIntraController addIntraController = Get.find();
    AddProductController addProductController = Get.find();
    return SingleChildScrollView(
      child: Column(
        children: [
          Obx(
            () => SelectFormField(
              type: SelectFormFieldType.dialog, // or can be dialog
              enableSearch: true,
              // initialValue: 'circle',
              labelText: "Choisir l'unité",
              controller: addIntraController.intrantUnite.value,
              items: addIntraController.listUniteMap,
              onChanged: (val) {
                addIntraController.intrantUniteId.value = int.parse(val);
              },
            ),
          ),
          const SizedBox(height: 20),
          TextFormField(
            controller: addIntraController.description.value,
            decoration: InputDecoration(
              hintText: "Entrez la quantité",
              label: Text(
                "Décription",
                style: getRegularTextStyle(color: Colors.grey),
              ),
            ),
            keyboardType: TextInputType.multiline,
            maxLines: 3,
          ),
          const SizedBox(height: 20),
          Obx(
            () =>
                addProductController.productTypeOffreValue.value.text == "ACHAT"
                    ? Container()
                    : Column(
                        children: [
                          const Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Le produit est-il disponible maintenant ?",
                            ),
                          ),
                          RadioListTile(
                            title: const Text("Oui"),
                            value: Disponibility.IMMEDIAT,
                            groupValue: disponibilityValue,
                            onChanged: (value) {
                              setState(() {
                                disponibilityValue = Disponibility.IMMEDIAT;
                                addProductController.valueDisponibility.value =
                                    "IMMEDIAT";
                              });
                              // print(addProductController.valueDisponibility);
                            },
                          ),
                          RadioListTile(
                            title: const Text("Non"),
                            value: Disponibility.FUTURE,
                            groupValue: disponibilityValue,
                            onChanged: (value) {
                              setState(
                                () {
                                  disponibilityValue = Disponibility.FUTURE;
                                  addProductController
                                      .valueDisponibility.value = "FUTUR";
                                },
                              );
                              // print(addProductController.valueDisponibility);
                            },
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          addProductController.valueDisponibility.value ==
                                  "IMMEDIAT"
                              ? Container()
                              : DateField(
                                  dateController:
                                      addProductController.dataController.value,
                                  hintText: "Choisir la date de disponibilité",
                                ),
                        ],
                      ),
          ),
          const AddProductImageBloc()
        ],
      ),
    );
  }
}
