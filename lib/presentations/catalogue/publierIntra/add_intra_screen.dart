import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/presentations/catalogue/publierIntra/controllers/Add_intra_controller.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

import '../../../app/constant.dart';
import '../../common/default_button.dart';

class PublierView extends StatelessWidget {
  const PublierView({super.key});

  @override
  Widget build(BuildContext context) {
   // AddProductController addProductController = Get.put(AddProductController());
    AddIntraController addIntraController = Get.put(AddIntraController());
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        title: const Text('Publication'),
        centerTitle: true,
        actions: [
          TextButton(
            onPressed: () {
              Get.back();
            },
            child: Text(
              "Annuler",
              style: getSemiBoldTextStyle(color: ColorManager.primary2),
            ),
          ),
        ],
      ),
      // floatingActionButton: FloatingActionButton(
      //     onPressed: () {
      //       inspect(addIntraController.listeRegiomBloc);
      //     },
      //     child: const Icon(
      //       Icons.arrow_forward_ios,
      //     )),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            Column(
              children: [
                Obx(() {
                  return Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Étape ${addIntraController.currentPage.value + 1}/${addIntraController.listeStep.length}",
                      style: getSemiBoldTextStyle(fontSize: 16),
                    ),
                  );
                }),
                const SizedBox(
                  height: 10,
                ),
                Obx(
                  () {
                    return StepProgressIndicator(
                      totalSteps: addIntraController.listeStep.length,
                      currentStep: addIntraController.currentPage.value + 1,
                      selectedColor: ColorManager.primary,
                      unselectedColor: Colors.grey,
                    );
                  },
                ),
                const SizedBox(
                  height: 10,
                ),
                Obx(
                  () {
                    return addIntraController.currentPage.value <= 0
                        ? Container()
                        : GestureDetector(
                            onTap: () {
                              addIntraController.currentPage.value--;
                            },
                            child: Row(
                              children: [
                                Icon(
                                  Icons.arrow_back_ios,
                                  color: ColorManager.primary2,
                                ),
                                Text(
                                  "Retour",
                                  style: getSemiBoldTextStyle(
                                      fontSize: 16,
                                      color: ColorManager.primary2),
                                ),
                              ],
                            ),
                          );
                  },
                )
              ],
            ),
            const SizedBox(height: 20),
            Expanded(
              child: Obx(() {
                return AnimatedContainer(
                    duration: const Duration(milliseconds: 500),
                    curve: Curves.easeInOut,
                    child: addIntraController
                        .listeStep[addIntraController.currentPage.value]);
              }),
            ),
            const SizedBox(
              height: 10,
            ),
            Obx(() {
              return DefaultButton(
                text: addIntraController.currentPage.value <
                        addIntraController.listeStep.length - 1
                    ? "Suivant"
                    : "Publier",
                press: () async {
                  if (addIntraController.currentPage.value <
                      addIntraController.listeStep.length - 1) {
                    if (addIntraController.intrantSelectId.value == 0) {
                      return Flushbar(
                        title: "Avertissement",
                        message: "Veuillez choisir le l'intrant",
                        duration: const Duration(milliseconds: 3000),
                      ).show(context);
                    }
                    if (addIntraController.intrantUnite.value.text.isEmpty) {
                      return Flushbar(
                        title: "Avertissement",
                        message: "Veuillez choisier l'unité",
                        duration: const Duration(milliseconds: 3000),
                      ).show(context);
                    }
                    if (addIntraController.description.value.text.isEmpty) {
                      return Flushbar(
                        title: "Avertissement",
                        message: "Veuillez entrer la description",
                        duration: const Duration(milliseconds: 3000),
                      ).show(context);
                    }
                    if (addIntraController.methodPaiementId.value==0 && addIntraController.currentPage.value== addIntraController.listeStep.length - 1) {
                      return Flushbar(
                        title: "Avertissement",
                        message: "Choisir la methode de paiement",
                      ).show(context);
                    }
                   
                   
                    addIntraController.currentPage.value++;
                  } else {
                       if (addIntraController.methodPaiementId.value==0 ) {
                      return Flushbar(
                        title: "Avertissement",
                        message: "Choisir la methode de paiement",
                      ).show(context);
                    }
                     else  if (addIntraController
                                              .methodPayment.value.code ==
                                          codePaiementCredit &&
                                      addIntraController.nbPayementCreditValue
                                          .value.text.isEmpty) {
                                    return Flushbar(
                                      title: "Avertissement",
                                      message: "Precisez la fréquence",
                                      duration:
                                          const Duration(milliseconds: 3000),
                                    ).show(context);
                                  }
                              else    if (addIntraController
                                              .methodPayment.value.code ==
                                          codePaiementCredit &&
                                      addIntraController
                                          .unitNbPayementCreditValue
                                          .value
                                          .text
                                          .isEmpty) {
                                    return Flushbar(
                                      title: "Avertissement",
                                      message: "Precisez l'unité",
                                      duration:
                                          const Duration(milliseconds: 3000),
                                    ).show(context);
                                  }else{
                                    await addIntraController.addIntrant();
                                  }
                    
                  
                    // Get.to(SuccessPageView());
                    // Get.defaultDialog(
                    //   title: "Succès",
                    //   titleStyle: TextStyle(
                    //     color: ColorManager.primary2,
                    //   ),
                    //   onConfirm: () {},
                    //   onCancel: () {
                    //     // Get.to(SuccessPageView());
                    //   },
                    //   textCancel: "Non",
                    //   cancelTextColor: ColorManager.primary,
                    //   textConfirm: "Oui",
                    //   buttonColor: ColorManager.primary,
                    //   content: const Text(
                    //     "Voulez vous publiez le même produit dans d'autre région ?",
                    //     textAlign: TextAlign.center,
                    //   ),
                    // );

                  }
                },
              );
            }),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }
}
