import 'package:flutter/material.dart';

import '../../common/search_field.dart';
import '../../ressources/styles_manager.dart';

class ChoiseProductBloc extends StatelessWidget {
  const ChoiseProductBloc({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            "Produit",
            style: getSemiBoldTextStyle(
              color: Colors.black,
              fontSize: 20,
            ),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        const SearchField(
          hintText: "Rechercher un produit",
        ),
        const ListTile(
          title: Text(
            "Agricole",
          ),
        )
      ],
    );
  }
}
