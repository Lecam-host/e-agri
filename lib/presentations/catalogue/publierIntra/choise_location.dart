import 'dart:developer';

import 'package:eagri/presentations/catalogue/publierIntra/controllers/Add_intra_controller.dart';
import 'package:eagri/presentations/catalogue/publierIntra/widgets/add_region_bloc.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../common/default_button.dart';

class ChoiseLocationBloc extends StatelessWidget {
  const ChoiseLocationBloc({super.key});

  @override
  Widget build(BuildContext context) {
    AddIntraController addIntraController = Get.find();
    return Obx(() {
      return SingleChildScrollView(
        child: Column(
          children: [
            Align(
              child: Text(
                "Ajouter pour un ou plusieurs lieux",
                style: getBoldTextStyle(fontSize: 17),
              ),
            ),
            const SizedBox(height: 20),
            for (int i = 0;
                i < addIntraController.listeRegiomBloc.length;
                i++) ...[
              Column(
                children: [
                  Align(
                    alignment: Alignment.centerRight,
                    child: GestureDetector(
                      onTap: () {
                        addIntraController.removeItemListBloc(i);
                        inspect(addIntraController.listeRegiomBloc);
                      },
                      child: Row(
                        children: [
                          const Icon(
                            Icons.remove_circle,
                            color: Colors.red,
                            size: 30,
                          ),
                          const SizedBox(width: 7),
                          Text(
                            "Lieu ${i + 1}",
                            style: getMeduimTextStyle(fontSize: 16),
                          )
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(height: 10),
                  Container(
                    child: addIntraController.listeRegiomBloc[i],
                  )
                ],
              )
            ],
            DefaultButton(
              color: ColorManager.blueLight,
              text: "Ajouter une region",
              press: () {
                addIntraController.listeRegiomBloc.add(AddRegionBloc());
              },
            )
          ],
        ),
      );
    });
  }
}
