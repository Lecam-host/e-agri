// ignore_for_file: file_names

import 'dart:developer';
import 'dart:io';

import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/app/constant.dart';
import 'package:eagri/app/di.dart';
import 'package:eagri/app/functions.dart';
import 'package:eagri/data/request/intran_request.dart';

import 'package:eagri/domain/model/addRegionBloc_model.dart';
import 'package:eagri/domain/model/typeCulture_model.dart';
import 'package:eagri/presentations/catalogue/controllers/catalogue_controller.dart';
import 'package:eagri/presentations/catalogue/publierIntra/widgets/add_region_bloc.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/common/state/succes_page_view.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../app/app_prefs.dart';
import '../../../../data/repository/api/repository_impl.dart';
import '../../../../domain/model/intrant_model.dart';
import '../../../../domain/model/methode_paiement_model.dart';
import '../../../../domain/model/user_model.dart';
import '../../addPrduct/controllers/add_product_controller.dart';
import '../choise_categorie.dart';
import '../choise_infoProduct.dart';
import '../choise_location.dart';
import '../type_paiement_bloc.dart';

class AddIntraController extends GetxController {
  AppSharedPreference appSharedPreference = instance<AppSharedPreference>();
  var userInfo = UserModel().obs;
  CatalogueController catalogueController = Get.find();
  final files = <File>[].obs;
  final currentPage = 0.obs;
  final listTypeCultures = <ItemTypeCulture>[].obs;
  final listTypeCulturesCopy = <ItemTypeCulture>[].obs;
  var nbPayementCreditValue = TextEditingController().obs;

  var intrantSelectId = 0.obs;
  var intrantUnite = TextEditingController().obs;
  var intrantUniteId = 0.obs;

  var description = TextEditingController().obs;
  var price = TextEditingController().obs;
  var quantity = TextEditingController().obs;
  var regionId = TextEditingController().obs;
  var souspPrefectureId = TextEditingController().obs;
  var departementId = TextEditingController().obs;
  var localiteId = TextEditingController().obs;

  final listIntrant = <ItemIntrant>[].obs;
  final listIntrantCopy = <ItemIntrant>[].obs;

  final loadTypeCultures = false.obs;

  final loadIntrant = false.obs;
  final loadAddIntrant = false.obs;

  final typeCulturesSelectId = 0.obs;
  var methodPaiementId = 0.obs;
  var listMethodPaiement = <PaymentMethodModel>[];
  var unitNbPayementCreditValue = TextEditingController().obs;
  var methodPayment = PaymentMethodModel().obs;
  var listeUnitNbPayementCrediMap = <Map<String, dynamic>>[].obs;

  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
Future fetchMethodPaiement() async {
    (await repositoryImpl.getMethodPaiementAll()).fold(
        (l) => log(l.toString()), (r) => listMethodPaiement = r.data!.items!);
  }
  final listeStep = [
    const ChoiseCategorieBloc(),
    // ChoiseProductBloc(),
    const ChoiseInfoProductBloc(),
    // const AddIntrantChoiseOffreBloc(),

    const ChoiseLocationBloc(),
   const TypePaiementBloc(),
  ];
  final intrant = <AddIntrantModel>[];

  final addIntrantRequest = AddIntrantRequestModel().obs;

  final listeRegiomBloc = <AddRegionBloc>[AddRegionBloc()].obs;
  final listDataAddIntrant = <DataAddIntrant>[];
  AddProductController addProductController = Get.put(AddProductController());
  var listUniteMap = <Map<String, dynamic>>[].obs;

  @override
  void onInit() async {
    fetchIntrantAll();fetchMethodPaiement();
    userInfo.value = await appSharedPreference.getUserInformation();
    getUnite();
    getListUnitTime();
    super.onInit();
  }

  getUnite() {
    repositoryImpl.geIntrantUnits().then((response) {
      response.fold((l) {}, (listUnite) {
        listUniteMap.value = listUnite.data
            .map((element) => {"value": element.id, "label": element.name})
            .toList();
      });
    });
  }

  fetchTypeCultures() async {
    loadTypeCultures.value = true;
    await repositoryImpl.getTypeCulturesAll().then((value) {
      value.fold((l) {
        loadTypeCultures.value = false;
      }, (r) {
        listTypeCultures.addAll(r.data!.items!);
        listTypeCulturesCopy.addAll(r.data!.items!);
        loadTypeCultures.value = false;
      });
    });
  }
getListUnitTime()async{
  await repositoryImpl.getUnitTime().then((response){
    response.fold((failure) {}, (data) {
    listeUnitNbPayementCrediMap.value=  data.data
        .map((e) => {"value": e, "label": e.toCapitalize()})
        .toList();
     
      inspect(listeUnitNbPayementCrediMap);
    });
  });
}
  //fetchIntrantAll
  fetchIntrantAll() async {
    loadIntrant.value = true;
    await repositoryImpl.getIntrantAll().then((value) {
      value.fold((l) {
        loadIntrant.value = false;
      }, (r) {
        listIntrant.addAll(r.data!.items!);
        listIntrantCopy.addAll(r.data!.items!);
        loadIntrant.value = false;
      });
    });
  }

  seacrhTypeCultures(String query) {
    if (query.isNotEmpty) {
      final newList = listTypeCultures
          .where((item) =>
              item.libelle!.toLowerCase().contains(query.toLowerCase()))
          .toList();

      listTypeCulturesCopy.value = newList;
    } else {
      listTypeCulturesCopy.value = listTypeCultures;
    }
  }

  removeItemListBloc(int index) {
    listeRegiomBloc.removeAt(index);
  }

  seacrhIntrantAll(String query) {
    if (query.isNotEmpty) {
      final newList = listIntrant
          .where((item) =>
              item.libelle!.toLowerCase().contains(query.toLowerCase()))
          .toList();
      listIntrantCopy.value = newList;
    } else {
      listIntrantCopy.value = listIntrant;
    }
  }

  convertIntrant() {
    log(intrantUnite.value.text);
    listDataAddIntrant.clear();
    for (var element in listeRegiomBloc) {
      listDataAddIntrant.add(DataAddIntrant(
          price: element.addIntrantModel.priceU.toString(),
          payment: methodPaiementId.value.toString(),
          unitOfMeasurment: intrantUniteId.value,
          
          quantity: element.addIntrantModel.quantity.toString(),
          localisation: LocalisationAddIntrant(
              departementId: element.addIntrantModel.departementId,
              localiteId: element.addIntrantModel.localiteId,
              regionId: element.addIntrantModel.regionId,
              sprefectureId: element.addIntrantModel.sousPrefectureId)));
    }
    addIntrantRequest.value = AddIntrantRequestModel(
      availabilityDate:
          addProductController.valueDisponibility.value == "IMMEDIAT"
              ? null
              : addProductController.dataController.value.text,
      // certification: "",
      // expirationDate: null,
      fournisseurId: userInfo.value.id,
      images: addProductController.files,
      offerType: "VENTE",
      intrantId: intrantSelectId.value,
      intrantCategorieId: intrantSelectId.value,

      availability: addProductController.valueDisponibility.value,
      description: description.value.text,
      data: listDataAddIntrant,
    );
  }

  addIntrant() async {
    Get.dialog(const LoaderComponent());
    //  await createCatalogue();
    convertIntrant();
    inspect(addIntrantRequest.value);
    await repositoryImpl.addIntrant(addIntrantRequest.value).then((value) {
      value.fold((l) {
        inspect(l);
        Get.back();
        showCustomFlushbar(Get.context!, "Publication échoué",
            ColorManager.error, FlushbarPosition.TOP);
      }, (r) {
        catalogueController.getListFnCodeProduct(codeIntrant.toString());
        loadAddIntrant.value = false;
        Get.back();
        Get.off(const SuccessPageView(
          message: "Publié avec succes !",
        ));
      });
    });
  }
}
