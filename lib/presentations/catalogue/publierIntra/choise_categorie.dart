import 'package:eagri/presentations/catalogue/publierIntra/controllers/Add_intra_controller.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../common/search_field.dart';
import '../../ressources/styles_manager.dart';

class ChoiseCategorieBloc extends StatelessWidget {
  const ChoiseCategorieBloc({super.key});

  @override
  Widget build(BuildContext context) {
    AddIntraController addIntraController = Get.find();
    return Column(
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            "Choisir un intrant",
            style: getSemiBoldTextStyle(
              color: Colors.black,
              fontSize: 20,
            ),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        SearchField(
          hintText: "Rechercher un intrant",
          onChanged: (value) {
            addIntraController.seacrhIntrantAll(value);
          },
        ),
        Expanded(
          child: Obx(() {
            return addIntraController.loadIntrant.value
                ? const LoaderComponent()
                : ListView(
                    physics: const BouncingScrollPhysics(),
                    children: [
                      ...addIntraController.listIntrantCopy.map(
                        (element) {
                          return ListTile(
                            selectedColor:
                                addIntraController.intrantSelectId.value ==
                                        element.id
                                    ? Colors.green[200]
                                    : null,
                            selected:
                                addIntraController.intrantSelectId.value ==
                                    element.id,
                            onTap: () {
                              addIntraController.intrantSelectId.value =
                                  element.id!;
                              addIntraController.currentPage.value++;
                            },
                            title: Text(
                              element.libelle!,
                            ),
                          );
                        },
                      ),
                    ],
                  );
          }),
        )
      ],
    );
  }
}
