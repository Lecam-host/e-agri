import 'dart:developer';

import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/app/constant.dart';
import 'package:eagri/app/di.dart';
import 'package:eagri/data/repository/api/repository_impl.dart';
import 'package:eagri/data/request/shop_resquests.dart';
import 'package:eagri/data/request/transport_resquest.dart';
import 'package:eagri/domain/model/Product.dart';
import 'package:eagri/domain/model/location_model.dart';
import 'package:eagri/domain/usecase/location_usecase.dart';
import 'package:eagri/domain/usecase/speculation_price_usecase.dart';
import 'package:eagri/presentations/catalogue/controllers/catalogue_controller.dart';
import 'package:eagri/presentations/common/state_renderer/state_render_impl.dart';
import 'package:eagri/presentations/common/state_renderer/state_renderer.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../data/request/prestation_request.dart';
import '../../../common/flushAlert_componenent.dart';
import '../../../common/state/loader_component.dart';
import '../../../ressources/color_manager.dart';

class UpdateProductController extends GetxController {
  LocationUsecase locationUsecase = instance<LocationUsecase>();

  SpeculationPriceUseCase speculationPriceUseCase =
      instance<SpeculationPriceUseCase>();

  int productId = 0;
  // var regionId ;
  var productDescription = TextEditingController();
  var matriculeController = TextEditingController();

  var productPrice = TextEditingController();
  var productQuatity = TextEditingController();
  var productLocalite = TextEditingController();
  var productRegion = TextEditingController();

  var productSousPrefecture = TextEditingController();
  var productDepartement = TextEditingController();

  var listDepartement = <DepartementModel>[].obs;
  var listSousPrefecture = <SousPrefectureModel>[].obs;
  var listLocalite = <LocaliteModel>[].obs;
  var listRegion = <RegionModel>[];

  var listDepartementMap = <Map<String, dynamic>>[].obs;
  var listSousPrefectureMap = <Map<String, dynamic>>[].obs;
  var listRegionMap = <Map<String, dynamic>>[].obs;
  var listLocaliteMap = <Map<String, dynamic>>[].obs;
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  CatalogueController catalogueController = Get.find();

  @override
  void onInit() {
    getRegion(Get.context);
    super.onInit();
  }

  updatePrestation(UpdatePrestationRequest updatePrestationRequest,
      ProductModel product) async {
    ProductModel newProduct = product;

    await repositoryImpl
        .updatePrestation(updatePrestationRequest)
        .then((value) => value.fold((l) {
              Get.back();
              showCustomFlushbar(Get.context!, "Échec de la modification",
                  ColorManager.error, FlushbarPosition.TOP);
            }, (newProd) async {
              log("New Product : $newProd");
              var data = newProd;
              inspect(data);
              await repositoryImpl
                  .getDetailProduct(product.id)
                  .then((response) async {
                await response.fold((l) => null, (data) async {
                  newProduct = (await data)!;
                });
              });
              Get.close(2);

              showCustomFlushbar(Get.context!, "Produit modifié avec succés",
                  ColorManager.succes, FlushbarPosition.TOP);
              // catalogueController.fetchProductFournisseur();
            }));
    return newProduct;
  }

  Future<ProductModel> modifProduct(ProductModel product) async {
    ProductModel newProduct = product;
    Get.dialog(const LoaderComponent());
    if (product.code == codePrestation.toString()) {
      await repositoryImpl
          .updatePrestation(UpdatePrestationRequest(
              prestationId: product.id,
              description: productDescription.value.text,
              paymentId: product.paymentId,
              availability: product.disponibilite,
              availabilityDate: product.dataDisponibilite,
              priceU: int.parse(
                productPrice.value.text,
              ),
              location: [
                int.parse(productRegion.value.text),
                productDepartement.value.text == "" ||
                        productDepartement.value.text == "null"
                    ? 0
                    : int.parse(productDepartement.value.text),
                productSousPrefecture.value.text == "" ||
                        productSousPrefecture.value.text == "null"
                    ? 0
                    : int.parse(productSousPrefecture.value.text),
                productLocalite.value.text == "" ||
                        productLocalite.value.text == "null"
                    ? 0
                    : int.parse(productLocalite.value.text),
              ]))
          .then((value) => value.fold((l) {
                Get.back();

                showCustomFlushbar(Get.context!, "Échec de la modification",
                    ColorManager.error, FlushbarPosition.TOP);
              }, (newProd) async {
                await repositoryImpl
                    .getDetailProduct(product.id)
                    .then((response) async {
                  await response.fold((l) => null, (data) async {
                    newProduct = (await data)!;
                  });
                });
                Get.close(2);

                showCustomFlushbar(Get.context!, "Produit modifié avec succés",
                    ColorManager.succes, FlushbarPosition.TOP);
                // catalogueController.fetchProductFournisseur();
              }));
      return newProduct;
    } else if (product.code == codeTransport.toString()) {
      UpdateTransportRequest updateTransportRequest = UpdateTransportRequest(
        fournisseurId: catalogueController.userInfo.value.id!,
        idProduct: productId,
        price: int.parse(productPrice.value.text),
        description: productDescription.value.text,
        capacite: int.parse(productQuatity.value.text),
        matricule: matriculeController.value.text,
      );
      await repositoryImpl
          .updateTransport(updateTransportRequest)
          .then((value) => value.fold((l) {
                Get.back();

                showCustomFlushbar(Get.context!, "Échec de la modification",
                    ColorManager.error, FlushbarPosition.TOP);
              }, (newProd) async {
                await repositoryImpl
                    .getDetailProduct(product.id)
                    .then((response) async {
                  await response.fold((l) => null, (data) async {
                    newProduct = (await data)!;
                  });
                });
                Get.close(2);

                showCustomFlushbar(Get.context!, "Produit modifié avec succés",
                    ColorManager.succes, FlushbarPosition.TOP);
                // catalogueController.fetchProductFournisseur();
              }));

      return newProduct;
    } else {
      UpdateProductOnMarketrequest updateRequest = UpdateProductOnMarketrequest(
        fournisseurId: catalogueController.userInfo.value.id!,
        idProduct: productId,
        locationId: productLocalite.value.text == "" ||
                productLocalite.value.text == "null"
            ? null
            : int.parse(productLocalite.value.text),
        description: productDescription.value.text,
        price: int.parse(productPrice.value.text),
        quantity: int.parse(productQuatity.value.text),
        regionId:
            productRegion.value.text == "" || productRegion.value.text == "null"
                ? null
                : int.parse(productRegion.value.text),
        departementId: productDepartement.value.text == "" ||
                productDepartement.value.text == "null"
            ? null
            : int.parse(productDepartement.value.text),
        sousPrefectureId: productSousPrefecture.value.text == "" ||
                productSousPrefecture.value.text == "null"
            ? null
            : int.parse(productSousPrefecture.value.text),
      );

      await repositoryImpl
          .updateProductIntrant(updateRequest)
          .then((value) => value.fold((l) {
                Get.back();

                showCustomFlushbar(Get.context!, "Échec de la modification",
                    ColorManager.error, FlushbarPosition.TOP);
              }, (newProd) async {
                await repositoryImpl
                    .getDetailProduct(product.id)
                    .then((response) async {
                  await response.fold((l) => null, (data) async {
                    newProduct = (await data)!;
                  });
                });
                Get.close(2);

                showCustomFlushbar(Get.context!, "Produit modifié avec succés",
                    ColorManager.succes, FlushbarPosition.TOP);
                // catalogueController.fetchProductFournisseur();
              }));

      return newProduct;
    }
  }

  updateProduct(ProductModel productModel) async {
    await catalogueController.modifProduct(
      UpdateProductOnMarketrequest(
        fournisseurId: catalogueController.userInfo.value.id!,
        idProduct: productId,
        locationId: productLocalite.value.text.isEmpty
            ? null
            : int.parse(productLocalite.value.text),
        description: productDescription.value.text,
        price: int.parse(productPrice.value.text),
        quantity: int.parse(productQuatity.value.text),
        regionId: productRegion.value.text.isEmpty
            ? null
            : int.parse(productRegion.value.text),
        departementId: productDepartement.value.text.isEmpty
            ? null
            : int.parse(productDepartement.value.text),
        sousPrefectureId: productSousPrefecture.value.text.isEmpty
            ? null
            : int.parse(productSousPrefecture.value.text),
      ),
      productModel,
    );
  }

  Future getRegion(context) async {
    LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE)
        .getScreenWidget(context, Container(), () {});
    (await speculationPriceUseCase.getListRegion()).fold((failure) {
      Navigator.of(context).pop();
    }, (result) {
      listRegion = result.data;
      Navigator.of(context).pop();
    });
    inspect(listRegion);
    listRegionMap.value = listRegion
        .map((element) => {"value": element.regionId, "label": element.name})
        .toList();
  }

  fetchDepartement(int idRegion) async {
    (await locationUsecase.getListDepartement(idRegion)).fold(
      (l) => null,
      (r) {
        productDepartement.text = "";
        listDepartement.value = r.listDepartement!;
        listDepartementMap.clear();
        inspect(r.listDepartement);
        listDepartementMap.value = listDepartement
            .map((element) =>
                {"value": element.departementId, "label": element.name})
            .toList();
      },
    );
  }

  fetchSpFectureOfDepartement(int idDepartement) async {
    (await locationUsecase.getListSpOfDepartement(idDepartement)).fold(
      (l) => null,
      (r) {
        listSousPrefecture.value = r.listSousPrefecture!;
        inspect(r.listSousPrefecture);
        listSousPrefectureMap.value = listSousPrefecture
            .map((element) =>
                {"value": element.sousPrefectureId, "label": element.name})
            .toList();
      },
    );
  }

  fecthLocalite(int idSp) async {
    (await locationUsecase.getListLocaliteOfSp(idSp)).fold(
      (l) => null,
      (r) {
        listLocalite.value = r.listLocalite!;
        inspect(r.listLocalite);
        listLocaliteMap.value = listLocalite
            .map((element) =>
                {"value": element.localiteId, "label": element.name})
            .toList();
      },
    );
  }
}
