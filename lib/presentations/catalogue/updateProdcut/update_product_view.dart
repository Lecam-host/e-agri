import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/data/repository/local/speculation_repo.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/domain/usecase/shop_usecase.dart';
import 'package:eagri/presentations/catalogue/addPrduct/app_bar.dart';
import 'package:eagri/presentations/catalogue/addPrduct/controllers/add_product_controller.dart';
import 'package:eagri/presentations/catalogue/updateProdcut/update_product_choise_methodPaiement_bloc.dart';
import 'package:eagri/presentations/catalogue/updateProdcut/update_product_choisi_offre_bloc%20copie.dart';
import 'package:eagri/presentations/catalogue/updateProdcut/update_product_info_bloc%20copie.dart';
import 'package:eagri/presentations/catalogue/updateProdcut/update_product_location_bloc%20copie.dart';
import 'package:eagri/presentations/common/default_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../app/di.dart';

class UpdateProductView extends StatefulWidget {
  const UpdateProductView({Key? key}) : super(key: key);

  @override
  State<UpdateProductView> createState() => _UpdateProductViewState();
}

class _UpdateProductViewState extends State<UpdateProductView> {
  ShopUseCase shopUseCase = instance<ShopUseCase>();
  // AddProductOnMarketrequest addProductOnMarketrequest =AddProductOnMarketrequest();
  List<SpeculationModel> listSpeculation = [];
  getListSpeculation() {
    SpeculationRepo speculationRepo = SpeculationRepo();
    speculationRepo.getAllItem().then((value) {
      setState(() {
        listSpeculation = value;
      });
    });
  }

  final pageController = PageController();
  final addProductController = Get.put(AddProductController());

  @override
  void initState() {
    getListSpeculation();
    addProductController.getRegion(context);
    super.initState();
  }

  @override
  void dispose() {
    addProductController.pageIndex.value = 0;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final keyBoardIsOpen = MediaQuery.of(context).viewInsets.bottom > 0;
    return Scaffold(
      // floatingActionButton: FloatingActionButton(onPressed: ()async {
      //   inspect(addProductController.listeSpeculation);
      // },),
      appBar: appBarCustom(),
      body: SafeArea(
        child: Container(
          margin: const EdgeInsets.all(10),
          child: PageView(
            onPageChanged: (value) {
              addProductController.pageIndex.value = value;
            },
            controller: pageController,
            physics: const NeverScrollableScrollPhysics(),
            children: const [
              UpdateProductChoiseOffreBloc(),
              UpdateProductInfoBloc(),
              UpdateProductLocationBloc(),
              // UpdateProductImageBloc(),
              UpdateProductChoiseMethodPaiementBloc()
            ],
          ),
        ),
      ),
      bottomSheet: keyBoardIsOpen
          ? Container(
              height: 0,
            )
          : Container(
              margin: const EdgeInsets.only(
                  left: 10, right: 10, top: 10, bottom: 30),
              child: Obx(
                () => Row(
                  children: [
                    addProductController.pageIndex >= 1
                        ? Expanded(
                            child: DefaultButton(
                              text: "Retour",
                              // text: pageController.page!.toInt().toString(),
                              color: Colors.black,
                              press: () {
                                pageController.previousPage(
                                    duration: const Duration(milliseconds: 300),
                                    curve: Curves.bounceInOut);
                              },
                            ),
                          )
                        : Container(
                            height: 50,
                          ),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: DefaultButton(
                        text: addProductController.pageIndex >= 4
                            ? "Publier"
                            : "Suivant",
                        press: () {
                          if (addProductController
                              .productTypeOffreValue.value.text.isEmpty) {
                            // print(object)
                            return Flushbar(
                              title: "Avertissement",
                              message: "Veuillez choisir le type d'offre",
                              duration: const Duration(milliseconds: 3000),
                            ).show(context);
                          }
                          if (addProductController.pageIndex >= 1) {
                            if (addProductController
                                .productTextValue.value.text.isEmpty) {
                              // print(object)
                              return Flushbar(
                                title: "Avertissement",
                                message: "Veuillez choisir le produit",
                                duration: const Duration(milliseconds: 3000),
                              ).show(context);
                            }
                            if (addProductController
                                .productUniteTextValue.value.text.isEmpty) {
                              // print(object)
                              return Flushbar(
                                title: "Avertissement",
                                message: "Veuillez entrer l'unité du produit",
                                duration: const Duration(milliseconds: 3000),
                              ).show(context);
                            }
                            if (addProductController
                                .productDescriptionValue.value.text.isEmpty) {
                              // print(object)
                              return Flushbar(
                                title: "Avertissement",
                                message:
                                    "Veuillez entrer la description du produit",
                                duration: const Duration(milliseconds: 3000),
                              ).show(context);
                            }
                            if (addProductController
                                .productQuantityValue.value.text.isEmpty) {
                              // print(object)
                              return Flushbar(
                                title: "Avertissement",
                                message:
                                    "Veuillez entrer la quantité du produit",
                                duration: const Duration(milliseconds: 3000),
                              ).show(context);
                            }
                            if (addProductController
                                .productPriceValue.value.text.isEmpty) {
                              // print(object)
                              return Flushbar(
                                title: "Avertissement",
                                message: "Veuillez entrer le prix du produit",
                                duration: const Duration(milliseconds: 3000),
                              ).show(context);
                            }
                            if (addProductController
                                .productTypeVenteValue.value.text.isEmpty) {
                              // print(object)
                              return Flushbar(
                                title: "Avertissement",
                                message:
                                    "Veuillez entrer le type de vente du produit",
                                duration: const Duration(milliseconds: 3000),
                              ).show(context);
                            }
                          }
                          pageController.nextPage(
                            duration: const Duration(milliseconds: 300),
                            curve: Curves.bounceInOut,
                          );
                          if (addProductController.pageIndex >= 4) {
                            addProductController.sendProduct();
                          }
                        },
                      ),
                    )
                  ],
                ),
              ),
            ),
    );
  }
}
