// ignore_for_file: file_names

import 'package:eagri/presentations/catalogue/addPrduct/controllers/add_product_controller.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UpdateProductChoiseMethodPaiementBloc extends StatelessWidget {
  const UpdateProductChoiseMethodPaiementBloc({super.key});

  @override
  Widget build(BuildContext context) {
    final AddProductController addProductController = Get.find();
    return Column(
      children: [
        Text(
          "Choissez votre type d'offre",
          style: getBoldTextStyle(
            color: Colors.black,
            fontSize: 20,
          ),
        ),
        const SizedBox(
          height: 30,
        ),
        Wrap(
          children: [
            ...addProductController.listMethodPaiement.map(
              (item) {
                return GestureDetector(
                  onTap: () {
                    addProductController.methodPaiementId.value = item.id!;
                  },
                  child: Card(
                    child: Obx(
                      () {
                        return Container(
                          alignment: Alignment.center,
                          color: addProductController.methodPaiementId.value ==
                                  item.id
                              ? Colors.green[200]
                              : Colors.white,
                          height: 100,
                          width: 100,
                          child: Text(
                            item.name.toString(),
                            style: getSemiBoldTextStyle(
                              color: Colors.black,
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                );
              },
            )
          ],
        ),
      ],
    );
  }
}
