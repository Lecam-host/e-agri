// ignore_for_file: constant_identifier_names, file_names

import 'package:eagri/presentations/catalogue/addPrduct/controllers/add_product_controller.dart';
import 'package:eagri/presentations/common/date_field.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:select_form_field/select_form_field.dart';

enum Disponibility { FUTURE, IMMEDIAT }

class UpdateProductInfoBloc extends StatefulWidget {
  const UpdateProductInfoBloc({super.key});

  @override
  State<UpdateProductInfoBloc> createState() => _UpdateProductInfoBlocState();
}

class _UpdateProductInfoBlocState extends State<UpdateProductInfoBloc> {
  Disponibility disponibilityValue = Disponibility.IMMEDIAT;

  @override
  Widget build(BuildContext context) {
    AddProductController addProductController = Get.find();

    return ListView(
      padding: const EdgeInsets.only(bottom: 80),
      children: [
        const SizedBox(
          height: 30,
        ),
        Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Information sur le produit",
              style: getBoldTextStyle(color: Colors.black, fontSize: 19),
            )),
        const SizedBox(
          height: 30,
        ),
        SelectFormField(
          type: SelectFormFieldType.dialog, // or can be dialog
          enableSearch: true,
          // initialValue: 'circle',
          labelText: 'Choisir le produit',

          controller: addProductController.productTextValue.value,
          items: addProductController.listeSpeculationMap,
          onChanged: (val) {
            setState(() {});
            addProductController.changeUniteBySpeculation(int.parse(val));
          },
        ),
        const SizedBox(
          height: 30,
        ),
        Obx(
          () => addProductController.listUnite.isEmpty
              ? Container()
              : Column(
                  children: [
                    SelectFormField(
                      type: SelectFormFieldType.dropdown, // or can be dialog
                      labelText: "Choisir l'unité du produit",
                      controller:
                          addProductController.productUniteTextValue.value,
                      items: addProductController.listUniteMap,
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                  ],
                ),
        ),
        TextFormField(
          controller: addProductController.productDescriptionValue.value,
          decoration: InputDecoration(
            hintText: "Entrez une description",
            label: Text(
              "Description",
              style: getRegularTextStyle(color: Colors.grey),
            ),
          ),
          keyboardType: TextInputType.text,
        ),
        const SizedBox(
          height: 30,
        ),
        TextFormField(
          controller: addProductController.productQuantityValue.value,
          decoration: InputDecoration(
            hintText: "Entrez la quantité",
            label: Text(
              "Quantité",
              style: getRegularTextStyle(color: Colors.grey),
            ),
          ),
          keyboardType: TextInputType.number,
        ),
        const SizedBox(
          height: 30,
        ),
        TextFormField(
          controller: addProductController.productPriceValue.value,
          decoration: InputDecoration(
            hintText: "Entrez le prix",
            label: Text(
              "Prix",
              style: getRegularTextStyle(color: Colors.grey),
            ),
          ),
          keyboardType: TextInputType.number,
        ),
        const SizedBox(
          height: 30,
        ),
        SelectFormField(
          type: SelectFormFieldType.dropdown, // or can be dialog
          // initialValue: 'circle',
          labelText: 'Type de vente',
          controller: addProductController.productTypeVenteValue.value,
          items: addProductController.listeTypeProductMap,
        ),
        const SizedBox(
          height: 30,
        ),
        // SelectFormField(
        //   type: SelectFormFieldType.dropdown, // or can be dialog
        //   // initialValue: 'circle',
        //   labelText: "Type d'offre",
        //   controller: addProductController.productTypeOffreValue.value,
        //   items: addProductController.listeTypeOffreMap,
        //   onChanged: (val) => print(val),
        //   onSaved: (val) => print(val),
        // ),
        // const SizedBox(
        //   height: 30,
        // ),
        // const Align(
        //   alignment: Alignment.centerLeft,
        //   child: Text(
        //     "Le produit est-il disponible maintenant ?",
        //   ),
        // ),
        // RadioListTile(
        //   title: const Text("Oui"),
        //   value: Disponibility.IMMEDIAT,
        //   groupValue: disponibilityValue,
        //   onChanged: (value) {
        //     setState(() {
        //       disponibilityValue = Disponibility.IMMEDIAT;
        //       addProductController.valueDisponibility.value = "IMMEDIAT";
        //     });
        //     // print(addProductController.valueDisponibility);
        //   },
        // ),
        // RadioListTile(
        //   title: const Text("Non"),
        //   value: Disponibility.FUTURE,
        //   groupValue: disponibilityValue,
        //   onChanged: (value) {
        //     setState(() {
        //       disponibilityValue = Disponibility.FUTURE;
        //       addProductController.valueDisponibility.value = "FUTUR";
        //     });
        //     // print(addProductController.valueDisponibility);
        //   },
        // ),
        // const SizedBox(
        //   height: 30,
        // ),
        Obx(
          () => addProductController.productTypeOffreValue.value.text == "ACHAT"
              ? Container()
              : Column(
                  children: [
                    const Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Le produit est-il disponible maintenant ?",
                      ),
                    ),
                    RadioListTile(
                      title: const Text("Oui"),
                      value: Disponibility.IMMEDIAT,
                      groupValue: disponibilityValue,
                      onChanged: (value) {
                        setState(() {
                          disponibilityValue = Disponibility.IMMEDIAT;
                          addProductController.valueDisponibility.value =
                              "IMMEDIAT";
                        });
                        // print(addProductController.valueDisponibility);
                      },
                    ),
                    RadioListTile(
                      title: const Text("Non"),
                      value: Disponibility.FUTURE,
                      groupValue: disponibilityValue,
                      onChanged: (value) {
                        setState(
                          () {
                            disponibilityValue = Disponibility.FUTURE;
                            addProductController.valueDisponibility.value =
                                "FUTUR";
                          },
                        );
                        // print(addProductController.valueDisponibility);
                      },
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    addProductController.valueDisponibility.value == "IMMEDIAT"
                        ? Container()
                        : DateField(
                            dateController:
                                addProductController.dataController.value,
                            hintText: "Choisir la date de disponibilité",
                          ),
                  ],
                ),
        ),
      ],
    );
  }
}
