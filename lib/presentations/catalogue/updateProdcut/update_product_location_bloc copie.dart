// ignore_for_file: file_names

import 'package:eagri/presentations/catalogue/addPrduct/controllers/add_product_controller.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:select_form_field/select_form_field.dart';

class UpdateProductLocationBloc extends StatelessWidget {
  const UpdateProductLocationBloc({super.key});

  @override
  Widget build(BuildContext context) {
    final AddProductController addProductController = Get.find();
    return Column(
      children: [
        const SizedBox(
          height: 30,
        ),
        Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Où se situe votre produit ?",
              style: getBoldTextStyle(color: Colors.black, fontSize: 19),
            )),
        const SizedBox(
          height: 30,
        ),
        SelectFormField(
          type: SelectFormFieldType.dialog,
          enableSearch: true, // or can be dialog
          //initialValue: 'circle',
          controller: addProductController.productRegionValue.value,
          labelText: 'Choisir la region',
          items: addProductController.listRegionMap,
          onChanged: (val) =>
              addProductController.fetchDepartement(int.parse(val)),
        ),
        const SizedBox(
          height: 30,
        ),
        Obx(() => addProductController.listeDepartementMap.isEmpty
            ? Container()
            : Column(
                children: [
                  SelectFormField(
                    controller:
                        addProductController.prodcutDepartementValue.value,
                    type: SelectFormFieldType.dropdown, // or can be dialog
                    labelText: "Choisir le departement",
                    items: addProductController.listeDepartementMap,
                    onChanged: (val) {
                      addProductController
                          .fetchSpFectureOfDepartement(int.parse(val));
                    },
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                ],
              )),
        Obx(
          () => addProductController.listeSousPrefectureMap.isEmpty
              ? Container()
              : Column(
                  children: [
                    SelectFormField(
                      controller:
                          addProductController.productSousPrefectureValue.value,
                      type: SelectFormFieldType.dropdown, // or can be dialog
                      labelText: "Choisir la sous-prefercture",
                      items: addProductController.listeSousPrefectureMap,
                      onChanged: (val) =>
                          addProductController.fecthLocalite(int.parse(val)),
                    ),
                    const SizedBox(
                      height: 30,
                    )
                  ],
                ),
        ),
        Obx(
          () => addProductController.listLocaliteMap.isEmpty
              ? Container()
              : SelectFormField(
                  controller: addProductController.productLocaliteValue.value,
                  type: SelectFormFieldType.dropdown, // or can be dialog
                  labelText: "Choisir la localité",
                  items: addProductController.listLocaliteMap,
                ),
        )
      ],
    );
  }
}
