import 'dart:developer';

import 'package:eagri/app/app_prefs.dart';
import 'package:eagri/app/constant.dart';
import 'package:eagri/data/mapper/shop_mapper.dart';
import 'package:eagri/data/repository/api/repository_impl.dart';
import 'package:eagri/presentations/catalogue/controllers/catalogue_controller.dart';
import 'package:eagri/presentations/catalogue/publierIntra/add_intra_screen.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/ressources/assets_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import '../../app/di.dart';
import '../../app/functions.dart';
import '../../domain/model/Product.dart';
import '../../domain/model/user_model.dart';
import '../prestations/addPrestation/add_screen.dart';
import '../ressources/color_manager.dart';
import "package:get/get.dart";

import '../ressources/values_manager.dart';
import '../transport/addTransportOffer/add_car_screen.dart';
import 'addPrduct/add_product_view.dart';
import 'components/catalogue_card.dart';

class CatalogueHome extends StatefulWidget {
  const CatalogueHome({Key? key}) : super(key: key);

  @override
  State<CatalogueHome> createState() => _CatalogueHomeState();
}

class _CatalogueHomeState extends State<CatalogueHome> {
  AppSharedPreference appSharedPreference = instance<AppSharedPreference>();
  int indexCategorie = 0;
  final catalogueController = Get.put(CatalogueController());
  late Widget listWiget;

  getFirstIndex() async {
    await appSharedPreference.getUserInformation().then((userInfo) async {
      if (isInScopes(
          UserPermissions.CATALOGUE_PRODUIT_AGRICOLE_VOIR, userInfo.scopes!)) {
        setState(() {
          indexCategorie = codeProductAgricultural;
        });
      } else if (isInScopes(
          UserPermissions.CATALOGUE_INTRANT_VOIR, userInfo.scopes!)) {
        setState(() {
          indexCategorie = codeIntrant;
        });
      } else if (isInScopes(
          UserPermissions.CATALOGUE_PRESTATION_VOIR, userInfo.scopes!)) {
        setState(() {
          indexCategorie = codePrestation;
        });
      } else if (isInScopes(
          UserPermissions.CATALOGUE_TRANSPORT_VOIR, userInfo.scopes!)) {
        setState(() {
          indexCategorie = codeTransport;
        });
      }
      setState(() {
        listWiget = ListProductWidget(codeProduct: indexCategorie);
      });
    });
  }

  listAddScreen() {
    if (indexCategorie == codeProductAgricultural) {
      Get.to(const AddProductView());
    }
    if (indexCategorie == codeIntrant) {
      Get.to(const PublierView());
    }
    if (indexCategorie == codePrestation) {
      Get.to(const AddPrestationScreen());
    }
    if (indexCategorie == codeTransport) {
      Get.to(const AddTransportScreen());
    }
  }

  @override
  void initState() {
    getFirstIndex();
    //  _tabController.addListener(_handleTabSelection);
    // catalogueController.fetchProductFournisseur();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
        builder: (context, user, child) => Scaffold(
            appBar: AppBar(
              centerTitle: false,
              title: Text(
                'Gestionnaire de catalogue ',
                style: getRegularTextStyle(
                  color: ColorManager.black,
                ),
              ),
              leading: const BackButtonCustom(),
            ),
            floatingActionButton: hasAtLeastOneScope([
              UserPermissions.PUBLIEROFFRE_PRODUIT_AGRICOLE_AJOUTER,
              UserPermissions.PUBLIEROFFRE_INTRANT_AJOUTER,
              UserPermissions.PUBLIEROFFRE_PRESTATION_AJOUTER,
              UserPermissions.PUBLIEROFFRE_TRANSPORT_AJOUTER,
            ],user.scopes!)
                ? FloatingActionButton(
                    backgroundColor: ColorManager.primary,
                    onPressed: () {
                      listAddScreen();
                    },
                    child: Icon(
                      Icons.add,
                      color: ColorManager.white,
                    ),
                  )
                : const SizedBox(),
            body: Column(
              children: [
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      if (isInScopes(
                          UserPermissions.CATALOGUE_PRODUIT_AGRICOLE_VOIR,
                          user.scopes!))
                        categorieButton(
                            categoryId: codeProductAgricultural,
                            name: "Produit agricole"),
                      if (isInScopes(
                          UserPermissions.CATALOGUE_INTRANT_VOIR, user.scopes!))
                        categorieButton(
                            categoryId: codeIntrant, name: "Intrant"),
                      if (isInScopes(UserPermissions.CATALOGUE_PRESTATION_VOIR,
                          user.scopes!))
                        categorieButton(
                            categoryId: codePrestation, name: "Prestation"),
                      if (isInScopes(UserPermissions.CATALOGUE_TRANSPORT_VOIR,
                          user.scopes!))
                        categorieButton(
                            categoryId: codeTransport, name: "Transport"),
                    ],
                  ),
                ),
                if (isInScopes(UserPermissions.CATALOGUE_PRODUIT_AGRICOLE_VOIR,
                        user.scopes!) &&
                    indexCategorie == codeProductAgricultural)
                  const Expanded(child: ListProdAgri()),
                if (isInScopes(
                        UserPermissions.CATALOGUE_INTRANT_VOIR, user.scopes!) &&
                    indexCategorie == codeIntrant)
                  const Expanded(child: ListProdIntrant()),
                if (isInScopes(UserPermissions.CATALOGUE_PRESTATION_VOIR,
                        user.scopes!) &&
                    indexCategorie == codePrestation)
                  const Expanded(child: ListPrestation()),
                if (isInScopes(UserPermissions.CATALOGUE_TRANSPORT_VOIR,
                        user.scopes!) &&
                    indexCategorie == codeTransport)
                  const Expanded(child: ListTransport()),

                // if(indexCategorie!=0)
                // Expanded(
                //   child: listWiget
                // )
              ],
            )));
  }

  categorieButton({
    required int categoryId,
    required String name,
  }) {
    return GestureDetector(
      onTap: () {
        setState(() {
          indexCategorie = categoryId;
          // listWiget =ListProductWidget(codeProduct: categoryId);
          super.widget;
        });
      },
      child: Container(
        decoration: BoxDecoration(
          color: indexCategorie == categoryId
              ? ColorManager.primary
              : ColorManager.grey,
          borderRadius: BorderRadius.circular(15),
        ),
        padding: const EdgeInsets.only(right: 10, left: 10, bottom: 7, top: 5),
        margin: const EdgeInsets.only(
          left: AppMargin.m5,
          bottom: AppMargin.m5,
        ),
        child: Text(
          name.toString(),
          style: getRegularTextStyle(
            color: ColorManager.white,
          ),
        ),
      ),
    );
  }
}

class ListProdAgri extends StatelessWidget {
  const ListProdAgri({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const ListProductWidget(codeProduct: codeProductAgricultural);
  }
}

class ListProdIntrant extends StatelessWidget {
  const ListProdIntrant({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return const ListProductWidget(codeProduct: codeIntrant);
  }
}

class ListTransport extends StatelessWidget {
  const ListTransport({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return const ListProductWidget(codeProduct: codePrestation);
  }
}

class ListPrestation extends StatelessWidget {
  const ListPrestation({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return const ListProductWidget(codeProduct: codePrestation);
  }
}

class ListProductWidget extends StatefulWidget {
  const ListProductWidget({Key? key, required this.codeProduct})
      : super(key: key);
  final int codeProduct;

  @override
  State<ListProductWidget> createState() => _ListProductWidgetState();
}

class _ListProductWidgetState extends State<ListProductWidget> {
  final _focusNode = FocusNode();

  CatalogueController catalogueController = Get.find();
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  AppSharedPreference appSharedPreference = instance<AppSharedPreference>();
  List<ProductModel> listProduct = [];
  List<ProductModel> listProductShow = [];
  bool isLoad = true;

  getListPoduct() async {
    await appSharedPreference.getUserInformation().then((value) async {
      await repositoryImpl
          .getListProductsFournisseurWithCode(value.id!, widget.codeProduct)
          .then((response) async {
        await response.fold((l) {
          if (mounted) {
            setState(() {
              isLoad = false;
            });
          }
        }, (data) async {
          listProduct = await data.toDomain();
          if (mounted) {
            setState(() {
              listProductShow = listProduct;
              isLoad = false;
              listProduct;
            });
          }
        });
      });
    });
  }

  @override
  void initState() {
    isLoad = true;
    log("widget.codeProduct ::  ${widget.codeProduct}");
    getListPoduct();
    super.initState();
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return isLoad == true
        ? SingleChildScrollView(
            child: Column(
              children: [
                ...List.generate(
                  10,
                  (index) {
                    return shimmerCustom();
                    // here by default width and height is 0
                  },
                )
              ],
            ),
          )
        : RefreshIndicator(
            onRefresh: () async {
              await getListPoduct();
            },
            child: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: Column(
                children: [
                  Container(
                    height: 40,
                    margin: const EdgeInsets.all(10),
                    child: TextField(
                      onChanged: (value) async {
                        List<ProductModel> listProductSearch = [];
                        if (value != "") {
                          await Future.forEach(listProduct,
                              (ProductModel element) {
                            if (element.title
                                .toLowerCase()
                                .contains(value.toLowerCase())) {
                              listProductSearch.add(element);
                            }
                          });
                          if (mounted) {
                            setState(() {
                              listProductShow = listProductSearch;
                            });
                          }
                        } else {
                          if (mounted) {
                            setState(() {
                              listProductShow = listProduct;
                            });
                          }
                        }
                      },
                      focusNode: _focusNode,
                      decoration: InputDecoration(
                        suffixIcon: Icon(
                          Icons.search,
                          color: ColorManager.grey,
                        ),
                        filled: true,
                        fillColor: Colors.grey.withOpacity(0.2),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15.0),
                          borderSide: const BorderSide(
                              width: 0, style: BorderStyle.none),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15.0),
                          borderSide: const BorderSide(
                              width: 0, style: BorderStyle.none),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15.0),
                          borderSide: const BorderSide(
                              width: 0, style: BorderStyle.none),
                        ),
                        hintText: "Rechercher un produit",
                      ),
                    ),
                  ),
                  if (listProductShow.isNotEmpty) ...[
                    _buildCatalogueProduct(listProductShow)
                  ] else ...[
                    EmptyComponenent(
                      icon: JsonAssets.empty,
                      message: "Liste vide",
                    ),
                  ]
                ],
              ),
            ),
          );
  }

  Widget _buildCatalogueProduct(List<ProductModel> data) {
    return ListView.builder(
      physics: const NeverScrollableScrollPhysics(),
      padding: const EdgeInsets.only(bottom: 10),
      shrinkWrap: true,
      itemCount: data.length,
      itemBuilder: (context, index) {
        // log(index.toString());
        return CatalogueCard(
          product: data[index],
          index: index,
        );
      },
    );
  }

  Shimmer shimmerCustom() {
    return Shimmer.fromColors(
      baseColor: const Color.fromARGB(255, 115, 115, 115),
      highlightColor: const Color.fromARGB(255, 181, 181, 181),
      child: Container(
        margin: const EdgeInsets.only(top: 10),
        height: 60,
        child: Row(
          children: [
            Container(
              height: 60,
              margin: const EdgeInsets.only(bottom: 10, left: 10),
              width: 40,
              decoration: BoxDecoration(
                color: ColorManager.white,
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  color: ColorManager.white,
                  height: 10,
                  width: 150,
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  color: ColorManager.white,
                  height: 10,
                  width: 200,
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  color: ColorManager.white,
                  height: 10,
                  width: 100,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
