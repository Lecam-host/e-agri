import 'package:eagri/app/constant.dart';
import 'package:eagri/domain/model/Product.dart';
import 'package:eagri/presentations/catalogue/controllers/catalogue_controller.dart';
import 'package:eagri/presentations/ressources/assets_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../app/functions.dart';
import '../../common/cache_network_image.dart';
import '../../details_produit/details_produit_screen.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/strings_manager.dart';
import '../../ressources/styles_manager.dart';

class CatalogueCard extends StatelessWidget {
  const CatalogueCard({Key? key, required this.index, required this.product})
      : super(key: key);
  final int index;
  final ProductModel product;

  @override
  Widget build(BuildContext context) {
    final CatalogueController catalogueController =
        Get.put(CatalogueController());
    return InkWell(
      splashColor: ColorManager.primary4,
      onTap: () {
        catalogueController.selectedProduct.value = product;
        Get.to(
          DetailsProduitScreen(
            product: ProductDetailsArguments(
              product: product,
              isMyproduct: true,
            ),
          ),
        );
      },
      child: Container(
        margin: const EdgeInsets.only(top: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: ColorManager.white,
        ),
        padding: const EdgeInsets.all(5),
        child: Row(
          children: [
            if (product.images!.isNotEmpty)
              Container(
                width: 60.0,
                height: 80,
                decoration: BoxDecoration(
                  color: ColorManager.grey,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(5.0),
                  child: CacheNetworkImage(
                    image: product.images![0],
                  ),
                ),
              ),
            if (product.images!.isEmpty)
              Container(
                width: 60.0,
                height: 80,
                decoration: BoxDecoration(
                  color: ColorManager.grey,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(5.0),
                    child: const Icon(Icons.image)),
              ),
            const SizedBox(
              width: 5,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    product.title,
                    style: getRegularTextStyle(
                      color: ColorManager.black,
                      fontSize: 14,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Row(
                    children: [
                      Text(
                        "${product.price.toString()} ${AppStrings.moneyDevise}",
                        style: getBoldTextStyle(
                          color: ColorManager.black,
                          fontSize: 14,
                        ),
                      ),
                      const Spacer(),
                      Text(
                        product.status == "UNAVAILABLE"
                            ? "Indisponible"
                            : product.status == "DELETED"
                                ? "Rétiré"
                                : "Actif",
                        style: getRegularTextStyle(
                          color: product.status == "UNAVAILABLE"
                              ? Colors.orange
                              : product.status == "DELETED"
                                  ? ColorManager.red
                                  : ColorManager.primary,
                          fontSize: 12,
                        ),
                      ),
                    ],
                  ),
                  if (product.code != codePrestation.toString())
                    Text(
                      "Qte: ${product.quantity} ${product.unite}",
                      style: getRegularTextStyle(
                        color: ColorManager.black,
                        fontSize: 12,
                      ),
                    ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "Publié le : ${convertDate(product.publicationDate)}",
                        style: getRegularTextStyle(
                          color: ColorManager.black,
                          fontSize: 12,
                        ),
                      ),
                      const Spacer(),
                      Text(
                        "${product.rating}",
                        style: getRegularTextStyle(
                          color: ColorManager.black,
                          fontSize: 12,
                        ),
                      ),
                      SvgPicture.asset(SvgManager.star),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
