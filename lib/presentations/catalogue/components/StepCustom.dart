// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

import '../../ressources/color_manager.dart';
import '../../ressources/styles_manager.dart';

class StepCustomWidget extends StatelessWidget {
  const StepCustomWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int currentPage = 0;

    return Column(
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Text("Étape 1/3", style: getSemiBoldTextStyle(fontSize: 16)),
        ),
        const SizedBox(
          height: 10,
        ),
        StepProgressIndicator(
          totalSteps: 4,
          currentStep: 3,
          selectedColor: ColorManager.primary,
          unselectedColor: Colors.grey,
        ),
        const SizedBox(
          height: 10,
        ),
        GestureDetector(
          onTap: () {
            currentPage = currentPage - 1;
          },
          child: Row(
            children: [
              Icon(
                Icons.arrow_back_ios,
                color: ColorManager.primary2,
              ),
              Text("Retour",
                  style: getSemiBoldTextStyle(
                      fontSize: 16, color: ColorManager.primary2)),
            ],
          ),
        )
      ],
    );
  }
}
