import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/domain/model/Product.dart';
import 'package:eagri/presentations/details_produit/components/product_description_catalogue.dart';
import 'package:eagri/presentations/details_produit/components/product_images_catalogue.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';

class BodyCatalogue extends StatelessWidget {
  const BodyCatalogue({
    Key? key,
    required this.product,
  }) : super(key: key);
  final ProductModel product;
  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: [
        ProductImagesCatalogue(product: product),
        ProductDescriptionSelft(
          product: product,
          pressOnSeeMore: () {},
        ),
        const SizedBox(
          height: 70,
        ),
        // const ProductSimilaireS(),
      ],
    );
  }

  showCustomFlushbar(BuildContext ctx, bool isExist, String message) {
    return Flushbar(
      flushbarPosition: FlushbarPosition.TOP,
      duration: const Duration(seconds: 3),
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.all(10),
      borderRadius: BorderRadius.circular(15),
      backgroundColor:
          isExist == true ? ColorManager.blue : ColorManager.primary,
      icon: Icon(Icons.info, color: ColorManager.grey),
      boxShadows: const [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],
      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      // title: '',
      message: message,
    ).show(ctx);
  }
}
