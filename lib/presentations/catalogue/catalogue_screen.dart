import 'package:eagri/app/app_prefs.dart';
import 'package:eagri/app/constant.dart';
import 'package:eagri/data/mapper/shop_mapper.dart';
import 'package:eagri/data/repository/api/repository_impl.dart';
import 'package:eagri/presentations/catalogue/controllers/catalogue_controller.dart';
import 'package:eagri/presentations/catalogue/publierIntra/add_intra_screen.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/ressources/assets_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import '../../app/di.dart';
import '../../app/functions.dart';
import '../../domain/model/Product.dart';
import '../../domain/model/user_model.dart';
import '../prestations/addPrestation/add_screen.dart';
import '../ressources/color_manager.dart';
import "package:get/get.dart";

import '../transport/addTransportOffer/add_car_screen.dart';
import 'addPrduct/add_product_view.dart';
import 'components/catalogue_card.dart';

class CatalogueView extends StatefulWidget {
  const CatalogueView({Key? key}) : super(key: key);

  @override
  State<CatalogueView> createState() => _CatalogueViewState();
}

class _CatalogueViewState extends State<CatalogueView>
    with SingleTickerProviderStateMixin {
  final catalogueController = Get.put(CatalogueController());
  late TabController _tabController;
  void _handleTabSelection() {
    catalogueController.isLoad.value = true;
//_tabController.
    catalogueController.pageIndex.value = _tabController.index;
    catalogueController.searchInput.value.clear();
    catalogueController.getListProduct();
  }

  listAddScreen(List<Widget> listTab) {
    if (_tabController.index == 0) {
      Get.to(const AddProductView());
    }
    if (_tabController.index == 1) {
      Get.to(const PublierView());
    }
    if (_tabController.index == 2) {
      Get.to(const AddPrestationScreen());
    }
    if (_tabController.index == 3) {
      Get.to(const AddTransportScreen());
    }
  }

  List<Widget> listTab(List<String> scopes) => [
        if (isInScopes(UserPermissions.RESERVATION_TRANSPORT, scopes) == true)
          Tab(
            child: Text(
              "Produit agricole",
              style: getMeduimTextStyle(color: ColorManager.black),
            ),
          ),
        Tab(
          child: Text(
            "Intrant",
            style: getMeduimTextStyle(color: ColorManager.black),
          ),
        ),
        Tab(
          child: Text(
            "Prestation",
            style: getMeduimTextStyle(color: ColorManager.black),
          ),
        ),
        Tab(
          child: Text(
            "Transport",
            style: getMeduimTextStyle(color: ColorManager.black),
          ),
        ),
      ];
  @override
  void initState() {
    _tabController = TabController(length: 4, vsync: this);

    //  _tabController.addListener(_handleTabSelection);
    // catalogueController.fetchProductFournisseur();
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();

    _tabController.removeListener(_handleTabSelection);

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
        builder: (context, user, child) => DefaultTabController(
              length: 4,
              child: Scaffold(
                appBar: AppBar(
                  centerTitle: false,
                  bottom: TabBar(
                      isScrollable: true,
                      controller: _tabController,
                      indicatorColor: ColorManager.primary,
                      tabs: listTab(user.scopes!)),
                  title: Text(
                    'Gestionnaire de catalogue ',
                    style: getRegularTextStyle(
                      color: ColorManager.black,
                    ),
                  ),
                  leading: const BackButtonCustom(),
                ),
                floatingActionButton: FloatingActionButton(
                  backgroundColor: ColorManager.primary,
                  onPressed: () {
                    listAddScreen(listTab(user.scopes!)) ;
                  },
                  child: Icon(
                    Icons.add,
                    color: ColorManager.white,
                  ),
                ),
                body: TabBarView(
                  controller: _tabController,
                  children: const [
                    ListProductWidget(
                      codeProduct: codeProductAgricultural,
                    ),
                    ListProductWidget(
                      codeProduct: codeIntrant,
                    ),
                    ListProductWidget(
                      codeProduct: codePrestation,
                    ),
                    ListProductWidget(
                      codeProduct: codeTransport,
                    ),

                    // productAgriculWidget(),
                  ],
                ),
              ),
            ));
  }
}

class ListProductWidget extends StatefulWidget {
  const ListProductWidget({Key? key, required this.codeProduct})
      : super(key: key);
  final int codeProduct;

  @override
  State<ListProductWidget> createState() => _ListProductWidgetState();
}

class _ListProductWidgetState extends State<ListProductWidget> {
  final _focusNode = FocusNode();

  CatalogueController catalogueController = Get.find();
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  AppSharedPreference appSharedPreference = instance<AppSharedPreference>();
  List<ProductModel> listProduct = [];
  List<ProductModel> listProductShow = [];
  getListPoduct() async {
    await appSharedPreference.getUserInformation().then((value) async {
      await repositoryImpl
          .getListProductsFournisseurWithCode(value.id!, widget.codeProduct)
          .then((response) async {
        await response.fold((l) {
          if (mounted) {
            setState(() {
              isLoad = false;
            });
          }
        }, (data) async {
          listProduct = await data.toDomain();
          if (mounted) {
            setState(() {
              listProductShow = listProduct;
              isLoad = false;
              listProduct;
            });
          }
        });
      });
    });
  }

  bool isLoad = true;
  @override
  void initState() {
    isLoad = true;

    getListPoduct();
    super.initState();
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return isLoad == true
        ? SingleChildScrollView(
            child: Column(
              children: [
                ...List.generate(
                  10,
                  (index) {
                    return shimmerCustom();
                    // here by default width and height is 0
                  },
                )
              ],
            ),
          )
        : RefreshIndicator(
            onRefresh: () async {
              await getListPoduct();
            },
            child: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: Column(
                children: [
                  Container(
                    height: 40,
                    margin: const EdgeInsets.all(10),
                    child: TextField(
                      onChanged: (value) async {
                        List<ProductModel> listProductSearch = [];
                        if (value != "") {
                          await Future.forEach(listProduct,
                              (ProductModel element) {
                            if (element.title
                                .toLowerCase()
                                .contains(value.toLowerCase())) {
                              listProductSearch.add(element);
                            }
                          });
                          if (mounted) {
                            setState(() {
                              listProductShow = listProductSearch;
                            });
                          }
                        } else {
                          if (mounted) {
                            setState(() {
                              listProductShow = listProduct;
                            });
                          }
                        }
                      },
                      focusNode: _focusNode,
                      decoration: InputDecoration(
                        suffixIcon: Icon(
                          Icons.search,
                          color: ColorManager.grey,
                        ),
                        filled: true,
                        fillColor: Colors.grey.withOpacity(0.2),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15.0),
                          borderSide: const BorderSide(
                              width: 0, style: BorderStyle.none),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15.0),
                          borderSide: const BorderSide(
                              width: 0, style: BorderStyle.none),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15.0),
                          borderSide: const BorderSide(
                              width: 0, style: BorderStyle.none),
                        ),
                        hintText: "Rechercher un produit",
                      ),
                    ),
                  ),
                  if (listProductShow.isNotEmpty) ...[
                    _buildCatalogueProduct(listProductShow)
                  ] else ...[
                    EmptyComponenent(
                      icon: JsonAssets.empty,
                      message: "Liste vide",
                    ),
                  ]
                ],
              ),
            ),
          );
  }

  Widget _buildCatalogueProduct(List<ProductModel> data) {
    return ListView.builder(
      physics: const NeverScrollableScrollPhysics(),
      padding: const EdgeInsets.only(bottom: 10),
      shrinkWrap: true,
      itemCount: data.length,
      itemBuilder: (context, index) {
        // log(index.toString());
        return CatalogueCard(
          product: data[index],
          index: index,
        );
      },
    );
  }

  Shimmer shimmerCustom() {
    return Shimmer.fromColors(
      baseColor: const Color.fromARGB(255, 115, 115, 115),
      highlightColor: const Color.fromARGB(255, 181, 181, 181),
      child: Container(
        margin: const EdgeInsets.only(top: 10),
        height: 60,
        child: Row(
          children: [
            Container(
              height: 60,
              margin: const EdgeInsets.only(bottom: 10, left: 10),
              width: 40,
              decoration: BoxDecoration(
                color: ColorManager.white,
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  color: ColorManager.white,
                  height: 10,
                  width: 150,
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  color: ColorManager.white,
                  height: 10,
                  width: 200,
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  color: ColorManager.white,
                  height: 10,
                  width: 100,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
