import 'dart:async';
import 'dart:developer';
import 'dart:isolate';

import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/app/app_prefs.dart';
import 'package:eagri/app/constant.dart';
import 'package:eagri/data/mapper/shop_mapper.dart';
import 'package:eagri/data/repository/api/repository_impl.dart';
import 'package:eagri/data/request/shop_resquests.dart';
import 'package:eagri/domain/model/Product.dart';
import 'package:eagri/domain/model/user_model.dart';
import 'package:eagri/domain/usecase/shop_usecase.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/common/state/succes_page_view.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../app/di.dart';
import '../../ressources/assets_manager.dart';
import '../../ressources/strings_manager.dart';

class CatalogueController extends GetxController {
  Isolate? isolate;
  AppSharedPreference appSharedPreference = instance<AppSharedPreference>();
  var userInfo = UserModel().obs;
  final isLoad = false.obs;
  final listProductCatalogue = <ProductModel>[].obs;

  final listProductAgricolCatalogueShow = <ProductModel>[].obs;
  final listProductTransportCatalogueShow = <ProductModel>[].obs;
  final listProductIntrantCatalogueShow = <ProductModel>[].obs;
  final listProductPrestionCatalogueShow = <ProductModel>[].obs;

  final listProductProductAgricoleCatalogueShow = <ProductModel>[].obs;
  final searchInput = TextEditingController().obs;
  final pageIndex = 0.obs;

  // ProductModel productNew = instance<ProductModel>();

  final selectedProduct = Rx<ProductModel?>(null);

  @override
  void onInit() async {
    userInfo.value = await appSharedPreference.getUserInformation();
    getList();

    super.onInit();
  }

  ShopUseCase shopUseCase = instance<ShopUseCase>();
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  Future<void> getListProduct() async {
    if (pageIndex.value == 0) {
      await fetchProductFournisseurWithCode(codeProductAgricultural);
    } else if (pageIndex.value == 1) {
      await fetchProductFournisseurWithCode(codeIntrant);
    } else if (pageIndex.value == 2) {
      await fetchProductFournisseurWithCode(codePrestation);
    } else if (pageIndex.value == 3) {
      await fetchProductFournisseurWithCode(codeTransport);
    } else {}
  }

  getListFnCodeProduct(String codeProduct) {
    if (codeProduct == codeProductAgricultural.toString()) {
      fetchProductFournisseurWithCode(codeProductAgricultural);
    } else if (codeProduct == codeIntrant.toString()) {
      fetchProductFournisseurWithCode(codeIntrant);
    } else if (codeProduct == codePrestation.toString()) {
      fetchProductFournisseurWithCode(codePrestation);
    }
  }
  // fetchProductFournisseur() async {
  //   isLoad(true);
  //   userInfo.value = await appSharedPreference.getUserInformation();
  //   await shopUseCase
  //       .getListProductsFournisseur(userInfo.value.id!)
  //       .then((value) async {
  //     value.fold(
  //       (l) => {},
  //       (r) async {
  //         if (r.data != null) listProductCatalogue.value = await r.toDomain();
  //         listProductCatalogueShow.value = listProductCatalogue;
  //         isLoad(false);
  //       },
  //     );
  //   });
  // }

  fetchProductFournisseurWithCode(int codeTypeProduct) async {
    isLoad(true);
    List<ProductModel> list = [];
    userInfo.value = await appSharedPreference.getUserInformation();
    await repositoryImpl
        .getListProductsFournisseurWithCode(userInfo.value.id!, codeTypeProduct)
        .then((value) {
      return value.fold(
        (l) => {},
        (r) async {
          listProductCatalogue.value = [];

          if (r.data != null) list = await r.toDomain();
          if (r.data != null) listProductCatalogue.value = list;
          if (pageIndex.value == 0) {
            listProductAgricolCatalogueShow.value = list;
          } else if (pageIndex.value == 1) {
            listProductIntrantCatalogueShow.value = list;
          } else if (pageIndex.value == 2) {
            listProductPrestionCatalogueShow.value = list;
          } else {
            listProductTransportCatalogueShow.value = list;
          }

          isLoad(false);
        },
      );
    });
  }

  deleteProduct(ProductModel product) {
    showDialog(
      context: Get.context!,
      builder: (BuildContext context) {
        return AlertDialog(
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: const Text(AppStrings.cancel),
              ),
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                  Get.dialog(const LoaderComponent());
                  shopUseCase.deleteProduct(product.id).then(
                        (value) => value.fold((l) => Get.back(), (r) {
                          getListFnCodeProduct(product.code);

                          Get.back();
                          Get.off(
                            SuccessPageView(
                              icon: JsonAssets.deleteSuccess,
                              message: "Votre produit a été rétiré",
                              action: () {
                                Get.back();
                              },
                              titreAction: "Revenir",
                            ),
                          );
                        }),
                      );
                },
                child: Text(
                  "Rétirer",
                  style: getRegularTextStyle(
                    color: ColorManager.red,
                  ),
                ),
              )
            ],
            contentPadding: const EdgeInsets.all(10),
            content: const Text("Voulez-vous vraiment rétirer ce produit ?"));
      },
    );
  }

  modifProduct(
      UpdateProductOnMarketrequest updateRequest, ProductModel product) async {
    Get.dialog(const LoaderComponent());
    await repositoryImpl
        .updateProductOnMarket(updateRequest)
        .then((value) => value.fold((l) {
              Get.back();

              showCustomFlushbar(Get.context!, "Échec de la modification",
                  ColorManager.error, FlushbarPosition.BOTTOM);
            }, (r) async {
              final newProduct = await r;
              selectedProduct.value = newProduct;

              if (product.images != null) {
                if (product.images!.isNotEmpty) {
                  selectedProduct.value!.images = product.images!;
                } else {
                  selectedProduct.value!.images!.add(product.defaultImg!);
                }
              }

              inspect(r);
              getListFnCodeProduct(product.code);

              Get.close(2);

              showCustomFlushbar(Get.context!, "Produit modifié avec succés",
                  ColorManager.succes, FlushbarPosition.BOTTOM);
            }));
  }

  searchProduct() async {
    List<ProductModel> prod = [];
    if (searchInput.value.text != "") {
      await Future.forEach(listProductCatalogue, (ProductModel element) {
        if (element.title
            .toLowerCase()
            .contains(searchInput.value.text.toLowerCase())) {
          prod.add(element);
        }
      });
    } else {
      prod = listProductCatalogue;
    }

    if (pageIndex.value == 0) {
      listProductAgricolCatalogueShow.value = listProductCatalogue;
    } else if (pageIndex.value == 1) {
      listProductIntrantCatalogueShow.value = listProductCatalogue;
    } else if (pageIndex.value == 2) {
      listProductPrestionCatalogueShow.value = listProductCatalogue;
    } else {
      listProductTransportCatalogueShow.value = listProductCatalogue;
    }
  }

  Future<void> getList() async {
    // Lance la première instance de la fonction longRunningFunction.

    // Attend une seconde.

    // Coupe la fonction si elle est toujours en cours d'exécution.
    // future.asStream().;
    // Lance une deuxième instance de la fonction longRunningFunction.
    await getListProduct();
  }

  // void getList() async {
  //   if (isolate != null) {
  //     // Si la fonction est déjà en cours d'exécution, on arrête son exécution
  //     isolate!.kill(priority: Isolate.immediate);
  //   }

  //   Completer completer = Completer();

  //   isolate = await Isolate.spawn(getListProduct, completer);

  //   await completer.future;
  // }
}

typedef SingletonFactory<T> = T Function();

class Singleton {
  static dynamic _instance;

  static void register(SingletonFactory factory) {
    _instance ??= factory();
  }

  static dynamic get instanceSingle {
    return _instance;
  }
}
