import 'dart:developer';
import 'dart:io';

import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/app/app_prefs.dart';
import 'package:eagri/app/di.dart';
import 'package:eagri/data/repository/api/repository_impl.dart';
import 'package:eagri/data/repository/local/speculation_repo.dart';
import 'package:eagri/data/request/request_object.dart';
import 'package:eagri/data/request/shop_resquests.dart';
import 'package:eagri/domain/model/location_model.dart';
import 'package:eagri/domain/model/methode_paiement_model.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/domain/model/user_model.dart';
import 'package:eagri/domain/usecase/location_usecase.dart';
import 'package:eagri/domain/usecase/shop_usecase.dart';
import 'package:eagri/domain/usecase/speculation_price_usecase.dart';
import 'package:eagri/presentations/catalogue/controllers/catalogue_controller.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/common/state/succes_page_view.dart';
import 'package:eagri/presentations/common/state_renderer/state_render_impl.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../../app/constant.dart';
import '../../../../app/functions.dart';
import '../../../common/state_renderer/state_renderer.dart';

class AddProductController extends GetxController {
  CatalogueController catalogueController = Get.put(CatalogueController());
  var pageController = PageController().obs;

  final files = <File>[].obs;

  final currentPage = 0.obs;

  final isLoadTypeOffre = false.obs;

  final selectRadioValue = 0.obs;
  final isvalide = false.obs;
  final pageIndex = 0.obs;
  var methodPayment = PaymentMethodModel().obs;
  var listeTypeVenteMap = <dynamic>[];
  var listeTypeOffre = [];
  var methodPaiementId = 0.obs;

  var listeSpeculation = <SpeculationModel>[];
  var listDepartement = <DepartementModel>[].obs;
  var listeSousPrefecture = <SousPrefectureModel>[].obs;
  var listLocalite = <LocaliteModel>[].obs;

  var listeSpeculationMap = <Map<String, dynamic>>[].obs;
  var listeTypeProductMap = <Map<String, dynamic>>[];
  var listeUnitNbPayementCrediMap = <Map<String, dynamic>>[].obs;

  var listeTypeOffreMap = <Map<String, dynamic>>[];
  var listeDepartementMap = <Map<String, dynamic>>[].obs;
  var listeSousPrefectureMap = <Map<String, dynamic>>[].obs;
  var listLocaliteMap = <Map<String, dynamic>>[].obs;
  var categorieId = 0.obs;

  var listMethodPaiement = <PaymentMethodModel>[];
  var listUnite = <String>[].obs;

  var listUniteMap = <Map<String, dynamic>>[].obs;

  //Infomation sur le produit
  var valueDisponibility = "IMMEDIAT".obs;
  var dataController = TextEditingController().obs;
  var productTextValue = TextEditingController().obs;
  var productUniteTextValue = TextEditingController().obs;
  var productPriceValue = TextEditingController().obs;
  var pricePreOrderValue = TextEditingController().obs;

  var productQuantityValue = TextEditingController().obs;
  var productDescriptionValue = TextEditingController().obs;
  var nbPayementCreditValue = TextEditingController().obs;
  var unitNbPayementCreditValue = TextEditingController().obs;

  var productTypeVenteValue = TextEditingController().obs;
  var productTypeOffreValue = TextEditingController().obs;
  var productSousPrefectureValue = TextEditingController().obs;
  var productLocaliteValue = TextEditingController().obs;
  var prodcutDepartementValue = TextEditingController().obs;

  //Info sur la localisation du produit
  final productRegionValue = TextEditingController().obs;
  final productDepartementValue = TextEditingController().obs;

  var listRegion = <RegionModel>[];
  var listRegionMap = <Map<String, dynamic>>[];

  SpeculationPriceUseCase speculationPriceUseCase =
      instance<SpeculationPriceUseCase>();

  UserModel userInfo = UserModel();

  ShopUseCase shopUseCase = instance<ShopUseCase>();
  LocationUsecase locationUsecase = instance<LocationUsecase>();
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();

  // @override
  // void onClose() {
  //   // Get.deleteAll();
  //   super.onClose();
  // }

  @override
  void onInit() async {
    productTypeOffreValue.value.clear();
    getListUnit();
    await fetchMethodPaiement();
    await getRegion(Get.context);
    await fetchSpeculation();
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    listeTypeVenteMap =
        await AppSharedPreference(sharedPreferences).getListTypeVente();
    userInfo =
        await AppSharedPreference(sharedPreferences).getUserInformation();

    listeTypeProductMap = listeTypeVenteMap
        .map((e) => {"value": e, "label": "En ${e.toLowerCase()}"})
        .toList();

    isLoadTypeOffre(true);
    listeTypeOffre =
        await AppSharedPreference(sharedPreferences).getListTypeOffers();
    isLoadTypeOffre(false);

    listeTypeOffreMap =
        listeTypeOffre.map((e) => {"value": e, "label": e}).toList();

    super.onInit();
  }

  getListUnit() async {
    await repositoryImpl.getUnitTime().then((response) {
      response.fold((failure) {}, (data) {
        listeUnitNbPayementCrediMap.value = data.data
            .map((e) => {"value": e, "label": e.toCapitalize()})
            .toList();

        inspect(listeUnitNbPayementCrediMap);
      });
    });
  }

  /// Fetch the speculation from the server and update the UI.
  fetchSpeculation() async {
    listeSpeculation = await SpeculationRepo().getAllItem();
    listeSpeculationMap.value = listeSpeculation
        .map((e) => {"value": e.speculationId, "label": e.name})
        .toList();
  }

  getCategorie() {
    categorieId.value = listeSpeculation
        .firstWhere((SpeculationModel element) =>
            element.speculationId! == int.parse(productTextValue.value.text))
        .categoryInfo!
        .categorieId!;
  }

  /// It changes the unit of a given id by speculation
  ///
  /// Args:
  ///   id (int): The id of the unit you want to change.
  changeUniteBySpeculation(int id) {
    if (productUniteTextValue.value.text.isNotEmpty) {
      productUniteTextValue.value.text = "";
    }
    listUnite.value = listeSpeculation
            .where((element) => element.speculationId == id)
            .toList()[0]
            .listUnit ??
        [];
    listUniteMap.value = listUnite
        .map((element) => {"value": element, "label": element})
        .toList();
  }

  /// It gets the region from the database.
  ///
  /// Args:
  ///   context: The context of the current screen.
  Future getRegion(context) async {
    LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE)
        .getScreenWidget(context, Container(), () {});
    (await speculationPriceUseCase.getListRegion()).fold((failure) {
      Navigator.of(context).pop();
    }, (result) {
      listRegion = result.data;
      Navigator.of(context).pop();
      inspect(listRegion);
      listRegionMap = listRegion
          .map((element) => {"value": element.regionId, "label": element.name})
          .toList();
    });
  }

  /// It fetches the list of payment methods from the API.
  Future fetchMethodPaiement() async {
    (await repositoryImpl.getMethodPaiementAll()).fold(
        (l) => log(l.toString()), (r) => listMethodPaiement = r.data!.items!);
  }
  // Future postProduct()async{

  // }

  /// The function that is called when the user clicks on the button to publish the product.
  sendProduct() async {
    Get.dialog(const LoaderComponent());
    log(productLocaliteValue.value.text);
    await createCatalogue();
    AddProductOnMarketrequest addProductOnMarketrequest =
        AddProductOnMarketrequest(
            unitOfMeasurment: productUniteTextValue.value.text,
            availability: valueDisponibility.value,
            location: AddProductLocationRequest(
              departementId: prodcutDepartementValue.value.text.isNotEmpty
                  ? int.parse(prodcutDepartementValue.value.text)
                  : null,
              localiteId: productLocaliteValue.value.text.isNotEmpty
                  ? int.parse(productLocaliteValue.value.text)
                  : null,
              sousPrefectureId: productSousPrefectureValue.value.text.isNotEmpty
                  ? int.parse(productSousPrefectureValue.value.text)
                  : null,
              regionId: int.parse(productRegionValue.value.text),
            ),
            description: productDescriptionValue.value.text,
            fournisseurId: userInfo.id!,
            offerType: productTypeOffreValue.value.text.isEmpty
                ? "ACHAT"
                : productTypeOffreValue.value.text,
            price: int.parse(productPriceValue.value.text),
            quantity: int.parse(productQuantityValue.value.text),
            speculationId: int.parse(productTextValue.value.text),
            typeVente: productTypeVenteValue.value.text,
            availabilityDate: valueDisponibility.value == "IMMEDIAT"
                ? null
                : dataController.value.text,
            images: files.isNotEmpty ? files : null,
            paymentId: methodPaiementId.value,
            categorieId: categorieId.value,
            numberSlice: nbPayementCreditValue.value.text.isNotEmpty
                ? int.parse(nbPayementCreditValue.value.text)
                : null,
            unitTime: unitNbPayementCreditValue.value.text.isNotEmpty
                ? unitNbPayementCreditValue.value.text
                : null,
            pricePreOrder: pricePreOrderValue.value.text != ""
                ? double.parse(pricePreOrderValue.value.text)
                : null);
    inspect(addProductOnMarketrequest);

    await shopUseCase
        .addProductOnMarket(addProductOnMarketrequest)
        .then(
          (value) => value.fold((l) {
            print(l);
            Get.back();
            showCustomFlushbar(Get.context!, l.message, ColorManager.error,
                FlushbarPosition.TOP);
          }, (r) {
            log(r.toString());
            //  clearVariable();
            catalogueController
                .getListFnCodeProduct(codeProductAgricultural.toString());
            Get.back();

            Get.off(
              const SuccessPageView(
                message: "Votre annonce a été publiée",
              ),
            );
            //
          }),
        )
        .catchError((onError) => log(onError.toString()));
  }

  clearVariable() {
    productUniteTextValue.close();
    valueDisponibility.close();
    prodcutDepartementValue.close();

    productLocaliteValue.close();

    productSousPrefectureValue.close();
    productRegionValue.close();

    productTypeOffreValue.close();

    productPriceValue.close();
    productTextValue.close();
    productTypeVenteValue.close();
    valueDisponibility.close();

    dataController.close();
    files.close();

    methodPaiementId.close();
    categorieId.close();
  }

  /// It adds a file to the project and converts it to Dart
  ///
  /// Args:
  ///   file (XFile): The file to be added to the list.
  addFileAndConvert(XFile file) {
    final newFile = File(file.path);
    files.add(newFile);
  }

  fetchDepartement(int idRegion) async {
    (await locationUsecase.getListDepartement(idRegion)).fold(
      (l) => null,
      (r) {
        productDepartementValue.value.text = "";
        listDepartement.value = r.listDepartement!;
        listeDepartementMap.clear();
        inspect(r.listDepartement);
        listeDepartementMap.value = listDepartement
            .map((element) =>
                {"value": element.departementId, "label": element.name})
            .toList();
      },
    );
  }

  fetchSpFectureOfDepartement(int idDepartement) async {
    (await locationUsecase.getListSpOfDepartement(idDepartement)).fold(
      (l) => null,
      (r) {
        listeSousPrefecture.value = r.listSousPrefecture!;
        inspect(r.listSousPrefecture);
        listeSousPrefectureMap.value = listeSousPrefecture
            .map((element) =>
                {"value": element.sousPrefectureId, "label": element.name})
            .toList();
      },
    );
  }

  fecthLocalite(int idSp) async {
    (await locationUsecase.getListLocaliteOfSp(idSp)).fold(
      (l) => null,
      (r) {
        listLocalite.value = r.listLocalite!;
        inspect(r.listLocalite);
        listLocaliteMap.value = listLocalite
            .map((element) =>
                {"value": element.localiteId, "label": element.name})
            .toList();
      },
    );
  }
}
