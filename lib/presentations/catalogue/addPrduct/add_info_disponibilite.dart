// ignore_for_file: constant_identifier_names

import 'package:eagri/presentations/catalogue/addPrduct/controllers/add_product_controller.dart';
import 'package:eagri/presentations/common/date_field.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:select_form_field/select_form_field.dart';

enum Disponibility { FUTURE, IMMEDIAT }

class AddInfoDisonibilite extends StatefulWidget {
  const AddInfoDisonibilite({super.key, required this.isOffreAchat});
  final bool isOffreAchat;
  @override
  State<AddInfoDisonibilite> createState() => _AddInfoDisonibiliteState();
}

class _AddInfoDisonibiliteState extends State<AddInfoDisonibilite> {
  Disponibility disponibilityValue = Disponibility.IMMEDIAT;
  AddProductController addProductController = Get.find();
  @override
  void initState() {
    if (widget.isOffreAchat == true) {
      addProductController.productTypeVenteValue.value.text = "ACHAT";
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      child: ListView(
        padding: const EdgeInsets.only(bottom: 80),
        children: [
          const SizedBox(
            height: 30,
          ),
          Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "Information sur le produit",
                style: getBoldTextStyle(color: Colors.black, fontSize: 19),
              )),

          const SizedBox(
            height: 30,
          ),
          if (widget.isOffreAchat == false)
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text('Comment vous vendez votre produit ?'),
                const SizedBox(
                  height: 10,
                ),
                SelectFormField(
                  type: SelectFormFieldType.dropdown, // or can be dialog
                  // initialValue: 'circle',
                  labelText: 'Type de vente',
                  controller: addProductController.productTypeVenteValue.value,
                  items: addProductController.listeTypeProductMap,
                ),
              ],
            ),
          const SizedBox(
            height: 30,
          ),
          // SelectFormField(
          //   type: SelectFormFieldType.dropdown, // or can be dialog
          //   // initialValue: 'circle',
          //   labelText: "Type d'offre",
          //   controller: addProductController.productTypeOffreValue.value,
          //   items: addProductController.listeTypeOffreMap,
          //   onChanged: (val) => print(val),
          //   onSaved: (val) => print(val),
          // ),
          // const SizedBox(
          //   height: 30,
          // ),
          // const Align(
          //   alignment: Alignment.centerLeft,
          //   child: Text(
          //     "Le produit est-il disponible maintenant ?",
          //   ),
          // ),
          // RadioListTile(
          //   title: const Text("Oui"),
          //   value: Disponibility.IMMEDIAT,
          //   groupValue: disponibilityValue,
          //   onChanged: (value) {
          //     setState(() {
          //       disponibilityValue = Disponibility.IMMEDIAT;
          //       addProductController.valueDisponibility.value = "IMMEDIAT";
          //     });
          //     // print(addProductController.valueDisponibility);
          //   },
          // ),
          // RadioListTile(
          //   title: const Text("Non"),
          //   value: Disponibility.FUTURE,
          //   groupValue: disponibilityValue,
          //   onChanged: (value) {
          //     setState(() {
          //       disponibilityValue = Disponibility.FUTURE;
          //       addProductController.valueDisponibility.value = "FUTUR";
          //     });
          //     // print(addProductController.valueDisponibility);
          //   },
          // ),
          // const SizedBox(
          //   height: 30,
          // ),
          Obx(
            () =>
                addProductController.productTypeOffreValue.value.text == "ACHAT"
                    ? const SizedBox()
                    : Column(
                        children: [
                          const Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Le produit est-il disponible maintenant ?",
                            ),
                          ),
                          RadioListTile(
                            title: const Text("Oui"),
                            value: Disponibility.IMMEDIAT,
                            groupValue: disponibilityValue,
                            onChanged: (value) {
                              setState(() {
                                disponibilityValue = Disponibility.IMMEDIAT;
                                addProductController.valueDisponibility.value =
                                    "IMMEDIAT";
                              });
                              // print(addProductController.valueDisponibility);
                            },
                          ),
                          RadioListTile(
                            title: const Text("Non"),
                            value: Disponibility.FUTURE,
                            groupValue: disponibilityValue,
                            onChanged: (value) {
                              setState(
                                () {
                                  disponibilityValue = Disponibility.FUTURE;
                                  addProductController
                                      .valueDisponibility.value = "FUTUR";
                                },
                              );
                              // print(addProductController.valueDisponibility);
                            },
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          addProductController.valueDisponibility.value ==
                                  "IMMEDIAT"
                              ? const SizedBox()
                              : DateField(
                                  dateController:
                                      addProductController.dataController.value,
                                  hintText: "Choisir la date de disponibilité",
                                ),
                        ],
                      ),
          ),
        ],
      ),
    );
  }
}
