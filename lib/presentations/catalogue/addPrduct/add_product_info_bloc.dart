// ignore_for_file: constant_identifier_names

import 'package:eagri/presentations/catalogue/addPrduct/controllers/add_product_controller.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:select_form_field/select_form_field.dart';

enum Disponibility { FUTURE, IMMEDIAT }

class AddProductInfoBloc extends StatefulWidget {
  const AddProductInfoBloc({super.key, required this.isOffreAchat});
  final bool isOffreAchat;
  @override
  State<AddProductInfoBloc> createState() => _AddProductInfoBlocState();
}

class _AddProductInfoBlocState extends State<AddProductInfoBloc> {
  Disponibility disponibilityValue = Disponibility.IMMEDIAT;
  AddProductController addProductController = Get.find();
  @override
  void initState() {
   
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      child: ListView(
        padding: const EdgeInsets.only(bottom: 80),
        children: [
          const SizedBox(
            height: 30,
          ),
          Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "Information sur le produit",
                style: getBoldTextStyle(color: Colors.black, fontSize: 19),
              )),
          const SizedBox(
            height: 30,
          ),
          Obx(
            () => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text('Produit'),
                const SizedBox(
                  height: 5,
                ),
                SelectFormField(
                  type: SelectFormFieldType.dialog, // or can be dialog
                  enableSearch: true,
                  // initialValue: 'circle',
                  labelText: 'Choisir le produit',
                  controller: addProductController.productTextValue.value,
                  items: addProductController.listeSpeculationMap,
                  onChanged: (val) {
                    addProductController
                        .changeUniteBySpeculation(int.parse(val));
                    addProductController.getCategorie();
                  },
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          Obx(
            () => addProductController.listUnite.isEmpty
                ? Container()
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text('Unité'),
                      const SizedBox(
                        height: 5,
                      ),
                      SelectFormField(
                        type: SelectFormFieldType.dropdown, // or can be dialog
                        labelText: "Choisir l'unité du produit",
                        controller:
                            addProductController.productUniteTextValue.value,
                        items: addProductController.listUniteMap,
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                    ],
                  ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text('Precisez la quantité du produit'),
              const SizedBox(
                height: 5,
              ),
              TextFormField(
                controller: addProductController.productQuantityValue.value,
                decoration: InputDecoration(
                  hintText: "Entrez la quantité",
                  label: Text(
                    "Quantité",
                    style: getRegularTextStyle(color: Colors.grey),
                  ),
                ),
                keyboardType: TextInputType.number,
              ),
            ],
          ),
          const SizedBox(
            height: 30,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text('Precisez le prix du produit'),
              const SizedBox(
                height: 5,
              ),
              TextFormField(
                controller: addProductController.productPriceValue.value,
                decoration: InputDecoration(
                  hintText: "Entrez le prix",
                  label: Text(
                    "Prix",
                    style: getRegularTextStyle(color: Colors.grey),
                  ),
                ),
                keyboardType: TextInputType.number,
              ),
            ],
          ),
          const SizedBox(
            height: 30,
          ),
          if(addProductController.productTypeOffreValue.value.text=="PREACHAT" && addProductController.productPriceValue.value.text!="")...[
              Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text('Combien êtes-vous prêt à payer'),
              const SizedBox(
                height: 5,
              ),
              TextFormField(
                controller: addProductController.pricePreOrderValue.value,
                decoration: InputDecoration(
                  hintText: "Montant d'avance",
                  label: Text(
                    "Montant",
                    style: getRegularTextStyle(color: Colors.grey),
                  ),
                ),
                keyboardType: TextInputType.number,
              ),
            ],
          ),
          const SizedBox(
            height: 30,
          ),
          ]
        
        ],
      ),
    );
  }
}
