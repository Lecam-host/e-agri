import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/app/constant.dart';
import 'package:eagri/domain/usecase/shop_usecase.dart';
import 'package:eagri/presentations/catalogue/addPrduct/add_picture.dart';
import 'package:eagri/presentations/catalogue/addPrduct/add_product_choise_methodPaiement_bloc.dart';
import 'package:eagri/presentations/catalogue/addPrduct/add_product_info_bloc.dart';
import 'package:eagri/presentations/catalogue/addPrduct/add_product_location_bloc.dart';
import 'package:eagri/presentations/catalogue/addPrduct/controllers/add_product_controller.dart';
import 'package:eagri/presentations/common/default_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../app/di.dart';

import '../../../app/functions.dart';
import '../../common/buttons/back_button.dart';
import 'add_description.dart';
import 'add_info_disponibilite.dart';

class AddProductView extends StatefulWidget {
  const AddProductView({Key? key, this.isOffreAchat = false,this.typeOffer = "VENTE"}) : super(key: key);
  final bool isOffreAchat;
  final String typeOffer;
  @override
  State<AddProductView> createState() => _AddProductViewState();
}

class _AddProductViewState extends State<AddProductView> {
  ShopUseCase shopUseCase = instance<ShopUseCase>();
  AddProductController addProductController = Get.put(AddProductController());
  // AddProductOnMarketrequest addProductOnMarketrequest =AddProductOnMarketrequest();
int page = 0;
    bool keyBoardIsOpen =false;
  @override
  void initState() {
    // addProductController.onInit();
    
   addProductController.productTypeOffreValue.value.text =widget.typeOffer;
    addProductController.getRegion(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    page = 0;
     keyBoardIsOpen = MediaQuery.of(context).viewInsets.bottom > 0;
    return Scaffold(
      // floatingActionButton: FloatingActionButton(onPressed: ()async {
      //   inspect(addProductController.listeSpeculation);
      // },),
      appBar: AppBar(
        centerTitle: true,
        leading: const BackButtonCustom(),
        title: Text(widget.isOffreAchat == false
            ? "Publier un produit"
            : "Publier votre demande"),
      ),
      body: SafeArea(
        child: Container(
          margin: const EdgeInsets.all(10),
          child: Column(
            children: [
              Expanded(
                child: PageView(
                  onPageChanged: (value) {
                    setState(() {
                       addProductController.pageIndex.value = value;
                    });
                   
                  },
                  controller: addProductController.pageController.value,
                  physics: const NeverScrollableScrollPhysics(),
                  children: [
                    //if (widget.isOffreAchat == false)
                    // const AddProductChoiseOffreBloc(),
                    AddProductInfoBloc(
                      isOffreAchat: widget.isOffreAchat,
                    ),
                    if (widget.isOffreAchat == false)
                      AddInfoDisonibilite(
                        isOffreAchat: widget.isOffreAchat,
                      ),
                    const AddProductLocationBloc(),
                    if (widget.isOffreAchat == false) const AddProductImageBloc(),
                    const AddProductChoiseMethodPaiementBloc(),
                    AddDescription(
                      isOffreAchat: widget.isOffreAchat,
                    ),
                  ],
              
                ),
              ),
             controllerBoutton(),
             
            ],
          ),
        ),
      ),
      // bottomSheet: keyBoardIsOpen
      //     ?const SizedBox(
      //         height: 0,
      //       )
      //     :
    );
    
  }

  Widget controllerBoutton(){
   return  Container(
              margin: const EdgeInsets.only(
                  left: 10, right: 10, top: 10, bottom: 30),
              child: Obx(() => Row(
                    children: [
                      (addProductController.pageIndex >= 1)
                          ? Expanded(
                              child: DefaultButton(
                                text: "Retour",
                                // text: pageController.page!.toInt().toString(),
                                color: Colors.black,
                                press: () {
                                  addProductController.pageController.value
                                      .previousPage(
                                          duration:
                                              const Duration(milliseconds: 300),
                                          curve: Curves.bounceInOut);
                                },
                              ),
                            )
                          :const SizedBox(
                              height: 50,
                            ),
                      const SizedBox(
                        width: 10,
                      ),
                     
                        Expanded(
                          child: DefaultButton(
                            text: widget.isOffreAchat == false
                                ? addProductController.pageIndex >= 5
                                    ? "Publier"
                                    : "Suivant"
                                : addProductController.pageIndex >= 3
                                    ? "Publier"
                                    : "Suivant",
                            press: () {
                              if (addProductController.pageIndex >= page) {
                                if (addProductController
                                    .productTextValue.value.text.isEmpty) {
                                  // print(object)
                                  return Flushbar(
                                    title: "Avertissement",
                                    message: "Veuillez choisir le produit",
                                    duration:
                                        const Duration(milliseconds: 3000),
                                  ).show(context);
                                }
                                if (addProductController
                                    .productUniteTextValue.value.text.isEmpty) {
                                  // print(object)
                                  return Flushbar(
                                    title: "Avertissement",
                                    message:
                                        "Veuillez entrer l'unité du produit",
                                    duration:
                                        const Duration(milliseconds: 3000),
                                  ).show(context);
                                }

                                if (addProductController
                                    .productQuantityValue.value.text.isEmpty) {
                                  // print(object)
                                  return Flushbar(
                                    title: "Avertissement",
                                    message:
                                        "Veuillez entrer la quantité du produit",
                                    duration:
                                        const Duration(milliseconds: 3000),
                                  ).show(context);
                                }
                                if (addProductController
                                    .productPriceValue.value.text.isEmpty) {
                                  // print(object)
                                  return Flushbar(
                                    title: "Avertissement",
                                    message:
                                        "Veuillez entrer le prix du produit",
                                    duration:
                                        const Duration(milliseconds: 3000),
                                  ).show(context);
                                }
                                if (verifPrice(int.parse(addProductController
                                        .productPriceValue.value.text)) ==
                                    false) {
                                  // print(object)
                                  return Flushbar(
                                    title: "Avertissement",
                                    message: "Le prix entré n'est pas correct",
                                    duration:
                                        const Duration(milliseconds: 3000),
                                  ).show(context);
                                }
                              
                                if(addProductController.productTypeOffreValue.value.text=="PREACHAT"){
                                  if (addProductController
                                    .pricePreOrderValue.value.text.isEmpty && addProductController.productTypeOffreValue.value.text.isEmpty) {
                                  // print(object)
                                  return Flushbar(
                                    title: "Avertissement",
                                    message:
                                        "Entrer le montant d'avance",
                                    duration:
                                        const Duration(milliseconds: 3000),
                                  ).show(context);
                                }
                                   if (verifPrice(int.parse(addProductController
                                        .pricePreOrderValue.value.text)) ==
                                    false && addProductController.productTypeOffreValue.value.text.isEmpty) {
                                  // print(object)
                                  return Flushbar(
                                    title: "Avertissement",
                                    message: "Le montant d'avance entré n'est pas correct",
                                    duration:
                                        const Duration(milliseconds: 3000),
                                  ).show(context);
                                }
                                }
                               
                                if (addProductController.pageIndex >= 2 &&widget.isOffreAchat==false) {
                                  if (addProductController
                                      .productRegionValue.value.text.isEmpty) {
                                    // print(object)
                                    return Flushbar(
                                      title: "Avertissement",
                                      message: "Veuillez precisez le lieu",
                                      duration:
                                          const Duration(milliseconds: 3000),
                                    ).show(context);
                                  }
                                }  if (addProductController.pageIndex >= 1 &&widget.isOffreAchat==true) {
                                  if (addProductController
                                      .productRegionValue.value.text.isEmpty) {
                                    // print(object)
                                    return Flushbar(
                                      title: "Avertissement",
                                      message: "Veuillez precisez le lieu",
                                      duration:
                                          const Duration(milliseconds: 3000),
                                    ).show(context);
                                  }
                                }
                              }
                              if (widget.isOffreAchat == false) {
                                if (addProductController.pageIndex.value == 1) {
                                  if (addProductController.productTypeVenteValue
                                      .value.text.isEmpty) {
                                    // print(object)
                                    return Flushbar(
                                      title: "Avertissement",
                                      message: "Precisez le type de vente",
                                      duration:
                                          const Duration(milliseconds: 3000),
                                    ).show(context);
                                  }
                                  if (addProductController
                                              .valueDisponibility.value !=
                                          "IMMEDIAT" &&
                                      addProductController
                                          .dataController.value.text.isEmpty) {
                                    // print(object)
                                    return Flushbar(
                                      title: "Avertissement",
                                      message:
                                          "Veuillez entrer la date de disponibilité",
                                      duration:
                                          const Duration(milliseconds: 3000),
                                    ).show(context);
                                  }
                                }
                              } else {
                                if (addProductController.pageIndex.value == 2) {
                                  if (addProductController
                                      .productRegionValue.value.text.isEmpty) {
                                    // print(object)
                                    return Flushbar(
                                      title: "Avertissement",
                                      message: "Veuillez precisez le lieu",
                                      duration:
                                          const Duration(milliseconds: 3000),
                                    ).show(context);
                                  }
                                }
                              }
                              if (widget.isOffreAchat == false) {
                                if (addProductController.pageIndex.value == 4) {
                                  if (addProductController
                                          .methodPaiementId.value ==
                                      0) {
                                    return Flushbar(
                                      title: "Avertissement",
                                      message: "Précisez le moyen de paiemennt",
                                      duration:
                                          const Duration(milliseconds: 3000),
                                    ).show(context);
                                  }
                                  if (addProductController
                                              .methodPayment.value.code ==
                                          codePaiementCredit &&
                                      addProductController.nbPayementCreditValue
                                          .value.text.isEmpty) {
                                    return Flushbar(
                                      title: "Avertissement",
                                      message: "Precisez la fréquence",
                                      duration:
                                          const Duration(milliseconds: 3000),
                                    ).show(context);
                                  }
                                  if (addProductController
                                              .methodPayment.value.code ==
                                          codePaiementCredit &&
                                      addProductController
                                          .unitNbPayementCreditValue
                                          .value
                                          .text
                                          .isEmpty) {
                                    return Flushbar(
                                      title: "Avertissement",
                                      message: "Precisez l'unité",
                                      duration:
                                          const Duration(milliseconds: 3000),
                                    ).show(context);
                                  }
                                }
                              }

                              addProductController.pageController.value
                                  .nextPage(
                                duration: const Duration(milliseconds: 300),
                                curve: Curves.bounceInOut,
                              );
                              if (widget.isOffreAchat == true) {
                                if (addProductController.pageIndex >= 3) {
                                  addProductController.sendProduct();
                                }
                              } else {
                                if (addProductController.pageIndex >= 5) {
                                  addProductController.sendProduct();
                                }
                              }
                            },
                          ),
                        )
                    ],
                  )),
            );
  }
}
