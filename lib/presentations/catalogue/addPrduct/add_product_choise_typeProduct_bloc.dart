// ignore_for_file: file_names

import 'package:eagri/presentations/catalogue/addPrduct/add_product_view.dart';
import 'package:eagri/presentations/ressources/assets_manager.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../prestations/addPrestation/add_screen.dart';
import '../../ressources/styles_manager.dart';
import '../../transport/addTransportOffer/add_car_screen.dart';
import '../publierIntra/add_intra_screen.dart';

class AddProductChoiseTypeProductBloc extends StatelessWidget {
  const AddProductChoiseTypeProductBloc({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButton(
          color: Colors.black,
        ),
        title: const Text("Publier une offre"),
        centerTitle: true,
      ),
      body: Container(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Type de publication",
                style: getSemiBoldTextStyle(
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            Wrap(
              alignment: WrapAlignment.center,
              children: [
                cardButtonCustom("Agricole", const AddProductView(),
                    IconAssetManager.adviceIcon),
                cardButtonCustom("Intrant", const PublierView(),
                    IconAssetManager.adviceIcon),
                cardButtonCustom("Prestation", const AddPrestationScreen(),
                    IconAssetManager.serviceIcon),
                cardButtonCustom("Transport", const AddTransportScreen(),
                    IconAssetManager.transportIcon),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget cardButtonCustom(String titre, Widget page, String icon) {
    return GestureDetector(
      onTap: () {
        Get.to(page);
      },
      child: Container(
        margin: const EdgeInsets.all(10),
        child: Column(
          children: [
            CircleAvatar(
              backgroundColor: ColorManager.white,
              radius: 30,
              child: Image.asset(
                icon,
                scale: 4 / 2,
              ),
            ),
            Text(
              titre,
              style: getSemiBoldTextStyle(fontSize: 15),
            )
          ],
        ),
      ),
    );
  }
}
