import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'controllers/add_product_controller.dart';

class AddProductResumeView extends StatelessWidget {
  const AddProductResumeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AddProductController>(
      builder: (addProductController) => Column(
        children: [
          const SizedBox(
            height: 30,
          ),
          Text(addProductController.productPriceValue.value.text),
          Text(addProductController.productRegionValue.value.text),
          Text(addProductController.productPriceValue.value.text),
          Text(addProductController.productRegionValue.value.text),
          Text(addProductController.productPriceValue.value.text),
          Text(addProductController.productRegionValue.value.text),
        ],
      ),
      init: AddProductController(),
    );
  }
}
