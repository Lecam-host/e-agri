// ignore_for_file: constant_identifier_names

import 'package:eagri/presentations/catalogue/addPrduct/controllers/add_product_controller.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddDescription extends StatefulWidget {
  const AddDescription({super.key, required this.isOffreAchat});
  final bool isOffreAchat;
  @override
  State<AddDescription> createState() => _AddDescriptionState();
}

class _AddDescriptionState extends State<AddDescription> {
  AddProductController addProductController = Get.find();
  @override
  void initState() {
    if (widget.isOffreAchat == true) {
      addProductController.productTypeVenteValue.value.text = "ACHAT";
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      child: ListView(
        padding: const EdgeInsets.only(bottom: 80),
        children: [
          const SizedBox(
            height: 30,
          ),
          Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "Dites quelque chose sur votre publication",
                style: getBoldTextStyle(color: Colors.black, fontSize: 19),
              )),
          const SizedBox(
            height: 30,
          ),
          TextFormField(
            controller: addProductController.productDescriptionValue.value,
            decoration: InputDecoration(
              hintText: "Commentaire",
              label: Text(
                "Entrer le commentaire",
                style: getRegularTextStyle(color: Colors.grey),
              ),
            ),
            keyboardType: TextInputType.multiline,
            maxLines: null,
          ),
        ],
      ),
    );
  }
}
