// ignore_for_file: file_names

import 'package:eagri/app/constant.dart';
import 'package:eagri/presentations/catalogue/addPrduct/controllers/add_product_controller.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:select_form_field/select_form_field.dart';

import '../../../domain/model/methode_paiement_model.dart';
import '../../ressources/color_manager.dart';

class AddProductChoiseMethodPaiementBloc extends StatefulWidget {
  const AddProductChoiseMethodPaiementBloc({super.key});

  @override
  State<AddProductChoiseMethodPaiementBloc> createState() =>
      _AddProductChoiseMethodPaiementBlocState();
}

class _AddProductChoiseMethodPaiementBlocState
    extends State<AddProductChoiseMethodPaiementBloc> {
  final AddProductController addProductController =
      Get.find();
  late PaymentMethodModel methodPaiement;
  @override
  void initState() {
    if (addProductController.methodPaiementId.value == 0) {
      methodPaiement = addProductController.listMethodPaiement
          .firstWhere((element) => element.code == codePaiementOnligne);
      addProductController.methodPaiementId.value = methodPaiement.id!;
    } else {
      methodPaiement = addProductController.listMethodPaiement.firstWhere(
          (element) =>
              element.id.toString() ==
              addProductController.methodPaiementId.value.toString());
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Choisissez le moyen de paiement que vous préférez",
            style: getBoldTextStyle(
              color: Colors.black,
              fontSize: 20,
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          ...List.generate(
            addProductController.listMethodPaiement.length,
            (index) => ListTile(
              title: Text(addProductController.listMethodPaiement[index].name!),
              leading: Radio<PaymentMethodModel>(
                activeColor: ColorManager.primary,
                value: addProductController.listMethodPaiement[index],
                groupValue: methodPaiement,
                onChanged: (PaymentMethodModel? value) {
                  setState(() {
                    addProductController.nbPayementCreditValue.value.clear();
                    methodPaiement =
                        addProductController.listMethodPaiement[index];
                    addProductController.unitNbPayementCreditValue.value
                        .clear();
                  });
                  addProductController.methodPayment.value = value!;
                  addProductController.methodPaiementId.value =
                      addProductController.listMethodPaiement[index].id!;
                },
              ),
            ),
          ),
          if (methodPaiement.code == codePaiementCredit) ...[
            const SizedBox(
              height: 30,
            ),
            const Text("Frequence de payement"),
            const SizedBox(
              height: 30,
            ),
            TextFormField(
              controller: addProductController.nbPayementCreditValue.value,
              decoration: InputDecoration(
                hintText: "Frequence",
                label: Text(
                  "Entrer la Fréquance",
                  style: getRegularTextStyle(color: Colors.grey),
                ),
              ),
              keyboardType: TextInputType.number,
            ),
            const SizedBox(
              height: 30,
            ),
            
            SelectFormField(
              type: SelectFormFieldType.dropdown, // or can be dialog
              // initialValue: 'circle',
              labelText: 'Unité',
             controller: addProductController.unitNbPayementCreditValue.value,
              items: addProductController.listeUnitNbPayementCrediMap,
             
            ),
          ],
        ],
      ),
    );
  }
}
