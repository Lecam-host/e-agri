import 'package:flutter/material.dart';

import '../../common/buttons/back_button.dart';

AppBar appBarCustom() {
  return AppBar(
    centerTitle: true,
    leading: const BackButtonCustom(),
    title: const Text("Publier un produit"),
  );
}
