import 'dart:io';

import 'package:eagri/presentations/catalogue/addPrduct/controllers/add_product_controller.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import '../../../app/constant.dart';
import '../../ressources/color_manager.dart';

class AddProductImageBloc extends StatelessWidget {
  const AddProductImageBloc({super.key});

  @override
  Widget build(BuildContext context) {
    AddProductController addProductController = Get.put(AddProductController());
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 30,
        ),
        Container(
          alignment: Alignment.centerLeft,
          child: Text(
            "Ajouter des images pour publier votre produit",
            style: getBoldTextStyle(color: Colors.black, fontSize: 19),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Text(
          "${Constant.nbImageMax} images maximum",
          style: getRegularTextStyle(color: Colors.black, fontSize: 14),
        ),
        const SizedBox(
          height: 30,
        ),
        Obx(
          () {
            return Wrap(
              spacing: 20,
              runSpacing: 20,
              children: [
                ...addProductController.files.map(
                  (item) {
                    return Stack(
                      clipBehavior: Clip.none,
                      alignment: AlignmentDirectional.bottomCenter,
                      children: [
                        Container(
                          width: 150,
                          height: 150,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(7),
                              border: Border.all(
                                color: Colors.grey,
                                width: 1,
                              ),
                              image: DecorationImage(
                                  image: FileImage(File(item.path)),
                                  fit: BoxFit.cover)),
                          // child: Image.file(),
                        ),
                        Positioned(
                          right: -10,
                          top: -10,
                          child: GestureDetector(
                            onTap: () {
                              addProductController.files.removeAt(
                                  addProductController.files.indexOf(item));
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.red,
                                  borderRadius: BorderRadius.circular(10)),
                              child: const Icon(
                                Icons.close,
                                color: Colors.white,
                                size: 25,
                              ),
                            ),
                          ),
                        )
                      ],
                    );
                  },
                ),
                if (addProductController.files.length < Constant.nbImageMax)
                  Center(
                    child: GestureDetector(
                      onTap: () {
                        // pickImages(addProductController);
                        selectGalleryOrCamera();
                      },
                      child: Container(
                        width: 100,
                        height: 100,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(7),
                          border: Border.all(
                            color: Colors.grey,
                            width: 1,
                          ),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(
                              Icons.add,
                              color: Colors.grey,
                            ),
                            Text(
                              "Ajouter",
                              style: getRegularTextStyle(color: Colors.grey),
                            )
                          ],
                        ),
                      ),
                    ),
                  )
              ],
            );
          },
        )
      ],
    );
  }
}

// Get defaultDialog
selectGalleryOrCamera() {
  return Get.defaultDialog(
    title: "Ajouter une image",
    titleStyle: TextStyle(
      color: ColorManager.primary2,
    ),
    content: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          onPressed: () {
            pickImages(Get.find(), ImageSource.camera);
          },
          icon: const Icon(
            Icons.camera_alt,
            size: 35,
          ),
        ),
        const SizedBox(width: 20),
        IconButton(
          onPressed: () {
            pickImages(Get.find(), ImageSource.gallery);
          },
          icon: const Icon(Icons.image, size: 35),
        ),
      ],
    ),
  );
}

Future<void> pickImages(
    AddProductController controller, ImageSource source) async {
  final picker = ImagePicker();
  final XFile? selectedImages =
      await picker.pickImage(imageQuality: 50, source: source);
  Get.back();
  if (selectedImages != null) {
    controller.addFileAndConvert(selectedImages);
    // Get.back();
    // controller.files.refresh();
  }
  // Get.back();
}
