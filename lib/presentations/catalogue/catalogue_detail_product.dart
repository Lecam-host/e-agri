// // ignore_for_file: must_be_immutable, use_build_context_synchronously

// import 'package:another_flushbar/flushbar.dart';
// import 'package:eagri/app/constant.dart';
// import 'package:eagri/data/repository/api/repository_impl.dart';
// import 'package:eagri/domain/model/Product.dart';
// import 'package:eagri/presentations/catalogue/catalogue_body.dart';
// import 'package:eagri/presentations/catalogue/updateProdcut/controller/update_product_controller.dart';
// import 'package:eagri/presentations/common/flushAlert_componenent.dart';
// import 'package:eagri/presentations/common/state/loader_component.dart';
// import 'package:eagri/presentations/details_produit/components/custom_details_app_bar_catalogue.dart';
// import 'package:eagri/presentations/image/update_image_screen.dart';
// import 'package:eagri/presentations/ressources/color_manager.dart';
// import 'package:eagri/presentations/ressources/styles_manager.dart';
// import 'package:eagri/presentations/shop/controller/shop_controller.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:lottie/lottie.dart';
// import 'package:select_form_field/select_form_field.dart';

// import '../../app/di.dart';
// import '../common/default_button.dart';
// import '../details_produit/components/loader_details_produc.dart';
// import '../details_produit/components/product_images_catalogue.dart';
// import '../ressources/assets_manager.dart';
// import 'controllers/catalogue_controller.dart';

// class CatalogueDetailsProduitScreen extends StatefulWidget {
//   final ProductModel product;

//   const CatalogueDetailsProduitScreen({Key? key, required this.product})
//       : super(key: key);

//   @override
//   State<CatalogueDetailsProduitScreen> createState() =>
//       _CatalogueDetailsProduitScreenState();
// }

// class _CatalogueDetailsProduitScreenState
//     extends State<CatalogueDetailsProduitScreen> {
 
//   late ProductModel productShow;

 

//   bool isLoadDetails = true;
//   getDetailsProduct() async {
//     if (mounted) {
//       await shopController.getDetailsProduct(widget.product).then((value) {
//         if (mounted) {
       
//         }
//       });
//     }
//   }

//   @override
//   void initState() {
//     productShow = widget.product;

//     getDetailsProduct();

//     // updateProductController.init(catalogueController.selectedProduct.value! );

//     super.initState();
//   }

//   @override
//   void dispose() {
//     updateProductController.productRegion.clear();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: const Color(0xFFF5F6F9),
//       appBar: PreferredSize(
//         preferredSize: Size.fromHeight(AppBar().preferredSize.height),
//         child: CustomAppBarCatalogue(
//             product: catalogueController.selectedProduct.value!),
//       ),
//       bottomSheet: catalogueController.selectedProduct.value!.status !=
//               "DELETED"
//           ? Container(
//               margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
//               child: Row(
//                 children: [
//                   // Expanded(
//                   //   child: DefaultButton(
//                   //     color: ColorManager.black,
//                   //     text: "Disponibilité",
//                   //     press: () {

//                   //     },
//                   //   ),
//                   // ),
//                   // const SizedBox(
//                   //   width: 10,
//                   // ),
//                   Expanded(
//                     child: DefaultButton(
//                       color: ColorManager.blue,
//                       text: "Modifier",
//                       press: () {
//                         // Get.to(const UpdateProductView());
//                         modifProduct(catalogueController);
//                       },
//                     ),
//                   ),
//                 ],
//               ),
//             )
//           : const SizedBox(),
//       body: isLoadDetails == true
//           ? ListView(
//               children: [
//                 ProductImagesCatalogue(
//                   product: productShow,
//                 ),
//                 loadDetailProduct()
//               ],
//             )
//           : RefreshIndicator(
//               onRefresh: () async {
//                 await getDetailsProduct();
//                 showCustomFlushbar(context, "Terminer", ColorManager.blue,
//                     FlushbarPosition.TOP);
//               },
//               child: BodyCatalogue(
//                 product: productShow,
//               ),
//             ),
//     );
//   }

//   motifRetrait() {
//     Get.bottomSheet(Container(
//       padding: const EdgeInsets.all(10),
//       margin: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
//       decoration: BoxDecoration(
//           color: Colors.white, borderRadius: BorderRadius.circular(10)),
//       child: Column(
//         mainAxisSize: MainAxisSize.min,
//         children: [
//           Text(
//             "Veuillez précisement la raison du rétrait ?",
//             style: getBoldTextStyle(color: Colors.black, fontSize: 15),
//           ),
//           ListView.builder(
//             padding: const EdgeInsets.only(bottom: 10),
//             shrinkWrap: true,
//             itemCount: listMotif.length,
//             itemBuilder: (context, index) {
//               return RadioListTile(
//                 value: listMotif[index],
//                 groupValue: listMotif,
//                 onChanged: (value) {},
//                 title: Text(listMotif[index]),
//               );
//             },
//           ),
//           Row(
//             children: [
//               Expanded(
//                 child: DefaultButton(
//                   text: "Retirer",
//                   color: ColorManager.red,
//                   press: () {
//                     Get.back();
//                     Get.dialog(const LoaderComponent());
//                     catalogueController.deleteProduct(
//                         catalogueController.selectedProduct.value!);
//                   },
//                 ),
//               ),
//               const SizedBox(
//                 width: 10,
//               ),
//               Expanded(
//                 child: DefaultButton(
//                   text: "Annuler",
//                   color: ColorManager.grey,
//                   press: () {
//                     Get.back();
//                   },
//                 ),
//               ),
//             ],
//           )
//         ],
//       ),
//     ));
//   }
// }
