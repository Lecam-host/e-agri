import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/app/functions.dart';
import 'package:eagri/presentations/market_price/request_history_price_view.dart';
import 'package:eagri/presentations/ressources/assets_manager.dart';
import 'package:eagri/presentations/ressources/icon_manager.dart';
import 'package:eagri/presentations/ressources/strings_manager.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

import '../../app/constant.dart';
import '../../app/di.dart';
import '../../data/request/request.dart';
import '../../data/request/request_object.dart';
import '../../domain/model/location_model.dart';
import '../../domain/model/model.dart';
import '../../domain/model/user_model.dart';
import '../../domain/usecase/speculation_price_usecase.dart';
import '../common/date_field.dart';
import '../common/flushAlert_componenent.dart';
import '../ressources/color_manager.dart';
import '../ressources/font_manager.dart';
import '../ressources/styles_manager.dart';
import '../ressources/values_manager.dart';
import 'add_speculation_price.dart';
import 'comparaison_price_view.dart';
import 'first_view.dart';
import 'list_speculation_price_user_view.dart';

class MarketPriceView extends StatefulWidget {
  const MarketPriceView({Key? key}) : super(key: key);

  @override
  State<MarketPriceView> createState() => _MarketPriceViewState();
}

class _MarketPriceViewState extends State<MarketPriceView> {
  List<Map<String, dynamic>> listlocaltionBysForComparaison = [];

  List<RegionModel> listRegion = [];
  List<SpeculationModel> listSpeculation = [];
  SpeculationModel? speculationChoisiPourComparaison;
  List<RegionModel> listRegionChoisi = [];
  List<SpeculationModel> listSpeculationChoisi = [];
  List<LocationBodyId> localtionBys = [];
  SpeculationPriceUseCase speculationPriceUseCase =
      instance<SpeculationPriceUseCase>();

  TextEditingController dateDebutController = TextEditingController();
  TextEditingController dateFinController = TextEditingController();
  getScope() {}
  Future getListSpeculation() async {
    (await speculationPriceUseCase.getListSpeculation()).fold((failure) {},
        (result) {
      if (mounted) {
        setState(() {
          listSpeculation = result.data!;
        });
      }
    });
  }

  Future getListRegion() async {
    (await speculationPriceUseCase.getListRegion()).fold((failure) {},
        (result) {
      if (mounted) {
        setState(() {
          listRegion = result.data;
        });
      }
    });
  }

  @override
  void initState() {
    getListSpeculation();
    getListRegion();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
        builder: (context, user, child) => Scaffold(
              appBar: AppBar(
                title: const Text('Prix sur le marché'),
                leading: const SizedBox(),
              ),
              body: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: Text(
                        "Catégories de prix",
                        style: getBoldTextStyle(
                          color: ColorManager.black,
                          fontSize: 15,
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          if (isInScopes(
                                  UserPermissions.SHOW_PRIX_OFFICIEL_PRODUIT,
                                  user.scopes!) ==
                              true)
                            categorySpeculationPriceCard(
                              icon: Icons.verified,
                              titre: "Prix officiel",
                              nextWidget: const FirstView(),
                            ),
                          if (isInScopes(
                                  UserPermissions
                                      .SHOW_PRIX_NON_OFFICIEL_PRODUIT,
                                  user.scopes!) ==
                              true)
                            categorySpeculationPriceCard(
                                icon: Icons.bookmark,
                                titre: "Prix non officiel",
                                nextWidget:
                                    const ListSpeculationPriceUserView()),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    const Divider(),
                    const SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: Text(
                        "Autres",
                        style: getBoldTextStyle(
                          color: ColorManager.black,
                          fontSize: 15,
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Column(
                      children: [
                        if (isInScopes(
                                UserPermissions.ADD_PRIX, user.scopes!) ==
                            true)
                          menuCard(
                            icon: Icons.add,
                            titre: "Ajouter un prix",
                            onTap: () {
                              Navigator.push(
                                context,
                                PageTransition(
                                  type: PageTransitionType.bottomToTop,
                                  child: const AddSpeculationPriceView(),
                                  isIos: true,
                                  duration: const Duration(milliseconds: 400),
                                ),
                              );
                            },
                          ),
                        const SizedBox(
                          height: 10,
                        ),
                        if (isInScopes(UserPermissions.SHOW_HISTORY_PRIX,
                                user.scopes!) ==
                            true)
                          menuCard(
                            icon: Icons.history,
                            titre: "Consulter l'historique des prix",
                            onTap: () {
                              Navigator.push(
                                context,
                                PageTransition(
                                  type: PageTransitionType.bottomToTop,
                                  child: const RequestHistoryPriceView(),
                                  isIos: true,
                                  duration: const Duration(milliseconds: 400),
                                ),
                              );
                            },
                          ),
                        const SizedBox(
                          height: 10,
                        ),
                        if (isInScopes(
                                UserPermissions.COMPARE_PRIX, user.scopes!) ==
                            true)
                          menuCard(
                            icon: Icons.compare,
                            titre: "Comparer des prix",
                            onTap: () {
                              comparePrice();
                            },
                          ),
                      ],
                    ),

                    const SizedBox(
                      height: 50,
                    ),

                    // Text(
                    //   AppStrings.lorem,
                    //   style: getRegularTextStyle(
                    //     fontSize: 15,
                    //     color: ColorManager.black,
                    //   ),
                    //   maxLines: 4,
                    // ),
                  ],
                ),
              ),
            ));
  }

  comparePrice() {
    showModalBottomSheet(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
          ),
        ),
        isDismissible: false,
        context: context,
        builder: (context) {
          return StatefulBuilder(
              builder: (context, setInnerState) => SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        const SizedBox(
                          height: 20,
                        ),
                        const Text("Comparer"),
                        const SizedBox(
                          height: 20,
                        ),
                        Container(
                          margin: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: ColorManager.grey.withOpacity(0.1)),
                          child: DropdownButton(
                            hint: const Text(AppStrings.speculationQuestion),
                            dropdownColor: ColorManager.white,
                            isExpanded: true,
                            alignment: AlignmentDirectional.centerEnd,
                            value: speculationChoisiPourComparaison,
                            icon: const Icon(Icons.keyboard_arrow_down),
                            items:
                                listSpeculation.map((SpeculationModel items) {
                              return DropdownMenuItem(
                                value: items,
                                child: Text(items.name!),
                              );
                            }).toList(),
                            onChanged: (SpeculationModel? newValue) {
                              setInnerState(() {
                                speculationChoisiPourComparaison = newValue!;
                              });
                              setState(() {
                                speculationChoisiPourComparaison = newValue!;
                              });
                            },
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Container(
                          margin: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: ColorManager.grey.withOpacity(0.1)),
                          child: MultiSelectDialogField(
                            buttonIcon: const Icon(Icons.keyboard_arrow_down),
                            buttonText: const Text(
                              AppStrings.regionQuestion,
                            ),
                            confirmText: const Text(AppStrings.ok),
                            cancelText: const Text(AppStrings.cancel),
                            title: const Text(AppStrings.regionQuestion),
                            items: listRegion
                                .map((e) => MultiSelectItem(e, e.name!))
                                .toList(),
                            listType: MultiSelectListType.LIST,
                            onConfirm: (List<RegionModel> regions) {
                              for (var element in regions) {
                                setState(() {
                                  listlocaltionBysForComparaison.add(
                                      LocationBodyId(regionId: element.regionId)
                                          .toJson());
                                });
                              }
                              setInnerState(() {
                                listRegionChoisi = regions;
                              });
                              setState(() {
                                listRegionChoisi = regions;
                              });
                            },
                            selectedColor: ColorManager.primary,
                          ),
                        ),
                         DateField(
                            dateController: dateDebutController,
                            hintText: "${AppStrings.dateDebut} *",
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          DateField(
                            dateController: dateFinController,
                            hintText: "${AppStrings.dateFin} *",
                          ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                elevation: 0,
                                backgroundColor: ColorManager.grey,
                              ),
                              onPressed: () {
                                listRegionChoisi = [];
                                listlocaltionBysForComparaison = [];
                                speculationChoisiPourComparaison = null;
                                Navigator.pop(context);
                              },
                              child: const Text(
                                AppStrings.cancel,
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                elevation: 0,
                                backgroundColor: ColorManager.primary,
                              ),
                              onPressed: () {
                                if (listRegionChoisi.isEmpty ||
                                    speculationChoisiPourComparaison == null||dateDebutController.text.isEmpty||dateFinController.text.isEmpty) {
                                  showCustomFlushbar(
                                      context,
                                      "Veuillez remplir les champs",
                                      ColorManager.blue,
                                      FlushbarPosition.BOTTOM);
                                } else {
                                 
                                 
                                  Navigator.push(
                                    context,
                                    PageTransition(
                                      type: PageTransitionType.bottomToTop,
                                      child: ComparaisonPriceView(
                                        regions: listRegionChoisi,
                                        compareRequest: CompareSpeculationPriceRequest(
                                            from:convertDate(dateDebutController.text) ,
                                            to:convertDate(dateFinController.text) ,
                                            userType: true,
                                            speculationId:
                                                speculationChoisiPourComparaison!
                                                    .speculationId,
                                            locationByid:
                                                listlocaltionBysForComparaison),
                                        speculation:
                                            speculationChoisiPourComparaison,
                                      ),
                                      isIos: true,
                                      duration:
                                          const Duration(milliseconds: 400),
                                    ),
                                  );
                                }
                              },
                              child: const Text(AppStrings.next),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ));
        });
  }

  Container addPrice() {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: ColorManager.white,
      ),
      height: 157,
      child: Row(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width / 2,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Ajouter le prix des produits dans votre region",
                  style: getMeduimTextStyle(
                      color: ColorManager.black, fontSize: FontSize.s20),
                ),
                const Spacer(),
                const SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: AppSize.s40,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        elevation: 0,
                        backgroundColor: ColorManager.primary,
                        textStyle: getRegularTextStyle(
                          color: ColorManager.white,
                        ),
                      ),
                      child: const Text(AppStrings.addPrice),
                      onPressed: () {
                        Navigator.push(
                          context,
                          PageTransition(
                            type: PageTransitionType.bottomToTop,
                            child: const AddSpeculationPriceView(),
                            isIos: true,
                            duration: const Duration(milliseconds: 400),
                          ),
                        );
                      }),
                )
              ],
            ),
          ),
          Image.asset(
            ImageAssets.souscription,
            width: 140,
          ),
        ],
      ),
    );
  }

  menuCard({required IconData icon, required String titre, Function()? onTap}) {
    return Container(
      margin: const EdgeInsets.only(left: 10, right: 10),
      child: ListTile(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        contentPadding: const EdgeInsets.all(10),
        tileColor: ColorManager.black.withOpacity(0.05),
        onTap: onTap,
        title: Text(
          titre,
          style: getMeduimTextStyle(
            fontSize: 15,
            color: ColorManager.black,
          ),
        ),
        leading: Container(
          decoration: BoxDecoration(
              color: ColorManager.white,
              borderRadius: BorderRadius.circular(5)),
          padding: const EdgeInsets.all(10),
          child: Icon(
            icon,
            color: Colors.black,
          ),
        ),
        trailing: const Icon(IconManager.arrowRight),
      ),
    );
  }

  Widget categorySpeculationPriceCard(
      {required IconData icon,
      required String titre,
      required Widget nextWidget}) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          PageTransition(
            type: PageTransitionType.bottomToTop,
            child: nextWidget,
            isIos: true,
            duration: const Duration(milliseconds: 400),
          ),
        );
      },
      child: Container(
        // padding: const EdgeInsets.all(10),
        width: 160,
        height: 50,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: ColorManager.primary,
        ),
        margin: const EdgeInsets.only(left: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              icon,
              color: ColorManager.white.withOpacity(0.5),
              size: 30,
            ),
            const SizedBox(
              width: 7,
            ),
            Text(
              titre,
              style: getBoldTextStyle(color: ColorManager.white),
            ),
          ],
        ),
      ),
    );
  }
}
String convertDate(String date){
  var dateTime = DateTime.parse(date);
  String month=dateTime.month<10?"0${dateTime.month}":dateTime.month.toString();
  String day=dateTime.day<10?"0${dateTime.day}":dateTime.day.toString();
  
return "${dateTime.year}-$month-$day";
}