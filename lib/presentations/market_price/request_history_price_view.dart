import 'package:eagri/data/request/request.dart';
import 'package:eagri/domain/model/location_model.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import '../../app/di.dart';
import '../../domain/usecase/speculation_price_usecase.dart';
import '../common/date_field.dart';
import '../ressources/color_manager.dart';
import '../ressources/strings_manager.dart';
import '../ressources/styles_manager.dart';
import '../ressources/values_manager.dart';
import 'history_price.dart';
import 'market_price_view_model.dart';

class RequestHistoryPriceView extends StatefulWidget {
  const RequestHistoryPriceView({Key? key}) : super(key: key);

  @override
  State<RequestHistoryPriceView> createState() =>
      _RequestHistoryPriceViewState();
}

class _RequestHistoryPriceViewState extends State<RequestHistoryPriceView> {
  MarketPriceViewModel viewModel = instance<MarketPriceViewModel>();
  SpeculationPriceUseCase speculationPriceUseCase =
      instance<SpeculationPriceUseCase>();
  List<SpeculationModel> listSpeculation = [];
  SpeculationModel? speculationChoisi;
  List<RegionModel> listRegions = [];
  RegionModel? region;
  TextEditingController dateDebutController = TextEditingController();
  TextEditingController dateFinController = TextEditingController();

  Future getListSpeculation() async {
    (await speculationPriceUseCase.getListSpeculation()).fold((failure) {
      Navigator.of(context).pop();
    }, (result) {
      if (mounted) {
        setState(() {
          listSpeculation = result.data!;
        });
      }
      // Navigator.of(context).pop();
    });
  }

  Future getListRegion() async {
    (await speculationPriceUseCase.getListRegion()).fold((failure) {},
        (regions) {
      if (mounted) {
        setState(() {
          listRegions = regions.data;
        });
      }
      // Navigator.of(context).pop();
    });
  }

  @override
  void initState() {
    getListSpeculation();
    getListRegion();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppStrings.historyPrice,
          style: getBoldTextStyle(
            color: ColorManager.black,
            fontSize: 12,
          ),
        ),
        leading: const BackButtonCustom(),
      ),
      body: Container(
        margin: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text("${AppStrings.speculationQuestion} *"),
            Container(
              color: ColorManager.grey1.withOpacity(
                0.1,
              ),
              child: DropdownButton(
                hint: const Text(AppStrings.speculationQuestion),
                dropdownColor: ColorManager.white,
                isExpanded: true,
                alignment: AlignmentDirectional.centerEnd,
                value: speculationChoisi,
                icon: const Icon(Icons.keyboard_arrow_down),
                items: listSpeculation.map((SpeculationModel items) {
                  return DropdownMenuItem(
                    value: items,
                    child: Text(items.name!),
                  );
                }).toList(),
                onChanged: (SpeculationModel? speculation) {
                  setState(() {
                    speculationChoisi = speculation;
                  });
                },
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text("${AppStrings.region} *"),
            Container(
              color: ColorManager.grey1.withOpacity(
                0.1,
              ),
              child: DropdownButton(
                hint: const Text(AppStrings.regionQuestion),
                dropdownColor: ColorManager.white,
                isExpanded: true,
                alignment: AlignmentDirectional.centerEnd,
                value: region,
                icon: const Icon(Icons.keyboard_arrow_down),
                items: listRegions.map((RegionModel items) {
                  return DropdownMenuItem(
                    value: items,
                    child: Text(items.name!),
                  );
                }).toList(),
                onChanged: (RegionModel? newValue) {
                  setState(() {
                    region = newValue;
                  });
                },
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            DateField(
              dateController: dateDebutController,
              hintText: "${AppStrings.dateDebut} *",
            ),
            const SizedBox(
              height: 20,
            ),
            DateField(
              dateController: dateFinController,
              hintText: "${AppStrings.dateFin} *",
            ),
            const SizedBox(
              height: 20,
            ),
            Center(
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    elevation: 0,
                    backgroundColor: ColorManager.primary,
                    textStyle: getRegularTextStyle(
                      color: ColorManager.white,
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(
                        AppSize.s12,
                      ),
                    ),
                  ),
                  child: const Text(AppStrings.submit),
                  onPressed: () {
                    if (validForm()) {
                      Navigator.push(
                        context,
                        PageTransition(
                          type: PageTransitionType.rightToLeft,
                          child: HistoryPriceView(
                            regionName: region!.name!,
                            speculation: speculationChoisi!,
                            historyreRequest: SpeculationPriceRequest(
                              regionId: region!.regionId,
                              userId: 1,
                              speculationId: speculationChoisi!.speculationId,
                              official: true,
                              from: dateDebutController.text != ""
                                  ? dateDebutController.text
                                  : null,
                              to: dateFinController.text != ""
                                  ? dateFinController.text
                                  : null,
                            ),
                          ),
                          isIos: true,
                          duration: const Duration(milliseconds: 400),
                        ),
                      );
                    } else {
                      ScaffoldMessenger.of(context)
                        ..removeCurrentSnackBar()
                        ..showSnackBar(
                          const SnackBar(
                            content: Text("Remplir tout les champs"),
                          ),
                        );
                    }
                  }),
            ),
          ],
        ),
      ),
    );
  }

  bool validForm() {
    return speculationChoisi != null &&
        region != null &&
        dateDebutController.text != "" &&
        dateFinController.text != "";
  }
}
