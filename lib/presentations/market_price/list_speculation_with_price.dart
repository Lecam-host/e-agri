import 'package:eagri/domain/model/location_model.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/common/state_renderer/state_render_impl.dart';
import 'package:eagri/presentations/common/state_renderer/state_renderer.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/icon_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';

import '../../app/di.dart';
import '../../app/functions.dart';
import '../../data/request/request.dart';
import '../../domain/usecase/speculation_price_usecase.dart';
import '../base/base_view_model.dart';
import '../common/buttons/search_button.dart';
import '../common/showModalBottomSheetListTile.dart';
import '../conseils/demande_conseil_view.dart';
import '../ressources/assets_manager.dart';
import '../ressources/strings_manager.dart';
import 'components/speculation_with_price_component.dart';
import 'market_price_view_model.dart';

class ListSpeculationWithPriceView extends StatefulWidget {
  const ListSpeculationWithPriceView(
      {Key? key, required this.request, required this.region})
      : super(key: key);
  final RegionModel region;
  final SpeculationPriceRequest request;
  @override
  State<ListSpeculationWithPriceView> createState() =>
      _ListSpeculationWithPriceViewState();
}

class _ListSpeculationWithPriceViewState
    extends State<ListSpeculationWithPriceView> {
  BaseViewModel? baseViewModel;
  MarketPriceViewModel viewModel = instance<MarketPriceViewModel>();
  GetListSpeculationPriceRequest request =
      GetListSpeculationPriceRequest(0, 10);
  ListSpeculationModel listSpeculationModel = ListSpeculationModel(data: []);
  List<SpeculationModel> initialListSpeculation = [];
  List<SpeculationPriceModel> initialListSpeculationPrice = [];
  List<SpeculationPriceModel> listSpeculationPrice = [];

  SpeculationPriceUseCase speculationPriceUseCase =
      instance<SpeculationPriceUseCase>();
  Future getListSpeculation() async {
    (await speculationPriceUseCase.getListSpeculation()).fold((failure) {},
        (result) {
      setState(() {
        initialListSpeculation = result.data!;
      });
    });
  }

  Future getListSpeculationPrice() async {
    LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE)
        .getScreenWidget(context, getContent(), () {});
    (await speculationPriceUseCase.getSpeculationPriceHistory(widget.request))
        .fold((failure) {
      Navigator.of(context).pop();
    }, (result) {
      setState(() {
        listSpeculationPrice = result.listSpeculationPrice!;
        initialListSpeculationPrice = result.listSpeculationPrice!;
      });
      Navigator.of(context).pop();
    });
  }

  @override
  void initState() {
    getListSpeculation();
    getListSpeculationPrice();

    super.initState();
  }

  @override
  void dispose() {
    viewModel.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return getContent();
  }

  List<Widget> actions() => [
        IconButton(
          onPressed: () {
            showModalBottomSheet(
                context: context,
                builder: (context) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      showModalBottomSheetListTile(
                          titre: AppStrings.orderAsc,
                          icon: IconManager.orderAsc,
                          widget: const DemandeConveilView(),
                          context: context),
                      showModalBottomSheetListTile(
                          titre: AppStrings.orderDesc,
                          icon: IconManager.orderDesc,
                          widget: const DemandeConveilView(),
                          context: context),
                      showModalBottomSheetListTile(
                          titre: AppStrings.orderAlpha,
                          icon: IconManager.orderAlpha,
                          widget: const DemandeConveilView(),
                          context: context),
                    ],
                  );
                });
          },
          icon: Image.asset(
            ImageAssets.filter,
            width: 20,
          ),
        ),
        const SearchIconButton(),
      ];
  Widget getContent() {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButtonCustom(),
        title: Text(
          "Prix à ${widget.region.name}",
          style: getBoldTextStyle(
            color: ColorManager.black,
          ),
        ),
      ),
      body: listContent(),
    );
  }

  listContent() {
    return listSpeculationPrice.isNotEmpty
        ? Column(
            children: [
              const SizedBox(
                height: 5,
              ),
              Text(
                "Période du ${convertDate(listSpeculationPrice[0].priceInfo!.fromPeriod!)} à ${convertDate(listSpeculationPrice[0].priceInfo!.toPeriod!)}",
                style: getBoldTextStyle(color: ColorManager.black),
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 5,
              ),
              // Container(
              //   color: ColorManager.white,
              //   margin: const EdgeInsets.only(
              //     top: 10,
              //     left: 10,
              //     right: 10,
              //   ),
              //   child: selectContent(),
              // ),
              Expanded(
                child: ListView.builder(
                  itemCount: listSpeculationPrice.length,
                  itemBuilder: (BuildContext context, int index) {
                    return AnimationConfiguration.staggeredList(
                      position: index,
                      duration: const Duration(milliseconds: 100),
                      child: SlideAnimation(
                        verticalOffset: 50.0,
                        child: FadeInAnimation(
                          child: SpeculationWithPriceComponenent(
                            speculationPrice: listSpeculationPrice[index],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          )
        : EmptyComponenent(
            message: "Aucun prix trouvé à ${widget.region.name}",
          );
  }

  Widget selectContent() {
    return initialListSpeculation.isNotEmpty
        ? MultiSelectDialogField(
            buttonIcon: const Icon(Icons.keyboard_arrow_down),
            buttonText: const Text(
              AppStrings.choisirProduit,
            ),
            confirmText: const Text(AppStrings.ok),
            cancelText: const Text(AppStrings.cancel),
            title: const Text(AppStrings.regionQuestion),
            items: initialListSpeculation
                .map((e) => MultiSelectItem(e, e.name!))
                .toList(),
            listType: MultiSelectListType.LIST,
            onConfirm: (List<SpeculationModel> values) {
              List<int> ids = [];
              for (var element in values) {
                ids.add(element.speculationId!);
              }

              viewModel
                  .filterListSpeculation(ids, initialListSpeculation)
                  .then((value) {
                setState(() {
                  listSpeculationModel.data = value;
                });
              });
            },
            selectedColor: ColorManager.primary,
          )
        : Container();
  }
}
