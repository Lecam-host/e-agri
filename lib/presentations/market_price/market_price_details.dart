import 'package:flutter/material.dart';

import '../common/buttons/back_button.dart';
import '../ressources/values_manager.dart';
import 'components/details_price_card_componenent.dart';

class MarketPriceDetails extends StatefulWidget {
  const MarketPriceDetails({Key? key}) : super(key: key);

  @override
  State<MarketPriceDetails> createState() => _MarketPriceDetailsState();
}

class _MarketPriceDetailsState extends State<MarketPriceDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: const BackButtonCustom(),
          title: const Text("Details du prix"),
        ),
        body: const Column(
          children: [
            SizedBox(
              height: AppMargin.m10,
            ),
            DetailsPriceComponent(),
            DetailsPriceComponent(),
            DetailsPriceComponent(),
          ],
        ));
  }
}
