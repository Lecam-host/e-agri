import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/data/request/request_object.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/presentations/market_price/list_speculation_view.dart';
import 'package:eagri/presentations/market_price/request_history_price_view.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/icon_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'package:page_transition/page_transition.dart';

import '../../app/di.dart';
import '../../data/request/request.dart';
import '../../domain/model/location_model.dart';
import '../../domain/usecase/speculation_price_usecase.dart';
import '../common/buttons/back_button.dart';
import '../common/flushAlert_componenent.dart';
import '../common/showModalBottomSheetListTile.dart';
import '../ressources/strings_manager.dart';
import '../ressources/styles_manager.dart';
import 'comparaison_price_view.dart';
import 'components/market_price_official_view_listTile.dart';
import 'list_location_view.dart';
import 'market_price_view_model.dart';

class FirstView extends StatefulWidget {
  const FirstView({Key? key}) : super(key: key);

  @override
  State<FirstView> createState() => _FirstViewState();
}

class _FirstViewState extends State<FirstView> {
  MarketPriceViewModel viewModel = instance<MarketPriceViewModel>();
  GetListSpeculationPriceRequest request =
      GetListSpeculationPriceRequest(0, 10);

  List<RegionModel> listRegion = [];
  List<SpeculationModel> listSpeculation = [];
  SpeculationModel? speculationChoisiPourComparaison;
  List<RegionModel> listRegionChoisi = [];
  List<SpeculationModel> listSpeculationChoisi = [];
  List<LocationBodyId> localtionBys = [];
  List<Map<String, dynamic>> listlocaltionBysForComparaison = [];
  SpeculationPriceUseCase speculationPriceUseCase =
      instance<SpeculationPriceUseCase>();
  TabController? controller;

  Future getListSpeculation() async {
    (await speculationPriceUseCase.getListSpeculation()).fold((failure) {},
        (result) {
      if (mounted) {
        setState(() {
          listSpeculation = result.data!;
        });
      }
    });
  }

  Future getListRegion() async {
    (await speculationPriceUseCase.getListRegion()).fold((failure) {},
        (result) {
      setState(() {
        listRegion = result.data;
      });
    });
  }

  @override
  void initState() {
    getListSpeculation();
    getListRegion();
    // viewModel.getListRegion();
    // viewModel.getListSpeculation();

    // viewModel.getListSpeculationPrice(request);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: const Color.fromARGB(255, 246, 245, 245),
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(100.0),
          child: AppBar(
            actions: actions(),
            leading: const BackButtonCustom(),
            bottom: TabBar(
              indicatorColor: ColorManager.primary,
              tabs: [
                Tab(
                  child: Text(
                    "Produit",
                    style: getMeduimTextStyle(color: ColorManager.black),
                  ),
                ),
                Tab(
                  child: Text(
                    "Region",
                    style: getMeduimTextStyle(color: ColorManager.black),
                  ),
                ),
              ],
            ),
            title: Row(
              children: [
                Text(
                  'Prix offciel',
                  style: getMeduimTextStyle(color: ColorManager.black),
                ),
                const SizedBox(width: 10),
                Image.asset('assets/images/logo_ocpv.png', height: 60),
              ],
            ),
          ),
        ),
        body: const TabBarView(
          children: [
            ListSpeculationView(),
            ListLocationView(),
          ],
        ),
      ),
    );
  }

  comparePrice() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return StatefulBuilder(
              builder: (context, setInnerState) => Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      const Text("Comparer"),
                      const SizedBox(
                        height: 10,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            color: ColorManager.grey.withOpacity(0.1)),
                        child: MultiSelectDialogField(
                          buttonIcon: const Icon(Icons.keyboard_arrow_down),
                          buttonText: const Text(
                            AppStrings.regionQuestion,
                          ),
                          confirmText: const Text(AppStrings.ok),
                          cancelText: const Text(AppStrings.cancel),
                          title: const Text(AppStrings.regionQuestion),
                          items: listRegion
                              .map((e) => MultiSelectItem(e, e.name!))
                              .toList(),
                          listType: MultiSelectListType.LIST,
                          onConfirm: (List<RegionModel> regions) {
                            for (var element in regions) {
                              setState(() {
                                listlocaltionBysForComparaison.add(
                                    LocationBodyId(regionId: element.regionId)
                                        .toJson());
                              });
                            }
                            setInnerState(() {
                              listRegionChoisi = regions;
                            });
                            setState(() {
                              listRegionChoisi = regions;
                            });
                          },
                          selectedColor: ColorManager.primary,
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            color: ColorManager.grey.withOpacity(0.1)),
                        child: DropdownButton(
                          hint: const Text(AppStrings.speculationQuestion),
                          dropdownColor: ColorManager.white,
                          isExpanded: true,
                          alignment: AlignmentDirectional.centerEnd,
                          value: speculationChoisiPourComparaison,
                          icon: const Icon(Icons.keyboard_arrow_down),
                          items: listSpeculation.map((SpeculationModel items) {
                            return DropdownMenuItem(
                              value: items,
                              child: Text(items.name!),
                            );
                          }).toList(),
                          onChanged: (SpeculationModel? newValue) {
                            setInnerState(() {
                              speculationChoisiPourComparaison = newValue!;
                            });
                            setState(() {
                              speculationChoisiPourComparaison = newValue!;
                            });
                          },
                        ),
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          elevation: 0,
                          backgroundColor: ColorManager.primary,
                        ),
                        onPressed: () {
                          if (listRegionChoisi.isEmpty ||
                              speculationChoisiPourComparaison == null) {
                            showCustomFlushbar(
                                context,
                                "Veuillez remplir les champs",
                                ColorManager.blue,
                                FlushbarPosition.BOTTOM);
                          } else {
                            Navigator.push(
                              context,
                              PageTransition(
                                type: PageTransitionType.bottomToTop,
                                child: ComparaisonPriceView(
                                  regions: listRegionChoisi,
                                  compareRequest:
                                      CompareSpeculationPriceRequest(
                                          from: "2023-01-01",
                                          to: "2023-04-27",
                                          userType: true,
                                          speculationId:
                                              speculationChoisiPourComparaison!
                                                  .speculationId,
                                          locationByid:
                                              listlocaltionBysForComparaison),
                                  speculation: speculationChoisiPourComparaison,
                                ),
                                isIos: true,
                                duration: const Duration(milliseconds: 400),
                              ),
                            );
                          }
                        },
                        child: const Text(AppStrings.next),
                      ),
                    ],
                  ));
        });
  }

  List<Widget> actions() => [
        IconButton(
          onPressed: () {
            showModalBottomSheet(
                context: context,
                builder: (context) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      showModalBottomSheetListTile(
                          titre: AppStrings.historyPrice,
                          icon: IconManager.history,
                          widget: const RequestHistoryPriceView(),
                          context: context),
                      ListTile(
                        leading: Icon(
                          IconManager.compare,
                          color: ColorManager.black,
                        ),
                        title: Text(
                          AppStrings.comparePrice,
                          style: getSemiBoldTextStyle(
                            color: ColorManager.black,
                          ),
                        ),
                        trailing: Icon(IconManager.arrowRight,
                            color: ColorManager.black),
                        onTap: () {
                          comparePrice();
                        },
                      ),
                    ],
                  );
                });
          },
          icon: Icon(
            IconManager.moreMenu,
            color: ColorManager.black,
          ),
        ),
        //const SearchIconButton(),
      ];
  Widget getContent() {
    return StreamBuilder<ListSpeculationPriceModel>(
      stream: viewModel.outPutIsListSpeculationPrice,
      builder: (context, snapshot) {
        return listContent(snapshot.data);
      },
    );
  }

  listContent(ListSpeculationPriceModel? data) {
    return data != null
        ? Column(
            children: [
              Container(
                color: ColorManager.white,
                margin: const EdgeInsets.only(
                  top: 10,
                  left: 10,
                  right: 10,
                ),
                child: StreamBuilder<ListSpeculationModel>(
                  stream: viewModel.outPutListSpeculation,
                  builder: (context, snapshot) {
                    return selectContent(snapshot.data);
                  },
                ),
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: data.listSpeculationPrice!.length,
                  itemBuilder: (BuildContext context, int index) {
                    return AnimationConfiguration.staggeredList(
                      position: index,
                      duration: const Duration(milliseconds: 100),
                      child: SlideAnimation(
                        verticalOffset: 50.0,
                        child: FadeInAnimation(
                          child: MarketPriceOfficialViewListTileComponent(
                              data: data.listSpeculationPrice![index]),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          )
        : const Text("vide");
  }

  Widget selectContent(ListSpeculationModel? data) {
    return data != null
        ? MultiSelectDialogField(
            buttonIcon: const Icon(Icons.keyboard_arrow_down),
            buttonText: const Text(
              AppStrings.choisirProduit,
            ),
            confirmText: const Text(AppStrings.ok),
            cancelText: const Text(AppStrings.cancel),
            title: const Text(AppStrings.choisirProduit),
            items: data.data!.map((e) => MultiSelectItem(e, e.name!)).toList(),
            listType: MultiSelectListType.LIST,
            onConfirm: (List<SpeculationModel> values) {
              List<int> ids = [];
              for (var element in values) {
                ids.add(element.speculationId!);
              }
              // viewModel.filterListSpeculationPrice(ids);
            },
            selectedColor: ColorManager.primary,
          )
        : const SizedBox();
  }
}
