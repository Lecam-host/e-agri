import 'package:eagri/domain/model/location_model.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/icon_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

import '../../app/di.dart';
import '../../app/functions.dart';
import '../../data/request/request.dart';
import '../../domain/usecase/speculation_price_usecase.dart';
import '../common/buttons/back_button.dart';
import '../common/state/empty_component.dart';
import '../common/state_renderer/state_render_impl.dart';
import '../common/state_renderer/state_renderer.dart';
import '../ressources/strings_manager.dart';
import '../ressources/styles_manager.dart';
import '../ressources/values_manager.dart';
import 'market_price_view_model.dart';

class ComparaisonPriceView extends StatefulWidget {
  const ComparaisonPriceView(
      {Key? key, this.compareRequest, this.speculation, this.regions})
      : super(key: key);
  final SpeculationModel? speculation;
  final List<RegionModel>? regions;

  final CompareSpeculationPriceRequest? compareRequest;

  @override
  State<ComparaisonPriceView> createState() => _ComparaisonPriceViewState();
}

class _ComparaisonPriceViewState extends State<ComparaisonPriceView> {
  MarketPriceViewModel viewModel = instance<MarketPriceViewModel>();
  GetListSpeculationPriceRequest request =
      GetListSpeculationPriceRequest(0, 10);
  List<RegionModel> listRegion = [];
  List<SpeculationPriceModel> listSpeculationPrice = [];
  List<SpeculationPriceModel> initialListSpeculationPrice = [];

  SpeculationPriceUseCase speculationPriceUseCase =
      instance<SpeculationPriceUseCase>();

  Future compareSpeculationPrice() async {
    LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE)
        .getScreenWidget(context, getContent(), () {});
    (await speculationPriceUseCase
            .compareSpeculationPrice(widget.compareRequest!))
        .fold((failure) {
      Navigator.of(context).pop();
    }, (result) {
      Navigator.of(context).pop();

      setState(() {
        initialListSpeculationPrice = result.listSpeculationPrice!;
        listSpeculationPrice = result.listSpeculationPrice!;
      });
    });
  }

  start() {
    if (widget.compareRequest != null) compareSpeculationPrice();
  }

  @override
  void initState() {
    start();
    // getListSpeculationPrice();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 237, 237, 237),
      appBar: AppBar(
        leading: const BackButtonCustom(),
        title: const Text(AppStrings.comparaison),
      ),
      body: getContent(),
    );
  }

  Widget getContent() {
    return listContent();
  }

  listContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 10,
        ),
        if (listSpeculationPrice.isNotEmpty)
          Padding(
            padding: EdgeInsets.all(10),
            child: Text(
              "Période du ${convertDate(widget.compareRequest!.from!)} à ${convertDate(widget.compareRequest!.to!)}",
              style: getBoldTextStyle(color: ColorManager.black),
              textAlign: TextAlign.center,
            ),
          ),
        const SizedBox(
          height: 5,
        ),
        Container(
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: ColorManager.white,
          ),
          margin: const EdgeInsets.all(10),
          child: Row(
            children: [
              Text(
                'Speculation :',
                style: getRegularTextStyle(
                    color: ColorManager.black, fontSize: 14),
              ),
              Text(
                '${widget.speculation!.name}',
                style:
                    getBoldTextStyle(color: ColorManager.primary, fontSize: 14),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.only(
            top: 10,
            left: 10,
          ),
          child: Text(
            'Region',
            style: getBoldTextStyle(color: ColorManager.black, fontSize: 14),
          ),
        ),
        Container(
          width: double.infinity,
          margin: const EdgeInsets.only(
            left: 10,
            top: 5,
            right: 10,
          ),
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: ColorManager.white,
          ),
          child: Wrap(children: [
            for (RegionModel region in widget.regions ?? <RegionModel>[]) ...[
              Container(
                decoration: BoxDecoration(
                    color: ColorManager.primary.withOpacity(0.3),
                    borderRadius: BorderRadius.circular(5)),
                margin: const EdgeInsets.all(4),
                padding: const EdgeInsets.all(5),
                child: Text(
                  region.name!,
                ),
              ),
            ],
          ]),
        ),
        listSpeculationPrice.isNotEmpty
            ? Expanded(
                child: ListView.builder(
                  itemCount: listSpeculationPrice.length,
                  itemBuilder: (BuildContext context, int index) {
                    return AnimationConfiguration.staggeredList(
                      position: index,
                      duration: const Duration(milliseconds: 100),
                      child: SlideAnimation(
                        verticalOffset: 50.0,
                        child: FadeInAnimation(
                          child: priceWidget(listSpeculationPrice[index]),
                        ),
                      ),
                    );
                  },
                ),
              )
            : EmptyComponenent(
                message: "Aucun prix trouvé pour ${widget.speculation!.name}",
              )
      ],
    );
  }

  Widget priceWidget(SpeculationPriceModel data) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(color: ColorManager.white),
      margin: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Icon(
                IconManager.locationPin,
                color: ColorManager.primary,
              ),
              Expanded(
                child: Text(
                  data.locationInfo!.region!.name.toString(),
                  style: getMeduimTextStyle(
                    color: ColorManager.black,
                  ),
                  maxLines: 3,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: AppMargin.m10,
          ),
          Row(
            children: [
              Text(
                '${data.priceInfo!.prixGros} ${AppStrings.moneyUnit}',
                style: getSemiBoldTextStyle(color: ColorManager.blue),
              ),
              const Spacer(),
              Text(
                '${data.priceInfo!.prixDetails} ${AppStrings.moneyUnit}',
                style: getSemiBoldTextStyle(
                  color: ColorManager.blue,
                ),
              ),
            ],
          ),
          Row(
            children: [
              Text(
                AppStrings.gros,
                style: getRegularTextStyle(color: ColorManager.grey),
              ),
              const Spacer(),
              Text(
                AppStrings.details,
                style: getRegularTextStyle(color: ColorManager.grey),
              )
            ],
          )
        ],
      ),
    );
  }
}
