import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:flutter/material.dart';
import 'package:sliver_tools/sliver_tools.dart';

import '../../app/di.dart';
import '../../data/request/request.dart';
import '../../domain/model/model.dart';
import '../../domain/usecase/speculation_price_usecase.dart';
import '../common/buttons/back_button.dart';
import '../common/state_renderer/state_render_impl.dart';
import '../common/state_renderer/state_renderer.dart';
import '../ressources/color_manager.dart';
import '../ressources/strings_manager.dart';
import '../ressources/styles_manager.dart';
import 'components/history_card_componenent.dart';

class HistoryPriceView extends StatefulWidget {
  const HistoryPriceView({
    Key? key,
    required this.historyreRequest,
    required this.speculation,
    required this.regionName,
  }) : super(key: key);
  final SpeculationPriceRequest historyreRequest;
  final SpeculationModel speculation;
  final String regionName;

  @override
  State<HistoryPriceView> createState() => _HistoryPriceViewState();
}

class _HistoryPriceViewState extends State<HistoryPriceView> {
  late ScrollController _scrollController;
  Color textColor = Colors.white;
  bool get _isSliverAppBarExpanded {
    return _scrollController.hasClients &&
        _scrollController.offset > (200 - kToolbarHeight);
  }

  List<SpeculationPriceModel> listSpeculationPrice = [];
  List<SpeculationPriceModel> initialListSpeculationPrice = [];

  SpeculationPriceUseCase speculationPriceUseCase =
      instance<SpeculationPriceUseCase>();
  Future getListSpeculationPrice() async {
    LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE)
        .getScreenWidget(context, Container(), () {});
    (await speculationPriceUseCase
            .getSpeculationPriceHistory(widget.historyreRequest))
        .fold((failure) {
      Navigator.of(context).pop();
    }, (result) {
      Navigator.of(context).pop();

      setState(() {
        listSpeculationPrice = result.listSpeculationPrice!;
      });
    });
  }

  @override
  void initState() {
    _scrollController = ScrollController()
      ..addListener(() {
        setState(() {
          textColor =
              _isSliverAppBarExpanded ? ColorManager.black : ColorManager.white;
        });
      });
    getListSpeculationPrice();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 237, 237, 237),
      body: CustomScrollView(
        controller: _scrollController,
        slivers: <Widget>[
          SliverAppBar(
            stretchTriggerOffset: 50,
            pinned: true,
            snap: true,
            floating: true,
            expandedHeight: 200.0,

            leading: BackButtonCustom(
              color: textColor,
            ),
            flexibleSpace: FlexibleSpaceBar(
              titlePadding: const EdgeInsets.all(10),
              background: CachedNetworkImage(
                imageUrl: widget.speculation.imageUrl!,
                imageBuilder: (context, imageProvider) => Container(
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(5),
                      topRight: Radius.circular(5),
                    ),
                    image: DecorationImage(
                      colorFilter: ColorFilter.mode(
                          Colors.black.withOpacity(0.5), BlendMode.darken),
                      scale: 2,
                      image: imageProvider,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                placeholder: (context, url) => Container(
                  color: ColorManager.grey1,
                  height: 100,
                  width: MediaQuery.of(context).size.width,
                ),
                errorWidget: (context, url, error) => const Icon(Icons.error),
              ),
              collapseMode: CollapseMode.parallax,
              centerTitle: true,
              title: Text(
                "${AppStrings.history} de ${widget.speculation.name} à ${widget.regionName}",
                style: getRegularTextStyle(color: textColor),
              ),
            ),
            //actions: actions(),
          ),
          MultiSliver(
            // defaults to false
            pushPinnedChildren: true,
            children: <Widget>[
              const SizedBox(
                height: 10,
              ),
              if(listSpeculationPrice.isEmpty)
              const SliverPinnedHeader(
                
                child: EmptyComponenent(message: "Aucun prix trouvé",),
              ),
                if(listSpeculationPrice.isNotEmpty)
              SliverList(
                delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                  return HistoryCardComponent(
                      speculationPrice: listSpeculationPrice[index]);
                },
                    // 40 list items
                    childCount: listSpeculationPrice.length),
              ),
            ],
          )

          //   getContent()
          // SizedBox()
        ],
      ),
    );
  }
}
