import 'dart:async';

import 'package:eagri/data/request/request.dart';
import 'package:eagri/domain/model/location_model.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:rxdart/rxdart.dart';

import '../../domain/usecase/speculation_price_usecase.dart';
import '../base/base_view_model.dart';
import '../common/state_renderer/state_render_impl.dart';
import '../common/state_renderer/state_renderer.dart';

class MarketPriceViewModel extends BaseViewModel {
  final listSpeculationPrice = BehaviorSubject<ListSpeculationPriceModel>();
  final biglistSpeculationPrice = BehaviorSubject<ListSpeculationPriceModel>();

  final listFilterSpeculationPrice =
      BehaviorSubject<ListSpeculationPriceModel>();

  final listRegion = BehaviorSubject<ListRegionModel>();
  final listSpeculation = BehaviorSubject<ListSpeculationModel>();
  final listSelectSpeculation = BehaviorSubject<ListSpeculationModel>();

  SpeculationPriceUseCase speculationPriceUseCase;
  final StreamController _isAllInputValidStreamController =
      StreamController<void>.broadcast();
  MarketPriceViewModel(this.speculationPriceUseCase);
  @override
  void start() {
    inputState.add(ContentState());
  }

  @override
  dispose() {
    listSpeculation.close();
    listSelectSpeculation.close();
    listFilterSpeculationPrice.close();
    listSpeculationPrice.close();
    _isAllInputValidStreamController.close();
    biglistSpeculationPrice.close();
    listRegion.close();
  }

  Sink get inputListSpeculation => listSpeculation.sink;
  Sink get inputlistSelectSpeculation => listSelectSpeculation.sink;
  Sink get inputlistFilterSpeculationPrice => listFilterSpeculationPrice.sink;
  Sink get inputlistSpeculationPrice => listSpeculationPrice.sink;
  Sink get inputbiglistSpeculationPrice => biglistSpeculationPrice.sink;
  Sink get inputlistRegion => listRegion.sink;

  addSpeculationPrice(AddSpeculationPriceRequest request) async {
    inputState.add(
        LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE));
    (await speculationPriceUseCase.addSpeculationPrice(request)).fold(
        (failure) => {
              inputState.add(ErrorState(
                  StateRendererType.FULL_SCREEN_ERROR_STATE, failure.message))
            }, (data) {
      // listSpeculationPrice.add(data);
      inputState.add(SuccessState(data.statusMessage!));
    });
  }

  getListSpeculationPrice(GetListSpeculationPriceRequest request) async {
    inputState.add(
        LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE));
    (await speculationPriceUseCase.getListSpeculationPrice(request)).fold(
        (failure) => {
              // left -> failure
              inputState.add(ErrorState(
                  StateRendererType.FULL_SCREEN_ERROR_STATE, failure.message))
            }, (data) {
      if (!biglistSpeculationPrice.isClosed) biglistSpeculationPrice.add(data);
      if (!listSpeculationPrice.isClosed) listSpeculationPrice.add(data);
      inputState.add(ContentState());
    });
  }

  getListSpeculationPriceAllRegion(SpeculationPriceRequest request) async {
    inputState.add(
        LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE));
    (await speculationPriceUseCase.getSpeculationPriceHistory(request)).fold(
        (failure) => {
              // left -> failure
              inputState.add(ErrorState(
                  StateRendererType.FULL_SCREEN_ERROR_STATE, failure.message))
            }, (data) {
      biglistSpeculationPrice.add(data);
      listSpeculationPrice.add(data);
      inputState.add(ContentState());
    });
  }

  Future<List<SpeculationPriceModel>> filterListSpeculationPriceByLocation(
      List<int> ids, List<SpeculationPriceModel> input) async {
    List<SpeculationPriceModel> result = [];
    if (ids.isNotEmpty) {
      for (var element in input) {
        if (ids.contains(element.locationInfo!.region!.regionId)) {
          result.add(element);
        }
      }
    } else {
      result = input;
    }
    return result;
  }

  Future<List<SpeculationModel>> filterListSpeculation(
      List<int> ids, List<SpeculationModel> input) async {
    List<SpeculationModel> result = [];
    if (ids.isNotEmpty) {
      for (var element in input) {
        if (ids.contains(element.speculationId)) {
          result.add(element);
        }
      }
    } else {
      result = input;
    }

    return result;
  }

  Future<List<RegionModel>> filterListRegion(
      List<int> ids, List<RegionModel> input) async {
    List<RegionModel> result = [];
    if (ids.isNotEmpty) {
      for (var element in input) {
        if (ids.contains(element.regionId)) {
          result.add(element);
        }
      }
    } else {
      result = input;
    }

    return result;
  }

  getListRegion() async {
    inputState.add(
        LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE));
    (await speculationPriceUseCase.getListRegion()).fold(
        (failure) => {
              // left -> failure
              inputState.add(ErrorState(
                  StateRendererType.FULL_SCREEN_ERROR_STATE, failure.message))
            }, (data) {
      listRegion.add(data);
      inputState.add(ContentState());
    });
  }

  getListSpeculation() async {
    inputState.add(
        LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE));
    (await speculationPriceUseCase.getListSpeculation()).fold(
        (failure) => {
              // left -> failure
              inputState.add(ErrorState(
                  StateRendererType.FULL_SCREEN_ERROR_STATE, failure.message))
            }, (data) {
      listSelectSpeculation.add(data);
      listSpeculation.add(data);
      inputState.add(ContentState());
    });
  }

  getListSpeculationPriceEnterByUser(
      GetListSpeculationPriceRequest request) async {
    inputState.add(
        LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE));
    (await speculationPriceUseCase.getListSpeculationPriceEnterByUser(request))
        .fold(
            (failure) => {
                  // left -> failure
                  inputState.add(ErrorState(
                      StateRendererType.FULL_SCREEN_ERROR_STATE,
                      failure.message))
                }, (data) {
      listSpeculationPrice.add(data);
      inputState.add(ContentState());
    });
  }

  Stream<bool> get outputIsAllInputValid =>
      _isAllInputValidStreamController.stream.map((isAllInputValid) => true);
  Stream<ListSpeculationPriceModel> get outPutIsListSpeculationPrice =>
      listSpeculationPrice.stream.map((data) => data);
  Stream<ListRegionModel> get outPutListRegion =>
      listRegion.stream.map((data) => data);
  Stream<ListSpeculationModel> get outPutListSelectSpeculation =>
      listSelectSpeculation.stream.map((data) => data);

  Stream<ListSpeculationModel> get outPutListSpeculation =>
      listSpeculation.stream.map((data) => data);
}
