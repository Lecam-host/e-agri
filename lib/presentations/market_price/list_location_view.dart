import 'package:eagri/domain/model/location_model.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/market_price/components/region_component_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

import '../../app/di.dart';
import '../../data/request/request.dart';
import '../../domain/usecase/speculation_price_usecase.dart';
import '../common/search_field.dart';
import '../common/state_renderer/state_render_impl.dart';
import '../common/state_renderer/state_renderer.dart';
import 'market_price_view_model.dart';

class ListLocationView extends StatefulWidget {
  const ListLocationView({Key? key}) : super(key: key);

  @override
  State<ListLocationView> createState() => _ListLocationViewState();
}

class _ListLocationViewState extends State<ListLocationView> {
  MarketPriceViewModel viewModel = instance<MarketPriceViewModel>();
  GetListSpeculationPriceRequest request =
      GetListSpeculationPriceRequest(0, 10);
  List<RegionModel> initialListRegion = [];
  List<RegionModel> listRegion = [];
  bool isLoadPrice = true;
  SpeculationPriceUseCase speculationPriceUseCase =
      instance<SpeculationPriceUseCase>();
  Future getListRegion() async {
    LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE)
        .getScreenWidget(context, getContent(), () {});
    (await speculationPriceUseCase.getListRegion()).fold((failure) {
      Navigator.of(context).pop();
      setState(() {
        isLoadPrice = false;
      });
    }, (result) {
      setState(() {
        initialListRegion = result.data;
        listRegion = result.data;
        isLoadPrice = false;
      });
      Navigator.of(context).pop();
    });
  }

  @override
  void initState() {
    getListRegion();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return getContent();
  }

  Widget getContent() {
    return listContent();
  }

  listContent() {
    return isLoadPrice == false
        ? Column(
            children: [
              selectContent(),
              listRegion.isNotEmpty
                  ? Expanded(
                      child: ListView.builder(
                        itemCount: listRegion.length,
                        itemBuilder: (BuildContext context, int index) {
                          return AnimationConfiguration.staggeredList(
                            position: index,
                            duration: const Duration(milliseconds: 100),
                            child: SlideAnimation(
                              verticalOffset: 50.0,
                              child: FadeInAnimation(
                                child: RegionComponenent(
                                    region: listRegion[index]),
                              ),
                            ),
                          );
                        },
                      ),
                    )
                  : const EmptyComponenent(
                      message: "Liste vide",
                    ),
            ],
          )
        : const SizedBox();
  }

  Widget selectContent() {
    return initialListRegion.isNotEmpty
        ? Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
            ),
            child: SearchField(
              onChanged: (value) {
                List<RegionModel> result = [];
                if (value != "") {
                  for (var element in initialListRegion) {
                    if (element.name!
                        .toLowerCase()
                        .contains(value.toLowerCase())) {
                      result.add(element);
                    }
                  }
                } else {
                  result = initialListRegion;
                }

                setState(() {
                  listRegion = result;
                });
              },
              hintText: "Rechercher un produit",
            ))
        : Container();
  }
}
