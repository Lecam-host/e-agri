import 'package:eagri/data/request/request.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:page_transition/page_transition.dart';

import '../../app/di.dart';
import '../../data/request/request_object.dart';
import '../../domain/model/location_model.dart';
import '../../domain/usecase/speculation_price_usecase.dart';
import '../common/state/succes_page_view.dart';
import '../common/state_renderer/state_render_impl.dart';
import '../common/state_renderer/state_renderer.dart';
import '../ressources/color_manager.dart';
import '../ressources/strings_manager.dart';
import '../ressources/styles_manager.dart';
import '../ressources/values_manager.dart';
import 'market_price_view_model.dart';

class AddSpeculationPriceView extends StatefulWidget {
  const AddSpeculationPriceView({Key? key}) : super(key: key);

  @override
  State<AddSpeculationPriceView> createState() =>
      _AddSpeculationPriceViewState();
}

class _AddSpeculationPriceViewState extends State<AddSpeculationPriceView> {
  MarketPriceViewModel viewModel = instance<MarketPriceViewModel>();
  RegionModel? region;
  DepartementModel? departement;
  SousPrefectureModel? sousPrefecture;
  LocaliteModel? localite;
  String? unite;
  SpeculationModel? speculation;

  TextEditingController priceDetail = TextEditingController();
  TextEditingController priceGros = TextEditingController();
  List<String> listUnit = [];
  List<DepartementModel> listDepartement = [];
  List<SousPrefectureModel> listSp = [];
  List<LocaliteModel> listLocalite = [];

  List<String> listunit = [];
  List<RegionModel> listRegion = [];
  List<SpeculationModel> listSpeculation = [];

  SpeculationPriceUseCase speculationPriceUseCase =
      instance<SpeculationPriceUseCase>();
  getListSpeculationUnit(int idSpeculation) {
    for (var speculation in listSpeculation) {
      if (speculation.speculationId == idSpeculation) {
        setState(() {
          listUnit = speculation.listUnit!;
        });
      }
    }
  }

  Future getListRegion() async {
    LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE)
        .getScreenWidget(context, Container(), () {});
    (await speculationPriceUseCase.getListRegion()).fold((failure) {
      Navigator.of(context).pop();
    }, (result) {
      setState(() {
        listRegion = result.data;
      });
      Navigator.of(context).pop();
    });
  }

  Future getListDepartement(int idregion) async {
    LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE)
        .getScreenWidget(context, Container(), () {});
    (await speculationPriceUseCase.getListDepartementOfRegion(idregion)).fold(
        (failure) {
      Navigator.of(context).pop();
    }, (result) {
      Navigator.of(context).pop();
      setState(() {
        listDepartement = result.listDepartement!;
      });
    });
  }

  Future getListSousPrefecture(int idDepartement) async {
    LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE)
        .getScreenWidget(context, Container(), () {});
    (await speculationPriceUseCase.getListSpOfDepartment(idDepartement)).fold(
        (failure) {
      Navigator.of(context).pop();
    }, (result) {
      Navigator.of(context).pop();
      setState(() {
        listSp = result.listSousPrefecture!;
      });
    });
  }

  Future getListLocalite(int idSp) async {
    LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE)
        .getScreenWidget(context, Container(), () {});
    (await speculationPriceUseCase.getListLocaliteOfsp(idSp)).fold((failure) {
      Navigator.of(context).pop();
    }, (result) {
      Navigator.of(context).pop();
      setState(() {
        listLocalite = result.listLocalite!;
      });
    });
  }

  Future getListSpeculation() async {
    (await speculationPriceUseCase.getListSpeculation()).fold((failure) {},
        (result) {
      setState(() {
        listSpeculation = result.data!;
      });
    });
  }

  // @JsonKey(name: "value")
  // int? value;
  // @JsonKey(name: "regionId")
  // int? regionId;

  // @JsonKey(name: "departementId")
  // int? departementId;
  // @JsonKey(name: "sousPrefectureId")
  // int? sousPrefectureId;
  // @JsonKey(name: "localiteId")
  // int? localiteId;
  // PriceSpeculationData priceSpeculationData =
  //     PriceSpeculationData(1000, 1, 1, 1, 1);

  AddSpeculationPriceRequest requestData = AddSpeculationPriceRequest();
  @override
  void initState() {
    getListRegion();
    getListSpeculation();
    super.initState();
  }

  int _currentStep = 0;

  int lieuStepNumber = 0;
  int produitStepNumber = 1;
  int verificationStepNumber = 2;
  StepperType stepperType = StepperType.horizontal;
  switchStepsType() {
    setState(() => stepperType == StepperType.vertical
        ? stepperType = StepperType.horizontal
        : stepperType = StepperType.vertical);
  }

  tapped(int step) {
    setState(() => _currentStep = step);
  }

  continued() {
    _currentStep < getListStep().length - 1 ? validForm() : null;
  }

  cancel() {
    _currentStep > 0 ? setState(() => _currentStep -= 1) : null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButtonCustom(),
        title: const Text(AppStrings.addPrice),
      ),
      body: Container(
        margin: const EdgeInsets.all(10),
        child: Column(children: [
          Expanded(
              child: Stepper(
            type: stepperType,
            controlsBuilder: (context, controlsDetails) {
              final isLastStep = _currentStep == getListStep().length - 1;
              return Row(
                children: [
                  Expanded(
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        elevation: 0,
                        backgroundColor: Colors.grey,
                      ),
                      onPressed: controlsDetails.onStepCancel,
                      child: const Text(AppStrings.previous),
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  if (isLastStep)
                    //   StreamBuilder<bool>(
                    //     stream: viewModel.outputIsAllInputValid,
                    //     builder: (context, snapshot) {
                    //       return SizedBox(
                    //         width: double.infinity,
                    //         height: AppSize.s40,
                    //         child: ElevatedButton(
                    //             onPressed: (snapshot.data ?? false)
                    //                 ? () =>
                    //                     viewModel.addSpeculationPrice(requestData)
                    //                 : null,
                    //             child: const Text(AppStrings.submit)),
                    //       );
                    //     },
                    //   ),
                    Expanded(
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          elevation: 0,
                          backgroundColor: ColorManager.primary,
                          textStyle: getRegularTextStyle(
                            color: ColorManager.white,
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(
                              AppSize.s5,
                            ),
                          ),
                        ),
                        child: const Text(AppStrings.submit),
                        onPressed: () {
                          LoadingState(
                                  stateRendererType:
                                      StateRendererType.POPUP_LOADING_STATE)
                              .getScreenWidget(context, Container(), () {});
                          final DateFormat formatter = DateFormat('yyyy-MM-dd');

                          requestData.official = false;
                          requestData.userId = DateTime.now().second;
                          requestData.from = formatter.format(DateTime.now());
                          requestData.to = formatter
                              .format(DateTime.now().add(const Duration(
                            days: 30,
                          )));
                          if (speculation != null) {
                            requestData.speculationId =
                                speculation!.speculationId;
                          }
                          PriceSpeculationData priceSpeculationData =
                              PriceSpeculationData(
                            prixGros: int.parse(priceGros.text),
                            prixDetail: int.parse(priceDetail.text),
                            regionId: region!.regionId,

                            // departementId: departement!.departementId,
                            // sousPrefectureId:
                            //   sousPrefecture!.sousPrefectureId,
                            // localiteId: localite!.localiteId,
                          );
                          requestData.unit = unite;
                          requestData.source = "Kedja";
                          requestData.data = [priceSpeculationData.toJson()];

                          speculationPriceUseCase
                              .addSpeculationPrice(requestData)
                              .then((value) {
                            value.fold((failure) {
                              Navigator.pop(context);
                              ScaffoldMessenger.of(context)
                                ..removeCurrentSnackBar()
                                ..showSnackBar(
                                  const SnackBar(
                                    content: Text("Echec"),
                                  ),
                                );
                            }, (response) {
                              Navigator.pop(context);
                              Navigator.pushReplacement(
                                context,
                                PageTransition(
                                  type: PageTransitionType.bottomToTop,
                                  child: const SuccessPageView(
                                    message: "Prix ajouté avec succès",
                                  ),
                                  isIos: true,
                                  duration: const Duration(milliseconds: 400),
                                ),
                              );
                            });
                          });

                          //viewModel.addSpeculationPrice(requestData);
                        },
                      ),
                    ),
                  if (!isLastStep)
                    Expanded(
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          elevation: 0,
                          backgroundColor: ColorManager.primary,
                        ),
                        onPressed: controlsDetails.onStepContinue,
                        child: const Text(AppStrings.next),
                      ),
                    ),
                ],
              );
            },
            physics: const ScrollPhysics(),
            currentStep: _currentStep,
            onStepTapped: (step) => tapped(step),
            onStepContinue: continued,
            onStepCancel: cancel,
            steps: getListStep(),
          )),
        ]),
      ),
    );
  }

  List<Step> getListStep() => [stepLieu(), stepSpeculationInfo(), stepResume()];
  Step stepLieu() => Step(
        title: const Text('Lieu'),
        content: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              AppStrings.indiqueLieu,
              style: getRegularTextStyle(
                color: ColorManager.black,
                fontSize: 16,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              color: ColorManager.white,
              child: DropdownButton<RegionModel>(
                value: region,
                dropdownColor: ColorManager.white,
                isExpanded: true,
                hint: const Text(AppStrings.regionQuestion),
                alignment: AlignmentDirectional.center,
                icon: const Icon(Icons.keyboard_arrow_down),
                items: listRegion.map((RegionModel items) {
                  return DropdownMenuItem(
                    value: items,
                    child: Text(items.name!),
                  );
                }).toList(),
                onChanged: (RegionModel? newValue) {
                  getListDepartement(newValue!.regionId!);
                  setState(() {
                    region = newValue;
                  });
                },
              ),
            ),
            const SizedBox(
              height: AppMargin.m20,
            ),
            if (region != null)
              Container(
                color: ColorManager.white,
                child: DropdownButton(
                  value: departement,
                  dropdownColor: ColorManager.white,
                  isExpanded: true,
                  hint: const Text(AppStrings.departementQuestion),
                  alignment: AlignmentDirectional.center,
                  icon: const Icon(Icons.keyboard_arrow_down),
                  items: listDepartement.map((DepartementModel items) {
                    return DropdownMenuItem(
                      value: items,
                      child: Text(items.name!),
                    );
                  }).toList(),
                  onChanged: (DepartementModel? newValue) {
                    getListSousPrefecture(newValue!.departementId!);
                    setState(() {
                      departement = newValue;
                    });
                  },
                ),
              ),
            const SizedBox(
              height: AppMargin.m20,
            ),
            if (departement != null)
              Container(
                color: ColorManager.white,
                child: DropdownButton(
                  value: sousPrefecture,
                  dropdownColor: ColorManager.white,
                  isExpanded: true,
                  hint: const Text(AppStrings.sousPrefectureQuestion),
                  alignment: AlignmentDirectional.center,
                  icon: const Icon(Icons.keyboard_arrow_down),
                  items: listSp.map((SousPrefectureModel items) {
                    return DropdownMenuItem(
                      value: items,
                      child: Text(items.name!),
                    );
                  }).toList(),
                  onChanged: (SousPrefectureModel? newValue) {
                    getListLocalite(newValue!.sousPrefectureId!);
                    setState(() {
                      sousPrefecture = newValue;
                    });
                  },
                ),
              ),
            const SizedBox(
              height: AppMargin.m20,
            ),
            if (sousPrefecture != null)
              Container(
                color: ColorManager.white,
                child: DropdownButton(
                  value: localite,
                  dropdownColor: ColorManager.white,
                  isExpanded: true,
                  hint: const Text(AppStrings.localiteQuestion),
                  alignment: AlignmentDirectional.center,
                  icon: const Icon(Icons.keyboard_arrow_down),
                  items: listLocalite.map((LocaliteModel items) {
                    return DropdownMenuItem(
                      value: items,
                      child: Text(items.name!),
                    );
                  }).toList(),
                  onChanged: (LocaliteModel? newValue) {
                    setState(() {
                      localite = newValue!;
                    });
                  },
                ),
              ),
            const SizedBox(
              height: AppMargin.m20,
            ),
          ],
        ),
        isActive: _currentStep >= lieuStepNumber,
        state: _currentStep >= lieuStepNumber
            ? StepState.complete
            : StepState.disabled,
      );

  Step stepSpeculationInfo() => Step(
        title: const Text('Produit'),
        content:
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          const Text('Produit *'),
          Container(
            color: ColorManager.white,
            child: DropdownButton(
              value: speculation,
              dropdownColor: ColorManager.white,
              isExpanded: true,
              hint: const Text(AppStrings.speculationQuestion),
              alignment: AlignmentDirectional.center,
              icon: const Icon(Icons.keyboard_arrow_down),
              items: listSpeculation.map((SpeculationModel items) {
                return DropdownMenuItem(
                  value: items,
                  child: Text(items.name!),
                );
              }).toList(),
              onChanged: (SpeculationModel? newValue) {
                getListSpeculationUnit(newValue!.speculationId!);
                setState(() {
                  speculation = newValue;
                });
              },
            ),
          ),
          const SizedBox(
            height: AppMargin.m20,
          ),
          const Text('Unité du produit *'),
          Container(
            color: ColorManager.white,
            child: DropdownButton(
              value: unite,
              dropdownColor: ColorManager.white,
              isExpanded: true,
              hint: const Text(AppStrings.uniteSpeculationQuestion),
              alignment: AlignmentDirectional.center,
              icon: const Icon(Icons.keyboard_arrow_down),
              items: listUnit.map((String items) {
                return DropdownMenuItem(
                  value: items,
                  child: Text(items),
                );
              }).toList(),
              onChanged: (String? newValue) {
                setState(() {
                  unite = newValue!;
                });
              },
            ),
          ),
          const SizedBox(
            height: AppMargin.m20,
          ),
          const Text('Prix en details *'),
          TextFormField(
            keyboardType: TextInputType.number,
            controller: priceDetail,
            decoration: InputDecoration(
              filled: true,
              fillColor: ColorManager.white,
              hintText: AppStrings.priceDetail,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          const Text('Prix en gros *'),
          TextFormField(
            keyboardType: TextInputType.number,
            controller: priceGros,
            decoration: InputDecoration(
              filled: true,
              fillColor: ColorManager.white,
              hintText: AppStrings.priceGros,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
        ]),
        isActive: _currentStep >= produitStepNumber,
        state: _currentStep >= produitStepNumber
            ? StepState.complete
            : StepState.disabled,
      );
  Step stepResume() => Step(
        title: const Text('Vérification'),
        content: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              AppStrings.infoSpeculation,
              style: getBoldTextStyle(color: ColorManager.black, fontSize: 20),
            ),
            const SizedBox(
              height: 10,
            ),
            if (speculation != null)
              resumeLine(AppStrings.speculation, speculation!.name!),
            if (priceDetail.text != "")
              resumeLine(AppStrings.priceDetail, priceDetail.text),
            if (priceGros.text != "")
              resumeLine(AppStrings.priceGros, priceGros.text),
            if (unite != null) resumeLine(AppStrings.unite, unite!),
            const SizedBox(
              height: 20,
            ),
            Text(
              AppStrings.infoLieu,
              style: getBoldTextStyle(color: ColorManager.black, fontSize: 20),
            ),
            const SizedBox(
              height: 10,
            ),
            if (region != null) resumeLine(AppStrings.region, region!.name!),
            if (departement != null)
              resumeLine(AppStrings.departement, departement!.name!),
            if (sousPrefecture != null)
              resumeLine(AppStrings.sousPrefecture, sousPrefecture!.name!),
            if (localite != null)
              resumeLine(AppStrings.localite, localite!.name!),
          ],
        ),
        isActive: _currentStep >= verificationStepNumber,
        state: _currentStep >= verificationStepNumber
            ? StepState.complete
            : StepState.disabled,
      );

  Container resumeLine(String titre, String data) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(10),
      color: ColorManager.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            data,
            style: getBoldTextStyle(
              color: ColorManager.black,
              fontSize: 15,
            ),
          ),
          Text(
            titre,
            style: getBoldTextStyle(
              color: ColorManager.grey,
            ),
          ),
        ],
      ),
    );
  }

  validForm() {
    if (_currentStep == lieuStepNumber && region == null) {
      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(
          const SnackBar(
            content: Text("Choisir la région"),
          ),
        );
    } else if (_currentStep == produitStepNumber &&
        (speculation == null ||
            unite == null ||
            priceDetail.text == "" ||
            priceGros.text == "")) {
      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(
          const SnackBar(
            content: Text("Remplir tout les champs"),
          ),
        );
    } else {
      setState(() {
        _currentStep++;
      });
    }
  }
}
