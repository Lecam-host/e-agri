import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/data/request/request.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:flutter/material.dart';

import '../../../app/di.dart';
import '../../../domain/usecase/speculation_price_usecase.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/font_manager.dart';
import '../../ressources/strings_manager.dart';
import '../../ressources/styles_manager.dart';

class SpeculationComponenent extends StatefulWidget {
  const SpeculationComponenent({
    Key? key,
    required this.speculation,
    required this.request,
    this.minAndMaxObject,
  }) : super(key: key);
  final SpeculationModel speculation;
  final MinAndMaxSpeculationPriceModel? minAndMaxObject;

  final SpeculationPriceRequest request;

  @override
  State<SpeculationComponenent> createState() => _SpeculationComponenentState();
}

class _SpeculationComponenentState extends State<SpeculationComponenent> {
  SpeculationPriceUseCase speculationPriceUseCase =
      instance<SpeculationPriceUseCase>();
  MinAndMaxSpeculationPriceModel? speculationPricWithMinMax;
  bool isLoadMinMax = true;
  getListMinMaxPrice() async {
    await speculationPriceUseCase
        .getMinMaxPrice(
          GetMinAndMaxSpeculationnPriceRequest(
              speculationId: widget.speculation.speculationId!, official: true),
        )
        .then((value) => value.fold((l) {
              setState(() {
                isLoadMinMax = false;
              });
            }, (response) {
              if (mounted) {
                setState(() {
                  isLoadMinMax = false;
                  speculationPricWithMinMax = response;
                });
              }
            }));
    // ignore: use_build_context_synchronously
  }

  @override
  void initState() {
    getListMinMaxPrice();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return speculationPricWithMinMax!=null? Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: ColorManager.white,
        boxShadow: [shadow()],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 100,
            width: MediaQuery.of(context).size.width,
            child: CachedNetworkImage(
              imageUrl: widget.speculation.imageUrl!,
              imageBuilder: (context, imageProvider) => Container(
                decoration: BoxDecoration(
                  color: ColorManager.grey1,
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(5),
                    topRight: Radius.circular(5),
                  ),
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              placeholder: (context, url) => Container(
                color: ColorManager.grey1,
                height: 100,
                width: MediaQuery.of(context).size.width,
              ),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: SizedBox(
              height: 40,
              child: Text(
                widget.speculation.name.toString(),
                style: getMeduimTextStyle(
                    color: ColorManager.black, fontSize: FontSize.s12),
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          if (speculationPricWithMinMax != null) ...[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: Text(
                speculationPricWithMinMax!.minPriceRegion.name!,
                style: getSemiBoldTextStyle(
                    color: ColorManager.primary, fontSize: 8),
                maxLines: 1,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: Text(
                "${speculationPricWithMinMax!.minPrice.toString()} ${AppStrings.moneyUnit}",
                style: getSemiBoldTextStyle(
                    color: ColorManager.primary, fontSize: 8),
                maxLines: 1,
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: Text(
                speculationPricWithMinMax!.maxPriceRegion.name!,
                style:
                    getSemiBoldTextStyle(color: ColorManager.red, fontSize: 8),
                maxLines: 1,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: Text(
                "${speculationPricWithMinMax!.maxPrice.toString()} ${AppStrings.moneyUnit}",
                style:
                    getSemiBoldTextStyle(color: ColorManager.red, fontSize: 8),
                maxLines: 1,
              ),
            ),
          ],
          if (speculationPricWithMinMax == null && isLoadMinMax == false)
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: Container(
                color: ColorManager.blue.withOpacity(0.2),
                width: double.infinity,
                child: const Text("Prix non disponible"),
              ),
            )
        ],
      ),
    ):const SizedBox();
  }
}
