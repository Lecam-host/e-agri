// ignore_for_file: file_names

import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/presentations/ressources/icon_manager.dart';
import 'package:flutter/material.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/font_manager.dart';
import '../../ressources/routes_manager.dart';
import '../../ressources/strings_manager.dart';
import '../../ressources/styles_manager.dart';

class MarketPriceOfficialViewListTileComponent extends StatelessWidget {
  const MarketPriceOfficialViewListTileComponent({Key? key, required this.data})
      : super(key: key);
  final SpeculationPriceModel data;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: ColorManager.white,
        boxShadow: [shadow()],
      ),
      margin: const EdgeInsets.only(
        top: 20,
        right: 10,
        left: 10,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 100,
            width: MediaQuery.of(context).size.width,
            child: CachedNetworkImage(
              imageUrl: data.speculationInfo!.imageUrl!,
              imageBuilder: (context, imageProvider) => Container(
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10),
                  ),
                  image: DecorationImage(
                      scale: 2,
                      image: imageProvider,
                      fit: BoxFit.cover,
                      colorFilter: const ColorFilter.mode(
                          Colors.red, BlendMode.colorBurn)),
                ),
              ),
              placeholder: (context, url) => Container(
                color: ColorManager.grey1,
                height: 100,
                width: MediaQuery.of(context).size.width,
              ),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 5, top: 5),
            padding: const EdgeInsets.all(5),
            child: Text(
              data.speculationInfo!.name.toString(),
              style: getBoldTextStyle(
                  color: ColorManager.black, fontSize: FontSize.s15),
            ),
          ),
          Row(
            children: [
              prixCard(isMin: true, context: context),
              const Spacer(),
              prixCard(isMin: false, context: context),
            ],
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: TextButton(
              style: TextButton.styleFrom(padding: const EdgeInsets.all(0)),
              onPressed: () {
                Navigator.pushNamed(context, Routes.detailsMarketPrice);
              },
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    AppStrings.seeMore,
                    style: getSemiBoldTextStyle(color: ColorManager.black),
                  ),
                  Icon(IconManager.arrowRight, color: ColorManager.black)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget prixCard({bool? isMin, required BuildContext context}) {
    return TextButton(
      onPressed: () {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: isMin == false
                  ? const Text('Zone la plus chére')
                  : const Text('Zone la moins chére'),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text('Fermer'),
                ),
              ],
              content: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      CircleAvatar(
                        radius: 5,
                        backgroundColor:
                            isMin == true ? Colors.green : Colors.red,
                      ),
                      Text(
                        data.locationInfo!.region!.name.toString(),
                        style: getBoldTextStyle(
                            color: ColorManager.black, fontSize: FontSize.s13),
                      ),
                      // Image.asset(
                      //   ImageAssets.downArrow,
                      //   width: 12,
                      // ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Text(
                        "En gros : ",
                        style: getLightTextStyle(
                            color: ColorManager.black, fontSize: FontSize.s12),
                      ),
                      Text(
                        "${data.priceInfo!.prixGros.toString()} ${AppStrings.moneyDevise}",
                        style: getMeduimTextStyle(
                            color: ColorManager.black, fontSize: FontSize.s12),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: [
                      Text(
                        "En détail : ",
                        style: getLightTextStyle(
                            color: ColorManager.black, fontSize: FontSize.s12),
                      ),
                      Text(
                        "${data.priceInfo!.prixDetails.toString()} ${AppStrings.moneyDevise}",
                        style: getMeduimTextStyle(
                            color: ColorManager.black, fontSize: FontSize.s12),
                      )
                    ],
                  )
                ],
              ),
            );
          },
        );
      },
      child: Row(
        children: [
          if (isMin == true)
            const CircleAvatar(
              radius: 5,
              backgroundColor: Colors.green,
            ),
          Text(
            data.locationInfo!.region!.name.toString(),
            style: getMeduimTextStyle(
                color: ColorManager.black, fontSize: FontSize.s13),
          ),
          if (isMin == false)
            const CircleAvatar(
              radius: 5,
              backgroundColor: Colors.red,
            ),
          // Image.asset(
          //   ImageAssets.downArrow,
          //   width: 12,
          // ),
        ],
      ),
    );
  }
}
