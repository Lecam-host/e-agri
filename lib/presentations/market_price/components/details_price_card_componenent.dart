import 'package:flutter/material.dart';

import '../../ressources/color_manager.dart';
import '../../ressources/font_manager.dart';
import '../../ressources/routes_manager.dart';
import '../../ressources/strings_manager.dart';
import '../../ressources/styles_manager.dart';
import '../../ressources/values_manager.dart';

class DetailsPriceComponent extends StatelessWidget {
  const DetailsPriceComponent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: AppMargin.m10),
      child: ListTile(
        onTap: () {
          Navigator.pushNamed(context, Routes.historyPrice);
        },
        tileColor: ColorManager.white,
        title: Text(
          'Yamoussoukro',
          style: getRegularTextStyle(
              color: ColorManager.black, fontSize: FontSize.s14),
        ),
        subtitle: Column(
          children: [
            const SizedBox(
              height: AppMargin.m10,
            ),
            Row(
              children: [
                Text(
                  '2000 ${AppStrings.moneyUnit}',
                  style: getSemiBoldTextStyle(color: ColorManager.blue),
                ),
                const Spacer(),
                Text(
                  '2500 ${AppStrings.moneyUnit}',
                  style: getSemiBoldTextStyle(color: ColorManager.blue),
                ),
              ],
            ),
            const Row(
              children: [
                Text(AppStrings.gros),
                Spacer(),
                Text(AppStrings.details)
              ],
            )
          ],
        ),
      ),
    );
  }
}
