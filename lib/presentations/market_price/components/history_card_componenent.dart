import 'package:eagri/domain/model/model.dart';
import 'package:flutter/material.dart';

import '../../../app/functions.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/font_manager.dart';
import '../../ressources/strings_manager.dart';
import '../../ressources/styles_manager.dart';
import '../../ressources/values_manager.dart';

class HistoryCardComponent extends StatelessWidget {
  const HistoryCardComponent({Key? key, required this.speculationPrice})
      : super(key: key);
  final SpeculationPriceModel speculationPrice;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: AppMargin.m10),
      child: ListTile(
        tileColor: ColorManager.white,
        title: Text(
          'Du ${convertDate(speculationPrice.priceInfo!.fromPeriod!)} au ${convertDate(speculationPrice.priceInfo!.toPeriod!)}',
          style: getRegularTextStyle(
              color: ColorManager.black, fontSize: FontSize.s14),
        ),
        subtitle: Column(
          children: [
            const SizedBox(
              height: AppMargin.m10,
            ),
            Row(
              children: [
                Text(
                  '${speculationPrice.priceInfo!.prixDetails!} ${AppStrings.moneyUnit}',
                  style: getBoldTextStyle(color: ColorManager.blue),
                ),
                const Spacer(),
                Text(
                  '${speculationPrice.priceInfo!.prixGros!} ${AppStrings.moneyUnit}',
                  style: getBoldTextStyle(color: ColorManager.blue),
                ),
              ],
            ),
            const Row(
              children: [
                Text(AppStrings.details),
                Spacer(),
                Text(AppStrings.gros),
              ],
            )
          ],
        ),
      ),
    );
  }
}
