import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:flutter/material.dart';

import '../../ressources/color_manager.dart';
import '../../ressources/font_manager.dart';
import '../../ressources/strings_manager.dart';
import '../../ressources/styles_manager.dart';
import '../../ressources/values_manager.dart';

class SpeculationWithPriceComponenent extends StatefulWidget {
  const SpeculationWithPriceComponenent({
    Key? key,
    required this.speculationPrice,
  }) : super(key: key);
  final SpeculationPriceModel speculationPrice;

  @override
  State<SpeculationWithPriceComponenent> createState() =>
      _SpeculationWithPriceComponenentState();
}

class _SpeculationWithPriceComponenentState
    extends State<SpeculationWithPriceComponenent> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: ColorManager.white,
        boxShadow: [shadow()],
      ),
      margin: const EdgeInsets.only(
        top: 20,
        right: 10,
        left: 10,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (widget.speculationPrice.speculationInfo != null)
            if (widget.speculationPrice.speculationInfo!.imageUrl != null)
              SizedBox(
                height: 100,
                width: MediaQuery.of(context).size.width,
                child: CachedNetworkImage(
                  imageUrl: widget.speculationPrice.speculationInfo!.imageUrl!,
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10),
                      ),
                      image: DecorationImage(
                        scale: 2,
                        image: imageProvider,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  placeholder: (context, url) => Container(
                    color: ColorManager.grey1,
                    height: 100,
                    width: MediaQuery.of(context).size.width,
                  ),
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                ),
              ),
          if (widget.speculationPrice.speculationInfo == null)
            Container(
              width: double.maxFinite,
              height: 100,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10)),
              ),
              child: const Icon(Icons.image),
            ),
          Container(
            margin: const EdgeInsets.only(left: 5, top: 5),
            padding: const EdgeInsets.all(5),
            child: Text(
              widget.speculationPrice.speculationInfo!.name.toString(),
              style: getBoldTextStyle(
                  color: ColorManager.black, fontSize: FontSize.s15),
            ),
          ),
          const SizedBox(
            height: AppMargin.m10,
          ),
          Row(
            children: [
              Text(
                '${widget.speculationPrice.priceInfo!.prixGros} ${AppStrings.moneyUnit}',
                style: getSemiBoldTextStyle(color: ColorManager.blue),
              ),
              const Spacer(),
              Text(
                '${widget.speculationPrice.priceInfo!.prixDetails} ${AppStrings.moneyUnit}',
                style: getSemiBoldTextStyle(
                  color: ColorManager.blue,
                ),
              ),
            ],
          ),
          Row(
            children: [
              Text(
                AppStrings.gros,
                style: getRegularTextStyle(color: ColorManager.grey),
              ),
              const Spacer(),
              Text(
                AppStrings.details,
                style: getRegularTextStyle(color: ColorManager.grey),
              )
            ],
          )
        ],
      ),
    );
  }
}
