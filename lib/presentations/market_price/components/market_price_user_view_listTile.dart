// ignore_for_file: file_names

import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/app/functions.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/font_manager.dart';
import '../../ressources/strings_manager.dart';
import '../../ressources/styles_manager.dart';

class MarketPriceUserViewListTileComponent extends StatelessWidget {
  const MarketPriceUserViewListTileComponent({Key? key, required this.data})
      : super(key: key);
  final SpeculationPriceModel data;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        // Navigator.pushNamed(context, Routes.detailsMarketPrice);
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: ColorManager.white,
          boxShadow: [shadow()],
        ),
        margin: const EdgeInsets.only(
          bottom: 20,
          right: 10,
          left: 10,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 100,
              width: MediaQuery.of(context).size.width,
              child: CachedNetworkImage(
                imageUrl: data.speculationInfo!.imageUrl ?? "",
                imageBuilder: (context, imageProvider) => Container(
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10),
                    ),
                    image: DecorationImage(
                      scale: 2,
                      image: imageProvider,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                placeholder: (context, url) => Container(
                  color: ColorManager.grey1,
                  height: 100,
                  width: MediaQuery.of(context).size.width,
                ),
                errorWidget: (context, url, error) => const Icon(Icons.error),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(left: 5, top: 5),
              padding: const EdgeInsets.all(5),
              child: Text(
                data.speculationInfo!.name.toString(),
                style: getBoldTextStyle(
                    color: ColorManager.black, fontSize: FontSize.s15),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(left: 10),
              child: Text(
                data.locationInfo!.region != null
                    ? data.locationInfo!.region!.name != null
                        ? data.locationInfo!.region!.name!
                        : ""
                    : "",
                style: getRegularTextStyle(
                    color: ColorManager.black, fontSize: FontSize.s13),
              ),
            ),
            Row(
              children: [
                Container(
                  margin: const EdgeInsets.only(left: 10),
                  child: Text(
                    'Publié par ${data.priceInfo!.source}',
                    style: getRegularTextStyle(color: ColorManager.grey),
                  ),
                ),
                const Spacer(),
                Container(
                  padding:
                      const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                  child: RatingBar.builder(
                    initialRating: 2.2,
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    itemSize: 15,
                    itemPadding: const EdgeInsets.symmetric(horizontal: 1),
                    itemBuilder: (context, _) => const Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (rating) {},
                  ),
                )
              ],
            ),
            prixCard(context: context),
            const SizedBox(
              height: 5,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  margin: const EdgeInsets.only(
                    left: 10,
                  ),
                  child: Text(
                    convertDate(data.priceInfo!.fromPeriod!),
                    style: getRegularTextStyle(
                      color: ColorManager.grey,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Container prixCard({required BuildContext context}) {
    return Container(
      margin: const EdgeInsets.only(top: 5, right: 10, left: 10, bottom: 5),
      padding: const EdgeInsets.only(top: 5, right: 10, left: 10, bottom: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(
          color: ColorManager.grey,
          width: 0.5,
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            children: [
              Text(
                "${data.priceInfo!.prixDetails.toString()} ${AppStrings.moneyDevise}",
                style: getBoldTextStyle(
                  color: ColorManager.black,
                  fontSize: FontSize.s15,
                ),
              ),
              const Spacer(),
              Text(
                "${data.priceInfo!.prixGros.toString()} ${AppStrings.moneyDevise}",
                style: getBoldTextStyle(
                  color: ColorManager.black,
                  fontSize: FontSize.s15,
                ),
              ),
            ],
          ),
          Row(
            children: [
              Text(
                AppStrings.priceDetail,
                style: getRegularTextStyle(
                  color: ColorManager.grey,
                ),
              ),
              const Spacer(),
              Text(
                AppStrings.priceGros,
                style: getRegularTextStyle(
                  color: ColorManager.grey,
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
