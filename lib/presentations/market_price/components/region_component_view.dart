import 'package:eagri/data/request/request.dart';
import 'package:eagri/domain/model/location_model.dart';
import 'package:eagri/presentations/common/buttons/see_more_button.dart';
import 'package:eagri/presentations/market_price/list_speculation_with_price.dart';
import 'package:eagri/presentations/ressources/icon_manager.dart';
import 'package:flutter/material.dart';

import '../../ressources/color_manager.dart';
import '../../ressources/font_manager.dart';
import '../../ressources/styles_manager.dart';

class RegionComponenent extends StatefulWidget {
  const RegionComponenent({Key? key, required this.region}) : super(key: key);
  final RegionModel region;
  @override
  State<RegionComponenent> createState() => _RegionComponenentState();
}

class _RegionComponenentState extends State<RegionComponenent> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: ColorManager.white,
        boxShadow: [shadow()],
      ),
      margin: const EdgeInsets.only(
        top: 10,
        right: 10,
        left: 10,
      ),
      child: Row(
        children: [
          Container(
            margin: const EdgeInsets.only(left: 5, top: 5),
            padding: const EdgeInsets.all(5),
            child: Row(
              children: [
                Icon(
                  IconManager.locationPin,
                  color: ColorManager.primary,
                ),
                const SizedBox(
                  width: 5,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width / 2,
                  child: Text(
                    widget.region.name.toString(),
                    style: getMeduimTextStyle(
                        color: ColorManager.black, fontSize: FontSize.s14),
                    textAlign: TextAlign.start,
                  ),
                ),
              ],
            ),
          ),
          const Spacer(),
          SeeMoreTextButton(
            child: ListSpeculationWithPriceView(
              region: widget.region,
              request: SpeculationPriceRequest(
                  regionId: widget.region.regionId, userId: 1, official: true),
            ),
          )
        ],
      ),
    );
  }
}
