import 'package:animations/animations.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/common/state/loader.dart';
import 'package:eagri/presentations/common/search_field.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/icon_manager.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import '../../app/constant.dart';
import '../../app/di.dart';
import '../../data/request/request.dart';
import '../../domain/usecase/speculation_price_usecase.dart';
import '../base/base_view_model.dart';
import '../common/buttons/search_button.dart';
import '../common/showModalBottomSheetListTile.dart';
import '../conseils/demande_conseil_view.dart';
import '../ressources/assets_manager.dart';
import '../ressources/font_manager.dart';
import '../ressources/strings_manager.dart';
import '../ressources/styles_manager.dart';
import 'list_speculation_price_by_location_view.dart';
import 'market_price_view_model.dart';

class ListSpeculationView extends StatefulWidget {
  const ListSpeculationView({Key? key}) : super(key: key);

  @override
  State<ListSpeculationView> createState() => _ListSpeculationViewState();
}

class _ListSpeculationViewState extends State<ListSpeculationView> {
  BaseViewModel? baseViewModel;
  MarketPriceViewModel viewModel = instance<MarketPriceViewModel>();
  GetListSpeculationPriceRequest request =
      GetListSpeculationPriceRequest(0, 10);

  List<SpeculationWithMinAndMaxSpeculationPriceModel>
      listSpeculationWithMinMax = [];
  List<SpeculationWithMinAndMaxSpeculationPriceModel>
      initialListSpeculationWithMinMax = [];

  SpeculationPriceUseCase speculationPriceUseCase =
      instance<SpeculationPriceUseCase>();
  bool isLoadPrice = true;
  bool isFistShowList = true;
  getMinMaxPrice(SpeculationModel speculation) async{
  await  speculationPriceUseCase
        .getMinMaxPrice(
          GetMinAndMaxSpeculationnPriceRequest(
              speculationId: speculation.speculationId!, official: true),
        )
        .then((value) => value.fold((l) {
              setState(() {
                isLoadPrice = false;
              });
            }, (response) {
              if (mounted) {
                setState(() {
                  isLoadPrice = false;

                  if (response != null) {
                    listSpeculationWithMinMax.add(
                        SpeculationWithMinAndMaxSpeculationPriceModel(
                            miMaxObject: response, speculation: speculation));
                    initialListSpeculationWithMinMax.add(
                        SpeculationWithMinAndMaxSpeculationPriceModel(
                            miMaxObject: response, speculation: speculation));
                             initialListSpeculationWithMinMax.sort((a, b) => a.speculation.name! .compareTo(b.speculation.name!)); 
                             listSpeculationWithMinMax.sort((a, b) => a.speculation.name! .compareTo(b.speculation.name!));
                             
                  }
                });
              }
            }));
  }

  Future getListSpeculation() async {
    WidgetsBinding.instance.addPostFrameCallback(
        (_) => loaderDialog(context, text: "Veuillez patienter ..."));

    (await speculationPriceUseCase.getListSpeculation()).fold((failure) {
      Navigator.pop(context);
      setState(() {
        isLoadPrice = false;
      });
      showCustomFlushbar(
          context, failure.message, ColorManager.error, FlushbarPosition.TOP);
    }, (result)async {
      if (result.data != null) {
        for (var element in result.data!) {
           getMinMaxPrice(element);
       
        }
     
        
        
      }

      setState(() {
        isLoadPrice = false;
      });

      Navigator.pop(context);
    });
  }
filterListPrice(List<SpeculationModel> data){
  setState(() {
  listSpeculationWithMinMax.sort((a, b) => a.speculation.name! .compareTo(b.speculation.name!));
    
  });
  // listSpeculationWithMinMax=[];
  // for (var index = 0; index < initialListSpeculationWithMinMax.length; index++) {
  //    if(data[index].speculationId==initialListSpeculationWithMinMax[index].speculation.speculationId){
  //     setState(() {
  //         listSpeculationWithMinMax.add(initialListSpeculationWithMinMax[index]);
  //     });
    
  //    }
  // }
 

}
  @override
  void initState() {
    viewModel.start();
    getListSpeculation();

    super.initState();
  }

  @override
  void dispose() {
    viewModel.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return getContent();
  }

  Shimmer shimmerCustom() {
    return Shimmer.fromColors(
      baseColor: const Color.fromARGB(255, 0, 0, 0),
      highlightColor: const Color.fromARGB(255, 181, 181, 181),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: ColorManager.white,
        ),
        margin: const EdgeInsets.only(
          top: 20,
          right: 10,
          left: 10,
        ),
        height: 100.0,
      ),
    );
  }

  List<Widget> actions() => [
        IconButton(
          onPressed: () {
            showModalBottomSheet(
                context: context,
                builder: (context) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      showModalBottomSheetListTile(
                          titre: AppStrings.orderAsc,
                          icon: IconManager.orderAsc,
                          widget: const DemandeConveilView(),
                          context: context),
                      showModalBottomSheetListTile(
                          titre: AppStrings.orderDesc,
                          icon: IconManager.orderDesc,
                          widget: const DemandeConveilView(),
                          context: context),
                      showModalBottomSheetListTile(
                          titre: AppStrings.orderAlpha,
                          icon: IconManager.orderAlpha,
                          widget: const DemandeConveilView(),
                          context: context),
                    ],
                  );
                });
          },
          icon: Image.asset(
            ImageAssets.filter,
            width: 20,
          ),
        ),
        const SearchIconButton(),
      ];
  Widget getContent() {
    return isLoadPrice == false
        ? initialListSpeculationWithMinMax.isNotEmpty
            ? listContent(listSpeculationWithMinMax)
            : const EmptyComponenent(
                message: "Aucun prix trouvé",
              )
        : const SizedBox();
  }

  listContent(
      List<SpeculationWithMinAndMaxSpeculationPriceModel> listSpeculation) {
    return Column(
      children: [
        if (initialListSpeculationWithMinMax.isNotEmpty) selectContent(),
        if (listSpeculationWithMinMax.isNotEmpty)
          Expanded(
            child: GridView.builder(
              itemCount: listSpeculation.length,
              itemBuilder: (BuildContext context, int index) => OpenContainer(
                openElevation: 0,
                closedElevation: 0,
                transitionType: transitionType,
                transitionDuration: transitionDuration,
                openBuilder: (context, _) {
                  return ListSpeculationPriceByLocationView(
                    speculation: listSpeculationWithMinMax[index].speculation,
                  );
                },
                closedBuilder: (context, VoidCallback openContainer) =>
                    speculationCard(
                        listSpeculationWithMinMax[index],
                        SpeculationPriceRequest(
                          speculationId: listSpeculationWithMinMax[index]
                              .speculation
                              .speculationId,
                          official: true,
                        ),
                        isFistShowList),
              ),

              //return Image.asset(images[index], fit: BoxFit.cover);

              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisSpacing: 10,
                crossAxisSpacing: 10,
                mainAxisExtent: 220,
              ),
              padding: const EdgeInsets.all(10),
              shrinkWrap: true,
            ),
          ),
      ],
    );
  }

  showFlushbar() {
    return Flushbar(
      maxWidth: 200,
      flushbarPosition: FlushbarPosition.BOTTOM,
      duration: const Duration(seconds: 3),
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.all(10),
      borderRadius: BorderRadius.circular(15),
      backgroundColor: Colors.blue,
      icon: Icon(Icons.info, color: ColorManager.white),
      boxShadows: const [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],
      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      // title: '',
      message: "Prix non disponible",
    ).show(context);
  }

  Widget selectContent() {
    return initialListSpeculationWithMinMax.isNotEmpty
        ? Card(
            child: SearchField(
            onChanged: (value) {
              setState(() {
                isFistShowList = false;
              });

              List<SpeculationWithMinAndMaxSpeculationPriceModel> result = [];
              if (value != "") {
                for (SpeculationWithMinAndMaxSpeculationPriceModel element
                    in initialListSpeculationWithMinMax) {
                  if (element.speculation.name != null) {
                    if (element.speculation.name!
                        .toLowerCase()
                        .contains(value.toLowerCase())) {
                      result.add(SpeculationWithMinAndMaxSpeculationPriceModel(
                          speculation: element.speculation,
                          miMaxObject: element.miMaxObject));
                    }
                  }
                }
              } else {
                result = initialListSpeculationWithMinMax;
              }

              setState(() {
                listSpeculationWithMinMax = result;
              });
            },
            hintText: "Rechercher un produit",
          ))
        : Container();
  }

  Widget speculationCard(
    SpeculationWithMinAndMaxSpeculationPriceModel speculationWithMinAndMax,
    SpeculationPriceRequest request,
    bool isFirstShow,
  ) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: ColorManager.white,
        boxShadow: [shadow()],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 100,
            width: MediaQuery.of(context).size.width,
            child: CachedNetworkImage(
              imageUrl: speculationWithMinAndMax.speculation.imageUrl!,
              imageBuilder: (context, imageProvider) => Container(
                decoration: BoxDecoration(
                  color: ColorManager.grey1,
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(5),
                    topRight: Radius.circular(5),
                  ),
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              placeholder: (context, url) => Container(
                color: ColorManager.grey1,
                height: 100,
                width: MediaQuery.of(context).size.width,
              ),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: SizedBox(
              height: 40,
              child: Text(
                speculationWithMinAndMax.speculation.name.toString(),
                style: getMeduimTextStyle(
                    color: ColorManager.black, fontSize: FontSize.s12),
              ),
            ),
          ),
         
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: Text(
              speculationWithMinAndMax.miMaxObject.maxPriceRegion.name!,
              style: getSemiBoldTextStyle(color: ColorManager.red, fontSize: 10),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: Text(
              "${speculationWithMinAndMax.miMaxObject.maxPrice.toString()} ${AppStrings.moneyUnit}",
              style: getSemiBoldTextStyle(color: ColorManager.red, fontSize: 10),
              maxLines: 1,
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: Text(
              speculationWithMinAndMax.miMaxObject.minPriceRegion.name!,
              style: getSemiBoldTextStyle(
                  color: ColorManager.primary, fontSize: 10),
              maxLines: 1,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: Text(
              "${speculationWithMinAndMax.miMaxObject.minPrice.toString()} ${AppStrings.moneyUnit}",
              style: getSemiBoldTextStyle(
                  color: ColorManager.primary, fontSize: 10),
              maxLines: 1,
            ),
          ),
           
        ],
      ),
    );
  }
}

class SpeculationWithMinAndMaxSpeculationPriceModel {
  final MinAndMaxSpeculationPriceModel miMaxObject;
  final SpeculationModel speculation;
  SpeculationWithMinAndMaxSpeculationPriceModel({
    required this.speculation,
    required this.miMaxObject,
  });
}
