import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/domain/model/location_model.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/icon_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:sliver_tools/sliver_tools.dart';
import '../../app/di.dart';
import '../../data/request/request.dart';
import '../../domain/usecase/speculation_price_usecase.dart';
import '../common/buttons/back_button.dart';
import '../common/buttons/search_button.dart';
import '../common/search_field.dart';
import '../common/showModalBottomSheetListTile.dart';
import '../common/state_renderer/state_render_impl.dart';
import '../common/state_renderer/state_renderer.dart';
import '../conseils/demande_conseil_view.dart';
import '../ressources/assets_manager.dart';
import '../ressources/strings_manager.dart';
import '../ressources/styles_manager.dart';
import '../ressources/values_manager.dart';
import 'market_price_view_model.dart';

class ListSpeculationPriceByLocationView extends StatefulWidget {
  const ListSpeculationPriceByLocationView({Key? key, this.speculation})
      : super(key: key);
  final SpeculationModel? speculation;

  @override
  State<ListSpeculationPriceByLocationView> createState() =>
      _ListSpeculationPriceByLocationViewState();
}

class _ListSpeculationPriceByLocationViewState
    extends State<ListSpeculationPriceByLocationView> {
  MarketPriceViewModel viewModel = instance<MarketPriceViewModel>();
  GetListSpeculationPriceRequest request =
      GetListSpeculationPriceRequest(0, 10);
  List<RegionModel> listRegion = [];
  List<SpeculationPriceModel> listSpeculationPrice = [];
  List<SpeculationPriceModel> initialListSpeculationPrice = [];

  SpeculationPriceUseCase speculationPriceUseCase =
      instance<SpeculationPriceUseCase>();
  late ScrollController _scrollController;
  Color textColor = Colors.white;
  bool get _isSliverAppBarExpanded {
    return _scrollController.hasClients &&
        _scrollController.offset > (200 - kToolbarHeight);
  }

  getListRegion() async {
    (await speculationPriceUseCase.getListRegion()).fold((failure) {},
        (result) {
      setState(() {
        listRegion = result.data;
      });
    });
  }

  Future getListSpeculationPrice() async {
    LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE)
        .getScreenWidget(context, getContent(), () {});
    (await speculationPriceUseCase.getSpeculationPriceHistory(
            SpeculationPriceRequest(
                speculationId: widget.speculation!.speculationId!,
                official: true)))
        .fold((failure) {
      Navigator.of(context).pop();
    }, (result) {
      Navigator.of(context).pop();

      setState(() {
        initialListSpeculationPrice = result.listSpeculationPrice!;
        listSpeculationPrice = result.listSpeculationPrice!;
      });
    });
  }

  start() {
    getListSpeculationPrice();
    // if (widget.compareRequest != null) compareSpeculationPrice();
  }

  @override
  void initState() {
    start();
    getListRegion();
    _scrollController = ScrollController()
      ..addListener(() {
        setState(() {
          textColor =
              _isSliverAppBarExpanded ? ColorManager.black : ColorManager.white;
        });
      });

    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 237, 237, 237),
      body: CustomScrollView(
        controller: _scrollController,
        slivers: <Widget>[
          SliverAppBar(
            stretchTriggerOffset: 50,
            pinned: true,
            snap: true,
            floating: true,
            expandedHeight: 200.0,

            leading: BackButtonCustom(
              color: textColor,
            ),
            flexibleSpace: FlexibleSpaceBar(
              background: CachedNetworkImage(
                imageUrl: widget.speculation!.imageUrl!,
                imageBuilder: (context, imageProvider) => Container(
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(5),
                      topRight: Radius.circular(5),
                    ),
                    image: DecorationImage(
                      colorFilter: ColorFilter.mode(
                          Colors.black.withOpacity(0.5), BlendMode.darken),
                      scale: 2,
                      image: imageProvider,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                placeholder: (context, url) => Container(
                  color: ColorManager.grey1,
                  height: 100,
                  width: MediaQuery.of(context).size.width,
                ),
                errorWidget: (context, url, error) => const Icon(Icons.error),
              ),
              collapseMode: CollapseMode.parallax,
              centerTitle: true,
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
                children: [
                  
                  Text(
                    "${AppStrings.marketPrice} de ${widget.speculation!.name}",
                    style: getSemiBoldTextStyle(color: textColor),
                  ),
                  if(initialListSpeculationPrice.isNotEmpty)
                  Row(
                    children: [
                      Text(
                    "Du ${initialListSpeculationPrice[0].priceInfo!.fromPeriod!}",
                        style: getSemiBoldTextStyle(color: textColor,fontSize: 10),
                      ),
                  
                       Text(
                        " Au ${initialListSpeculationPrice[0].priceInfo!.toPeriod!}",
                        style: getSemiBoldTextStyle(color: textColor,fontSize: 10),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            //actions: actions(),
          ),
          MultiSliver(
            // defaults to false
            pushPinnedChildren: true,
            children: <Widget>[
              SliverPinnedHeader(
                child: selectContent(),
              ),
              SliverList(
                delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                  return priceWidget(listSpeculationPrice[index]);
                },
                    // 40 list items
                    childCount: listSpeculationPrice.length),
              ),
            ],
          )

          //   getContent()
          // SizedBox()
        ],
      ),
    );
  }

  // Widget selectContent() {
  //   return initiaLlistSpeculationPrice.isNotEmpty
  //       ? Card(
  //           child: SearchField(
  //           onChanged: (value) {

  //             List<SpeculationPriceModel> result = [];
  //             if (value != "") {
  //               for (SpeculationPriceModel element
  //                   in initiaLlistSpeculationPrice) {
  //                 if (element.locationInfo != null) {
  //                   if (element.locationInfo!.region != null) {
  //                     if (element.locationInfo!.region!.name != null) {
  //                       if (element.locationInfo!.region!.name!
  //                           .toLowerCase()
  //                           .contains(value.toLowerCase())) {
  //                         result.add(element);
  //                       }
  //                     }
  //                   }
  //                 }
  //               }
  //             } else {
  //               result = initiaLlistSpeculationPrice;
  //             }

  //             setState(() {
  //               listSpeculationPrice = result;
  //             });
  //           },
  //           hintText: "Rechercher une region",
  //         ))
  //       : Container();
  // }

  List<Widget> actions() => [
        IconButton(
          onPressed: () {
            showModalBottomSheet(
                context: context,
                builder: (context) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      showModalBottomSheetListTile(
                          titre: AppStrings.orderAsc,
                          icon: IconManager.orderAsc,
                          widget: const DemandeConveilView(),
                          context: context),
                      showModalBottomSheetListTile(
                          titre: AppStrings.orderDesc,
                          icon: IconManager.orderDesc,
                          widget: const DemandeConveilView(),
                          context: context),
                      showModalBottomSheetListTile(
                          titre: AppStrings.orderAlpha,
                          icon: IconManager.orderAlpha,
                          widget: const DemandeConveilView(),
                          context: context),
                    ],
                  );
                });
          },
          icon: Image.asset(
            ImageAssets.filter,
            width: 20,
          ),
        ),
        const SearchIconButton(),
      ];
  Widget getContent() {
    return listContent();
  }

  listContent() {
    return ListView.builder(
      itemCount: listSpeculationPrice.length,
      itemBuilder: (BuildContext context, int index) {
        return AnimationConfiguration.staggeredList(
          position: index,
          duration: const Duration(milliseconds: 100),
          child: SlideAnimation(
            verticalOffset: 50.0,
            child: FadeInAnimation(
              child: priceWidget(listSpeculationPrice[index]),
            ),
          ),
        );
      },
    );
  }

  Widget selectContent() {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        child: SearchField(
          onChanged: (value) {
            List<SpeculationPriceModel> result = [];
            if (value != "") {
              for (var element in initialListSpeculationPrice) {
                if (element.locationInfo!.region!.name!
                    .toLowerCase()
                    .contains(value.toLowerCase())) {
                  result.add(element);
                }
              }
            } else {
              result = initialListSpeculationPrice;
            }

            setState(() {
              listSpeculationPrice = result;
            });
          },
          hintText: "Rechercher une region",
        ));
  }

  Widget priceWidget(SpeculationPriceModel data) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: ColorManager.white, borderRadius: BorderRadius.circular(10)),
      margin: const EdgeInsets.all(5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Icon(
                IconManager.locationPin,
                color: ColorManager.primary,
              ),
              Expanded(
                child: Text(
                  data.locationInfo!.region!.name.toString(),
                  style: getMeduimTextStyle(
                    color: ColorManager.black,
                  ),
                  maxLines: 3,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: AppMargin.m10,
          ),
          Row(
            children: [
              Text(
                '${data.priceInfo!.prixGros} ${AppStrings.moneyUnit}',
                style: getSemiBoldTextStyle(color: ColorManager.blue),
              ),
              const Spacer(),
              Text(
                '${data.priceInfo!.prixDetails} ${AppStrings.moneyUnit}',
                style: getSemiBoldTextStyle(
                  color: ColorManager.blue,
                ),
              ),
            ],
          ),
          Row(
            children: [
              Text(
                AppStrings.gros,
                style: getRegularTextStyle(color: ColorManager.grey),
              ),
              const Spacer(),
              Text(
                AppStrings.details,
                style: getRegularTextStyle(color: ColorManager.grey),
              )
            ],
          )
        ],
      ),
    );
  }
}
