import 'dart:developer';

import 'package:eagri/domain/model/location_model.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/presentations/market_price/add_speculation_price.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:multi_select_flutter/dialog/multi_select_dialog_field.dart';
import 'package:multi_select_flutter/util/multi_select_item.dart';
import 'package:multi_select_flutter/util/multi_select_list_type.dart';
import 'package:page_transition/page_transition.dart';
import 'package:sliver_tools/sliver_tools.dart';

import '../../app/di.dart';
import '../../data/request/request.dart';
import '../../domain/usecase/speculation_price_usecase.dart';
import '../common/buttons/back_button.dart';
import '../common/buttons/search_button.dart';
import '../common/search_field.dart';
import '../common/state_renderer/state_render_impl.dart';
import '../common/state_renderer/state_renderer.dart';
import '../ressources/assets_manager.dart';
import '../ressources/strings_manager.dart';
import '../ressources/values_manager.dart';
import 'components/market_price_user_view_listTile.dart';
import 'market_price_view_model.dart';

class ListSpeculationPriceUserView extends StatefulWidget {
  const ListSpeculationPriceUserView({Key? key}) : super(key: key);

  @override
  State<ListSpeculationPriceUserView> createState() =>
      _ListSpeculationPriceUserViewState();
}

class _ListSpeculationPriceUserViewState
    extends State<ListSpeculationPriceUserView> {
  MarketPriceViewModel viewModel = instance<MarketPriceViewModel>();
  GetListSpeculationPriceRequest request =
      GetListSpeculationPriceRequest(0, 100);

  List<RegionModel> listRegion = [];
  List<SpeculationModel> listSpeculation = [];
  List<RegionModel> listRegionChoisi = [];
  List<SpeculationModel> listSpeculationChoisi = [];

  SpeculationPriceUseCase speculationPriceUseCase =
      instance<SpeculationPriceUseCase>();
  List<SpeculationPriceModel> listSpeculationPrice = [];
  List<SpeculationPriceModel> initialListSpeculationPrice = [];

  Future getListSpeculation() async {
    (await speculationPriceUseCase.getListSpeculation()).fold((failure) {},
        (result) {
      setState(() {
        listSpeculation = result.data!;
      });
    });
  }

  Future getListRegion() async {
    (await speculationPriceUseCase.getListRegion()).fold((failure) {},
        (result) {
      setState(() {
        listRegion = result.data;
      });
    });
  }

  Future getListSpeculationPrice() async {
    LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE)
        .getScreenWidget(context, Container(), () {});
    (await speculationPriceUseCase.getListSpeculationPriceEnterByUser(request))
        .fold((failure) {
      inspect(failure);
      Navigator.of(context).pop();
    }, (result) {
      Navigator.of(context).pop();

      setState(() {
        initialListSpeculationPrice = result.listSpeculationPrice!;
        listSpeculationPrice = result.listSpeculationPrice!;
      });
    });
  }

  late ScrollController _scrollController;
  Color textColor = Colors.white;
  bool get _isSliverAppBarExpanded {
    return _scrollController.hasClients &&
        _scrollController.offset > (200 - kToolbarHeight);
  }

  @override
  void initState() {
    getListSpeculationPrice();
    // getListSpeculation();
    //  getListRegion();
    _scrollController = ScrollController()
      ..addListener(() {
        setState(() {
          textColor =
              _isSliverAppBarExpanded ? ColorManager.black : ColorManager.white;
        });
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: ColorManager.primary,
        onPressed: () {
          Navigator.push(
            context,
            PageTransition(
              type: PageTransitionType.rightToLeft,
              child: const AddSpeculationPriceView(),
              isIos: true,
              duration: const Duration(milliseconds: 400),
            ),
          );
        },
        child: const Icon(Icons.add),
      ),
      backgroundColor: const Color.fromARGB(255, 237, 237, 237),
      body: CustomScrollView(controller: _scrollController, slivers: <Widget>[
        SliverAppBar(
          stretchTriggerOffset: 50,
          pinned: true,
          snap: true,
          floating: true,
          expandedHeight: 200.0,
          leading: const BackButtonCustom(),
          flexibleSpace: FlexibleSpaceBar(
            collapseMode: CollapseMode.parallax,
            background: SafeArea(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 20,
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: Image.asset(
                        ImageAssets.souscription,
                        fit: BoxFit.cover,
                        height: 100,
                        width: 100,
                      ),
                    ),
                  ),
                  Text(
                    'Fournie par les utilisateurs de E-AGRI',
                    style: getRegularTextStyle(
                        color: ColorManager.black, fontSize: 16),
                  ),
                ],
              ),
            ),
            centerTitle: true,
            title: Text(AppStrings.marketPrice,
                style: getRegularTextStyle(
                    color: ColorManager.black, fontSize: 16)),
          ),
          //actions: actions(),
        ),
        MultiSliver(
          // defaults to false
          pushPinnedChildren: true,
          children: <Widget>[
            SliverPinnedHeader(
              child: selectContent(),
            ),
            if (listSpeculationPrice.isNotEmpty)
              SliverList(
                delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                  return MarketPriceUserViewListTileComponent(
                      data: listSpeculationPrice[index]);
                },
                    // 40 list items
                    childCount: listSpeculationPrice.length),
              ),
          ],
        ),
      ]),
    );
  }

  Widget selectContent() {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        child: SearchField(
          onChanged: (value) {
            List<SpeculationPriceModel> result = [];
            if (value != "") {
              for (var element in initialListSpeculationPrice) {
                if (element.speculationInfo != null) {
                  if (element.speculationInfo!.name != null) {
                    if (element.speculationInfo!.name!
                        .toString()
                        .toLowerCase()
                        .contains(value.toLowerCase())) {
                      result.add(element);
                    }
                  }
                } else if (element.locationInfo != null) {
                  if (element.locationInfo!.region != null) {
                    if (element.locationInfo!.region!.name!
                        .toString()
                        .toLowerCase()
                        .contains(value.toLowerCase())) {
                      result.add(element);
                    }
                  }
                }
              }
            } else {
              result = initialListSpeculationPrice;
            }

            setState(() {
              listSpeculationPrice = result;
            });
          },
          hintText: "Rechercher un produit",
        ));
  }

  List<Widget> actions() => [
        IconButton(
          onPressed: () {
            showModalBottomSheet(
                context: context,
                builder: (context) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      const SizedBox(
                        height: 20,
                      ),
                      const Text('Filtrer'),
                      const SizedBox(
                        height: 20,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            color: ColorManager.grey.withOpacity(0.1)),
                        child: MultiSelectDialogField(
                          buttonIcon: const Icon(Icons.keyboard_arrow_down),
                          buttonText: const Text(
                            AppStrings.regionQuestion,
                          ),
                          confirmText: const Text(AppStrings.ok),
                          cancelText: const Text(AppStrings.cancel),
                          title: const Text(AppStrings.regionQuestion),
                          items: listRegion
                              .map((e) => MultiSelectItem(e, e.name!))
                              .toList(),
                          listType: MultiSelectListType.LIST,
                          onConfirm: (List<RegionModel> regions) {
                            setState(() {
                              listRegionChoisi = regions;
                            });
                          },
                          selectedColor: ColorManager.primary,
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            color: ColorManager.grey.withOpacity(0.1)),
                        child: MultiSelectDialogField(
                          buttonIcon: const Icon(Icons.keyboard_arrow_down),
                          buttonText: const Text(
                            AppStrings.speculationQuestion,
                          ),
                          confirmText: const Text(AppStrings.ok),
                          cancelText: const Text(AppStrings.cancel),
                          title: const Text(AppStrings.speculationQuestion),
                          items: listSpeculation
                              .map((e) => MultiSelectItem(e, e.name!))
                              .toList(),
                          listType: MultiSelectListType.LIST,
                          onConfirm: (List<SpeculationModel> values) {
                            setState(() {
                              listSpeculationChoisi = values;
                            });
                          },
                          selectedColor: ColorManager.primary,
                        ),
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          elevation: 0,
                          backgroundColor: ColorManager.primary,
                          textStyle: getRegularTextStyle(
                            color: ColorManager.white,
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(
                              AppSize.s5,
                            ),
                          ),
                        ),
                        child: const Text(AppStrings.cancel),
                        onPressed: () {},
                      ),

                      // showModalBottomSheetListTile(
                      //     titre: AppStrings.orderAsc,
                      //     icon: IconManager.orderAsc,
                      //     widget: const DemandeConveilView(),
                      //     context: context),
                      // showModalBottomSheetListTile(
                      //     titre: AppStrings.orderDesc,
                      //     icon: IconManager.orderDesc,
                      //     widget: const DemandeConveilView(),
                      //     context: context),
                      // showModalBottomSheetListTile(
                      //     titre: AppStrings.orderAlpha,
                      //     icon: IconManager.orderAlpha,
                      //     widget: const DemandeConveilView(),
                      //     context: context),
                    ],
                  );
                });
          },
          icon: Image.asset(
            ImageAssets.filter,
            width: 20,
          ),
        ),
        const SearchIconButton(),
      ];
}
