import 'package:eagri/presentations/account/profil_view.dart';
import 'package:eagri/presentations/account/update_password_screen.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import '../../domain/model/user_model.dart';

class InfoPersoPage extends StatefulWidget {
  const InfoPersoPage({Key? key}) : super(key: key);

  @override
  State<InfoPersoPage> createState() => _InfoPersoPageState();
}

class _InfoPersoPageState extends State<InfoPersoPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
      builder: (context, user, child) => Scaffold(
        appBar: AppBar(
          actions: const [
            // TextButton(
            //   child: const Row(
            //     children: [
            //       Icon(
            //         Icons.edit,
            //         color: Colors.grey,
            //         size: 15,
            //       ),
            //       Text(
            //         "Modifier",
            //         style: TextStyle(color: Colors.grey),
            //       ),
            //     ],
            //   ),
            //   onPressed: () {
            //     _showBottomSheet(
            //       user,
            //     );
            //   },
            // ),
          ],
          centerTitle: true,
          title: Text('Détails du compte',
              style: TextStyle(
                  fontSize: 15,
                  fontStyle: FontStyle.normal,
                  color: ColorManager.black)),
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: const Icon(
              Icons.clear,
              color: Colors.black,
            ),
          ),
          backgroundColor: Colors.white,
          elevation: 0,
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(crossAxisAlignment: CrossAxisAlignment.end, children: [
            const SizedBox(height: 20),
            Center(
              child: Column(
                children: [
                  Container(
                    height: 150,
                    width: 150,
                    decoration:const BoxDecoration(
                        shape: BoxShape.circle, color: Colors.grey),
                  ),
                  Column(
                    children: [
                      Text(
                        user.firstName!,
                        style: getBoldTextStyle(fontSize: 17),
                      ),
                      Text(user.lastName!)
                    ],
                  )
                ],
              ),
            ),
            const SizedBox(height: 40),
            ListTileInfo(
              iconData: Icons.person_outlined,
              text: "Modifier le profil",
              subText: "Nom",
              onTap: () {
                Navigator.push(
                    context,
                    PageTransition(
                      type: PageTransitionType.rightToLeft,
                      child: const ProfilView(),
                    ));
              },
            ),
            const SizedBox(height: 10),
            const Divider(),
            const SizedBox(height: 10),

            ListTileInfo(
              iconData: Icons.security,
              text: "Modifier le mot de passe",
              subText: "Modifier le mot de passe",
              onTap: () {
                Navigator.push(
                  context,
                  PageTransition(
                    type: PageTransitionType.rightToLeft,
                    child: const UpdatePasswordScreen(),
                    isIos: true,
                    duration: const Duration(milliseconds: 400),
                  ),
                );
              },
            ),
            const SizedBox(height: 10),
            const Divider(),
            const SizedBox(height: 10),
            // ListTileInfo(
            //   iconData: Icons.place_outlined,
            //   text: user.address,
            //   subText: "Lieu de résidence",
            // ),
            // ListTileInfo(
            //   iconData: Icons.work_outline,
            //   text: user.profession,
            //   subText: "Proféssion",
            // ),
            // ListTile(
            //   onTap: () {
            //     Navigator.push(
            //       context,
            //       PageTransition(
            //         type: PageTransitionType.rightToLeft,
            //         child: const UpdatePasswordScreen(),
            //         isIos: true,
            //         duration: const Duration(milliseconds: 400),
            //       ),
            //     );
            //   },
            //   title: const Text("Changer le mot de passe"),
            //   trailing: const Icon(IconManager.arrowRight),
            // )
          ]),
        ),
      ),
    );
  }

}

class ListTileInfo extends StatelessWidget {
  const ListTileInfo(
      {Key? key,
      required this.text,
      required this.subText,
      required this.iconData,
      this.onTap})
      : super(key: key);
  final String text;
  final String subText;
  final IconData iconData;
  final Function()? onTap;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Row(
        children: [
          Container(
            width: 30.0,
            height: 30.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: ColorManager.primary.withOpacity(0.2),
            ),
            child: Icon(
              iconData,
              color: ColorManager.primary,
            ),
          ),
          const SizedBox(width: 10),
          Text(text)
        ],
      ),
    );
  }
}
