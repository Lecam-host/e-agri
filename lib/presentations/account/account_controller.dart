import 'dart:developer';

import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/domain/model/user_info_model.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../app/app_prefs.dart';
import '../../app/di.dart';
import '../../data/repository/api/repository_impl.dart';
import '../../domain/model/user_model.dart';

class AccountController extends GetxController {
  var oldPasswordController = TextEditingController().obs;
  var newPasswordController = TextEditingController().obs;
  var confirmNewPasswordController = TextEditingController().obs;

  var newNom = TextEditingController().obs;
  var newPrenom = TextEditingController().obs;
  var newNumber = TextEditingController().obs;
  var newBirthdate = TextEditingController().obs;
  var gender = "".obs;

  var isModifyFirstName = false.obs;
  var isModifyLastName = false.obs;
  var isModifyNumber = false.obs;
  var isModifyBirthdate = false.obs;
  var isModifyGender = false.obs;

  final RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  AppSharedPreference appSharedPreference = instance<AppSharedPreference>();

  var load = false.obs;
  var loadUserInfo = false.obs;
  var userInfo = UserModel().obs;

  updatePassword() async {
    load.value = true;
    await repositoryImpl
        .updatePassword(userInfo.value.id!, userInfo.value.userName!,
            newPasswordController.value.text, oldPasswordController.value.text)
        .then(
          (value) => value.fold(
            (l) {
              load.value = false;
              SnackBar(content: Text(l.message));
            },
            (r) {
              if (r!.status == 200) {
                load.value = false;
                Get.back();
                showCustomFlushbar(
                    Get.context!,
                    "Mot de passe effectué avec succes",
                    ColorManager.succes,
                    FlushbarPosition.TOP);
                newPasswordController.value.clear();
                oldPasswordController.value.clear();
                confirmNewPasswordController.value.clear();
                // Get.back();
              } else {
                log(r.toString());
                load.value = false;
                showCustomFlushbar(
                    Get.context!,
                    "Echec de la mise a jour. Mot de passe non valide",
                    ColorManager.error,
                    FlushbarPosition.TOP,
                    callback: () => Get.back());
              }
            },
          ),
        )
        .catchError((onError) {
      log(onError);
      load.value = false;
    });
  }

  updateUserInfoModel() async {
    loadUserInfo.value = true;
    var newUser = UserInfoModel(
        firstname: newNom.value.text,
        lastname: newPrenom.value.text,
        phone: newNumber.value.text,
        gender: gender.value,
        birthdate: newBirthdate.value.text.isEmpty
            ? userInfo.value.birthdate.toString()
            : newBirthdate.value.text,
        username: userInfo.value.userName);
    await repositoryImpl
        .updateUserInfo(userInfo.value.id!, newUser)
        .then(
          (value) => value.fold(
            (l) {
              loadUserInfo.value = false;
              showCustomFlushbar(Get.context!, "Echec de la mise a jour !",
                  ColorManager.error, FlushbarPosition.TOP);
            },
            (r) async {
              if (r!.status == 200) {
                await appSharedPreference.createLoginSession(
                    Get.context!,
                    UserModel(
                        token: userInfo.value.token,
                        id: userInfo.value.id,
                        firstName: newNom.value.text,
                        userName: userInfo.value.userName,
                        lastName: newPrenom.value.text,
                        birthdate: newBirthdate.value.text,
                        gender: gender.value,
                        firebaseToken: userInfo.value.firebaseToken,
                        number: newNumber.value.text));
                userInfo.value = await appSharedPreference.getUserInformation();
                log(r.message!);
                // Get.back();
                showCustomFlushbar(
                    Get.context!,
                    "Informations mise a jour avec succes",
                    ColorManager.succes,
                    FlushbarPosition.TOP);

                loadUserInfo.value = false;
              } else {
                log(r.toString());
              }
            },
          ),
        )
        .catchError((onError) {
      loadUserInfo.value = false;
      return null;
    });
  }

  @override
  void onInit() async {
    userInfo.value = await appSharedPreference.getUserInformation();
    newNom.value.text = userInfo.value.firstName ?? "";
    newPrenom.value.text = userInfo.value.lastName ?? "";
    newNumber.value.text = userInfo.value.number!;
    gender.value = userInfo.value.gender!;
    newNom.value.addListener(() {
      if (newNom.value.text != userInfo.value.firstName) {
        isModifyFirstName.value = true;
      } else {
        isModifyFirstName.value = false;
      }
    });
    newPrenom.value.addListener(() {
      if (newPrenom.value.text != userInfo.value.lastName) {
        isModifyLastName.value = true;
      } else {
        isModifyLastName.value = false;
      }
    });
    newNumber.value.addListener(() {
      if (newNumber.value.text != userInfo.value.number) {
        isModifyNumber.value = true;
      } else {
        isModifyNumber.value = false;
      }
    });
    newBirthdate.value.addListener(() {
      if (newBirthdate.value.text != userInfo.value.birthdate) {
        isModifyBirthdate.value = true;
      } else {
        isModifyBirthdate.value = false;
      }
    });

    super.onInit();
  }
}
