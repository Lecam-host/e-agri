import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../ressources/size_config.dart';
import '../ressources/styles_manager.dart';
import 'account_controller.dart';

class UpdatePasswordScreen extends StatefulWidget {
  const UpdatePasswordScreen({super.key});

  @override
  State<UpdatePasswordScreen> createState() => _UpdatePasswordScreenState();
}

class _UpdatePasswordScreenState extends State<UpdatePasswordScreen> {
  bool isObscure = true;
  final formKey = GlobalKey<FormState>();
  AccountController accountController = Get.put(AccountController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Modifier le mot de passe'),
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: const Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
        ),
      ),
      body: Form(
        key: formKey,
        child: Container(
          margin: const EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 10,
              ),
              const Text('Mot de passe actuel'),
              const SizedBox(
                height: 5,
              ),
              TextFormField(
                obscureText: isObscure,
                controller: accountController.oldPasswordController.value,
                decoration: InputDecoration(
                    // hintText:"La ",
                    label: Text(
                      "Mot de passe actuel",
                      style: getRegularTextStyle(color: Colors.grey),
                    ),
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          isObscure = !isObscure;
                        });
                      },
                      icon: Icon(
                        isObscure
                            ? Icons.remove_red_eye
                            : Icons.remove_red_eye_outlined,
                      ),
                    )),
              ),
              const SizedBox(
                height: 20,
              ),
              const Text('Nouveau de mot passe'),
              const SizedBox(
                height: 5,
              ),
              TextFormField(
                obscureText: true,
                controller: accountController.newPasswordController.value,
                decoration: InputDecoration(
                  // hintText:"La ",
                  label: Text(
                    "Nouveau de mot passe",
                    style: getRegularTextStyle(color: Colors.grey),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              const Text('Confimer le mot de passe'),
              const SizedBox(
                height: 5,
              ),
              TextFormField(
                obscureText: true,
                controller:
                    accountController.confirmNewPasswordController.value,
                decoration: InputDecoration(
                  // hintText:"La ",
                  label: Text(
                    "Confimer le mot de passe",
                    style: getRegularTextStyle(color: Colors.grey),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Obx(
                () => accountController.load.value
                    ? const LoaderComponent()
                    : SizedBox(
                        width: double.infinity,
                        height: getProportionateScreenHeight(56),
                        child: TextButton(
                          style: TextButton.styleFrom(
                            foregroundColor: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            backgroundColor: ColorManager.blue,
                          ),
                          onPressed: () async {
                            if (accountController.newPasswordController.value
                                    .text.isNotEmpty &&
                                accountController.confirmNewPasswordController
                                    .value.text.isNotEmpty &&
                                accountController.oldPasswordController.value
                                    .text.isNotEmpty) {
                              if (accountController
                                      .newPasswordController.value.text ==
                                  accountController.confirmNewPasswordController
                                      .value.text) {
                                await accountController.updatePassword();
                              } else {
                                showCustomFlushbar(
                                    context,
                                    "Les mot de passes ne correspondent pas",
                                    ColorManager.blue,
                                    FlushbarPosition.TOP);
                              }
                            } else {
                            
                            }
                          },
                          child: Text(
                            "Mettre à jour le mot de passe",
                            style: TextStyle(
                              fontSize: getProportionateScreenWidth(18),
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
