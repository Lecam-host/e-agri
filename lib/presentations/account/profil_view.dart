import 'dart:developer';

import 'package:eagri/presentations/account/account_controller.dart';
import 'package:eagri/presentations/common/date_field.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

import '../../app/functions.dart';
import '../../domain/model/user_model.dart';
import '../ressources/color_manager.dart';
import '../ressources/size_config.dart';

class ProfilView extends StatefulWidget {
  const ProfilView({super.key});

  @override
  State<ProfilView> createState() => _ProfilViewState();
}

class _ProfilViewState extends State<ProfilView> {
  @override
  Widget build(BuildContext context) {
    AccountController accountController = Get.put(AccountController());
    return Consumer<UserModel>(
      builder: (context, user, child) => Scaffold(
        appBar: AppBar(
          actions: const [
            // TextButton(
            //   child: const Row(
            //     children: [
            //       Icon(
            //         Icons.edit,
            //         color: Colors.grey,
            //         size: 15,
            //       ),
            //       Text(
            //         "Modifier",
            //         style: TextStyle(color: Colors.grey),
            //       ),
            //     ],
            //   ),
            //   onPressed: () {},
            // ),
          ],
          centerTitle: true,
          title: Text(
            'Modification du profil',
            style: TextStyle(
                fontSize: 15,
                fontStyle: FontStyle.normal,
                color: ColorManager.black),
          ),
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: const Icon(
              Icons.clear,
              color: Colors.black,
            ),
          ),
          backgroundColor: Colors.white,
          elevation: 0,
        ),
        bottomSheet: Padding(
          padding: const EdgeInsets.all(25),
          child: Obx(() => accountController.loadUserInfo.value
              ? const LoaderComponent()
              : SizedBox(
                  width: double.infinity,
                  height: getProportionateScreenHeight(56),
                  child: TextButton(
                    style: TextButton.styleFrom(
                        foregroundColor: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        backgroundColor: accountController
                                    .isModifyLastName.value ||
                                accountController.isModifyFirstName.value ||
                                accountController.isModifyNumber.value ||
                                accountController.gender.value != user.gender ||
                                accountController.isModifyBirthdate.value
                            ? ColorManager.blue
                            : Colors.grey),
                    onPressed: () async {
                      if (accountController.isModifyLastName.value ||
                          accountController.isModifyFirstName.value ||
                          accountController.isModifyBirthdate.value ||
                          accountController.gender.value != user.gender ||
                          accountController.isModifyNumber.value) {
                        inspect(user.birthdate);
                        await accountController.updateUserInfoModel();
                      }
                    },
                    child: Text(
                      "Enregistrer",
                      style: TextStyle(
                        fontSize: getProportionateScreenWidth(18),
                        color: Colors.white,
                      ),
                    ),
                  ),
                )),
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.only(left: 20, right: 20, bottom: 150),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 20),
              Center(
                child: Container(
                  height: 150,
                  width: 150,
                  decoration: const BoxDecoration(
                      shape: BoxShape.circle, color: Colors.grey),
                ),
              ),
              const SizedBox(height: 40),
              InputLabel(
                  label: "Nom de famille",
                  controller: accountController.newNom.value),
              const SizedBox(height: 20),
              InputLabel(
                  label: "Prénom",
                  controller: accountController.newPrenom.value),
              const SizedBox(height: 20),
              InputLabel(
                  label: "Numéro de téléphone",
                  controller: accountController.newNumber.value),
              const SizedBox(height: 20),
              const Text("Date de naissance"),
              const SizedBox(height: 5),
              DateField(
                  dateController: accountController.newBirthdate.value,
                  hintText: user.birthdate!.isNotEmpty
                      ? convertDate(user.birthdate.toString())
                      : "Aucune date de naissance"),
              const SizedBox(height: 20),
              const Text("Genre"),
              const SizedBox(height: 10),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: Colors.grey)),
                      child: Obx(() => RadioListTile(
                            value: "M",
                            groupValue: accountController.gender.value,
                            onChanged: (value) {
                              accountController.gender.value = value.toString();
                            },
                            title: const Text("Masculin"),
                          )),
                    ),
                  ),
                  const SizedBox(width: 20),
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: Colors.grey)),
                      child: Obx(() => RadioListTile(
                            value: "F",
                            groupValue: accountController.gender.value,
                            onChanged: (value) {
                              accountController.gender.value = value.toString();
                            },
                            title: const Text("Féminin"),
                          )),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class InputLabel extends StatelessWidget {
  final String label;
  final TextEditingController controller;
  // final String placeholder;
  const InputLabel({
    super.key,
    required this.label,
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(label),
        const SizedBox(height: 5),
        TextFormField(
          controller: controller,
          // validator: (value) => value!.isEmpty ? 'Entrer votre login' : null,
          decoration: InputDecoration(
            // prefixIcon: const Icon(Icons.numbers_outlined),
            filled: true,
            fillColor: ColorManager.white,
            // hintText: placeholder,
            // errorText: (snapshot.data ?? true)
            //     ? null
            //     : AppStrings.number,
          ),
        )
      ],
    );
  }
}
