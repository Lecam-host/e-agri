import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

import '../../../domain/model/user_model.dart';
import '../user_info_screen.dart';

class InfoUser extends StatelessWidget {
  const InfoUser({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
      builder: (context, user, child) => GestureDetector(
        onTap: () {
          ///
          Navigator.push(
            context,
            PageTransition(
              type: PageTransitionType.rightToLeft,
              child: const InfoPersoPage(),
              isIos: true,
              duration: const Duration(milliseconds: 400),
            ),
          );
        },
        child: Center(
          child: Column(
            children: [
              const CircleAvatar(
                radius: 50,
              ),
              const SizedBox(
                height: 10,
              ),
               Text(
                      "${user.firstName!.toUpperCase()} ${user.lastName!.toUpperCase()}",
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
              if (user.rate != null)
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                   
                   const SizedBox(width: 10,),
                    RatingBar.builder(
                      ignoreGestures: true,
                      initialRating: user.rate!,
                      minRating: 0,
                      direction: Axis.horizontal,
                      allowHalfRating: false,
                      itemCount: 5,
                      itemSize: 10,
                      itemPadding: const EdgeInsets.symmetric(horizontal: 0),
                      itemBuilder: (context, _) => const Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      onRatingUpdate: (rating) {},
                    ),
                    Text(
                      "(${user.rate!})",
                      style: getMeduimTextStyle(
                          color: ColorManager.grey, fontSize: 10),
                    ),
                  ],
                ),
              // Container(
              //   padding: const EdgeInsets.all(10),
              //   margin: const EdgeInsets.all(10),
              //   color: ColorManager.white,
              //   child: SingleChildScrollView(
              //     scrollDirection: Axis.horizontal,
              //     child: Row(
              //       crossAxisAlignment: CrossAxisAlignment.center,
              //       mainAxisAlignment: MainAxisAlignment.center,
              //       children: [
              //         statCard(
              //           "achatAvailable",
              //           userController.userStat.value.achatAvailable.toString(),
              //         ),
              //         statCard(
              //           "achatUnavailable",
              //           userController.userStat.value.achatUnavailable
              //               .toString(),
              //         ),
              //         statCard(
              //           "preAchatAvailable",
              //           userController.userStat.value.preAchatAvailable
              //               .toString(),
              //         ),
              //         statCard(
              //           "preAchatUnavailable",
              //           userController.userStat.value.preAchatUnavailable
              //               .toString(),
              //         )
              //       ],
              //     ),
              //   ),
              // ),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Container statCard(String titre, String data) => Container(
        margin: const EdgeInsets.all(5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              data,
              style: getBoldTextStyle(fontSize: 14),
            ),
            Text(
              titre,
              style:
                  getRegularTextStyle(fontSize: 12, color: ColorManager.grey),
            ),
          ],
        ),
      );
}
