// ignore_for_file: use_build_context_synchronously

import 'dart:developer';

import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/app/app_prefs.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/historique/historique_view.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:eagri/presentations/shop/controller/shop_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../app/constant.dart';
import '../../app/di.dart';
import '../../app/functions.dart';
import '../../controllers/user_controller.dart';
import '../../data/repository/api/repository_impl.dart';
import '../../domain/model/user_model.dart';
import '../avis/vos_avis_screen.dart';
import '../command/list_command_screen.dart';
import '../command/mes_produit_commander/mes_produits_commannder_view.dart';
import '../common/flushAlert_componenent.dart';
import '../demande/demande_screen.dart';
import '../favorite/list_favorite_screen.dart';
import '../notifications/controller/notification_controller.dart';
import '../product_odered_in_credit/product_odered_in_credit_view.dart';
import '../ressources/routes_manager.dart';
import 'components/info_user.dart';

class AccountPageView extends StatefulWidget {
  const AccountPageView({Key? key}) : super(key: key);

  @override
  State<AccountPageView> createState() => _AccountPageViewState();
}

class _AccountPageViewState extends State<AccountPageView> {
  AppSharedPreference appSharedPreference = instance<AppSharedPreference>();
  //CommandCrontroller commandCrontroller = Get.put(CommandCrontroller());
  ShopController shopController = Get.put(ShopController());
  //CatalogueController catalogueController = Get.put(CatalogueController());
  UserController userController = Get.put(UserController());
  NotificationController notificationController = Get.find();
  final Uri _url = Uri.parse(Constant.confidentialiteUrl);
  Future<void> _launchUrl() async {
    if (!await launchUrl(_url)) {
      throw Exception('page indisponible');
    }
  }

  @override
  void initState() {
    userController.updateUserInfo(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
        builder: (context, user, child) => Scaffold(
              appBar: AppBar(
                automaticallyImplyLeading: false,
                title: Center(
                  child: Text(
                    'Mon compte',
                    style: TextStyle(
                        fontSize: 15,
                        fontStyle: FontStyle.normal,
                        color: ColorManager.black),
                  ),
                ),
                backgroundColor: Colors.white,
                elevation: 0,
              ),
              body: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (user.isConnected == true) ...[
                      const SizedBox(
                        height: 10,
                      ),
                      const InfoUser(),
                      const SizedBox(
                        height: 10,
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 10),
                        child: Text(
                          "Mon compte e-agri",
                          style: getBoldTextStyle(
                            color: ColorManager.grey,
                          ),
                        ),
                      ),

                      if (hasAtLeastOneScope([
                        UserPermissions.CATALOGUE_INTRANT_VOIR,
                        UserPermissions.CATALOGUE_PRESTATION_VOIR,
                        UserPermissions.CATALOGUE_PRODUIT_AGRICOLE_VOIR,
                        UserPermissions.CATALOGUE_TRANSPORT_VOIR
                      ], user.scopes)) ...[
                        const SizedBox(
                          height: 10,
                        ),
                        ListMenuSetting(
                          onTap: () {
                            Navigator.pushNamed(
                                context, Routes.catalogueProduit);
                          },
                          iconData: Icons.person_outlined,
                          text: "Mon catalogue",
                        ),
                      ],
                      if (user.isConnected != false)
                        Stack(
                          children: [
                            ListMenuSetting(
                              onTap: () {
                                Navigator.pushNamed(
                                    context, Routes.notification);
                              },
                              iconData: Icons.notifications,
                              text: "Notifications",
                            ),
                            if (notificationController.nbNotifNotRead > 0)
                              Positioned(
                                left: 50,
                                top: 10,
                                child: CircleAvatar(
                                  backgroundColor: ColorManager.red,
                                  radius: 5,
                                ),
                              ),
                          ],
                        ),
                      if (user.scopes != null)
                        if (hasAtLeastOneScope([
                              UserPermissions
                                  .HISTORIQUETRANSCATION_HISTORIQUEVENTE_VOIR,
                              UserPermissions
                                  .HISTORIQUETRANSCATION_HISTORIQUEACHAT_VOIR
                            ], user.scopes!) ==
                            true)
                          ListMenuSetting(
                            onTap: () {
                              Get.to(const HistoriqueView());
                            },
                            iconData: Icons.history,
                            text: "Historiques achats et ventes",
                          ),
                      // ListMenuSetting(
                      //   onTap: () {
                      //     // Get.to(const HistoriqueView());
                      //   },
                      //   iconData: Icons.history,
                      //   text: "Mes notifications",
                      // ),
                      if (isInScopes(
                          UserPermissions.COMMANDE_VOIR, user.scopes))
                        ListMenuSetting(
                          onTap: () {
                            Get.to(const ListCommandScreen());
                          },
                          iconData: Icons.shopping_bag_outlined,
                          text: "Mes commandes",
                        ),
                      ListMenuSetting(
                        onTap: () {
                          Get.to(const MesProduitsCommanderView());
                        },
                        iconData: Icons.shopping_bag_outlined,
                        text: "Mes produits commandés",
                      ),
                      ListMenuSetting(
                        onTap: () {
                          Get.to(const ProductsOderedInCreditView());
                        },
                        iconData: Icons.shopping_bag_outlined,
                        text: "Credit",
                      ),
                      ListMenuSetting(
                        onTap: () async {
                          Get.to(const DemandeScreen());
                        },
                        iconData: Icons.search,
                        text: "Mes demandes",
                      ),

                      ListMenuSetting(
                        onTap: () {
                          Get.to(const ListFavoriteScreen());
                        },
                        iconData: Icons.favorite_outline,
                        text: "Mes favoris",
                      ),

                      ListMenuSetting(
                        onTap: () {
                          Get.to(const VosAvisScreen());
                        },
                        iconData: Icons.comment,
                        text: "Notes en attente",
                      ),
                    ],
                    // if (user.isConnected == true)
                    //   Container(
                    //     margin: const EdgeInsets.only(left: 10),
                    //     child: Text(
                    //       "Mon profil",
                    //       style: getBoldTextStyle(
                    //         color: ColorManager.grey,
                    //       ),
                    //     ),
                    //   ),
                    // const SizedBox(
                    //   height: 10,
                    // ),
                    // if (user.isConnected == true)
                    //   ListMenuSetting(
                    //     onTap: () {},
                    //     iconData: Icons.person_outlined,
                    //     text: "Informations personnelles",
                    //   ),
                    // ListMenuSetting(
                    //   onTap: () {},
                    //   withDivider: true,
                    //   iconData: Icons.key,
                    //   text: "Confidentialités",
                    // ),
                    // ListMenuSetting(
                    //   onTap: () {},
                    //   iconData: Icons.info_outlined,
                    //   text: "A-propos",
                    // ),
                    // const SizedBox(
                    //   height: 10,
                    // ),
                    // ListMenuSetting(
                    //   onTap: () {
                    //     showDialog(
                    //         context: context,
                    //         barrierDismissible: false,
                    //         builder: (context) {
                    //           return AlertDialog(
                    //             backgroundColor: Colors.white,
                    //             contentPadding: const EdgeInsets.all(12),
                    //             content: const Column(
                    //               mainAxisSize: MainAxisSize.min,
                    //               children: [
                    //                 Text(
                    //                   'Appeler le service client',
                    //                   textAlign: TextAlign.center,
                    //                 ),
                    //                 Text(
                    //                   "Service",
                    //                   textAlign: TextAlign.center,
                    //                   style: TextStyle(
                    //                       fontWeight: FontWeight.bold),
                    //                 ),
                    //               ],
                    //             ),
                    //             title: const Icon(Icons.phone,
                    //                 color: Colors.blue, size: 40),
                    //             actions: [
                    //               TextButton(
                    //                 onPressed: () {
                    //                   Navigator.pop(context);
                    //                 },
                    //                 child: const Text('Annuler',
                    //                     style: TextStyle(color: Colors.grey)),
                    //               ),
                    //               TextButton(
                    //                 onPressed: () async {},
                    //                 child: const Text('Appeler',
                    //                     style: TextStyle(color: Colors.green)),
                    //               ),
                    //             ],
                    //           );
                    //         });
                    //   },
                    //   withDivider: true,
                    //   iconData: Icons.phone,
                    //   text: "Nous contacter",
                    // ),
                    // const SizedBox(
                    //   height: 10,
                    // ),
                    if (user.isConnected == false)
                      Container(
                        height: 45,
                        width: double.infinity,
                        margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: ElevatedButton(
                          onPressed: () {
                            Navigator.pushNamed(context, Routes.loginRoute);
                          },
                          style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.resolveWith((states) {
                                if (states.contains(MaterialState.pressed)) {
                                  return ColorManager.primary.withOpacity(0.5);
                                }
                                return ColorManager.primary;
                              }),
                              shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ))),
                          child: const Text(
                            "Se connecter",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    const SizedBox(
                      height: 10,
                    ),
                    ListMenuSetting(
                      onTap: () {
                        _launchUrl();
                      },
                      iconData: Icons.key,
                      text: "Confidentialité",
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    if (user.isConnected == true)
                      ListMenuSetting(
                        onTap: () {
                          showDialog(
                              context: context,
                              barrierDismissible: false,
                              builder: (context) {
                                return AlertDialog(
                                  backgroundColor: Colors.white,
                                  contentPadding: const EdgeInsets.all(12),
                                  actions: [
                                    TextButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      child: Text('Annuler',
                                          style: getBoldTextStyle(
                                              color: ColorManager.blue)),
                                    ),
                                    TextButton(
                                      onPressed: () async {
                                        Get.defaultDialog(
                                          title: "",
                                          content: const Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              LoaderComponent(),
                                              SizedBox(
                                                height: 5,
                                              ),
                                              Text('Deconnexion en cours ...')
                                            ],
                                          ),
                                        );
                                        RepositoryImpl repositoryImpl =
                                            instance<RepositoryImpl>();
                                        log(user.firebaseToken!);
                                        await repositoryImpl
                                            .deleteFirebaseToken(user.userName!,
                                                user.firebaseToken!)
                                            .then((response) {
                                          response.fold((l) {
                                            Get.back();
                                            showCustomFlushbar(
                                                context,
                                                l.message,
                                                ColorManager.error,
                                                FlushbarPosition.TOP);
                                          }, (r) async {
                                            //
                                            await appSharedPreference
                                                .logoutUser(context)
                                                .then((value) async {
                                              // Navigator.pop(context);
                                              await instance.reset();
                                              await initAppModule();

                                              Get.back();
                                              Navigator.pop(context);

                                              showCustomFlushbar(
                                                  context,
                                                  "Déconnecté",
                                                  ColorManager.succes,
                                                  FlushbarPosition.TOP);
                                            });
                                          });
                                        });
                                      },
                                      child: Text('Se deconnecter',
                                          style: getBoldTextStyle(
                                              color: ColorManager.red)),
                                    )
                                  ],
                                  content: const Text(
                                    'Voulez-vous vraiment vous déconnecter ?',
                                    textAlign: TextAlign.center,
                                  ),
                                  title: const Icon(Icons.logout_outlined,
                                      color: Colors.red, size: 40),
                                );
                              });
                        },
                        iconData: Icons.logout,
                        text: "Se déconnecter",
                      ),
                  ],
                ),
              ),
            ));
  }

  // showCustomFlushbar() {
  //   return Flushbar(
  //     maxWidth: 200,
  //     flushbarPosition: FlushbarPosition.TOP,
  //     duration: const Duration(seconds: 3),
  //     margin: const EdgeInsets.all(10),
  //     padding: const EdgeInsets.all(10),
  //     borderRadius: BorderRadius.circular(15),
  //     backgroundColor: ColorManager.primary,
  //     icon: Icon(Icons.verified, color: ColorManager.white),
  //     boxShadows: const [
  //       BoxShadow(
  //         color: Colors.black45,
  //         offset: Offset(3, 3),
  //         blurRadius: 3,
  //       ),
  //     ],
  //     dismissDirection: FlushbarDismissDirection.HORIZONTAL,
  //     forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
  //     // title: '',
  //     message: "Déconnexion",
  //   ).show(context);
  // }
}

class ListMenuSetting extends StatelessWidget {
  const ListMenuSetting(
      {Key? key,
      required this.text,
      required this.iconData,
      this.onTap,
      this.color,
      this.withDivider})
      : super(key: key);
  final String text;
  final IconData iconData;
  final Function()? onTap;
  final bool? withDivider;
  final Color? color;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 10, right: 10),
      color: Colors.white,
      child: Column(
        children: [
          GestureDetector(
            onTap: onTap,
            child: ListTile(
                trailing: Icon(
                  Icons.arrow_right_rounded,
                  color: ColorManager.primary,
                ),
                title: Text(text,
                    style: getSemiBoldTextStyle(color: ColorManager.black)),
                leading: Container(
                  width: 30.0,
                  height: 30.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: ColorManager.primary.withOpacity(0.1),
                  ),
                  child: Icon(
                    iconData,
                    color: ColorManager.primary,
                  ),
                )),
          ),
          if (withDivider == true)
            const Divider(
              height: 1,
            ),
        ],
      ),
    );
  }
}
