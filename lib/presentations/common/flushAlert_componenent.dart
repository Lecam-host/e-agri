// ignore_for_file: file_names

import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';

import '../ressources/color_manager.dart';

enum FlushbarCustomState { success, error, warning }

showCustomFlushbar(
  BuildContext context,
  String message,
  Color color,
  FlushbarPosition flushbarPosition, {
  int? duration,
  Function()? callback,
}) {
  return Flushbar(
    maxWidth: 300,
    flushbarPosition: flushbarPosition,
    duration: Duration(seconds: duration ?? 5),
    margin: const EdgeInsets.all(10),
    padding: const EdgeInsets.all(10),
    borderRadius: BorderRadius.circular(15),
    backgroundColor: color,
    icon: Icon(Icons.info, color: ColorManager.white),
    onStatusChanged: (status) {
      if (status == FlushbarStatus.DISMISSED) {
        if (callback != null) {
          callback();
        }
      }
    },
    boxShadows: const [
      BoxShadow(
        color: Colors.black45,
        offset: Offset(3, 3),
        blurRadius: 3,
      ),
    ],
    dismissDirection: FlushbarDismissDirection.HORIZONTAL,
    forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
    // title: '',
    message: message,
  ).show(context);
}
