import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/font_manager.dart';
import 'package:eagri/presentations/ressources/strings_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:eagri/presentations/ressources/values_manager.dart';
import 'package:flutter/material.dart';

import '../../ressources/assets_manager.dart';

class SearchEmptyComponent extends StatelessWidget {
  const SearchEmptyComponent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: const EdgeInsets.all(AppMargin.m10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              ImageAssets.search,
              width: 150,
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              AppStrings.searchEmpty,
              style: getMeduimTextStyle(
                color: ColorManager.error,
                fontSize: FontSize.s25,
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
