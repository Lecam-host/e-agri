import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/default_button.dart';
import 'package:eagri/presentations/common/state/success_component.dart';
import 'package:flutter/material.dart';

class SuccessPageView extends StatelessWidget {
  const SuccessPageView(
      {Key? key,
      this.message,
      this.icon,
      this.titreAction,
      this.action,
      this.subMessage})
      : super(key: key);
  final String? message;
  final String? subMessage;

  final String? icon;
  final String? titreAction;

  final Function? action;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(leading: const BackButtonCustom()),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SuccessComponenent(
            message: message ?? "",
            icon: icon,
          ),
          if (subMessage != null)
            Container(
              margin: const EdgeInsets.all(10),
              child: Text(
                subMessage!,
                textAlign: TextAlign.center,
              ),
            ),
          if (action != null)
            Container(
              margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              child: DefaultButton(
                press: action,
                text: titreAction ?? "",
              ),
            ),
        ],
      ),
    );
  }
}
