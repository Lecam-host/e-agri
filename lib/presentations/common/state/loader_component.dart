import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../ressources/assets_manager.dart';
import '../../ressources/values_manager.dart';

class LoaderComponent extends StatelessWidget {
  const LoaderComponent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        height: AppSize.s100,
        width: AppSize.s100,
        child: Lottie.asset(JsonAssets.loader),
      ),
    );
  }
}
