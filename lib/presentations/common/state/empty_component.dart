import 'package:eagri/presentations/ressources/assets_manager.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../ressources/values_manager.dart';

class EmptyComponenent extends StatelessWidget {
  const EmptyComponenent({Key? key, this.message, this.icon}) : super(key: key);
  final String? message;
  final String? icon;
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: SizedBox(
                height: AppSize.s150,
                width: AppSize.s150,
                child: Lottie.asset(
                  icon ?? JsonAssets.empty,
                ),
              ),
            ),
            Text(
              message ?? "",
              style: getRegularTextStyle(
                color: ColorManager.black,
                fontSize: 20,
              ),
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }
}
