import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../ressources/assets_manager.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/styles_manager.dart';

class NotExistProductComponent extends StatelessWidget {
  const NotExistProductComponent({super.key,this.icon,this.message});
final String? icon;
final String? message;
  @override
  Widget build(BuildContext context) {
    return  Align(
    alignment: Alignment.center,
    child: Container(
      padding: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: SizedBox(
              height: 150,
              width: 150,
              child: Lottie.asset(icon ?? JsonAssets.empty),
            ),
          ),
          Text(
            message ?? "Ce produit n'est plus disponible",
            style: getRegularTextStyle(
              color: ColorManager.black,
              fontSize: 20,
            ),
            textAlign: TextAlign.center,
          )
        ],
      ),
    ),
    );
  }
}