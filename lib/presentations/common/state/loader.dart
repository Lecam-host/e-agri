import 'package:eagri/presentations/ressources/assets_manager.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../ressources/values_manager.dart';

loaderDialog(BuildContext ctx, {String? text, String? icon}) {
  showDialog(
      context: ctx,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          backgroundColor: Colors.white,
          contentPadding: const EdgeInsets.all(12),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(
                height: AppSize.s100,
                width: AppSize.s100,
                child: Lottie.asset(icon ?? JsonAssets.loader),
              ),
              if (text != null) Text(text),
              const SizedBox(
                height: 20,
              ),
              //const LoadingWidget()
            ],
          ),
        );
      });
}
