import 'package:flutter/material.dart';

import 'buttons/back_button.dart';
import 'pdf_screen.dart';

class DocComponentView extends StatefulWidget {
  const DocComponentView({Key? key, this.link}) : super(key: key);
  final String? link;
  @override
  State<DocComponentView> createState() => _DocComponentViewState();
}

class _DocComponentViewState extends State<DocComponentView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: const BackButtonCustom(),
        ),
        body: PdfScreenn(
          pdfLink: widget.link!,
        ));
  }
}
