import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';

class CacheNetworkImage extends StatelessWidget {
  const CacheNetworkImage({Key? key, this.image}) : super(key: key);
  final String? image;
  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: image!,
      imageBuilder: (context, imageProvider) => Container(
        width: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
            scale: 1,
            image: imageProvider,
            fit: BoxFit.cover,
          ),
        ),
      ),
      placeholder: (context, url) => Container(
        height: double.infinity,
        width: double.infinity,
        color: ColorManager.grey1.withOpacity(0.2),
      ),
      errorWidget: (context, url, error) => Container(
        height: double.infinity,
        width: double.infinity,
        color: ColorManager.grey1.withOpacity(0.2),
      ),
    );
  }
}
