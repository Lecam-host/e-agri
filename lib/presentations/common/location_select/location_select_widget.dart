// ignore_for_file: must_be_immutable

import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:select_form_field/select_form_field.dart';

import '../../../app/di.dart';
import '../../../domain/usecase/location_usecase.dart';

class LocationSelectWidget extends StatefulWidget {
  LocationSelectWidget({Key? key, required this.locationReturn})
      : super(key: key);
  LocationReturnModel locationReturn;
  @override
  State<LocationSelectWidget> createState() => _LocationSelectWidgetState();
}

class _LocationSelectWidgetState extends State<LocationSelectWidget> {
  TextEditingController regionController = TextEditingController();
  TextEditingController departementController = TextEditingController();
  TextEditingController sousPrefectureController = TextEditingController();
  TextEditingController localiteController = TextEditingController();
  List<Map<String, dynamic>> listeDepartementMap = [];
  List<Map<String, dynamic>> listRegionMap = [];
  List<Map<String, dynamic>> listeSousPrefectureMap = [];
  List<Map<String, dynamic>> listLocaliteMap = [];
  LocationReturnModel locationReturn = LocationReturnModel();
  LocationUsecase locationUsecase = instance<LocationUsecase>();
  @override
  void initState() {
    super.initState();
    getRegion();
  }

  disposeElement() {
    regionController.clearComposing();
    departementController.clearComposing();
    sousPrefectureController.clearComposing();
    localiteController.clearComposing();
  }

  getRegion() async {
    await locationUsecase
        .getListRegion()
        .then((response) => response.fold((failure) {}, (result) {
              listRegionMap = result.data
                  .map((element) =>
                      {'value': element.regionId, 'label': element.name})
                  .toList();
              setState(() {
                listRegionMap;
              });
            }));
  }

  fetchDepartement(int idRegion) async {
    await locationUsecase
        .getListDepartement(idRegion)
        .then((response) => response.fold(
              (l) => null,
              (r) {
                if (r.listDepartement!.isNotEmpty) {
                  listeDepartementMap = r.listDepartement!
                      .map((element) => {
                            "value": element.departementId,
                            "label": element.name
                          })
                      .toList();
                  setState(() {
                    listeDepartementMap;
                  });
                }
              },
            ));
  }

  fetchSpFectureOfDepartement(int idDepartement) async {
    await locationUsecase
        .getListSpOfDepartement(idDepartement)
        .then((response) => response.fold(
              (l) => null,
              (r) {
                if (r.listSousPrefecture!.isNotEmpty) {
                  setState(() {
                    listeSousPrefectureMap = r.listSousPrefecture!
                        .map((element) => {
                              "value": element.sousPrefectureId,
                              "label": element.name
                            })
                        .toList();
                  });
                }
              },
            ));
  }

  fecthLocalite(int idSp) async {
    (await locationUsecase.getListLocaliteOfSp(idSp)).fold(
      (l) => null,
      (r) {
        if (r.listLocalite!.isNotEmpty) {
          setState(() {
            listLocaliteMap = r.listLocalite!
                .map((element) =>
                    {"value": element.localiteId, "label": element.name})
                .toList();
          });
        }
      },
    );
  }

  @override
  void dispose() {
    disposeElement();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SelectFormField(
          type: SelectFormFieldType.dialog,
          enableSearch: true, // or can be dialog
          //initialValue: 'circle',
          controller: regionController,
          labelText: 'Choisir la region',
          items: listRegionMap,
          onChanged: (val) {
            if (val.isNotEmpty) {
              setState(() {
                widget.locationReturn.region = LocationObject(
                  id: int.parse(val),
                );
                departementController.clearComposing();
                widget.locationReturn.departement = null;

                sousPrefectureController.clearComposing();
                localiteController.clearComposing();

                widget.locationReturn.sprefecture = null;
                widget.locationReturn.localite = null;
              });
              // setState(() {

              //  });
              fetchDepartement(widget.locationReturn.region!.id);
            }
            //
          },
        ),
        const SizedBox(
          height: 30,
        ),
        listeDepartementMap.isEmpty
            ? const SizedBox()
            : Column(
                children: [
                  SelectFormField(
                    controller: departementController,
                    type: SelectFormFieldType.dropdown, // or can be dialog
                    labelText: "Choisir le departement",
                    items: listeDepartementMap,
                    onChanged: (val) {
                      if (val.isNotEmpty) {
                        setState(() {
                          widget.locationReturn.departement = LocationObject(
                            id: int.parse(val),
                          );

                          sousPrefectureController.clearComposing();
                          widget.locationReturn.sprefecture = null;
                          localiteController.clearComposing();
                          widget.locationReturn.localite = null;
                        });

                        // departement = val;
                        fetchSpFectureOfDepartement(
                            widget.locationReturn.departement!.id);
                      }
                    },
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                ],
              ),
        listeSousPrefectureMap.isEmpty
            ? const SizedBox()
            : Column(
                children: [
                  SelectFormField(
                    controller: sousPrefectureController,
                    type: SelectFormFieldType.dropdown, // or can be dialog
                    labelText: "Choisir la sous-prefercture",
                    items: listeSousPrefectureMap,
                    onChanged: (val) {
                      if (val.isNotEmpty) {
                        setState(() {
                          widget.locationReturn.sprefecture = LocationObject(
                            id: int.parse(val),
                          );
                          localiteController.clearComposing();
                          widget.locationReturn.localite = null;
                        });

                        fecthLocalite(widget.locationReturn.sprefecture!.id);
                      }
                    },
                  ),
                  const SizedBox(
                    height: 30,
                  )
                ],
              ),
        listLocaliteMap.isEmpty
            ? const SizedBox()
            : SelectFormField(
                controller: localiteController,
                onChanged: (val) {
                  if (val.isNotEmpty) {
                    setState(() {
                      widget.locationReturn.localite = LocationObject(
                        id: int.parse(val),
                      );
                    });
                    inspect(widget.locationReturn);
                  }
                  // localite = val;
                },
                type: SelectFormFieldType.dropdown, // or can be dialog
                labelText: "Choisir la localité",
                items: listLocaliteMap,
              ),
      ],
    );
  }
}

class LocationReturnModel {
  LocationObject? region;
  LocationObject? departement;
  LocationObject? sprefecture;
  LocationObject? localite;
  LocationReturnModel(
      {this.departement, this.region, this.localite, this.sprefecture});
}

class LocationObject {
  int id;
  String? name;

  LocationObject({required this.id, this.name});
}
