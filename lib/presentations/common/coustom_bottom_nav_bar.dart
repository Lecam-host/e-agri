import 'dart:developer';

import 'package:eagri/presentations/home/controllers/home_controller.dart';
import 'package:eagri/presentations/ressources/assets_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:eagri/presentations/shop/controller/shop_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

import '../../app/constant.dart';
import '../../app/functions.dart';
import '../../domain/model/user_model.dart';
import '../ressources/color_manager.dart';
import '../ressources/routes_manager.dart';
import 'enums.dart';

Color inActiveIconColor = ColorManager.grey;
Color activeIconColor = ColorManager.primary;

class CustomBottomNavBar extends StatelessWidget {
  const CustomBottomNavBar({
    Key? key,
    required this.selectedMenu,
  }) : super(key: key);

  final MenuState selectedMenu;

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
      builder: (context, user, child) => Container(
        height: 80,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: const Color.fromARGB(255, 255, 255, 255),
          boxShadow: [
            BoxShadow(
              offset: const Offset(0, -1),
              blurRadius: 10,
              color: const Color.fromARGB(255, 111, 111, 111).withOpacity(0.15),
            ),
          ],
          borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(10), topRight: Radius.circular(10)),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            iconButton(
              context,
              SvgManager.home,
              "Accueil",
              MenuState.home,
              Routes.homeRoute,
              SvgManager.homeOutl,
            ),
            iconButton(
              context,
              SvgManager.priceTag,
              "Prix",
              MenuState.price,
              Routes.marketRoute,
              SvgManager.priceTagOutl,
            ),
            if (isInScopes(
                        UserPermissions.SHOW_PRODUCT_AGRICOLE, user.scopes??[]) ==
                    true ||
                isInScopes(
                        UserPermissions.SHOW_PRODUCT_INTRANT, user.scopes??[]) ==
                    true)
              iconButton(
                context,
                SvgManager.shop,
                "Marché",
                MenuState.shop,
                Routes.shop,
                SvgManager.shopOutl,
              ),
            iconButton(
              context,
              SvgManager.user,
              "Compte",
              MenuState.profile,
              Routes.profil,
              SvgManager.userOutl,
            ),
            // IconButton(
            //   icon: SvgPicture.asset("assets/icons/Chat bubble Icon.svg"),
            //   onPressed: () {},
            // ),
          ],
        ),
      ),
    );
  }

  Widget iconButton(
    BuildContext context,
    String icon,
    String titre,
    MenuState menuState,
    String route,
    String iconActif,
  ) {
    final HomeController homeController = Get.put(HomeController());
    final ShopController shopController = Get.put(ShopController());

    return Obx(() {
      return InkWell(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SvgPicture.asset(
              homeController.pageHomeIndex.value == menuState.index
                  ? iconActif
                  : icon,
              colorFilter: ColorFilter.mode(
                  homeController.pageHomeIndex.value == menuState.index
                      ? activeIconColor
                      : inActiveIconColor,
                  BlendMode.srcIn),
              width: 20,
            ),
            Text(
              titre,
              style: homeController.pageHomeIndex.value == menuState.index
                  ? getSemiBoldTextStyle(
                      color:
                          homeController.pageHomeIndex.value == menuState.index
                              ? activeIconColor
                              : inActiveIconColor,
                      fontSize: 12)
                  : getLightTextStyle(
                      color:
                          homeController.pageHomeIndex.value == menuState.index
                              ? activeIconColor
                              : inActiveIconColor,
                      fontSize: 12,
                    ),
            )
          ],
        ),
        onTap: () {
          if (shopController.isFilter.value == true) {
            shopController.resetFilter();
          }

          log(menuState.index.toString());
          homeController.pageHomeIndex.value = menuState.index;
        },
      );
    });
  }
}
