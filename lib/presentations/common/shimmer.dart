import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import '../ressources/color_manager.dart';

Shimmer shimmerProductCustom() {
  return Shimmer.fromColors(
    baseColor: const Color.fromARGB(255, 175, 175, 175),
    highlightColor: const Color.fromARGB(255, 213, 213, 213),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 10, left: 10),
          width: 150,
          height: 130,
          decoration: BoxDecoration(
            color: ColorManager.white,
            borderRadius: BorderRadius.circular(15),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(bottom: 10, left: 10),
          width: 130,
          height: 10,
          decoration: BoxDecoration(
            color: ColorManager.white,
            borderRadius: BorderRadius.circular(15),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(bottom: 10, left: 10),
          width: 110,
          height: 10,
          decoration: BoxDecoration(
            color: ColorManager.white,
            borderRadius: BorderRadius.circular(15),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(bottom: 10, left: 10),
          width: 150,
          height: 10,
          decoration: BoxDecoration(
            color: ColorManager.white,
            borderRadius: BorderRadius.circular(15),
          ),
        ),
      ],
    ),
  );
}
