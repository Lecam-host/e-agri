import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';

import '../../app/constant.dart';
import '../ressources/size_config.dart';

class DefaultButton extends StatelessWidget {
  const DefaultButton({
    Key? key,
    required this.text,
    this.press,
    this.color,
    this.isActive = true,
  }) : super(key: key);
  final String text;
  final Function? press;
  final bool? isActive;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: getProportionateScreenHeight(56),
      child: TextButton(
        style: TextButton.styleFrom(
          foregroundColor: Colors.white,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          backgroundColor:
              color ?? (isActive == true ? kPrimaryColor : ColorManager.grey),
        ),
        onPressed: press as void Function()?,
        child: Text(
          text,
          style: TextStyle(
            fontSize: getProportionateScreenWidth(18),
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
