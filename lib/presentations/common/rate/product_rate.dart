import 'package:eagri/domain/model/Product.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';

import 'controller/rate_controller.dart';

class ProductRate extends StatefulWidget {
  const ProductRate({Key? key, required this.product}) : super(key: key);
  final ProductModel product;

  @override
  State<ProductRate> createState() => _ProductRateState();
}

class _ProductRateState extends State<ProductRate> {
  var rateController = Get.put(RateController());
  final _focusNode = FocusNode();
  @override
  void initState() {
    rateController.personneNoteId.value = widget.product.fournisseurId;
    rateController.resourceIdObs.value = widget.product.id;
    super.initState();
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RatingBar.builder(
      initialRating: widget.product.rating,
      minRating: 0,
      direction: Axis.horizontal,
      allowHalfRating: true,
      itemCount: 5,
      itemSize: 15,
      itemPadding: const EdgeInsets.symmetric(horizontal: 1),
      itemBuilder: (context, _) => const Icon(
        Icons.star,
        color: Colors.amber,
      ),
      onRatingUpdate: (rating) {
        rateController.rating.value = rating.toInt();
      },
    );
  }
}
