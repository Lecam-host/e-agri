import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/domain/usecase/rate_usecase.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../app/app_prefs.dart';
import '../../../../app/di.dart';
import '../../../../data/request/notation_request.dart';
import '../../../../domain/model/rate_model.dart';
import '../../../../domain/model/user_model.dart';
import 'package:eagri/data/mapper/rate_mapper.dart';

import '../../../../domain/usecase/shop_usecase.dart';

class RateController extends GetxController {
  RateUseCase rateUseCase = instance<RateUseCase>();
  ShopUseCase shopUseCase = instance<ShopUseCase>();
  AppSharedPreference appSharedPreference = instance<AppSharedPreference>();
  UserModel userInfo = UserModel();
  var listComments = <RateAndCommentItem>[].obs;
  final isLoad = false.obs;

  var rating = 0.obs;
  var resourceIdObs = 0.obs;
  var personneNoteId = 0.obs;
  final commantaire = TextEditingController().obs;
  Future<bool> ratingRessource(String codeService) async {
    Get.back();
    Get.dialog(const LoaderComponent());
    bool isNoted = false;
    await appSharedPreference.getUserInformation().then((value) async {
      AddRateRequest request = AddRateRequest(
        note: rating.value,
        noteurId: value.id!,
        resourceId: resourceIdObs.value,
        userId: personneNoteId.value,
        codeService: codeService,
        comment: commantaire.value.text,
      );

      await rateUseCase.addRate(request).then((response) async {
        response.fold((l) {
          Get.back();
          showCustomFlushbar(Get.context!, "Echec veuillez réessayer",
              ColorManager.error, FlushbarPosition.TOP);
        }, (r) {
          isNoted = true;
          Get.back();
          showCustomFlushbar(Get.context!, "Merci d'avoir noté ce produit",
              ColorManager.succes, FlushbarPosition.TOP);
        });
      });
      commantaire.value.clear();

      personneNoteId.value = 0;
      rating.value = 0;
      resourceIdObs.value = 0;
      // CheckRateRequest checkRateRequest = CheckRateRequest(
      //   userId: value.id!,
      //   ressourceId: resourceIdObs.value,
      //   codeService: int.parse(codeService),
      // );
      // inspect(checkRateRequest);
      // await rateUseCase.checkAlreadyRated(checkRateRequest);
    });
    return isNoted;
  }

  getProductComment(ProductListCommentRequest request) async {
    listComments.value = [];
    await rateUseCase
        .getProductComment(request)
        .then((response) => response.fold((failure) => null, (comments) async {
              await Future.forEach(comments.data.items,
                  (RateAndCommentItem element) async {
                listComments.add(await element.toDomain());
              });
            }));
  }
}
