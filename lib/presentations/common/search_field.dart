import 'package:flutter/material.dart';

import '../../app/constant.dart';
import '../ressources/size_config.dart';

class SearchField extends StatelessWidget {
  const SearchField({
    this.hintText,
    this.onChanged,
    Key? key,
  }) : super(key: key);
  final String? hintText;
  final Function(String)? onChanged;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        color: kSecondaryColor.withOpacity(0.1),
        borderRadius: BorderRadius.circular(15),
      ),
      child: TextField(
        onChanged: onChanged,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(20),
                vertical: getProportionateScreenWidth(9)),
            border: InputBorder.none,
            focusedBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
            hintText: hintText,
            prefixIcon: const Icon(Icons.search)),
      ),
    );
  }
}
