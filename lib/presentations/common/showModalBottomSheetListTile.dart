// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import '../ressources/color_manager.dart';
import '../ressources/icon_manager.dart';
import '../ressources/styles_manager.dart';

ListTile showModalBottomSheetListTile({
  required String titre,
  required IconData icon,
  Widget? widget,
  required BuildContext context,
}) {
  return ListTile(
    leading: Icon(
      icon,
      color: ColorManager.black,
    ),
    title: Text(
      titre,
      style: getSemiBoldTextStyle(
        color: ColorManager.black,
      ),
    ),
    trailing: Icon(IconManager.arrowRight, color: ColorManager.black),
    onTap: () {
      Navigator.push(
        context,
        PageTransition(
          type: PageTransitionType.bottomToTop,
          child: widget!,
          isIos: true,
          duration: const Duration(milliseconds: 400),
        ),
      );
    },
  );
}
