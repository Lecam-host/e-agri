import 'package:eagri/presentations/common/cache_network_image.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';

import '../../app/constant.dart';
import '../ressources/size_config.dart';

class PopularProductCard extends StatelessWidget {
  const PopularProductCard({
    Key? key,
    this.width = 140,
    this.aspectRetio = 1.02,
    required this.img,
    required this.name,
    required this.prix,
  }) : super(key: key);
  final String name, prix, img;
  final double width, aspectRetio;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: getProportionateScreenWidth(10)),
      child: SizedBox(
        width: getProportionateScreenWidth(width),
        child: GestureDetector(
          onTap: () {},
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: 150,
                height: 100,
                decoration: BoxDecoration(
                  color: kSecondaryColor.withOpacity(0.1),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: CacheNetworkImage(
                    image: img,
                  ),
                ),
              ),
              Text(
                name,
                style: getRegularTextStyle(color: ColorManager.black),
                maxLines: 2,
              ),
              Row(
                children: [
                  Text(
                    prix,
                    style: getBoldTextStyle(color: ColorManager.primary),
                  ),
                  Text(
                    " FCFA",
                    style: getBoldTextStyle(color: ColorManager.primary),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
