// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';

import 'date_field.dart';

enum Disponibility { future, immediat }

Disponibility disponibilityValue = Disponibility.immediat;

class DisponibiliteWidget extends StatefulWidget {
  DisponibiliteWidget(
      {Key? key,
      required this.disponibilite,
      required this.dateDisponibilite,
      required this.titre})
      : super(key: key);
  TextEditingController disponibilite;
  TextEditingController dateDisponibilite;
  String titre;

  @override
  State<DisponibiliteWidget> createState() => _DisponibiliteWidgetState();
}

class _DisponibiliteWidgetState extends State<DisponibiliteWidget> {
  @override
  void initState() {
    widget.disponibilite.text = "IMMEDIAT";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            widget.titre,
          ),
        ),
        RadioListTile(
          title: const Text("Oui"),
          value: Disponibility.immediat,
          groupValue: disponibilityValue,
          onChanged: (value) {
            setState(() {
              disponibilityValue = Disponibility.immediat;
              widget.disponibilite.text = "IMMEDIAT";
            });
            // print(addProductController.valueDisponibility);
          },
        ),
        RadioListTile(
          title: const Text("Non"),
          value: Disponibility.future,
          groupValue: disponibilityValue,
          onChanged: (value) {
            setState(
              () {
                disponibilityValue = Disponibility.future;
                widget.disponibilite.text = "FUTUR";
              },
            );
            // print(addProductController.valueDisponibility);
          },
        ),
        const SizedBox(
          height: 10,
        ),
        widget.disponibilite.text == "IMMEDIAT"
            ? Container()
            : DateField(
                dateController: widget.dateDisponibilite,
                hintText: "Choisir la date de disponibilité",
              ),
      ],
    );
  }
}
