import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';

class NoConnectionComponennt extends StatelessWidget {
  const NoConnectionComponennt({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          Icons.wifi_off,
          color: ColorManager.error,
        ),
        const SizedBox(
          width: 5,
        ),
        Text(
          "Aucune ou mauvaise connexion",
          style: getRegularTextStyle(
            fontSize: 10,
            color: ColorManager.error,
          ),
        ),
      ],
    );
  }
}
