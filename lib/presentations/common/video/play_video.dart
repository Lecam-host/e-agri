import 'dart:io';

import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

import '../../../app/functions.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/icon_manager.dart';
import '../../ressources/values_manager.dart';

/// Stateful widget to fetch and then display video content.
class AppVideoPlay extends StatefulWidget {
  const AppVideoPlay({
    Key? key,
    required this.link,
    required this.isNetworkVideo,
    this.file,
  }) : super(key: key);
  final String link;
  final bool isNetworkVideo;
  final File? file;

  @override
  AppVideoPlayState createState() => AppVideoPlayState();
}

class AppVideoPlayState extends State<AppVideoPlay> {
  late VideoPlayerController _controller;
  String timeVideo = "";
  bool loadVideo = true;
  initialConntroller() {
    if (widget.isNetworkVideo == true) {
      _controller = VideoPlayerController.network(
        // widget.file?.fileLink ??
        'http://192.168.4.36:8087/archive/ressource/NA==',
      )..initialize().then((_) {
          setState(() {
            loadVideo = false;
            timeVideo = formatPlayTime(
                Duration(seconds: _controller.value.duration.inSeconds));
          });
          _controller.pause();
        });
    } else {
      _controller = VideoPlayerController.file(widget.file!)
        ..initialize().then((_) {
          setState(() {
            loadVideo = false;
            timeVideo = formatPlayTime(
                Duration(seconds: _controller.value.duration.inSeconds));
          });
          _controller.pause();
        });
    }
  }

  @override
  void initState() {
    initialConntroller();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Center(
          child: _controller.value.isInitialized
              ? GestureDetector(
                  onTap: () {
                    setState(() {
                      _controller.value.isPlaying
                          ? _controller.pause()
                          : _controller.play();
                    });
                  },
                  child: Stack(
                    alignment: AlignmentDirectional.center,
                    children: [
                      AspectRatio(
                        aspectRatio: _controller.value.aspectRatio,
                        child: VideoPlayer(_controller),
                      ),
                      if (!_controller.value.isPlaying)
                        CircleAvatar(
                          backgroundColor: Colors.black.withOpacity(0.4),
                          child: Center(
                            child: Icon(
                              IconManager.play,
                              size: AppSize.s30,
                              color: ColorManager.white,
                            ),
                          ),
                        )
                    ],
                  ),
                )
              : Container(
                  height: 200,
                  width: double.infinity,
                  color: ColorManager.black,
                ),
        ),
        VideoProgressIndicator(_controller, //video player controller
            allowScrubbing: true,
            colors: VideoProgressColors(
              //video player progress bar
              backgroundColor: ColorManager.grey,
              playedColor: ColorManager.black,
              bufferedColor: ColorManager.grey1,
            )),
      ],
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }
}
