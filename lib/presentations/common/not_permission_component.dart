import 'package:flutter/material.dart';

import '../ressources/color_manager.dart';
import '../ressources/styles_manager.dart';

class NotPermissionComponent extends StatelessWidget {
  const NotPermissionComponent({super.key});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // Center(
            //   child: SizedBox(
            //     height: AppSize.s150,
            //     width: AppSize.s150,
            //     child: Lottie.asset(
            //       icon ?? JsonAssets.empty,
            //     ),
            //   ),
            // ),
            Text(
              "Vous ne disposez pas de la permissions",
              style: getRegularTextStyle(
                color: ColorManager.black,
                fontSize: 20,
              ),
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }
}