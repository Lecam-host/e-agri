import 'package:flutter/material.dart';

import '../ressources/color_manager.dart';
import '../ressources/font_manager.dart';
import '../ressources/styles_manager.dart';

class TitreComponent extends StatelessWidget {
  const TitreComponent({Key? key, required this.titre}) : super(key: key);
  final String titre;
  @override
  Widget build(BuildContext context) {
    return Text(
      titre,
      style:
          getMeduimTextStyle(color: ColorManager.black, fontSize: FontSize.s16),
    );
  }
}
