import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import '../../ressources/color_manager.dart';
import '../../ressources/styles_manager.dart';

// ignore: must_be_immutable
class AddImageWidget extends StatefulWidget {
  AddImageWidget({Key? key, required this.imageReturn}) : super(key: key);

  List<File> imageReturn;

  @override
  State<AddImageWidget> createState() => _AddImageWidgetState();
}

class _AddImageWidgetState extends State<AddImageWidget> {
  @override
  Widget build(BuildContext context) {
    return Wrap(
      spacing: 20,
      runSpacing: 20,
      children: [
        ...widget.imageReturn.map(
          (item) {
            return Stack(
              clipBehavior: Clip.none,
              alignment: AlignmentDirectional.bottomCenter,
              children: [
                Container(
                  width: 150,
                  height: 150,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(7),
                      border: Border.all(
                        color: Colors.grey,
                        width: 1,
                      ),
                      image: DecorationImage(
                          image: FileImage(File(item.path)),
                          fit: BoxFit.cover)),
                  // child: Image.file(),
                ),
                Positioned(
                  right: -10,
                  top: -10,
                  child: GestureDetector(
                    onTap: () {
                      widget.imageReturn
                          .removeAt(widget.imageReturn.indexOf(item));
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(10)),
                      child: const Icon(
                        Icons.close,
                        color: Colors.white,
                        size: 25,
                      ),
                    ),
                  ),
                )
              ],
            );
          },
        ),
        GestureDetector(
          onTap: () {
            // pickImages(PrestationController);
            selectGalleryOrCamera();
          },
          child: Container(
            width: 100,
            height: 100,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(7),
              border: Border.all(
                color: Colors.grey,
                width: 1,
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Icon(
                  Icons.add,
                  color: Colors.grey,
                ),
                Text(
                  "Ajouter",
                  style: getRegularTextStyle(color: Colors.grey),
                )
              ],
            ),
          ),
        )
      ],
    );
  }

  addFileAndConvert(XFile file) {
    final newFile = File(file.path);
    setState(() {
      widget.imageReturn.add(newFile);
    });
  }

  selectGalleryOrCamera() {
    return Get.defaultDialog(
      title: "Ajouter une image",
      titleStyle: TextStyle(
        color: ColorManager.primary2,
      ),
      content: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          IconButton(
            onPressed: () {
              pickImages(ImageSource.camera);
            },
            icon: const Icon(
              Icons.camera_alt,
              size: 35,
            ),
          ),
          const SizedBox(width: 20),
          IconButton(
            onPressed: () {
              pickImages(ImageSource.gallery);
            },
            icon: const Icon(Icons.image, size: 35),
          ),
        ],
      ),
    );
  }

  Future<void> pickImages(ImageSource source) async {
    final picker = ImagePicker();
    final XFile? selectedImages =
        await picker.pickImage(imageQuality: 50, source: source);
    Get.back();
    if (selectedImages != null) {
      addFileAndConvert(selectedImages);
      // Get.back();
      // controller.files.refresh();
    }
    // Get.back();
  }
}
