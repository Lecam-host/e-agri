import 'package:animations/animations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/app/constant.dart';
import 'package:eagri/app/functions.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/details_produit/details_produit_screen.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../app/di.dart';
import '../../data/repository/api/repository_impl.dart';
import '../../data/request/request.dart';
import '../../domain/model/Product.dart';

import '../cart/controller/cart_controller.dart';
import '../ressources/assets_manager.dart';
import '../ressources/strings_manager.dart';
import 'default_button.dart';

typedef ProductCardOnTaped = void Function(ProductModel data);

class ProductInfo extends StatefulWidget {
  const ProductInfo({
    super.key,
    required this.data,
    this.ontap,
    this.isOffreAchatFind = false,
    this.isIntrant = false,
  });

  final ProductModel data;
  final bool isOffreAchatFind;
  final bool isIntrant;

  final ProductCardOnTaped? ontap;

  @override
  State<ProductInfo> createState() => _ProductInfoState();
}

class _ProductInfoState extends State<ProductInfo> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  late ProductModel productShow;
  bool isLoadDetails = true;
  CartController cartController = Get.find();
  bool isLoadAddcart = false;

  getMoreInfo() async {
    {
      await repositoryImpl
          .getInfoById(InfoByIdRequest(
            userId: productShow.fournisseurId,
            regionId: productShow.location!.regionId,
            departementId: productShow.location!.departementId != 0 &&
                    productShow.location!.departementId != null
                ? productShow.location!.departementId
                : null,
            sprefectureId: productShow.location!.sPrefectureId != 0 &&
                    productShow.location!.sPrefectureId != null
                ? productShow.location!.sPrefectureId
                : null,
            localiteId: productShow.location!.localiteId != 0 &&
                    productShow.location!.localiteId != null
                ? productShow.location!.localiteId
                : null,
          ))
          .then((value) => value.fold((l) {
                if (mounted) {
                  setState(() {
                    isLoadDetails = false;
                  });
                }
              }, (info) {
                productShow.location!.regionName =
                    info.region != null ? info.region!.name : null;
                productShow.location!.departementName =
                    info.departement != null ? info.departement!.name : null;
                productShow.location!.sPrefectureName =
                    info.sousprefecture != null
                        ? info.sousprefecture!.name
                        : null;
                productShow.location!.localiteName =
                    info.localite != null ? info.localite!.name : null;

                if (mounted) {
                  setState(() {
                    isLoadDetails = false;
                    productShow;
                  });
                }
              }));
    }
  }

  @override
  void initState() {
    productShow = widget.data;
    getMoreInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // final data = datas[index % datas.length];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Stack(
          children: [
            if (widget.data.images!.isNotEmpty)
              CachedNetworkImage(
                imageUrl: productShow.images![0],
                imageBuilder: (context, imageProvider) => Container(
                  width: double.maxFinite,
                  height: 150,
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(5),
                        topRight: Radius.circular(5)),
                    image: DecorationImage(
                      scale: 1,
                      image: imageProvider,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                placeholder: (context, url) => Container(
                  color: ColorManager.grey.withOpacity(0.2),
                  height: 150,
                  width: double.infinity,
                ),
                errorWidget: (context, url, error) => Container(
                  color: ColorManager.grey.withOpacity(0.2),
                  height: 150,
                  width: double.infinity,
                  child: const Icon(Icons.error),
                ),
              ),
            if (productShow.images!.isEmpty)
              Container(
                width: double.maxFinite,
                height: 150,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10)),
                ),
                child: const Icon(Icons.image),
              ),

            if (productShow.typeOffers.toLowerCase() == "vente" &&
                productShow.code == codeProductAgricultural.toString())
              Positioned(
                top: 5,
                left: 5,
                child: Container(
                  // alignment: Alignment.center,
                  padding: const EdgeInsets.only(
                      bottom: 4, top: 2, right: 3, left: 3),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: ColorManager.primary.withOpacity(0.7),
                  ),
                  child: Text(
                    "En ${productShow.typeVente}",
                    style: getRegularTextStyle(
                        color: ColorManager.white, fontSize: 10),
                  ),
                ),
              ),
            // Positioned(
            //   bottom: 5,
            //   right: 5,
            //   child: CircleAvatar(
            //     radius: iconSize,
            //     backgroundColor: ColorManager.white.withOpacity(0.8),
            //     child: SvgPicture.asset(SvgManager.like,
            //         color: ColorManager.black,
            //         width: iconSize,
            //         height: iconSize),
            //   ),
            // ),
            if (productShow.code == codeIntrant.toString() ||
                productShow.code == codeProductAgricultural.toString())
              Positioned(
                top: 5,
                right: 5,
                child: !cartController.isAlredyAdded(productShow)
                    ? isLoadAddcart == false
                        ? GestureDetector(
                            onTap: () {
                              quantityInput(productShow);
                            },
                            child: CircleAvatar(
                              radius: 15,
                              backgroundColor:
                                  ColorManager.white.withOpacity(0.8),
                              child: SvgPicture.asset(SvgManager.cart,
                                  color: ColorManager.black,
                                  width: 15,
                                  height: 15),
                            ),
                          )
                        : CircleAvatar(
                            radius: 15,
                            backgroundColor:
                                ColorManager.white.withOpacity(0.8),
                            child: const LoaderComponent(),
                          )
                    : Container(
                        // alignment: Alignment.center,
                        padding: const EdgeInsets.only(
                            bottom: 4, top: 2, right: 3, left: 3),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: ColorManager.blue.withOpacity(0.7),
                        ),
                        child: Text(
                          "En panier",
                          style: getRegularTextStyle(
                              color: ColorManager.white, fontSize: 10),
                        ),
                      ),
              )
          ],
        ),
        Container(
          height: 150,
          decoration: BoxDecoration(
            color: ColorManager.white,
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10),
            ),
          ),
          padding: const EdgeInsets.all(8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                // height: 20,
                child: Text(
                  productShow.title.toCapitalize(),
                  style: getMeduimTextStyle(
                    color: ColorManager.primary,
                    fontSize: 12,
                  ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              if (productShow.identifiant != null)
                SizedBox(
                  // height: 20,
                  child: Text(
                    productShow.identifiant!,
                    style: getLightTextStyle(
                      color: ColorManager.grey,
                      fontSize: 10,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),

              Text(
                'Qte: ${productShow.quantity} ${productShow.unite}',
                style: getLightTextStyle(
                  color: ColorManager.red,
                  fontSize: 12,
                ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              const SizedBox(
                height: 5,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Icon(Icons.location_on, size: 12, color: ColorManager.jaune),
                  Expanded(
                    child: RichText(
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      text: TextSpan(
                        text: "",
                        style: getRegularTextStyle(
                            color: ColorManager.grey, fontSize: 10),
                        children: [
                          TextSpan(
                            text: productShow.location!.regionName != null
                                ? productShow.location!.regionName!
                                    .toCapitalize()
                                : "[Region]",
                            style: getRegularTextStyle(fontSize: 12),
                          ),
                          TextSpan(
                            text: productShow.location!.sPrefectureName != null
                                ? " ,${productShow.location!.departementName!.toCapitalize()} "
                                : "",
                            style: getRegularTextStyle(fontSize: 12),
                          ),
                          TextSpan(
                            text: productShow.location!.sPrefectureName != null
                                ? " ,${productShow.location!.sPrefectureName!.toCapitalize()}"
                                : "",
                            style: getRegularTextStyle(fontSize: 12),
                          ),
                          TextSpan(
                            text: productShow.location!.localiteName != null
                                ? " ,${productShow.location!.localiteName!.toCapitalize()}"
                                : "",
                            style: getRegularTextStyle(fontSize: 12),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              // _buildSoldPoint(4.5, 6937),

              const Spacer(),
              Row(
                children: [
                  RatingBar.builder(
                    initialRating: productShow.rating,
                    minRating: 0,
                    direction: Axis.horizontal,
                    allowHalfRating: false,
                    itemCount: 5,
                    itemSize: 10,
                    itemPadding: const EdgeInsets.symmetric(horizontal: 0),
                    itemBuilder: (context, _) => const Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (rating) {},
                  ),
                  Text(
                    "(${productShow.rating})",
                    style: getMeduimTextStyle(
                        color: ColorManager.grey, fontSize: 10),
                  ),
                ],
              ),
              Text('${productShow.price} ${AppStrings.moneyUnit}',
                  style: getMeduimTextStyle(
                    color: ColorManager.primary,
                    fontSize: 14,
                  )),
            ],
          ),
        ),
      ],
    );
  }

  quantityInput(ProductModel product) {
    if (product.typeVente == "BLOC") {
      cartController.quantityCommand.value.text = product.quantity.toString();
    } else {
      cartController.quantityCommand.value.text = "1";
    }
    Get.bottomSheet(
      StatefulBuilder(
        builder: (cxt, setStateBottomSheet) {
          return Container(
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(10)),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                if (product.typeVente == "BLOC") ...[
                  Text(
                    'Ce produit est vendu en bloc donc vous ne pouvez pas changer la quantité',
                    style: getRegularTextStyle(),
                    textAlign: TextAlign.center,
                  ),
                ] else ...[
                  Text(
                    "Entrer la quantité",
                    style: getBoldTextStyle(
                        color: ColorManager.black, fontSize: 14),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Form(
                    key: _formKey,
                    child: TextFormField(
                      initialValue: "1",
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Entrer la quantité';
                        } else if (validQuantity(
                              int.parse(value),
                            ) ==
                            false) {
                          return 'Quantité invalide';
                        } else if (int.parse(value) > product.quantity) {
                          return "Quantité indisponible";
                        } else {
                          return null;
                        }
                      },
                      onChanged: (value) {
                        if (_formKey.currentState!.validate()) {
                          setStateBottomSheet(() {
                            cartController.quantityCommand.value.text = value;
                          });
                        }
                      },
                      // controller: cartController.quantityCommand.value,
                      keyboardType: TextInputType.number,

                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.grey.withOpacity(0.2),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15.0),
                          borderSide: const BorderSide(
                              width: 0, style: BorderStyle.none),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15.0),
                          borderSide: const BorderSide(
                              width: 0, style: BorderStyle.none),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15.0),
                          borderSide: const BorderSide(
                              width: 0, style: BorderStyle.none),
                        ),
                        hintText: 'Entrer la quantité',
                      ),
                    ),
                  ),
                ],
                const SizedBox(
                  height: 20,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    RichText(
                      text: TextSpan(
                        text: 'Quantité disponible : ',
                        style: getRegularTextStyle(color: ColorManager.black),
                        children: [
                          if (cartController
                              .quantityCommand.value.text.isNotEmpty)
                            TextSpan(
                              text: "${product.quantity} ${product.unite}",
                              style: getMeduimTextStyle(
                                color: ColorManager.jaune,
                                fontSize: 16,
                              ),
                            ),
                          if (cartController.quantityCommand.value.text.isEmpty)
                            TextSpan(
                              text: "_ _ _",
                              style: getMeduimTextStyle(
                                color: ColorManager.jaune,
                                fontSize: 16,
                              ),
                            ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    RichText(
                      text: TextSpan(
                        text: 'Quantité démandée : ',
                        style: getRegularTextStyle(color: ColorManager.black),
                        children: [
                          if (cartController
                              .quantityCommand.value.text.isNotEmpty)
                            TextSpan(
                              text:
                                  "${cartController.quantityCommand.value.text} ${product.unite}",
                              style: getMeduimTextStyle(
                                color: ColorManager.jaune,
                                fontSize: 16,
                              ),
                            ),
                          if (cartController.quantityCommand.value.text.isEmpty)
                            TextSpan(
                              text: "_ _ _",
                              style: getMeduimTextStyle(
                                color: ColorManager.jaune,
                                fontSize: 16,
                              ),
                            ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    RichText(
                      text: TextSpan(
                        text: 'Prix total : ',
                        style: getRegularTextStyle(color: ColorManager.black),
                        children: [
                          if (cartController
                              .quantityCommand.value.text.isNotEmpty)
                            TextSpan(
                              text: " ${separateur((int.parse(
                                    cartController.quantityCommand.value.text,
                                  ) * product.price).toDouble())} FCFA",
                              style: getMeduimTextStyle(
                                color: ColorManager.jaune,
                                fontSize: 16,
                              ),
                            ),
                          if (cartController.quantityCommand.value.text.isEmpty)
                            TextSpan(
                              text: "_ _ _",
                              style: getMeduimTextStyle(
                                color: ColorManager.jaune,
                                fontSize: 16,
                              ),
                            ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                SizedBox(
                  width: 150,
                  child: DefaultButton(
                    text: "Ajouter",
                    press: () async {
                      if (product.typeVente == "BLOC") {
                        Get.back();

                        setState(() {
                          isLoadAddcart = true;
                        });

                        if (cartController.isAlredyAdded(product) == false) {
                          await cartController.addToCart(
                            product,
                            int.parse(
                              cartController.quantityCommand.value.text,
                            ),
                          );
                          setState(() {
                            isLoadAddcart = false;
                          });
                        }
                      } else {
                        if (_formKey.currentState!.validate()) {
                          Get.back();

                          setState(() {
                            isLoadAddcart = true;
                          });

                          if (cartController.isAlredyAdded(product) == false) {
                            await cartController.addToCart(
                              product,
                              int.parse(
                                cartController.quantityCommand.value.text,
                              ),
                            );
                            setState(() {
                              isLoadAddcart = false;
                            });
                          }
                        }
                      }
                    },
                  ),
                ),
              ],
            ),
          );
        },
      ),
      isScrollControlled: true,
    );
  }
}

class ProductCard extends StatelessWidget {
  const ProductCard({
    super.key,
    required this.data,
    this.ontap,
    this.isOffreAchatFind = false,
    this.isIntrant = false,
  });

  final ProductModel data;
  final bool isOffreAchatFind;
  final bool isIntrant;

  final ProductCardOnTaped? ontap;
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openElevation: 0,
      closedElevation: 0,
      transitionType: transitionType,
      transitionDuration: transitionDuration,
      openBuilder: (context, _) => DetailsProduitScreen(
          product: ProductDetailsArguments(
        product: data,
        isIntrant: isIntrant,
      )),
      closedBuilder: (context, VoidCallback openContainer) => ProductInfo(
        data: data,
      ),
    );
  }
}
