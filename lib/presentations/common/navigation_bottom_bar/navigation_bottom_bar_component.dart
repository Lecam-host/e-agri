import 'package:flutter/material.dart';

import '../../ressources/color_manager.dart';
import '../../ressources/routes_manager.dart';
import '../../ressources/strings_manager.dart';

class CustomBottomNavBar extends StatelessWidget {
  const CustomBottomNavBar({
    Key? key,
    required this.selectWidgetName,
  }) : super(key: key);

  final String selectWidgetName;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: const Border(
          top: BorderSide(
            color: Colors.black,
            width: 0.1,
          ),
        ),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            offset: const Offset(0, -15),
            blurRadius: 20,
            color: const Color(0xFFDADADA).withOpacity(0.15),
          ),
        ],
      ),
      child: SafeArea(
          top: false,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              NavBarBouttonn(
                widgetName: Routes.homeRoute,
                selectWidgetName: selectWidgetName,
                icon: selectWidgetName == Routes.homeRoute
                    ? Icons.home
                    : Icons.home_outlined,
                text: AppStrings.home,
              ),
              NavBarBouttonn(
                widgetName: Routes.notification,
                selectWidgetName: selectWidgetName,
                icon: selectWidgetName == Routes.notification
                    ? Icons.notifications
                    : Icons.notifications_rounded,
                text: AppStrings.profil,
              ),
              NavBarBouttonn(
                widgetName: Routes.docView,
                selectWidgetName: selectWidgetName,
                icon: selectWidgetName == Routes.docView
                    ? Icons.settings
                    : Icons.settings_outlined,
                text: AppStrings.profil,
              ),
            ],
          )),
    );
  }
}

class NavBarBouttonn extends StatelessWidget {
  const NavBarBouttonn({
    Key? key,
    required this.selectWidgetName,
    required this.icon,
    required this.text,
    required this.widgetName,
  }) : super(key: key);
  final IconData icon;
  final String text;
  final String widgetName;
  final String selectWidgetName;
  @override
  Widget build(BuildContext context) {
    Color inActiveIconColor = ColorManager.grey1;
    Color activeIconColor = ColorManager.primary;

    return Wrap(
      crossAxisAlignment: WrapCrossAlignment.center,
      direction: Axis.vertical,
      children: [
        IconButton(
            icon: Icon(
              icon,
              size: 25,
              color: widgetName == selectWidgetName
                  ? activeIconColor
                  : inActiveIconColor,
            ),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed(widgetName);
            }),
        Text(text,
            style: TextStyle(
              fontSize: 12,
              color: widgetName == selectWidgetName
                  ? activeIconColor
                  : inActiveIconColor,
            )),
      ],
    );
  }
}
