import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import '../../ressources/color_manager.dart';
import '../../ressources/icon_manager.dart';
import '../../ressources/strings_manager.dart';
import '../../ressources/styles_manager.dart';

class SeeMoreTextButton extends StatelessWidget {
  const SeeMoreTextButton({Key? key, this.child}) : super(key: key);
  final Widget? child;
  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: TextButton.styleFrom(padding: const EdgeInsets.all(0)),
      onPressed: () {
        if (child != null) {
          Navigator.push(
            context,
            PageTransition(
              type: PageTransitionType.rightToLeft,
              child: child!,
              isIos: true,
              duration: const Duration(milliseconds: 400),
            ),
          );
        }
      },
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            AppStrings.seeMore,
            style: getMeduimTextStyle(color: ColorManager.blue),
          ),
          Icon(IconManager.arrowRight, color: ColorManager.blue)
        ],
      ),
    );
  }
}
