import 'package:flutter/material.dart';

import '../../ressources/color_manager.dart';

class BackButtonCustom extends StatelessWidget {
  const BackButtonCustom({Key? key, this.onPressed, this.color, this.icon})
      : super(key: key);
  final Function()? onPressed;
  final Color? color;
  final IconData? icon;
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(
        icon ?? Icons.chevron_left,
        color: color ?? ColorManager.black,
      ),
      onPressed: onPressed ??
          () {
            Navigator.of(context).pop();
          },
    );
  }
}
