// ignore_for_file: must_be_immutable

import 'package:eagri/domain/model/user_model.dart';
import 'package:eagri/presentations/cart/controller/cart_controller.dart';
import 'package:eagri/presentations/login/login_view.dart';
import 'package:eagri/presentations/ressources/assets_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

import '../../ressources/routes_manager.dart';
import '../../ressources/size_config.dart';

class CartButtomComponent extends StatelessWidget {
  CartButtomComponent({Key? key}) : super(key: key);

  CartController cartController = Get.put(CartController());

  // late AnimationController controller;
  // void tapDown() {
  //   print('tapDowntapDown');

  //   controller.forward().then((value) {
  //     print('forwardforward');
  //     controller.reverse();
  //   });
  // }

  // void tapUp() async {
  //   await controller.reverse();
  //   print('tapUptapUp');
  // }

  // anime() {
  //   print('animeanime ');
  //   cartController.cartItems.listen((p0) {
  //     print('listenlisten ');

  //     tapDown();
  //     // tapUp();
  //   });
  // }

  // animation() {
  //   print('animationanimation ');
  //   controller = AnimationController(
  //     vsync: this,
  //     duration: const Duration(
  //       milliseconds: 500,
  //     ),
  //     lowerBound: 0.0,
  //     upperBound: 0.5,
  //   )..addListener(() {
  //       setState(() {});
  //     });
  // }

  // @override
  // void initState() {
  //   animation();
  //   anime();
  //   super.initState();
  // }

  // @override
  // void dispose() {
  //   controller.dispose();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
      builder: (context, user, child) => Obx(
        () => InkWell(
          borderRadius: BorderRadius.circular(100),
          onTap: () {
            if (user.isConnected == true) {
              Get.toNamed(Routes.cart);
            } else {
              Get.to(const LoginView());
            }
          },
          child: Stack(
            clipBehavior: Clip.none,
            children: [
              Container(
                padding: const EdgeInsets.all(10),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.circle,
                ),
                child: SvgPicture.asset(
                  cartController.cartItems.isNotEmpty &&
                          user.isConnected == true
                      ? SvgManager.cartIsNotEmpty
                      : SvgManager.cart,
                  height: 20,
                ),
              ),
              if (cartController.cartItems.isNotEmpty &&
                  user.isConnected == true)
                Positioned(
                  right: 0,
                  child: Container(
                    height: getProportionateScreenWidth(16),
                    width: getProportionateScreenWidth(16),
                    decoration: BoxDecoration(
                      color: const Color(0xFFFF4848),
                      shape: BoxShape.circle,
                      border: Border.all(width: 1.5, color: Colors.white),
                    ),
                    child: Center(
                      child: Text(
                        cartController.cartItems.length.toString(),
                        style: TextStyle(
                          fontSize: getProportionateScreenWidth(10),
                          height: 1,
                          fontWeight: FontWeight.w600,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
