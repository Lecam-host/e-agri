import 'package:flutter/material.dart';

import '../../ressources/color_manager.dart';
import '../../ressources/icon_manager.dart';

class SearchIconButton extends StatelessWidget {
  const SearchIconButton({Key? key, this.onPressed}) : super(key: key);
  final Function()? onPressed;
  @override
  Widget build(BuildContext context) {
    return IconButton(
        onPressed: onPressed ?? () {},
        icon: Icon(
          IconManager.search,
          color: ColorManager.black,
        ));
  }
}
