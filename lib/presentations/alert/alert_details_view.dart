import 'package:eagri/domain/model/alerte_model.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';

import '../../app/functions.dart';
import '../common/buttons/back_button.dart';

class AlertDetailsView extends StatelessWidget {
  final DataAlerte data;
  const AlertDetailsView({super.key, required this.data});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButtonCustom(),
        title: const Text('Details de l\'alerte'),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        physics: const BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Hero(
              tag: "alert${data.id}",
              child: Container(
                height: MediaQuery.of(context).size.height / 3,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: NetworkImage(data.reporting.files!.isNotEmpty
                        ? data.reporting.files![0].fileUrl!
                        : 'https://cdn3.iconfinder.com/data/icons/security-line-color-vol-1/52/alert__error__notfound__battery__dead__missing__404-512.png'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            const SizedBox(height: 20),
            Text("Incendie sur la voie de l'Ouest",
                style: getMeduimTextStyle(fontSize: 20)),
            Text(
              data.reporting.description!,
              overflow: TextOverflow.ellipsis,
              maxLines: 3,
            ),
            const SizedBox(height: 20),
            RichText(
                text: TextSpan(children: [
              TextSpan(
                text: "Date : ",
                style: getMeduimTextStyle(fontSize: 16, color: Colors.black),
              ),
              TextSpan(
                text: convertDateWithTime(data.reporting.createdAt!.toString()),
                style: getRegularTextStyle(fontSize: 16, color: Colors.black),
              )
            ])),
            const SizedBox(height: 20),
            RichText(
                text: TextSpan(children: [
              TextSpan(
                text: "Auteur :",
                style: getMeduimTextStyle(fontSize: 16, color: Colors.black),
              ),
              if (data.reporting.user != null)
                TextSpan(
                  text:
                      "${data.reporting.user!.firstname} ${data.reporting.user!.lastname}",
                  style: getRegularTextStyle(fontSize: 16, color: Colors.blue),
                )
            ]))
          ],
        ),
      ),
    );
  }
}
