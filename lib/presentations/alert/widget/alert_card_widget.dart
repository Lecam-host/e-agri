import 'package:eagri/app/functions.dart';
import 'package:eagri/domain/model/alerte_model.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../ressources/styles_manager.dart';
import '../alert_details_view.dart';

class AlertCardWidget extends StatelessWidget {
  final DataAlerte data;
  const AlertCardWidget({super.key, required this.data});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.to(AlertDetailsView(data: data));
      },
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Hero(
                tag: "alert${data.id}",
                child: Stack(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height / 4,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        image: DecorationImage(
                          image: NetworkImage(data.reporting.files!.isNotEmpty
                              ? data.reporting.files![0].fileUrl!
                              : 'https://cdn3.iconfinder.com/data/icons/security-line-color-vol-1/52/alert__error__notfound__battery__dead__missing__404-512.png'),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 0,
                      left: 0,
                      child: Material(
                        borderRadius: const BorderRadius.only(
                          bottomLeft: Radius.circular(10),
                          topRight: Radius.circular(10),
                        ),
                        child: Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            color: ColorManager.primary3.withOpacity(0.8),
                            borderRadius: const BorderRadius.only(
                              bottomLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                            ),
                          ),
                          child: Text(
                            convertDateWithTime(
                                data.reporting.createdAt!.toString()),
                            style: const TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                        top: 10,
                        right: 10,
                        child: Material(
                          borderRadius: BorderRadius.circular(10),
                          child: Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              color: ColorManager.primary.withOpacity(0.7),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: const Text("En cours de traitement",
                                style: TextStyle(
                                  color: Colors.white,
                                )),
                          ),
                        ))
                  ],
                )),
            const SizedBox(height: 10),
            Text(
              data.reporting.domain == null ? "" : data.reporting.domain!.name!,
              style: getSemiBoldTextStyle(fontSize: 17),
            ),
            Text(
              data.reporting.speculation == null
                  ? '[Titre introuvable !]'
                  : data.reporting.speculation!.name!,
              style: getRegularTextStyle(
                  fontSize: 17, color: ColorManager.primary),
            ),
            Text(
              data.reporting.description!,
              overflow: TextOverflow.ellipsis,
              maxLines: 3,
            ),
            const SizedBox(height: 5),
            Row(
              children: [
                Icon(Icons.location_on, color: ColorManager.primary),
                Text(
                  data.reporting.location == null
                      ? ''
                      : data.reporting.location!.name!,
                  style: getRegularTextStyle(fontSize: 15),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}