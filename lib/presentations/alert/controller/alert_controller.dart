import 'dart:developer';

import 'package:eagri/app/di.dart';
import 'package:eagri/data/repository/api/repository_impl.dart';

import 'package:get/get.dart';

import '../../../domain/model/alerte_model.dart';

class AlertController extends GetxController {
  var listAlerte = <DataAlerte>[].obs;
  var load = false.obs;
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  @override
  void onInit() async {
    load.value = true;
    await fetchListAlerte();
    load.value = false;
    super.onInit();
  }

  fetchListAlerte() async {
    await repositoryImpl
        .getListAlert()
        .then((value) => (value).fold((l) {
              listAlerte.value = [];
              inspect(l);
              log(l.toString());
            }, (r) {
              listAlerte.value = r.data!;
              log(r.toString());
            }))
        .catchError((onError) =>null);
  }
}
