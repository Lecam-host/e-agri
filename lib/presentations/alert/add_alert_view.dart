import 'dart:io';

import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/data/request/request.dart';
import 'package:eagri/domain/model/location_model.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/domain/usecase/advice_usecase.dart';
import 'package:eagri/domain/usecase/alert_usecase.dart';
import 'package:eagri/domain/usecase/config_usecase.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/icon_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

import '../../app/di.dart';
import '../../domain/model/user_model.dart';
import '../common/state/succes_page_view.dart';
import '../common/state_renderer/state_render_impl.dart';
import '../common/state_renderer/state_renderer.dart';
import '../ressources/strings_manager.dart';

class AddAlertView extends StatefulWidget {
  const AddAlertView({Key? key}) : super(key: key);

  @override
  State<AddAlertView> createState() => _AddAlertViewState();
}

class _AddAlertViewState extends State<AddAlertView> {
  List<RegionModel> listRegion = [];
  List<ConfigModel> listDomain = [];
  TextEditingController descriptionController = TextEditingController();
  TextEditingController lieuController = TextEditingController();

  AlertUserCase alertUsecase = instance<AlertUserCase>();
  ConfigUsecase configUsecase = instance<ConfigUsecase>();

  final ImagePicker imagePicker = ImagePicker();
  List<File> imageFileList = [];
  sendAlert(AddAlertRequest request) {
    LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE)
        .getScreenWidget(context, Container(), () {});
    alertUsecase.addAlert(request).then(
          (value) => value.fold(
            (left) {
              ErrorState(StateRendererType.POPUP_ERROR_STATE, left.message);
              Navigator.pop(context);
            },
            (alert) {
              alertUsecase
                  .addAlertFiles(alert.id, request.files)
                  .then((value) => value.fold((l) => null, (r) {
                        Navigator.pop(context);

                        Navigator.pushReplacement(
                          context,
                          PageTransition(
                            type: PageTransitionType.rightToLeft,
                            child: const SuccessPageView(
                                message:
                                    "Votre alerte a été rémonté avec succes"),
                            isIos: true,
                            duration: const Duration(milliseconds: 400),
                          ),
                        );
                      }));
            },
          ),
        );
  }

  void selectImagesInGalery() async {
    List<XFile> selectedImages = await imagePicker.pickMultiImage();
    if (selectedImages.isNotEmpty) {
      for (var image in selectedImages) {
        imageFileList.add(File(image.path));
      }
    }
    setState(() {
      imageFileList;
    });
  }

  void takePicture() async {
    XFile? selectedImages = await imagePicker.pickImage(
        source: ImageSource.camera,
        imageQuality: 50,
        preferredCameraDevice: CameraDevice.rear);
    if (selectedImages != null) {
      imageFileList.add(File(selectedImages.path));
    }
    setState(() {
      imageFileList;
    });
  }

  Future getListRegion() async {
    LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE)
        .getScreenWidget(context, Container(), () {});
    (await alertUsecase.getListRegion()).fold((failure) {
      Navigator.of(context).pop();
    }, (result) {
      setState(() {
        listRegion = result.data;
      });
      Navigator.of(context).pop();
    });
  }

  Future getConfig() async {
    LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE)
        .getScreenWidget(context, Container(), () {});
    (await configUsecase.getAlertDomain()).fold((failure) {
      if (mounted) Navigator.of(context).pop();
    }, (result) {
      setState(() {
        listDomain = result;
      });

      setState(() {});
      Navigator.of(context).pop();
    });
  }

  @override
  void initState() {
    getConfig();
    getListRegion();
    super.initState();
  }

  int _currentStep = 0;
  StepperType stepperType = StepperType.horizontal;
  ConfigModel? domaineOuput;
  RegionModel? regionOutput;

  AdviceUseCase advice = instance<AdviceUseCase>();
  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
        builder: (context, user, child) => Scaffold(
              appBar: AppBar(
                leading: const BackButtonCustom(),
                automaticallyImplyLeading: false,
                title: const Text('Remonter un signalement'),
                centerTitle: true,
              ),
              body: Column(
                children: [
                  Expanded(
                    child: Stepper(
                      type: stepperType,
                      controlsBuilder: (context, controlsDetails) {
                        final isLastStep = _currentStep == listStep.length - 1;
                        return Row(
                          children: [
                            Expanded(
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  elevation: 0,
                                  backgroundColor: Colors.grey,
                                ),
                                onPressed: controlsDetails.onStepCancel,
                                child: const Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Icon(IconManager.arrowLef),
                                    Text(AppStrings.previous),
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  elevation: 0,
                                  backgroundColor: ColorManager.primary,
                                ),
                                onPressed: isLastStep == true
                                    ? () {
                                        AddAlertRequest addAlertRequest =
                                            AddAlertRequest(
                                                departementId: 0,
                                                sousprefectureId: 0,
                                                localiteId: 0,
                                                userId: user.id!,
                                                speculationId: 0,
                                                description:
                                                    descriptionController.text,
                                                domain: domaineOuput!.id!,
                                                files: imageFileList,
                                                regionId:
                                                    regionOutput!.regionId!);
                                        sendAlert(addAlertRequest);
                                      }
                                    : controlsDetails.onStepContinue,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(isLastStep == true
                                        ? AppStrings.submit
                                        : AppStrings.next),
                                    const Icon(IconManager.arrowRight)
                                  ],
                                ),
                              ),
                            ),
                          ],
                        );
                      },
                      physics: const ScrollPhysics(),
                      currentStep: _currentStep,
                      onStepTapped: (step) => tapped(step),
                      onStepContinue: continued,
                      onStepCancel: cancel,
                      steps: listStep,
                    ),
                  ),
                ],
              ),
            ));
  }

  showCustomFlushbar() {
    return Flushbar(
      maxWidth: 300,
      flushbarPosition: FlushbarPosition.BOTTOM,
      duration: const Duration(seconds: 3),
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.all(10),
      borderRadius: BorderRadius.circular(15),
      backgroundColor: Colors.blue,
      icon: Icon(Icons.info, color: ColorManager.white),
      boxShadows: const [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],
      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      // title: '',
      message: "Remplissez correctement ce formulaire",
    ).show(context);
  }

  verifForm() {
    if (_currentStep == 0 &&
        (domaineOuput == null ||
            descriptionController.text == "" ||
            regionOutput == null)) {
      showCustomFlushbar();
    } else {
      _currentStep < 2 ? setState(() => _currentStep += 1) : null;
    }
  }

  List<Step> get listStep {
    return <Step>[
      Step(
        title: const Text('Etape 1'),
        content: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const Text('Domaine *'),
            Container(
              color: ColorManager.formFieldBackgroundColor,
              child: DropdownButton<ConfigModel>(
                dropdownColor: ColorManager.white,
                isExpanded: true,
                hint: const Text(AppStrings.precisezDomaine),
                alignment: AlignmentDirectional.center,
                value: domaineOuput,
                icon: const Icon(Icons.keyboard_arrow_down),
                items: listDomain.map((ConfigModel items) {
                  return DropdownMenuItem(
                    value: items,
                    child: Text(items.name!),
                  );
                }).toList(),
                onChanged: (ConfigModel? newValue) {
                  setState(() {
                    domaineOuput = newValue;
                  });
                },
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              color: ColorManager.formFieldBackgroundColor,
              child: DropdownButton<RegionModel>(
                dropdownColor: ColorManager.white,
                isExpanded: true,
                hint: const Text(AppStrings.regionQuestion),
                alignment: AlignmentDirectional.center,
                value: regionOutput,
                icon: const Icon(Icons.keyboard_arrow_down),
                items: listRegion.map((RegionModel items) {
                  return DropdownMenuItem(
                    value: items,
                    child: Text(items.name!),
                  );
                }).toList(),
                onChanged: (RegionModel? newValue) {
                  setState(() {
                    regionOutput = newValue;
                  });
                },
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text('Description *'),
            SizedBox(
              height: 200,
              child: TextFormField(
                maxLines: null,
                controller: descriptionController,
                decoration: InputDecoration(
                  filled: true,
                  hintText: "Entrer la description",
                  fillColor: ColorManager.formFieldBackgroundColor,
                ),
              ),
            ),
          ],
        ),
        isActive: _currentStep >= 0,
        state: _currentStep >= 0 ? StepState.complete : StepState.disabled,
      ),
      Step(
        title: const Text('Etape 2'),
        content: Column(
          children: [
            Text(
              "Ajouter une photo",
              style:
                  getMeduimTextStyle(color: ColorManager.black, fontSize: 14),
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                GestureDetector(
                  onTap: () {
                    takePicture();
                  },
                  child: SizedBox(
                    height: 100,
                    width: 150,
                    child: Card(
                      color: ColorManager.blue,
                      child: Container(
                        margin: const EdgeInsets.all(10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              IconManager.photoCamera,
                              color: ColorManager.white,
                            ),
                            Text(
                              "Appareil photo",
                              style: getRegularTextStyle(
                                color: ColorManager.white,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    selectImagesInGalery();
                  },
                  child: SizedBox(
                    height: 100,
                    width: 150,
                    child: Card(
                      color: ColorManager.blue,
                      child: Container(
                        margin: const EdgeInsets.all(10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              IconManager.gallery,
                              color: ColorManager.white,
                            ),
                            Text(
                              "Gallerie",
                              style: getRegularTextStyle(
                                color: ColorManager.white,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
            if (imageFileList.isNotEmpty) imageWidget(),
            if (imageFileList.isEmpty) const Text(AppStrings.lorem),
            const SizedBox(
              height: 30,
            )
          ],
        ),
        isActive: _currentStep >= 0,
        state: _currentStep >= 1 ? StepState.complete : StepState.disabled,
      ),
      Step(
        title: const Text('Validation'),
        content: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Vérifier les différentes informations que vous avez entrées',
              style: getSemiBoldTextStyle(color: ColorManager.black),
            ),
            const Divider(),
            const SizedBox(
              height: 40,
            ),
            if (domaineOuput != null)
              resumeCard(AppStrings.domain, domaineOuput!.name!),
            const Divider(),
            if (lieuController.text != "")
              resumeCard(
                AppStrings.lieu,
                lieuController.text,
              ),
            const Divider(),
            if (descriptionController.text != "")
              resumeCard(
                AppStrings.description,
                descriptionController.text,
              ),
            const Divider(),
            const SizedBox(
              height: 10,
            ),
            const Text(
              'Photo',
            ),
            if (imageFileList.isNotEmpty) imageWidget(),
            const SizedBox(
              height: 20,
            )
          ],
        ),
        isActive: _currentStep >= 0,
        state: _currentStep >= 2 ? StepState.complete : StepState.disabled,
      ),
    ];
  }

  Padding imageWidget() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GridView.builder(
          shrinkWrap: true,
          itemCount: imageFileList.length,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3),
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      insetPadding: EdgeInsets.zero,
                      contentPadding: const EdgeInsets.all(0),
                      content: Container(
                        color: ColorManager.black,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: const EdgeInsets.only(left: 10),
                              height: 100,
                              child: CircleAvatar(
                                backgroundColor: ColorManager.white,
                                child: IconButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  icon: Icon(
                                    IconManager.clear,
                                    color: ColorManager.black,
                                  ),
                                ),
                              ),
                            ),
                            Image.file(
                              File(imageFileList[index].path),
                              fit: BoxFit.cover,
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                );
              },
              child: Image.file(
                File(imageFileList[index].path),
                fit: BoxFit.cover,
              ),
            );
          }),
    );
  }

  switchStepsType() {
    setState(() => stepperType == StepperType.vertical
        ? stepperType = StepperType.horizontal
        : stepperType = StepperType.vertical);
  }

  tapped(int step) {
    setState(() => _currentStep = step);
  }

  continued() {
    verifForm();
  }

  cancel() {
    _currentStep > 0 ? setState(() => _currentStep -= 1) : null;
  }

  Column resumeCard(String titre, String data) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          titre,
        ),
        Container(
          width: double.infinity,
          margin: const EdgeInsets.only(bottom: 10),
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
              color: ColorManager.white,
              borderRadius: BorderRadius.circular(5)),
          child: Text(
            data,
            style: getMeduimTextStyle(
              color: ColorManager.black,
              fontSize: 15,
            ),
          ),
        )
      ],
    );
  }
}
