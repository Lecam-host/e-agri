import 'package:eagri/presentations/alert/add_alert_view.dart';
import 'package:eagri/presentations/alert/widget/alert_card_widget.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

import '../../app/constant.dart';
import '../../app/functions.dart';
import '../../domain/model/user_model.dart';
import '../common/buttons/back_button.dart';
import '../ressources/color_manager.dart';
import 'controller/alert_controller.dart';

class AlertView extends StatelessWidget {
  const AlertView({super.key});

  @override
  Widget build(BuildContext context) {
    final AlertController alertController = Get.put(AlertController());
    return  Consumer<UserModel>(
        builder: (context, user, child) => Scaffold(
      backgroundColor: ColorManager.backgroundColor,
      appBar: AppBar(
        leading: const BackButtonCustom(),
        title: const Text('Les alertes'),
      ),
      floatingActionButton:isInScopes(
          UserPermissions.CATALOGUE_PRESTATION_VOIR, user.scopes!)? FloatingActionButton(
        backgroundColor: ColorManager.primary,
        onPressed: () {
          Get.to(const AddAlertView());
        },
        child: Icon(
          Icons.add,
          color: ColorManager.white,
        ),
      ):const SizedBox(),
      body: Obx(
        () => alertController.load.value
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : alertController.listAlerte.isEmpty
                ? const EmptyComponenent()
                : ListView(
                    physics: const BouncingScrollPhysics(),
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    children: [
                      ...alertController.listAlerte.map((e) {
                        return Column(
                          children: [
                            AlertCardWidget(
                              data: e,
                            ),
                            const SizedBox(height: 20)
                          ],
                        );
                      }).toList()
                    ],
                  ),
      ),
    ));
  }
}
