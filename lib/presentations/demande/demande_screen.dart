import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/demande/paiementCash/demande_cash_list_screenn.dart';
import 'package:eagri/presentations/demande/preachat/demande_preachat_screen.dart';
import 'package:eagri/presentations/paiement/controller/paiement_controller.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/icon_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../prestations/list_demande_prestation_screen.dart';
import '../ressources/assets_manager.dart';
import '../shop/controller/shop_controller.dart';
import '../transport/list_demande_screen.dart';

class DemandeScreen extends StatefulWidget {
  const DemandeScreen({super.key});

  @override
  State<DemandeScreen> createState() => _DemandeScreenState();
}

class _DemandeScreenState extends State<DemandeScreen> {
  ShopController shopController = Get.find();
  PaiementController paiementController = Get.put(PaiementController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorManager.backgroundColor,
      appBar: AppBar(
        leading: const BackButtonCustom(),
        title: const Text("Types demandes"),
      ),
      body: SingleChildScrollView(
        child: Column(children: [
          const SizedBox(
            height: 20,
          ),
          demandcard("Offre agricole", () {
            Get.bottomSheet(
      isScrollControlled: true,
      Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children:  [
           
          ListTile(title:const Text("Achat"),
          onTap: (){Get.back();shopController.gitUserListOffreAchat();},
            trailing:const  Icon(IconManager.arrowRight),
          ),
          ListTile(title: const Text("Pre-achat"),
           onTap: (){Get.back();
             Get.to( const DemandePreachatScreen());},
            trailing:const  Icon(IconManager.arrowRight),),
           
          ],
        ),
      ));
           // shopController.gitUserListOffreAchat();
          }, IconAssetManager.tractorIcon),
          const SizedBox(
            height: 20,
          ),
          demandcard("Service transport", () {
            Get.to(const ListDemandeTransportScreen());
          }, IconAssetManager.transportIcon),
          const SizedBox(
            height: 20,
          ),
          demandcard("Prestation", () {
            Get.to(const ListDemandePrestationScreen());
          }, IconAssetManager.serviceIcon),
          const SizedBox(
            height: 20,
          ),
          demandcard("Paiement", () {
            Get.to(const DemandePaiementCashListScreen());
          }, IconAssetManager.payIcon),
          const SizedBox(
            height: 20,
          ),
        ]),
      ),
    );
  }

  Widget demandcard(String titre, Function()? onTap, String icon) {
    return ListTile(
      onTap: onTap,
      leading: CircleAvatar(
        radius: 30,
        backgroundColor: Colors.white,
        child: SizedBox(
          height: 50,
          child: Image.asset(
            icon,
            scale: 3 / 2,
          ),
        ),
      ),
      trailing: Icon(
        IconManager.arrowRight,
        color: ColorManager.black,
      ),
      title: Text(titre),
    );
  }
}
