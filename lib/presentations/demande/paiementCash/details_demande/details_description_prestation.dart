import 'package:eagri/app/constant.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';

import '../../../../app/functions.dart';
import '../../../../domain/model/paiement_model.dart';

class DetatilsDescriptionDemandecash extends StatelessWidget {
  final DemandePaiementCashItem demandeData;

  const DetatilsDescriptionDemandecash({Key? key, required this.demandeData})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            demandeData.product!.title,
            style: getBoldTextStyle(
              fontSize: 14,
              color: ColorManager.primary,
            ),
          ),
          Text(
            "Produit",
            style: getRegularTextStyle(
              color: ColorManager.grey,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            "Info sur la demande",
            style: getMeduimTextStyle(
              color: ColorManager.black,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            color: ColorManager.white,
            width: double.infinity,
            padding: const EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(demandeData.methodId==codePaiementCash? 'Paiement cash':"Paiement à credit",style: getSemiBoldTextStyle(color: ColorManager.blue),),
                 const SizedBox(
                  height: 10,
                ),
                Text(
                  demandeData.isReceive == true
                      ? "Demande reçue"
                      : "Demande envoyée",
                  style: getBoldTextStyle(
                    fontSize: 12,
                    color: ColorManager.black,
                  ),
                ),
                Text(
                  "Type de demande",
                  style: getRegularTextStyle(
                    color: ColorManager.grey,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                if (demandeData.acheteur != null) ...[
                  cardInfo(
                      title: "Demandeur",
                      value:
                          "${demandeData.acheteur!.firstName} ${demandeData.acheteur!.lastName}"),
                  const SizedBox(
                    height: 10,
                  ),
                ],
                if (demandeData.acheteur != null) ...[
                  cardInfo(
                    title: "Numero",
                    value: "${demandeData.acheteur!.number}",
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                ],
                if (
                    demandeData.confirmationCode != null) ...[
                  Text(
                    "${demandeData.confirmationCode}",
                    style: getBoldTextStyle(
                      fontSize: 12,
                      color: ColorManager.jaune,
                    ),
                  ),
                  Text(
                    "Code de confirmation",
                    style: getRegularTextStyle(
                      color: ColorManager.grey,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                ],
                cardInfo(
                  title: "Statut",
                  value: demandeData.isConfirmed == true
                      ? "Paiement confirmé"
                      : demandeData.sellerAgrees == null
                          ? "Pas encore accepté"
                          : demandeData.sellerAgrees == true
                              ? "Accepté"
                              : "Refusé",
                  color: demandeData.isConfirmed == true
                      ? Colors.green
                      : demandeData.sellerAgrees == null
                          ? Colors.orange
                          : demandeData.sellerAgrees == true
                              ? ColorManager.primary2
                              : ColorManager.red,
                ),
                const SizedBox(
                  height: 10,
                ),
                cardInfo(
                  title: "Date de la demannde",
                  value: convertDateWithTime(demandeData.createdAt.toString()),
                ),
                const SizedBox(
                  height: 10,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

Widget cardInfo({required String title, required String value, Color? color}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        value,
        style: getBoldTextStyle(
          fontSize: 12,
          color: color ?? ColorManager.black,
        ),
      ),
      Text(
        title,
        style: getRegularTextStyle(
          color: ColorManager.grey,
        ),
      ),
    ],
  );
}
