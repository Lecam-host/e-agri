import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/app/constant.dart';
import 'package:eagri/data/repository/api/repository_impl.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:page_transition/page_transition.dart';

import '../../../../app/di.dart';
import '../../../../domain/model/paiement_model.dart';
import '../../../common/default_button.dart';
import '../../../paiement/controller/paiement_controller.dart';
import '../../../ressources/size_config.dart';
import '../detail_demande_cash.dart';

class DetailsDemandeCashAppBar extends StatefulWidget {
  final DemandePaiementCashItem demandeData;

  const DetailsDemandeCashAppBar({Key? key, required this.demandeData})
      : super(key: key);

  @override
  State<DetailsDemandeCashAppBar> createState() =>
      _DetailsDemandeCashAppBarState();
}

class _DetailsDemandeCashAppBarState extends State<DetailsDemandeCashAppBar> {
  // AppBar().preferredSize.height provide us the height that appy on our app bar
  Size get preferredSize => Size.fromHeight(AppBar().preferredSize.height);
  late DemandePaiementCashItem demandeDataShow;

  PaiementController paiementController = Get.find();

  @override
  void initState() {
    demandeDataShow = widget.demandeData;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: getProportionateScreenWidth(20), vertical: 10),
        child: Row(
          children: [
            SizedBox(
              height: getProportionateScreenWidth(40),
              width: getProportionateScreenWidth(40),
              child: TextButton(
                style: TextButton.styleFrom(
                  foregroundColor: kPrimaryColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(60),
                  ),
                  backgroundColor: Colors.white,
                  padding: EdgeInsets.zero,
                ),
                onPressed: () => Navigator.pop(context),
                child: SvgPicture.asset(
                  "assets/icons/Back ICon.svg",
                  height: 15,
                ),
              ),
            ),
            const SizedBox(width: 10,),
            const Text('Details de la demande'),
            const Spacer(),

            const SizedBox(
              width: 5,
            ),
            if (demandeDataShow.product != null)
              if (demandeDataShow.sellerAgrees == null)
                TextButton(
                  onPressed: () {
                    showDialog(
                        context: context,
                        barrierDismissible: false,
                        builder: (context) {
                          return AlertDialog(
                            backgroundColor: Colors.white,
                            contentPadding: const EdgeInsets.all(12),
                            content: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                const Text(
                                  "Voulez-vous vraiment réfuser cette demande ?",
                                  textAlign: TextAlign.center,
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                DefaultButton(
                                  isActive: false,
                                  text: "Annuler",
                                  press: () {
                                    Navigator.pop(context);
                                  },
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                DefaultButton(
                                  text: "Confirmer le refus",
                                  press: () async {
                                    Get.dialog(const LoaderComponent());

                                    RepositoryImpl repositoryImpl =
                                        instance<RepositoryImpl>();
                                    await repositoryImpl
                                        .acceptPaiementCash(
                                            demandeDataShow.orderId,
                                            demandeDataShow.productId,
                                            false,
                                            "Refus")
                                        .then((response) {
                                      response.fold((failure) {
                                        Get.back();
                                        showCustomFlushbar(
                                            context,
                                            failure.message,
                                            ColorManager.error,
                                            FlushbarPosition.TOP);
                                      }, (data) {
                                        setState(() {
                                          demandeDataShow.sellerAgrees = false;
                                        });

                                        Get.back();
                                        paiementController
                                            .getListDemandePaimentCashRecu();

                                        Navigator.pop(context);
                                        Navigator.pushReplacement(
                                          context,
                                          PageTransition(
                                            type:
                                                PageTransitionType.rightToLeft,
                                            child: DetailDemandeCash(
                                              demande: demandeDataShow,
                                            ),
                                            isIos: true,
                                            duration: const Duration(
                                                milliseconds: 400),
                                          ),
                                        );

                                        showCustomFlushbar(
                                            context,
                                            "Démande refusé",
                                            ColorManager.succes,
                                            FlushbarPosition.TOP);
                                      });
                                    });
                                  },
                                ),
                              ],
                            ),
                          );
                        });
                  },
                  child: Text(
                    'Refuser',
                    style: getMeduimTextStyle(color: ColorManager.red),
                  ),
                )
            // Container(
            //   padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 5),
            //   decoration: BoxDecoration(
            //     color: Colors.white,
            //     borderRadius: BorderRadius.circular(14),
            //   ),
            //   child: Row(
            //     children: [
            //       Text(
            //         "${product.rating}",
            //         style: const TextStyle(
            //           fontSize: 14,
            //           fontWeight: FontWeight.w600,
            //         ),
            //       ),
            //       const SizedBox(width: 5),
            //       SvgPicture.asset("assets/icons/Star Icon.svg"),
            //     ],
            //   ),
            // ),
            // IconButton(
            //   onPressed: () {},
            //   icon: Icon(
            //     Icons.delete,
            //     color: ColorManager.red,
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
