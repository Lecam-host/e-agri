import 'package:eagri/presentations/paiement/controller/paiement_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../common/buttons/back_button.dart';
import '../../common/state/loader_component.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/styles_manager.dart';
import 'demande_paiement_cash_list.dart';

class DemandePaiementCashListScreen extends StatefulWidget {
  const DemandePaiementCashListScreen({Key? key}) : super(key: key);

  @override
  State<DemandePaiementCashListScreen> createState() =>
      _DemandePaiementCashListScreenState();
}

class _DemandePaiementCashListScreenState
    extends State<DemandePaiementCashListScreen>
    with SingleTickerProviderStateMixin {
  late TabController tabController;
  PaiementController paiementController = Get.put(PaiementController());
  @override
  void initState() {
    paiementController.getListDemandePaimentCashRecu();

    paiementController.getListDemandePaimentCashEnvoyer();

    tabController = TabController(
      length: 2,
      vsync: this,
    );
    super.initState();
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          leading: const BackButtonCustom(),
          title: const Text('Demandes de paiements'),
          bottom: TabBar(
            indicatorColor: ColorManager.primary,
            controller: tabController,
            tabs: [
              Text(
                "Reçues",
                style: getBoldTextStyle(),
              ),
              Text(
                "Envoyées",
                style: getBoldTextStyle(),
              ),
            ],
          ),
        ),
        body: TabBarView(
          controller: tabController,
          children: [
            Obx(
              () => paiementController.loadDemandeRecu.value == true
                  ? const LoaderComponent()
                  : DemandePaiementCashList(
                      listDemande: paiementController.listDemandeRecu,
                    ),
            ),
            Obx(
              () => paiementController.loadDemandeEnvoye.value == true
                  ? const LoaderComponent()
                  : DemandePaiementCashList(
                      listDemande: paiementController.listDemandeEnvoye,
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
