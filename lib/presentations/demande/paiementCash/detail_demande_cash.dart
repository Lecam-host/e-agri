import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/app/constant.dart';
import 'package:eagri/presentations/common/default_button.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../app/di.dart';
import '../../../data/repository/api/repository_impl.dart';
import '../../../domain/model/paiement_model.dart';
import '../../alert/add_alert_view.dart';
import '../../paiement/controller/paiement_controller.dart';
import '../../ressources/styles_manager.dart';
import 'details_demande/details_demande_appbar.dart';
import 'details_demande/details_description_prestation.dart';
import 'details_demande/details_image.dart';

class DetailDemandeCash extends StatefulWidget {
  const DetailDemandeCash({Key? key, required this.demande}) : super(key: key);

  final DemandePaiementCashItem demande;
  @override
  State<DetailDemandeCash> createState() => _DetailDemandeCashState();
}

class _DetailDemandeCashState extends State<DetailDemandeCash> {
  PaiementController paiementController = Get.find();
  TextEditingController codeController = TextEditingController();
  bool isload = true;
  late DemandePaiementCashItem demandeShow;
  getDetailsProduct() async {
    setState(() {
      isload = true;
    });

    RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
    await repositoryImpl
        .getDetailProduct(demandeShow.productId)
        .then((response) async {
      await response.fold((failure) {}, (data) async {
        demandeShow.product = (await data);
        setState(() {
          demandeShow;
        });
      });
    });
    setState(() {
      isload = false;
    });
  }

  @override
  void initState() {
    demandeShow = widget.demande;
    getDetailsProduct();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
    
        bottomNavigationBar: isload == false
            ? demandeShow.product != null
                ? Container(
                    margin: const EdgeInsets.all(10),
                    child: demandeShow.isConfirmed == null
                        ? demandeShow.isReceive == true
                            ? demandeShow.sellerAgrees == null
                                ? DefaultButton(
                                    text: "Valider",
                                    press: () {
                                      showDialog(
                                          context: context,
                                          barrierDismissible: false,
                                          builder: (context) {
                                            return AlertDialog(
                                              backgroundColor: Colors.white,
                                              contentPadding:
                                                  const EdgeInsets.all(12),
                                              content: Column(
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                                  const Text(
                                                    'Vous-voulez vraiment accepter cette demande ?',
                                                    textAlign: TextAlign.center,
                                                  ),
                                                  const SizedBox(
                                                    height: 10,
                                                  ),
                                                  DefaultButton(
                                                    isActive: false,
                                                    text: "Annuler",
                                                    press: () {
                                                      Navigator.pop(context);
                                                    },
                                                  ),
                                                  const SizedBox(
                                                    height: 10,
                                                  ),
                                                  DefaultButton(
                                                    text: "Accepter",
                                                    press: () async {
                                                      Navigator.pop(context);

                                                      bool result =
                                                          await paiementController
                                                              .acceptDemande(
                                                                  demandeShow
                                                                      .orderId,
                                                                  demandeShow
                                                                      .productId);

                                                      if (result == true) {
                                                        demandeShow
                                                                .sellerAgrees =
                                                            true;
                                                      }
                                                      setState(() {
                                                        demandeShow;
                                                      });
                                                    },
                                                  ),
                                                ],
                                              ),
                                            );
                                          });
                                    },
                                  )
                                : const SizedBox()
                            : demandeShow.sellerAgrees == true &&demandeShow.methodId==codePaiementCash
                                ? DefaultButton(
                                    text: "Confirmer le paiement",
                                    press: () {
                                      showDialog(
                                          context: context,
                                          barrierDismissible: false,
                                          builder: (context) {
                                            return AlertDialog(
                                              backgroundColor: Colors.white,
                                              contentPadding:
                                                  const EdgeInsets.all(12),
                                              content: Column(
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                                  const Text(
                                                    "Vous confirmez que le client a été payé",
                                                    textAlign: TextAlign.center,
                                                  ),
                                                  const SizedBox(
                                                    height: 10,
                                                  ),
                                                  DefaultButton(
                                                    isActive: false,
                                                    text: "Annuler",
                                                    press: () {
                                                      Navigator.pop(context);
                                                    },
                                                  ),
                                                  const SizedBox(
                                                    height: 10,
                                                  ),
                                                  DefaultButton(
                                                    text: "Confirmer",
                                                    press: () async {
                                                      Navigator.pop(context);
                                                      confirmPaeiment();
                                                    },
                                                  ),
                                                ],
                                              ),
                                            );
                                          });
                                    },
                                  )
                                :demandeShow.sellerAgrees == true &&demandeShow.methodId==codePaiementCredit? DefaultButton(
                                                    text: "Payer",
                                                    press: () {
                                                      Navigator.pop(context);
                                                    },
                                                  ) :const SizedBox()
                        : demandeShow.isConfirmed == true
                            ? DefaultButton(
                                text: "Signaler l'etat de la route",
                                press: () {
                                  Get.to(const AddAlertView());
                                },
                              )
                            : const SizedBox(),
                  )
                : const SizedBox()
            : const SizedBox(),
        backgroundColor: ColorManager.backgroundColor,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(AppBar().preferredSize.height),
            child: DetailsDemandeCashAppBar(
              demandeData: demandeShow,
            )),
        body: isload == false
            ? demandeShow.product != null
                ? Hero(
                    tag: "demande",
                    child: ListView(
                      shrinkWrap: true,
                      children: [
                        DetailsDemandeCashImages(
                          dataDemande: demandeShow,
                        ),
                        DetatilsDescriptionDemandecash(
                          demandeData: demandeShow,
                        )
                      ],
                    ),
                  )
                : const EmptyComponenent(
                    message: "Ce produit n'est plus disponible",
                  )
            : const LoaderComponent());
  }

  confirmPaeiment() {
    Get.bottomSheet(
      isScrollControlled: false,
      Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10)),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Entrer le code de confirmation",
                style: getBoldTextStyle(color: Colors.black, fontSize: 20),
              ),
              const SizedBox(
                height: 10,
              ),
              TextField(
                controller: codeController,
                maxLines: null,
                decoration: const InputDecoration(
                  hintText: "Entrer le code ",
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 100,
                    child: DefaultButton(
                      color: ColorManager.grey,
                      text: "Annuler",
                      press: () {
                        Get.back();
                      },
                    ),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  SizedBox(
                    width: 100,
                    child: DefaultButton(
                      text: "Envoyer",
                      press: () async {
                        Get.back();
                        if (codeController.text == "") {
                          showCustomFlushbar(context, "Entrer le code",
                              ColorManager.error, FlushbarPosition.TOP);
                        } else {
                          bool result =
                              await paiementController.confirmPaiementCash(
                            demandeShow.id,
                            codeController.text,
                          );
                          if (result == true) {
                            demandeShow.isConfirmed = true;
                          }
                          setState(() {
                            demandeShow;
                          });
                        }
                      },
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
