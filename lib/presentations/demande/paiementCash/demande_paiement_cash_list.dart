import 'package:eagri/app/constant.dart';
import 'package:eagri/app/functions.dart';
import 'package:eagri/domain/model/paiement_model.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/ressources/assets_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:page_transition/page_transition.dart';

import '../../ressources/color_manager.dart';
import 'detail_demande_cash.dart';

class DemandePaiementCashList extends StatelessWidget {
  const DemandePaiementCashList({Key? key, required this.listDemande})
      : super(key: key);
  final RxList<DemandePaiementCashItem> listDemande;
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => listDemande.isNotEmpty
          ? ListView.builder(
              shrinkWrap: true,
              itemCount: listDemande.length,
              itemBuilder: (context, index) => Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      PageTransition(
                        type: PageTransitionType.bottomToTop,
                        child: DetailDemandeCash(
                          demande: listDemande[index],
                        ),
                        isIos: true,
                        duration: const Duration(milliseconds: 400),
                      ),
                    );
                  },
                  child: Container(
                    width: double.infinity,
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    color: ColorManager.white,
                    child: Stack(
                      children: [
                        Positioned(right: 0,child: Text(listDemande[index].methodId==codePaiementCredit?"Credit":"Cash",style: getBoldTextStyle(color: ColorManager.blue,),),
                        ),
                        Row(
                          children: [
                            CircleAvatar(
                              backgroundColor:
                                  listDemande[index].isReceive == true
                                      ? ColorManager.primary4
                                      : ColorManager.primaryOpacity70,
                              child: Image.asset(
                                listDemande[index].isReceive == true
                                    ? IconAssetManager.receiveIcon
                                    : IconAssetManager.sendIcon,
                                width: 25,
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                // Text(
                                //   listDemande[index].product!.title,
                                //   style: getMeduimTextStyle(
                                //     color: ColorManager.black,
                                //     fontSize: 12,
                                //   ),
                                //   maxLines: 1,
                                //   overflow: TextOverflow.ellipsis,
                                // ),

                                if (listDemande[index].acheteur != null ||
                                    listDemande[index].vendeur != null) ...[
                                  if (listDemande[index].isReceive == true)
                                    Text(
                                      "${listDemande[index].acheteur!.firstName.toString()} ${listDemande[index].acheteur!.lastName.toString()}",
                                      style: getMeduimTextStyle(
                                        color: ColorManager.black,
                                        fontSize: 12,
                                      ),
                                    ),
                                  if (listDemande[index].isReceive == false)
                                    Text(
                                      "${listDemande[index].vendeur!.firstName.toString()} ${listDemande[index].vendeur!.lastName.toString()}",
                                      style: getMeduimTextStyle(
                                        color: ColorManager.black,
                                        fontSize: 12,
                                      ),
                                    ),
                                  Text(
                                    listDemande[index].isReceive == false
                                        ? "Fournisseur"
                                        : 'Demandeur',
                                    style: getRegularTextStyle(
                                      color: ColorManager.grey1,
                                      fontSize: 12,
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                ],
                                Text(
                                  "${convertDateWithTime(listDemande[index].createdAt.toString())} ",
                                  style: getMeduimTextStyle(
                                    color: ColorManager.black,
                                    fontSize: 12,
                                  ),
                                ),
                                Text(
                                  'Date de demande',
                                  style: getRegularTextStyle(
                                    color: ColorManager.grey1,
                                    fontSize: 12,
                                  ),
                                ),
                                Text(
                                  listDemande[index].isConfirmed == true
                                      ? "Paiement confirmé"
                                      : listDemande[index].sellerAgrees ==
                                              null
                                          ? "Pas encore accepté"
                                          : listDemande[index]
                                                      .sellerAgrees ==
                                                  true
                                              ? "Accepté"
                                              : "Refusé",
                                  style: getMeduimTextStyle(
                                    color: listDemande[index].isConfirmed ==
                                            true
                                        ? Colors.green
                                        : listDemande[index].sellerAgrees ==
                                                null
                                            ? Colors.orange
                                            : listDemande[index]
                                                        .sellerAgrees ==
                                                    true
                                                ? ColorManager.primary2
                                                : ColorManager.red,
                                    fontSize: 12,
                                  ),
                                ),
                                Text(
                                  'Statut',
                                  style: getRegularTextStyle(
                                    color: ColorManager.grey1,
                                    fontSize: 12,
                                  ),
                                ),
                              ],
                            ),
                            
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            )
          : const EmptyComponenent(
              message: "Aucune demande trouvée",
            ),
    );
  }
}
