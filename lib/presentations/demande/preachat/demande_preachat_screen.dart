import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/app/app_prefs.dart';
import 'package:eagri/data/repository/api/repository_impl.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shimmer/shimmer.dart';

import '../../../app/di.dart';
import '../../../data/request/request.dart';
import '../../../domain/model/achat_or_preachat_model.dart';
import '../../../domain/model/info_by_id_model.dart';
import '../../catalogue/addPrduct/add_product_view.dart';
import '../../common/buttons/back_button.dart';
import '../../common/cache_network_image.dart';
import '../../common/state/empty_component.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/icon_manager.dart';
import '../../ressources/styles_manager.dart';
import 'details_demande_preachat.dart';

class DemandePreachatScreen extends StatefulWidget {
  const DemandePreachatScreen({super.key});

  @override
  State<DemandePreachatScreen> createState() => _DemandePreachatScreenState();
}

class _DemandePreachatScreenState extends State<DemandePreachatScreen> with SingleTickerProviderStateMixin{
  late TabController _tabController;
  RepositoryImpl repositoryImpl=instance<RepositoryImpl>();
  AppSharedPreference appSharedPreference=instance<AppSharedPreference>();
  List<DemandePreachatModel> listDemandeRecu=[];
  List<DemandePreachatModel> listDemandeEnvoye=[];
  bool isLoadDemandeRecu=true;
  bool isLoadDemandeEnvoye=true;
 
  @override
  void initState() {
    // shopController.gitUserListOffrePreAchat();
    // shopController.getlistSpreculationOffreAchat(true, widget.speculation);
    getDemandeRecu();
    getDemandeEnvoye();
    _tabController = TabController(initialIndex: 0, length: 2, vsync: this,);
    super.initState();
  }
  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

getDemandeRecu()async{
 await appSharedPreference.getUserInformation().then((user) async{

await repositoryImpl.getDemandeAchatOrPreAchat(user.id!, "FOURNISSEUR", "PREACHAT").then((response) async{
  response.fold((failure) {
    setState(() {
      isLoadDemandeRecu=false;
    });
  showCustomFlushbar(context, failure.message, ColorManager.error, FlushbarPosition.TOP);
  }, (listData){
    setState(() {
       isLoadDemandeRecu=false;
      listDemandeRecu=listData.data.items;
    });
  
  });
});

  });
}
getDemandeEnvoye()async{
 await appSharedPreference.getUserInformation().then((user) async{

await repositoryImpl.getDemandeAchatOrPreAchat(user.id!, "BUYER", "PREACHAT").then((response) async{
  response.fold((failure) {
    setState(() {
      isLoadDemandeEnvoye=false;
    });
  showCustomFlushbar(context, failure.message, ColorManager.error, FlushbarPosition.TOP);
  }, (listData){
    setState(() {
       isLoadDemandeEnvoye=false;
      listDemandeEnvoye=listData.data.items;
    });
  
  });
});

  });
}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
        appBar: AppBar(
          leading: const BackButtonCustom(),
          title:  Text(
            "Mes demandes de preachat",
           style: getBoldTextStyle(color: ColorManager.black),
          ),
          bottom: TabBar(
                      controller: _tabController,
                      indicatorColor: ColorManager.primary,
                      tabs:[ Tab(
            child: Text(
              "Réçu",
              style: getMeduimTextStyle(color: ColorManager.black),
            ),
          ), Tab(
            child: Text(
              "Envoyé",
              style: getMeduimTextStyle(color: ColorManager.black),
            ),
          ),],
                    ),
        ),
        floatingActionButton:
            FloatingActionButton(
                    backgroundColor: ColorManager.primary,
                    onPressed: () {
                                       Get.to(const AddProductView(
                                                  isOffreAchat: true,
                                                  typeOffer: "PREACHAT",
                                                ));
                      //  Get.bottomSheet(Container(
                      //    padding: const EdgeInsets.all(10),
                      //   margin: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
                      //   decoration: BoxDecoration(
                      //       color: Colors.white, borderRadius: BorderRadius.circular(10)),
                      //   child:  Column(
                      //     mainAxisSize: MainAxisSize.min,
                      //     children: [
                      //    const Text("Choisir le type d'offre"),
                      //  const SizedBox(height: 10,),
                      //   ListTile(
                      //     trailing:const Icon(IconManager.arrowRight),
                      //     title: const Text('Achat'),onTap: (){
                      //                  Get.to(const AddProductView(
                      //                             isOffreAchat: true,
                      //                             typeOffer: "ACHAT",
                      //                           ));
                      //   },),  
                      //   ListTile(
                      //      trailing:const Icon(IconManager.arrowRight),
                      //     title: const Text('Pre-achat'),onTap: (){
                      //                  Get.to(const AddProductView(
                      //                             isOffreAchat: true,
                      //                             typeOffer: "PREACHAT",
                      //                           ));
                      //   },), 
                      //  ],),));
                     
                    },
                    child: Icon(
                      Icons.add,
                      color: ColorManager.white,
                    ),
                  ),
               
        body:  TabBarView(
                  controller: _tabController,
          children: [
       isLoadDemandeRecu==false?   demandePreAchatList(listDemandeRecu,true):const LoaderComponent(),

      isLoadDemandeEnvoye ==false?   demandePreAchatList(listDemandeEnvoye,false):const LoaderComponent(),
            
          ],
        ));
  }


  Widget demandePreAchatList(List<DemandePreachatModel> listData ,bool isReceive) {
    return  RefreshIndicator(
      onRefresh: ()async{
        if(isReceive==true){
        await  getDemandeRecu();
   
        }else{
         await  getDemandeEnvoye();
        }
      },
      child: SingleChildScrollView(
         physics: const AlwaysScrollableScrollPhysics(),
                  child: Container(
                    margin: const EdgeInsets.symmetric(horizontal: 10),
                    child:  Column(
                            children: [
                            
                              ListView.builder(
                              physics: const NeverScrollableScrollPhysics(),
                              padding: const EdgeInsets.only(bottom: 10),
                              shrinkWrap: true,
                              itemCount: listData.length,
                              itemBuilder: (context, index) {
                                return DemandeCard(demande :listData[index],isReceive:isReceive);
                              },
                            ),
                            if(listData.isEmpty) 
                                const EmptyComponenent(
                                  message: "Aucun élément trouvé",
                                ),
                              
                            ],
                          ),
                  ),
                ),
    );
  }

  Shimmer shimmerCustom() {
    return Shimmer.fromColors(
      baseColor: const Color.fromARGB(255, 115, 115, 115),
      highlightColor: const Color.fromARGB(255, 181, 181, 181),
      child: Container(
        margin: const EdgeInsets.only(top: 10),
        height: 60,
        child: Row(
          children: [
            Container(
              height: 60,
              margin: const EdgeInsets.only(bottom: 10, left: 10),
              width: 40,
              decoration: BoxDecoration(
                color: ColorManager.white,
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  color: ColorManager.white,
                  height: 10,
                  width: 150,
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  color: ColorManager.white,
                  height: 10,
                  width: 200,
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  color: ColorManager.white,
                  height: 10,
                  width: 100,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}


class DemandeCard extends StatefulWidget {
  const DemandeCard({super.key,required this.demande,required this.isReceive, });
final DemandePreachatModel demande;final bool isReceive;
  @override  
  State<DemandeCard> createState() => _DemandeCardState();
}

class _DemandeCardState extends State<DemandeCard> {
   SpeculationInfoModel? speculation;
   RepositoryImpl repositoryImpl=instance<RepositoryImpl>();
getSpeculationInfo(){
  repositoryImpl.getInfoById(InfoByIdRequest(speculation:widget. demande.offer.product.speculationId)).then((response){
    response.fold((failure) {}, (data) {
    if(mounted) {setState(() {
        speculation=data.speculation;
      });} 
      });
   } );
}
@override
  void initState() {
    getSpeculationInfo();
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    return Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      PageTransition(
                        type: PageTransitionType.bottomToTop,
                        child: DetailsDemandePreAchat(
                          demande: widget.demande,
                          isReceive:widget.isReceive ,
                        ),
                        isIos: true,
                        duration: const Duration(milliseconds: 400),
                      ),
                    );
                  },
                  child: Stack(
                    children: [
                      Container(
                        width: double.infinity,
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        color: ColorManager.white,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                               if (widget. demande.offer .images.isNotEmpty)...[ Container(
                width: 60.0,
                height: 80,
                decoration: BoxDecoration(
                  color: ColorManager.grey,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(5.0),
                  child: CacheNetworkImage(
                    image: widget. demande.offer.images[0].link,
                  ),
                ),
              ),]else if(speculation!=null)...[
                 Container(
                width: 60.0,
                height: 80,
                decoration: BoxDecoration(
                  color: ColorManager.grey,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(5.0),
                  child: CacheNetworkImage(
                    image:speculation!.imageUrl,
                  ),
                ),)
              ]else...[  Container(
                width: 60.0,
                height: 80,
                decoration: BoxDecoration(
                  color: ColorManager.grey,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(5.0),
                    child: const Icon(Icons.image)),
              ),],
             
            if (widget.demande.offer.images.isEmpty)
            
                                const SizedBox(
                                  width: 10,
                                ),
                            
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                     if( speculation!=null)  
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                         speculation!.name,
                                          style: getMeduimTextStyle(
                                            color: ColorManager.black,
                                            fontSize: 12,
                                          ),
                                        ),
                                         Text(
                                     "Produit",
                                      style: getRegularTextStyle(
                                        color: ColorManager.grey1,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ],
                                ),
                                    Text(
                                     widget.isReceive==true? "${widget. demande.buyer.firstname .toString()} ${widget. demande.fournisseur .lastname .toString()}":"${widget. demande.buyer.firstname .toString()} ${widget. demande.fournisseur.lastname .toString()}",
                                      style: getMeduimTextStyle(
                                        color: ColorManager.black,
                                        fontSize: 12,
                                      ),
                                    ),
                                    Text(
                                         widget.isReceive==true? 'Acheteur':"Vendeur",
                                      style: getRegularTextStyle(
                                        color: ColorManager.grey1,
                                        fontSize: 12,
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 5,
                                    ),
                                  if( widget.demande.accepted==true) 
                                   Text("Accepté", style: getMeduimTextStyle(
                                        color: ColorManager.primary,
                                        fontSize: 12,
                                      ),),
                                      
                                  ],
                                ),
                              ],
                            ),
                            IconButton(
                                onPressed: () {},
                                icon: const Icon(IconManager.arrowRight))
                            // if (listPrestation[index].prestationwidget. demande.Data.isValid ==
                            //     false)
                            // TextButton(
                            //     onPressed: () {
                            //       showDialog(
                            //           context: context,
                            //           barrierDismissible: false,
                            //           builder: (context) {
                            //             return AlertDialog(
                            //               backgroundColor: Colors.white,
                            //               contentPadding: const EdgeInsets.all(12),
                            //               content: Column(
                            //                 mainAxisSize: MainAxisSize.min,
                            //                 children: [
                            //                   const Text(
                            //                     'Vous-voulez vraiment accepter cette démande',
                            //                     textAlign: TextAlign.center,
                            //                   ),
                            //                   TextButton(
                            //                     onPressed: () {
                            //                       Navigator.pop(context);
                            //                     },
                            //                     child: Text('Non',
                            //                         style: getBoldTextStyle(
                            //                             color: ColorManager.grey)),
                            //                   ),
                            //                   TextButton(
                            //                     onPressed: () async {
                            //                       Navigator.pop(context);
                            //                       await prestationController
                            //                           .validateDemandePrestation(
                            //                               listPrestation[index]
                            //                                   .prestation
                            //                                   .id);
                            //                     },
                            //                     child: Text('Oui',
                            //                         style: getBoldTextStyle(
                            //                             color: ColorManager.primary,
                            //                             fontSize: 14)),
                            //                   ),
                            //                 ],
                            //               ),
                            //             );
                            //           });
                            //     },
                            //     child: Text(
                            //       "Accepter",
                            //       style: getMeduimTextStyle(
                            //         color: ColorManager.primary,
                            //         fontSize: 12,
                            //       ),
                            //     ))
                          ],
                        ),
                      ),
                      // if (listPrestation[index].prestationDemandeData.isValid ==
                      //     true)
                      //   Positioned(
                      //     top: 50,
                      //     left: 30,
                      //     child: CircleAvatar(
                      //       radius: 10,
                      //       backgroundColor: ColorManager.primary,
                      //       child: Icon(
                      //         Icons.check,
                      //         size: 10,
                      //         color: ColorManager.white,
                      //       ),
                      //     ),
                      //   ),
                    ],
                  ),
                ),
              );
  }
}