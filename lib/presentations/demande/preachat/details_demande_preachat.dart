import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/common/state/loader.dart';
import 'package:flutter/material.dart';

import '../../../app/di.dart';
import '../../../app/functions.dart';
import '../../../data/repository/api/repository_impl.dart';
import '../../../data/request/request.dart';
import '../../../domain/model/achat_or_preachat_model.dart';
import '../../../domain/model/info_by_id_model.dart';
import '../../common/default_button.dart';
import '../../common/multi_image_widget.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/styles_manager.dart';

class DetailsDemandePreAchat extends StatefulWidget {
   const DetailsDemandePreAchat({super.key,required this.demande,required this.isReceive, });
final DemandePreachatModel demande;final bool isReceive;

  @override
  State<DetailsDemandePreAchat> createState() => _DetailsDemandePreAchatState();
}

class _DetailsDemandePreAchatState extends State<DetailsDemandePreAchat> {
  List<String> listImage =[] ;
     SpeculationInfoModel? speculation;
   RepositoryImpl repositoryImpl=instance<RepositoryImpl>();
 late  DemandePreachatModel demandeShow;
   validedOrInvalidedDemandePreAchat(bool state)async{
     loaderDialog(context,text:"Veuillez patientez");
   await repositoryImpl.validedOrInvalidedDemandePreAchat(widget.demande.id,state).then((response) async{
      response.fold((failure) {
         Navigator.pop(context);
        showCustomFlushbar(context, failure.message, ColorManager.error, FlushbarPosition.TOP);},
         (data) {
         Navigator.pop(context);
         showCustomFlushbar(context, state==true?"Demande acceptée":"Demande refusée", ColorManager.succes, FlushbarPosition.TOP);
        
        setState(() {
           if(state==true){
            demandeShow.accepted=true;
           }else{
              demandeShow.refused=false;
           }
           
        });
     


      });
    });
   }
getSpeculationInfo(){
  repositoryImpl.getInfoById(InfoByIdRequest(speculation:widget. demande.offer.product.speculationId)).then((response){
    response.fold((failure) {}, (data) {
    if(mounted) {setState(() {
      if(widget.demande.offer.images.isEmpty){
        if(data.speculation!=null) {
          listImage=[data.speculation!.imageUrl];
        }
      }
        speculation=data.speculation;
      });} 
      });
   } );
}
  @override
  void initState() {
    getSpeculationInfo();
    demandeShow = widget.demande;
   widget.demande.offer.images.map((e) => listImage.add(e.link));
    super.initState();
    
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       
      appBar: AppBar(leading:const BackButtonCustom(),title:const Text('Details de la demande'),),
      body: SingleChildScrollView(
        child: Column(children: [
      MultiImageWidget(images: listImage,),
      Container(
      margin: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if( speculation!=null)
          Text(
            speculation!.name,
            style: getBoldTextStyle(
              fontSize: 14,
              color: ColorManager.primary,
            ),
          ),
          Text(
            "Produit",
            style: getRegularTextStyle(
              color: ColorManager.grey,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            "Info sur la demande",
            style: getMeduimTextStyle(
              color: ColorManager.black,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            color: ColorManager.white,
            width: double.infinity,
            padding: const EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // //Text( widget.demande.methodId==codePaiementCash? 'Paiement cash':"Paiement à credit",style: getSemiBoldTextStyle(color: ColorManager.blue),),
                //  const SizedBox(
                //   height: 10,
                // ),
                Text(
                   widget.isReceive == true
                      ? "Demande reçue"
                      : "Demande envoyée",
                  style: getBoldTextStyle(
                    fontSize: 12,
                    color: ColorManager.black,
                  ),
                ),
                Text(
                  "Type de demande",
                  style: getRegularTextStyle(
                    color: ColorManager.grey,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                 cardInfo(
                   color: ColorManager.blue,
                      title: "Quantité",
                      value:
                         "${widget.demande.offer.product.quantity} ${widget.demande.offer.product.unitOfMeasurment}"),
                        const SizedBox(
                  height: 10,
                ),   cardInfo(
                  color: ColorManager.red,
                      title: "Prix unitaire",
                      value:
                         "${separateur(widget.demande.offer.product.priceU) } FCFA "),
                const SizedBox(
                  height: 10,
                ),
                if ( widget.isReceive ==true) ...[
                  cardInfo(
                      title: "Demandeur",
                      value:
                          "${ widget.demande.buyer.firstname} ${ widget.demande.buyer.lastname}"),
                  const SizedBox(
                    height: 10,
                  ),
                ],if ( widget.isReceive ==false) ...[
                  cardInfo(
                      title: "Fournisseur",
                      value:
                          "${ widget.demande.fournisseur.firstname} ${ widget.demande.fournisseur.lastname}"),
                  const SizedBox(
                    height: 10,
                  ),
                ],
                cardInfo(
                    title: "Numero",
                    value:widget.isReceive ==true? widget.demande.buyer.phone :widget.demande.fournisseur.phone,
                  ),
                 
                
               if(demandeShow.accepted==true)...[ const SizedBox(
                    height: 10,
                  ), cardInfo(
                  title: "Statut",
                  value: "Acceptée",
                  color: ColorManager.primary
                ),],
               
                const SizedBox(
                  height: 10,
                ),
                cardInfo(
                  title: "Date de la demannde",
                  value: convertDateWithTime( widget.demande.createdAt.toString()),
                ),
                const SizedBox(
                  height: 20,
                ),
                if(demandeShow.accepted==false)
                 Row(children: [
                    Expanded(
                              child: DefaultButton(
                              text: "Refuser",press: (){
                                
                                showDialog(
                                          context: context,
                                          barrierDismissible: false,
                                          builder: (context) {
                                            return AlertDialog(
                                              backgroundColor: Colors.white,
                                              contentPadding:
                                                  const EdgeInsets.all(12),
                                              content: Column(
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                                  const Text(
                                                    'Vous-voulez vraiment refuser cette demande ?',
                                                    textAlign: TextAlign.center,
                                                  ),
                                                  const SizedBox(
                                                    height: 10,
                                                  ),
                                                  DefaultButton(
                                                    isActive: false,
                                                    text: "Annuler",
                                                    press: () {
                                                      Navigator.pop(context);
                                                    },
                                                  ),
                                                  const SizedBox(
                                                    height: 10,
                                                  ),
                                                  DefaultButton(
                                                    color: ColorManager.red,
                                                    text: "Refuser",
                                                    press: () async {
                                                       Navigator.pop(context);
                                                      
                                                    await validedOrInvalidedDemandePreAchat(false);
                                                    
                                                    },
                                                  ),
                                                ],
                                              ),
                                            );
                                          });
                              }, color: ColorManager.red ,),
                          ),
                  const SizedBox(
                              width: 5,
                            ),
                  Expanded(
                    child: DefaultButton(
                               text: "Valider",press: (){
                               
                                 showDialog(
                                          context: context,
                                          barrierDismissible: false,
                                          builder: (context) {
                                            return AlertDialog(
                                              backgroundColor: Colors.white,
                                              contentPadding:
                                                  const EdgeInsets.all(12),
                                              content: Column(
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                                  const Text(
                                                    'Vous-voulez vraiment accepter cette demande ?',
                                                    textAlign: TextAlign.center,
                                                  ),
                                                  const SizedBox(
                                                    height: 10,
                                                  ),
                                                  DefaultButton(
                                                    isActive: false,
                                                    text: "Annuler",
                                                    press: () {
                                                      Navigator.pop(context);
                                                    },
                                                  ),
                                                  const SizedBox(
                                                    height: 10,
                                                  ),
                                                  DefaultButton(
                                                    text: "Accepter",
                                                    press: () async {
                                                       Navigator.pop(context);
                                                     validedOrInvalidedDemandePreAchat(true);
                                                    },
                                                  ),
                                                ],
                                              ),
                                            );
                                          });
                            },
                    color: ColorManager.primary,),
                  ),
                ],),const SizedBox(
                              height: 10,
                            ),
              ],
            ),
          )
        ],
      ),
    ),
   
    
          ],),
      ),);
  }
}

Widget cardInfo({required String title, required String value, Color? color}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        value,
        style: getBoldTextStyle(
          fontSize: 12,
          color: color ?? ColorManager.black,
        ),
      ),
      Text(
        title,
        style: getRegularTextStyle(
          color: ColorManager.grey,
        ),
      ),
    ],
  );
}