import 'dart:async';

import 'package:eagri/controllers/user_controller.dart';
import 'package:get/get.dart';

import '../../../app/di.dart';
import '../../../app/functions.dart';
import '../../../domain/model/notification_model.dart';
import '../../../service/notification/notification_service.dart';
import '../notifications_view.dart';

class NotificationController extends GetxController {
  NotificationServiceImpl notificationServiceImpl =
      instance<NotificationServiceImpl>();

  UserController userController = Get.find();
  var isLoadNotification = true.obs;
  var listNotification = <NotificationModel>[].obs;
  var listNotificationWithDate = <NotificationWithDateModel>[].obs;
  var nbNotifNotRead = 0.obs;

  @override
  void onInit() {
    getUserNotification();
    super.onInit();
  }

  Future<bool> deleteNotification(int index,NotificationModel notification
     ) async {
    Timer(const Duration(milliseconds: 100), () {
      listNotification.removeAt(index);
        });
   
    bool result = false;
    notificationServiceImpl
        .deleteNotification(notification.id)
        .then((response) {
      response.fold((failure) async {
        result = false;
      }, (data) async {
        result = true;
      });
    });
    return result;
  }

  Future<bool> readNotification(String idNotification) async {
    bool result = false;
    await notificationServiceImpl
        .readNotification(idNotification)
        .then((response) async {
      // inspect(response);
      await response.fold((failure) async {
        result = false;
      }, (data) async {
        result = true;
      });
    });
    return result;
  }

  getUserNotification() async {
    String dateNotification = "";
    nbNotifNotRead.value = 0;
    List<NotificationModel> listNotifResult = [];
    if (userController.userInfo.value.isConnected!) {
      await notificationServiceImpl
          .getUserNotification(userController.userInfo.value.id!)
          .then((response) {
        response.fold((failure) {}, (data) async {
          listNotification.value=data.data;
          for (NotificationModel element in data.data) {
            //comptez le nombre de notification non lu
            if (element.isRead == 0) {
              nbNotifNotRead = nbNotifNotRead + 1;
            }
          }
          listNotification.value = data.data;
          if (data.data.isNotEmpty) {
            dateNotification = data.data[0].createdAt.toString();
          }
          await Future.forEach(data.data, (element) {
            bool isEvenDate = convertDate(dateNotification) ==
                convertDate(element.createdAt.toString());
            if (isEvenDate == false) {
              listNotificationWithDate.add(NotificationWithDateModel(
                  listNotif: listNotifResult,
                  title: convertDate(dateNotification)));
              dateNotification = element.createdAt.toString();
              listNotifResult = [];
            } else {
              listNotifResult.add(element);
            }
          });
        });
        isLoadNotification.value = false;
      });
    }
  }
}
