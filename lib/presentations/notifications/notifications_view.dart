import 'package:eagri/app/functions.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../domain/model/notification_model.dart';
import '../common/state/empty_component.dart';
import '../ressources/color_manager.dart';
import '../ressources/font_manager.dart';
import '../ressources/styles_manager.dart';
import 'controller/notification_controller.dart';

class NotificationsView extends StatefulWidget {
  const NotificationsView({Key? key}) : super(key: key);

  @override
  State<NotificationsView> createState() => _NotificationsViewState();
}

class _NotificationsViewState extends State<NotificationsView> {
  NotificationController notificationController = Get.find();
  String dateNotification = "";
  @override
  void initState() {
    // notificationController.getUserNotification();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorManager.backgroundColor,
      appBar: AppBar(
        title: const Text("Notification"),
        leading: const BackButtonCustom(),
      ),
      body: Obx(
        () => notificationController.isLoadNotification.value == true
            ? const LoaderComponent()
            :notificationController.listNotification .isNotEmpty? ListView.builder(
               itemCount:notificationController.listNotification .length,
                      itemBuilder: (context, index) => Column(
                                  children: [
                                    StatefulBuilder(
                                      builder: (context, changeState) {
                                        double height = 100;
                                        return AnimatedContainer(
                                          curve: Curves.fastOutSlowIn,
                                          width: double.infinity,
                                          height: height,
                                          duration: const Duration(seconds: 1),
                                          child: height > 0
                                              ? AlertListTile(
                                                  onTap: () {
                                                    notificationController
                                                        .readNotification(
                                                            notificationController.listNotification [index]
                                                                .id);
                                                    showDetailNotification(
                                                        notificationController.listNotification [index]);
                                                  },
                                                  onDelete: () {
                                                    changeState(() {
                                                      height = 0;
                                                    });
                                                  
                                                    notificationController
                                                        .deleteNotification(index,
                                                            notificationController.listNotification [index],
                                                          )
                                                        .then((value) {
                                                      if (mounted) {
                                                        setState(() {
                                                         notificationController.listNotification ;
                                                        });
                                                        super.widget;
                                                        // setState(() {
                                                        //   notShowDeleteErreur = value;
                                                        //   isloadDelete = false;
                                                        // });
                                                      }
                                                    });
                                                  },
                                                  notification:
                                                      notificationController.listNotification [index],
                                                  
                                                )
                                              : Container(
                                                  width: double.infinity,
                                                  height: height,
                                                  color: notificationController
                                                              .listNotificationWithDate[
                                                                  index]
                                                              .listNotif[index]
                                                              .isRead ==
                                                          1
                                                      ? ColorManager.white
                                                      : ColorManager.blue
                                                          .withOpacity(0.2),
                                                ),
                                        );
                                      },
                                    ),
                                    const Divider(
                                      height: 2,
                                    ),
                                  ],
                                )
            )
            // : notificationController.listNotificationWithDate.isNotEmpty
            //     ? ListView.builder(
            //         shrinkWrap: true,
            //         itemCount:
            //             notificationController.listNotificationWithDate.length,
            //         itemBuilder: (context, index) => Padding(
            //             padding: const EdgeInsets.symmetric(vertical: 2),
            //             child: Column(
            //               crossAxisAlignment: CrossAxisAlignment.start,
            //               children: [
            //                 const SizedBox(
            //                   height: 10,
            //                 ),
            //                 Text(
            //                   notificationController
            //                       .listNotificationWithDate[index].title
            //                       .toString(),
            //                   style: getMeduimTextStyle(fontSize: 15),
            //                 ),
            //                 const SizedBox(
            //                   height: 20,
            //                 ),
            //                 ...List.generate(
            //                   notificationController
            //                       .listNotificationWithDate[index]
            //                       .listNotif
            //                       .length,
            //                   (int i) {
            //                     // double _width =

            //                     return Column(
            //                       children: [
            //                         StatefulBuilder(
            //                           builder: (context, changeState) {
            //                             double height = 100;
            //                             return AnimatedContainer(
            //                               curve: Curves.fastOutSlowIn,
            //                               width: double.infinity,
            //                               height: height,
            //                               duration: const Duration(seconds: 1),
            //                               child: height > 0
            //                                   ? AlertListTile(
            //                                       onTap: () {
            //                                         notificationController
            //                                             .readNotification(
            //                                                 notificationController
            //                                                     .listNotificationWithDate[
            //                                                         index]
            //                                                     .listNotif[i]
            //                                                     .id);
            //                                         showDetailNotification(
            //                                             notificationController
            //                                                 .listNotificationWithDate[
            //                                                     index]
            //                                                 .listNotif[i]);
            //                                       },
            //                                       onDelete: () {
            //                                         changeState(() {
            //                                           height = 0;
            //                                         });
            //                                         print('heightheight:: == ' +
            //                                             height.toString());
            //                                         notificationController
            //                                             .deleteNotification(
            //                                                 notificationController
            //                                                     .listNotificationWithDate[
            //                                                         index]
            //                                                     .listNotif[i],
            //                                                 notificationController
            //                                                         .listNotificationWithDate[
            //                                                     index])
            //                                             .then((value) {
            //                                           if (mounted) {
            //                                             setState(() {
            //                                               notificationController
            //                                                   .listNotificationWithDate;
            //                                             });
            //                                             super.widget;
            //                                             // setState(() {
            //                                             //   notShowDeleteErreur = value;
            //                                             //   isloadDelete = false;
            //                                             // });
            //                                           }
            //                                         });
            //                                       },
            //                                       notification:
            //                                           notificationController
            //                                               .listNotificationWithDate[
            //                                                   index]
            //                                               .listNotif[i],
            //                                       notificationWithDate:
            //                                           notificationController
            //                                                   .listNotificationWithDate[
            //                                               index],
            //                                     )
            //                                   : Container(
            //                                       width: double.infinity,
            //                                       height: height,
            //                                       color: notificationController
            //                                                   .listNotificationWithDate[
            //                                                       index]
            //                                                   .listNotif[i]
            //                                                   .isRead ==
            //                                               1
            //                                           ? ColorManager.white
            //                                           : ColorManager.blue
            //                                               .withOpacity(0.2),
            //                                     ),
            //                             );
            //                           },
            //                         ),
            //                         const Divider(
            //                           height: 2,
            //                         ),
            //                       ],
            //                     );
            //                   },
            //                 )
            //               ],
            //             )),
            //       )
                : const EmptyComponenent(
                    message: "Vous n'avez pas de notification",
                  ),
      ),
    );
  }
}

class AlertListTile extends StatefulWidget {
  const AlertListTile(
      {super.key,
      required this.notification,
     
      this.onDelete,
      this.onTap});
  final NotificationModel notification;
  final Function()? onDelete;
  final Function()? onTap;

  @override
  State<AlertListTile> createState() => _AlertListTileState();
}

class _AlertListTileState extends State<AlertListTile> {
  NotificationController notificationController = Get.find();
  bool isloadDelete = false;
  bool notShowDeleteErreur = true;
  late NotificationModel notification;
  @override
  void initState() {
    notification = widget.notification;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      tileColor: notification.isRead == 1
          ? ColorManager.backgroundColor
          : ColorManager.white .withOpacity(0.2),
      onTap: () {
        notificationController.readNotification(notification.id);
        showDetailNotification(notification);
      },
      minLeadingWidth: 0,
      contentPadding: const EdgeInsets.all(2),
      leading: const Icon(Icons.notifications),
      title: Text(
        notification.title,
        style: getMeduimTextStyle(
            color: ColorManager.black, fontSize: FontSize.s15),
        maxLines: 2,
        overflow: TextOverflow.ellipsis,
      ),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            notification.message,
            style: getRegularTextStyle(
              color: ColorManager.black,
            ),
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
          const SizedBox(
            height: 5,
          ),
          Text(
            convertDateWithTime(notification.createdAt.toString()),
            style: getRegularTextStyle(
              color: ColorManager.grey,
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          if (notShowDeleteErreur == false) ...[
            Text(
              "Echec de la suppression",
              style: getRegularTextStyle(
                color: ColorManager.red,
              ),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ]
        ],
      ),
      trailing: isloadDelete == false
          ? SizedBox(
              width: 30,
              child: IconButton(
                  icon: const Icon(Icons.delete), onPressed: widget.onDelete),
            )
          : const SizedBox(
              width: 30,
              child: LoaderComponent(),
            ),
    );
  }
}

showDetailNotification(NotificationModel notification) {
  return Get.bottomSheet(
      isScrollControlled: true,
      Container(
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10)),
        margin: const EdgeInsets.all(10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              notification.title,
              style:
                  getMeduimTextStyle(color: ColorManager.black, fontSize: 17),
            ),
            const SizedBox(
              height: 5,
            ),
            Text(
              notification.message,
              style:
                  getRegularTextStyle(color: ColorManager.black, fontSize: 14),
            ),
            const SizedBox(
              height: 5,
            ),
            Text(
              convertDateWithTime(notification.createdAt.toString()),
              style: getRegularTextStyle(
                color: ColorManager.grey,
              ),
            ),
          ],
        ),
      ),
      backgroundColor: ColorManager.white);
}

class NotificationWithDateModel {
  List<NotificationModel> listNotif;
  String title;
  NotificationWithDateModel({required this.listNotif, required this.title});
}
