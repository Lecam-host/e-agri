import 'package:flutter/material.dart';

import '../../app/functions.dart';
import '../common/buttons/back_button.dart';
import '../ressources/assets_manager.dart';
import '../ressources/color_manager.dart';
import '../ressources/font_manager.dart';
import '../ressources/strings_manager.dart';
import '../ressources/styles_manager.dart';
import '../ressources/values_manager.dart';

class DetailsNotificationView extends StatefulWidget {
  const DetailsNotificationView({Key? key}) : super(key: key);

  @override
  State<DetailsNotificationView> createState() =>
      _DetailsNotificationViewState();
}

class _DetailsNotificationViewState extends State<DetailsNotificationView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.transparent,
          leading: const BackButtonCustom(),
          elevation: 0,
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset(ImageAssets.connexionBg),
              Container(
                margin: const EdgeInsets.only(
                    left: AppMargin.m10,
                    right: AppMargin.m10,
                    top: AppMargin.m10),
                child: Text(
                  "Alerte rafale de vent",
                  style: getBoldTextStyle(
                    color: ColorManager.black,
                    fontSize: FontSize.s20,
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(
                  left: AppMargin.m10,
                  right: AppMargin.m10,
                  bottom: AppMargin.m10,
                ),
                child: Row(
                  children: [
                    Text(
                      "ANADER",
                      style: getRegularTextStyle(
                        color: ColorManager.blue,
                        fontSize: FontSize.s12,
                      ),
                    ),
                    Text(
                      " , le ${convertDate(AppStrings.fakeText)}",
                      style: getRegularTextStyle(
                        color: ColorManager.grey,
                        fontSize: FontSize.s12,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.all(AppMargin.m10),
                child: Text(
                  "Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.",
                  style: getRegularTextStyle(
                    color: ColorManager.black,
                  ),
                ),
              )
            ],
          ),
        ));
  }
}
