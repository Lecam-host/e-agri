import 'package:eagri/presentations/ressources/assets_manager.dart';

class OnboardingContents {
  final String title;
  final String image;
  final String desc;

  OnboardingContents({
    required this.title,
    required this.image,
    required this.desc,
  });
}

List<OnboardingContents> contents = [
  OnboardingContents(
    title: "E-agriculture",
    image: JsonAssets.agri,
    desc: "Les outils et les technologies modernes d'information et de communication pour accroître la productivité agricole.",
  ),
  OnboardingContents(
    title: "Conseils agricoles",
    image: JsonAssets.advice,
    desc:
        "Un ensemble de démarches et dispositifs permettant d'apporter un appui aux exploitations agricoles",
  ),
  OnboardingContents(
    title: "Consulter la météo",
    image: JsonAssets.weather,
    desc:
        "Un service de prévisions météo destiné aux agriculteurs.",
  ),
];
