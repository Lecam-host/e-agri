import 'package:eagri/presentations/ressources/icon_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:eagri/presentations/search_filter/controller/filter_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../ressources/color_manager.dart';
import '../ressources/strings_manager.dart';
import '../shop/controller/shop_controller.dart';

class SearchView extends StatefulWidget {
  const SearchView({Key? key}) : super(key: key);

  @override
  State<SearchView> createState() => _SearchViewState();
}

class _SearchViewState extends State<SearchView> {
  final _focusNode = FocusNode();
  ShopController shopController = Get.find();
  FilterController filterController = Get.find();

  @override
  void initState() {
    openKeyboard();
    super.initState();
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  void openKeyboard() {
    _focusNode.requestFocus();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.all(10),
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      onChanged: (value) {
                        filterController.searchSpeculationOrCategory();
                      },
                      controller: filterController.searchInput.value,
                      focusNode: _focusNode,
                      decoration: InputDecoration(
                        suffixIcon: Icon(
                          Icons.search,
                          color: ColorManager.grey,
                        ),
                        filled: true,
                        fillColor: Colors.grey.withOpacity(0.2),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15.0),
                          borderSide: const BorderSide(
                              width: 0, style: BorderStyle.none),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15.0),
                          borderSide: const BorderSide(
                              width: 0, style: BorderStyle.none),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15.0),
                          borderSide: const BorderSide(
                              width: 0, style: BorderStyle.none),
                        ),
                        hintText: AppStrings.searchSomething,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  TextButton(
                    onPressed: () {
                      Get.back();
                    },
                    child: const Text('Annuler'),
                  )
                ],
              ),
            ),
            Obx(
              () => filterController.listSearchItem.isNotEmpty
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 20,
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 2,
                          child: ListView.builder(
                            padding: const EdgeInsets.only(bottom: 10),
                            shrinkWrap: true,
                            itemCount: filterController.listSearchItem.length,
                            itemBuilder: (context, index) {
                              return ListTile(
                                onTap: () {
                                  shopController.getListProduct(
                                      shopController.shopPageIndex.value);
                                },
                                title: Text(
                                  filterController.listSearchItem[index].name,
                                  style: getRegularTextStyle(),
                                ),
                                trailing: const Icon(IconManager.arrowRight),
                              );
                            },
                          ),
                        ),
                      ],
                    )
                  : const Center(
                      child: Text('Entrer ce que vous recherchez'),
                    ),
            ),
          ],
        ),
      )),
    );
  }
}
