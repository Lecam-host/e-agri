import 'package:eagri/app/constant.dart';
import 'package:eagri/controllers/location_controller.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/default_button.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:eagri/presentations/shop/controller/shop_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:select_form_field/select_form_field.dart';


class FilterView extends StatefulWidget {
  const FilterView({Key? key}) : super(key: key);

  @override
  State<FilterView> createState() => _FilterViewState();
}

class _FilterViewState extends State<FilterView> {
  LocationController locationController = Get.find();
  ShopController shopController = Get.put(ShopController());

  final _focusNodePrixMin = FocusNode();
  final _focusNodePrixMax = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // shopController.fetchIntrantAll();
    _focusNodePrixMin.dispose();
    _focusNodePrixMax.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButtonCustom(
          onPressed: () {
            Get.back(result: 0);
          },
        ),
        centerTitle: true,
        title: const Text('Filtre'),
        actions: [
          TextButton(
              onPressed: () {
                shopController.resetFilter();
              },
              child: const Text('Annuler'))
        ],
      ),
      body: Obx(
        () => Container(
          margin: const EdgeInsets.all(10),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Les types de produits",
                  style: getMeduimTextStyle(
                      color: ColorManager.black, fontSize: 16),
                ),
                const SizedBox(
                  height: 10,
                ),
                Wrap(
                  children: [
                    categoryButton("Produit agricole", codeProductAgricultural),
                    categoryButton("Intrant", codeIntrant),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                const Divider(),
                Text(
                  "Veuillez définir les parametres de ce que vous cherchez",
                  style: getRegularTextStyle(),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(
                  height: 10,
                ),
                if (shopController.typeProductCodeInFilter.value ==
                    codeProductAgricultural) ...[
                  Text(
                    'Catégories de produit',
                    style: getMeduimTextStyle(fontSize: 14),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Obx(
                    () => SelectFormField(
                      controller: shopController.categoryController.value,

                      type: SelectFormFieldType.dialog, // or can be dialog
                      enableSearch: true,
                      // initialValue: 'circle',
                      labelText: 'Choisir la Categorie',

                      items: shopController.listCategoryMap,
                      onChanged: (val) {
                        shopController
                            .filterProdAgricolFnCategorie(int.parse(val));
                      },
                      onSaved: (val) {},
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                ] else ...[
                  Text(
                    "Catégories d'intrant",
                    style: getMeduimTextStyle(fontSize: 14),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Obx(
                    () => SelectFormField(
                      controller: shopController.categoryController.value,

                      type: SelectFormFieldType.dialog, // or can be dialog
                      enableSearch: true,
                      // initialValue: 'circle',
                      labelText: 'Choisir la Categorie',

                      items: shopController.listCategoryIntrantMap,
                      onChanged: (val) {
                        shopController.filterIntrantFnCategorie(int.parse(val));
                      },
                      onSaved: (val) {},
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                ],
                Text(
                  'Produit',
                  style: getMeduimTextStyle(fontSize: 14),
                ),
                const SizedBox(
                  height: 10,
                ),
                Obx(
                  () => SelectFormField(
                    type: SelectFormFieldType.dialog, // or can be dialog
                    enableSearch: true,
                    controller: shopController.productController.value,
                    // initialValue: 'circle',
                    labelText: 'Choisir le produit',

                    items: shopController.typeProductCodeInFilter.value ==
                            codeIntrant
                        ? shopController.listCategoryIntrantMapInSelectFeild
                        : shopController.listeSpeculationMapInSelectField,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  'Lieu',
                  style: getBoldTextStyle(fontSize: 14),
                ),
              const  SizedBox(height: 10,),
             Obx(() =>  SelectFormField(
                    type: SelectFormFieldType.dialog, // or can be dialog
                    enableSearch: true,
                    controller: shopController.regionController.value,
                    // initialValue: 'circle',
                    labelText: 'Choisir la région',
                  
                    items: locationController.listRegionMap,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  'Prix',
                  style: getBoldTextStyle(fontSize: 14),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: SizedBox(
                        width: double.infinity,
                        height: 40,
                        child: TextField(
                          controller: shopController.prixMinController.value,
                          focusNode: _focusNodePrixMin,
                          keyboardType: const TextInputType.numberWithOptions(),
                          decoration: const InputDecoration(
                              label: Text('Prix min'),
                              hintText: 'Enter le prix min'),
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: SizedBox(
                        height: 40,
                        child: TextField(
                          controller: shopController.prixMaxController.value,
                          focusNode: _focusNodePrixMax,
                          keyboardType: const TextInputType.numberWithOptions(),
                          decoration: const InputDecoration(
                              label: Text('Prix max'),
                              hintText: 'Enter le prix max'),
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: const EdgeInsets.all(5),
                  padding: const EdgeInsets.all(10),
                  width: double.infinity,
                  decoration: BoxDecoration(color: ColorManager.white),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Note',
                        style: getBoldTextStyle(fontSize: 14),
                      ),
                      RangeSlider(
                        values: shopController.noteController.value,
                        max: 5,
                        divisions: 5,
                        labels: RangeLabels(
                          shopController.noteController.value.start
                              .round()
                              .toString(),
                          shopController.noteController.value.end
                              .round()
                              .toString(),
                        ),
                        onChanged: (RangeValues values) {
                          setState(() {
                            shopController.noteController.value = values;
                          });
                        },
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(10),
                  child: DefaultButton(
                    text: "Rechercher",
                    press: () {
                      shopController.listProductIntrant.value = [];
                      shopController.listProductAgricul.value = [];

                      shopController.nextPage.value = 0;
                      shopController.filterProduct();
                      // shopController.filterProduct();
                    },
                  ),
                ),
                Center(
                  child: TextButton(
                      onPressed: () {
                        shopController.renitialiseFilter();
                        super.widget;
                      },
                      child:  Text('Effacer le filtre',style: TextStyle(
                      ),),),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Obx categoryButton(String titre, int code) {
    return Obx(() => InkWell(
          onTap: () {
            shopController.renitialiseFilter();
            shopController.typeProductCodeInFilter.value = code;

            setState(() {
              shopController.categoryController.value.clear();
              shopController.productController.value.clear();
            });
          },
          child: Container(
            margin: const EdgeInsets.only(
              left: 5,
              bottom: 5,
            ),
            padding: const EdgeInsets.all(5),
            decoration: BoxDecoration(
                color: shopController.typeProductCodeInFilter.value == code
                    ? ColorManager.primary
                    : ColorManager.grey1,
                borderRadius: const BorderRadius.all(Radius.circular(10))),
            child: Text(
              titre,
              style: getRegularTextStyle(
                color: ColorManager.white,
              ),
            ),
          ),
        ));
  }
}

class TypeProductCard extends StatelessWidget {
  const TypeProductCard({
    Key? key,
    required this.titre,
    required this.img,
  }) : super(key: key);
  final String titre;
  final String img;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: ColorManager.white),
      width: 100,
      height: 110,
      margin: const EdgeInsets.only(left: 10),
      padding: const EdgeInsets.all(10),
      child: Column(
        children: [
          SizedBox(
            height: 50,
            child: Image.asset(
              img,
              height: 50,
            ),
          ),
          Text(
            titre,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.center,
            style: getMeduimTextStyle(
              color: ColorManager.black,
              fontSize: 12,
            ),
          ),
        ],
      ),
    );
  }
}
