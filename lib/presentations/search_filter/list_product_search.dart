import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../shop/list_product.dart';
import 'controller/filter_controller.dart';

class ListProductSearchView extends StatefulWidget {
  const ListProductSearchView({Key? key, this.titre}) : super(key: key);
  final String? titre;

  @override
  State<ListProductSearchView> createState() => _ListProductSearchViewState();
}

class _ListProductSearchViewState extends State<ListProductSearchView> {
  FilterController filterController = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButtonCustom(),
        title: Text(widget.titre ?? ""),
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: CustomScrollView(slivers: [
          SliverPadding(
            padding: const EdgeInsets.all(0),
            sliver: filterController.listProd.isNotEmpty
                ? ListProduct(
                    listProd: filterController.listProd,
                  )
                : const SliverToBoxAdapter(
                    child: Center(
                        child: EmptyComponenent(
                      message:
                          "Nous n'avons pas trouvé de résultat pour cette recherche",
                    )),
                  ),
          ),
        ]),
      ),
    );
  }
}
