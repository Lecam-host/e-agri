import 'dart:developer';

import 'package:eagri/controllers/location_controller.dart';
import 'package:eagri/controllers/speculation_controller.dart';
import 'package:eagri/data/mapper/shop_mapper.dart';
import 'package:eagri/data/request/shop_resquests.dart';
import 'package:eagri/domain/model/category_model.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/domain/usecase/shop_usecase.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../app/di.dart';
import '../../../domain/model/Product.dart';
import '../list_product_search.dart';

class FilterController extends GetxController {
  ShopUseCase shopUseCase = instance<ShopUseCase>();

  var listSpeculation = <SpeculationModel>[].obs;
  var listCategory = <CategoryModel>[].obs;
  var listSearchItem = <SearchItemModel>[].obs;

  var listeSpeculationMap = <Map<String, dynamic>>[];
  var listCategoryMap = <Map<String, dynamic>>[];
  var listRegionMap = <Map<String, dynamic>>[];
  var noteController = const RangeValues(0, 5).obs;
  var categoryController = TextEditingController().obs;
  var productController = TextEditingController().obs;
  var regionController = TextEditingController().obs;
  var searchInput = TextEditingController().obs;

  var listProd = <ProductModel>[].obs;
  var listSpecultionSearch = <SpeculationModel>[].obs;
  var listCategorySearch = <CategoryModel>[].obs;

  var prixMaxController = TextEditingController().obs;
  var prixMinController = TextEditingController().obs;
  var typeOffer = "VENTE".obs;
  var isFilter = false.obs;

  SpeculationController speculationController =
      Get.put(SpeculationController());
  LocationController locationController = Get.put(LocationController());

  @override
  onInit() {
    listRegionMap = locationController.listRegionMap;
    listeSpeculationMap = speculationController.listeSpeculationMap;
    listCategoryMap = speculationController.listCategorysMap;
    inspect(listSpeculation);

    super.onInit();
  }

  search() {}

  searchSpeculationOrCategory() async {
    listSearchItem.value = [];
    if (searchInput.value.text.isNotEmpty) {
      await Future.forEach(speculationController.listSpeculation,
          (SpeculationModel speculation) {
        if (speculation.name!
            .toUpperCase()
            .contains(searchInput.value.text.toUpperCase())) {
          listSearchItem.add(SearchItemModel(
              id: speculation.speculationId!,
              name: speculation.name!,
              type: "speculation"));
        }
      });
      await Future.forEach(speculationController.listCategorys,
          (CategoryModel category) {
        if (category.name
            .toUpperCase()
            .contains(searchInput.value.text.toUpperCase())) {
          listSearchItem.add(
            SearchItemModel(
                id: category.categoryId,
                name: category.name,
                type: "categorie"),
          );
        }
      });
    }

    // listSpeculationAfficher.value = listSpeculationSearch;
  }

  searchProduct(
    SearchItemModel searchItem,
  ) async {
    Get.dialog(const LoaderComponent());
    await shopUseCase
        .searchProduct(
          SearchProductRequest(
            offerType: typeOffer.value,
            limit: 10,
            page: 0,
            speculationId:
                searchItem.type == "speculation" ? [searchItem.id] : null,
            categorieId: searchItem.type == "categorie" ? searchItem.id : null,
          ),
        )
        .then((response) => response.fold((l) => null, (searchData) async {
              listProd.value = await searchData.toDomain();
            }));
    Get.back();
    Get.to(ListProductSearchView(
      titre: searchItem.name,
    ));
  }

  getList() {}
}

class SearchItemModel {
  int id;
  String name;
  String type;
  SearchItemModel({
    required this.id,
    required this.name,
    required this.type,
  });
}
