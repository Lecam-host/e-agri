import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';

class TageWidget extends StatelessWidget {
  final String label;
  final Color backgrounColor;
  const TageWidget(
      {super.key, required this.label, required this.backgrounColor});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 2),
      width: 140,
      decoration: BoxDecoration(
        color: backgrounColor,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Text(
        label,
        overflow: TextOverflow.ellipsis,
        style: getRegularTextStyle(fontSize: 12, color: ColorManager.blue),
        maxLines: 2,
      ),
    );
  }
}