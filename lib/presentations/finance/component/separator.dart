import 'package:flutter/material.dart';

class SeparatorWidget extends StatelessWidget {
  final double size;
  const SeparatorWidget({super.key, required this.size});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: size),
        const Divider(),
        SizedBox(height: size),
      ],
    );
  }
}
