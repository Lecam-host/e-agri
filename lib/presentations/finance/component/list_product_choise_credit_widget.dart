import 'dart:developer';

import 'package:eagri/domain/model/type_offre_credit_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../ressources/color_manager.dart';
import '../controller/finance_controller.dart';

class ListProductChoiseCreditWidget extends StatelessWidget {
  final List<DataTypeOffreCreditModel> list;
  const ListProductChoiseCreditWidget({super.key, required this.list});

  @override
  Widget build(BuildContext context) {
    final FinanceController financeController = Get.find();
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      physics: const BouncingScrollPhysics(),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 7,
            ),
            child: Obx(
              () => ChoiceChip(
                onSelected: (bool isSelected) async {
                  financeController.clearSetting();
                  financeController.isLoadListCredit.value = true;
                  await financeController.fetchCredit();
                  financeController.isLoadListCredit.value = false;
                },
                selectedColor: ColorManager.primary,
                label: Text(
                  "Tout",
                  style: TextStyle(
                      color: financeController.typeCreditSeleted.value == 0
                          ? Colors.white
                          : Colors.black),
                ),
                selected: financeController.typeCreditSeleted.value == 0,
              ),
            ),
          ),
          ...List.generate(
            list.length,
            (index) => Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 7,
              ),
              child: Obx(
                () => ChoiceChip(
                  onSelected: (bool isSelected) async {
                    log(list[index].name!);
                    financeController.typeCreditSeleted.value = list[index].id!;
                    financeController.isLoadListCredit.value = true;
                    await financeController.fetchCredit();
                    financeController.isLoadListCredit.value = false;
                  },
                  selectedColor: ColorManager.primary,
                  label: Text(
                    list[index].name!,
                    style: TextStyle(
                        color: financeController.typeCreditSeleted.value ==
                                list[index].id!
                            ? Colors.white
                            : Colors.black),
                  ),
                  selected: financeController.typeCreditSeleted.value ==
                      list[index].id!,
                ),
              ),
            ),
          ).toList()
        ],
      ),
    );
  }
}
