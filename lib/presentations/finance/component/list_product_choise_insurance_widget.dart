import 'dart:developer';

import 'package:eagri/domain/model/type_offre__insurance_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../ressources/color_manager.dart';
import '../controller/finance_controller.dart';

class ListProductChoiseInsuranceWidget extends StatelessWidget {
  final List<DataTypeOffreInsuranceModel> list;
  const ListProductChoiseInsuranceWidget({super.key, required this.list});

  @override
  Widget build(BuildContext context) {
    final FinanceController financeController = Get.find();
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      physics: const BouncingScrollPhysics(),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 7,
            ),
            child: Obx(
              () => ChoiceChip(
                onSelected: (bool isSelected) async {
                  financeController.clearSetting();
                  financeController.isLoadListInsurance.value = true;
                  await financeController.fetchInsurance();
                  financeController.isLoadListInsurance.value = false;
                },
                selectedColor: ColorManager.primary,
                label: Text(
                  "Tout",
                  style: TextStyle(
                      color: financeController.typeInsuranceSeleted.value == 0
                          ? Colors.white
                          : Colors.black),
                ),
                selected: financeController.typeInsuranceSeleted.value == 0,
              ),
            ),
          ),
          ...List.generate(
            list.length,
            (index) => Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 7,
              ),
              child: Obx(
                () => ChoiceChip(
                  onSelected: (bool isSelected) async {
                    log(list[index].name!);
                    financeController.typeInsuranceSeleted.value =
                        list[index].id!;
                    financeController.isLoadListInsurance.value = true;
                    await financeController.fetchInsurance();
                    financeController.isLoadListInsurance.value = false;
                  },
                  selectedColor: ColorManager.primary,
                  label: Text(
                    list[index].name!,
                    style: TextStyle(
                        color: financeController.typeInsuranceSeleted.value ==
                                list[index].id!
                            ? Colors.white
                            : Colors.black),
                  ),
                  selected: financeController.typeInsuranceSeleted.value ==
                      list[index].id!,
                ),
              ),
            ),
          ).toList()
        ],
      ),
    );
  }
}
