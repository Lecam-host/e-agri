import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:get/get.dart';

import '../../../domain/model/offre_credit_model.dart';
import '../../ressources/styles_manager.dart';

class OptionOffreCredit extends StatelessWidget {
  final List<ItemOffreCreditModel>? list;
  const OptionOffreCredit({super.key, required this.list});

  @override
  Widget build(BuildContext context) {
    return list == null || list!.isEmpty
        ? const SizedBox()
        : Column(
            children: [
              const SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  const Icon(
                    Icons.list,
                    color: Colors.grey,
                  ),
                  const SizedBox(width: 5),
                  Text(
                    "Options de l'offre",
                    style:
                        getMeduimTextStyle(fontSize: 18, color: Colors.black54),
                  )
                ],
              ),
              const Divider(),
              const SizedBox(height: 10),
              ListView.builder(
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: list!.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(bottom: 10),
                    child: ListTile(
                        onTap: () {
                          Get.bottomSheet(
                            isScrollControlled: true,
                            Container(
                              padding: const EdgeInsets.all(10),
                              margin: const EdgeInsets.symmetric(
                                  horizontal: 7, vertical: 7),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(
                                  10,
                                ),
                              ),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  if (list![index].illustrationImage != null)
                                    SizedBox(
                                      width: 100,
                                      height: 100,
                                      child: CachedNetworkImage(
                                        imageUrl:
                                            list![index].illustrationImage!,
                                        imageBuilder:
                                            (context, imageProvider) =>
                                                Container(
                                          margin: const EdgeInsets.only(
                                            bottom: 10,
                                          ),
                                          //    height: MediaQuery.of(context).size.height / 2.5,
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                              scale: 1,
                                              image: imageProvider,
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                        ),
                                        placeholder: (context, url) => const SizedBox(
                                            // height: MediaQuery.of(context).size.height / 2.5,
                                            // width: MediaQuery.of(context).size.width,
                                            ),
                                        errorWidget: (context, url, error) =>
                                            const Icon(Icons.error),
                                      ),
                                    ),
                                  Text(
                                    list![index].name!,
                                    style: getBoldTextStyle(
                                        color: Colors.black, fontSize: 20),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  HtmlWidget(
                                    list![index].description!,
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                        title: Text(list![index].name!),
                        leading: SizedBox(
                          height: 70,
                          width: 70,
                          child: CachedNetworkImage(
                            imageUrl: list![index].illustrationImage!,
                            fit: BoxFit.cover,
                          ),
                        ),
                        trailing: IconButton(
                          onPressed: () {},
                          icon: const Icon(Icons.arrow_forward_ios),
                        )),
                  );
                },
              ),
              const SizedBox(height: 20),
            ],
          );
  }
}
