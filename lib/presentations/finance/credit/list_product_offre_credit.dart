import 'package:eagri/domain/model/offre_credit_model.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/transport/transport_screen.dart';
import 'package:flutter/material.dart';
import '../../../domain/model/type_offre_credit_model.dart';
import '../component/list_product_choise_credit_widget.dart';
import 'product_offre_card_credit.dart';
import '../controller/finance_controller.dart';
import 'package:get/get.dart';

class ListProductOffreCredit extends StatefulWidget {
  final List<ItemOffreCreditModel> list;
  final List<DataTypeOffreCreditModel> listTypeCredit;
  const ListProductOffreCredit(
      {super.key, required this.list, required this.listTypeCredit});

  @override
  State<ListProductOffreCredit> createState() => _ListProductOffreCreditState();
}

class _ListProductOffreCreditState extends State<ListProductOffreCredit>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    FinanceController financeController = Get.put(FinanceController());
    return Obx(() => financeController.isLoadingAsssurance.value
        ? const LoaderComponent()
        : Column(
            children: [
              ListProductChoiseCreditWidget(
                list: widget.listTypeCredit,
              ),
              financeController.isLoadListCredit.value
                  ? const LoaderComponent()
                  : Expanded(
                      child: widget.list.isEmpty
                          ? const EmptyComponenent(
                              message: "Aucun produit pour ce type de crédit",
                            )
                          : GridView.builder(
                              physics: const BouncingScrollPhysics(),
                              controller: financeController
                                  .scrollControllerCredit.value,
                              shrinkWrap: true,
                              itemCount:
                                  financeController.totalItemsCredit.value >
                                          financeController.listCredit.length
                                      ? financeController.listCredit.length + 1
                                      : financeController.listCredit.length,
                              gridDelegate:
                                  const SliverGridDelegateWithFixedCrossAxisCount(
                                      mainAxisSpacing: 10,
                                      crossAxisSpacing: 10,
                                      crossAxisCount: 2,
                                      mainAxisExtent: 250),
                              itemBuilder: (BuildContext context, int index) =>
                                  index ==
                                              financeController
                                                  .listCredit.length &&
                                          financeController
                                                  .totalItemsCredit.value >
                                              financeController
                                                  .listCredit.length
                                      ? shimmerCustom()
                                      : ProducOffreCreditCard(
                                          product: widget.list[index],
                                        ),
                            ),
                    )
            ],
          ));
  }
}
