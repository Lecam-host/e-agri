import 'package:eagri/presentations/finance/component/separator.dart';
import 'package:flutter/material.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/default_button.dart';
import 'package:eagri/presentations/finance/component/tag_widget.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
// import 'package:flutter_html/flutter_html.dart';
// import 'package:flutter_html/flutter_html.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../domain/model/offre_credit_model.dart';
import '../../ressources/color_manager.dart';

class ProductOtherDetailCreditWidget<T> extends StatefulWidget {
  const ProductOtherDetailCreditWidget(
      {super.key, required this.financeProduct});
  final ItemOffreCreditModel financeProduct;

  @override
  State<ProductOtherDetailCreditWidget> createState() =>
      _ProductOtherDetailCreditWidgetState();
}

class _ProductOtherDetailCreditWidgetState
    extends State<ProductOtherDetailCreditWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomSheet: Container(
        margin: const EdgeInsets.all(10),
        child: DefaultButton(
          text: "Voir le site",
          press: () async {
            if (!await launchUrl(
              Uri.parse(widget.financeProduct.webLink!),
              mode: LaunchMode.inAppWebView,
            )) {
              throw 'Could not launch ${widget.financeProduct.webLink}';
            }
          },
        ),
      ),
      appBar: AppBar(
        leading: const BackButtonCustom(),
        title: Text(widget.financeProduct.name!),
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        padding: const EdgeInsets.only(bottom: 100),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              // height: MediaQuery.of(context).size.height / 3,
              width: MediaQuery.of(context).size.width,
              child: AspectRatio(
                aspectRatio: 1,
                child: Container(
                  decoration: BoxDecoration(
                    color: ColorManager.lightGrey,
                  ),
                  child: CachedNetworkImage(
                    imageUrl: widget.financeProduct.illustrationImage!,
                    imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          scale: 1,
                          image: imageProvider,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    placeholder: (context, url) => SizedBox(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                    ),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                  ),
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.financeProduct.creditor!.name!,
                    style: getLightTextStyle(
                        fontSize: 12, color: ColorManager.blue),
                  ),
                  Text(
                    widget.financeProduct.name!,
                    style: getMeduimTextStyle(
                      fontSize: 16,
                    ),
                  ),
                  const SizedBox(height: 5),
                  TageWidget(
                    label: "Assurance maladie",
                    backgrounColor: ColorManager.primary.withOpacity(0.2),
                  ),
                  const SizedBox(height: 10),
                  const SizedBox(
                    height: 10,
                  ),
                  // Html(
                  //   data: widget.financeProduct.description,
                  // ),
                  const SeparatorWidget(size: 10),
                  Text(
                    "Ressource",
                    style:
                        getSemiBoldTextStyle(fontSize: 18, color: Colors.grey),
                  ),
                  const SeparatorWidget(size: 10),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
