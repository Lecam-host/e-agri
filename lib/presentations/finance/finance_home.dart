import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:flutter/material.dart';

import '../ressources/assets_manager.dart';
import 'component/product_card.dart';

class FinanceHome extends StatefulWidget {
  const FinanceHome({super.key});

  @override
  State<FinanceHome> createState() => _FinanceHomeState();
}

class _FinanceHomeState extends State<FinanceHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Finance"),
        leading: const BackButtonCustom(),
      ),
      body: Container(
        margin: const EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ProduitCard(
              titre: "Assurance",
              image: ImageAssets.connexionBg,
            ),
            const SizedBox(
              height: 10,
            ),
            ProduitCard(
              titre: "Credit",
              image: ImageAssets.mettreEngrais,
            ),
          ],
        ),
      ),
    );
  }
}
