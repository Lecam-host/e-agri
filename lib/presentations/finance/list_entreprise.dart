import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ListEntreprise extends StatefulWidget {
  const ListEntreprise({super.key});

  @override
  State<ListEntreprise> createState() => _ListEntrepriseState();
}

class _ListEntrepriseState extends State<ListEntreprise> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.only(top: 10, right: 10, left: 10),
        child: GridView.builder(
            shrinkWrap: true,
            itemCount: 10,
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              mainAxisSpacing: 10,
              crossAxisSpacing: 10,
              crossAxisCount: 2,
            ),
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                  onTap: () {},
                  child: Container(
                    //  margin: const EdgeInsets.symmetric(horizontal: 13, vertical: 8),
                    width: MediaQuery.of(context).size.width,
                    height: 200,
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(15),
                      image: DecorationImage(
                        onError: (Object exception, StackTrace? stackTrace) {},
                        colorFilter: ColorFilter.mode(
                          Colors.black.withOpacity(0.35),
                          BlendMode.multiply,
                        ),
                        image: const CachedNetworkImageProvider(
                          'https://api.eagri.mobisoft.ci/archive/ressource/MTQ4OA==',
                        ),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ));
            }),
      ),
    );
  }
}
