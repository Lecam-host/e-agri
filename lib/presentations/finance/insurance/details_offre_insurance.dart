import 'package:another_flushbar/flushbar.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/data/repository/api/repository_impl.dart';
import 'package:eagri/domain/model/offfre_item_model.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/common/pdf_screen.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/finance/insurance/option_offre_insurance.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:get/get.dart';
import 'package:page_transition/page_transition.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:video_player/video_player.dart';
import '../../../app/di.dart';
import '../../../app/functions.dart';
import '../../../domain/model/audio_model.dart';
import '../../../domain/model/offre_insurance_model.dart';
import '../../common/audio/app_audio.dart';
import '../../ressources/assets_manager.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/icon_manager.dart';
import '../../ressources/values_manager.dart';
import 'product_other_insurance_widget.dart';
import '../component/separator.dart';
import '../controller/finance_controller.dart';

class DetailsOffreInsuranceScreen<T> extends StatefulWidget {
  const DetailsOffreInsuranceScreen(
      {super.key, required this.financeProduct, this.isAutreProduct = false});
  final ItemOffreInsuranceModel financeProduct;
  final bool isAutreProduct;

  @override
  State<DetailsOffreInsuranceScreen> createState() =>
      _DetailsOffreInsuranceScreenState();
}

class _DetailsOffreInsuranceScreenState
    extends State<DetailsOffreInsuranceScreen> {
  FinanceController financeController = Get.find();

  @override
  void initState() {
    super.initState();
    getMedia();
    if (widget.isAutreProduct == false) {
      financeController
          .fetchInsuranceByInsurer(widget.financeProduct.insurer!.id!);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // bottomSheet: Container(
      //   margin: const EdgeInsets.all(10),
      //   child: DefaultButton(
      //     text: "Voir le site",
      //     press: () async {
      //       if (!await launchUrl(
      //         Uri.parse(widget.financeProduct.webLink!),
      //         mode: LaunchMode.inAppWebView,
      //       )) {
      //         throw 'Could not launch ${widget.financeProduct.webLink}';
      //       }
      //     },
      //   ),
      // ),
      appBar: AppBar(
        leading: const BackButtonCustom(),
        title: const Text("Details du produit"),
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        padding: const EdgeInsets.only(bottom: 100),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              // height: MediaQuery.of(context).size.height / 3,
              width: MediaQuery.of(context).size.width,
              child: AspectRatio(
                aspectRatio: 1,
                child: Container(
                  decoration: BoxDecoration(
                    color: ColorManager.lightGrey,
                  ),
                  child: CachedNetworkImage(
                    imageUrl: widget.financeProduct.illustrationImage == null
                        ? "https://images.unsplash.com/photo-1575936123452-b67c3203c357?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8aW1hZ2V8ZW58MHx8MHx8&w=1000&q=80"
                        : widget.financeProduct.illustrationImage!,
                    imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          scale: 1,
                          image: imageProvider,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    placeholder: (context, url) => SizedBox(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                    ),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                  ),
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () async {
                      late EntrepriseModel entreprise;
                      RepositoryImpl repositoryImpl =
                          instance<RepositoryImpl>();
                      Get.bottomSheet(
                        isScrollControlled: false,
                        Container(
                          padding: const EdgeInsets.all(10),
                          margin: const EdgeInsets.symmetric(
                              horizontal: 7, vertical: 7),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10)),
                          child: const LoaderComponent(),
                        ),
                      );
                      await repositoryImpl
                          .getDetailsEntreprise(
                              widget.financeProduct.insurer!.id!)
                          .then((response) async {
                        await response.fold((failure) {
                          Get.back();
                          showCustomFlushbar(
                            context,
                            failure.message,
                            ColorManager.error,
                            FlushbarPosition.TOP,
                          );
                        }, (data) async {
                          Get.back();
                          entreprise = data;
                          setState(() {
                            entreprise;
                          });
                          entrepriseInfo(entreprise);
                        });
                      });
                    },
                    child: Row(
                      children: [
                        Icon(
                          Icons.info,
                          color: ColorManager.blue,
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Text(
                          widget.financeProduct.insurer!.name!,
                          style: getLightTextStyle(
                              fontSize: 12, color: ColorManager.blue),
                        ),
                      ],
                    ),
                  ),
                  Text(
                    widget.financeProduct.name!,
                    style: getMeduimTextStyle(
                      fontSize: 16,
                    ),
                  ),
                  Text(
                    widget.financeProduct.typeInsurance!.name!,
                    style: getRegularTextStyle(
                        fontSize: 12, color: ColorManager.grey),
                  ),
                  const SizedBox(height: 10),
                  if (widget.financeProduct.description != null)
                    // Text(widget.financeProduct.description.toString()),
                    HtmlWidget(
                      widget.financeProduct.description!,
                    ),
                  if (widget.financeProduct.webLink != null)
                    GestureDetector(
                      onTap: () async {
                        if (!await launchUrl(
                          Uri.parse(widget.financeProduct.webLink!),
                          mode: LaunchMode.inAppWebView,
                        )) {
                          throw 'Could not launch ${widget.financeProduct.webLink!}';
                        }
                      },
                      child: Row(
                        children: [
                          Icon(
                            Icons.link,
                            color: ColorManager.blue,
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          Text(
                            "Plus de details ici",
                            style: getRegularTextStyle(
                              color: ColorManager.blue,
                            ),
                          )
                        ],
                      ),
                    ),
                  OptionOffreInsurance(
                      list: widget.financeProduct.subInsurances),
                  // Obx(() => financeController
                  //         .isLoadingListInsuranceByInsurer.value
                  //     ? const LoaderComponent()
                  //     : widget.financeProduct.subInsurances!.isEmpty
                  //         ? Container()
                  //         : ProductOtherInsuranceWidget(
                  //             list: widget.financeProduct.subInsurances ?? [])),
                  // const SeparatorWidget(size: 10),

                  widget.financeProduct.files == null ||
                          widget.financeProduct.files!.isEmpty
                      ? const SizedBox()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(height: 20),
                            Text(
                              "Documents",
                              style: getMeduimTextStyle(
                                  fontSize: 18, color: Colors.black54),
                            ),
                            const Divider(),
                            ...List.generate(
                                widget.financeProduct.files!.length, (i) {
                              return widget.financeProduct.files![i].type ==
                                      "video"
                                  ? videoCard()
                                  : widget.financeProduct.files![i].type ==
                                          "pdf"
                                      ? documentCard(
                                          widget.financeProduct.files![i].link!)
                                      : widget.financeProduct.files![i].type ==
                                              "image"
                                          ? CachedNetworkImage(
                                              imageUrl: widget.financeProduct
                                                  .files![i].link!,
                                              imageBuilder:
                                                  (context, imageProvider) =>
                                                      Container(
                                                margin: const EdgeInsets.only(
                                                  bottom: 10,
                                                ),
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height /
                                                    2.5,
                                                decoration: BoxDecoration(
                                                  image: DecorationImage(
                                                    scale: 1,
                                                    image: imageProvider,
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                              ),
                                              placeholder: (context, url) =>
                                                  SizedBox(
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height /
                                                    2.5,
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                              ),
                                              errorWidget:
                                                  (context, url, error) =>
                                                      const Icon(Icons.error),
                                            )
                                          : widget.financeProduct.files![i]
                                                      .type ==
                                                  "audio"
                                              ? Card(
                                                  child: AppAudio(
                                                  audio: AudioModel(
                                                      link: widget
                                                          .financeProduct
                                                          .files![i]
                                                          .link!),
                                                ))
                                              : const SizedBox();
                            }),
                          ],
                        ),

                  const SizedBox(height: 10),
                  if (widget.isAutreProduct == false)
                    Obx(() => financeController
                            .isLoadingListInsuranceByInsurer.value
                        ? const LoaderComponent()
                        : financeController.listInsuranceByInsurer.isEmpty
                            ? Container()
                            : Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    widget.financeProduct.insurer != null
                                        ? "Autres produits de ${widget.financeProduct.insurer!.name}"
                                        : "Autres produits",
                                    style: getSemiBoldTextStyle(
                                        fontSize: 15, color: Colors.grey),
                                  ),
                                  const SeparatorWidget(size: 10),
                                  ProductOtherInsuranceWidget(
                                    currentId: widget.financeProduct.id!,
                                    list: financeController
                                        .listInsuranceByInsurer,
                                  )
                                ],
                              )),

                  const SizedBox(height: 10),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  videoCard() {
    return Column(
      children: [
        const SizedBox(
          height: AppSize.s10,
        ),
        Center(
          child: _controllerVideo.value.isInitialized
              ? GestureDetector(
                  onTap: () {
                    setState(() {
                      audioPlayer.pause();
                      _controllerVideo.value.isPlaying
                          ? _controllerVideo.pause()
                          : _controllerVideo.play();
                    });
                  },
                  child: Stack(
                    alignment: AlignmentDirectional.center,
                    children: [
                      SizedBox(
                        height: 200,
                        child: AspectRatio(
                          aspectRatio: _controllerVideo.value.aspectRatio,
                          child: VideoPlayer(_controllerVideo),
                        ),
                      ),
                      if (!_controllerVideo.value.isPlaying)
                        CircleAvatar(
                          backgroundColor: Colors.black.withOpacity(0.4),
                          child: Center(
                            child: Icon(
                              IconManager.play,
                              size: AppSize.s30,
                              color: ColorManager.white,
                            ),
                          ),
                        )
                    ],
                  ),
                )
              : Container(
                  height: 200,
                  width: double.infinity,
                  color: ColorManager.black,
                ),
        ),
        VideoProgressIndicator(_controllerVideo, //video player controller
            allowScrubbing: true,
            colors: VideoProgressColors(
              //video player progress bar
              backgroundColor: ColorManager.grey,
              playedColor: ColorManager.red,
              bufferedColor: ColorManager.grey1,
            )),
        const SizedBox(
          height: AppSize.s20,
        ),
      ],
    );
  }

  entrepriseInfo(EntrepriseModel entreprise) {
    Get.bottomSheet(
      isScrollControlled: true,
      Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10)),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (entreprise.logo != null)
                SizedBox(
                  width: 100,
                  height: 100,
                  child: CachedNetworkImage(
                    imageUrl: entreprise.logo!,
                    imageBuilder: (context, imageProvider) => Container(
                      margin: const EdgeInsets.only(
                        bottom: 10,
                      ),
                      //    height: MediaQuery.of(context).size.height / 2.5,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          scale: 1,
                          image: imageProvider,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    placeholder: (context, url) => const SizedBox(
                        // height: MediaQuery.of(context).size.height / 2.5,
                        // width: MediaQuery.of(context).size.width,
                        ),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                  ),
                ),
              Text(
                entreprise.name,
                style: getBoldTextStyle(color: Colors.black, fontSize: 20),
              ),
              const SizedBox(
                height: 10,
              ),
              GestureDetector(
                onTap: () async {
                  if (!await launchUrl(
                    Uri.parse(entreprise.webLink),
                    mode: LaunchMode.inAppWebView,
                  )) {
                    throw 'Could not launch ${entreprise.webLink}';
                  }
                },
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.link,
                      color: ColorManager.blue,
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Text(
                      "Consulter le site",
                      style: getRegularTextStyle(
                        color: ColorManager.blue,
                      ),
                    )
                  ],
                ),
              ),
              Wrap(
                runSpacing: 10,
                children: [
                  ...List.generate(entreprise.contacts.length, (i) {
                    return GestureDetector(
                      onTap: () {
                        makePhoneCall(entreprise.contacts[i]);
                      },
                      child: Container(
                        margin: const EdgeInsets.only(
                          left: 10,
                        ),
                        padding: const EdgeInsets.symmetric(
                          horizontal: 5,
                          vertical: 2,
                        ),
                        decoration: BoxDecoration(
                          color: ColorManager.lightGrey,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            const Icon(Icons.phone),
                            const SizedBox(
                              width: 5,
                            ),
                            Text(
                              entreprise.contacts[i],
                              overflow: TextOverflow.ellipsis,
                              style: getRegularTextStyle(
                                  fontSize: 12, color: ColorManager.blue),
                              maxLines: 2,
                            ),
                          ],
                        ),
                      ),
                    );
                  }),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              if (entreprise.description != null)
                HtmlWidget(entreprise.description!),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Duration duration = Duration.zero;
  bool findVideo = false;
  int indexPlay = -1;
  bool isPlaying = false;
  bool loadVideo = true;
  String audioPlay = "";
  String timeVideo = "";

  Duration position = Duration.zero;
  int selectedPlayerIdx = 0;

  AudioPlayer audioPlayer = AudioPlayer();
  Future setAudio() async {
    audioPlayer.setReleaseMode(ReleaseMode.loop);
  }

  getAudio(String linkAudio) {
    audioPlayer = AudioPlayer(playerId: linkAudio);
    audioPlayer.setSourceUrl(linkAudio).then((value) {
      audioPlayer.onPlayerStateChanged.listen((event) {
        if (mounted) {
          setState(() {
            isPlaying = event == PlayerState.playing;
          });
        }
      });
      audioPlayer.onDurationChanged.listen((newDuration) {
        if (mounted) {
          setState(() {
            duration = newDuration;
          });
        }
      });
      audioPlayer.onPositionChanged.listen((newposition) {
        if (mounted) {
          setState(() {
            position = newposition;
          });
        }
      });
      setState(() {
        isPlaying = false;
      });
    });
  }

  late VideoPlayerController _controllerVideo;
  getMedia() {
    for (var element in widget.financeProduct.files!) {
      if (element.link != "") {
        if (element.type == "video") {
          readerVideo(element.link!);
        }
        if (element.type == "audio") {
          getAudio(element.link!);
        }
      }
    }
  }

  readerVideo(String linkVideo) {
    setState(() {
      findVideo = true;
    });
    _controllerVideo = VideoPlayerController.network(
      linkVideo,
    )..initialize().then((_) {
        if (mounted) {
          setState(() {
            loadVideo = false;
            timeVideo = formatPlayTime(
                Duration(seconds: _controllerVideo.value.duration.inSeconds));
          });
        }

        _controllerVideo.pause();
      });
  }

  ListTile documentCard(String link) {
    return ListTile(
      contentPadding: const EdgeInsets.all(10),
      onTap: () {
        Navigator.push(
          context,
          PageTransition(
            type: PageTransitionType.rightToLeft,
            child: PdfScreenn(
              pdfLink: link,
            ),
            isIos: true,
            duration: const Duration(milliseconds: 400),
          ),
        );
      },
      tileColor: ColorManager.grey.withOpacity(0.2),
      leading: Image.asset(
        ImageAssets.lectureDoc,
      ),
      title: const Text("Voir le fichier"),
      trailing: const Icon(IconManager.arrowRight),
    );
    // Container(
    //   decoration: const BoxDecoration(),
    //   width: double.infinity,
    //   // height: AppSize.s50,
    //   child: Column(children: [
    //     Image.asset(
    //       image ?? ImageAssets.lectureDoc,
    //       width: 150,
    //     )
    //   ]),
    // );
  }
}
