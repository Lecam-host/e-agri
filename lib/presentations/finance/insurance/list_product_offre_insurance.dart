import 'package:eagri/domain/model/type_offre__insurance_model.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/finance/component/list_product_choise_insurance_widget.dart';
import 'package:flutter/material.dart';
import '../../prestations/prestations_home.dart';
import 'product_offre_card_insurance.dart';
import '../controller/finance_controller.dart';
import 'package:get/get.dart';

class ListProductOffreAssurance<T> extends StatefulWidget {
  final List<T> list;
  final List<DataTypeOffreInsuranceModel> listTypeInsurance;
  const ListProductOffreAssurance(
      {super.key, required this.list, required this.listTypeInsurance});

  @override
  State<ListProductOffreAssurance> createState() =>
      _ListProductOffreAssuranceState();
}

class _ListProductOffreAssuranceState extends State<ListProductOffreAssurance>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    FinanceController financeController = Get.put(FinanceController());
    return Obx(() => financeController.isLoadingCredit.value
        ? const LoaderComponent()
        : Column(
            children: [
              ListProductChoiseInsuranceWidget(
                list: widget.listTypeInsurance,
              ),
              financeController.isLoadListInsurance.value
                  ? const LoaderComponent()
                  : Expanded(
                      child: widget.list.isEmpty
                          ? const EmptyComponenent(
                              message: "Aucun produit pour ce type d'assurance",
                            )
                          : GridView.builder(
                              // physics: const BouncingScrollPhysics(),
                              shrinkWrap: true,
                              controller: financeController
                                  .scrollControllerInsurance.value,
                              itemCount: financeController
                                          .totalItemsInsurance.value >
                                      financeController.listInsurance.length
                                  ? financeController.listInsurance.length + 1
                                  : financeController.listInsurance.length,
                              gridDelegate:
                                  const SliverGridDelegateWithFixedCrossAxisCount(
                                      mainAxisSpacing: 10,
                                      crossAxisSpacing: 10,
                                      crossAxisCount: 2,
                                      mainAxisExtent: 250),
                              itemBuilder: (BuildContext context, int index) =>
                                  index ==
                                              financeController
                                                  .listInsurance.length &&
                                          financeController
                                                  .totalItemsInsurance.value >
                                              financeController
                                                  .listInsurance.length
                                      ? shimmerCustom()
                                      : ProducOffreInsuranceCard(
                                          product: widget.list[index],
                                        ),
                            ),
                    )
            ],
          ));
  }
}
