// import 'package:flutter/material.dart';
// import 'package:flutter/widgets.dart';
// import 'package:flutter_pdfview/flutter_pdfview.dart';
// import 'package:video_player/video_player.dart';
// import 'package:audioplayers/audioplayers.dart';
// import 'package:path_provider/path_provider.dart';

// class MediaWidget extends StatefulWidget {
//   final String mediaType;
//   final String mediaUrl;

//   MediaWidget({required this.mediaType, required this.mediaUrl});

//   @override
//   _MediaWidgetState createState() => _MediaWidgetState();
// }

// class _MediaWidgetState extends State<MediaWidget> {
//   VideoPlayerController? _videoPlayerController;
//   AudioPlayer _audioPlayer = AudioPlayer();
//   bool _isPlaying = false;
//   String? _pdfPath;

//   @override
//   void initState() {
//     super.initState();

//     if (widget.mediaType == 'video') {
//       _videoPlayerController = VideoPlayerController.network(widget.mediaUrl)
//         ..initialize().then((_) {
//           setState(() {});
//         });
//     } else if (widget.mediaType == 'audio') {
//       _initializeAudioPlayer();
//     } else if (widget.mediaType == 'pdf') {
//       _loadPdf();
//     }
//   }

//   @override
//   void dispose() {
//     _videoPlayerController?.dispose();
//     _audioPlayer.dispose();
//     super.dispose();
//   }

//   Future<void> _initializeAudioPlayer() async {
//     await _audioPlayer.setSourceUrl(widget.mediaUrl);
//   }

//   void _playAudio() async {
//      await _audioPlayer.set();
//       setState(() {
//         _isPlaying = true;
//       });
//   }

//   void _pauseAudio() async {
//   await _audioPlayer.pause();
//       setState(() {
//         _isPlaying = false;
//       });
//   }

//   void _stopAudio() async {
//     int result = await _audioPlayer.stop();
//     if (result == 1) {
//       setState(() {
//         _isPlaying = false;
//       });
//     }
//   }

//   Future<void> _loadPdf() async {
//     final appDir = await getApplicationDocumentsDirectory();
//     final fileName = widget.mediaUrl.split('/').last;
//     final filePath = '${appDir.path}/$fileName';
//     setState(() {
//       _pdfPath = filePath;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     if (widget.mediaType == 'video') {
//       if (_videoPlayerController != null &&
//           _videoPlayerController!.value.isInitialized) {
//         return AspectRatio(
//           aspectRatio: _videoPlayerController!.value.aspectRatio,
//           child: VideoPlayer(_videoPlayerController!),
//         );
//       } else {
//         return const Center(
//           child: CircularProgressIndicator(),
//         );
//       }
//     } else if (widget.mediaType == 'pdf') {
//       if (_pdfPath != null) {
//         return PDFView(
//           filePath: _pdfPath!,
//         );
//       } else {
//         return const Center(
//           child: CircularProgressIndicator(),
//         );
//       }
//     } else if (widget.mediaType == 'audio') {
//       return Column(
//         children: [
//           IconButton(
//             icon: Icon(_isPlaying ? Icons.pause : Icons.play_arrow),
//             onPressed: () {
//               if (_isPlaying) {
//                 _pauseAudio();
//               } else {
//                 _playAudio();
//               }
//             },
//           ),
//           IconButton(
//             icon: const Icon(Icons.stop),
//             onPressed: _stopAudio,
//           ),
//         ],
//       );
//     } else {
//       return const Text('Type de média non pris en charge.');
//     }
//   }
// }
