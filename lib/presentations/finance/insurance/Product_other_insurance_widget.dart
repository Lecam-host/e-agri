// ignore_for_file: file_names

import 'package:animations/animations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/domain/model/offre_insurance_model.dart';
import 'package:eagri/presentations/finance/component/tag_widget.dart';

import 'package:flutter/material.dart';

import '../../../app/constant.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/styles_manager.dart';
import 'details_offre_insurance.dart';

class ProductOtherInsuranceWidget<T> extends StatelessWidget {
  const ProductOtherInsuranceWidget({
    super.key,
    required this.list,
    required this.currentId,
  });
  final int currentId;
  final List<ItemOffreInsuranceModel> list;
  @override
  Widget build(BuildContext context) {
    final newList = list.where((element) => element.id != currentId).toList();
    return GridView.builder(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: newList.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            mainAxisSpacing: 10,
            crossAxisSpacing: 10,
            crossAxisCount: 2,
            mainAxisExtent: 250),
        itemBuilder: (BuildContext context, int index) {
          return OpenContainer(
            openElevation: 0,
            closedElevation: 0,
            transitionType: transitionType,
            transitionDuration: transitionDuration,
            openBuilder: (context, _) => DetailsOffreInsuranceScreen(
              isAutreProduct: true,
              financeProduct: newList[index],
            ),
            closedBuilder: (context, VoidCallback openContainer) => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Stack(
                  children: [
                    CachedNetworkImage(
                      imageUrl: newList[index].illustrationImage == null
                          ? "https://images.unsplash.com/photo-1575936123452-b67c3203c357?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8aW1hZ2V8ZW58MHx8MHx8&w=1000&q=80"
                          : newList[index].illustrationImage!,
                      imageBuilder: (context, imageProvider) => Container(
                        width: double.maxFinite,
                        height: 150,
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(5),
                              topRight: Radius.circular(5)),
                          image: DecorationImage(
                            scale: 1,
                            image: imageProvider,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      placeholder: (context, url) => Container(
                        color: ColorManager.grey.withOpacity(0.2),
                        height: 150,
                        width: double.infinity,
                      ),
                      errorWidget: (context, url, error) => Container(
                        color: ColorManager.red,
                        height: 150,
                        width: double.infinity,
                        child: const Icon(Icons.error),
                      ),
                    ),
                    // if (data.typeOffers.toLowerCase() == "vente" &&
                    //     data.code == codeProductAgricultural.toString())
                    // Positioned(
                    //   top: 5,
                    //   left: 5,
                    //   child: Container(
                    //     // alignment: Alignment.center,
                    //     padding: const EdgeInsets.only(
                    //         bottom: 4, top: 2, right: 3, left: 3),
                    //     decoration: BoxDecoration(
                    //       borderRadius: BorderRadius.circular(5),
                    //       color: ColorManager.primary.withOpacity(0.7),
                    //     ),
                    //     child: Text(
                    //       "En ${data.typeVente}",
                    //       style: getRegularTextStyle(
                    //           color: ColorManager.white, fontSize: 10),
                    //     ),
                    //   ),
                    // ),
                    // Positioned(
                    //   bottom: 5,
                    //   right: 5,
                    //   child: CircleAvatar(
                    //     radius: iconSize,
                    //     backgroundColor: ColorManager.white.withOpacity(0.8),
                    //     child: SvgPicture.asset(SvgManager.like,
                    //         color: ColorManager.black,
                    //         width: iconSize,
                    //         height: iconSize),
                    //   ),
                    // ),

                    // Positioned(
                    //   bottom: 5,
                    //   left: 5,
                    //   child: CircleAvatar(
                    //     radius: iconSize,
                    //     backgroundColor: ColorManager.white.withOpacity(0.8),
                    //     child: SvgPicture.asset(SvgManager.search,
                    //         color: ColorManager.black,
                    //         width: iconSize,
                    //         height: iconSize),
                    //   ),
                    // )
                  ],
                ),
                Container(
                  decoration: BoxDecoration(
                    color: ColorManager.white,
                    borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10),
                    ),
                  ),
                  padding: const EdgeInsets.all(8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        // height: 20,
                        child: Text(
                          newList[index].name!,
                          style: getMeduimTextStyle(
                            color: ColorManager.black,
                            fontSize: 12,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      SizedBox(
                        child: Text(
                          newList[index].insurer == null
                              ? ""
                              : newList[index].insurer!.name!,
                          style: getRegularTextStyle(
                            color: ColorManager.grey,
                            fontSize: 12,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),

                      // _buildSoldPoint(4.5, 6937),

                      // ProductRate(
                      //   product: data,
                      // )
                    ],
                  ),
                ),
                TageWidget(
                  label: newList[index].typeInsurance == null
                      ? ""
                      : newList[index].typeInsurance!.name!,
                  backgrounColor: Colors.transparent,
                )
              ],
            ),
          );
        });
  }
}
