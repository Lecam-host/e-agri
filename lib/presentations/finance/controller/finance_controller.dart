import 'dart:developer';
import 'package:eagri/app/di.dart';
import 'package:eagri/data/request/request_offre_insurance_model.dart';
import 'package:eagri/domain/model/creditor_model.dart';
import 'package:eagri/domain/model/insurer_model.dart';
import 'package:eagri/domain/model/offre_insurance_model.dart';
import 'package:eagri/domain/model/type_offre__insurance_model.dart';
import 'package:eagri/domain/model/type_offre_credit_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../data/repository/api/repository_impl.dart';
import '../../../data/request/request_offre_credit_model.dart';

import '../../../domain/model/offre_credit_model.dart';

class FinanceController extends GetxController
    with GetSingleTickerProviderStateMixin {
  var tabBarIndex = 0.obs;
  var scrollControllerInsurance = ScrollController().obs;
  var scrollControllerCredit = ScrollController().obs;
  RepositoryImpl remoteImpl = instance<RepositoryImpl>();
  var listInsurance = <ItemOffreInsuranceModel>[].obs;
  var listCredit = <ItemOffreCreditModel>[].obs;
  var listCreditor = <ItemCreditorModel>[].obs;
  var listInsurer = <ItemInsurerModel>[].obs;
  var listTypeInsurance = <DataTypeOffreInsuranceModel>[].obs;
  var listTypeCredit = <DataTypeOffreCreditModel>[].obs;
  var listInsuranceByInsurer = <ItemOffreInsuranceModel>[].obs;
  var listCreditByCreditor = <ItemOffreCreditModel>[].obs;
  var isLoadingAsssurance = true.obs;
  var isLoadingCredit = true.obs;
  var isLoadingListInsuranceByInsurer = true.obs;
  var isLoadListInsurance = false.obs;
  var isLoadListCredit = false.obs;

  var listTypeInsuranceMap = <Map<String, dynamic>>[].obs;
  var listTypeCreditMap = <Map<String, dynamic>>[].obs;
  var listCreditorMap = <Map<String, dynamic>>[].obs;
  var listInsurerMap = <Map<String, dynamic>>[].obs;

  var typeInsuranceSeleted = 0.obs;
  var typeCreditSeleted = 0.obs;
  var creditorSeleted = 0.obs;
  var insurerSeleted = 0.obs;

  var isLoadMoreInsurance = false.obs;
  var totalItemsInsurance = 0.obs;
  var pageInsurance = 0.obs;

  var isLoadMoreCredit = false.obs;
  var totalItemsCredit = 0.obs;
  var pageCredit = 0.obs;
  var prixMinController = 0.0.obs;
  var prixMaxController = 0.0.obs;

  var businessLineCreditSelected = "".obs;
  var businessLineInsuranceSelected = "".obs;

  var listBusinessLineCreditMap = <Map<String, dynamic>>[
    {"label": "Crédit", "value": "crdt"},
    {"label": "Micro-crédit", "value": "microfinance"},
  ];
  var listBusinessLineInsuranceMap = <Map<String, dynamic>>[
    {"label": "Assurance", "value": "assurance"},
    {"label": "Miro-assurance", "value": "microassurance"},
  ];

  fetchInsurance() async {
    isLoadingAsssurance.value = true;
    await remoteImpl
        .getInsurance(RequestOffreInsuranceModel(
          page: pageInsurance.value,
          perPage: 10,
          businessLineSlug: businessLineInsuranceSelected.value == ""
              ? null
              : businessLineInsuranceSelected.value,
          insuranceId: null,
          insuranceName: null,
          insurerId: insurerSeleted.value == 0 ? null : insurerSeleted.value,
          insurerName: null,
          sortBy: "id",
          typeInsuranceId: typeInsuranceSeleted.value == 0
              ? null
              : typeInsuranceSeleted.value,
          typeInsuranceName: null,
          prixMax:
              prixMaxController.value == 0.0 ? null : prixMaxController.value,
          prixMin:
              prixMinController.value == 0.0 ? null : prixMinController.value,
        ))
        .then((value) => value.fold((l) => log(l.toString()), (r) {
              totalItemsInsurance.value = r.data!.total!;
              listInsurance.value =
                  r.data!.items!.whereType<ItemOffreInsuranceModel>().toList();
              isLoadingAsssurance.value = false;
            }))
        .catchError((onError) {
      log(onError);
      isLoadingAsssurance.value = false;
    });
  }

  fetchCredit() async {
    isLoadingCredit.value = true;
    await remoteImpl
        .getCredit(RequestOffreCreditModel(
          page: pageCredit.value,
          perPage: 10,
          creditId: null,
          businessLineSlug: businessLineCreditSelected.value == ""
              ? null
              : businessLineCreditSelected.value,
          creditorId: creditorSeleted.value == 0 ? null : creditorSeleted.value,
          creditorName: null,
          sortBy: "id",
          typeCreditId:
              typeCreditSeleted.value == 0 ? null : typeCreditSeleted.value,
          typeCreditName: null,
          prixMax:
              prixMaxController.value == 0.0 ? null : prixMaxController.value,
          prixMin:
              prixMinController.value == 0.0 ? null : prixMinController.value,
        ))
        .then((value) => value.fold(
              (l) {
                log(l.toString());
                inspect(l);
                isLoadingCredit.value = false;
              },
              (r) {
                totalItemsCredit.value = r.data!.total!;
                listCredit.value =
                    r.data!.items!.whereType<ItemOffreCreditModel>().toList();
                isLoadingCredit.value = false;

                log("message");
              },
            ))
        .catchError((onError) {
      log(onError);
      isLoadingCredit.value = false;
    });
  }

  fetchTypeInsurance() async {
    await remoteImpl
        .getTypeInsurance()
        .then((value) => value.fold(
              (l) => log(l.toString()),
              (r) {
                listTypeInsurance.value = r.data!;
                createMapForList(listTypeInsurance, listTypeInsuranceMap);
              },
            ))
        .catchError((onError) => log(onError));
  }

  fetchTypeCredit() async {
    await remoteImpl
        .getTypeCredit()
        .then((value) => value.fold(
              (l) {
                log(l.toString());
                inspect(l);
              },
              (r) {
                listTypeCredit.value = r.data!;
                createMapForList(listTypeCredit, listTypeCreditMap);
              },
            ))
        .catchError((onError) => null);
  }

  fetchCreditor() async {
    await remoteImpl
        .getCreditor()
        .then((value) => value.fold(
              (l) {
                log(l.toString());
                inspect(l);
              },
              (r) {
                listCreditor.value = r.data!.items!;
                createMapForList(listCreditor, listCreditorMap);
                inspect(listCreditorMap);
              },
            ))
        .catchError((onError) => null);
  }

  fetchInsurer() async {
    await remoteImpl
        .getInsurer()
        .then((value) => value.fold(
              (l) {
                log(l.toString());
                inspect(l);
              },
              (r) {
                listInsurer.value = r.data!.items!;
                createMapForList(listInsurer, listInsurerMap);
              },
            ))
        .catchError((onError) => null);
  }

  createMapForList(List<dynamic> list, List<Map<String, dynamic>> listMap) {
    for (var element in list) {
      listMap.add({'value': element.id, 'label': element.name});
    }
  }

  addItemInfList(int lastId, int currentId) {
    if (lastId == currentId) {
      lastId = 0;
    } else {
      lastId = currentId;
    }
  }

  isSelected(int lastId, int currentId) {
    log((lastId == currentId).toString());
    return lastId == currentId;
  }

  fetchInsuranceByInsurer(int insurerId) async {
    isLoadingListInsuranceByInsurer.value = true;
    await remoteImpl
        .getInsuranceByInsurer(insurerId)
        .then((value) => value.fold(
              (l) {
                inspect(l.toString());
                isLoadingListInsuranceByInsurer.value = false;
              },
              (r) {
                listInsuranceByInsurer.value = r.data!;
                isLoadingListInsuranceByInsurer.value = false;
              },
            ))
        .catchError((onError) => null);
  }

  fetchCreditByICreditor(int creditorId) async {
    isLoadingListInsuranceByInsurer.value = true;
    await remoteImpl
        .getCreditByCreditor(creditorId)
        .then((value) => value.fold(
              (l) {
                inspect(l.toString());
                isLoadingListInsuranceByInsurer.value = false;
              },
              (r) {
                if (r.data != null) listCreditByCreditor.value = r.data!.items!;
                isLoadingListInsuranceByInsurer.value = false;
              },
            ))
        .catchError((onError) => null);
  }

  @override
  void onInit() async {
    fetchInsurance();
    fetchTypeInsurance();
    fetchInsurer();

    fetchCredit();
    fetchCreditor();
    fetchTypeCredit();

    scrollControllerInsurance.value.addListener(_scrollListenerInsurance);
    scrollControllerCredit.value.addListener(_scrollListenerCredit);

    super.onInit();
  }

  moreGetListInsurance() async {
    await remoteImpl
        .getInsurance(RequestOffreInsuranceModel(
          page: 0,
          perPage: 10,
          businessLineSlug: null,
          insuranceId: null,
          insuranceName: null,
          insurerId: null,
          insurerName: null,
          sortBy: "id",
          typeInsuranceId: typeInsuranceSeleted.value == 0
              ? null
              : typeInsuranceSeleted.value,
          typeInsuranceName: null,
        ))
        .then((value) => value.fold((l) {
              log(l.toString());
              isLoadMoreInsurance.value = false;
            }, (r) {
              totalItemsInsurance.value = r.data!.total!;
              listInsurance.addAll(
                  r.data!.items!.whereType<ItemOffreInsuranceModel>().toList());
              isLoadMoreInsurance.value = false;
            }))
        .catchError((onError) {
      log(onError);
      isLoadMoreInsurance.value = false;
    });
  }

  moreGetListCredit() async {
    await remoteImpl
        .getCredit(RequestOffreCreditModel(
          page: pageCredit.value,
          perPage: 10,
          businessLineSlug: null,
          creditorId: null,
          creditorName: null,
          sortBy: "id",
          typeCreditId:
              typeCreditSeleted.value == 0 ? null : typeCreditSeleted.value,
          typeCreditName: null,
        ))
        .then((value) => value.fold((l) {
              log(l.toString());
              isLoadMoreCredit.value = false;
            }, (r) {
              totalItemsCredit.value = r.data!.total!;
              listCredit.addAll(
                  r.data!.items!.whereType<ItemOffreCreditModel>().toList());
              isLoadMoreCredit.value = false;
            }))
        .catchError((onError) {
      log(onError);
      isLoadMoreCredit.value = false;
    });
  }

  void _scrollListenerInsurance() async {
    log("TotalPage :${totalItemsInsurance.value}");
    log("Total liste :${listInsurance.length}");
    if (isLoadMoreInsurance.value ||
        totalItemsInsurance.value == listInsurance.length) {
      return null;
    } else if (scrollControllerInsurance.value.offset >=
            scrollControllerInsurance.value.position.maxScrollExtent &&
        !scrollControllerInsurance.value.position.outOfRange) {
      log("EN LOAD MORE");
      pageInsurance.value = pageInsurance.value + 1;
      isLoadMoreInsurance.value = true;
      await moreGetListInsurance();
    }
  }

  void _scrollListenerCredit() async {
    log("TotalPage :${totalItemsInsurance.value}");
    log("Total liste :${listInsurance.length}");
    if (isLoadMoreCredit.value || totalItemsCredit.value == listCredit.length) {
      return null;
    } else if (scrollControllerCredit.value.offset >=
            scrollControllerCredit.value.position.maxScrollExtent &&
        !scrollControllerCredit.value.position.outOfRange) {
      log("EN LOAD MORE");
      pageCredit.value = pageCredit.value + 1;
      isLoadMoreCredit.value = true;
      await moreGetListInsurance();
    }
  }

  clearSetting() {
    if (tabBarIndex.value == 0) {
      typeInsuranceSeleted.value = 0;
      insurerSeleted.value = 0;
      businessLineInsuranceSelected.value = "";
    } else {
      typeCreditSeleted.value = 0;
      creditorSeleted.value = 0;
      businessLineCreditSelected.value = "";
    }
    prixMaxController.value = 0.0;
    prixMinController.value = 0.0;
  }
}
