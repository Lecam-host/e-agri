import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/finance/insurance/list_product_offre_insurance.dart';
import 'package:eagri/presentations/finance/credit/list_product_offre_credit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:select_form_field/select_form_field.dart';
import '../ressources/assets_manager.dart';
import '../ressources/color_manager.dart';
import '../ressources/styles_manager.dart';
import 'controller/finance_controller.dart';

class FinanceScreen extends StatefulWidget {
  const FinanceScreen({super.key});

  @override
  State<FinanceScreen> createState() => _FinanceScreenState();
}

class _FinanceScreenState extends State<FinanceScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  late FinanceController financeController = Get.put(FinanceController());
  final GlobalKey<ScaffoldState> _key = GlobalKey();
  final _focusNodePrixMin = FocusNode();
  final _focusNodePrixMax = FocusNode();

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);
    _tabController.addListener(() {
      financeController.tabBarIndex.value = _tabController.index;
    });

    super.initState();
  }

  @override
  void dispose() {
    _focusNodePrixMin.dispose();
    _focusNodePrixMax.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        key: _key,
        endDrawer: Drawer(
          child: ListView(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            children: [
              DrawerHeader(
                child: Center(
                  child: Text(
                    'Filtrez votre recherche',
                    style: getSemiBoldTextStyle(fontSize: 17),
                  ),
                ),
              ),
              Obx(
                () => Text(
                  'Type ${financeController.tabBarIndex.value == 0 ? 'd\'Assurance' : 'de Crédit'}',
                  style: getMeduimTextStyle(fontSize: 14),
                ),
              ),
              const SizedBox(height: 5),
              Obx(() => SelectFormField(
                    // controller: shopController.categoryController.value,
                    type: SelectFormFieldType.dropdown, // or can be dialog
                    enableSearch: true,
                    // initialValue: 'circle',
                    labelText:
                        'Type ${financeController.tabBarIndex.value == 0 ? 'Assurance' : 'Crédit'}',

                    items: financeController.tabBarIndex.value == 0
                        ? financeController.listTypeInsuranceMap
                        : financeController.listTypeCreditMap,
                    onChanged: (val) {
                      if (financeController.tabBarIndex.value == 0) {
                        financeController.typeInsuranceSeleted.value =
                            int.parse(val);
                        financeController.fetchInsurance();
                      } else {
                        financeController.typeCreditSeleted.value =
                            int.parse(val);
                        financeController.fetchCredit();
                      }
                    },
                    onSaved: (val) {},
                  )),
              const SizedBox(height: 20),
              Text(
                'Entreprise',
                style: getMeduimTextStyle(fontSize: 14),
              ),
              const SizedBox(height: 5),
              Obx(() => SelectFormField(
                    // controller: shopController.categoryController.value,
                    type: SelectFormFieldType.dropdown, // or can be dialog
                    enableSearch: true,
                    // initialValue: 'circle',
                    labelText: 'Choisir l\'entreprise',
                    items: financeController.tabBarIndex.value == 0
                        ? financeController.listInsurerMap
                        : financeController.listCreditorMap,
                    onChanged: (val) {
                      if (financeController.tabBarIndex.value == 0) {
                        financeController.pageInsurance.value = 0;
                        financeController.insurerSeleted.value = int.parse(val);
                        financeController.fetchInsurance();
                      } else {
                        financeController.pageCredit.value = 0;
                        financeController.creditorSeleted.value =
                            int.parse(val);
                        financeController.fetchCredit();
                      }
                    },
                    onSaved: (val) {},
                  )),
              const SizedBox(height: 20),
              Obx(() => Text(
                    'Sous ${financeController.tabBarIndex.value == 0 ? 'assurance' : 'crédit'}',
                    style: getMeduimTextStyle(fontSize: 14),
                  )),
              const SizedBox(height: 5),

              Obx(() => SelectFormField(
                    // controller: shopController.categoryController.value,
                    type: SelectFormFieldType.dropdown, // or can be dialog
                    enableSearch: true,
                    // initialValue: 'circle',
                    labelText: 'Choisir le secteur',
                    items: financeController.tabBarIndex.value == 0
                        ? financeController.listBusinessLineInsuranceMap
                        : financeController.listBusinessLineCreditMap,
                    onChanged: (val) {
                      if (financeController.tabBarIndex.value == 0) {
                        financeController.pageInsurance.value = 0;
                        financeController.businessLineInsuranceSelected.value =
                            val;
                        financeController.fetchInsurance();
                      } else {
                        financeController.pageCredit.value = 0;
                        financeController.businessLineCreditSelected.value =
                            val;

                        financeController.fetchCredit();
                      }
                    },
                    onSaved: (val) {},
                  )),
              const SizedBox(height: 20),
              Text(
                'Prix min',
                style: getMeduimTextStyle(fontSize: 14),
              ),
              const SizedBox(height: 5),
              TextField(
                onChanged: (value) {
                  if (value != "") {
                    financeController.prixMinController.value =
                        double.parse(value);
                  } else {
                    financeController.prixMinController.value = 0.0;
                  }
                  financeController.tabBarIndex.value == 0
                      ? financeController.fetchInsurance()
                      : financeController.fetchCredit();
                },
                focusNode: _focusNodePrixMin,
                keyboardType: const TextInputType.numberWithOptions(),
                decoration: const InputDecoration(
                    label: Text('Prix min'), hintText: 'Enter le prix min'),
              ),
              const SizedBox(height: 20),
              Text(
                'Prix max',
                style: getMeduimTextStyle(fontSize: 14),
              ),
              const SizedBox(height: 5),
              TextField(
                onChanged: (value) {
                  if (value != "") {
                    financeController.prixMaxController.value =
                        double.parse(value);
                  } else {
                    financeController.prixMaxController.value = 0.0;
                  }

                  financeController.tabBarIndex.value == 0
                      ? financeController.fetchInsurance()
                      : financeController.fetchCredit();
                },
                focusNode: _focusNodePrixMax,
                keyboardType: const TextInputType.numberWithOptions(),
                decoration: const InputDecoration(
                    label: Text('Prix max'), hintText: 'Enter le prix max'),
              ),
              const SizedBox(height: 20),
              // DefaultButton(
              //   text: "Filtrer",
              //   press: () {
              //     log(_tabController.index.toString());
              //   },
              // ),
            ],
          ),
        ),
        appBar: AppBar(
          toolbarHeight: 40,
          titleSpacing: 20,
          elevation: 1,
          leading: const BackButtonCustom(),
          title: Text(
            'Finance',
            style: TextStyle(
              fontSize: 15,
              fontStyle: FontStyle.normal,
              color: ColorManager.black,
            ),
          ),
          bottom: TabBar(
            controller: _tabController,
            indicatorColor: ColorManager.primary,
            tabs: [
              Tab(
                child: Text(
                  "Assurance",
                  style: getMeduimTextStyle(color: ColorManager.black),
                ),
              ),
              Tab(
                child: Text(
                  "Crédit",
                  style: getMeduimTextStyle(color: ColorManager.black),
                ),
              ),
            ],
          ),
          actions: [
            Container(
              margin: const EdgeInsets.only(
                top: 5,
              ),
              child: IconButton(
                icon: SvgPicture.asset(
                  SvgManager.filter,
                  width: 20,
                ),
                onPressed: () {
                  _key.currentState!.openEndDrawer();
                },
              ),
            ),
            // Container(
            //   margin: const EdgeInsets.only(
            //     top: 5,
            //   ),
            //   child: CartButtomComponent(),
            // ),
            IconButton(
              onPressed: () {
                financeController.onInit();
              },
              icon: const Icon(Icons.refresh),
              color: Colors.black,
            ),
            const SizedBox(
              width: 10,
            ),
          ],
        ),
        body: TabBarView(
          controller: _tabController,
          children: [
            Padding(
              padding: const EdgeInsets.all(10),
              child: ListProductOffreAssurance(
                listTypeInsurance: financeController.listTypeInsurance,
                list: financeController.listInsurance,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: ListProductOffreCredit(
                listTypeCredit: financeController.listTypeCredit,
                list: financeController.listCredit,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
