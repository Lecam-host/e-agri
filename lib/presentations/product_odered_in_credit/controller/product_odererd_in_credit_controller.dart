import 'package:get/get.dart';
import '../../../app/di.dart';
import '../../../data/repository/api/repository_impl.dart';
import '../../../domain/model/odered_product_credit_model.dart';

class ProductOderedInCreditController extends GetxController{
 

var listOderedProductCreditReceive=<OderedProductCredit>[].obs;
var listOderedProductCreditSend=<OderedProductCredit>[].obs;

var isLoadDataReceive =true.obs;
var isLoadDataSend =true.obs;
 RepositoryImpl repositoryImpl = instance<RepositoryImpl>();

  getProductOderedInCreditReceive()async{
   await repositoryImpl.getProductOderedInCredit("SELLER").then((response) async{
   await response.fold((failure) {
    isLoadDataReceive.value=false;
   }, (data)async{
    
        listOderedProductCreditReceive.value=data.data.items;
    isLoadDataReceive.value=false;


      });
    });
  }

   getProductOderedInCreditSend()async{
   await repositoryImpl.getProductOderedInCredit("BUYER").then((response) async{
   await response.fold((failure) {
    isLoadDataSend.value=false;
   }, (data)async{
    
        listOderedProductCreditSend.value=data.data.items;
    isLoadDataSend.value=false;


      });
    });
  }
}
