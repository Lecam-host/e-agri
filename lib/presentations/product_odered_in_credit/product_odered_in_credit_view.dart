
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/product_odered_in_credit/product_odered_credit_card.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../app/di.dart';
import '../../data/repository/api/repository_impl.dart';
import '../ressources/color_manager.dart';
import '../ressources/styles_manager.dart';
import 'controller/product_odererd_in_credit_controller.dart';

class ProductsOderedInCreditView extends StatefulWidget {
  const ProductsOderedInCreditView({super.key,});
  @override
  State<ProductsOderedInCreditView> createState() => _ProductsOderedInCreditViewState();
}

class _ProductsOderedInCreditViewState extends State<ProductsOderedInCreditView>
   with SingleTickerProviderStateMixin {
  late TabController tabController;
ProductOderedInCreditController productOderedInCredit =Get.put(ProductOderedInCreditController());
    RepositoryImpl repositoryImpl = instance<RepositoryImpl>();


  @override
  void initState() {
    productOderedInCredit.getProductOderedInCreditReceive();
    productOderedInCredit.getProductOderedInCreditSend();
     tabController = TabController(
      length: 2,
      vsync: this,
    );
    super.initState();
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(title:const  Text("Mes demandes en credit"),
      leading:const BackButtonCustom(),
       bottom: TabBar(
            indicatorColor: ColorManager.primary,
            controller: tabController,
            tabs: [
              Text(
                "Reçues",
                style: getBoldTextStyle(),
              ),
              Text(
                "Envoyées",
                style: getBoldTextStyle(),
              ),
            ],
          ),
      ),
      body: TabBarView(
      controller: tabController,
     children: [ Obx(()=> SizedBox(
         child:productOderedInCredit.isLoadDataReceive.value==false? 
         productOderedInCredit.listOderedProductCreditReceive.isNotEmpty?
         ListView.builder(
                  shrinkWrap: true,
                  itemCount: productOderedInCredit.listOderedProductCreditReceive.length,
                  itemBuilder: (context, index) => Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      color: ColorManager.white,
                      child:ProductOderedInCreditCard(productOdered : productOderedInCredit.listOderedProductCreditReceive[index])
                    ),
                  ),
                ):const EmptyComponenent(message: "Liste vide",):const LoaderComponent(),
       ),),Obx(()=> SizedBox(
         child:productOderedInCredit.isLoadDataSend.value==false? 
         productOderedInCredit.listOderedProductCreditSend.isNotEmpty?
         ListView.builder(
                  shrinkWrap: true,
                  itemCount: productOderedInCredit.listOderedProductCreditSend.length,
                  itemBuilder: (context, index) => Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      color: ColorManager.white,
                      child:ProductOderedInCreditCard(productOdered : productOderedInCredit.listOderedProductCreditSend[index],isReceive:false,)
                    ),
                  ),
                ):const EmptyComponenent(message: "Liste vide",):const LoaderComponent(),
       ))],
     ),);
  }


 

}