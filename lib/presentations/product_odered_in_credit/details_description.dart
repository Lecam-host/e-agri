import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';

import '../../../../app/functions.dart';

import '../../domain/model/Product.dart';
import '../../domain/model/odered_product_credit_model.dart';

class DetailsDemandesCredit extends StatelessWidget {
 final OderedProductCredit productOdered;
final  ProductModel product;
  const DetailsDemandesCredit({Key? key, required this.productOdered,required this.product})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
        product.title,
            style: getBoldTextStyle(
              fontSize: 14,
              color: ColorManager.primary,
            ),
          ),
          Text(
            "Produit",
            style: getRegularTextStyle(
              color: ColorManager.grey,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            "Info sur la demande",
            style: getMeduimTextStyle(
              color: ColorManager.black,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            color: ColorManager.white,
            width: double.infinity,
            padding: const EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Paiement à credit",style: getSemiBoldTextStyle(color: ColorManager.blue),),
                
                // 
                Text(
                  "Type de demande",
                  style: getRegularTextStyle(
                    color: ColorManager.grey,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                if (productOdered.acheteur != null) ...[
                  cardInfo(
                      title: "Demandeur",
                      value:
                          "${productOdered.acheteur!.firstname} ${productOdered.acheteur!.laststname}"),
                  const SizedBox(
                    height: 10,
                  ),
                ],
                if (productOdered.acheteur != null) ...[
                  if (productOdered.acheteur!.phone!= null)
                  cardInfo(
                    title: "Numero",
                    value: productOdered.acheteur!.phone!,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                ],
                // if (
                //     demandeData.confirmationCode != null) ...[
                //   Text(
                //     "${demandeData.confirmationCode}",
                //     style: getBoldTextStyle(
                //       fontSize: 12,
                //       color: ColorManager.jaune,
                //     ),
                //   ),
                //   Text(
                //     "Code de confirmation",
                //     style: getRegularTextStyle(
                //       color: ColorManager.grey,
                //     ),
                //   ),
                //   const SizedBox(
                //     height: 10,
                //   ),
                // ],
                // cardInfo(
                //   title: "Statut",
                //   value: productOdered.status == true
                //       ? "Paiement confirmé"
                //       : productOdered.sellerAgrees == null
                //           ? "Pas encore accepté"
                //           : productOdered.sellerAgrees == true
                //               ? "Accepté"
                //               : "Refusé",
                //   color: productOdered.isConfirmed == true
                //       ? Colors.green
                //       : productOdered.sellerAgrees == null
                //           ? Colors.orange
                //           : productOdered.sellerAgrees == true
                //               ? ColorManager.primary2
                //               : ColorManager.red,
                // ),
                // const SizedBox(
                //   height: 10,
                // ),
                cardInfo(
                  title: "Date de la demannde",
                  value: convertDateWithTime(productOdered.createdAt.toString()),
                ),
                const SizedBox(
                  height: 10,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

Widget cardInfo({required String title, required String value, Color? color}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        value,
        style: getBoldTextStyle(
          fontSize: 12,
          color: color ?? ColorManager.black,
        ),
      ),
      Text(
        title,
        style: getRegularTextStyle(
          color: ColorManager.grey,
        ),
      ),
    ],
  );
}
