import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';

import '../../app/constant.dart';
import '../../app/di.dart';
import '../../app/functions.dart';
import '../../data/repository/api/repository_impl.dart';
import '../../data/request/request.dart';
import '../../domain/model/Product.dart';
import '../../domain/model/info_by_id_model.dart';
import '../../domain/model/odered_product_credit_model.dart';
import '../common/cache_network_image.dart';
import '../ressources/color_manager.dart';
import '../ressources/strings_manager.dart';
import '../ressources/styles_manager.dart';
import 'details_product_ordored.dart';

class ProductOderedInCreditCard extends StatefulWidget {
  const ProductOderedInCreditCard({super.key,required this.productOdered,this.isReceive=true});
final OderedProductCredit productOdered ;
final bool isReceive ;
  @override
  State<ProductOderedInCreditCard> createState() => _ProductOderedInCreditCardState();
}

class _ProductOderedInCreditCardState extends State<ProductOderedInCreditCard> {
   ProductModel? product;
   ClientModel ?acheteur;
   ClientModel ?vendeur;
late OderedProductCredit productOderedShow;
    RepositoryImpl repositoryImpl = instance<RepositoryImpl>();

bool isLoad =true;
 getDetailsProduct() async {
    setState(() {
      isLoad = true;
    });

    await repositoryImpl
        .getDetailProduct(widget.productOdered.productId)
        .then((response) async {
      await response.fold((failure) {}, (data) async {
       product = (await data);
   if(mounted) {
     setState(() {
          isLoad = false;
          product;
        });
   }
      });
    });
   if(mounted) {
     setState(() {
      isLoad = false;
    });
   }
  }
  getAcheteur(){
    repositoryImpl.getInfoById(InfoByIdRequest(userId: widget.productOdered.userId)).then((response) {response.fold((l) {}, (data) {
     if(mounted) {
       setState(() {
        acheteur=data.user; 
        productOderedShow.acheteur =data.user;     

      });
     }
    });});
  }
   getVendeur(){
    repositoryImpl.getInfoById(InfoByIdRequest(userId: widget.productOdered.vendorId)).then((response) {response.fold((l) {}, (data) {
    if(mounted) {
      setState(() {
        vendeur=data.user; 
        productOderedShow.vendeur =data.user;     
      });
    }
    });});
  }
  @override
  void initState() {productOderedShow= widget.productOdered;
 if( widget.isReceive==true)  {
    getAcheteur();
 }else{
    getVendeur();
 }
   
   
    getDetailsProduct();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return isLoad==true? shimmerCustomOdered():
    product!=null? InkWell(
      splashColor: ColorManager.primary4,
      onTap: () {
      
       Get.to(DetailsDemandeCredit(product: product!, productOdered: productOderedShow,isReceive: widget.isReceive,));
      },
      child: Container(
        margin: const EdgeInsets.only(top: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: ColorManager.white,
        ),
        padding: const EdgeInsets.all(5),
        child: Row(
          children: [
            if (product!.images!.isNotEmpty)
              Container(
                width: 60.0,
                height: 80,
                decoration: BoxDecoration(
                  color: ColorManager.grey,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(5.0),
                  child: CacheNetworkImage(
                    image: product!.images![0],
                  ),
                ),
              ),
            if (product!.images!.isEmpty)
              Container(
                width: 60.0,
                height: 80,
                decoration: BoxDecoration(
                  color: ColorManager.grey,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(5.0),
                    child: const Icon(Icons.image)),
              ),
            const SizedBox(
              width: 5,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    product!.title,
                    style: getBoldTextStyle(
                      color: ColorManager.primary,
                      fontSize: 14,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),  const SizedBox(height: 5,),
                   Text(
                        "${widget.productOdered.price.toString()} ${AppStrings.moneyDevise}",
                        style: getBoldTextStyle(
                          color: ColorManager.black,
                          fontSize: 14,
                        ),
                      ),
                    
                  if (product!.code != codePrestation.toString())
                    Text(
                      "Qte: ${widget.productOdered.quantity} ${product!.unite}",
                      style: getRegularTextStyle(
                        color: ColorManager.black,
                        fontSize: 12,
                      ),
                    ),  const SizedBox(height: 5,),
                    if(acheteur!=null)...[Text("${acheteur!.firstname} ${acheteur!.laststname}"),
                    Text("Acheteur", style: getRegularTextStyle(
                          color: ColorManager.grey,
                          fontSize: 10,
                        ),),
                       const SizedBox(height: 5,),
                    
                    ],
                    if(vendeur!=null)...[Text("${vendeur!.firstname} ${vendeur!.laststname}"),
                    Text("Vendeur", style: getRegularTextStyle(
                          color: ColorManager.grey,
                          fontSize: 10,
                        ),),
                       const SizedBox(height: 5,),
                    
                    ],
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        convertDateWithTime(widget.productOdered.createdAt.toString()),
                        style: getRegularTextStyle(
                          color: ColorManager.grey,
                          fontSize: 12,
                        ),
                      ),
                      
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    ):const Text("Ce produit n'est plius disponible");
  }

   Shimmer shimmerCustomOdered() {
    return Shimmer.fromColors(
      baseColor: const Color.fromARGB(255, 115, 115, 115),
      highlightColor: const Color.fromARGB(255, 181, 181, 181),
      child: Container(
        margin: const EdgeInsets.only(top: 10),
        height: 60,
        child: Row(
          children: [
            Container(
              height: 60,
              margin: const EdgeInsets.only(bottom: 10, left: 10),
              width: 40,
              decoration: BoxDecoration(
                color: ColorManager.white,
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  color: ColorManager.white,
                  height: 10,
                  width: 150,
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  color: ColorManager.white,
                  height: 10,
                  width: 200,
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  color: ColorManager.white,
                  height: 10,
                  width: 100,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

}