import 'dart:developer';

import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/app/functions.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/default_button.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../app/di.dart';
import '../../../data/repository/api/repository_impl.dart';
import '../../domain/model/Product.dart';
import '../../domain/model/odered_product_credit_model.dart';
import '../common/multi_image_widget.dart';
import '../common/state/loader.dart';
import '../paiement/paiement_screen.dart';
import '../ressources/styles_manager.dart';
import 'details_description.dart';

class DetailsDemandeCredit extends StatefulWidget {
  const DetailsDemandeCredit({Key? key, required this.productOdered,required this.product,this.isReceive=true}) : super(key: key);

 final OderedProductCredit productOdered;
final  ProductModel product;
final bool isReceive ;

  @override
  State<DetailsDemandeCredit> createState() => _DetailsDemandeCreditState();
}

class _DetailsDemandeCreditState extends State<DetailsDemandeCredit> {
  TextEditingController codeController = TextEditingController();

  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
 
  

  @override
  void initState() {
  
   // getDetailsProduct();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
    
        bottomNavigationBar: Container(
                    margin: const EdgeInsets.all(10),
                    child: DefaultButton(text: 'Voir les échéances',press:() {
                      Get.bottomSheet(
                       isScrollControlled: false,
                        Container(   padding: const EdgeInsets.all(10),
                        margin: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
                        decoration: BoxDecoration(
                        color: ColorManager.lightGrey, borderRadius: BorderRadius.circular(10)), 
                         child: Column(children: [
                          Text(
                                "Les échéances de paiement",
                                style: getSemiBoldTextStyle(color: Colors.black, fontSize: 15),
                              ),
                              Expanded(
                                child: ListView.builder(
                                 // physics: const NeverScrollableScrollPhysics(),
                                  padding: const EdgeInsets.only(bottom: 10),
                                  shrinkWrap: true,
                                  itemCount: widget.productOdered.paymentDates.length,
                                  itemBuilder: (context, index) {
                                    // log(index.toString());
                                    return Container(
                                      margin:const EdgeInsets.all(10),
                                      padding:const EdgeInsets.all(10),
                                      color: ColorManager.white,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(convertDate( widget.productOdered.paymentDates[index].date.toString()),
                                            style: getBoldTextStyle(color: ColorManager.primary),
                                       
                                            ), Text("${widget.productOdered.paymentDates[index].amount.toString()} FCFA",
                                            style: getBoldTextStyle(),
                                       
                                            ),
                                          ],
                                        ), 
                                        if(widget.productOdered.paymentDates[index].isOk==false)...[
                                         if(widget.isReceive==false)

                                        SizedBox(
                                        width:90,
                                        
                                        height: 40,
                                        child: TextButton(
                                          style: TextButton.styleFrom(
                                            foregroundColor: Colors.white,
                                            shape:
                                                RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                            backgroundColor:
                                                ColorManager.primary.withOpacity(0.5)
                                          ),
                                          onPressed: (){numberInput(widget.productOdered.paymentDates[index].id.toString());},
                                          child: Text(
                                            "Payer",
                                            style: getRegularTextStyle(fontSize: 12,color: ColorManager.white)
                                          ),
                                        ),
                                      ),
                                      if(widget.isReceive==true)
                                      Text(
                                            "Impayé",
                                            style: getRegularTextStyle(fontSize: 12,color: ColorManager.red)
                                          ),
                                      ]else...[Text(
                                            "Déjà payé",
                                            style: getRegularTextStyle(fontSize: 12,color: ColorManager.primary)
                                          ),],
                                       
                                      ],
                                    ));
                                  },
                                ),
                              ),
                         ],),),
                    );}) ),
        backgroundColor: ColorManager.backgroundColor,
        appBar: AppBar(leading:const BackButtonCustom(),title:const Text( "Details"),),
        body: Container(
          margin:const EdgeInsets.only(top: 10),
          child: ListView(
            shrinkWrap: true,
            children: [
              MultiImageWidget(
                images: widget.product.images!,
              ),
              DetailsDemandesCredit(
                productOdered: widget. productOdered,
                product:widget.product ,
                
        
              )
            ],
          ),
        ));
                
           
  }

  confirmPaeiment() {
    Get.bottomSheet(
      isScrollControlled: false,
      Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10)),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Entrer le code de confirmation",
                style: getBoldTextStyle(color: Colors.black, fontSize: 20),
              ),
              const SizedBox(
                height: 10,
              ),
              TextField(
                controller: codeController,
                maxLines: null,
                decoration: const InputDecoration(
                  hintText: "Entrer le code ",
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 100,
                    child: DefaultButton(
                      color: ColorManager.grey,
                      text: "Annuler",
                      press: () {
                        Get.back();
                      },
                    ),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  SizedBox(
                    width: 100,
                    child: DefaultButton(
                      text: "Envoyer",
                      press: () async {
                        // Get.back();
                        // if (codeController.text == "") {
                        //   showCustomFlushbar(context, "Entrer le code",
                        //       ColorManager.error, FlushbarPosition.TOP);
                        // } else {
                        //   bool result =
                        //       await paiementController.confirmPaiementCash(
                        //     demandeShow.id,
                        //     codeController.text,
                        //   );
                        //   if (result == true) {
                        //     demandeShow.isConfirmed = true;
                        //   }
                        //   setState(() {
                        //     demandeShow;
                        //   });
                        // }
                      },
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }

createPaiement(String phone,String reference)async{
   loaderDialog(context,
                                            text: "Veuillez patienter");
await repositoryImpl.createPaiementCredit(reference,widget. productOdered.orderId,"PAYMENT_CREDIT",phone).then((response) {
 log(reference);
  response.fold((failure) {
    Navigator.pop(context);

    showCustomFlushbar(context, failure.message, ColorManager.error, FlushbarPosition.TOP);
    inspect(failure);}, (data) {
    Navigator.pop(context);

        Get.to(PaiementScreen(
              reseau:determinateReseau(
                           phone),
              createPaiementResponse: data,
            ));
});});
}
TextEditingController numberController=TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

numberInput(String reference) {
    Get.bottomSheet(
      Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Entrer votre numéro de téléphone",
              style: getBoldTextStyle(color: ColorManager.black, fontSize: 14),
            ),
            const SizedBox(
              height: 10,
            ),
            Form(
              key: _formKey,
              child: TextFormField(
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Entrer votre numero';
                  } else if (valideNumber(
                        value,
                      ) ==
                      false) {
                    return 'Numero invalide';
                  } else {
                    return null;
                  }
                },
                controller: numberController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.grey.withOpacity(0.2),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide:
                        const BorderSide(width: 0, style: BorderStyle.none),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide:
                        const BorderSide(width: 0, style: BorderStyle.none),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide:
                        const BorderSide(width: 0, style: BorderStyle.none),
                  ),
                  hintText: "Entrer votre numéro",
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: 150,
              child: DefaultButton(
                text: "Suivant >",
                press: () async {
                  if (_formKey.currentState!.validate()) {
                    Get.back();
                     Get.back();

                    createPaiement(numberController.text,reference);
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
