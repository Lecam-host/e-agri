import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/ressources/assets_manager.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../ressources/size_config.dart';
import '../controller/cart_controller.dart';
import 'cart_card.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  BodyState createState() => BodyState();
}

class BodyState extends State<Body> {
  CartController cartController = Get.put(CartController());
  @override
  Widget build(BuildContext context) {
    return Obx(() => cartController.cartItems.isEmpty
        ? EmptyComponenent(
            message: "Panier vide",
            icon: JsonAssets.cartEmpty,
          )
        : Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(0)),
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: cartController.cartItems.length,
              itemBuilder: (context, index) => Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  color: ColorManager.white,
                  child: CartCard(
                    cartItem: cartController.cartItems[index],
                    index: index,
                  ),
                ),
              ),
            ),
          ));
  }
}
