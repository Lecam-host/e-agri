import 'package:eagri/presentations/command/controller/command_controller.dart';
import 'package:eagri/presentations/ressources/size_config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../common/default_button.dart';
import '../../ressources/strings_manager.dart';
import '../controller/cart_controller.dart';

class CheckoutCard extends StatefulWidget {
  const CheckoutCard({
    Key? key,
  }) : super(key: key);

  @override
  State<CheckoutCard> createState() => _CheckoutCardState();
}

class _CheckoutCardState extends State<CheckoutCard> {
  CartController cartController = Get.put(CartController());
  CommandCrontroller commandCrontroller = Get.put(CommandCrontroller());
  @override
  Widget build(BuildContext context) {
    return Obx(() => Container(
          padding: EdgeInsets.symmetric(
            vertical: getProportionateScreenWidth(15),
            horizontal: getProportionateScreenWidth(30),
          ),
          // height: 174,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
            ),
            boxShadow: [
              BoxShadow(
                offset: const Offset(0, -15),
                blurRadius: 20,
                color: const Color(0xFFDADADA).withOpacity(0.15),
              )
            ],
          ),
          child: SafeArea(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Row(
                //   children: [
                //     Container(
                //       padding: const EdgeInsets.all(10),
                //       height: getProportionateScreenWidth(40),
                //       width: getProportionateScreenWidth(40),
                //       decoration: BoxDecoration(
                //         color: const Color(0xFFF5F6F9),
                //         borderRadius: BorderRadius.circular(10),
                //       ),
                //       child: SvgPicture.asset("assets/icons/receipt.svg"),
                //     ),
                //     const Spacer(),
                //     const Text("Ajouter un code promo"),
                //     const SizedBox(width: 10),
                //     const Icon(
                //       Icons.arrow_forward_ios,
                //       size: 12,
                //       color: kTextColor,
                //     )
                //   ],
                // ),
                // SizedBox(height: getProportionateScreenHeight(20)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text.rich(
                      TextSpan(
                        text: "Total:\n",
                        children: [
                          TextSpan(
                            text:
                                "${cartController.totalPrice} ${AppStrings.moneyUnit}",
                            style: const TextStyle(
                                fontSize: 16, color: Colors.black),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: getProportionateScreenWidth(190),
                      child: DefaultButton(
                        text: "Commander",
                        press: () {
                          if (cartController.listCart.value.data.isNotEmpty) {
                            commandCrontroller.createCommand(
                                cartController.listCart.value.data[0].id);
                          }
                          // Get.to(const CommandScreen());
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ));
  }
}
