import 'package:another_flushbar/flushbar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/data/repository/api/repository_impl.dart';
import 'package:eagri/presentations/cart/controller/cart_controller.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

import '../../../app/constant.dart';
import '../../../app/di.dart';
import '../../../app/functions.dart';
import '../../../domain/model/Product.dart';
import '../../../domain/model/cart_model.dart';
import '../../../domain/model/user_model.dart';
import '../../../domain/usecase/shop_usecase.dart';
import '../../common/default_button.dart';
import '../../common/flushAlert_componenent.dart';
import '../../ressources/assets_manager.dart';
import '../../ressources/strings_manager.dart';

class CartCard extends StatefulWidget {
  const CartCard({
    Key? key,
    required this.cartItem,
    required this.index,
    this.isEdit = true,
  }) : super(key: key);

  final CartItem cartItem;
  final int index;
  final bool isEdit;

  @override
  State<CartCard> createState() => _CartCardState();
}

class _CartCardState extends State<CartCard> {
  bool isLoadChangeQuatity = false;
  bool isLoadRemove = false;
  bool isLoadInfo = true;
  ShopUseCase shopUseCase = instance<ShopUseCase>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  CartController cartController = Get.find();
  late CartItem cartItemShow;
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  getDetailsProduct() async {
    //detail du produit dans un panier

    await repositoryImpl.getDetailProduct(cartItemShow.idProduct).then(
          (value) => value.fold((l) {
            if (mounted) {
              setState(() {
                isLoadInfo = false;
              });
            }
          }, (product) async {
            cartItemShow.product = (await product)!;
            if (mounted) {
              setState(() {
                cartItemShow;
                isLoadInfo = false;
              });
            }
          }),
        );
  }

  @override
  void initState() {
    cartItemShow = widget.cartItem;
    getDetailsProduct();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
        builder: (context, user, child) => GetBuilder<CartController>(
              init: CartController(),
              builder: (value) => Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  cartItemShow.product == null && isLoadInfo == false
                      ? const Center(
                          child: Text("Ce produit n'est plus disponible"))
                      : Row(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            SizedBox(
                              width: 60,
                              child: AspectRatio(
                                aspectRatio: 0.88,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: const Color(0xFFF5F6F9),
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: isLoadInfo == false
                                      ? CachedNetworkImage(
                                          imageUrl:
                                              cartItemShow.product!.images![0],
                                          imageBuilder:
                                              (context, imageProvider) =>
                                                  Container(
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(15),
                                              image: DecorationImage(
                                                scale: 1,
                                                image: imageProvider,
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                          placeholder: (context, url) =>
                                              SizedBox(
                                            height: MediaQuery.of(context)
                                                .size
                                                .height,
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                          ),
                                          errorWidget: (context, url, error) =>
                                              const Icon(Icons.error),
                                        )
                                      : Container(
                                          decoration: BoxDecoration(
                                            color: ColorManager.grey,
                                            borderRadius:
                                                BorderRadius.circular(15),
                                          ),
                                        ),
                                ),
                              ),
                            ),
                            const SizedBox(width: 20),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    if (isLoadInfo == false)
                                      Text(
                                        cartItemShow.product!.title,
                                        style: const TextStyle(
                                            color: Colors.black, fontSize: 14),
                                        maxLines: 2,
                                      ),
                                    if (isLoadInfo == true)
                                      Shimmer.fromColors(
                                        baseColor: const Color.fromARGB(
                                            255, 115, 115, 115),
                                        highlightColor: const Color.fromARGB(
                                            255, 181, 181, 181),
                                        child: Container(
                                          margin: const EdgeInsets.only(
                                              bottom: 10, left: 10),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              1.5,
                                          height: 25.0,
                                          decoration: BoxDecoration(
                                            color: ColorManager.white,
                                            borderRadius:
                                                BorderRadius.circular(15),
                                          ),
                                        ),
                                      ), // TextButton(
                                    //     onPressed: () {
                                    //       // Get.to(DetailsProduitScreen(product:  cartItemShow.product,));
                                    //       Get.toNamed(
                                    //         Routes.detailsProduct,
                                    //         arguments: sendProdDetailArgument(
                                    //             cartItemShow.product!),
                                    //       );
                                    //     },
                                    //     child: Text(
                                    //       'Détails',
                                    //       style: getBoldTextStyle(
                                    //         color: ColorManager.blue,
                                    //         fontSize: 10,
                                    //       ),
                                    //     )),
                                  ],
                                ),
                                if (widget.isEdit == false)
                                  Text.rich(
                                    TextSpan(
                                      text: "${cartItemShow.quantity} ",
                                      style: getSemiBoldTextStyle(
                                        color: ColorManager.black,
                                      ),
                                      children: [
                                        TextSpan(
                                          text: cartItemShow.product!.unite,
                                          style: getSemiBoldTextStyle(
                                              color: ColorManager.black),
                                        ),
                                      ],
                                    ),
                                  ),
                                if (widget.isEdit == false)
                                  Text(
                                    "${cartItemShow.quantity * cartItemShow.price} FCFA",
                                    style: getBoldTextStyle(
                                      color: ColorManager.black,
                                      fontSize: 17,
                                    ),
                                  ),
                                const SizedBox(height: 5),
                                if (widget.isEdit == true)
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Text.rich(
                                        TextSpan(
                                          text:
                                              "${cartItemShow.price} ${AppStrings.moneyUnit}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              color: kPrimaryColor),
                                          children: [
                                            TextSpan(
                                                text:
                                                    " x${cartItemShow.quantity}",
                                                style: getBoldTextStyle()),
                                          ],
                                        ),
                                      ),
                                      Text(
                                          '  =${cartItemShow.price * cartItemShow.quantity}  ${AppStrings.moneyUnit}')
                                    ],
                                  )
                              ],
                            ),
                          ],
                        ),
                  if (widget.isEdit == true)
                    Row(
                      children: [
                        Visibility(
                          visible: isLoadRemove == false,
                          replacement: SizedBox(
                            height: 40,
                            width: 40,
                            child: Lottie.asset(JsonAssets.loader),
                          ),
                          child: IconButton(
                            onPressed: () {
                              Get.defaultDialog(
                                  title: "Supprimer",
                                  titleStyle: getRegularTextStyle(fontSize: 20),
                                  content: const Text(
                                    "voulez-Vous vraiment supprimer ce produit ?",
                                  ),
                                  onCancel: () {
                                    //  Get.back();
                                  },
                                  textCancel: "Annuler",
                                  textConfirm: "Supprimer",
                                  buttonColor: ColorManager.primary,
                                  cancelTextColor: Colors.black,
                                  confirmTextColor: ColorManager.white,
                                  // confirmTextColor: Colors.black,
                                  onConfirm: () async {
                                    setState(() {
                                      isLoadRemove = true;
                                    });
                                    Get.back();
                                    if (mounted) {
                                      await value
                                          .removeCart(
                                              cartItemShow,
                                              cartController.cartItems
                                                  .indexWhere((e) =>
                                                      e.id == cartItemShow.id))
                                          .then((value) {
                                        setState(() {
                                          isLoadRemove = false;
                                        });
                                      });
                                    }
                                  });
                            },
                            icon: const Icon(Icons.delete),
                          ),
                        ),
                        const Spacer(),
                        if (isLoadInfo == false) ...[
                          if (cartItemShow.product != null) ...[
                            if (cartItemShow.product!.typeVente != "BLOC") ...[
                              CircleAvatar(
                                backgroundColor: ColorManager.blue,
                                maxRadius: 15,
                                child: Center(
                                    child: IconButton(
                                        onPressed: () async {
                                          quantityInput(cartItemShow.product!);
                                        },
                                        icon: Icon(
                                          Icons.edit,
                                          size: 15,
                                          color: ColorManager.white,
                                        ))),
                              ),
                              const SizedBox(
                                  height: 30,
                                  width: 15,
                                  child: VerticalDivider(color: Colors.black)),
                              CircleAvatar(
                                backgroundColor: cartItemShow.quantity == 1 ||
                                        isLoadChangeQuatity == true
                                    ? ColorManager.grey
                                    : ColorManager.primary,
                                maxRadius: 15,
                                child: Center(
                                    child: IconButton(
                                        onPressed: () async {
                                          if (isLoadChangeQuatity == false) {
                                            setState(() {
                                              isLoadChangeQuatity = true;
                                            });
                                            await value
                                                .decreasqty(cartItemShow)
                                                .then((value) {
                                              setState(() {
                                                isLoadChangeQuatity = false;
                                              });
                                            });
                                          }
                                        },
                                        icon: Icon(
                                          Icons.remove,
                                          size: 15,
                                          color: ColorManager.white,
                                        ))),
                              ),
                            ],
                            const SizedBox(width: 10),
                            if (cartItemShow.product!.typeVente == "BLOC")
                              Text("Vendu en bloc",
                                  style: getBoldTextStyle(
                                      color: ColorManager.red)),
                            if (cartItemShow.product!.typeVente != "BLOC")
                              Visibility(
                                visible: isLoadChangeQuatity == false,
                                replacement: SizedBox(
                                  height: 15,
                                  width: 15,
                                  child: Lottie.asset(JsonAssets.loader),
                                ),
                                child: Text(" x${cartItemShow.quantity}",
                                    style:
                                        Theme.of(context).textTheme.bodyLarge),
                              ),
                            const SizedBox(width: 10),
                            if (cartItemShow.product!.typeVente != "BLOC")
                              CircleAvatar(
                                backgroundColor: cartItemShow.quantity >=
                                            cartItemShow.product!.quantity ||
                                        isLoadChangeQuatity == true
                                    ? ColorManager.grey
                                    : ColorManager.primary,
                                maxRadius: 15,
                                child: Center(
                                  child: IconButton(
                                    onPressed: () async {
                                      if (isLoadChangeQuatity == false) {
                                        setState(() {
                                          isLoadChangeQuatity = true;
                                        });
                                        await value
                                            .increasQty(cartItemShow)
                                            .then((value) {
                                          setState(() {
                                            isLoadChangeQuatity = false;
                                          });
                                        });
                                      }
                                    },
                                    icon: Icon(
                                      Icons.add,
                                      size: 15,
                                      color: ColorManager.white,
                                    ),
                                  ),
                                ),
                              ),
                          ],
                        ] else ...[
                          Shimmer.fromColors(
                            baseColor: const Color.fromARGB(255, 115, 115, 115),
                            highlightColor:
                                const Color.fromARGB(255, 181, 181, 181),
                            child: Container(
                              margin:
                                  const EdgeInsets.only(bottom: 10, left: 10),
                              width: MediaQuery.of(context).size.width / 1.5,
                              height: 40.0,
                              decoration: BoxDecoration(
                                color: ColorManager.white,
                                borderRadius: BorderRadius.circular(15),
                              ),
                            ),
                          ),
                        ],
                      ],
                    ),
                  // specify type as Controller
                ],
              ),
            ));
  }

  quantityInput(ProductModel product) {
    bool isLoadEditQuantity = false;
    TextEditingController newQuantityController = TextEditingController();

    Get.bottomSheet(
      StatefulBuilder(builder: (cxt, setStateBottomSheet) {
        return Container(
          padding: const EdgeInsets.all(10),
          margin: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(10)),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Modifier la quantité",
                style:
                    getBoldTextStyle(color: ColorManager.black, fontSize: 14),
              ),
              const SizedBox(
                height: 10,
              ),
              Form(
                key: _formKey,
                child: TextFormField(
                  // initialValue:cartItemShow.quantity.toString() ,
                  controller: newQuantityController,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Entrer la quantité';
                    } else if (validQuantity(
                          int.parse(value),
                        ) ==
                        false) {
                      return 'Quantité invalide';
                    } else if (int.parse(value) > product.quantity) {
                      return "Quantité indisponible";
                    } else {
                      return null;
                    }
                  },
                  onChanged: (value) {
                    if (_formKey.currentState!.validate()) {
                      setStateBottomSheet(() {
                        newQuantityController.text = value;
                        cartItemShow.quantity = int.parse(value);
                      });
                    }
                  },
                  // controller: quantityCommand.value,
                  keyboardType: TextInputType.number,

                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.grey.withOpacity(0.2),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15.0),
                      borderSide:
                          const BorderSide(width: 0, style: BorderStyle.none),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15.0),
                      borderSide:
                          const BorderSide(width: 0, style: BorderStyle.none),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15.0),
                      borderSide:
                          const BorderSide(width: 0, style: BorderStyle.none),
                    ),
                    hintText: 'Entrer la quantité',
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RichText(
                    text: TextSpan(
                      text: 'Quantité demandée: ',
                      style: getRegularTextStyle(color: ColorManager.black),
                      children: [
                        TextSpan(
                          text: "${cartItemShow.quantity} ${product.unite}",
                          style: getMeduimTextStyle(
                            color: ColorManager.jaune,
                            fontSize: 16,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  RichText(
                    text: TextSpan(
                      text: 'Prix total : ',
                      style: getRegularTextStyle(color: ColorManager.black),
                      children: [
                        TextSpan(
                          text: " ${separateur((int.parse(
                                cartItemShow.quantity.toString(),
                              ) * product.price).toDouble())} FCFA",
                          style: getMeduimTextStyle(
                            color: ColorManager.jaune,
                            fontSize: 16,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              RichText(
                text: TextSpan(
                  text: 'Quantité disponible: ',
                  style: getRegularTextStyle(color: ColorManager.black),
                  children: [
                    TextSpan(
                      text: "${product.quantity} ${product.unite}",
                      style: getMeduimTextStyle(
                        color: ColorManager.jaune,
                        fontSize: 16,
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              if (isLoadEditQuantity == true)
                const SizedBox(
                    width: 150, child: DefaultButton(text: "Patientez ...")),
              if (isLoadEditQuantity == false)
                SizedBox(
                  width: 150,
                  child: DefaultButton(
                    text: "Valider",
                    press: () async {
                      if (newQuantityController.text.isNotEmpty &&
                          cartItemShow.quantity <=
                              cartItemShow.product!.quantity) {
                        setStateBottomSheet(() {
                          isLoadEditQuantity = true;
                        });
                        await shopUseCase
                            .addProductShoppingCart(cartController
                                .requestAddProductInShoppingCartsRequest(
                                    cartItemShow))
                            .then((response) => response.fold((failure) {
                                  setStateBottomSheet(() {
                                    isLoadEditQuantity = false;
                                  });
                                  showCustomFlushbar(
                                      Get.context!,
                                      "Echec",
                                      ColorManager.error,
                                      FlushbarPosition.BOTTOM);
                                }, (data) {
                                  cartController.getTotalsMount();
                                  setStateBottomSheet(() {
                                    isLoadEditQuantity = false;
                                  });
                                  Get.back();
                                  int index = cartController.cartItems
                                      .indexWhere(
                                          (e) => e.id == cartItemShow.id);
                                  cartController.cartItems[index] =
                                      cartItemShow;

                                  showCustomFlushbar(
                                      Get.context!,
                                      "Quantité modifiée",
                                      ColorManager.succes,
                                      FlushbarPosition.TOP);
                                }));
                      }
                    },
                  ),
                ),
            ],
          ),
        );
      }),
      isScrollControlled: true,
    );
  }
}
