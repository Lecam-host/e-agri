import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/app/app_prefs.dart';
import 'package:eagri/app/di.dart';
import 'package:eagri/domain/usecase/shop_usecase.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../data/request/request_object.dart';
import '../../../data/request/shop_resquests.dart';
import '../../../domain/model/Product.dart';
import '../../../domain/model/cart_model.dart';
import '../../../domain/model/user_model.dart';

class CartController extends GetxController {
  ShopUseCase shopUseCase = instance<ShopUseCase>();
  AppSharedPreference appSharedPreference = instance<AppSharedPreference>();
  var isLoadCart = true.obs;
  var userInfo = UserModel().obs;
  var quantityCommand = TextEditingController().obs;

  var totalamount = 0.0.obs;
  var frais = 0.obs;
  double fraisPercent = 2;
  var cartItems = <CartItem>[].obs;
  int get count => cartItems.length;
  var listCart = ListCartResponse(data: []).obs;
  var totalPrice = 0.obs;

  // addProductIncartOnLigne(AddProductInShoppingCartsRequest request) a{

  // }

  getFrais() {
    frais.value = (totalPrice * fraisPercent) ~/ 100;
  }

  init() async {
    listCart.value.data = [];
    await getUserInfo();
    await getUserCart();
  }

  @override
  onInit() {
    init();
    super.onInit();
  }

  getUserCart() async {
    await getUserInfo();
    isLoadCart.value = true;
    await shopUseCase
        .getUserCart(userInfo.value.id!)
        .then((response) => response.fold((l) {}, (userCart) async {
              cartItems.value = [];
              listCart.value.data = [];

              await Future.forEach(userCart.data, (Cart cart) async {
                if (cart.status == "PENDIG") {
                  listCart.value.data.add(cart);
                  cartItems.addAll(cart.cartItems);
                }
              });
            }));
    getTotalsMount();
    isLoadCart.value = false;
  }

  getUserInfo() async {
    await appSharedPreference
        .getUserInformation()
        .then((value) => userInfo.value = value);
  }

  Future<CartItem> addToCart(ProductModel product, int quantityCommand) async {
    CartItem cartItem = CartItem(
      id: product.id,
      product: product,
      fournisseurId: product.fournisseurId,
      idProduct: product.id,
      price: product.price,
      quantity:
          product.typeVente == "BLOC" ? product.quantity : quantityCommand,
    );

    if (isAlredyAdded(product)) {
      Get.snackbar("", "Ce produit existe dans le panier",
          backgroundColor: ColorManager.primary,
          duration: const Duration(seconds: 1));
      // Get.snackbar(
      //   "",
      //   "",
      //   backgroundColor: ColorManager.primary,
      //   duration: const Duration(seconds: 1),
      //   titleText: Row(
      //     children: [
      //       const Text(
      //         "Ajouter au panier",
      //       ),
      //     ],
      //   ),
      // );
    } else {
      await getUserInfo();
      await shopUseCase
          .addProductShoppingCart(
              requestAddProductInShoppingCartsRequest(cartItem))
          .then((response) {
        response.fold((l) {
          showCustomFlushbar(Get.context!, l.message, ColorManager.error,
              FlushbarPosition.BOTTOM);
        }, (data) async {
          //cartItem.id=data
          cartItems.add(cartItem);
          getTotalsMount();
          update();
          showCustomFlushbar(Get.context!, "Ajouter au panier",
              ColorManager.primary, FlushbarPosition.BOTTOM);
        });
      });

      // Get.snackbar("", "Ajouter au panier",
      //     backgroundColor: ColorManager.primary,
      //     duration: const Duration(seconds: 1));
      // Get.snackbar(
      //   "Ajouté",
      //   "",
      //   backgroundColor: ColorManager.primary,
      //   duration: const Duration(seconds: 1),
      //   snackPosition: SnackPosition.TOP,
      //   margin: EdgeInsets.only(left: getProportionateScreenWidth(150)),
      // );
    }
    return cartItem;
  }

  AddProductInShoppingCartsRequest requestAddProductInShoppingCartsRequest(
      CartItem cartItem) {
    return AddProductInShoppingCartsRequest(
      userId: userInfo.value.id!,
      items: [
        AddProductShoppingCartsItem(
            speculationId: cartItem.product!.speculation.speculationId!,
            price: cartItem.product!.price.toDouble(),
            productId: cartItem.product!.id,
            quantity: cartItem.product!.typeVente == "BLOC"
                ? cartItem.product!.quantity
                : cartItem.quantity,
            fournisseurId: cartItem.product!.fournisseurId,
            productCode: cartItem.product!.code,
            paymentCode: cartItem.product!.paymentMethod!.code!)
      ],
    );
  }

  Future<CartItem> editProductQuantityInCart(
      CartItem cartItem, bool isDecreasqty) async {
    CartItem newCartItem = cartItem;
    if (isDecreasqty == false) {
      newCartItem.quantity = newCartItem.quantity + 1;
    } else {
      newCartItem.quantity = newCartItem.quantity - 1;
    }
    await getUserInfo();
    await shopUseCase
        .addProductShoppingCart(
            requestAddProductInShoppingCartsRequest(newCartItem))
        .then((response) async {
      await response.fold((l) async {
        if (isDecreasqty == true) {
          newCartItem.quantity = newCartItem.quantity + 1;
        } else {
          newCartItem.quantity = newCartItem.quantity - 1;
        }
        int index = cartItems.indexWhere((e) => e.id == cartItem.id);
        cartItems[index] = newCartItem;
        showCustomFlushbar(
            Get.context!, "Echec", ColorManager.error, FlushbarPosition.BOTTOM);
      }, (data) async {
        newCartItem = getSpecificCartItem(cartItem.product!);
        int index = cartItems.indexWhere((e) => e.id == cartItem.id);
        cartItems[index] = newCartItem;
        // cartItems.add(cartItem);

        // showCustomFlushbar(Get.context!, "Ajouter au panier",
        //     ColorManager.primary, FlushbarPosition.BOTTOM);
      });
    });
    getTotalsMount();
    update();
    return newCartItem;
  }

  bool isAlredyAdded(ProductModel product) =>
      cartItems.where((item) => item.idProduct == product.id).isNotEmpty;
  CartItem getSpecificCartItem(ProductModel product) {
    return cartItems.firstWhere((element) => product.id == element.idProduct);
  }

  Future<CartItem> decreasqty(
    CartItem cartItem,
  ) async {
    CartItem newCartItem = cartItem;
    if (cartItem.quantity == 1) {
      // removeCart(cart);
      showCustomFlushbar(Get.context!, "Quantité min atteint",
          ColorManager.blue, FlushbarPosition.TOP);
    } else {
      // int index = cartItems.indexWhere((e) => e.id == cartItem.id);

      newCartItem = await editProductQuantityInCart(cartItem, true);

      getTotalsMount();
      update();
    }
    return newCartItem;
  }

  Future<CartItem> increasQty(CartItem cartItem) async {
    CartItem newCartItem = cartItem;
    if (cartItem.quantity >= 1 &&
        (cartItem.product!.quantity > cartItem.quantity)) {
      newCartItem = await editProductQuantityInCart(cartItem, false);
    } else {
      showCustomFlushbar(Get.context!, "Quantité max atteint",
          ColorManager.blue, FlushbarPosition.TOP);
    }
    return newCartItem;
  }

  Future<void> removeCart(CartItem cartItem, int cartItemIndex) async {
    await shopUseCase
        .deleteProdInCart(listCart.value.data[0].id, cartItem.id)
        .then((response) async {
      await response.fold((l) {
        showCustomFlushbar(Get.context!, "Echec de suppression",
            ColorManager.error, FlushbarPosition.TOP);
      }, (r) async {
        getUserCart();
        print(cartItemIndex);
        // cartItems.removeAt(cartItemIndex);

        // getTotalsMount();
        // update();
        showCustomFlushbar(Get.context!, "Produit supprimé",
            ColorManager.primary, FlushbarPosition.TOP);
      });
    });
  }

  void getTotalsMount() {
    totalPrice.value =
        cartItems.fold(0, (sum, item) => sum + item.price * item.quantity);

    getFrais();
    //  count2 = totalamount;
  }
}
