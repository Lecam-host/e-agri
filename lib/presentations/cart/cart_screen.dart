import 'package:eagri/presentations/cart/controller/cart_controller.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'components/body.dart';
import 'components/check_out_card.dart';

class CartScreen extends StatefulWidget {
  static String routeName = "/cart";

  const CartScreen({Key? key}) : super(key: key);

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  final cartController = Get.put(CartController());
  @override
  void initState() {
    cartController.init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: Obx(() => cartController.isLoadCart.value == true
          ? const LoaderComponent()
          : const Body()),
      bottomNavigationBar: Obx(
        () => cartController.isLoadCart.value == false
            ? cartController.cartItems.isNotEmpty
                ? const CheckoutCard()
                : const SizedBox()
            : const SizedBox(),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    CartController cartController = Get.put(CartController());
    return AppBar(
      leading: const BackButtonCustom(),
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            "Mon panier",
            style: TextStyle(color: Colors.black),
          ),
          GetBuilder<CartController>(
            init: CartController(),
            builder: (value) => value.cartItems.isNotEmpty
                ? Text(
                    "${cartController.cartItems.length} Produits",
                    style: Theme.of(context).textTheme.bodySmall,
                  )
                : const SizedBox(),
          )
        ],
      ),
    );
  }
}
