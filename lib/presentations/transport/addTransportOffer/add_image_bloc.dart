import 'package:eagri/presentations/common/addImage/add_image_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../ressources/styles_manager.dart';
import '../controller/add_transport_controller.dart';

class AddImageBloc extends StatefulWidget {
  const AddImageBloc({Key? key}) : super(key: key);

  @override
  State<AddImageBloc> createState() => _AddImageBlocState();
}

class _AddImageBlocState extends State<AddImageBloc> {
  AddTransportController addTransportController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Obx(() => SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: AddImageWidget(
                  imageReturn: addTransportController.files,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              const Text("Description *"),
              TextFormField(
                controller: addTransportController.descriptionController.value,
                decoration: InputDecoration(
                  hintText: "Entrer la Description",
                  label: Text(
                    "Description",
                    style: getRegularTextStyle(color: Colors.grey),
                  ),
                ),
                keyboardType: TextInputType.multiline,
                maxLines: null,
              ),
            ],
          ),
        ));
  }
}
