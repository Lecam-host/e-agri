import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:select_form_field/select_form_field.dart';

import '../controller/add_transport_controller.dart';

class AddCarPrice extends StatefulWidget {
  const AddCarPrice({Key? key}) : super(key: key);

  @override
  State<AddCarPrice> createState() => _AddCarPriceState();
}

class _AddCarPriceState extends State<AddCarPrice> {
  AddTransportController addTransportController = Get.find();
  final _formKeyPrinceInfo = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKeyPrinceInfo,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 20,
              ),
              const Text('Prix *'),
              const SizedBox(
                height: 10,
              ),
              TextFormField(
                validator: (value) => value!.isEmpty ? 'Obligatoire' : null,
                controller: addTransportController.priceController.value,
                keyboardType: TextInputType.number,
                decoration: const InputDecoration(
                  hintText: "Entrer le prix",
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              const Text('Unité *'),
              const SizedBox(
                height: 10,
              ),
              SelectFormField(
                type: SelectFormFieldType.dialog,
                enableSearch: true, // or can be dialog
                //initialValue: 'circle',

                labelText: "Choisir l'unité",
                items: addTransportController.listUnitMap,
                onChanged: (val) {
                  addTransportController.uniteController.value = val;
                },
              ),
              const SizedBox(
                height: 20,
              ),
              const Text('Moyen de payement *'),
              const SizedBox(
                height: 10,
              ),
              SelectFormField(
                type: SelectFormFieldType.dialog,
                enableSearch: true, // or can be dialog
                //initialValue: 'circle',

                labelText: "Choisir le moyen de payement",
                items: addTransportController.listMethodPaiement,
                onChanged: (val) {
                  addTransportController.moyenPaiementController.value =
                      int.parse(val);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
