import 'package:eagri/presentations/common/location_select/location_select_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controller/add_transport_controller.dart';

class AddLocationBloc extends StatefulWidget {
  const AddLocationBloc({Key? key}) : super(key: key);

  @override
  State<AddLocationBloc> createState() => _AddLocationBlocState();
}

class _AddLocationBlocState extends State<AddLocationBloc> {
  AddTransportController addTransportController = Get.find();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text("Lieu de depart"),
          const SizedBox(
            height: 10,
          ),
          LocationSelectWidget(
            locationReturn: addTransportController.locationDepartReturn.value,
          ),
          const SizedBox(
            height: 20,
          ),
          const Text("Lieu de d'arrivée"),
          const SizedBox(
            height: 10,
          ),
          LocationSelectWidget(
            locationReturn: addTransportController.locationFinReturn.value,
          ),
        ],
      ),
    );
  }
}
