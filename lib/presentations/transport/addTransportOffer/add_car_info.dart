import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../common/disponibilite_widget.dart';
import '../../ressources/styles_manager.dart';
import '../controller/add_transport_controller.dart';

class AddCarInfoBloc extends StatefulWidget {
  const AddCarInfoBloc({Key? key}) : super(key: key);

  @override
  State<AddCarInfoBloc> createState() => _AddCarInfoBlocState();
}

class _AddCarInfoBlocState extends State<AddCarInfoBloc> {
  AddTransportController addTransportController = Get.find();
  final _formKeyCarInfo = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.all(10),
        child: Form(
          key: _formKeyCarInfo,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text('Matricule *'),
              const SizedBox(
                height: 10,
              ),
              TextFormField(
                validator: (value) => value!.isEmpty ? 'Obligatoire' : null,
                controller: addTransportController.matriculeController.value,
                decoration: InputDecoration(
                  hintText: "Entrer la matricule",
                  label: Text(
                    "Matricule",
                    style: getRegularTextStyle(color: Colors.grey),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              const Text('Capacité *'),
              const SizedBox(
                height: 10,
              ),
              TextFormField(
                validator: (value) => value!.isEmpty ? 'Obligatoire' : null,
                controller: addTransportController.capaciteController.value,
                decoration: const InputDecoration(
                  hintText: "Entrer la capacité",
                ),
                keyboardType: TextInputType.number,
              ),
              Text(
                "Cette quantité ne doit pas dépasser ${addTransportController.carCapacityMax} ${addTransportController.carCapacityMaxUnite}",
                style: getRegularTextStyle(color: ColorManager.jaune),
              ),
              const SizedBox(
                height: 20,
              ),
              DisponibiliteWidget(
                titre: "Le vehicule est disponible actuellement ?",
                dateDisponibilite:
                    addTransportController.dateDeDisponibilite.value,
                disponibilite: addTransportController.disponibilite.value,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
