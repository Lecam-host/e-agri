import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:eagri/presentations/transport/controller/add_transport_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

import '../../common/default_button.dart';

class AddTransportScreen extends StatefulWidget {
  const AddTransportScreen({super.key});

  @override
  State<AddTransportScreen> createState() => _AddTransportScreenState();
}

class _AddTransportScreenState extends State<AddTransportScreen> {
  AddTransportController addTransportController =
      Get.put(AddTransportController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: Container(),
          title: const Text('Publication'),
          centerTitle: true,
          actions: [
            TextButton(
              onPressed: () {
                Get.back();
              },
              child: Text(
                "Annuler",
                style: getSemiBoldTextStyle(color: ColorManager.primary2),
              ),
            ),
          ],
        ),
        // floatingActionButton: FloatingActionButton(
        //     onPressed: () {
        //       // await addTransportController.addIntrant();
        //     },
        //     child: const Icon(
        //       Icons.arrow_forward_ios,
        //     )),
        body: Obx(
          () => addTransportController.isLoadParams.value == false
              ? Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    children: [
                      Column(
                        children: [
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Étape ${addTransportController.currentPage.value + 1}/${addTransportController.listeStep.length}",
                              style: getSemiBoldTextStyle(fontSize: 16),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          StepProgressIndicator(
                            totalSteps: addTransportController.listeStep.length,
                            currentStep:
                                addTransportController.currentPage.value + 1,
                            selectedColor: ColorManager.primary,
                            unselectedColor: Colors.grey,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          addTransportController.currentPage.value <= 0
                              ? Container()
                              : GestureDetector(
                                  onTap: () {
                                    addTransportController.currentPage.value--;
                                  },
                                  child: Row(
                                    children: [
                                      Icon(
                                        Icons.arrow_back_ios,
                                        color: ColorManager.primary2,
                                      ),
                                      Text(
                                        "Retour",
                                        style: getSemiBoldTextStyle(
                                            fontSize: 16,
                                            color: ColorManager.primary2),
                                      ),
                                    ],
                                  ),
                                )
                        ],
                      ),
                      const SizedBox(height: 20),
                      Expanded(
                          child: AnimatedContainer(
                              duration: const Duration(milliseconds: 500),
                              curve: Curves.easeInOut,
                              child: addTransportController.listeStep[
                                  addTransportController.currentPage.value])),
                      const SizedBox(
                        height: 10,
                      ),
                      addTransportController.currentPage.value > 0
                          ? DefaultButton(
                              text: addTransportController.currentPage.value <
                                      addTransportController.listeStep.length -
                                          1
                                  ? "Suivant"
                                  : "Publier",
                              press: () async {
                                if (addTransportController.currentPage.value <
                                    addTransportController.listeStep.length -
                                        1) {
                                  if (addTransportController.currentPage.value == 1 &&
                                      (addTransportController.capaciteController.value.text == "" ||
                                          addTransportController
                                                  .matriculeController
                                                  .value
                                                  .text ==
                                              "")) {
                                    showCustomFlushbar(
                                      context,
                                      "Veuillez remplir les champs",
                                      ColorManager.error,
                                      FlushbarPosition.TOP,
                                    );
                                  } else if (addTransportController.currentPage.value == 1 &&
                                      addTransportController
                                              .dateDeDisponibilite.value.text ==
                                          "" &&
                                      addTransportController.disponibilite.value.text ==
                                          "FUTUR") {
                                    showCustomFlushbar(
                                      context,
                                      "Entrer la date de disponibilté",
                                      ColorManager.error,
                                      FlushbarPosition.TOP,
                                    );
                                  } else if (addTransportController.currentPage.value == 1 &&
                                      int.parse(addTransportController.capaciteController.value.text) >
                                          addTransportController
                                              .carCapacityMax.value) {
                                    showCustomFlushbar(
                                      context,
                                      "La capcité est incorrecte",
                                      ColorManager.error,
                                      FlushbarPosition.TOP,
                                    );
                                  } else if (addTransportController
                                          .currentPage.value ==
                                      2) {
                                    if (addTransportController
                                            .priceController.value.text ==
                                        "") {
                                      showCustomFlushbar(
                                        context,
                                        "Entrer le prix",
                                        ColorManager.error,
                                        FlushbarPosition.TOP,
                                      );
                                    } else if (addTransportController
                                            .uniteController.value ==
                                        "") {
                                      showCustomFlushbar(
                                        context,
                                        "Entrer l'unité",
                                        ColorManager.error,
                                        FlushbarPosition.TOP,
                                      );
                                    } else {
                                      addTransportController
                                          .currentPage.value++;
                                    }
                                  } else if (addTransportController
                                              .locationDepartReturn
                                              .value
                                              .region ==
                                          null &&
                                      addTransportController.currentPage.value ==
                                          3) {
                                    showCustomFlushbar(
                                      context,
                                      "Entrer la region de depart",
                                      ColorManager.error,
                                      FlushbarPosition.TOP,
                                    );
                                  } else if (addTransportController
                                              .locationFinReturn.value.region ==
                                          null &&
                                      addTransportController.currentPage.value == 3) {
                                    showCustomFlushbar(
                                      context,
                                      "Entrer la region de fin",
                                      ColorManager.error,
                                      FlushbarPosition.TOP,
                                    );
                                  } else {
                                    addTransportController.currentPage.value++;
                                  }
                                } else {
                                  if (addTransportController
                                          .descriptionController.value.text ==
                                      "") {
                                    showCustomFlushbar(
                                      context,
                                      "Entrer la description",
                                      ColorManager.error,
                                      FlushbarPosition.TOP,
                                    );
                                  } else if (addTransportController
                                      .files.isEmpty) {
                                    showCustomFlushbar(
                                      context,
                                      "Veuillez ajouter des images",
                                      ColorManager.error,
                                      FlushbarPosition.TOP,
                                    );
                                  } else {
                                    await addTransportController.addTransport();
                                  }
                                }
                              },
                            )
                          : const SizedBox(),
                      const SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                )
              : const LoaderComponent(),
        ));
  }
}
