import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../common/cache_network_image.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/icon_manager.dart';
import '../controller/add_transport_controller.dart';

class ChoiceCarBloc extends StatelessWidget {
  const ChoiceCarBloc({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AddTransportController addTransportController = Get.find();
    return ListView.builder(
      shrinkWrap: true,
      itemCount: addTransportController.listCar.length,
      itemBuilder: (context, index) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          color: ColorManager.white,
          child: ListTile(
            onTap: () {
              addTransportController.carController.value =
                  addTransportController.listCar[index].id;

              addTransportController.categoryCarController.value =
                  addTransportController.listCar[index].categoryCar.id;
              addTransportController.currentPage.value += 1;
              addTransportController.carCapacityMax.value =
                  addTransportController.listCar[index].capacity;
              addTransportController.carCapacityMaxUnite.value =
                  addTransportController.listCar[index].unitOfMeasurment;
            },
            leading: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: ColorManager.grey,
              ),
              height: 30,
              width: 30,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(5.0),
                child: CacheNetworkImage(
                  image: addTransportController.listCar[index].image,
                ),
              ),
            ),
            title: Text(
              addTransportController.listCar[index].libelle,
            ),
            trailing: const Icon(IconManager.arrowRight),
          ),
        ),
      ),
    );
  }
}
