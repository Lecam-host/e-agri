import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/domain/model/Product.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../common/buttons/back_button.dart';
import '../common/date_field.dart';
import '../common/default_button.dart';
import '../ressources/styles_manager.dart';
import 'controller/demande_transport_controller.dart';

class DemandeTransportScreen extends StatefulWidget {
  const DemandeTransportScreen({Key? key, required this.product})
      : super(key: key);
  final ProductModel product;
  @override
  State<DemandeTransportScreen> createState() => _DemandeTransportScreenState();
}

class _DemandeTransportScreenState extends State<DemandeTransportScreen> {
  DemandeTransportController demandeTransportController =
      Get.put(DemandeTransportController());
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButtonCustom(),
        title: const Text("Demande"),
      ),
      body: Container(
        margin: const EdgeInsets.all(10),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text('Quel est le produit à transporter ?'),
                TextFormField(
                  controller:
                      demandeTransportController.marchandiseController.value,
                  decoration: InputDecoration(
                    // hintText:"La ",

                    label: Text(
                      "Entrer le Produit",
                      style: getRegularTextStyle(color: Colors.grey),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                const Text('Quelle est la quantité de votre marchandise ?'),
                TextFormField(
                  controller:
                      demandeTransportController.capacityController.value,
                  decoration: InputDecoration(
                    // hintText:"La ",

                    label: Text(
                      "Entrer la quantité",
                      style: getRegularTextStyle(color: Colors.grey),
                    ),
                  ),
                  keyboardType: TextInputType.number,
                ),
                Text(
                  "La quantité ne doit pas être superieure à ${widget.product.quantity} ${widget.product.unite}",
                  style: getRegularTextStyle(color: Colors.orange),
                ),
                const SizedBox(
                  height: 20,
                ),
                const Text('Quand avez-vous besoin du transporteur ?'),
                const SizedBox(
                  height: 10,
                ),
                DateField(
                    dateController:
                        demandeTransportController.dateController.value,
                    hintText: "Date de fin"),
                const SizedBox(
                  height: 30,
                ),
                const Text('Ajouter une description'),
                const SizedBox(
                  height: 10,
                ),
                TextFormField(
                  controller: demandeTransportController.description.value,
                  decoration: InputDecoration(
                    hintText: "Entrez la quantité",
                    label: Text(
                      "Description",
                      style: getRegularTextStyle(color: Colors.grey),
                    ),
                  ),
                  keyboardType: TextInputType.multiline,
                  maxLines: 3,
                ),
                const SizedBox(
                  height: 30,
                ),
                DefaultButton(
                  text: "Envoyer",
                  press: () {
                    if (demandeTransportController.description.value.text ==
                            "" ||
                        demandeTransportController.dateController.value.text ==
                            "" ||
                        demandeTransportController
                                .capacityController.value.text ==
                            "" ||
                        demandeTransportController
                                .marchandiseController.value.text ==
                            "") {
                      showCustomFlushbar(
                          context,
                          "Veuillez remplir tous les champs",
                          ColorManager.error,
                          FlushbarPosition.TOP);
                    } else if (int.parse(demandeTransportController
                            .capacityController.value.text) >
                        widget.product.quantity) {
                      showCustomFlushbar(
                          context,
                          "La quantité de votre marchandise est superieure la capacité du vehicule",
                          ColorManager.error,
                          FlushbarPosition.TOP);
                    } else {
                      demandeTransportController
                          .reserveTransport(widget.product);
                    }
                    // inspect(prestationController.locationReturn.value);
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
