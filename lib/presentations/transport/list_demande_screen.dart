import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../ressources/color_manager.dart';
import 'component/list_demande.dart';
import 'controller/demande_transport_controller.dart';

class ListDemandeTransportScreen extends StatefulWidget {
  const ListDemandeTransportScreen({Key? key}) : super(key: key);

  @override
  State<ListDemandeTransportScreen> createState() =>
      _ListDemandeTransportScreenState();
}

class _ListDemandeTransportScreenState extends State<ListDemandeTransportScreen>
    with SingleTickerProviderStateMixin {
  late TabController tabController;
  DemandeTransportController demandeTransportController =
      Get.put(DemandeTransportController());
  @override
  void initState() {
    demandeTransportController.listDemandeTransportRecu();
    demandeTransportController.listDemandeTransportEnvoyer();

    tabController = TabController(
      length: 2,
      vsync: this,
    );
    super.initState();
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          leading: const BackButtonCustom(),
          title: const Text('Demandes de camion'),
          bottom: TabBar(
            indicatorColor: ColorManager.primary,
            controller: tabController,
            tabs: [
              Text(
                "Reçues",
                style: getBoldTextStyle(),
              ),
              Text(
                "Envoyées",
                style: getBoldTextStyle(),
              ),
            ],
          ),
        ),
        body: TabBarView(
          controller: tabController,
          children: [
            Obx(
              () => demandeTransportController.loadDemandeRecu.value == true
                  ? const LoaderComponent()
                  : ListDemande(
                      listPrestation:
                          demandeTransportController.listDemandeRecu,
                    ),
            ),
            Obx(
              () => demandeTransportController.loadDemandeSend.value == true
                  ? const LoaderComponent()
                  : ListDemande(
                      listPrestation:
                          demandeTransportController.listDemandeEnvoyer,
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
