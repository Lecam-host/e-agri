import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/transport/filter_transport_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

import '../../app/constant.dart';
import '../../app/functions.dart';
import '../../domain/model/user_model.dart';
import '../ressources/assets_manager.dart';
import '../ressources/color_manager.dart';
import '../ressources/styles_manager.dart';
import 'component/transport_card.dart';
import 'controller/transport_controller.dart';

class TransportScreen extends StatefulWidget {
  const TransportScreen({Key? key}) : super(key: key);

  @override
  State<TransportScreen> createState() => _TransportScreenState();
}

class _TransportScreenState extends State<TransportScreen> {
  TransportController transportController = Get.put(TransportController());
  @override
  void initState() {
    // transportController.onInit();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
        builder: (context, user, child) => Scaffold(
            backgroundColor: ColorManager.backgroundColor,
            appBar: AppBar(
              leading: const BackButtonCustom(),
              title: const Text("Transport"),
              actions: [
                if (isInScopes(
                        UserPermissions.FILTER_TRANSPORT, user.scopes!) ==
                    true)
                  Container(
                    margin: const EdgeInsets.only(
                      top: 5,
                    ),
                    child: IconButton(
                      icon: SvgPicture.asset(
                        SvgManager.filter,
                        width: 20,
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          PageTransition(
                            type: PageTransitionType.rightToLeft,
                            child: const FilterTransportView(),
                            isIos: true,
                            duration: const Duration(
                                milliseconds: Constant.navigatorDuration),
                          ),
                        );
                      },
                    ),
                  ),
              ],
            ),
            body: Obx(
              () => Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 5,
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(children: [
                      categoryButton("Tout", 0, transportController),
                      ...List.generate(transportController.listCategorie.length,
                          (i) {
                        return categoryButton(
                            transportController.listCategorie[i].libelle,
                            transportController.listCategorie[i].id,
                            transportController);
                      }),
                    ]),
                  ),

                  if (transportController.isLoadTransport.value == false) ...[
                    if (transportController.listTransport.isNotEmpty) ...[
                      Expanded(
                        child: Container(
                          margin: const EdgeInsets.all(10),
                          child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: transportController.listTransport.length,
                            itemBuilder: (context, index) => Padding(
                              padding: const EdgeInsets.symmetric(vertical: 10),
                              child: Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 10),
                                color: ColorManager.white,
                                child: TransportCard(
                                    data: transportController
                                        .listTransport[index]),
                              ),
                            ),
                            // shrinkWrap: true,
                          ),
                        ),
                      ),
                    ] else ...[
                      const EmptyComponenent(
                        message: "Liste vide",
                      )
                    ],
                  ],
                  if (transportController.isLoadTransport.value == true)
                    Expanded(
                      child: ListView.builder(
                        itemCount: 10,
                        shrinkWrap: true,
                        // physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (context, index) =>
                            shimmerCustomTransport(),
                      ),
                    ),

                  // Expanded(
                  //   child: Obx(
                  //     () => transportController.isLoadTransport.value == false
                  //         ? transportController.listTransport.isEmpty
                  //             ? const
                  //             :
                  //         :
                  //   ),
                  // ),
                ],
              ),
            )));
  }

  Widget categoryButton(
      String titre, int categorieId, TransportController transportController) {
    return Obx(
      () => InkWell(
        onTap: () async {
          transportController.renitialiseFilter();
          transportController.nextPage.value = 0;
          transportController.paginationParametre();
         
          transportController.filterCategorieId.value.text =
              categorieId.toString();
          await transportController.getListTransport();
        },
        child: Container(
          margin: const EdgeInsets.only(
            left: 5,
            bottom: 5,
          ),
          padding: const EdgeInsets.all(5),
          decoration: BoxDecoration(
              color: transportController.filterCategorieId.value.text ==
                      categorieId.toString()
                  ? ColorManager.primary
                  : ColorManager.lightGrey,
              borderRadius: const BorderRadius.all(Radius.circular(10))),
          child: Text(
            titre,
            style: getRegularTextStyle(color: ColorManager.white),
          ),
        ),
      ),
    );
  }
}

Shimmer shimmerCustom() {
  return Shimmer.fromColors(
    baseColor: const Color.fromARGB(255, 178, 178, 178),
    highlightColor: const Color.fromARGB(255, 224, 223, 223),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.all(5),
          height: 10,
          width: 150,
          decoration: BoxDecoration(
            color: ColorManager.white,
            borderRadius: BorderRadius.circular(5),
          ),
        ),
        Container(
          margin: const EdgeInsets.all(5),
          height: 10,
          width: 200,
          decoration: BoxDecoration(
            color: ColorManager.white,
            borderRadius: BorderRadius.circular(5),
          ),
        ),
        Container(
          margin: const EdgeInsets.all(10),
          width: double.infinity,
          height: 200.0,
          decoration: BoxDecoration(
            color: ColorManager.white,
            borderRadius: BorderRadius.circular(15),
          ),
        ),
      ],
    ),
  );
}

Shimmer shimmerCustomTransport() {
  return Shimmer.fromColors(
    baseColor: const Color.fromARGB(255, 178, 178, 178),
    highlightColor: const Color.fromARGB(255, 224, 223, 223),
    child: Container(
      // padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.all(10),
            width: double.infinity,
            height: 150.0,
            decoration: BoxDecoration(
              color: ColorManager.white,
              borderRadius: BorderRadius.circular(5),
            ),
          ),
          Container(
            margin: const EdgeInsets.all(5),
            height: 10,
            width: 150,
            decoration: BoxDecoration(
              color: ColorManager.white,
              borderRadius: BorderRadius.circular(5),
            ),
          ),
          Container(
            margin: const EdgeInsets.all(10),
            width: double.infinity,
            height: 50.0,
            decoration: BoxDecoration(
              color: ColorManager.white,
              borderRadius: BorderRadius.circular(5),
            ),
          ),
          Container(
            margin: const EdgeInsets.all(5),
            height: 10,
            width: 200,
            decoration: BoxDecoration(
              color: ColorManager.white,
              borderRadius: BorderRadius.circular(5),
            ),
          ),
        ],
      ),
    ),
  );
}
