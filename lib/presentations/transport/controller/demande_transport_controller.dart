import 'dart:developer';

import 'package:eagri/data/repository/api/repository_impl.dart';
import 'package:eagri/domain/model/Product.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/common/state/succes_page_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../app/app_prefs.dart';
import '../../../app/di.dart';
import '../../../data/request/request.dart';
import '../../../data/request/transport_resquest.dart';
import '../../../domain/model/transport_model.dart';
import '../../../domain/model/user_model.dart';

class DemandeTransportController extends GetxController {
  var capacityController = TextEditingController().obs;
  var marchandiseController = TextEditingController().obs;

  var dateController = TextEditingController().obs;
  var description = TextEditingController().obs;
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  AppSharedPreference appSharedPreference = instance<AppSharedPreference>();
  var listDemandeRecu = <ItemDemandeTransport>[].obs;
  var listDemandeEnvoyer = <ItemDemandeTransport>[].obs;

  var loadDemandeSend = true.obs;
  var loadDemandeRecu = true.obs;

  listDemandeTransportRecu() async {
    loadDemandeRecu.value = true;

    await appSharedPreference.getUserInformation().then((value) async {
      await repositoryImpl
          .getListDemandeTransportRecu(value.id!)
          .then((response) async {
        await response.fold((l) {}, (data) async {
          listDemandeRecu.value = [];
          await Future.forEach(data.dataDemandeTransport.items,
              (ItemDemandeTransport element) async {
            await repositoryImpl
                .getInfoById(
              InfoByIdRequest(userId: element.userId),
            )
                .then((response) async {
              await response.fold((l) {}, (userData) async {
                if (userData.user != null) {
                  UserModel user = UserModel(
                    firstName: userData.user!.firstname,
                    lastName: userData.user!.laststname,
                    number: userData.user!.phone,
                  );
                  ItemDemandeTransport item = element;
                  item.demandeur = user;
                  item.isReceive = true;
                  listDemandeRecu.add(item);
                  // listDemandeRecu.value = data.dataDemandeTransport.items;
                }
              });
            });
          });
        });
      });
    });
    loadDemandeRecu.value = false;
  }

  listDemandeTransportEnvoyer() async {
    loadDemandeSend.value = true;
    await appSharedPreference.getUserInformation().then((value) async {
      await repositoryImpl
          .getListDemandeTransportSend(value.id!)
          .then((response) async {
        await response.fold((l) {}, (data) async {
          listDemandeEnvoyer.value = [];
          await Future.forEach(data.dataDemandeTransport.items,
              (ItemDemandeTransport element) async {
            await repositoryImpl
                .getInfoById(
              InfoByIdRequest(userId: element.fournisseurId),
            )
                .then((response) async {
              await response.fold((l) {}, (userData) async {
                if (userData.user != null) {
                  UserModel user = UserModel(
                    firstName: userData.user!.firstname,
                    lastName: userData.user!.laststname,
                    number: userData.user!.phone,
                  );
                  ItemDemandeTransport item = element;
                  item.demandeur = user;
                  item.isReceive = false;
                  listDemandeEnvoyer.add(item);
                  // listDemandeRecu.value = data.dataDemandeTransport.items;
                }
              });
            });
          });
        });
      });
    });
    loadDemandeSend.value = false;
  }

  reserveTransport(ProductModel product) async {
    Get.dialog(const LoaderComponent());
    await appSharedPreference.getUserInformation().then((value) async {
      DemandeTransportRequest demandeTransportRequest = DemandeTransportRequest(
        marchandise: marchandiseController.value.text,
        userId: value.id!,
        idProduct: product.id,
        capacity: int.parse(capacityController.value.text),
        date: dateController.value.text,
        unitOfMeasurment: product.unite,
        description: description.value.text,
        hour: "06:00",
      );
      inspect(demandeTransportRequest);
      await repositoryImpl
          .reserveTransport(demandeTransportRequest)
          .then((response) async {
        response.fold((failure) {
          inspect(failure);
          Get.back();
        }, (returnResponse) {
          Get.back();
          Get.off(const SuccessPageView(
            message: "Votre reservation été prise en compte",
          ));
        });
      });
    });
  }
}
