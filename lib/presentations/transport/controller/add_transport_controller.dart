import 'dart:io';

import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/app/constant.dart';
import 'package:eagri/data/repository/api/repository_impl.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/common/state/succes_page_view.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../app/app_prefs.dart';
import '../../../app/di.dart';
import '../../../app/functions.dart';
import '../../../data/request/request_object.dart';
import '../../../data/request/transport_resquest.dart';
import '../../../domain/model/transport_model.dart';
import '../../catalogue/controllers/catalogue_controller.dart';
import '../../common/location_select/location_select_widget.dart';
import '../addTransportOffer/add_car_info.dart';
import '../addTransportOffer/add_car_price.dart';
import '../addTransportOffer/add_image_bloc.dart';
import '../addTransportOffer/add_location_bloc.dart';
import '../addTransportOffer/choice_car_bloc.dart';

class AddTransportController extends GetxController {
  AppSharedPreference appSharedPreference = instance<AppSharedPreference>();
  CatalogueController catalogueController = Get.find();

  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  var listCarMap = <Map<String, dynamic>>[];
  var listUnitMap = <Map<String, dynamic>>[];
  var listMethodPaiement = <Map<String, dynamic>>[];

  List<CarModel> listCar = [];
  var carController = 0.obs;
  var carCapacityMax = 0.obs;
  var carCapacityMaxUnite = "".obs;

  var categoryCarController = 0.obs;
  var uniteController = "".obs;
  var moyenPaiementController = 0.obs;

  var currentPage = 0.obs;

  var locationDepartReturn = LocationReturnModel().obs;
  var locationFinReturn = LocationReturnModel().obs;

  var descriptionController = TextEditingController().obs;
  var priceController = TextEditingController().obs;
  var matriculeController = TextEditingController().obs;
  var capaciteController = TextEditingController().obs;

  var dateDeDisponibilite = TextEditingController().obs;
  var disponibilite = TextEditingController(text: "IMMEDIAT").obs;

  var transportStartRegionValue = 0.obs;
  var transportEndRegionValue = 0.obs;

  var files = <File>[].obs;
  List<Widget> listeStep = [
    const ChoiceCarBloc(),
    const AddCarInfoBloc(),
    const AddCarPrice(),
    const AddLocationBloc(),
    const AddImageBloc()
  ];
  var isLoadParams = true.obs;
  @override
  onInit() async {
    isLoadParams.value = true;
    await getListCar();
    await getListUnit();
    await fetchMethodPaiement();
    isLoadParams.value = false;
    super.onInit();
  }

  addTransport() async {
    Get.dialog(const LoaderComponent());
    await createCatalogue();
    DateFormat formatter = DateFormat('yyyy-MM-dd');

    await appSharedPreference.getUserInformation().then((value) async {
      AddTransportRequest request = AddTransportRequest(
        matricule: matriculeController.value.text,
        hour: "06:00",
        unitOfMeasurment: uniteController.value,
        expirationDate: formatter.format(DateTime.now()),
        availabilityDate: dateDeDisponibilite.value.text != ""
            ? formatter.format(DateTime.parse(dateDeDisponibilite.value.text))
            : null,
        paymentId: moyenPaiementController.value,
        availability: disponibilite.value.text,
        images: files,
        fournisseurId: value.id!,
        categorieId: categoryCarController.value,
        vehiculeId: carController.value,
        capacite: int.parse(
          capaciteController.value.text,
        ),
        description: descriptionController.value.text,
        price: int.parse(
          priceController.value.text,
        ),
        locationEnd: AddProductLocationRequest(
          regionId: locationFinReturn.value.region!.id,
          departementId: locationFinReturn.value.departement != null
              ? locationFinReturn.value.departement!.id
              : 0,
          sousPrefectureId: locationFinReturn.value.sprefecture != null
              ? locationFinReturn.value.sprefecture!.id
              : 0,
          localiteId: locationFinReturn.value.localite != null
              ? locationFinReturn.value.localite!.id
              : 0,
        ),
        locationStart: AddProductLocationRequest(
          regionId: locationDepartReturn.value.region!.id,
          departementId: locationDepartReturn.value.departement != null
              ? locationDepartReturn.value.departement!.id
              : 0,
          sousPrefectureId: locationDepartReturn.value.sprefecture != null
              ? locationDepartReturn.value.sprefecture!.id
              : 0,
          localiteId: locationDepartReturn.value.localite != null
              ? locationDepartReturn.value.localite!.id
              : 0,
        ),
      );
      await repositoryImpl
          .addTransport(
            request,
          )
          .then(
            (response) => response.fold((failure) {
              Get.back();
              showCustomFlushbar(Get.context!, "Echec de la publication",
                  ColorManager.error, FlushbarPosition.TOP);
            }, (data) {
              catalogueController
                  .getListFnCodeProduct(codeTransport.toString());
              Get.back();
              Get.off(const SuccessPageView(
                message: "Votre publication a été effectuée avec succès",
              ));
            }),
          );
    });
  }

  Future fetchMethodPaiement() async {
    (await repositoryImpl.getMethodPaiementAll()).fold((l) {}, (r) {
      listMethodPaiement = r.data!.items!
          .map((element) => {"value": element.id, "label": element.name})
          .toList();
    });
  }

  getListUnit() async {
    await repositoryImpl.getTransportUnits().then((response) async {
      response.fold((l) => null, (list) {
        listUnitMap = list.data
            .map((element) => {"value": element.id, "label": element.name})
            .toList();
      });
    });
  }

  getListCar() async {
    await repositoryImpl.getListCar().then((response) async {
      response.fold((l) => null, (list) {
        listCar = list.data.listCars;
        listCarMap = list.data.listCars
            .map((element) => {"value": element, "label": element.libelle})
            .toList();
      });
    });
  }
}
