import 'package:eagri/data/mapper/shop_mapper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../app/constant.dart';
import '../../../app/di.dart';
import '../../../data/repository/api/repository_impl.dart';
import '../../../data/request/shop_resquests.dart';
import '../../../domain/model/Product.dart';
import '../../../domain/model/transport_model.dart';
import '../../common/location_select/location_select_widget.dart';

class TransportController extends GetxController {
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  var listCar = <CarModel>[].obs;
  var listCategorie = <CategoryVehicle>[].obs;
  var listCategorieMap = <Map<String, dynamic>>[].obs;
  var listCarMap = <Map<String, dynamic>>[].obs;

  var listTransport = <ProductModel>[].obs;
  var isLoadTransport = true.obs;
  var isLoadMoreTransport = false.obs;
  var scrollController = ScrollController().obs;
  var prixMaxController = TextEditingController().obs;
  var prixMinController = TextEditingController().obs;
  var filterCategorieId = TextEditingController(text: "0").obs;
  var locationReturn = LocationReturnModel().obs;
  var page = 0.obs;
  var limitPage = 10.obs;
  var totalItems = 0.obs;
  var nextPage = 0.obs;

  var loadMore = true.obs;
renitialiseFilter(){
  prixMaxController.value.clear();
  prixMinController.value.clear();
  filterCategorieId.value=TextEditingController(text: "0");
  locationReturn.value= LocationReturnModel();
  page.value=0;
  nextPage.value=0;
}
  @override
  onInit() async {
    getListTransport();

    getListCar();
    getCategorieTransport();
    scrollController.value.addListener(_scrollListener);
    super.onInit();
  }

  filterProduct() async {
    isLoadTransport.value = true;

    listTransport.value = [];

    Get.back();
     getListTransport();
  }

  getCategorieTransport() async {
    await repositoryImpl.getListCategorieVehicule().then((response) {
      response.fold((failure) {}, (data) {
        listCategorie.value = data.listCategorie;
        listCategorieMap.value = data.listCategorie
            .map((e) => {"value": e.id, "label": e.libelle})
            .toList();
      });
    });
  }

  getListCar() async {
    await repositoryImpl.getListCar().then((response) async {
      response.fold((l) => null, (list) {
        listCar.value = list.data.listCars;
        listCarMap.value = list.data.listCars
            .map((e) => {"value": e.id, "label": e.libelle})
            .toList();
      });
    });
  }

  getListTransport() async {
    if (isLoadMoreTransport.value == false) {
      isLoadTransport.value = true;
    }

    await repositoryImpl
        .searchProduct(
          SearchProductRequest(
            status: "AVAILABLE",
            limit: limitPage.value,
            page: nextPage.value,
            codeTypeProduit: codeTransport,
            categorieId: filterCategorieId.value.text != "0"
                ? int.parse(filterCategorieId.value.text)
                : null,
            priceMax: prixMaxController.value.text != ""
                ? double.parse(prixMaxController.value.text)
                : null,
            priceMin: prixMinController.value.text != ""
                ? double.parse(prixMinController.value.text)
                : null,
            regionId: locationReturn.value.region != null
                ? locationReturn.value.region!.id
                : null,
            // departementId: locationController.value.departement != null
            //     ? locationController.value.departement!.id
            //     : null,
            // sprefectureId: locationController.value.sprefecture != null
            //     ? locationController.value.sprefecture!.id
            //     : null,
            // locationId: locationController.value.localite != null
            //     ? locationController.value.localite!.id
            //     : null,
          ),
        )
        .then((value) => value.fold((l) => null, (list) async {
              totalItems.value = list.totalObject!;
              nextPage.value = list.nextPage!;
              List<ProductModel> listProduct = await list.toDomain();
              if (isLoadMoreTransport.value == true) {
                listTransport.addAll(listProduct);
              } else {
                listTransport.value = listProduct;
              }
              isLoadMoreTransport.value = false;
              isLoadTransport.value = false;
            }));
  }

  // Future<void> loadMoreData() async {
  //   log("loadMoreData");
  //   await repositoryImpl
  //       .searchProduct(
  //     SearchProductRequest(
  //       limit: limitPage.value,
  //       page: page.value,
  //       codeTypeProduit: codeTransport,
  //       categorieId: filterCategorieId.value.text != ""
  //           ? int.parse(filterCategorieId.value.text)
  //           : null,
  //       // priceMax: prixMaxController.value.text != ""
  //       //     ? double.parse(prixMaxController.value.text)
  //       //     : null,
  //       // priceMin: prixMinController.value.text != ""
  //       //     ? double.parse(prixMinController.value.text)
  //       //     : null,
  //       // regionId: locationController.value.region != null
  //       //     ? locationController.value.region!.id
  //       //     : null,
  //       // departementId: locationController.value.departement != null
  //       //     ? locationController.value.departement!.id
  //       //     : null,
  //       // sprefectureId: locationController.value.sprefecture != null
  //       //     ? locationController.value.sprefecture!.id
  //       //     : null,
  //       // locationId: locationController.value.localite != null
  //       //     ? locationController.value.localite!.id
  //       //     : null,
  //     ),
  //   )
  //       .then(
  //     (value) {
  //       log("C'est rentré");
  //       inspect(value);
  //       value.fold(
  //         (l) {
  //           log("Message ");
  //           inspect(l);
  //           isLoadMoreTransport.value = false;
  //         },
  //         (list) async {
  //           totalItems.value = list.totalObject!;
  //           log("loadMoreData yes");
  //           final data = await list.toDomain();
  //           for (var i = 0; i < data.length; i++) {
  //             listTransport.add(data[i]);
  //           }
  //           isLoadMoreTransport.value = false;
  //         },
  //       );
  //     },
  //   ).catchError((e) {
  //     log(e);
  //     isLoadMoreTransport.value = false;
  //   });
  // }

  void paginationParametre() {
    page.value = 0;
  }

  void _scrollListener() async {
    // limitPage.value = nextPage.value;
    if (isLoadMoreTransport.value && totalItems.value > listTransport.length) {
      return null;
    }

    if (scrollController.value.offset >=
            scrollController.value.position.maxScrollExtent &&
        !scrollController.value.position.outOfRange) {
      page.value = page.value + 1;
      isLoadMoreTransport.value = true;
      await getListTransport();
    }
  }
}
