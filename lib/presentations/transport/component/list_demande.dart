import 'package:eagri/domain/model/transport_model.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/ressources/assets_manager.dart';
import 'package:eagri/presentations/ressources/icon_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:page_transition/page_transition.dart';
import '../../../app/functions.dart';
import '../../ressources/color_manager.dart';
import '../details_demande/detail_demande.dart';

class ListDemande extends StatelessWidget {
  const ListDemande({Key? key, required this.listPrestation}) : super(key: key);
  final RxList<ItemDemandeTransport> listPrestation;
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => listPrestation.isNotEmpty
          ? ListView.builder(
              shrinkWrap: true,
              itemCount: listPrestation.length,
              itemBuilder: (context, index) => Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      PageTransition(
                        type: PageTransitionType.bottomToTop,
                        child: DetailDemande(
                          demande: listPrestation[index],
                        ),
                        isIos: true,
                        duration: const Duration(milliseconds: 400),
                      ),
                    );
                  },
                  child: Stack(
                    children: [
                      Container(
                        width: double.infinity,
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        color: ColorManager.white,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                CircleAvatar(
                                  backgroundColor:
                                      listPrestation[index].isReceive == true
                                          ? ColorManager.primary4
                                          : ColorManager.primaryOpacity70,
                                  child: Image.asset(
                                    listPrestation[index].isReceive == true
                                        ? IconAssetManager.receiveIcon
                                        : IconAssetManager.sendIcon,
                                    width: 25,
                                  ),
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    if (listPrestation[index].demandeur != null)
                                      Text(
                                        "${listPrestation[index].demandeur!.firstName.toString()} ${listPrestation[index].demandeur!.lastName.toString()}",
                                        style: getMeduimTextStyle(
                                          color: ColorManager.black,
                                          fontSize: 12,
                                        ),
                                      ),
                                    if (listPrestation[index].isReceive ==
                                        false)
                                      Text(
                                        'Transporteur',
                                        style: getRegularTextStyle(
                                          color: ColorManager.grey1,
                                          fontSize: 12,
                                        ),
                                      ),
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      "${convertDate(listPrestation[index].date.toString())} ",
                                      style: getMeduimTextStyle(
                                        color: ColorManager.black,
                                        fontSize: 12,
                                      ),
                                    ),
                                    Text(
                                      'Date',
                                      style: getRegularTextStyle(
                                        color: ColorManager.grey1,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            IconButton(
                                onPressed: () {},
                                icon: const Icon(IconManager.arrowRight))
                            // if (listPrestation[index].prestationDemandeData.isValid ==
                            //     false)
                            // TextButton(
                            //     onPressed: () {
                            //       showDialog(
                            //           context: context,
                            //           barrierDismissible: false,
                            //           builder: (context) {
                            //             return AlertDialog(
                            //               backgroundColor: Colors.white,
                            //               contentPadding: const EdgeInsets.all(12),
                            //               content: Column(
                            //                 mainAxisSize: MainAxisSize.min,
                            //                 children: [
                            //                   const Text(
                            //                     'Vous-voulez vraiment accepter cette démande',
                            //                     textAlign: TextAlign.center,
                            //                   ),
                            //                   TextButton(
                            //                     onPressed: () {
                            //                       Navigator.pop(context);
                            //                     },
                            //                     child: Text('Non',
                            //                         style: getBoldTextStyle(
                            //                             color: ColorManager.grey)),
                            //                   ),
                            //                   TextButton(
                            //                     onPressed: () async {
                            //                       Navigator.pop(context);
                            //                       await prestationController
                            //                           .validateDemandePrestation(
                            //                               listPrestation[index]
                            //                                   .prestation
                            //                                   .id);
                            //                     },
                            //                     child: Text('Oui',
                            //                         style: getBoldTextStyle(
                            //                             color: ColorManager.primary,
                            //                             fontSize: 14)),
                            //                   ),
                            //                 ],
                            //               ),
                            //             );
                            //           });
                            //     },
                            //     child: Text(
                            //       "Accepter",
                            //       style: getMeduimTextStyle(
                            //         color: ColorManager.primary,
                            //         fontSize: 12,
                            //       ),
                            //     ))
                          ],
                        ),
                      ),
                      // if (listPrestation[index].prestationDemandeData.isValid == true)
                      //   Positioned(
                      //     top: 50,
                      //     left: 30,
                      //     child: CircleAvatar(
                      //       radius: 10,
                      //       backgroundColor: ColorManager.primary,
                      //       child: Icon(
                      //         Icons.check,
                      //         size: 10,
                      //         color: ColorManager.white,
                      //       ),
                      //     ),
                      //   ),
                    ],
                  ),
                ),
              ),
            )
          : const EmptyComponenent(
              message: "Liste vide",
            ),
    );
  }
}
