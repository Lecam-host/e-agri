import 'package:animations/animations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/app/constant.dart';
import 'package:eagri/presentations/details_produit/details_produit_screen.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../domain/model/Product.dart';
import '../../ressources/strings_manager.dart';

typedef ProductCardOnTaped = void Function(ProductModel data);

class ProductInfo extends StatelessWidget {
  const ProductInfo({
    super.key,
    required this.data,
    this.ontap,
    this.isOffreAchatFind = false,
    this.isIntrant = false,
  });

  final ProductModel data;
  final bool isOffreAchatFind;
  final bool isIntrant;

  final ProductCardOnTaped? ontap;

  @override
  Widget build(BuildContext context) {
    // final data = datas[index % datas.length];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Stack(
          children: [
            if (data.images!.isNotEmpty)
              CachedNetworkImage(
                imageUrl: data.images![0],
                imageBuilder: (context, imageProvider) => Container(
                  width: double.maxFinite,
                  height: 150,
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(5),
                        topRight: Radius.circular(5)),
                    image: DecorationImage(
                      scale: 1,
                      image: imageProvider,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                placeholder: (context, url) => Container(
                  color: ColorManager.grey.withOpacity(0.2),
                  height: 150,
                  width: double.infinity,
                ),
                errorWidget: (context, url, error) => Container(
                  color: ColorManager.grey.withOpacity(0.2),
                  height: 150,
                  width: double.infinity,
                  child: const Icon(Icons.error),
                ),
              ),
            if (data.images!.isEmpty)
              Container(
                width: double.maxFinite,
                height: 150,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10)),
                ),
                child: const Icon(Icons.image),
              ),

            if (data.typeOffers.toLowerCase() == "vente" &&
                data.code == codeProductAgricultural.toString())
              Positioned(
                top: 5,
                left: 5,
                child: Container(
                  // alignment: Alignment.center,
                  padding: const EdgeInsets.only(
                      bottom: 4, top: 2, right: 3, left: 3),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: ColorManager.primary.withOpacity(0.7),
                  ),
                  child: Text(
                    "En ${data.typeVente}",
                    style: getRegularTextStyle(
                        color: ColorManager.white, fontSize: 10),
                  ),
                ),
              ),
            // Positioned(
            //   bottom: 5,
            //   right: 5,
            //   child: CircleAvatar(
            //     radius: iconSize,
            //     backgroundColor: ColorManager.white.withOpacity(0.8),
            //     child: SvgPicture.asset(SvgManager.like,
            //         color: ColorManager.black,
            //         width: iconSize,
            //         height: iconSize),
            //   ),
            // ),

            // Positioned(
            //   bottom: 5,
            //   left: 5,
            //   child: CircleAvatar(
            //     radius: iconSize,
            //     backgroundColor: ColorManager.white.withOpacity(0.8),
            //     child: SvgPicture.asset(SvgManager.search,
            //         color: ColorManager.black,
            //         width: iconSize,
            //         height: iconSize),
            //   ),
            // )
          ],
        ),
        Container(
          decoration: BoxDecoration(
            color: ColorManager.white,
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10),
            ),
          ),
          padding: const EdgeInsets.all(8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                // height: 20,
                child: Text(
                  data.title,
                  style: getMeduimTextStyle(
                    color: ColorManager.black,
                    fontSize: 12,
                  ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),

              Text(
                'Capacité : ${data.quantity} ${data.unite}',
                style:  TextStyle(
                  color: ColorManager.red,
                  fontWeight: FontWeight.w500,
                  fontSize: 12,
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              const Text("Trajet__"),
              Container(
                padding: const EdgeInsets.all(5),
                color: ColorManager.lightGrey.withOpacity(0.5),
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 5,
                    ),
                     if (data.startTrajetInfo != null)
                    RichText(
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      text: TextSpan(
                        text: "Depart : ",
                        style: getRegularTextStyle(
                            color: ColorManager.grey, fontSize: 12),
                        children: [
                           if (data.startTrajetInfo!.region != null)
                          TextSpan(
                            text: data.startTrajetInfo!.region,
                            style: getSemiBoldTextStyle(fontSize: 12),
                          ),
                          if (data.startTrajetInfo!.departement != null)
                            TextSpan(
                              text: ", ${data.startTrajetInfo!.departement}",
                              style: getSemiBoldTextStyle(fontSize: 12),
                            ),
                          if (data.startTrajetInfo!.sprefecture != null)
                            TextSpan(
                              text: ", ${data.startTrajetInfo!.sprefecture}",
                              style: getSemiBoldTextStyle(fontSize: 12),
                            ),
                          if (data.startTrajetInfo!.localite != null)
                            TextSpan(
                              text: ", ${data.startTrajetInfo!.localite}",
                              style: getSemiBoldTextStyle(fontSize: 12),
                            ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                      if (data.arrivalTrajetInfo != null)
                    RichText(
                       maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      text: TextSpan(
                        text: "Arrivé : ",
                        style: getRegularTextStyle(
                            color: ColorManager.grey, fontSize: 12),
                        children: [
                           if (data.arrivalTrajetInfo!.region != null)
                          TextSpan(
                            text: data.arrivalTrajetInfo!.region,
                            style: getSemiBoldTextStyle(fontSize: 12),
                          ),
                          if (data.arrivalTrajetInfo!.departement != null)
                            TextSpan(
                              text: ", ${data.arrivalTrajetInfo!.departement}",
                              style: getSemiBoldTextStyle(fontSize: 12),
                            ),
                          if (data.arrivalTrajetInfo!.sprefecture != null)
                            TextSpan(
                              text: ", ${data.arrivalTrajetInfo!.sprefecture}",
                              style: getSemiBoldTextStyle(fontSize: 12),
                            ),
                          if (data.arrivalTrajetInfo!.localite != null)
                            TextSpan(
                              text: ", ${data.arrivalTrajetInfo!.localite}",
                              style: getSemiBoldTextStyle(fontSize: 12),
                            ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),

              // _buildSoldPoint(4.5, 6937),
              const SizedBox(
                height: 5,
              ),
              Row(
                children: [
                  Text(
                    "${data.rating}/5",
                    style: getMeduimTextStyle(
                        color: ColorManager.grey, fontSize: 10),
                  ),
                  const SizedBox(
                    width: 2,
                  ),
                  SvgPicture.asset(
                    "assets/icons/Star Icon.svg",
                    width: 10,
                  ),
                  const Spacer(),
                  RichText(
                    text: TextSpan(
                      text: "Cout : ",
                      style: getRegularTextStyle(
                          color: ColorManager.grey, fontSize: 12),
                      children: [
                        TextSpan(
                            text: '${data.price} ${AppStrings.moneyUnit}',
                            style: getMeduimTextStyle(
                              color: ColorManager.primary,
                              fontSize: 14,
                            )),
                      ],
                    ),
                  ),
                ],
              ),

              // ProductRate(
              //   product: data,
              // )
            ],
          ),
        ),
      ],
    );
  }
}

class TransportCard extends StatelessWidget {
  const TransportCard({
    super.key,
    required this.data,
    this.ontap,
    this.isOffreAchatFind = false,
    this.isIntrant = false,
  });

  final ProductModel data;
  final bool isOffreAchatFind;
  final bool isIntrant;

  final ProductCardOnTaped? ontap;
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openElevation: 0,
      closedElevation: 0,
      transitionType: transitionType,
      transitionDuration: transitionDuration,
      openBuilder: (context, _) => DetailsProduitScreen(
          product: ProductDetailsArguments(
        product: data,
        isIntrant: isIntrant,
      )),
      closedBuilder: (context, VoidCallback openContainer) => ProductInfo(
        data: data,
      ),
    );
  }
}
