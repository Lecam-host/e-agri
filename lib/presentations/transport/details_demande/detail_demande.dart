import 'dart:developer';

import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/data/repository/api/repository_impl.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/common/state/succes_page_view.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

import '../../../app/di.dart';
import '../../../domain/model/transport_model.dart';

import '../../../domain/model/user_model.dart';
import '../../common/default_button.dart';
import '../../common/state/exist_product_component.dart';
import '../../ressources/styles_manager.dart';
import '../controller/demande_transport_controller.dart';
import 'details_demande_appbar.dart';
import 'details_description_prestation.dart';
import 'details_image.dart';

class DetailDemande extends StatefulWidget {
  const DetailDemande({Key? key, required this.demande}) : super(key: key);

  final ItemDemandeTransport demande;
  @override
  State<DetailDemande> createState() => _DetailDemandeState();
}

class _DetailDemandeState extends State<DetailDemande> {
   DataDemandeForDetailModel? demandeShow;
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  DemandeTransportController demandeTransportController = Get.find();
  bool loadInfo = true;
  @override
  void initState() {
    loadInfo = true;
    repositoryImpl
        .getDetailProduct(widget.demande.offerId)
        .then((response) async {
      await response.fold((l) {
        setState(() {
           loadInfo = false;
        });
        
      }, (data) async {
        await data.then((value) {
          setState(() {
            demandeShow = DataDemandeForDetailModel(
              prestation: value!,
              prestationDemandeData: widget.demande,
              user: widget.demande.demandeur!,
              isReceive: widget.demande.isReceive,
            );
            loadInfo = false;
          });
          inspect(demandeShow);
        });
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
      builder: (context, user, child) => Scaffold(
        
        bottomNavigationBar: loadInfo == true
            ? const SizedBox()
            :demandeShow!=null? demandeShow!.prestationDemandeData.terminate == false
                ? Container(
                    margin: const EdgeInsets.all(10),
                    child: demandeShow!.prestationDemandeData.isReceive == true
                        ? DefaultButton(
                            text: demandeShow!.prestationDemandeData.isValid ==
                                    true
                                ? "Valider le chargement"
                                : "Accepter",
                            press: () {
                              if (demandeShow!.prestationDemandeData.isValid ==
                                  true) {
                                validChargement("FOURNISSEUR");
                              } else {
                                validDemande(user.id!);
                              }
                            },
                          )
                        : demandeShow!.prestationDemandeData.removalStatus ==
                                true
                            ? DefaultButton(
                                text: "Valider le dechargment",
                                press: () {
                                  validChargement("USER");
                                },
                              )
                            : const SizedBox(),
                  ) :const SizedBox()
                : const SizedBox(),
        backgroundColor: ColorManager.backgroundColor,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(AppBar().preferredSize.height),
            child: loadInfo == true
                ? const SizedBox()
                :demandeShow!=null? DetailsDemandeAppBar(
                    demandeData: demandeShow!,
                  ): AppBar(leading:const BackButtonCustom(),)),
        body: loadInfo == true
            ? const LoaderComponent()
            :demandeShow!=null? Hero(
                tag: "demande",
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    DemandeImages(
                      dataDemande: demandeShow!,
                    ),
                    DetatilsDescriptionDemandeTransport(
                      demandeData: demandeShow!,
                    )
                  ],
                ),
              ):const  NotExistProductComponent(),
      ),
    );
  }

  validChargement(String personal) {
    TextEditingController codeController = TextEditingController();
    Get.bottomSheet(
      isScrollControlled: false,
      Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10)),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Veuillez entrer le code fourni par le client",
                style: getBoldTextStyle(color: Colors.black, fontSize: 20),
              ),
              const SizedBox(
                height: 10,
              ),
              TextField(
                controller: codeController,
                decoration: InputDecoration(
                  // hintText:"La ",
                  label: Text(
                    "Entrer le code",
                    style: getRegularTextStyle(color: Colors.grey),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 100,
                    child: DefaultButton(
                      color: ColorManager.grey,
                      text: "Annuler",
                      press: () {
                        Get.back();
                      },
                    ),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  SizedBox(
                    width: 100,
                    child: DefaultButton(
                      text: "Envoyer",
                      press: () async {
                        Get.dialog(const LoaderComponent());
                        repositoryImpl
                            .validChargement(
                                demandeShow!.prestationDemandeData.id,
                                personal,
                                codeController.text)
                            .then((response) {
                          response.fold((failure) {
                            Get.back();
                            showCustomFlushbar(context, "Code invalide",
                                ColorManager.error, FlushbarPosition.TOP);
                          }, (dataResponse) {
                            Get.back();
                            Get.back();
                            if (personal == "USER") {
                              demandeTransportController
                                  .listDemandeTransportEnvoyer();
                            } else {
                              demandeTransportController
                                  .listDemandeTransportRecu();
                            }

                            Get.off(
                              const SuccessPageView(
                                message: "Code validé",
                              ),
                            );
                          });
                        });
                      },
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }

  validDemande(int userId) {
    Get.dialog(const LoaderComponent());
    repositoryImpl
        .validDemandeTransport(userId, demandeShow!.prestationDemandeData.id)
        .then((response) {
      response.fold(
        (failure) {
          Get.back();
          showCustomFlushbar(
            context,
            "Error",
            ColorManager.error,
            FlushbarPosition.TOP,
          );
        },
        (dataResponse) {
          Get.back();
          Get.off(
            const SuccessPageView(
              message: "Demande acceptée",
              subMessage:
                  "Le client a reçu un code qu'il vous transmettra lors du chargement",
            ),
          );
        },
      );
    });
  }
}
