import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/controllers/speculation_controller.dart';
import 'package:eagri/data/repository/api/repository_impl.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../app/di.dart';
import '../../../domain/model/favorite_model.dart';
import '../../common/flushAlert_componenent.dart';
import '../../ressources/color_manager.dart';

class FavoriteController extends GetxController {
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  SpeculationController speculationController =
      Get.put(SpeculationController());
  final listSpeculation = <SpeculationModel>[].obs;
  final listSpeculationAfficher = <SpeculationModel>[].obs;
  final listSpeculationChoose = <SpeculationModel>[].obs;
  final listSpeculationFavorite = <SpeculationModel>[].obs;
  final listFavoriteData = <FavoriteData>[].obs;

  var userfavorites = <FavoriteModel>[].obs;
  var searchInput = TextEditingController().obs;
  var isloadAdd = true.obs;
  var isloadListFavorite = true.obs;

  @override
  void onInit() async {
    await getUserFavorite();
    await getListSpeculation();

    super.onInit();
  }

  getListSpeculation() async {
    List<SpeculationModel> list = [];
    List<int> ids = [];
    if (userfavorites.isEmpty) {
      list = speculationController.listSpeculation;
    } else {
      await Future.forEach(userfavorites,
          (FavoriteModel element) => ids.add(element.speculationId));

      await Future.forEach(speculationController.listSpeculation,
          (SpeculationModel element) {
        if (!ids.contains(element.speculationId)) {
          list.add(element);
        }
      });
    }
    listSpeculation.value = list;
    listSpeculationAfficher.value = list;
  }

  searchSpeculation() {
    List<SpeculationModel> listSpeculationSearch = [];
    if (searchInput.value.text.isNotEmpty) {
      for (var speculation in listSpeculation) {
        if (speculation.name!
            .toUpperCase()
            .contains(searchInput.value.text.toUpperCase())) {
          listSpeculationSearch.add(speculation);
        }
      }
    } else {
      listSpeculationSearch = listSpeculation;
    }
    listSpeculationAfficher.value = listSpeculationSearch;
  }

  chooseOrRemoveSpeculation(SpeculationModel speculation) {
    if (listSpeculationChoose.contains(speculation)) {
      listSpeculationChoose.removeWhere((element) => element == speculation);
    } else {
      listSpeculationChoose.add(speculation);
    }
  }

  getUserFavorite() async {
    isloadListFavorite.value = true;
    await getListSpeculation();
    List<int> favorisIds = [];

    listSpeculationFavorite.value = [];
    listFavoriteData.value = [];
    await repositoryImpl.getUserFavorite().then(
          (response) => response.fold(
            (failure) {},
            (favorite) async {
              userfavorites.value = favorite.data;
              await Future.forEach(favorite.data, (FavoriteModel element) {
                favorisIds.add(element.speculationId);
              });
              await Future.forEach(
                speculationController.listSpeculation,
                (SpeculationModel element) {
                  if (favorisIds.contains(element.speculationId)) {
                    listSpeculationFavorite.add(element);

                    listFavoriteData.add(
                      FavoriteData(
                        favorite: userfavorites.firstWhere(
                          (favorite) =>
                              element.speculationId == favorite.speculationId,
                        ),
                        speculation: element,
                      ),
                    );
                  }
                },
              );
            },
          ),
        );
    isloadListFavorite.value = false;
  }

  addFavorites() async {
    Get.dialog(const LoaderComponent());
    List<int> ids = [];
    await Future.forEach(listSpeculationChoose,
        (SpeculationModel element) => ids.add(element.speculationId!));

    await repositoryImpl
        .addFavorite(ids)
        .then((response) => response.fold((failure) {
              showCustomFlushbar(Get.context!, "Echec veuillez réessayer",
                  ColorManager.red, FlushbarPosition.TOP);
              Get.back();
            }, (favorite) async {
              await getUserFavorite();
              await getListSpeculation();
              Get.back();
              showCustomFlushbar(Get.context!, "Produit ajouté au favoris",
                  ColorManager.primary, FlushbarPosition.TOP);
              listSpeculationChoose.value = [];
            }));
  }

  deleteFavorite(int favoriteId) async {
    Get.dialog(
      const LoaderComponent(),
      barrierDismissible: false,
    );
    await repositoryImpl
        .deleteFavorite(favoriteId)
        .then((response) => response.fold((failure) {
              Get.back();
            }, (data) async {
              Get.back();
              showCustomFlushbar(Get.context!, "Retiré", ColorManager.primary,
                  FlushbarPosition.TOP);
              await getUserFavorite();
            }));
  }
}

class FavoriteData {
  FavoriteModel favorite;
  SpeculationModel speculation;
  FavoriteData({required this.favorite, required this.speculation});
}
