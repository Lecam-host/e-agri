import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/app/constant.dart';
import 'package:eagri/app/functions.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/favorite/add_favorite_screen.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

import '../../domain/model/user_model.dart';
import '../ressources/color_manager.dart';
import 'controller/favorite_controller.dart';

class ListFavoriteScreen extends StatefulWidget {
  const ListFavoriteScreen({Key? key}) : super(key: key);

  @override
  State<ListFavoriteScreen> createState() => _ListFavoriteScreenState();
}

class _ListFavoriteScreenState extends State<ListFavoriteScreen> {
  FavoriteController favoriteController = Get.put(FavoriteController());
  @override
  void initState() {
   favoriteController.getListSpeculation();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
        builder: (context, user, child) =>Scaffold(
      floatingActionButton:isInScopes(UserPermissions.FAVORIS_AJOUTER, user.scopes!)? FloatingActionButton(
        backgroundColor: ColorManager.primary,
        onPressed: () {
          Get.to(const AddFavoriteScreen());
        },
        child: Icon(
          Icons.add,
          color: ColorManager.white,
        ),
      ):const SizedBox(),
      appBar: AppBar(
        title: const Text('Mes favoris'),
        leading: const BackButtonCustom(),
      ),
      body: Obx(
        () => favoriteController.isloadListFavorite.value == false
            ? favoriteController.listFavoriteData.isNotEmpty
                ? ListView.builder(
                    shrinkWrap: true,
                    itemCount: favoriteController.listFavoriteData.length,
                    itemBuilder: (context, index) => Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        color: ColorManager.white,
                        child: ListTile(
                          leading: CachedNetworkImage(
                            imageUrl: favoriteController
                                .listSpeculationFavorite[index].imageUrl!,
                            imageBuilder: (context, imageProvider) => Container(
                              height: 80,
                              width: 80,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                image: DecorationImage(
                                  scale: 1,
                                  image: imageProvider,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            placeholder: (context, url) => Container(
                              color: ColorManager.grey1,
                              height: 80,
                              width: 80,
                            ),
                            errorWidget: (context, url, error) =>
                                const Icon(Icons.error),
                          ),
                          title: Text(
                              favoriteController
                                  .listFavoriteData[index].speculation.name!,
                              style: getRegularTextStyle()),
                          trailing: IconButton(
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (context) {
                                    return AlertDialog(
                                      backgroundColor: Colors.white,
                                      contentPadding: const EdgeInsets.all(12),
                                      content: const Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Text(
                                            'Voulez-vous retirer ce produit de vos favoris',
                                            textAlign: TextAlign.center,
                                          ),
                                        ],
                                      ),
                                      title: const Icon(Icons.delete,
                                          color: Colors.red, size: 40),
                                      actions: [
                                        TextButton(
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                          child: const Text('Annuler',
                                              style: TextStyle(
                                                  color: Colors.grey)),
                                        ),
                                        TextButton(
                                          onPressed: () async {
                                            Navigator.pop(context);
                                            favoriteController.deleteFavorite(
                                                favoriteController
                                                    .listFavoriteData[index]
                                                    .favorite
                                                    .idFavorite);
                                          },
                                          child: const Text('Rétirer',
                                              style:
                                                  TextStyle(color: Colors.red)),
                                        ),
                                      ],
                                    );
                                  });
                            },
                            icon: Icon(
                              Icons.delete,
                              color: ColorManager.red,
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                : const EmptyComponenent(
                    message: "Votre liste de favoris est vide",
                  )
            : const LoaderComponent(),
      ),
    ));
  }
}
