import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/favorite/controller/favorite_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/instance_manager.dart';

import '../ressources/color_manager.dart';
import '../ressources/strings_manager.dart';
import '../ressources/styles_manager.dart';

class AddFavoriteScreen extends StatefulWidget {
  const AddFavoriteScreen({Key? key}) : super(key: key);

  @override
  State<AddFavoriteScreen> createState() => _AddFavoriteScreenState();
}

class _AddFavoriteScreenState extends State<AddFavoriteScreen> {
  FavoriteController favoriteController = Get.put(FavoriteController());
  final _focusNode = FocusNode();
  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Obx(
        () => favoriteController.listSpeculationChoose.isNotEmpty
            ? ElevatedButton.icon(
                onPressed: () {
                  favoriteController.addFavorites();
                },
                icon: Icon(
                  Icons.save,
                  color: ColorManager.white,
                ),
                label: const Text("Enregistrer"))
            : const SizedBox(),
      ),
      appBar: AppBar(
        title: Text(
          "Dites-nous les produits qui vous interesse",
          style: getBoldTextStyle(color: ColorManager.black),
        ),
        leading: const BackButtonCustom(),
      ),
      body: Obx(() => Column(
            children: [
              Container(
                margin: const EdgeInsets.all(10),
                child: TextField(
                  focusNode: _focusNode,
                  onChanged: (value) {
                    favoriteController.searchSpeculation();
                  },
                  controller: favoriteController.searchInput.value,
                  decoration: InputDecoration(
                    suffixIcon: Icon(
                      Icons.search,
                      color: ColorManager.grey,
                    ),
                    filled: true,
                    fillColor: Colors.grey.withOpacity(0.2),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15.0),
                      borderSide:
                          const BorderSide(width: 0, style: BorderStyle.none),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15.0),
                      borderSide:
                          const BorderSide(width: 0, style: BorderStyle.none),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15.0),
                      borderSide:
                          const BorderSide(width: 0, style: BorderStyle.none),
                    ),
                    hintText: AppStrings.searchSomething,
                  ),
                ),
              ),
              Expanded(
                child: GridView.builder(
                  itemCount: favoriteController.listSpeculationAfficher.length,
                  itemBuilder: (BuildContext context, int index) => InkWell(
                    onTap: () {
                      favoriteController.chooseOrRemoveSpeculation(
                          favoriteController.listSpeculationAfficher[index]);
                    },
                    splashColor: ColorManager.primary,
                    child: Stack(
                      children: [
                        CachedNetworkImage(
                          imageUrl: favoriteController
                              .listSpeculationAfficher[index].imageUrl!,
                          imageBuilder: (context, imageProvider) => Container(
                            height: 80,
                            decoration: BoxDecoration(
                              borderRadius: const BorderRadius.only(
                                topLeft: Radius.circular(5),
                                topRight: Radius.circular(5),
                              ),
                              image: DecorationImage(
                                scale: 1,
                                image: imageProvider,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          placeholder: (context, url) => Container(
                            color: ColorManager.grey1,
                            width: MediaQuery.of(context).size.width,
                          ),
                          errorWidget: (context, url, error) =>
                              const Icon(Icons.error),
                        ),
                        Obx(
                          () => Positioned(
                              right: -10,
                              top: -10,
                              child: Checkbox(
                                activeColor: ColorManager.black,
                                onChanged: (value) {
                                  favoriteController.chooseOrRemoveSpeculation(
                                      favoriteController
                                          .listSpeculationAfficher[index]);
                                },
                                value: favoriteController.listSpeculationChoose
                                    .contains(favoriteController
                                        .listSpeculationAfficher[index]),
                              )),
                        ),
                        Positioned(
                          left: 0,
                          bottom: 0,
                          child: Text(
                            favoriteController
                                .listSpeculationAfficher[index].name
                                .toString(),
                            overflow: TextOverflow.ellipsis,
                            style: getRegularTextStyle(
                              color: Colors.black,
                              fontSize: 10,
                            ),
                            maxLines: 2,
                          ),
                        ),
                      ],
                    ),
                  ),
                  //return Image.asset(images[index], fit: BoxFit.cover);

                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      mainAxisSpacing: 15,
                      crossAxisSpacing: 10,
                      mainAxisExtent: 110),
                  padding: const EdgeInsets.all(10),
                ),
              ),
            ],
          )),
    );
  }
}
