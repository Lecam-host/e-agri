import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/default_button.dart';
import 'package:eagri/presentations/common/state/success_component.dart';
import 'package:eagri/presentations/home/controllers/home_controller.dart';
import 'package:eagri/presentations/home/home_screen.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

import '../../app/constant.dart';
import '../../app/functions.dart';
import '../../domain/model/paiement_model.dart';
import '../../domain/model/user_model.dart';
import '../ressources/assets_manager.dart';
import 'payment_facture.dart';

class AfterPaymentAction extends StatefulWidget {
  const AfterPaymentAction({
    Key? key,
    required this.verificationPaymentResponse,
    required this.datePayment,
  }) : super(key: key);
  final VerificationPaymentResponse verificationPaymentResponse;
  final DateTime datePayment;
  @override
  State<AfterPaymentAction> createState() => _AfterPaymentActionState();
}

class _AfterPaymentActionState extends State<AfterPaymentAction> {
  HomeController homeController = Get.put(HomeController());
  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
        builder: (context, user, child) => Scaffold(
      appBar: AppBar(leading: BackButtonCustom(
        onPressed: () {
          homeController.pageHomeIndex.value = 2;
          Get.offAll(const HomeScreen());
        },
      )),
      body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            if (widget.verificationPaymentResponse.data.status ==
                "REFUSED") ...[
              Text(
                'Echec du payement',
                style: getRegularTextStyle(
                    color: ColorManager.black, fontSize: 20),
              ),
              const SizedBox(height: 20),
              Center(
                child: Lottie.asset(
                  JsonAssets.error,
                  width: 150,
                ),
              ),
            ] else ...[
              const SuccessComponenent(
                message: "Payement validé",
              ),
              Center(
                child: Container(
                  margin: const EdgeInsets.all(10),
                  child: Text.rich(
                    TextSpan(
                      text: "Montant : ",
                      style: getRegularTextStyle(
                        color: ColorManager.black,
                      ),
                      children: [
                        TextSpan(
                            text:
                                "${separateur(widget.verificationPaymentResponse.data.amount.toDouble())} FCFA",
                            style: getBoldTextStyle(
                                fontSize: 20, color: ColorManager.red)),
                      ],
                    ),
                  ),
                ),
              ),
              cardElement(
                  "Numéro", widget.verificationPaymentResponse.data.phone),
              cardElement("Date", convertDate(widget.datePayment.toString())),
              cardElement("Heure",
                  "${widget.datePayment.hour} H ${widget.datePayment.minute}"),
              const SizedBox(
                height: 10,
              ),
               if (
                            isInScopes(UserPermissions.COMMANDE_VOIR_FACTURE,
                                user.scopes!))
              Container(
                margin: const EdgeInsets.all(10),
                child: DefaultButton(
                  color: ColorManager.blue,
                  text: "Voir la facture",
                  press: () {
                    Get.to(PaymentFacture(
                        factureUrl: widget
                            .verificationPaymentResponse.data.invoiceUrl));
                  },
                ),
              ),
            ],
          ]),
    ));
  }

  Container cardElement(String titre, String data) {
    return Container(
      margin: const EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            titre,
            style: getRegularTextStyle(color: ColorManager.grey, fontSize: 14),
          ),
          Text(data,
              style:
                  getSemiBoldTextStyle(fontSize: 16, color: ColorManager.black))
        ],
      ),
    );
  }
}
