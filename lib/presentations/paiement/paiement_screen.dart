import 'package:eagri/app/constant.dart';
import 'package:eagri/presentations/paiement/controller/paiement_controller.dart';
import 'package:eagri/presentations/ressources/icon_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:pinput/pinput.dart';
import '../../domain/model/paiement_model.dart';
import '../cart/controller/cart_controller.dart';
import '../common/buttons/back_button.dart';
import '../ressources/assets_manager.dart';
import '../ressources/color_manager.dart';

class PaiementScreen extends StatefulWidget {
  const PaiementScreen({
    Key? key,
    required this.reseau,
    required this.createPaiementResponse,
  }) : super(key: key);
  final Reseau reseau;
  final CreatePaiementResponse createPaiementResponse;

  @override
  State<PaiementScreen> createState() => _PaiementScreenState();
}

PaiementController paiementController = Get.put(PaiementController());
final defaultPinTheme = PinTheme(
  width: 56,
  height: 56,
  textStyle: const TextStyle(
      fontSize: 20,
      color: Color.fromRGBO(30, 60, 87, 1),
      fontWeight: FontWeight.w600),
  decoration: BoxDecoration(
    border: Border.all(color: const Color.fromRGBO(0, 0, 0, 1)),
    borderRadius: BorderRadius.circular(20),
  ),
);

class _PaiementScreenState extends State<PaiementScreen> {
  final focusedPinTheme = defaultPinTheme.copyDecorationWith(
    border: Border.all(color: const Color.fromRGBO(114, 178, 238, 1)),
    borderRadius: BorderRadius.circular(8),
  );

  final submittedPinTheme = defaultPinTheme.copyWith(
    decoration: defaultPinTheme.decoration!.copyWith(
      color: const Color.fromRGBO(234, 239, 243, 1),
    ),
  );

  CartController cartController = Get.put(CartController());
  PaiementController paiementController = Get.put(PaiementController());

  @override
  void initState() {
    paiementController.getUserCommand();
    if (widget.reseau != Reseau.orange) {
      paiementController.confirmPaiement(
          widget.createPaiementResponse.data!.signature ?? "",
          widget.createPaiementResponse.data!.otp ?? "");
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButtonCustom(),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Container(
              margin: const EdgeInsets.all(10),
              child: Center(
                child: Text(
                  widget.createPaiementResponse.data!.instruction ?? "",
                  style: getSemiBoldTextStyle(
                      color: ColorManager.black, fontSize: 15),
                ),
              ),
            ),
          ),
          if (widget.reseau == Reseau.orange) ...[
            Center(
              child: Lottie.asset(
                JsonAssets.waitingPassword,
                width: 200,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Center(
              child: Pinput(
                  autofocus: true,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  defaultPinTheme: defaultPinTheme,
                  focusedPinTheme: focusedPinTheme,
                  submittedPinTheme: submittedPinTheme,
                  pinputAutovalidateMode: PinputAutovalidateMode.onSubmit,
                  showCursor: true,
                  onCompleted: (pin) {
                    if (widget.reseau == Reseau.orange) {
                      paiementController.confirmPaiement(
                          widget.createPaiementResponse.data!.signature ?? "",
                          pin.toString());
                    }
                  }),
            ),
          ],
          if (widget.reseau != Reseau.orange) ...[
            Center(
              child: Lottie.asset(
                JsonAssets.waitingPaiement,
                width: 150,
              ),
            ),
          ],
          // DefaultButton(
          //   text: "Annuler",
          //   press: () {
          //     paiementController.endVerify.value = true;
          //   },
          // ),
        ],
      ),
    );
  }
}

class ListTileCustom extends StatelessWidget {
  const ListTileCustom({
    Key? key,
    required this.titre,
    required this.icon,
  }) : super(key: key);
  final String titre;
  final IconData icon;
  @override
  Widget build(BuildContext context) {
    return ListTile(
      tileColor: ColorManager.white,
      leading: Container(
        width: 30.0,
        height: 30.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: ColorManager.primary.withOpacity(0.1),
        ),
        child: Icon(
          icon,
          color: ColorManager.primary,
        ),
      ),
      title: Text(titre),
      trailing: const Icon(IconManager.arrowRight),
      onTap: () {},
    );
  }
}
