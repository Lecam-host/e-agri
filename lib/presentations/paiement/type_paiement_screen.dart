import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/app/constant.dart';
import 'package:eagri/presentations/command/controller/command_controller.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/ressources/icon_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../app/di.dart';
import '../../app/functions.dart';
import '../../data/request/paiement_request.dart';
import '../../domain/model/command_model.dart';
import '../../domain/usecase/paiement_usecase.dart';
import '../common/default_button.dart';
import '../ressources/color_manager.dart';

import '../ressources/styles_manager.dart';
import 'after_resquest_payment_cash.dart';
import 'controller/paiement_controller.dart';

class TypePaiementScreen extends StatefulWidget {
  const TypePaiementScreen({super.key, required this.commandData});
  final CommandModel commandData;
  @override
  State<TypePaiementScreen> createState() => _TypePaiementScreenState();
}

class _TypePaiementScreenState extends State<TypePaiementScreen> {
  PaiementController paiementController = Get.put(PaiementController());
  CommandCrontroller commandCrontroller = Get.find();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    paiementController.fetchMethodPaiement();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => ListView.builder(
        shrinkWrap: true,
        itemCount: paiementController.listMethodPaiement.length,
        itemBuilder: (context, index) => Padding(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: ListTile(
            trailing: const Icon(IconManager.arrowRight),
            onTap: () async {
              paiementController.methodPayementId.value =
                  paiementController.listMethodPaiement[index].id!;
              if (paiementController.listMethodPaiement[index].code ==
                  codePaiementCash) {
                PaiementUsecase paiementUsecase = instance<PaiementUsecase>();
                Get.dialog(const LoaderComponent());
                await paiementUsecase
                    .createPaiement(
                      CreatePaiementRequest(
                          methodId:
                              paiementController.listMethodPaiement[index].id!,
                          orderId: widget.commandData.id,
                          phone: null),
                    )
                    .then((response) => response.fold((failure) {
                          Get.back();
                          showCustomFlushbar(
                              context,
                              failure.message.toString(),
                              ColorManager.error,
                              FlushbarPosition.TOP,
                              duration: 5);
                        }, (r) {
                          commandCrontroller.getUserCommand();
                          Get.back();
                          Get.back();

                          Get.back();
                          Get.back();
                          Get.to(AfterRequestPaymentCash(
                            message: r.data!.instruction ?? "",
                          ));
                        }));
              } else if (paiementController.listMethodPaiement[index].code ==
                  codePaiementOnligne) {
                numberInput();
                // Get.bottomSheet(
                //   Container(
                //     height: 180,
                //     decoration: BoxDecoration(
                //       borderRadius: const BorderRadius.only(
                //         topLeft: Radius.circular(10),
                //         topRight: Radius.circular(10),
                //       ),
                //       color: ColorManager.white,
                //     ),
                //     child: Column(
                //       children: [
                //         const SizedBox(
                //           height: 10,
                //         ),
                //         Text(
                //           "Choisir un moyen de paiement",
                //           style: getBoldTextStyle(
                //               color: ColorManager.black, fontSize: 14),
                //         ),
                //         const SizedBox(
                //           height: 10,
                //         ),
                //         Row(
                //           crossAxisAlignment: CrossAxisAlignment.center,
                //           mainAxisAlignment: MainAxisAlignment.center,
                //           children: [
                //             paiementCard("Orange", Reseau.orange),
                //             const SizedBox(
                //               width: 10,
                //             ),
                //             paiementCard("Moov", Reseau.moov),
                //             const SizedBox(
                //               width: 10,
                //             ),
                //             paiementCard("MTN", Reseau.mtn),
                //           ],
                //         )
                //       ],
                //     ),
                //   ),
                // );
              } else {
                showCustomFlushbar(context, "Bientôt disponible",
                    ColorManager.blue, FlushbarPosition.TOP,
                    duration: 5);
              }
            },
            title: Text(
              paiementController.listMethodPaiement[index].name!,
            ),
          ),
        ),
      ),
    );
  }

  numberInput() {
    Get.bottomSheet(
      Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Entrer votre numéro de téléphone",
              style: getBoldTextStyle(color: ColorManager.black, fontSize: 14),
            ),
            const SizedBox(
              height: 10,
            ),
            Form(
              key: _formKey,
              child: TextFormField(
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Entrer votre numero';
                  } else if (valideNumber(
                        value,
                      ) ==
                      false) {
                    return 'Numero invalide';
                  } else {
                    return null;
                  }
                },
                controller: paiementController.phoneController.value,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.grey.withOpacity(0.2),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide:
                        const BorderSide(width: 0, style: BorderStyle.none),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide:
                        const BorderSide(width: 0, style: BorderStyle.none),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide:
                        const BorderSide(width: 0, style: BorderStyle.none),
                  ),
                  hintText: "Entrer votre numéro",
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: 150,
              child: DefaultButton(
                text: "Suivant >",
                press: () async {
                  if (_formKey.currentState!.validate()) {
                    Get.back();

                    await paiementController.createPaiement(
                        determinateReseau(
                            paiementController.phoneController.value.text),
                        widget.commandData.id);
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  InkWell paiementCard(String titre, Reseau typeReseau) {
    return InkWell(
      onTap: () {
        Get.back();
        numberInput();
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const CircleAvatar(
            radius: 40,
            child: Icon(
              Icons.payment,
            ),
          ),
          Text(titre)
        ],
      ),
    );
  }
}
