import 'dart:developer';

import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/data/repository/api/repository_impl.dart';
import 'package:eagri/data/request/paiement_request.dart';
import 'package:eagri/domain/model/paiement_model.dart';
import 'package:eagri/domain/usecase/paiement_usecase.dart';
import 'package:eagri/presentations/command/controller/command_controller.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/common/state/succes_page_view.dart';
import 'package:eagri/presentations/paiement/paiement_screen.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../app/constant.dart';
import '../../../app/di.dart';
import '../../../data/request/request.dart';
import '../../../domain/model/methode_paiement_model.dart';
import '../../../domain/model/user_model.dart';
import '../../common/state/loader_component.dart';
import '../after_payment.dart';

class PaiementController extends GetxController {
  PaiementUsecase paiementUsecase = instance<PaiementUsecase>();
  CommandCrontroller commandCrontroller = CommandCrontroller();
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  var listMethodPaiement = <PaymentMethodModel>[].obs;
  var listDemandeRecu = <DemandePaiementCashItem>[].obs;
  var listDemandeEnvoye = <DemandePaiementCashItem>[].obs;

  var loadDemandeEnvoye = true.obs;
  var loadDemandeRecu = true.obs;

  var endVerify = false.obs;
  var signature = "".obs;
  var phoneController = TextEditingController().obs;
  var methodPayementId = 0.obs;
  var orderId = 0.obs;
  var isLoadPaiment = false.obs;
  @override
  onInit() {
    fetchMethodPaiement();
    super.onInit();
  }

  fetchMethodPaiement() async {
    (await repositoryImpl.getMethodPaiementAll()).fold((l) => log(l.toString()),
        (r) => listMethodPaiement.value = r.data!.items!);
  }

  Future<bool> acceptDemande(int orderId, int productId) async {
    Get.dialog(const LoaderComponent());
    bool result = true;
    await repositoryImpl
        .acceptPaiementCash(orderId, productId, true, null)
        .then((response) async {
      await response.fold((failure) async {
        Get.back();
        showCustomFlushbar(Get.context!, failure.message, ColorManager.error,
            FlushbarPosition.TOP);
        result = false;
      }, (data) {
        Get.back();
        result = true;
        // getListDemandePaimentCashEnvoyer();
        getListDemandePaimentCashRecu();
        Get.off(const SuccessPageView(
          message: "Demande acceptée",
        ));
      });
    });
    return result;
  }

  Future<bool> confirmPaiementCash(int demandeId, String code) async {
    Get.dialog(const LoaderComponent());
    bool result = true;
    await repositoryImpl
        .confirmPaiementCash(
      demandeId,
      code,
    )
        .then((response) async {
      await response.fold((failure) async {
        result = false;
        Get.back();
        showCustomFlushbar(Get.context!, failure.message, ColorManager.error,
            FlushbarPosition.TOP);
      }, (data) async {
        result = true;
        Get.back();
        getListDemandePaimentCashEnvoyer();
        // getListDemandePaimentCashRecu();
        Get.off(const SuccessPageView(
          message: "Paiement confirmé",
        ));
      });
    });
    return result;
  }

  getUserCommand() {
    commandCrontroller.getUserCommand();
  }

  verifyPaiement(String signature) {
    while (endVerify.value == false) {
      paiementUsecase
          .verificationPayment(signature)
          .then((value) => value.fold((l) => null, (r) => null));
    }
  }

  init() {
    getUserCommand();
  }

  getListDemandePaimentCashEnvoyer() async {
    loadDemandeEnvoye = true.obs;
    await repositoryImpl
        .getListDemandePaimentCash("BUYER")
        .then((response) async {
      await response.fold((l) {
        loadDemandeEnvoye.value = false;
      }, (r) async {
        listDemandeEnvoye.value = [];
        await Future.forEach(r.items, (DemandePaiementCashItem element) async {
          late DemandePaiementCashItem result;
          UserModel vendeur = UserModel();
          await repositoryImpl
              .getInfoById(
            InfoByIdRequest(userId: element.sellerId),
          )
              .then((response) async {
            await response.fold((l) {}, (userData) async {
              if (userData.user != null) {
                vendeur = UserModel(
                  firstName: userData.user!.firstname,
                  lastName: userData.user!.laststname,
                  number: userData.user!.phone,
                );
              }
            });
          });
          result = element;
          result.vendeur = vendeur;

          result.isReceive = false;
          loadDemandeEnvoye.value = false;

          listDemandeEnvoye.add(result);
        });
         loadDemandeEnvoye.value = false;
      });
    });
  }

  getListDemandePaimentCashRecu() async {
    loadDemandeRecu = true.obs;

    await repositoryImpl
        .getListDemandePaimentCash("SELLER")
        .then((response) async {
      await response.fold((l) {
        loadDemandeRecu.value = false;
      }, (r) async {
        listDemandeRecu.value = [];
        await Future.forEach(r.items, (DemandePaiementCashItem element) async {
          DemandePaiementCashItem? result;

          UserModel acheteur = UserModel();
          await repositoryImpl
              .getInfoById(
            InfoByIdRequest(userId: element.buyerId),
          )
              .then((response) async {
            await response.fold((l) {}, (userData) async {
              if (userData.user != null) {
                acheteur = UserModel(
                  firstName: userData.user!.firstname,
                  lastName: userData.user!.laststname,
                  number: userData.user!.phone,
                );
              }
            });
          });
          result = element;

          result.acheteur = acheteur;
          result.isReceive = true;

          listDemandeRecu.add(result);
        });
        loadDemandeRecu.value = false;
      });
    });
  }

  confirmPaiement(String signature, String otp) async {
    isLoadPaiment.value = false;
    endVerify.value = false;
    await paiementUsecase
        .confirPaiement(signature, otp)
        .then((value) => value.fold((failure) {
          showCustomFlushbar(Get.context!, failure.message, ColorManager.error, FlushbarPosition.TOP);
        }, (r) async {
              while (endVerify.value == false) {
                await paiementUsecase
                    .verificationPayment(signature)
                    .then((value) => value.fold((l) {
                          inspect(l);
                        }, (r) {
                          if (r.data.message !=
                              "WAITING_CUSTOMER_TO_VALIDATE") {
                            endVerify.value = true;

                            Get.off(AfterPaymentAction(
                              verificationPaymentResponse: r,
                              datePayment: DateTime.now(),
                            ));
                          }
                        }));
              }
            }));
  }

  createPaiement(Reseau typeReseau, int idCommand) async {
    Get.dialog(const LoaderComponent());
    await paiementUsecase
        .createPaiement(
          CreatePaiementRequest(
              methodId: methodPayementId.value,
              orderId: idCommand,
              phone: phoneController.value.text),
        )
        .then(
          (value) => value.fold((l) {
            Get.back();
            showCustomFlushbar(Get.context!, l.message, ColorManager.error,
                FlushbarPosition.TOP);
          }, (r) async {
            Get.back();
            Get.back();
            Get.to(PaiementScreen(
              reseau: typeReseau,
              createPaiementResponse: r,
            ));
            // await paiementUsecase.confirPaiement(r.data.signature, r.data.otp);
          }),
        );
  }
}
