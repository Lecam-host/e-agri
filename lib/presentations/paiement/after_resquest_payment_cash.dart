import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/state/success_component.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';

class AfterRequestPaymentCash extends StatefulWidget {
  const AfterRequestPaymentCash({super.key, required this.message});
  final String message;
  @override
  State<AfterRequestPaymentCash> createState() =>
      _AfterRequestPaymentCashState();
}

class _AfterRequestPaymentCashState extends State<AfterRequestPaymentCash> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButtonCustom(),
      ),
      body: Container(
        margin: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
             widget.message,
              style: getBoldTextStyle(fontSize: 17),
              textAlign: TextAlign.center,
            ),
            const SuccessComponenent(),
            Text(
              widget.message,
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }
}
