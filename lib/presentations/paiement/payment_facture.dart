import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:flutter/material.dart';

import '../common/pdf_screen.dart';

class PaymentFacture extends StatefulWidget {
  const PaymentFacture({Key? key, required this.factureUrl}) : super(key: key);

  final String factureUrl;
  @override
  State<PaymentFacture> createState() => _PaymentFactureState();
}

class _PaymentFactureState extends State<PaymentFacture> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: const BackButtonCustom(),
        ),
        body: PdfScreenn(pdfLink: widget.factureUrl));
  }
}
