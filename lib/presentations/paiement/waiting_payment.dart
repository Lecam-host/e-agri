import 'package:eagri/presentations/paiement/controller/paiement_controller.dart';
import 'package:eagri/presentations/ressources/icon_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import '../../domain/model/paiement_model.dart';
import '../common/buttons/back_button.dart';
import '../ressources/assets_manager.dart';
import '../ressources/color_manager.dart';

class WaitingPaiement extends StatefulWidget {
  const WaitingPaiement({
    Key? key,
    required this.createPaiementResponse,
  }) : super(key: key);
  final CreatePaiementResponse createPaiementResponse;

  @override
  State<WaitingPaiement> createState() => _WaitingPaiementState();
}

class _WaitingPaiementState extends State<WaitingPaiement> {
  PaiementController paiementController = Get.put(PaiementController());

  @override
  void initState() {
    paiementController
        .verifyPaiement(widget.createPaiementResponse.data!.signature ?? "");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButtonCustom(),
      ),
      body: Center(
        child: Lottie.asset(
          JsonAssets.waitingPaiement,
          width: 150,
        ),
      ),
    );
  }
}

class ListTileCustom extends StatelessWidget {
  const ListTileCustom({
    Key? key,
    required this.titre,
    required this.icon,
  }) : super(key: key);
  final String titre;
  final IconData icon;
  @override
  Widget build(BuildContext context) {
    return ListTile(
      tileColor: ColorManager.white,
      leading: Container(
        width: 30.0,
        height: 30.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: ColorManager.primary.withOpacity(0.1),
        ),
        child: Icon(
          icon,
          color: ColorManager.primary,
        ),
      ),
      title: Text(titre),
      trailing: const Icon(IconManager.arrowRight),
      onTap: () {},
    );
  }
}
