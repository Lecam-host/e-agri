import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import '../common/buttons/back_button.dart';
import '../conseils/conseils_view.dart';
import '../ressources/assets_manager.dart';
import '../ressources/color_manager.dart';
import '../ressources/font_manager.dart';
import '../ressources/icon_manager.dart';
import '../ressources/styles_manager.dart';
import '../ressources/values_manager.dart';

class RecommandationView extends StatefulWidget {
  const RecommandationView({Key? key}) : super(key: key);

  @override
  State<RecommandationView> createState() => _RecommandationViewState();
}

class _RecommandationViewState extends State<RecommandationView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: const BackButtonCustom(),
          title: const Text("Recommandation"),
        ),
        body: Container(
          margin:
              const EdgeInsets.only(right: AppMargin.m10, left: AppMargin.m10),
          child: Column(
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    PageTransition(
                      type: PageTransitionType.rightToLeft,
                      child: const ConseilView(),
                      isIos: true,
                      duration: const Duration(milliseconds: 400),
                    ),
                  );
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: ColorManager.white,
                        image: DecorationImage(
                          image: AssetImage(ImageAssets.connexionBg),
                          fit: BoxFit.cover,
                          colorFilter: ColorFilter.mode(
                            Colors.black.withOpacity(0.1),
                            BlendMode.multiply,
                          ),
                        ),
                      ),
                      height: 150,
                      width: double.infinity,
                      child: Icon(
                        IconManager.play,
                        size: AppSize.s60,
                        color: ColorManager.red,
                      ),
                    ),
                    Row(
                      children: [
                        Text(
                          "Arroser vos plantes",
                          style: getSemiBoldTextStyle(
                              color: ColorManager.black,
                              fontSize: FontSize.s14),
                        ),
                        const Spacer(),
                        Text(
                          "30:00",
                          style: getRegularTextStyle(
                              color: ColorManager.black,
                              fontSize: FontSize.s12),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Text(
                      "par ANADER",
                      style: getLightTextStyle(
                          color: ColorManager.grey, fontSize: FontSize.s12),
                    ),
                    Row(
                      children: [
                        Text(
                          "02-Janvier-2022",
                          style: getLightTextStyle(
                              color: ColorManager.grey, fontSize: FontSize.s12),
                        ),
                        const Spacer(),
                        Text(
                          "323 vues",
                          style: getLightTextStyle(
                              color: ColorManager.grey, fontSize: FontSize.s12),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
