import 'package:eagri/domain/model/weather_model.dart';
import 'package:eagri/domain/usecase/weather_usecase.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';

import '../../app/di.dart';
import '../../app/functions.dart';
import '../ressources/icon_manager.dart';

class DetailsParcelView extends StatefulWidget {
  const DetailsParcelView({Key? key, required this.parcelInfo})
      : super(key: key);
  final ParcelInfoModel parcelInfo;

  @override
  State<DetailsParcelView> createState() => _DetailsParcelViewState();
}

class _DetailsParcelViewState extends State<DetailsParcelView> {
  WeatherUseCase weatherUseCase = instance<WeatherUseCase>();
  ParcelInfoModel? parcelTempInfo;
  getParcelleTempInfo() {
    weatherUseCase.getParcelInfo(widget.parcelInfo.parcelId!).then((value) {
      value.fold((l) => null, (r) {
        setState(() {
          parcelTempInfo = r;
        });
      });
    });
  }

  @override
  void initState() {
    getParcelleTempInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButtonCustom(),
        title: const Text('Détails de la parcelle'),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.all(5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Nomination',
              ),
              Card(
                color: ColorManager.grey.withOpacity(0.1),
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(11)),
                child: Container(
                  margin: const EdgeInsets.all(10),
                  width: double.maxFinite,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.parcelInfo.name.toString(),
                        style: getBoldTextStyle(
                          color: ColorManager.black,
                          fontSize: 14,
                        ),
                      ),
                      Text(
                        "Enregistré le : ${convertDateWithTime(widget.parcelInfo.createdAt!)}",
                        style: getRegularTextStyle(
                          color: ColorManager.grey,
                          fontSize: 14,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              const Text("Info supplementaire"),
              if (parcelTempInfo != null) tempParcelWidget(),
            ],
          ),
        ),
      ),
    );
  }

  Card tempParcelWidget() {
    return Card(
      color: ColorManager.grey.withOpacity(0.1),
      elevation: 0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(11)),
      child: SizedBox(
        width: double.maxFinite,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            customListTile(
              first: 'Humidité du sol:',
              second: ' ${parcelTempInfo!.moisture! * 100}°c',
              icon: Icons.air,
              iconColor: Colors.blue,
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              margin: const EdgeInsets.only(left: 5),
              child: Text(
                'Température',
                style: getBoldTextStyle(
                  color: ColorManager.black,
                  fontSize: 14,
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            customListTile(
              first: 'A 10 cm:',
              second: ' ${parcelTempInfo!.t10!.round()}°c',
              icon: IconManager.temperature,
              iconColor: Colors.red,
            ),
            customListTile(
              first: 'A la surface:',
              second: ' ${parcelTempInfo!.t0!.round()}°c',
              icon: IconManager.temperature,
              iconColor: Colors.red,
            ),
          ],
        ),
      ),
    );
  }
}

Widget customListTile({
  required String first,
  required String second,
  required IconData icon,
  required Color iconColor,
  String text = '',
}) {
  return ListTile(
    trailing: Text(
      text,
      style: getRegularTextStyle(color: ColorManager.darkGrey, fontSize: 16),
    ),
    leading: Icon(icon, color: iconColor),
    title: RichText(
      maxLines: 1,
      text: TextSpan(
        children: [
          TextSpan(
            text: first,
            style: getRegularTextStyle(color: ColorManager.grey, fontSize: 16),
          ),
          TextSpan(
            text: second,
            style: getRegularTextStyle(color: ColorManager.black, fontSize: 16),
          ),
        ],
      ),
    ),
  );
}
