import 'package:flutter/material.dart';

class MessageAfterCreateParcel extends StatefulWidget {
  const MessageAfterCreateParcel({Key? key}) : super(key: key);

  @override
  State<MessageAfterCreateParcel> createState() =>
      _MessageAfterCreateParcelState();
}

class _MessageAfterCreateParcelState extends State<MessageAfterCreateParcel> {
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Column(),
    );
  }
}
