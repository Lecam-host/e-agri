import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

import '../../app/di.dart';
import '../../data/request/request.dart';
import '../../domain/model/weather_model.dart';
import '../../domain/usecase/weather_usecase.dart';
import 'components/history_weather_component.dart';

class HistoryWeatherView extends StatefulWidget {
  const HistoryWeatherView({
    Key? key,
    required this.request,
    required this.cityName,
  }) : super(key: key);
  final HistoryWeatherRequest request;
  final String cityName;

  @override
  State<HistoryWeatherView> createState() => _HistoryWeatherViewState();
}

class _HistoryWeatherViewState extends State<HistoryWeatherView> {
  WeatherUseCase weatherUseCase = instance<WeatherUseCase>();
  HistoryWeatherRequest request = HistoryWeatherRequest(
      dateDeDebut: "2023-01-01", dateDeDeFin: "2023-02-02");
  List<HistoryWeatherModel> listHistory = [];
  // determinePosition() async {
  //   bool serviceEnabled;
  //   LocationPermission permission;

  //   serviceEnabled = await Geolocator.isLocationServiceEnabled();
  //   if (!serviceEnabled) {
  //     //return Future.error('Location services are disabled.');
  //   }

  //   permission = await Geolocator.checkPermission();
  //   if (permission == LocationPermission.denied) {
  //     permission = await Geolocator.requestPermission();
  //     if (permission == LocationPermission.denied) {
  //       return Future.error('Location permissions are denied');
  //     }
  //   }

  //   if (permission == LocationPermission.deniedForever) {
  //     return Future.error(
  //         'Location permissions are permanently denied, we cannot request permissions.');
  //   }

  //   await Geolocator.getCurrentPosition().then((value) {});
  // }
  List<HistoryWeatherModel> listHistoryWeather = [];
  getHistory() {
    weatherUseCase.getHistory(widget.request).then((value) {
      value.fold((l) {}, (r) {
        setState(() {
          listHistoryWeather = r.listHistory!;
        });
      });
    });
  }

  late bool isShowingMainData;
  @override
  void initState() {
    getHistory();
    // isShowingMainData = true;
    // determinePosition();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButtonCustom(),
        title: const Text('Historique'),
      ),
      body: Column(
        children: [
          Text(widget.cityName),
          Expanded(
              child: ListView.builder(
            itemCount: listHistoryWeather.length,
            itemBuilder: (BuildContext context, int index) {
              return AnimationConfiguration.staggeredList(
                position: index,
                duration: const Duration(milliseconds: 100),
                child: SlideAnimation(
                  verticalOffset: 50.0,
                  child: FadeInAnimation(
                    child: HistoryWeatherComponenet(
                      historyWeather: listHistoryWeather[index],
                    ),
                  ),
                ),
              );
            },
          )),
        ],
      ),
    );
  }
}
