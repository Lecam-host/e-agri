import 'dart:developer';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/app/app_prefs.dart';
import 'package:eagri/app/functions.dart';
import 'package:eagri/domain/model/advice_model.dart';
import 'package:eagri/domain/usecase/weather_usecase.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/date_field.dart';
import 'package:eagri/presentations/ressources/assets_manager.dart';
import 'package:eagri/presentations/ressources/icon_manager.dart';
import 'package:eagri/presentations/ressources/size_config.dart';
import 'package:eagri/presentations/ressources/strings_manager.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import '../../app/constant.dart';
import '../../app/di.dart';
import '../../data/request/request.dart';
import '../../domain/model/weather_model.dart';
import '../ressources/color_manager.dart';
import '../ressources/styles_manager.dart';
import 'history_weather_view.dart';
import 'list_location_meteo.dart';
import 'mes_parcelles_view.dart';
import 'weather_view_model.dart';

class DetailsMeteoView extends StatefulWidget {
  const DetailsMeteoView({Key? key, this.cityWeather, this.lat, this.long})
      : super(key: key);
  final CityWeatherModel? cityWeather;
  final String? long;
  final String? lat;

  @override
  State<DetailsMeteoView> createState() => _DetailsMeteoViewState();
}

Color cardColor = ColorManager.black.withOpacity(0.4);
Color titleColor = ColorManager.white;

class _DetailsMeteoViewState extends State<DetailsMeteoView> {
  WeatherViewModel weatherViewModel = instance<WeatherViewModel>();
  GetWeatherRequest request = GetWeatherRequest(0, 0);
  WeatherUseCase weatherUseCase = instance<WeatherUseCase>();
  AppSharedPreference appSharedPreference = instance<AppSharedPreference>();
  CityWeatherModel? cityWeatherShow;
  List<RecommandationModel> listRecommandationModel = [];
  getWeather() async {
     
    if (widget.cityWeather == null) {
      AppSharedPreference appSharedPreference = instance<AppSharedPreference>();
      await appSharedPreference.getCurrentPosition().then((value) {
        GetWeatherRequest location = GetWeatherRequest(value.lat, value.lon);
        weatherUseCase
            .getCurrentWeather(GetWeatherRequest(location.lat, location.lon))
            .then((response) {
          response.fold((l) {
            inspect(l);
          }, (weather) {
            setState(() {
              cityWeatherShow = weather;
            });
            getListRecommandation(cityWeatherShow!);
          });
        });
      });
    } else {
       getListRecommandation(cityWeatherShow!);
      cityWeatherShow = widget.cityWeather;
     // getListRecommandation(cityWeatherShow!);
    }
  }

  getListRecommandation(CityWeatherModel cityWeather) {
    weatherUseCase
        .getListRecommandation(GetRecommandationRequest(
            humidity: 15, windSpeed: 15, precipitation: 2.57, temperature: 28.36
            // humidity: cityWeather.listDayWeather![0].humidity!,
            // windSpeed: cityWeather.listDayWeather![0].windSpeed!.toDouble(),
            // precipitation: cityWeather.listDayWeather![0].precipitation!.toDouble(),
            // temperature: cityWeather.listDayWeather![0].temp!.toDouble(),
            ))
        .then((value) {
      value.fold((l) {
        inspect(l);
      }, (r) {
        if (r != null) {
          setState(() {
            listRecommandationModel = r;
          });
        }
      });
    });
  }

  @override
  void initState() {
    getWeather();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 162, 210, 250),
      extendBodyBehindAppBar: true,
      // appBar: AppBar(
      //   backgroundColor: Colors.transparent,
      //   actions: [
      //     TextButton(
      //         onPressed: () {
      //           Navigator.pushNamed(context, Routes.recommandationMeteoView);
      //         },
      //         child: Text(
      //           AppStrings.recommandations,
      //           style: getBoldTextStyle(color: ColorManager.white),
      //         ))
      //   ],
      // ),
      body: getContent(),
    );
  }

  Widget getContent() {
    return cityWeatherShow != null
        ? contentWidget(cityWeatherShow!)
        : Container();
  }

  Widget contentWidget(CityWeatherModel cityWeathe) {
    return SafeArea(
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(ImageAssets.meteoBg),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: [
            SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const SizedBox(height: 10),
                    CityInfoWidget(
                      cityWeather: cityWeathe,
                    ),
                    const SizedBox(height: 20),
                    CarouselWidget(
                      houlyWeathers:
                          cityWeathe.listDayWeather![0].houlyWeather!,
                    ),
                    const SizedBox(height: 10),
                    WindWidget(
                      dayWeather: cityWeathe.listDayWeather![0],
                    ),
                    const SizedBox(height: 10),
                    BarometerWidget(
                      dayWeather: cityWeathe.listDayWeather![0],
                    ),
                    const SizedBox(height: 10),
                    otherWidget(
                      cityWeathe.listDayWeather!,
                    ),
                    const SizedBox(height: 10),
                    recommadation(),
                  ]),
            ),
            headerWidget(cityWeathe),
          ],
        ),
      ),
    );
  }

  Container recommadation() {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(color: ColorManager.white),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Nos recommandations',
            style: getBoldTextStyle(color: ColorManager.black, fontSize: 15),
          ),
          const SizedBox(
            height: 10,
          ),
          GridView.builder(
            physics: const NeverScrollableScrollPhysics(),
            itemCount: listRecommandationModel.length,
            itemBuilder: (BuildContext context, int index) =>
                recommandationCard(listRecommandationModel[index]),
            //return Image.asset(images[index], fit: BoxFit.cover);

            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 10,
              crossAxisSpacing: 10,
              mainAxisExtent: 200,
            ),
            padding: const EdgeInsets.all(10),
            shrinkWrap: true,
          ),
        ],
      ),
    );
  }

  Widget recommandationCard(RecommandationModel recommandation) {
    return Container(
      decoration: BoxDecoration(
        color: const Color.fromARGB(255, 248, 247, 247),
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              height: 100,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5.0),
              ),
              child:recommandation.speculation!=null?  Stack(
                children: [
                   
                  ClipRRect(
                    borderRadius: BorderRadius.circular(5.0),
                    child: CachedNetworkImage(
                      imageUrl: recommandation.speculation!.imageUrl!,
                      imageBuilder: (context, imageProvider) => Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5.0),
                          image: DecorationImage(
                              scale: 2,
                              image: imageProvider,
                              fit: BoxFit.cover,
                          )
                        ),
                      ),
                      placeholder: (context, url) => Container(
                        decoration: BoxDecoration(
                          color: kSecondaryColor.withOpacity(0.1),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        width: double.maxFinite,
                        height: 100,
                      ),
                      errorWidget: (context, url, error) => Container(
                        decoration: BoxDecoration(
                          color: kSecondaryColor.withOpacity(0.1),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        width: double.maxFinite,
                        height: 100,
                        child: const Icon(Icons.error),
                      ),
                    ),
                  ),
                   Positioned(
                top: 5,
                left: 5,
                child: Container(
                  // alignment: Alignment.center,
                  padding: const EdgeInsets.only(
                      bottom: 4, top: 2, right: 3, left: 3),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: ColorManager.black.withOpacity(0.7),
                  ),
                  child: Text(
                    recommandation.speculation!.name!,
                    style: getRegularTextStyle(
                        color: ColorManager.white, fontSize: 10),
                  ),
                ),
              ),
                ],
              ):const SizedBox()),
          const SizedBox(
            height: 5,
          ),
          Container(
            padding: const EdgeInsets.all(5),
            margin: const EdgeInsets.only(left: 5),
            decoration: BoxDecoration(
                color: recommandation.treatmentRange == "PRUDENCE"
                    ? const Color.fromARGB(255, 254, 234, 192)
                    : recommandation.treatmentRange == "FAVORABLE"
                        ? const Color.fromARGB(255, 218, 242, 222)
                        : const Color.fromARGB(255, 239, 209, 207),
                borderRadius: BorderRadius.circular(10)),
            child: Text(
              recommandation.treatmentRange!,
              style: getRegularTextStyle(
                  color: recommandation.treatmentRange == "PRUDENCE"
                      ? Colors.orange
                      : recommandation.treatmentRange == "FAVORABLE"
                          ? ColorManager.primary
                          : ColorManager.red,
                  fontSize: 8),
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Container(
            margin: const EdgeInsets.only(left: 5),
            height: 50,
            child: Text(
              recommandation.message!,
              style: getMeduimTextStyle(
                color: ColorManager.black,
              ),
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      ),
    );
  }

  Container houlyWeatherInfo(HoulyWeatherModel data) {
    return Container(
      margin: const EdgeInsets.only(right: 20),
      child: Column(
        children: [
          Text(
            data.hour.toString(),
            style: getBoldTextStyle(
              color: ColorManager.white,
              fontSize: 10,
            ),
          ),
          CachedNetworkImage(
            imageUrl: data.iconLink!,
            imageBuilder: (context, imageProvider) => Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  scale: 1,
                  image: imageProvider,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            placeholder: (context, url) => Container(),
            errorWidget: (context, url, error) => const Icon(Icons.error),
            width: 30,
            height: 30,
          ),
          Text(
            "${data.temp!.toInt()}°",
            style: getBoldTextStyle(
              color: ColorManager.white,
              fontSize: 12,
            ),
          ),
        ],
      ),
    );
  }

  historyFormFields(CityWeatherModel cityWeather) {
    TextEditingController dateDebutController = TextEditingController();
    TextEditingController dateFinController = TextEditingController();

    showModalBottomSheet(
        context: context,
        builder: (context) {
          return StatefulBuilder(
              builder: (context, setInnerState) => Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      const Text("Hitorique de météo à Abidjan"),
                      const SizedBox(
                        height: 10,
                      ),
                      DateField(
                          dateController: dateDebutController,
                          hintText: "Date debut"),
                      const SizedBox(
                        height: 10,
                      ),
                      DateField(
                          dateController: dateFinController,
                          hintText: "Date fin"),
                      const SizedBox(
                        height: 10,
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          elevation: 0,
                          backgroundColor: ColorManager.primary,
                        ),
                        onPressed: () async {
                          appSharedPreference
                              .getCurrentPosition()
                              .then((value) => Navigator.push(
                                    context,
                                    PageTransition(
                                      type: PageTransitionType.bottomToTop,
                                      child: HistoryWeatherView(
                                        cityName: cityWeather.cityName!,
                                        request: HistoryWeatherRequest(
                                            lon: widget.long != null
                                                ? double.parse(widget.long!)
                                                : value.lon,
                                            // : -3.9559266,
                                            lat: widget.lat != null
                                                ? double.parse(widget.lat!)
                                                : value.lat,
                                            //  : 5.4010974,
                                            dateDeDeFin: dateFinController.text,
                                            dateDeDebut:
                                                dateDebutController.text),
                                      ),
                                      isIos: true,
                                      duration:
                                          const Duration(milliseconds: 400),
                                    ),
                                  ));
                        },
                        child: const Text(AppStrings.next),
                      ),
                    ],
                  ));
        });
  }

  menuModal(CityWeatherModel city) {
    showModalBottomSheet(
        context: context,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
          ),
        ),
        builder: (context) {
          return Container(
            padding: const EdgeInsets.all(10),
            child: StatefulBuilder(
                builder: (context, setInnerState) => Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        const SizedBox(
                          height: 10,
                        ),
                        Center(
                            child: Text(
                          'Avoir des informations',
                          style: getBoldTextStyle(
                            color: ColorManager.black,
                            fontSize: 16,
                          ),
                        )),
                        const SizedBox(
                          height: 10,
                        ),
                        menuModalListTile(
                            AppStrings.myParcel, IconManager.locationPin, () {
                          Navigator.push(
                            context,
                            PageTransition(
                              type: PageTransitionType.leftToRight,
                              child: const MesParcellesView(),
                              isIos: true,
                              duration: const Duration(milliseconds: 300),
                            ),
                          );
                        }),
                        // menuModalListTile("Historique", IconManager.history,
                        //     () {
                        //   historyFormFields(city);
                        // }),
                        menuModalListTile("Ville", IconManager.locationPin, () {
                          Navigator.push(
                            context,
                            PageTransition(
                              type: PageTransitionType.leftToRight,
                              child: const ListLocationMeteoView(),
                              isIos: true,
                              duration: const Duration(milliseconds: 300),
                            ),
                          );
                        }),
                      ],
                    )),
          );
        });
  }

  Padding headerWidget(CityWeatherModel cityWeather) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 20),
      child: Row(
        children: [
          Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: ColorManager.white.withOpacity(0.2),
              ),
              child: const BackButtonCustom()),
          const Spacer(),
          // Expanded(
          //   child: TextField(
          //     onChanged: ((value) {}),
          //     onSubmitted: (_) => {},
          //     decoration: InputDecoration(
          //       filled: true,
          //       fillColor: ColorManager.grey.withOpacity(0.1),
          //       hintText: 'Récherche',
          //       hintStyle: TextStyle(color: Colors.blue.withAlpha(135)),
          //       prefixIcon: IconButton(
          //         icon: const Icon(Icons.search, color: Colors.blue),
          //         onPressed: () {},
          //       ),
          //       border: const OutlineInputBorder(
          //         borderSide: BorderSide.none,
          //         borderRadius: BorderRadius.all(Radius.circular(15)),
          //       ),
          //     ),
          //   ),
          // ),
          const SizedBox(width: 10),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: cardColor,
            ),
            child: IconButton(
              padding: const EdgeInsets.all(12),
              iconSize: 26,
              onPressed: () {
                menuModal(cityWeather);
              },
              icon: const Icon(Icons.menu, color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }
}

//----------------------------------------)))))sdsdsdsddsds-------------------------------)))))sdsdsdsddsds-------------------------------)))))sdsdsdsddsds-------------------------------)))))sdsdsdsddsds---------------------------
parcelInfoModalSheet(BuildContext context, ParcelInfoModel parcelInfo) {
  showModalBottomSheet(
      context: context,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10),
        ),
      ),
      builder: (context) {
        return Container(
          padding: const EdgeInsets.all(10),
          child: StatefulBuilder(
              builder: (context, setInnerState) => Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      const SizedBox(
                        height: 10,
                      ),
                      Center(
                          child: Text(
                        'Infos sur votre parcelle',
                        style: getBoldTextStyle(
                          color: ColorManager.black,
                          fontSize: 16,
                        ),
                      )),
                      const SizedBox(
                        height: 10,
                      ),
                      Card(
                        color: ColorManager.black,
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(11)),
                        child: SizedBox(
                          width: double.maxFinite,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              customListTile(
                                first: 'Humidité du sol:',
                                second: ' ${parcelInfo.moisture! * 100}°',
                                icon: Icons.air,
                                iconColor: Colors.blue,
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Text(
                                'Température',
                                style: getBoldTextStyle(
                                  color: ColorManager.black,
                                  fontSize: 14,
                                ),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              customListTile(
                                first: 'A 10 cm:',
                                second: ' ${parcelInfo.t10!.round()}°',
                                icon: IconManager.temperature,
                                iconColor: Colors.red,
                              ),
                              customListTile(
                                first: 'A la surface:',
                                second: ' ${parcelInfo.t0!.round()}°',
                                icon: IconManager.temperature,
                                iconColor: Colors.red,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  )),
        );
      });
}

ListTile menuModalListTile(
  String titre,
  IconData icon,
  Function()? onTap,
) {
  return ListTile(
    leading: Icon(
      icon,
      color: ColorManager.black,
    ),
    title: Text(
      titre,
      style: getMeduimTextStyle(
        color: ColorManager.black,
      ),
    ),
    trailing: Icon(IconManager.arrowRight, color: ColorManager.black),
    onTap: onTap,
  );
}

class CityInfoWidget extends StatelessWidget {
  const CityInfoWidget({Key? key, required this.cityWeather}) : super(key: key);
  final CityWeatherModel cityWeather;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const SizedBox(
          height: 30,
        ),
        Image.network(
          cityWeather.listDayWeather![0].iconLink!,
          scale: 0.5,
        ),
        Text(
          cityWeather.cityName!,
          style: getBoldTextStyle(color: ColorManager.white, fontSize: 25),
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              cityWeather.listDayWeather![0].dayName.toString(),
              style: getRegularTextStyle(
                color: ColorManager.white,
                fontSize: 12,
              ),
            ),
            Text(
              ', ${convertDate(cityWeather.listDayWeather![0].date!)}',
              style: getRegularTextStyle(
                color: ColorManager.white,
                fontSize: 12,
              ),
            ),
          ],
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              '${cityWeather.listDayWeather![0].temp!.round()}',
              style:
                  getRegularTextStyle(color: ColorManager.white, fontSize: 60),
            ),
            Text(
              "°",
              style:
                  getRegularTextStyle(color: ColorManager.white, fontSize: 30),
            ),
          ],
        ),
        Text(
          cityWeather.listDayWeather![0].description!,
          style: getRegularTextStyle(
            color: ColorManager.white,
          ),
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(
                  Icons.south,
                  color: ColorManager.white,
                  size: 20,
                ),
                Text(
                  '  ${cityWeather.listDayWeather![0].tempMin!.round()}°',
                  style: getRegularTextStyle(
                      color: ColorManager.white, fontSize: 20),
                ),
              ],
            ),
            const SizedBox(
              width: 15,
            ),
            Icon(
              Icons.north,
              color: ColorManager.white,
              size: 20,
            ),
            Text(
              '  ${cityWeather.listDayWeather![0].tempMax!.round()}°',
              style:
                  getRegularTextStyle(color: ColorManager.white, fontSize: 20),
            ),
          ],
        ),
      ],
    );
  }
}

class BarometerOtherDayWidget extends StatelessWidget {
  const BarometerOtherDayWidget({Key? key, required this.dayWeather})
      : super(key: key);
  final DayWeatherModel dayWeather;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 5,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Text(
              'Barometre',
              style:
                  getSemiBoldTextStyle(color: ColorManager.black, fontSize: 20),
            ),
          ),
          Card(
            color: ColorManager.black,
            elevation: 0,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(11)),
            child: SizedBox(
              width: double.maxFinite,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  customListTile(
                    first: 'Temperature:',
                    second: ' ${dayWeather.temp!.round()} °C',
                    icon: Icons.thermostat,
                    iconColor: Colors.orange,
                  ),
                  customListTile(
                    first: 'Humidity:',
                    second: ' ${dayWeather.humidity!.round()} %',
                    icon: Icons.water_drop_outlined,
                    iconColor: Colors.blueGrey,
                  ),
                  customListTile(
                    first: 'Pressure:',
                    second: ' ${dayWeather.pressure!.round()} hPa',
                    icon: Icons.speed,
                    iconColor: Colors.red[300]!,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class BarometerWidget extends StatelessWidget {
  const BarometerWidget({Key? key, required this.dayWeather}) : super(key: key);
  final DayWeatherModel dayWeather;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 5,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Text(
              'Barometre',
              style: getSemiBoldTextStyle(color: titleColor, fontSize: 20),
            ),
          ),
          Card(
            color: cardColor,
            elevation: 0,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            child: SizedBox(
              width: double.maxFinite,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  customListTile(
                    first: 'Temperature:',
                    second: ' ${dayWeather.temp!.round()} °C',
                    icon: Icons.thermostat,
                    iconColor: Colors.orange,
                  ),
                  customListTile(
                    first: 'Humidity:',
                    second: ' ${dayWeather.humidity!.round()} %',
                    icon: Icons.water_drop_outlined,
                    iconColor: Colors.blueGrey,
                  ),
                  customListTile(
                    first: 'Pressure:',
                    second: ' ${dayWeather.pressure!.round()} hPa',
                    icon: Icons.speed,
                    iconColor: Colors.red[300]!,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class WindWidget extends StatelessWidget {
  const WindWidget({
    Key? key,
    required this.dayWeather,
    this.color,
  }) : super(key: key);

  final DayWeatherModel dayWeather;
  final Color? color;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 10),
          child: Text(
            'Vent',
            style: getSemiBoldTextStyle(color: titleColor, fontSize: 20),
          ),
        ),
        Card(
          color: cardColor,
          elevation: 0,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(11)),
          child: SizedBox(
            width: double.maxFinite,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                customListTile(
                  first: 'Vitesse:',
                  second: ' ${dayWeather.windSpeed} km/h',
                  icon: Icons.air,
                  iconColor: Colors.blue,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class WindOtherDayWidget extends StatelessWidget {
  const WindOtherDayWidget({
    Key? key,
    required this.dayWeather,
    this.color,
  }) : super(key: key);

  final DayWeatherModel dayWeather;
  final Color? color;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 5,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Text(
              'Vent',
              style:
                  getSemiBoldTextStyle(color: ColorManager.black, fontSize: 20),
            ),
          ),
          Card(
            color: ColorManager.black,
            elevation: 0,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(11)),
            child: SizedBox(
              width: double.maxFinite,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  customListTile(
                    first: 'Vitesse:',
                    second: ' ${dayWeather.windSpeed} km/h',
                    icon: Icons.air,
                    iconColor: Colors.blue,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Widget otherWidget(List<DayWeatherModel> dayWeathers) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Padding(
        padding: const EdgeInsets.only(left: 10),
        child: Text(
          'Autres jours',
          style: getSemiBoldTextStyle(color: ColorManager.white, fontSize: 20),
        ),
      ),
      Card(
          color: cardColor,
          elevation: 0,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(11)),
          child: OtherDayWidget(dayWeathers: dayWeathers)),
    ],
  );
}

otherDayWeatherDetail(DayWeatherModel dayWeather, BuildContext context) {
  showModalBottomSheet(
      backgroundColor: ColorManager.white,
      context: context,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10),
        ),
      ),
      builder: (context) {
        return StatefulBuilder(
          builder: (context, setInnerState) => Container(
            margin: const EdgeInsets.all(10),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Center(
                  child: Text(
                    "${dayWeather.dayName} ${convertDate(dayWeather.date!)}",
                    style: getBoldTextStyle(
                        color: ColorManager.black, fontSize: 15),
                  ),
                ),
                const Divider(),
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              "${dayWeather.temp!.round()}°",
                              style: getMeduimTextStyle(
                                  color: ColorManager.black, fontSize: 40),
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            Image.network(
                              dayWeather.iconLink!,
                              scale: 1,
                            ),
                          ],
                        ),
                        Text(
                          "${dayWeather.description}",
                          style: getRegularTextStyle(
                            color: ColorManager.black,
                            fontSize: 12,
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        CarouselOtherDayWidget(
                          houlyWeathers: dayWeather.houlyWeather!,
                        ),
                        const SizedBox(height: 10),
                        WindOtherDayWidget(
                          dayWeather: dayWeather,
                        ),
                        const SizedBox(height: 10),
                        BarometerOtherDayWidget(
                          dayWeather: dayWeather,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      });
}

class OtherDayWidget extends StatelessWidget {
  const OtherDayWidget({
    Key? key,
    required this.dayWeathers,
  }) : super(key: key);

  final List<DayWeatherModel> dayWeathers;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 330,
      child: ListView.builder(
        physics: const BouncingScrollPhysics(),
        itemCount: dayWeathers.length,
        scrollDirection: Axis.vertical,
        itemBuilder: (BuildContext context, index) {
          return GestureDetector(
            onTap: () {
              otherDayWeatherDetail(dayWeathers[index], context);
            },
            child: Container(
              decoration: const BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    color: Colors.grey,
                    width: 0.1,
                  ),
                ),
              ),
              padding: const EdgeInsets.symmetric(
                horizontal: 15,
                vertical: 5,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      SizedBox(
                        width: 80,
                        child: Text(
                          index == 0
                              ? "Aujourd'hui"
                              : dayWeathers[index].dayName!,
                          style: getMeduimTextStyle(
                              color: ColorManager.white.withOpacity(.8),
                              fontSize: 12),
                        ),
                      ),
                      Image.network(dayWeathers[index].iconLink!, scale: 2),
                      SizedBox(
                        width: getProportionateScreenWidth(100),
                        child: Text(
                          '${dayWeathers[index].description}',
                          style: getRegularTextStyle(
                              color: ColorManager.white, fontSize: 12),
                        ),
                      ),
                      const Spacer(),
                      Row(
                        children: [
                          Icon(
                            Icons.south,
                            color: ColorManager.blue,
                            size: 15,
                          ),
                          Text(
                            '${dayWeathers[index].tempMin!.round()}°',
                            style: getRegularTextStyle(
                                color: ColorManager.white, fontSize: 12),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Icon(
                            Icons.north,
                            color: ColorManager.red,
                            size: 15,
                          ),
                          Text(
                            '${dayWeathers[index].tempMax!.round()}°',
                            style: getRegularTextStyle(
                                color: ColorManager.white, fontSize: 12),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

class CarouselWidget extends StatelessWidget {
  const CarouselWidget({
    Key? key,
    required this.houlyWeathers,
    this.color,
  }) : super(key: key);
  final List<HoulyWeatherModel> houlyWeathers;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: ColorManager.black.withOpacity(0.2),
      elevation: 0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(11)),
      child: SizedBox(
        height: 100,
        child: ListView.builder(
          physics: const BouncingScrollPhysics(),
          itemCount: houlyWeathers.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext context, index) {
            return Container(
              margin: index == 0 ? const EdgeInsets.only(left: 20) : null,
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      houlyWeathers[index].hour!,
                      style: getRegularTextStyle(
                          color: ColorManager.white.withOpacity(.8),
                          fontSize: 15),
                    ),
                    const SizedBox(height: 10),
                    Image.network(houlyWeathers[index].iconLink!, scale: 2),
                    const SizedBox(height: 5),
                    Text(
                      '${houlyWeathers[index].temp!.round()}°',
                      style: getRegularTextStyle(
                          color: ColorManager.white.withOpacity(.8),
                          fontSize: 14),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}

class CarouselOtherDayWidget extends StatelessWidget {
  const CarouselOtherDayWidget({
    Key? key,
    required this.houlyWeathers,
  }) : super(key: key);
  final List<HoulyWeatherModel> houlyWeathers;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: ColorManager.black,
      elevation: 0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(11)),
      child: SizedBox(
        height: 100,
        child: ListView.builder(
          physics: const BouncingScrollPhysics(),
          itemCount: houlyWeathers.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext context, index) {
            return Container(
              margin: index == 0 ? const EdgeInsets.only(left: 20) : null,
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      houlyWeathers[index].hour!,
                      style: getRegularTextStyle(
                          color: ColorManager.white.withOpacity(.8),
                          fontSize: 15),
                    ),
                    const SizedBox(height: 10),
                    Image.network(houlyWeathers[index].iconLink!, scale: 2),
                    const SizedBox(height: 5),
                    Text(
                      '${houlyWeathers[index].temp!.round()}°',
                      style: getRegularTextStyle(
                          color: ColorManager.white.withOpacity(.8),
                          fontSize: 14),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}

Widget customListTile({
  required String first,
  required String second,
  required IconData icon,
  required Color iconColor,
  String text = '',
}) {
  return ListTile(
    trailing: Text(
      text,
      style: getRegularTextStyle(color: ColorManager.darkGrey, fontSize: 16),
    ),
    leading: Icon(icon, color: iconColor),
    title: RichText(
      maxLines: 1,
      text: TextSpan(
        children: [
          TextSpan(
            text: first,
            style: getRegularTextStyle(color: titleColor, fontSize: 16),
          ),
          TextSpan(
            text: second,
            style: getMeduimTextStyle(color: ColorManager.white, fontSize: 16),
          ),
        ],
      ),
    ),
  );
}
