import 'package:eagri/data/request/request.dart';
import 'package:eagri/domain/model/location_model.dart';
import 'package:eagri/domain/usecase/weather_usecase.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/state_renderer/state_render_impl.dart';
import 'package:eagri/presentations/meteo/weather_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
import '../../app/di.dart';
import '../../domain/model/weather_model.dart';
import '../../domain/usecase/location_usecase.dart';
import '../common/state_renderer/state_renderer.dart';
import '../ressources/color_manager.dart';
import '../ressources/strings_manager.dart';
import 'components/meteo_view_card_component.dart';

class ListLocationMeteoView extends StatefulWidget {
  const ListLocationMeteoView({Key? key}) : super(key: key);

  @override
  State<ListLocationMeteoView> createState() => _ListLocationMeteoViewState();
}

class _ListLocationMeteoViewState extends State<ListLocationMeteoView> {
  WeatherViewModel weatherViewModel = instance<WeatherViewModel>();
  GetWeatherRequest request = GetWeatherRequest(0, 0);
  LocationUsecase locationUsecase = instance<LocationUsecase>();
  List<LocationForWeatherModel> listLocation = [];
  List<LocationForWeatherModel> listSelected = [];
  WeatherUseCase weatherUseCase = instance<WeatherUseCase>();
  List<CityWeatherModel> listCityWeather = [];
  getLocationWeather() {
    listCityWeather = [];

    for (var element in listSelected) {
      LoadingState(
              stateRendererType: StateRendererType.POPUP_LOADING_STATE,
              message:
                  " ${element.cityName}n/ Collecte des données en cours...")
          .getScreenWidget(context, Container(), () {});
      weatherUseCase
          .getCurrentWeather(GetWeatherRequest(
              double.parse(element.lat!), (double.parse(element.lng!))))
          .then((value) {
        value.fold((l) {
          Navigator.pop(context);
        }, (r) {
          Navigator.pop(context);
          setState(() {
            listCityWeather.add(r);
          });
        });
      });
    }
  }

  getListLocationForWeather() {
    locationUsecase
        .getLocationForWeather()
        .then((value) => value.fold((l) => null, (list) {
              setState(() {
                if (list.listLocation != null) {
                  listLocation = list.listLocation!;
                }
              });
            }));
  }

  getWeather(GetWeatherRequest request) {
    weatherViewModel.getCurrentWeather(request);
  }

  @override
  void initState() {
    getListLocationForWeather();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButtonCustom(),
        title: const Text('Meteo'),
      ),
      body: Container(
        margin: const EdgeInsets.all(10),
        child: Column(children: [
          Container(
            color: ColorManager.grey.withOpacity(0.2),
            child: MultiSelectDialogField(
              buttonIcon: const Icon(Icons.keyboard_arrow_down),
              buttonText: const Text(AppStrings.choiceCity),
              confirmText: const Text(AppStrings.ok),
              cancelText: const Text(AppStrings.cancel),
              title: const Text(AppStrings.choiceCity),
              items: listLocation
                  .map((e) => MultiSelectItem(e, e.cityName!))
                  .toList(),
              listType: MultiSelectListType.LIST,
              onConfirm: (List<LocationForWeatherModel> values) {
                setState(() {
                  listSelected = values;
                });
                getLocationWeather();
              },
              selectedColor: ColorManager.primary,
            ),
          ),
          // Container(
          //   width: double.infinity,
          //   decoration: BoxDecoration(
          //     color: kSecondaryColor.withOpacity(0.1),
          //     borderRadius: BorderRadius.circular(15),
          //   ),
          //   child: TextField(
          //     onChanged: (value) => print(value),
          //     decoration: InputDecoration(
          //         contentPadding: EdgeInsets.symmetric(
          //             horizontal: getProportionateScreenWidth(20),
          //             vertical: getProportionateScreenWidth(9)),
          //         border: InputBorder.none,
          //         focusedBorder: InputBorder.none,
          //         enabledBorder: InputBorder.none,
          //         hintText: "Rechercher une ville",
          //         prefixIcon: const Icon(Icons.search)),
          //   ),
          // ),
          const SizedBox(
            height: 20,
          ),
          Expanded(
              child: ListView.builder(
            itemCount: listCityWeather.length,
            itemBuilder: (BuildContext context, int index) {
              return AnimationConfiguration.staggeredList(
                position: index,
                duration: const Duration(milliseconds: 100),
                child: SlideAnimation(
                  verticalOffset: 50.0,
                  child: FadeInAnimation(
                    child: MeteoVewCardComponent(
                        lat: listSelected[index].lat!,
                        lon: listSelected[index].lng!,
                        cityWeather: listCityWeather[index]),
                  ),
                ),
              );
            },
          ))
        ]),
      ),
    );
  }
}
