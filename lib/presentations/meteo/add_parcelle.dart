import 'package:eagri/data/network/error_handler.dart';
import 'package:eagri/data/repository/local/parcel_repo.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/ressources/assets_manager.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:page_transition/page_transition.dart';

import '../../app/di.dart';
import '../../app/functions.dart';
import '../../data/request/request.dart';
import '../../domain/usecase/weather_usecase.dart';
import '../common/state/succes_page_view.dart';
import '../ressources/color_manager.dart';
import '../ressources/strings_manager.dart';
import '../ressources/values_manager.dart';
import 'details_parcelle.dart';

class AddParcelView extends StatefulWidget {
  const AddParcelView({Key? key}) : super(key: key);

  @override
  State<AddParcelView> createState() => _AddParcelViewState();
}

class _AddParcelViewState extends State<AddParcelView> {
  TextEditingController parcelNameConntroller = TextEditingController();
  WeatherUseCase weatherUseCase = instance<WeatherUseCase>();
  ParcelRepo parcelRepo = ParcelRepo();
  Position? position;

  getPosition() {
    determinePosition().then((value) {
      setState(() {
        position = value;
      });
    });
  }

  @override
  void initState() {
    getPosition();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButtonCustom(),
        title: const Text(
          'Enregistrer ue parcelle',
        ),
      ),
      body: Container(
        margin: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text("Veuillez nommer votre parcelle"),
            const SizedBox(
              height: 20,
            ),
            TextFormField(
              maxLines: null,
              keyboardType: TextInputType.visiblePassword,
              controller: parcelNameConntroller,
              decoration: InputDecoration(
                filled: true,
                fillColor: ColorManager.white,
                hintText: AppStrings.enterNameParcelle,
              ),
            ),
            const SizedBox(
              height: AppMargin.m40,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 150,
                  child: ElevatedButton.icon(
                    onPressed: () {
                      saveInDraft();
                    },
                    icon: const Icon(Icons.save),
                    label: const Text("Enregistrer"),
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                SizedBox(
                  width: 150,
                  child: ElevatedButton.icon(
                    onPressed: () {
                      sendParcel();
                    },
                    icon: const Icon(Icons.send),
                    label: const Text("Envoyer"),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  saveInDraft() {
    CreateParcelRequest createParcelRequest = CreateParcelRequest(
        userId: 1,
        lat: position!.latitude,
        lon: position!.longitude,
        name: parcelNameConntroller.text);
    parcelRepo.findParcelWithCoordonnates(createParcelRequest).then((value) {
      if (value == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
                actions: [
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text(AppStrings.cancel),
                  )
                ],
                contentPadding: const EdgeInsets.all(10),
                content: const Text(
                    "Cette parcelle est déjà enrégistrée mais n'a pas été encore envoyée"));
          },
        );
      } else {
        parcelRepo.insert(createParcelRequest).then((value) {
          Navigator.pushReplacement(
            context,
            PageTransition(
              type: PageTransitionType.rightToLeft,
              child: SuccessPageView(
                message: "Sauvegarder",
                icon: JsonAssets.save,
              ),
              isIos: true,
              duration: const Duration(milliseconds: 400),
            ),
          );
        });
      }
    });
  }

  sendParcel() {
    weatherUseCase
        .createParcel(CreateParcelRequest(
            userId: 1,
            lat: position!.latitude,
            lon: position!.longitude,
            name: parcelNameConntroller.text))
        .then((value) {
      value.fold((failure) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
                actions: [
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                      // saveInDraft();
                    },
                    child: const Text('Annuler'),
                  ),
                  TextButton(
                    onPressed: () {
                      saveInDraft();
                    },
                    child: const Text('Enregistrer'),
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text(AppStrings.cancel),
                  )
                ],
                insetPadding: EdgeInsets.zero,
                contentPadding: const EdgeInsets.all(0),
                content: const Text(
                    "Echec de l'envoi. Enrégistrer dans brouillons pour pouvoir envoyer plustard "));
          },
        );
      }, (result) {
        if (result.statusCode == ResponseCode.success) {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                  actions: [
                    TextButton(
                      onPressed: () {
                        Navigator.pushReplacement(
                          context,
                          PageTransition(
                            type: PageTransitionType.rightToLeft,
                            child: DetailsParcelView(
                              parcelInfo: result,
                            ),
                            isIos: true,
                            duration: const Duration(milliseconds: 400),
                          ),
                        );
                      },
                      child: const Text('Voir les détails'),
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text(AppStrings.cancel),
                    )
                  ],
                  insetPadding: EdgeInsets.zero,
                  contentPadding: const EdgeInsets.all(0),
                  content: const Text('Cette parcelle est déjà en ligne'));
            },
          );
        } else {
          Navigator.pushReplacement(
            context,
            PageTransition(
              type: PageTransitionType.rightToLeft,
              child: const SuccessPageView(
                  message: "Votre parcelle a été envoyée avec succés"),
              isIos: true,
              duration: const Duration(milliseconds: 400),
            ),
          );
        }
      });
    });
  }
}
