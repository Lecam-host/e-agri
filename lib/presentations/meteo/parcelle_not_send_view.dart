import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/data/request/request.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/common/state_renderer/state_render_impl.dart';
import 'package:eagri/presentations/common/state_renderer/state_renderer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:page_transition/page_transition.dart';

import '../../app/di.dart';
import '../../app/functions.dart';
import '../../data/repository/local/parcel_repo.dart';
import '../../domain/usecase/weather_usecase.dart';
import '../common/state/succes_page_view.dart';
import '../ressources/color_manager.dart';

import '../ressources/styles_manager.dart';

class ParcelNotSendView extends StatefulWidget {
  const ParcelNotSendView({Key? key}) : super(key: key);

  @override
  State<ParcelNotSendView> createState() => _ParcelNotSendViewState();
}

class _ParcelNotSendViewState extends State<ParcelNotSendView> {
  List<CreateParcelRequest> listParcelNotSend = [];
  ParcelRepo parcelRepo = ParcelRepo();
  WeatherUseCase weatherUseCase = instance<WeatherUseCase>();
  List<CreateParcelRequest> listParcelChoisis = [];
  getLocalParcel() {
    parcelRepo.getAllItem().then((value) {
      setState(() {
        listParcelNotSend = value;
      });
    });
  }

  @override
  void initState() {
    getLocalParcel();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButtonCustom(),
        title: Text(
          "Parcelle non enrégistrée",
          style: getRegularTextStyle(),
        ),
        actions: [
          if (listParcelChoisis.isNotEmpty)
            TextButton(
                onPressed: () {
                  LoadingState(
                          stateRendererType:
                              StateRendererType.POPUP_LOADING_STATE)
                      .showPopUp(context, StateRendererType.POPUP_LOADING_STATE,
                          "Patientez");
                  weatherUseCase
                      .createFullParcelle(listParcelChoisis)
                      .then((value) {
                    Navigator.pop(context);
                    value.fold((l) {
                      showCustomFlushbar(
                          context,
                          "Une erreur s'est produite, veuillez ressayer",
                          ColorManager.error,
                          FlushbarPosition.TOP);
                    }, (r) {
                      Navigator.pushReplacement(
                        context,
                        PageTransition(
                          type: PageTransitionType.rightToLeft,
                          child: const SuccessPageView(
                            message: "Envoyer avec succès",
                          ),
                          isIos: true,
                          duration: const Duration(milliseconds: 400),
                        ),
                      );
                    });
                  });
                },
                child: Text(
                  'Envoyer',
                  style: getBoldTextStyle(
                    color: ColorManager.primary,
                    fontSize: 14,
                  ),
                ))
        ],
      ),
      body: Container(
        margin: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
                "La liste des parcelles que vous n'avez pas encore envoyé en ligne pour analyse"),
            const SizedBox(
              height: 10,
            ),
            Expanded(
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: listParcelNotSend.length,
                itemBuilder: (BuildContext context, int index) {
                  return AnimationConfiguration.staggeredList(
                    position: index,
                    duration: const Duration(milliseconds: 100),
                    child: SlideAnimation(
                      verticalOffset: 50.0,
                      child: FadeInAnimation(
                        child: parcelleCard(
                          listParcelNotSend[index],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget parcelleCard(CreateParcelRequest info) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: ColorManager.grey.withOpacity(0.1),
        ),
        margin: const EdgeInsets.only(
          bottom: 10,
          left: 10,
          right: 10,
        ),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Denomination',
                    style: getRegularTextStyle(
                      color: ColorManager.grey,
                    ),
                  ),
                  Text(
                    info.name.toString(),
                    style: getBoldTextStyle(
                        color: ColorManager.blue, fontSize: 14),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Date de creation',
                    style: getRegularTextStyle(
                      color: ColorManager.grey,
                    ),
                  ),
                  if (info.createAt != null && info.createAt != "")
                    Text(
                      convertDateWithTime(
                        info.createAt.toString(),
                      ),
                    ),
                ],
              ),
            ),
            Checkbox(
                activeColor: ColorManager.primary,
                value: listParcelChoisis.contains(info),
                onChanged: (value) {
                  setState(() {
                    if (!listParcelChoisis.contains(info)) {
                      listParcelChoisis.add(info);
                    } else {
                      listParcelChoisis
                          .removeWhere((element) => element == info);
                    }
                  });
                })
          ],
        ),
      ),
    );
  }
}
