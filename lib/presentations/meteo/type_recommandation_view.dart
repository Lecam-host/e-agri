import 'package:flutter/material.dart';

import '../common/buttons/back_button.dart';
import '../ressources/color_manager.dart';
import '../ressources/icon_manager.dart';
import '../ressources/routes_manager.dart';
import '../ressources/styles_manager.dart';
import '../ressources/values_manager.dart';

class TypeRecommandationView extends StatefulWidget {
  const TypeRecommandationView({Key? key}) : super(key: key);

  @override
  State<TypeRecommandationView> createState() => _TypeRecommandationViewState();
}

class _TypeRecommandationViewState extends State<TypeRecommandationView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: const BackButtonCustom(),
          title: const Text("Type de recommandation"),
        ),
        body: Container(
          margin:
              const EdgeInsets.only(right: AppMargin.m10, left: AppMargin.m10),
          child: Column(
            children: [
              const SizedBox(
                height: 10,
              ),
              ExpansionTile(
                backgroundColor: ColorManager.white,
                collapsedBackgroundColor: ColorManager.white,
                title: Text(
                  "Gestion de l'eau",
                  style: getBoldTextStyle(color: ColorManager.black),
                ),
                children: <Widget>[
                  ListTile(
                    onTap: () {
                      Navigator.pushNamed(
                          context, Routes.recommandationMeteoView);
                    },
                    title: Text(
                      "Tomate",
                      style: getRegularTextStyle(color: ColorManager.black),
                    ),
                    trailing: const Icon(IconManager.arrowRight),
                  )
                ],
              ),
            ],
          ),
        ));
  }
}
