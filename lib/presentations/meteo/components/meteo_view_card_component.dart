import 'package:eagri/presentations/meteo/details_meteo_view.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import '../../../domain/model/weather_model.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/font_manager.dart';
import '../../ressources/styles_manager.dart';
import '../../ressources/values_manager.dart';

class MeteoVewCardComponent extends StatelessWidget {
  const MeteoVewCardComponent(
      {Key? key,
      required this.cityWeather,
      required this.lat,
      required this.lon})
      : super(key: key);
  final CityWeatherModel cityWeather;
  final String lat;
  final String lon;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushReplacement(
          context,
          PageTransition(
            type: PageTransitionType.bottomToTop,
            child:
                DetailsMeteoView(cityWeather: cityWeather, lat: lat, long: lon),
            isIos: true,
            duration: const Duration(milliseconds: 400),
          ),
        );
      },
      child: Container(
        decoration: BoxDecoration(color: ColorManager.white),
        padding: const EdgeInsets.all(AppPadding.p10),
        margin: const EdgeInsets.only(
          bottom: AppMargin.m10,
          left: AppMargin.m10,
          right: AppMargin.m10,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(
                  child: SizedBox(
                    height: 50,
                    child: Text(
                      cityWeather.cityName!,
                      style: getBoldTextStyle(
                          color: ColorManager.black, fontSize: FontSize.s14),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
                const SizedBox(width: 5),
                Image.network(
                  cityWeather.listDayWeather![0].iconLink!,
                  scale: 0.9,
                ),
                const SizedBox(
                  width: 10,
                ),
                Column(
                  children: [
                    Text(
                      "${cityWeather.listDayWeather![0].temp!.round().toString()}°",
                      style: getBoldTextStyle(
                          color: ColorManager.black, fontSize: FontSize.s25),
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.south,
                          color: ColorManager.blue,
                          size: 15,
                        ),
                        Text(
                          '${cityWeather.listDayWeather![0].tempMin!.round()}°',
                          style: getRegularTextStyle(
                              color: ColorManager.blue, fontSize: 12),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Icon(
                          Icons.north,
                          color: ColorManager.red,
                          size: 15,
                        ),
                        Text(
                          '${cityWeather.listDayWeather![0].tempMax!.round()}°',
                          style: getRegularTextStyle(
                              color: ColorManager.red, fontSize: 12),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
            Column(
              children: [
                Text(
                  cityWeather.listDayWeather![0].description!,
                  style: getRegularTextStyle(
                    color: ColorManager.black,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
