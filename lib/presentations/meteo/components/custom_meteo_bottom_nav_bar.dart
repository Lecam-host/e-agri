import 'package:eagri/presentations/meteo/details_meteo_view.dart';
import 'package:eagri/presentations/meteo/list_location_meteo.dart';
import 'package:eagri/presentations/ressources/icon_manager.dart';
import 'package:eagri/presentations/ressources/strings_manager.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import '../../ressources/color_manager.dart';
import '../mes_parcelles_view.dart';

class CustomMeteoNavBar extends StatelessWidget {
  const CustomMeteoNavBar({
    Key? key,
    required this.widgetNumber,
  }) : super(key: key);

  final int widgetNumber;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: widgetNumber == 2 ? ColorManager.black : Colors.transparent,
        border: const Border(
          top: BorderSide(
            color: Colors.white,
            width: 0.3,
          ),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          NavBarBouttonn(
            widgetName: const DetailsMeteoView(),
            isActice: widgetNumber == 1,
            icon: widgetNumber == 1 ? Icons.home : Icons.home_outlined,
            text: AppStrings.meteo,
            widgetNumber: 1,
            previousWidgetNumer: widgetNumber,
          ),
          NavBarBouttonn(
            widgetName: const ListLocationMeteoView(),
            isActice: widgetNumber == 2,
            icon: widgetNumber == 2 ? Icons.list_rounded : Icons.list_rounded,
            text: "Ville",
            widgetNumber: 2,
            previousWidgetNumer: widgetNumber,
          ),
          NavBarBouttonn(
            widgetName: const MesParcellesView(),
            isActice: widgetNumber == 3,
            icon: widgetNumber == 3
                ? IconManager.locationPin
                : IconManager.locationPin,
            text: "Mes parcelles",
            widgetNumber: 3,
            previousWidgetNumer: widgetNumber,
          ),
        ],
      ),
    );
  }
}

class NavBarBouttonn extends StatelessWidget {
  const NavBarBouttonn({
    Key? key,
    required this.icon,
    required this.widgetName,
    required this.text,
    required this.isActice,
    required this.previousWidgetNumer,
    required this.widgetNumber,
  }) : super(key: key);
  final IconData icon;
  final String text;
  final bool isActice;

  final int previousWidgetNumer;
  final Widget widgetName;

  final int widgetNumber;

  @override
  Widget build(BuildContext context) {
    Color inActiveIconColor = ColorManager.grey;
    Color activeIconColor = ColorManager.blue;

    return Wrap(
      crossAxisAlignment: WrapCrossAlignment.center,
      direction: Axis.vertical,
      children: [
        IconButton(
            icon: Icon(
              icon,
              size: 25,
              color: isActice == true ? activeIconColor : inActiveIconColor,
            ),
            onPressed: () {
              if (widgetNumber > previousWidgetNumer) {
                Navigator.pushReplacement(
                  context,
                  PageTransition(
                    type: PageTransitionType.leftToRight,
                    child: widgetName,
                    isIos: true,
                    duration: const Duration(milliseconds: 300),
                  ),
                );
              }
              if (widgetNumber < previousWidgetNumer) {
                Navigator.pushReplacement(
                  context,
                  PageTransition(
                    type: PageTransitionType.rightToLeft,
                    child: widgetName,
                    isIos: true,
                    duration: const Duration(milliseconds: 400),
                  ),
                );
              }
            }),
        Text(text,
            style: TextStyle(
              fontSize: 12,
              color: isActice == true ? activeIconColor : inActiveIconColor,
            )),
      ],
    );
  }
}
