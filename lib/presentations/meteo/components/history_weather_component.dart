import 'package:eagri/app/functions.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';

import '../../../domain/model/weather_model.dart';
import '../../ressources/font_manager.dart';
import '../../ressources/values_manager.dart';

class HistoryWeatherComponenet extends StatelessWidget {
  const HistoryWeatherComponenet({Key? key, required this.historyWeather})
      : super(key: key);
  final HistoryWeatherModel historyWeather;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: ColorManager.white),
      padding: const EdgeInsets.all(AppPadding.p10),
      margin: const EdgeInsets.only(
        bottom: AppMargin.m10,
        left: AppMargin.m10,
        right: AppMargin.m10,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            child: Text(
              convertDate(historyWeather.date!),
              style: getBoldTextStyle(
                  color: ColorManager.black, fontSize: FontSize.s14),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "${historyWeather.tempDay!.round().toString()}°",
                style: getBoldTextStyle(
                    color: ColorManager.black, fontSize: FontSize.s25),
              ),
              Row(
                children: [
                  Icon(
                    Icons.south,
                    color: ColorManager.blue,
                    size: 15,
                  ),
                  Text(
                    '${historyWeather.tempMin!.round()}°',
                    style: getRegularTextStyle(
                        color: ColorManager.blue, fontSize: 12),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Icon(
                    Icons.north,
                    color: ColorManager.red,
                    size: 15,
                  ),
                  Text(
                    '${historyWeather.tempMax!.round()}°',
                    style: getRegularTextStyle(
                        color: ColorManager.red, fontSize: 12),
                  ),
                ],
              ),
            ],
          ),
          Text(
            historyWeather.description!,
            style: getRegularTextStyle(
              color: ColorManager.black,
            ),
          ),
        ],
      ),
    );
  }
}
