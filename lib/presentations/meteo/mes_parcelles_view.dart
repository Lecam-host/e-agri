import 'package:eagri/app/functions.dart';
import 'package:eagri/domain/model/weather_model.dart';
import 'package:eagri/domain/usecase/weather_usecase.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/ressources/assets_manager.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/icon_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:page_transition/page_transition.dart';

import '../../app/di.dart';
import '../../data/repository/local/parcel_repo.dart';
import '../../data/request/request.dart';
import '../common/icon_btn_with_counter.dart';
import 'add_parcelle.dart';
import 'details_parcelle.dart';
import 'parcelle_not_send_view.dart';

class MesParcellesView extends StatefulWidget {
  const MesParcellesView({Key? key}) : super(key: key);

  @override
  State<MesParcellesView> createState() => _MesParcellesViewState();
}

class _MesParcellesViewState extends State<MesParcellesView> {
  WeatherUseCase weatherUseCase = instance<WeatherUseCase>();
  List<ParcelInfoModel> listParcel = [];
  ParcelRepo parcelRepo = ParcelRepo();
  List<CreateParcelRequest> listParcelNotSend = [];
  bool isLoadParcel = true;
  getLocalParcel() {
    parcelRepo.getAllItem().then((value) {
      setState(() {
        listParcelNotSend = value;
      });
    });
  }

  getListParcel() {
    weatherUseCase.getUserListParcel(1).then((value) {
      value.fold((l) => null, (r) {
        setState(() {
          listParcel.addAll(r.listParcel!);
          isLoadParcel = false;
        });
      });
    });
  }

  @override
  void initState() {
    getLocalParcel();
    getListParcel();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: ColorManager.primary,
        onPressed: () {
          Navigator.push(
            context,
            PageTransition(
              type: PageTransitionType.rightToLeft,
              child: const AddParcelView(),
              isIos: true,
              duration: const Duration(milliseconds: 400),
            ),
          );
        },
        child: Icon(
          Icons.add,
          color: ColorManager.white,
        ),
      ),
      appBar: AppBar(
        leading: const BackButtonCustom(),
        title: const Text("Mes parcelles"),
        actions: [
          if (listParcelNotSend.isNotEmpty)
            Container(
              margin: const EdgeInsets.only(right: 10, top: 10),
              child: IconBtnWithCounter(
                svgSrc: SvgManager.draft,
                numOfitem: listParcelNotSend.length,
                press: () {
                  Navigator.push(
                    context,
                    PageTransition(
                      type: PageTransitionType.rightToLeft,
                      child: const ParcelNotSendView(),
                      isIos: true,
                      duration: const Duration(milliseconds: 400),
                    ),
                  );
                },
              ),
            )
        ],
      ),
      body: isLoadParcel == true
          ? const LoaderComponent()
          : listParcel.isNotEmpty
              ? ListView.builder(
                  itemCount: listParcel.length,
                  itemBuilder: (BuildContext context, int index) {
                    return AnimationConfiguration.staggeredList(
                      position: index,
                      duration: const Duration(milliseconds: 100),
                      child: SlideAnimation(
                        verticalOffset: 50.0,
                        child: FadeInAnimation(
                          child: parcelleCard(
                            listParcel[index],
                          ),
                        ),
                      ),
                    );
                  },
                )
              : const EmptyComponenent(),
    );
  }

  Widget parcelleCard(ParcelInfoModel parcelInfo) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          PageTransition(
            type: PageTransitionType.rightToLeft,
            child: DetailsParcelView(
              parcelInfo: parcelInfo,
            ),
            isIos: true,
            duration: const Duration(milliseconds: 400),
          ),
        );
      },
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: ColorManager.grey.withOpacity(0.1),
        ),
        margin: const EdgeInsets.only(
          bottom: 10,
          left: 10,
          right: 10,
        ),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Denomination',
                    style: getRegularTextStyle(
                      color: ColorManager.grey,
                    ),
                  ),
                  Text(
                    parcelInfo.name.toString(),
                    style: getBoldTextStyle(
                        color: ColorManager.blue, fontSize: 14),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Date de creation',
                    style: getRegularTextStyle(
                      color: ColorManager.grey,
                    ),
                  ),
                  Text(
                    convertDate(
                      parcelInfo.createdAt.toString(),
                    ),
                  ),
                ],
              ),
            ),
            Icon(
              IconManager.arrowRight,
              color: ColorManager.black,
            ),
          ],
        ),
      ),
    );
  }
}
