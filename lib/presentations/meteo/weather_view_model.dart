import 'dart:async';

import 'package:eagri/app/app_prefs.dart';
import 'package:eagri/data/request/request.dart';
import 'package:eagri/domain/usecase/weather_usecase.dart';
import 'package:geolocator/geolocator.dart';
import 'package:rxdart/rxdart.dart';

import '../../app/di.dart';
import '../../domain/model/weather_model.dart';
import '../base/base_view_model.dart';
import '../common/state_renderer/state_render_impl.dart';
import '../common/state_renderer/state_renderer.dart';

class WeatherViewModel extends BaseViewModel {
  StreamController longitude = StreamController<double>.broadcast();
  StreamController latitude = StreamController<double>.broadcast();
  StreamController position = StreamController<Position>.broadcast();
  StreamController weatherData = StreamController<CityWeatherModel>.broadcast();

  final weather = BehaviorSubject<List<DayWeatherModel>>();

  WeatherUseCase weatherUseCase;
  WeatherViewModel(this.weatherUseCase);

  @override
  void start() {
    getWeather();
  }

  void getWeather() {
    AppSharedPreference appSharedPreference = instance<AppSharedPreference>();
    appSharedPreference.getCurrentPosition().then((value) {
      GetWeatherRequest location = GetWeatherRequest(value.lat, value.lon);
      getCurrentWeather(location);
    });
  }

  void getRecommandation() {}

  getCurrentWeather(GetWeatherRequest location) async {
    (await weatherUseCase.getCurrentWeather(location)).fold(
        (failure) => {
              // left -> failure
              inputState.add(ErrorState(
                  StateRendererType.FULL_SCREEN_ERROR_STATE, failure.message))
            }, (data) {
      weatherData.add(data);
      inputState.add(ContentState());
    });
  }

  Stream<Position> get outputIsPosition =>
      position.stream.map((position) => position);
  Stream<CityWeatherModel> get outputIsWeather =>
      weatherData.stream.map((weather) => weather);
}
