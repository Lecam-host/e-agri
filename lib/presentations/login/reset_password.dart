import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/login/controllers/login_controller.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../ressources/color_manager.dart';
import '../ressources/strings_manager.dart';
import '../ressources/values_manager.dart';

class ResetPasswordView extends StatelessWidget {
  ResetPasswordView({super.key});
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    LoginController loginController = Get.put(LoginController());
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: const Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            )),
        centerTitle: true,
        title: const Text('Reinitialisation du mot de passe'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Veuillez entrez votre numéro",
                style: getBoldTextStyle(fontSize: 20),
              ),
              const SizedBox(height: 50),
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    TextFormField(
                      autofocus: true,
                      controller: loginController.number,
                      validator: (value) => value!.isEmpty
                          ? 'Veuillez entrez votre numéro'
                          : null,
                      keyboardType: TextInputType.visiblePassword,
                      decoration: const InputDecoration(
                        prefixIcon: Icon(Icons.password),
                        filled: true,
                        hintText: "Numéro de téléphone",
                      ),
                    ),
                    const SizedBox(height: 20),
                    SizedBox(
                      height: AppSize.s40,
                      width: double.infinity,
                      child: Obx(() => loginController.load.value
                          ? const LoaderComponent()
                          : ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: ColorManager.primary,
                                textStyle: getRegularTextStyle(
                                  color: ColorManager.white,
                                ),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(
                                    AppSize.s12,
                                  ),
                                ),
                              ),
                              child: const Text(AppStrings.submit),
                              onPressed: () async {
                                if (_formKey.currentState!.validate()) {
                                  await loginController.sendOtpResetPassword();
                                } else {}
                              })),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
