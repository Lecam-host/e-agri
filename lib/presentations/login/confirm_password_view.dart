import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/login/controllers/login_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../common/buttons/back_button.dart';
import '../ressources/color_manager.dart';
import '../ressources/strings_manager.dart';
import '../ressources/styles_manager.dart';
import '../ressources/values_manager.dart';

class ConfirmPasswordView extends StatefulWidget {
  const ConfirmPasswordView({super.key});

  @override
  State<ConfirmPasswordView> createState() => _ConfirmPasswordViewState();
}

class _ConfirmPasswordViewState extends State<ConfirmPasswordView> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    LoginController loginController = Get.find();
    return Scaffold(
      appBar: AppBar(
        leading: const BackButtonCustom(),
        centerTitle: true,
        title: const Text('Vérification de l\'OTP'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 30),
              const Text("Code de vérification"),
              TextFormField(
                controller: loginController.token,
                maxLength: 5,
                validator: (value) => value!.isEmpty
                    ? 'Veuillez entrez le code de vérification'
                    : null,
                keyboardType: TextInputType.number,
                decoration: const InputDecoration(
                  prefixIcon: Icon(Icons.password),
                  filled: true,
                  hintText: "Code de vérifiecation",
                ),
              ),
              const SizedBox(height: 20),
              const Text("Nouveau mot de passe"),
              TextFormField(
                controller: loginController.newPassword,
                validator: (value) => value!.isEmpty
                    ? 'Veuillez entrez le nouveau mot de passe'
                    : null,
                keyboardType: TextInputType.visiblePassword,
                decoration: const InputDecoration(
                  prefixIcon: Icon(Icons.password),
                  filled: true,
                  hintText: "Nouveau mot de passe",
                ),
              ),
              const SizedBox(height: 20),
              Obx(() => loginController.loadResetPassword.value ? const LoaderComponent(): SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: ColorManager.primary,
                        textStyle: getRegularTextStyle(
                          color: ColorManager.white,
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(
                            AppSize.s12,
                          ),
                        ),
                      ),
                      child: const Text(AppStrings.submit),
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          await loginController.resetPassword();
                        } else {}
                      },
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
