// ignore_for_file: prefer_final_fields, use_build_context_synchronously

import 'dart:developer';

import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/app/app_prefs.dart';
import 'package:eagri/controllers/user_controller.dart';
import 'package:eagri/data/request/request.dart';
import 'package:eagri/presentations/cart/controller/cart_controller.dart';
import 'package:eagri/presentations/catalogue/controllers/catalogue_controller.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/home/home_screen.dart';
import 'package:eagri/presentations/login/controllers/login_controller.dart';
import 'package:eagri/presentations/login/reset_password.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/get.dart';
import '../../app/constant.dart';
import '../../app/di.dart';
import '../../domain/usecase/login_usecase.dart';
import '../command/controller/command_controller.dart';
import '../common/state/loader.dart';
import '../common/state_renderer/state_render_impl.dart';
import '../ressources/assets_manager.dart';
import '../ressources/color_manager.dart';
import '../ressources/font_manager.dart';
import '../ressources/routes_manager.dart';
import '../ressources/strings_manager.dart';
import '../ressources/styles_manager.dart';
import '../ressources/values_manager.dart';
import 'login_view_model.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final _focusNode = FocusNode();
  CartController cartController = Get.put(CartController());
  CommandCrontroller commandCrontroller = Get.put(CommandCrontroller());
  UserController userController = Get.put(UserController());

  CatalogueController catalogueController = Get.put(CatalogueController());

  LoginViewModel _viewModel = instance<LoginViewModel>();
  TextEditingController userNumber = TextEditingController();
  TextEditingController password = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  AppSharedPreference appSharedPreference = instance<AppSharedPreference>();
  LoginUseCase loginUseCase = instance<LoginUseCase>();

  bool isObscure = true;

  bind() {
    _viewModel.start();
    userNumber.addListener(() {
      _viewModel.setNumber(userNumber.text);
    });
    password.addListener(() {
      _viewModel.setPassword(password.text);
    });

    _viewModel.isUserLoginSuccess.stream.listen((isSuccessLoggedIn) {
      // navigate to main screen
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Navigator.of(context).pushReplacementNamed(Routes.homeRoute);
      });
    });
  }

  @override
  void initState() {
    // _bind();
    super.initState();
  }

  @override
  void dispose() {
    _focusNode.dispose();
    _viewModel.dispose();
    super.dispose();
  }

  Widget getContent(LoginController loginController) {
    return Container(
      padding:
          const EdgeInsets.only(left: AppPadding.p10, right: AppPadding.p10),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(ImageAssets.connexionBg),
          fit: BoxFit.cover,
          colorFilter: ColorFilter.mode(
            Colors.black.withOpacity(0.7),
            BlendMode.multiply,
          ),
        ),
      ),
      child: Stack(
        children: [
          Positioned(
            top: MediaQuery.of(context).size.height / 15,
            left: MediaQuery.of(context).size.width / 100,
            child: Container(
              // padding:const  EdgeInsets.all(5),
              decoration: const BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
              ),
              child: const BackButtonCustom(),
            ),
          ),
          Center(
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          left: AppPadding.p28, right: AppPadding.p28),
                      child: Text(
                        AppStrings.login,
                        style: getBoldTextStyle(
                            color: ColorManager.white, fontSize: FontSize.s30),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    const SizedBox(
                      height: AppSize.s30,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                          top: AppPadding.p28,
                        ),
                        child: TextFormField(
                          focusNode: _focusNode,
                          controller: userNumber,
                          validator: (value) =>
                              value!.isEmpty ? 'Entrer votre login' : null,
                          decoration: InputDecoration(
                            prefix: Text(
                              "225 ",
                              style:
                                  getMeduimTextStyle(color: ColorManager.black),
                            ),
                            prefixIcon: const Icon(Icons.numbers_outlined),
                            filled: true,
                            fillColor: ColorManager.white,
                            hintText: "  Login",
                            // errorText: (snapshot.data ?? true)
                            //     ? null
                            //     : AppStrings.number,
                          ),
                        )),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: AppPadding.p28,
                      ),
                      child: TextFormField(
                        validator: (value) =>
                            value!.isEmpty ? 'Entrer votre mot de passe' : null,
                        keyboardType: TextInputType.visiblePassword,
                        controller: password,
                        obscureText: isObscure,
                        decoration: InputDecoration(
                            prefixIcon: const Icon(Icons.password),
                            filled: true,
                            fillColor: ColorManager.white,
                            hintText: AppStrings.password,
                            suffixIcon: IconButton(
                              onPressed: () {
                                setState(() {
                                  isObscure = !isObscure;
                                });
                              },
                              icon: Icon(
                                isObscure
                                    ? Icons.remove_red_eye
                                    : Icons.remove_red_eye_outlined,
                              ),
                            )
                            // errorText: (snapshot.data ?? true)
                            //     ? null
                            //     : AppStrings.password,
                            ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: TextButton(
                        onPressed: () {
                          Get.to(ResetPasswordView());
                        },
                        child: const Text(
                          "Mot de passe oublié ?",
                          style: TextStyle(
                              color: Colors.white,
                              decoration: TextDecoration.underline),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: AppPadding.p10),
                      child: SizedBox(
                        height: AppSize.s40,
                        width: double.infinity,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: ColorManager.primary,
                              textStyle: getRegularTextStyle(
                                color: ColorManager.white,
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(
                                  AppSize.s12,
                                ),
                              ),
                            ),
                            child: const Text(AppStrings.submit),
                            onPressed: () {
                              if (_formKey.currentState!.validate()) {
                                connectUser(loginController);
                              } else {}
                            }),
                      ),
                    ),
                    // TextButton(
                    //   onPressed: () {},
                    //   style: TextButton.styleFrom(
                    //     primary: ColorManager.white,
                    //   ),
                    //   child: const Text(AppStrings.forgetPassword),
                    // ),
                    // Padding(
                    //   padding: const EdgeInsets.only(top: AppPadding.p10),
                    //   child: Row(
                    //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //     children: [

                    //       TextButton(
                    //         onPressed: () {},
                    //         style: TextButton.styleFrom(
                    //           primary: ColorManager.white,
                    //         ),
                    //         child: const Text(AppStrings.signUp),
                    //       )
                    //     ],
                    //   ),
                    // ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  connectUser(LoginController loginController) async {
    loaderDialog(context, text: "Veuillez patienter");
    String userLogin = "";
    if (Constant.baseUrl == Constant.awsBaseUrl) {
      userLogin = "225${userNumber.text}";
    } else {
      userLogin = userNumber.text;
    }

    await loginUseCase
        .login(
      LoginRequest(userLogin, password.text),
    )
        .then(
      (response) async {
        await response.fold((authFailed) async {
          Navigator.pop(context);
          if (authFailed.code == 3001) {
            showCustomFlushbar(context, false,
                "Echec de connexion \n login ou mot de passe incorrect \n Si vous n'avez pas de compte veuillez contacter le support technique");
          } else {
            showCustomFlushbar(context, false, "Echec de connexion");
          }
        }, (authResponse) async {
          String registrationId =
              (await FirebaseMessaging.instance.getToken())!;
          log(registrationId);
          await loginUseCase.saveRegistrationId(
            SaveRegistrationIdRequest(userLogin, registrationId),
          );
          await loginUseCase
              .getUserInfo(
            authResponse.userInfo!.id!,
          )
              .then(
            (value) async {
              await value.fold(
                (userInfoFailed) {
                  Navigator.pop(context);
                  showCustomFlushbar(context, false, "Echec de connexion");
                  //
                },
                (userInfoResponse) async {
                  userInfoResponse.token = authResponse.userInfo!.token!;
                  userInfoResponse.userName = userLogin;
                  userInfoResponse.firebaseToken = registrationId;
                  await appSharedPreference
                      .createLoginSession(context, userInfoResponse)
                      .then(
                    (value) async {
                      await instance.reset();
                      await initAppModule();
                      userController.init();
                      cartController.init();
                      catalogueController.onInit();
                      commandCrontroller.init();
                      Get.offAll(
                        const HomeScreen(),
                      );
                      showCustomFlushbar(context, true, "Connexion réussie");
                      FocusManager.instance.primaryFocus?.unfocus();
                    },
                  );
                },
              );
            },
          );
        });
      },
    );
  }

  void showCustomFlushbar(
      BuildContext context, bool isSuccess, String message) {
    Flushbar(
      maxWidth: double.infinity,
      flushbarPosition: FlushbarPosition.TOP,
      duration: const Duration(seconds: 4),
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.all(10),
      borderRadius: BorderRadius.circular(15),
      backgroundColor:
          isSuccess == true ? ColorManager.primary : ColorManager.black,
      icon: Icon(isSuccess == true ? Icons.verified : Icons.error,
          color: ColorManager.white),
      boxShadows: const [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],
      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      message: message,
    ).show(context);
  }

  @override
  Widget build(BuildContext context) {
    final loginController = Get.put(LoginController());
    return Scaffold(
      body: StreamBuilder<FlowState>(
        stream: _viewModel.outputState,
        builder: (context, snapshot) {
          return snapshot.data
                  ?.getScreenWidget(context, getContent(loginController), () {
                _viewModel.login();
              }) ??
              getContent(loginController);
        },
      ),
    );
  }
}
