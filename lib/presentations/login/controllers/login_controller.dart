import 'dart:developer';

import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/domain/model/user_model.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../app/di.dart';
import '../../../data/repository/api/repository_impl.dart';
import '../confirm_password_view.dart';

class LoginController extends GetxController {
  UserModel userInfo = UserModel();
  var load = false.obs;
  var loadResetPassword = false.obs;
  TextEditingController number = TextEditingController();
  TextEditingController token = TextEditingController();
  TextEditingController newPassword = TextEditingController();
  RepositoryImpl remoteImpl = instance<RepositoryImpl>();

  Future<void> sendOtpResetPassword() async {
    load.value = true;
    await remoteImpl
        .sendOtpResetPassword(number.text)
        .then((value) => value.fold((l) {
              load.value = false;
              inspect(l);
              showCustomFlushbar(Get.context!, l.message, ColorManager.error,
                  FlushbarPosition.TOP);
            }, (r) {
              load.value = false;
              if (r != null) {
                if (r.status == 200) {
                  Get.to(const ConfirmPasswordView());
                } else {
                  showCustomFlushbar(Get.context!, "Numéro incorrecte!",
                      ColorManager.error, FlushbarPosition.TOP);
                }
              }
            }))
        .catchError((onError) {
      load.value = false;
      log(onError.toString());
    });
  }

  Future<void> resetPassword() async {
    loadResetPassword.value = true;
    await remoteImpl
        .resetPassword(number.text, newPassword.text, token.text)
        .then((value) => value.fold((l) {
              log(number.text);
              log(newPassword.text);
              log(token.text);
              loadResetPassword.value = false;
              inspect(l);
              showCustomFlushbar(Get.context!, l.message, ColorManager.error,
                  FlushbarPosition.TOP);
            }, (r) {
              loadResetPassword.value = false;
              if (r != null) {
                if (r.status == 200) {
                  Get.close(2);
                  showCustomFlushbar(
                      Get.context!,
                      "Mot de passe modifié avec succes",
                      ColorManager.succes,
                      FlushbarPosition.TOP);
                } else {
                  showCustomFlushbar(Get.context!, r.message!,
                      ColorManager.error, FlushbarPosition.TOP);
                }
              }
            }))
        .catchError((onError) {
      loadResetPassword.value = false;
      log(onError.toString());
    });
  }

  @override
  void onClose() {
    number.dispose();
    super.onClose();
  }
}
