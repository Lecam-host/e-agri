import 'dart:async';

import '../../domain/usecase/login_usecase.dart';
import '../common/freezed_data_classes.dart';
import '../common/state_renderer/state_render_impl.dart';
import 'package:eagri/presentations/base/base_view_model.dart';

class LoginViewModel extends BaseViewModel {
  StreamController numberStreamController =
      StreamController<String>.broadcast();
  StreamController passwordStreamController =
      StreamController<String>.broadcast();
  StreamController isAllInputsValidController =
      StreamController<void>.broadcast();

  StreamController isUserLoginSuccess = StreamController<bool>();
  var loginObject = LoginObject("", "");
  LoginUseCase loginUseCase;
  LoginViewModel(this.loginUseCase);
  @override
  void dispose() {
    numberStreamController.close();
    passwordStreamController.close();
    isAllInputsValidController.close();
    isUserLoginSuccess.close();
  }

  @override
  void start() {
    // view tells state renderer, please show the content of the screen
    inputState.add(ContentState());
  }

  // @override
  // login() async {
  //   inputState.add(
  //       LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE));
  //   (await loginUseCase.login(
  //           LoginUseCaseInput(loginObject.userNumber, loginObject.password)))
  //       .fold(
  //           (failure) => {
  //                 // left -> failure
  //                 inputState.add(ErrorState(
  //                     StateRendererType.FULL_SCREEN_ERROR_STATE,
  //                     failure.message))
  //               }, (data) {
  //     // right -> success (data)
  //     inputState.add(ContentState());

  //     isUserLoginSuccess.add(true);
  //   });
  // }

  Sink get inputPassword => passwordStreamController.sink;

  Sink get isAllInput => isAllInputsValidController.sink;

  Sink get inputNumber => numberStreamController.sink;

  Stream<bool> get outputIsNumberValid =>
      numberStreamController.stream.map((number) => isNumberValid(number));

  Stream<bool> get outputIsPasswordValid => passwordStreamController.stream
      .map((password) => isPasswordValid(password));
  Stream<bool> get outputIsAllInputValid =>
      isAllInputsValidController.stream.map((_) => isValidAllInput());
  setNumber(String number) {
    inputNumber.add(number);
    loginObject = loginObject.copyWith(userNumber: number);
    _validate();
  }

  setPassword(String password) {
    inputPassword.add(password);
    loginObject = loginObject.copyWith(password: password);
    _validate();
  }

  bool isPasswordValid(String password) {
    return password.isNotEmpty;
  }

  bool isNumberValid(String number) {
    return number.isNotEmpty;
  }

  bool isValidAllInput() {
    return isPasswordValid(loginObject.password) &&
        isNumberValid(loginObject.userNumber);
  }

  _validate() {
    isAllInput.add(null);
  }

  login() {}
}

abstract class LoginViewModelInput {
  setNumber(String number);
  setPassword(String password);
  login();
  Sink get inputNumber;
  Sink get inputPassword;
  Sink get isAllInput;
}

abstract class LoginViewModelOuput {
  Stream<bool> get outputIsNumberValid;
  Stream<bool> get outputIsPasswordValid;
  Stream<bool> get outputIsAllInputValid;
}
