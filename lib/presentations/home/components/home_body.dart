import 'package:eagri/controllers/user_controller.dart';
import 'package:eagri/presentations/conseils/components/list_last_advice.dart';
import 'package:eagri/presentations/home/components/services.dart';
import 'package:eagri/presentations/home/components/home_header.dart';
import 'package:eagri/presentations/home/components/last_product.dart';
import 'package:eagri/presentations/ressources/size_config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../shop/last_prestation_component.dart';
import '../../shop/last_transport_component.dart';
import '../../shop/some_intrants.dart';
import '../controllers/home_controller.dart';

class HomeHomeView extends StatefulWidget {
  const HomeHomeView({super.key});

  @override
  State<HomeHomeView> createState() => _HomeHomeViewState();
}

class _HomeHomeViewState extends State<HomeHomeView> {
  HomeController homeController = Get.put(HomeController());
  UserController userController = Get.put(UserController());
  Future<void> refresh() async {
    return homeController.refresh();
  }

  @override
  void initState() {
    userController.getUserStat();

    userController.updateUserInfo(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () => refresh(),
      child: SafeArea(
          child: SingleChildScrollView(
        physics: const AlwaysScrollableScrollPhysics(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // SizedBox(height: getProportionateScreenWidth(30)),
            // const Carousel(),
            // SizedBox(height: getProportionateScreenWidth(30)),
            // const DiscountBanner(),
            // const SizedBox(
            //   height: 20,
            // ),

            // const SpecialOffers(),
            // Padding(
            //   padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            //   child: Text(
            //     "Derniers produits",
            //       style:
            //           getMeduimTextStyle(color: ColorManager.black, fontSize: 18),
            //       ),
            // ),
            const HomeHeader(),
            SizedBox(height: getProportionateScreenWidth(30)),
            const Services(),
            SizedBox(height: getProportionateScreenWidth(30)),

            const LastProduct(),

            const SomeIntrant(),

            const LastPrestationComponenent(),

            const LastTransportComponenent(),
            SizedBox(height: getProportionateScreenWidth(30)),
            const ListLastAdvice(),
          ],
        ),
      )),
    );
  }
}
