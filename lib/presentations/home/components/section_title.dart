import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';

class SectionTitle extends StatelessWidget {
  const SectionTitle({
    Key? key,
    required this.title,
    required this.press,
    this.style,
  }) : super(key: key);

  final String title;
  final TextStyle? style;
  final GestureTapCallback press;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(title,
            style: style ??
                getMeduimTextStyle(color: ColorManager.black, fontSize: 16)),
        GestureDetector(
          onTap: press,
          child: Text(
            "Voir plus",
            style: getMeduimTextStyle(
              color: ColorManager.primary,
            ),
          ),
        ),
      ],
    );
  }
}
