import 'package:eagri/presentations/common/product_card.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:eagri/presentations/shop/controller/shop_controller.dart';
import 'package:flutter/material.dart';

import '../../common/shimmer.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/size_config.dart';
import '../../shop/shop_screen.dart';
import 'section_title.dart';

import 'package:get/get.dart';

class LastProduct extends StatelessWidget {
  const LastProduct({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ShopController shopController = Get.put(ShopController());

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: SectionTitle(
              title: "Produits agricoles",
              style:
                  getMeduimTextStyle(color: ColorManager.black, fontSize: 14),
              press: () { Get.to( const ShopScreen(appBarTitle: "Produit agricole",indexPage: 0,));
              }),
        ),
        SizedBox(height: getProportionateScreenWidth(10)),
        Obx(() => shopController.lastProduct.isNotEmpty
            ? SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Container(
                  margin: const EdgeInsets.only(right: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ...List.generate(
                        shopController.lastProduct.length,
                        (index) {
                          return Container(
                            margin: const EdgeInsets.only(left: 10),
                            width: 200,
                            child: ProductCard(
                              data: shopController.lastProduct[index],
                            ),
                          );
                          // here by default width and height is 0
                        },
                      ),
                      // SizedBox(width: getProportionateScreenWidth(20)),
                    ],
                  ),
                ),
              )
            : SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ...List.generate(
                      5,
                      (index) {
                        return shimmerProductCustom();
                        // here by default width and height is 0
                      },
                    ),
                    SizedBox(width: getProportionateScreenWidth(20)),
                  ],
                ),
              )),
      ],
    );
  }
}
