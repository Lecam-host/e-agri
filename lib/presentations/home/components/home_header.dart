import 'package:carousel_slider/carousel_slider.dart';
import 'package:eagri/presentations/common/buttons/cart_bottom_component.dart';
import 'package:eagri/presentations/ressources/assets_manager.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/strings_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import '../../../controllers/connection_controller.dart';
import '../../../domain/model/user_model.dart';
import '../../account/user_info_screen.dart';

class HomeHeader extends StatefulWidget {
  const HomeHeader({
    Key? key,
  }) : super(key: key);

  @override
  State<HomeHeader> createState() => _HomeHeaderState();
}

class _HomeHeaderState extends State<HomeHeader> {
  // final CarouselController _controller = CarouselController();
  ConnectionController connectionController = Get.find();
  List<Item> items = [
    Item(
      img: ImageAssets.connexionBg,
      text: "Consulter le prix des produits sur le marché",
    ),
    // Item(
    //   img: ImageAssets.connexionBg,
    //   text: "Acheter et vendre des produits",
    // ),
    // Item(
    //   img: ImageAssets.mettreEngrais,
    //   text: "Suivre les conseils des experts afin d'améliorer votre production",
    // ),
    Item(
      img: ImageAssets.allWeather,
      text: "Consulter les meteos afin de mieux cultiver",
    ),
  ];

  List<String> strings = [
    "Consulter les meteos afin de mieux cultiver",
    "Acheter des produits vivriers à bat prix",
    "Vendre des produits et gagner de l'argent",
  ];
  int current = 0;
  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
      builder: (context, user, child) => Obx(
        () => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (connectionController.connectReseau.value == false)
              Center(
                child: Container(
                  color: ColorManager.red,
                  height: 50,
                  width: double.infinity,
                  child: Center(
                      child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Icon(Icons.wifi_off),
                      const SizedBox(
                        width: 5,
                      ),
                      Text(
                        AppStrings.noConnection,
                        style: getRegularTextStyle(fontSize: 10),
                      ),
                    ],
                  )),
                ),
              ),
            Stack(
              children: [
                Column(
                  children: [
                    CarouselSlider(
                      options: CarouselOptions(
                          onPageChanged: (index, reason) {
                            setState(() {
                              current = index;
                            });
                          },
                          viewportFraction: 1,
                          autoPlay: true,
                          disableCenter: true,
                          height: MediaQuery.of(context).size.height / 4,
                          autoPlayInterval: const Duration(seconds: 20)),
                      items: items.map((item) {
                        return Builder(
                          builder: (BuildContext context) {
                            return Container(
                              height: MediaQuery.of(context).size.height / 2,
                              decoration: BoxDecoration(
                                color: Colors.black.withOpacity(0.3),
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  colorFilter: ColorFilter.mode(
                                    Colors.black.withOpacity(0.3),
                                    BlendMode.darken,
                                  ),
                                  image: AssetImage(
                                    item.img,
                                  ),
                                  // colorFilter:
                                ),
                              ),
                              child: Align(
                                alignment: Alignment.bottomLeft,
                                child: Container(
                                  margin: const EdgeInsets.only(
                                      left: 10, bottom: 10),
                                  width:
                                      MediaQuery.of(context).size.width / 1.2,
                                  child: Text(
                                    item.text,
                                    style: getSemiBoldTextStyle(
                                      fontSize: 18,
                                      color: ColorManager.white,
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                        );
                      }).toList(),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: items.asMap().entries.map((entry) {
                        return GestureDetector(
                          //  onTap: () => _controller.animateToPage(entry.key),
                          child: AnimatedContainer(
                            duration: const Duration(milliseconds: 200),
                            margin: const EdgeInsets.only(right: 5),
                            height: 7,
                            width: current == entry.key ? 20 : 7,
                            decoration: BoxDecoration(
                              color: current == entry.key
                                  ? ColorManager.primary
                                  : const Color(0xFFD8D8D8),
                              borderRadius: BorderRadius.circular(5),
                            ),
                          ),
                        );
                      }).toList(),
                    ),
                  ],
                ),
                if (user.isConnected == true)
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        PageTransition(
                          type: PageTransitionType.rightToLeft,
                          child: const InfoPersoPage(),
                          isIos: true,
                          duration: const Duration(milliseconds: 400),
                        ),
                      );
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const CircleAvatar(),
                              const SizedBox(
                                width: 5,
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    user.firstName!,
                                    style: getRegularTextStyle(
                                        color: ColorManager.white),
                                  ),
                                  Text(
                                    user.lastName!,
                                    style: getRegularTextStyle(
                                        fontSize: 12,
                                        color: ColorManager.white),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          // const SearchField(),
                          const Spacer(),
                          CartButtomComponent(),

                          // const SizedBox(
                          //   width: 5,
                          // ),
                          // IconBtnWithCounter(
                          //   svgSrc: "assets/icons/Bell.svg",
                          //   numOfitem: 3,
                          //   press: () {},
                          // ),
                        ],
                      ),
                    ),
                  ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class Item {
  String img;
  String text;
  Item({required this.img, required this.text});
}
