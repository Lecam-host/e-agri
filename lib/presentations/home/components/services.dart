import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/app/functions.dart';
import 'package:eagri/presentations/ressources/assets_manager.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/strings_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../app/constant.dart';
import '../../../domain/model/user_model.dart';
import '../../common/flushAlert_componenent.dart';
import '../../ressources/routes_manager.dart';

class Services extends StatelessWidget {
  const Services({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Map<String, dynamic>> categories(List<String> scopes) => [
         // if (isInScopes(UserPermissions.SHOW_PRESTATION, scopes) == true)
            // {
            //   "icon": IconAssetManager.serviceIcon,
            //   "text": "Prestation",
            //   "route": Routes.prestationHome,
            //   "color": ColorManager.grey
            // },
          // if (isInScopes(UserPermissions.SHOW_TRANSPORT, scopes) == true)
          //   {
          //     "icon": IconAssetManager.transportIcon,
          //     "text": "Transport",
          //     "route": Routes.transport,
          //     "color": ColorManager.grey
          //   },
          if(hasAtLeastOneScope([UserPermissions.CREDIT_SHOW,UserPermissions.ASSURANCE_VOIR], scopes))
          {
            "icon": IconAssetManager.coinsIcon,
            "text": "Financier",
            "route": Routes.finance,
            "color": ColorManager.grey
          },
           if(isInScopes(UserPermissions.CONSEIL_AGRICOLE_VOIR, scopes) == true)
          {
            "icon": IconAssetManager.adviceIcon,
            "text": AppStrings.conseils,
            "route": Routes.conseils,
            "color": ColorManager.jaune
          },
           if(isInScopes(UserPermissions.METEO_RECOMMANDATION_VOIR, scopes) == true)
          {
            "icon": IconAssetManager.weatherIcon,
            "text": AppStrings.weather,
            "route": Routes.detailsMeteo,
            "color": ColorManager.blue
          },
           if(isInScopes(UserPermissions.ALERT_VOIR, scopes) == true)
          {
            "icon": IconAssetManager.alertIcon,
            "text": AppStrings.alert,
            "route": Routes.alert,
            "color": ColorManager.grey
          },
        ];

    return Consumer<UserModel>(
      builder: (context, user, child) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: Text("Services",
                style: getMeduimTextStyle(
                    color: ColorManager.black, fontSize: 18)),
          ),
          Center(
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Container(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ...List.generate(
                      categories(user.scopes??[]).length,
                      (index) => Container(
                        width: 72,
                        margin: const EdgeInsets.only(left: 10),
                        child: CategoryCard(
                          color: categories(user.scopes??[])[index]["color"],
                          icon: categories(user.scopes??[])[index]["icon"],
                          text: categories(user.scopes??[])[index]["text"],
                          press: () {
                            if (user.isConnected == true) {
                              Navigator.pushNamed(
                                context,
                                categories(user.scopes??[])[index]["route"],
                              );
                            } else {
                              Navigator.pushNamed(
                                context,
                                Routes.loginRoute,
                              );
                              showCustomFlushbar(context, "Connexion requise",
                                  ColorManager.error, FlushbarPosition.BOTTOM);
                            }
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class CategoryCard extends StatelessWidget {
  const CategoryCard({
    Key? key,
    required this.icon,
    required this.text,
    required this.press,
    required this.color,
  }) : super(key: key);

  final String? icon, text;
  final GestureTapCallback press;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              color: ColorManager.white,
              borderRadius: BorderRadius.circular(5),
            ),
            child: SizedBox(
              height: 60,
              child: Image.asset(
                icon!,
                scale: 3 / 2,
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(2),
            decoration: BoxDecoration(
              //  color: ColorManager.white,
              borderRadius: BorderRadius.circular(5),
            ),
            child: Text(
              text!,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
              style: getRegularTextStyle(
                color: ColorManager.black,
                fontSize: 12,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
