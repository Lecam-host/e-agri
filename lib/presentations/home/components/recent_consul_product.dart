import 'package:eagri/presentations/common/product_card.dart';
import 'package:eagri/presentations/ressources/routes_manager.dart';
import 'package:eagri/presentations/shop/controller/shop_controller.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import '../../ressources/color_manager.dart';
import '../../ressources/size_config.dart';
import 'section_title.dart';

import 'package:get/get.dart';

class RecentConsultProducts extends StatelessWidget {
  const RecentConsultProducts({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ShopController shopController = Get.put(ShopController());

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: SectionTitle(
              title: "Consultés recement",
              press: () {
                Get.offNamed(Routes.shop);
              }),
        ),
        SizedBox(height: getProportionateScreenWidth(10)),
        Obx(() => shopController.lastProduct.isNotEmpty
            ? SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ...List.generate(
                      shopController.lastProduct.length,
                      (index) {
                        return Container(
                          margin: const EdgeInsets.only(left: 10),
                          width: 160,
                          height: 240,
                          child: ProductCard(
                            data: shopController.lastProduct[index],
                          ),
                        );
                        // here by default width and height is 0
                      },
                    ),
                    SizedBox(width: getProportionateScreenWidth(20)),
                  ],
                ),
              )
            : SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ...List.generate(
                      5,
                      (index) {
                        return shimmerCustom();
                        // here by default width and height is 0
                      },
                    ),
                    SizedBox(width: getProportionateScreenWidth(20)),
                  ],
                ),
              )),
      ],
    );
  }

  Shimmer shimmerCustom() {
    return Shimmer.fromColors(
      baseColor: const Color.fromARGB(255, 145, 145, 145),
      highlightColor: const Color.fromARGB(255, 181, 181, 181),
      child: Container(
        margin: const EdgeInsets.only(bottom: 10, left: 10),
        width: 150,
        height: 130,
        decoration: BoxDecoration(
          color: ColorManager.white,
          borderRadius: BorderRadius.circular(15),
        ),
      ),
    );
  }
}
