import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import '../../ressources/color_manager.dart';
import '../../ressources/styles_manager.dart';
import '../../ressources/values_manager.dart';

class OtherMenuCardComponent extends StatefulWidget {
  const OtherMenuCardComponent(
      {Key? key,
      required this.assetImage,
      required this.titre,
      this.cardColor,
      required this.child})
      : super(key: key);
  final String assetImage;
  final String titre;
  final Color? cardColor;
  final Widget child;
  @override
  State<OtherMenuCardComponent> createState() => _OtherMenuCardComponentState();
}

class _OtherMenuCardComponentState extends State<OtherMenuCardComponent> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          PageTransition(
            type: PageTransitionType.rightToLeft,
            child: widget.child,
            isIos: true,
            duration: const Duration(milliseconds: 400),
          ),
        );
      },
      child: Container(
        padding: const EdgeInsets.all(AppPadding.p2),
        height: AppSize.s55,
        width: AppSize.s100,
        decoration: BoxDecoration(
          color: widget.cardColor ?? ColorManager.lightGrey,
          borderRadius: BorderRadius.circular(AppSize.s5),
        ),
        child: Column(children: [
          Image.asset(
            widget.assetImage,
            width: 35,
          ),
          Text(widget.titre,
              style: getRegularTextStyle(color: ColorManager.black)),
        ]),
      ),
    );
  }
}
