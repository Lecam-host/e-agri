import 'package:eagri/presentations/home/components/recent_consul_product.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../domain/model/user_model.dart';
import '../../conseils/components/list_last_advice.dart';
import 'services.dart';
import 'home_header.dart';
import 'last_product.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
      builder: (context, user, child) => const SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              HomeHeader(),
              // SizedBox(height: getProportionateScreenWidth(20)),
              Services(),
              // SizedBox(height: getProportionateScreenWidth(30)),
              // const Carousel(),
              // SizedBox(height: getProportionateScreenWidth(30)),
              // const DiscountBanner(),
              // const SizedBox(
              //   height: 20,
              // ),

              // const SpecialOffers(),
              LastProduct(),

              RecentConsultProducts(),
              ListLastAdvice(),

              // SizedBox(height: getProportionateScreenWidth(30)),
            ],
          ),
        ),
      ),
    );
  }
}
