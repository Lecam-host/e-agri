import 'dart:async';

import '../base/base_view_model.dart';

class HomeViewModel extends BaseViewModel {
  final StreamController streamController = StreamController();
  @override
  void dispose() {}

  @override
  void start() {}
}

abstract class HomeViewModelInputs {
  // implementations des entrees
}

abstract class HomeViewModelOutput {}
