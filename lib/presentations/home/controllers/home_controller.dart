import 'package:eagri/presentations/shop/controller/shop_controller.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  ShopController shopController = Get.put(ShopController());

  var pageHomeIndex = 0.obs;
  @override
  @override
  Future<void> refresh() async {
    shopController.getLastProduct();
  }
}
