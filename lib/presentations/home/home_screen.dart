import 'package:eagri/presentations/account/account_view.dart';
import 'package:eagri/presentations/home/components/home_body.dart';
import 'package:eagri/presentations/home/controllers/home_controller.dart';
import 'package:eagri/presentations/market_price/market_price_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

import '../../domain/model/user_model.dart';
import '../common/coustom_bottom_nav_bar.dart';
import '../common/enums.dart';
import '../ressources/color_manager.dart';
import '../ressources/size_config.dart';
import '../shop/shop_home.dart';

class HomeScreen extends StatefulWidget {
  static String routeName = "/home";
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<Widget> listeMenu(List<String> scopes) => [
        const HomeHomeView(),
        const MarketPriceView(),
        const ShopHome(),
       // const ShopScreen(),
        const AccountPageView()
      ];
  @override
  Widget build(BuildContext context) {
    final HomeController homeController = Get.put(HomeController());
    SizeConfig().init(context);
    return Consumer<UserModel>(
      builder: (context, user, child) => Scaffold(
        backgroundColor: ColorManager.backgroundColor,
        body:  Obx(() {
          return listeMenu(user.scopes??[])[homeController.pageHomeIndex.value];
        }),
        bottomNavigationBar:
            const CustomBottomNavBar(selectedMenu: MenuState.home),
      ),
    );
  }
}
