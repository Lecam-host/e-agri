import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:flutter/material.dart';

import '../../app/functions.dart';
import '../../domain/model/history_achat_model.dart';
import '../ressources/color_manager.dart';
import '../ressources/styles_manager.dart';

class DetailsHistoryAchatView extends StatefulWidget {
  const DetailsHistoryAchatView({Key? key, required this.productAchete})
      : super(key: key);
  final AchatElement productAchete;
  @override
  State<DetailsHistoryAchatView> createState() =>
      _DetailsHistoryAchatViewState();
}

class _DetailsHistoryAchatViewState extends State<DetailsHistoryAchatView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButtonCustom(),
        title: const Text("Détails de l'historique"),
      ),
      body: Container(
        margin: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(color: ColorManager.white),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Nombre de produit : ${widget.productAchete.data.products.length}',
                    style: getRegularTextStyle(
                      color: ColorManager.black,
                      fontSize: 13,
                    ),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Total payé : ${widget.productAchete.data.amount} FCFA',
                        style: getBoldTextStyle(
                          color: ColorManager.red,
                          fontSize: 12,
                        ),
                      ),

                      // Container(
                      //   padding: const EdgeInsets.all(5),
                      //   decoration: BoxDecoration(
                      //     color: ColorManager.primary.withOpacity(0.2),
                      //     borderRadius: BorderRadius.circular(10),
                      //   ),
                      //   child: Text(
                      //     'Payée',
                      //     style: getMeduimTextStyle(
                      //       color: ColorManager.primary,
                      //       fontSize: 10,
                      //     ),
                      //   ),
                      // ),
                    ],
                  ),
                  Text(
                    convertDateWithTime(
                        widget.productAchete.data.datePaiement.toString()),
                    style: getRegularTextStyle(
                      color: ColorManager.grey,
                      // fontSize: 10,
                    ),
                  ),
                  // if (widget.commandModel.status.libelle == "PAID")
                  //   InkWell(
                  //     onTap: () {
                  //       commandCrontroller
                  //           .getCommandFacture(widget.commandModel.id);
                  //       //  Get.to(CommandFacture());
                  //     },
                  //     child: Text(
                  //       "Voir la facture",
                  //       style: getBoldTextStyle(color: ColorManager.blue),
                  //     ),
                  //   ),
                ],
              ),
            ),

            const Divider(),
            // InkWell(
            //     onTap: () {
            //       commandCrontroller.getCommandFacture(widget.productAchete.id);
            //       //  Get.to(CommandFacture());
            //     },
            //     child: Text(
            //       "Voir la facture",
            //       style: getBoldTextStyle(color: ColorManager.blue),
            //     ),
            //   ),
            Text(
              "Liste des produits",
              style: getSemiBoldTextStyle(
                color: ColorManager.black,
                fontSize: 12,
              ),
            ),
            ListView.builder(
              shrinkWrap: true,
              itemCount: widget.productAchete.data.products.length,
              itemBuilder: (context, index) => Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  color: ColorManager.white,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      if (widget.productAchete.data.products[index].product
                          .images.isNotEmpty)
                        CachedNetworkImage(
                          imageUrl: widget.productAchete.data.products[index]
                                  .product.images.isNotEmpty
                              ? widget.productAchete.data.products[index]
                                  .product.images[0].link
                              : widget.productAchete.data.products[index]
                                  .product.images[0].link,
                          imageBuilder: (context, imageProvider) => Container(
                            width: 70,
                            height: 70,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                scale: 1,
                                image: imageProvider,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          placeholder: (context, url) => Container(
                            height: 70,
                            width: 70,
                            color: ColorManager.grey1.withOpacity(0.2),
                          ),
                          errorWidget: (context, url, error) => Container(
                            height: 70,
                            width: 70,
                            color: ColorManager.grey1.withOpacity(0.2),
                          ),
                        ),
                      if (widget.productAchete.data.products[index].product
                          .images.isEmpty)
                        Container(
                          height: 70,
                          width: 70,
                          color: ColorManager.grey1.withOpacity(0.2),
                          child: const Icon(Icons.image),
                        ),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            if(widget.productAchete.data.products[index]
                                    .product.speculation!=null)
                            SizedBox(
                              child: Text(
                                widget.productAchete.data.products[index]
                                    .product.speculation!.libelle,
                                style: getMeduimTextStyle(
                                  color: ColorManager.black,
                                  fontSize: 12,
                                ),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                              ),
                            ),
                            Text(
                              "Qte : ${widget.productAchete.data.products[index].quantity.toString()}",
                              style: getRegularTextStyle(
                                color: ColorManager.black,
                                fontSize: 12,
                              ),
                            ),
                            Text(
                              "Total : ${widget.productAchete.data.products[index].price.toString()} FCFA",
                              style: getRegularTextStyle(
                                color: ColorManager.red,
                                fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
