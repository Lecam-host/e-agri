import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/app/functions.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';

import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'historique_controller.dart';

enum TypeVente { achat, vente }

class HistoriqueVenteView extends StatefulWidget {
  const HistoriqueVenteView({super.key});

  @override
  State<HistoriqueVenteView> createState() => _HistoriqueVenteViewState();
}

class _HistoriqueVenteViewState extends State<HistoriqueVenteView> {
  HistoriqueController historiqueController = Get.put(HistoriqueController());
  @override
  void initState() {
    historiqueController.getUserHistoryVente();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => historiqueController.loadHistoryVente.value == false
          ? SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.all(10),
                    margin: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: ColorManager.lightGrey,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text("Total"),
                            Obx(
                              () => Text(
                                "${separateur(historiqueController.totalVente.toDouble())} FCFA",
                                style: getBoldTextStyle(fontSize: 16),
                              ),
                            )
                          ],
                        ),
                        // const Spacer(),
                        Container(
                          width: 2,
                          height: 50,
                          decoration: BoxDecoration(
                            color: ColorManager.black,
                          ),
                        ),
                        // const Spacer(),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text("Nbre de produit"),
                            Text(
                              historiqueController.historyDataVente.length
                                  .toString(),
                              style: getBoldTextStyle(
                                  fontSize: 16, color: ColorManager.black),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  //  Text("Transaction"),
                  Obx(
                    () => historiqueController.historyDataVente.isNotEmpty
                        ? ListView.builder(
                            physics: const NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount:
                                historiqueController.historyDataVente.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Container(
                                decoration: BoxDecoration(
                                  color: ColorManager.white,
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                padding: const EdgeInsets.all(10),
                                margin: const EdgeInsets.all(10),
                                child: Row(
                                  children: [
                                    if (historiqueController
                                        .historyDataVente[index]
                                        .product
                                        .images
                                        .isNotEmpty)
                                      CachedNetworkImage(
                                        imageUrl: historiqueController
                                            .historyDataVente[index]
                                            .product
                                            .images[0]
                                            .link,
                                        imageBuilder:
                                            (context, imageProvider) =>
                                                Container(
                                          width: 70,
                                          height: 70,
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                              scale: 1,
                                              image: imageProvider,
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                        ),
                                        placeholder: (context, url) =>
                                            Container(
                                          height: 70,
                                          width: 70,
                                          color: ColorManager.grey1
                                              .withOpacity(0.2),
                                        ),
                                        errorWidget: (context, url, error) =>
                                            Container(
                                          height: 70,
                                          width: 70,
                                          color: ColorManager.grey1
                                              .withOpacity(0.2),
                                        ),
                                      ),
                                    if (historiqueController
                                        .historyDataVente[index]
                                        .product
                                        .images
                                        .isEmpty)
                                      Container(
                                        height: 70,
                                        width: 70,
                                        color:
                                            ColorManager.grey1.withOpacity(0.2),
                                        child: const Icon(Icons.image),
                                      ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          convertHourAndDate(
                                              historiqueController
                                                  .historyDataVente[index]
                                                  .datePaiement
                                                  .toString()),
                                          style: getRegularTextStyle(
                                            color: ColorManager.grey,
                                          ),
                                        ),
                                        Text.rich(
                                          TextSpan(
                                            text: "Qte : ",
                                            style: getSemiBoldTextStyle(
                                              color: ColorManager.black,
                                            ),
                                            children: [
                                              TextSpan(
                                                text: historiqueController
                                                    .historyDataVente[index]
                                                    .quantity
                                                    .toString(),
                                                style: getBoldTextStyle(
                                                  fontSize: 14,
                                                  color: ColorManager.black,
                                                ),
                                              ),
                                              TextSpan(
                                                text:
                                                    " ${historiqueController.historyDataVente[index].product.unityName}",
                                                style: getBoldTextStyle(
                                                  fontSize: 14,
                                                  color: ColorManager.black,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Text.rich(
                                          TextSpan(
                                            text: "Total : ",
                                            style: getSemiBoldTextStyle(
                                              color: ColorManager.black,
                                            ),
                                            children: [
                                              TextSpan(
                                                text: historiqueController
                                                    .historyDataVente[index]
                                                    .price
                                                    .toString(),
                                                style: getBoldTextStyle(
                                                  fontSize: 14,
                                                  color: ColorManager.primary,
                                                ),
                                              ),
                                              TextSpan(
                                                text: " FCFA",
                                                style: getBoldTextStyle(
                                                  fontSize: 14,
                                                  color: ColorManager.primary,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Text(
                                          convertHourAndDate(
                                              historiqueController
                                                  .historyDataVente[index]
                                                  .datePaiement
                                                  .toString()),
                                          style: getRegularTextStyle(
                                            color: ColorManager.grey,
                                          ),
                                        ),
                                      ],
                                    ),
                                    const Spacer(),
                                    // Text(
                                    //   "${historiqueController.historyDataVente[index].data.amount.toString()} FCFA",
                                    //   style: getMeduimTextStyle(
                                    //     fontSize: 14,
                                    //     color: ColorManager.red,
                                    //   ),
                                    // ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    // Container(
                                    //   decoration: BoxDecoration(
                                    //     shape: BoxShape.circle,
                                    //     // color: ColorManager.grey
                                    //     //     .withOpacity(0.3),
                                    //   ),
                                    //   child: const Center(
                                    //       child: Icon(
                                    //     Icons.chevron_right,
                                    //   )),
                                    // )
                                  ],
                                ),
                              );
                            },
                          )
                        : const EmptyComponenent(),
                  ),

                  // CartItemHistory(),
                ],
              ),
            )
          : const LoaderComponent(),
    );
  }
}

class CartItemHistory extends StatelessWidget {
  const CartItemHistory({
    Key? key,
    this.type,
  }) : super(key: key);
  final TypeVente? type;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: ColorManager.primary4,
      onTap: () {},
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: ColorManager.white,
        ),
        padding: const EdgeInsets.all(5),
        child: Row(
          children: [
            Container(
              width: 60.0,
              height: 60,
              decoration: BoxDecoration(
                color: ColorManager.grey,
                borderRadius: BorderRadius.circular(5),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(5.0),
                // child: CacheNetworkImage(
                //   image: product.images![0],
                // ),
              ),
            ),
            const SizedBox(
              width: 5,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    type == TypeVente.achat ? "Achat" : "Vente",
                    style: getBoldTextStyle(
                      color: type == TypeVente.achat
                          ? Colors.red
                          : ColorManager.primary,
                      fontSize: 14,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Row(
                    children: [
                      Text(
                        "Maïs - jaune",
                        style: getRegularTextStyle(
                          color: ColorManager.black,
                          fontSize: 14,
                        ),
                      ),
                      const Spacer(),
                      Column(
                        children: [
                          Text(
                            "2 500 fcfa",
                            style: getRegularTextStyle(
                              color: ColorManager.primary,
                              fontSize: 12,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  // Row(
                  //   crossAxisAlignment: CrossAxisAlignment.center,
                  //   mainAxisAlignment: MainAxisAlignment.start,
                  //   children: [
                  //     Text(
                  //       "200 vues",
                  //       style: getRegularTextStyle(
                  //         color: ColorManager.black,
                  //         fontSize: 12,
                  //       ),
                  //     ),
                  //     Spacer(),
                  //     // Text(
                  //     //   "3.5",
                  //     //   style: getRegularTextStyle(
                  //     //     color: ColorManager.black,
                  //     //     fontSize: 12,
                  //     //   ),
                  //     // ),
                  //     // SvgPicture.asset(SvgManager.star),
                  //   ],
                  // ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
