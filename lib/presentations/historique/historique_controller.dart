import 'dart:developer';

import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/app/app_prefs.dart';
import 'package:eagri/data/repository/api/repository_impl.dart';
import 'package:eagri/data/request/history_vente_achat.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';

import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:get/get.dart';

import '../../app/di.dart';
import '../../controllers/user_controller.dart';
import '../../domain/model/history_achat_model.dart';
import '../../domain/model/history_vente_model.dart';

class HistoriqueController extends GetxController {
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  UserController userController = Get.put(UserController());
  AppSharedPreference appSharedPreference = instance<AppSharedPreference>();
  var historyDataAchat = <AchatElement>[].obs;
  var historyDataVente = <VenteProductElement>[].obs;

  var loadHistoryAchat = true.obs;
  var loadHistoryVente = true.obs;

  var totalAchat = 0.obs;
  var totalVente = 0.obs;

  var totalCommand = 0.obs;

  getUserHistoryAchat() async {
    loadHistoryAchat.value = true;
    totalAchat.value = 0;
    await appSharedPreference.getUserInformation().then((value) async {
      await repositoryImpl
          .getUserHistoryAchat(
            GetUserHistoryAchatVente(userId: value.id!, page: 0, perPage: 50),
          )
          .then((response) => response.fold((failure) {
                inspect(failure);
                showCustomFlushbar(Get.context!, failure.message,
                    ColorManager.error, FlushbarPosition.TOP);
              }, (historyResponse) {
                historyDataAchat.value = historyResponse.data;
                Future.forEach(historyDataAchat, (AchatElement element) {
                  totalAchat = totalAchat + element.data.amount.toInt();
                });
              }));
    });

    loadHistoryAchat.value = false;
  }

  getUserHistoryVente() async {
    loadHistoryVente.value = true;
    await appSharedPreference.getUserInformation().then((value) async {
      await repositoryImpl
          .getUserHistoryVente(
            GetUserHistoryAchatVente(userId: value.id!, page: 0, perPage: 50),
          )
          .then((response) => response.fold((failure) {
                inspect(failure);
              }, (historyResponse) {
                historyDataVente.value = historyResponse.data.products;
                totalVente.value = historyResponse.data.amount;
              }));
    });

    loadHistoryVente.value = false;
  }
}
