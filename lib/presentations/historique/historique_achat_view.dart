import 'package:eagri/app/functions.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';

import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'details_history_achat_view.dart';
import 'historique_controller.dart';

enum TypeVente { achat, vente }

class HistoriqueAchatView extends StatefulWidget {
  const HistoriqueAchatView({super.key});

  @override
  State<HistoriqueAchatView> createState() => _HistoriqueAchatViewState();
}

class _HistoriqueAchatViewState extends State<HistoriqueAchatView> {
  HistoriqueController historiqueController = Get.put(HistoriqueController());
  @override
  void initState() {
    historiqueController.getUserHistoryAchat();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => historiqueController.loadHistoryAchat.value == false
          ? SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.all(10),
                    margin: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: ColorManager.lightGrey,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text("Total"),
                            Obx(
                              () => Text(
                                "${separateur(historiqueController.totalAchat.toDouble())} FCFA",
                                style: getBoldTextStyle(fontSize: 16),
                              ),
                            )
                          ],
                        ),
                        // const Spacer(),
                        Container(
                          width: 2,
                          height: 50,
                          decoration: BoxDecoration(
                            color: ColorManager.black,
                          ),
                        ),
                        // const Spacer(),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text("Nbre commande"),
                            Text(
                              historiqueController.historyDataAchat.length
                                  .toString(),
                              style: getBoldTextStyle(
                                  fontSize: 16, color: ColorManager.black),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  //  Text("Transaction"),
                  Obx(
                    () => historiqueController.historyDataAchat.isNotEmpty
                        ? ListView.builder(
                            physics: const NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount:
                                historiqueController.historyDataAchat.length,
                            itemBuilder: (BuildContext context, int index) {
                              return InkWell(
                                onTap: () {
                                  Get.to(DetailsHistoryAchatView(
                                    productAchete: historiqueController
                                        .historyDataAchat[index],
                                  ));
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: ColorManager.white,
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  padding: const EdgeInsets.all(10),
                                  margin: const EdgeInsets.all(10),
                                  child: Row(
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text.rich(
                                            TextSpan(
                                              text: "Nbre produit : ",
                                              style: getSemiBoldTextStyle(
                                                color: ColorManager.black,
                                              ),
                                              children: [
                                                TextSpan(
                                                  text:
                                                      "${historiqueController.historyDataAchat[index].data.products.length} ",
                                                  style: getBoldTextStyle(
                                                    fontSize: 14,
                                                    color: ColorManager.black,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Text(
                                            convertHourAndDate(
                                                historiqueController
                                                    .historyDataAchat[index]
                                                    .data
                                                    .datePaiement
                                                    .toString()),
                                            style: getRegularTextStyle(
                                              color: ColorManager.grey,
                                            ),
                                          ),
                                        ],
                                      ),
                                      const Spacer(),
                                      Text(
                                        "${historiqueController.historyDataAchat[index].data.amount.toString()} FCFA",
                                        style: getMeduimTextStyle(
                                          fontSize: 14,
                                          color: ColorManager.red,
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Container(
                                        decoration: const BoxDecoration(
                                          shape: BoxShape.circle,
                                          // color: ColorManager.grey
                                          //     .withOpacity(0.3),
                                        ),
                                        child: const Center(
                                            child: Icon(
                                          Icons.chevron_right,
                                        )),
                                      )
                                    ],
                                  ),
                                ),
                              );
                            },
                          )
                        : const EmptyComponenent(),
                  ),

                  // CartItemHistory(),
                ],
              ),
            )
          : const LoaderComponent(),
    );
  }
}

class CartItemHistory extends StatelessWidget {
  const CartItemHistory({
    Key? key,
    this.type,
  }) : super(key: key);
  final TypeVente? type;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: ColorManager.primary4,
      onTap: () {},
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: ColorManager.white,
        ),
        padding: const EdgeInsets.all(5),
        child: Row(
          children: [
            Container(
              width: 60.0,
              height: 60,
              decoration: BoxDecoration(
                color: ColorManager.grey,
                borderRadius: BorderRadius.circular(5),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(5.0),
                // child: CacheNetworkImage(
                //   image: product.images![0],
                // ),
              ),
            ),
            const SizedBox(
              width: 5,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    type == TypeVente.achat ? "Achat" : "Vente",
                    style: getBoldTextStyle(
                      color: type == TypeVente.achat
                          ? Colors.red
                          : ColorManager.primary,
                      fontSize: 14,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Row(
                    children: [
                      Text(
                        "Maïs - jaune",
                        style: getRegularTextStyle(
                          color: ColorManager.black,
                          fontSize: 14,
                        ),
                      ),
                      const Spacer(),
                      Column(
                        children: [
                          Text(
                            "2 500 fcfa",
                            style: getRegularTextStyle(
                              color: ColorManager.primary,
                              fontSize: 12,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  // Row(
                  //   crossAxisAlignment: CrossAxisAlignment.center,
                  //   mainAxisAlignment: MainAxisAlignment.start,
                  //   children: [
                  //     Text(
                  //       "200 vues",
                  //       style: getRegularTextStyle(
                  //         color: ColorManager.black,
                  //         fontSize: 12,
                  //       ),
                  //     ),
                  //     Spacer(),
                  //     // Text(
                  //     //   "3.5",
                  //     //   style: getRegularTextStyle(
                  //     //     color: ColorManager.black,
                  //     //     fontSize: 12,
                  //     //   ),
                  //     // ),
                  //     // SvgPicture.asset(SvgManager.star),
                  //   ],
                  // ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
