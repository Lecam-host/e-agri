import 'package:eagri/app/functions.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/historique/historique_achat_view.dart';
import 'package:eagri/presentations/historique/historique_vente_view.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../app/constant.dart';
import '../../domain/model/user_model.dart';
import '../common/not_permission_component.dart';

class HistoriqueView extends StatelessWidget {
  const HistoriqueView({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
      builder: (context, user, child) => DefaultTabController(
      length: 2,
      child: Scaffold(
          backgroundColor: ColorManager.backgroundColor,
          body: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  leading: const BackButtonCustom(),
                  centerTitle: true,
                  title: Text(
                    'Historiques',
                    style: TextStyle(
                      fontSize: 15,
                      fontStyle: FontStyle.normal,
                      color: ColorManager.black,
                    ),
                  ),
                  pinned: true,
                  floating: true,
                  bottom: TabBar(
                    isScrollable: true,
                    tabs: [
                      Tab(
                        child: Text(
                          'Liste des articles achetés',
                          style: getBoldTextStyle(fontSize: 14),
                        ),
                      ),
                      Tab(
                        child: Text(
                          'Liste des articles vendus',
                          style: getBoldTextStyle(fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                ),
              ];
            },
            body:  TabBarView(
              children: <Widget>[
                if(isInScopes( UserPermissions.HISTORIQUETRANSCATION_HISTORIQUEACHAT_VOIR,user.scopes!))...[
                 const  HistoriqueAchatView(),
                ]else...[const NotPermissionComponent()],
                if(isInScopes( UserPermissions.HISTORIQUETRANSCATION_HISTORIQUEACHAT_VOIR,user.scopes!))...[
                 const  HistoriqueVenteView(),
                ]else...[const NotPermissionComponent()],
               
              ],
            ),
          )),
    ));
  }
}
