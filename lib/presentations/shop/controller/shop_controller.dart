import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:eagri/app/constant.dart';
import 'package:eagri/app/extensions.dart';
import 'package:eagri/data/mapper/shop_mapper.dart';
import 'package:eagri/data/repository/api/repository_impl.dart';
import 'package:eagri/data/repository/local/speculation_repo.dart';
import 'package:eagri/data/request/shop_resquests.dart';
import 'package:eagri/domain/model/Product.dart';
import 'package:eagri/domain/model/model.dart';
import 'package:eagri/domain/usecase/shop_usecase.dart';
import 'package:eagri/presentations/search_filter/controller/filter_controller.dart';
import 'package:eagri/presentations/shop/shop_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../app/app_prefs.dart';
import '../../../app/di.dart';
import '../../../controllers/location_controller.dart';
import '../../../controllers/speculation_controller.dart';
import '../../../domain/model/category_model.dart';
import '../../../domain/model/intrant_model.dart';
import '../../../domain/model/shop_model.dart';
import '../../common/state/loader_component.dart';
import '../../search_filter/list_product_search.dart';
import '../list_offre_achat.dart';
import '../list_offre_achat_find.dart';

class ShopController extends GetxController {
  final ScrollController scrollController0 = ScrollController();
  final ScrollController scrollController1 = ScrollController();
  final ScrollController scrollController2 = ScrollController();

  final FilterController filterController = Get.put(FilterController());
  var speculationController = Get.put(SpeculationController());
  LocationController locationController = Get.put(LocationController());

  var isFilter = false.obs;
  var lastPage = 0.obs;
  var nextPage = 0.obs;
  var totalPage = 0.obs;
  var shopPageIndex = 0.obs;

  var totalObjectInBd = 0.obs;

  var loadMore = false.obs;
  var refreshList = false.obs;

  var isInitList = false.obs;

  var isLoadOffreVenteForAchatProduct = true.obs;

  var isListLoadMesDemandes = true.obs;
  var isListLoadMesDemandesPreAchat = true.obs;

  var initLoadOffreAchat = false.obs;
  var loadOffreAchat = false.obs;
  var isError = false.obs;

  ShopUseCase shopUseCase = instance<ShopUseCase>();

  var listeSpeculation = <SpeculationModel>[];
  var listeSpeculationWithOffreAchat = <NbSpeculationOffreAchatModel>[].obs;

  final pageOffre = TypeOffre.vente.obs;

  var listProductOffreAchatFind = <ProductModel>[].obs;

  var limitProduct = 6.obs;
  var limitPage = 0.obs;
  var listSpeculationOffreAchat = <ProductModel>[].obs;
  var listSpeculationOffreAchatShow = <ProductModel>[].obs;
  var listSpeculationOffrePreAchat = <ProductModel>[].obs;
  var listSpeculationOffrePreAchatShow = <ProductModel>[].obs;
  var lastProduct = <ProductModel>[].obs;
  var lastPrestation = <ProductModel>[].obs;
  var lastTransport = <ProductModel>[].obs;

  var similarProduct = <ProductModel>[].obs;
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  var listSpeculation = <SpeculationModel>[].obs;
  var listSearchItem = <SearchItemModel>[].obs;

  var listeSpeculationMap = <Map<String, dynamic>>[].obs;
  var listeSpeculationMapInSelectField = <Map<String, dynamic>>[].obs;
  var listCategoryMap = <Map<String, dynamic>>[].obs;
  var listCategoryIntrantMap = <Map<String, dynamic>>[].obs;
  var listCategoryIntrantMapInSelectFeild = <Map<String, dynamic>>[].obs;

  var listIntrantMap = <Map<String, dynamic>>[].obs;
  var listIntrant = <ItemIntrant>[].obs;

  var listRegionMap = <Map<String, dynamic>>[].obs;
  var noteController = const RangeValues(0, 5).obs;
  var categoryController = TextEditingController().obs;

  var productController = TextEditingController().obs;
  var regionController = TextEditingController().obs;
  var searchInput = TextEditingController().obs;
  var searchInListOffreAchatInput = TextEditingController().obs;
  var searchInListOffrePreAchatInput = TextEditingController().obs;

  var listProductAgricul = <ProductModel>[].obs;
  var listProductIntrant = <ProductModel>[].obs;

  var lastListProdIntrant = <ProductModel>[].obs;
  var allListProdIntrant = <ProductModel>[].obs;

  var listSpecultionSearch = <SpeculationModel>[].obs;
  var listCategorySearch = <CategoryModel>[].obs;

  var prixMaxController = TextEditingController().obs;
  var prixMinController = TextEditingController().obs;
  var typeOffer = "VENTE".obs;
  var listCategorieSpeculation = <CategoryModel>[].obs;
  var listCategorieIntrant = <IntrantCategoryModel>[].obs;

  var typeProductCodeInFilter = codeProductAgricultural.obs;

  init(SearchProductRequest request) {}

  notifyConsultationProduct(ConsultationProductRequest request) {
    repositoryImpl
        .notifyConsultationProduct(request)
        .then((response) => response.fold((failure) => null, (data) {}));
  }

  renitialiseFilter() {
    prixMaxController.value = TextEditingController();
    prixMinController.value = TextEditingController();
    categoryController.value = TextEditingController();
    //categoryController.value.text="0";
    listCategoryIntrantMapInSelectFeild.value = listIntrantMap;
    listeSpeculationMapInSelectField = listeSpeculationMap;
    regionController.value = TextEditingController();
    noteController.value = const RangeValues(0, 5);
    productController.value = TextEditingController();
  }

  getIntrantCategories() async {
    await repositoryImpl.getIntrantCategories().then((response) async {
      response.fold((l) {}, (list) {
        listCategorieIntrant.value = list.data;
        listCategoryIntrantMap.value = list.data
            .map((e) => {"value": e.categoryId, "label": e.libelle})
            .toList();
      });
    });
  }

  getLastProduct() async {
    await shopUseCase
        .searchProduct(
          SearchProductRequest(
            limit: 5,
            page: 0,
            offerType: "VENTE",
            status: "AVAILABLE",
            codeTypeProduit: codeProductAgricultural,
          ),
        )
        .then((response) => response.fold((failure) {
              inspect(failure);
            }, (data) async {
              lastProduct.value = (await data.toDomain());
            }));
  }

  getLastPrestation() async {
    await shopUseCase
        .searchProduct(
          SearchProductRequest(
            limit: 5,
            page: 0,
            status: "AVAILABLE",
            codeTypeProduit: codePrestation,
          ),
        )
        .then((response) => response.fold((failure) {
              inspect(failure);
            }, (data) async {
              lastPrestation.value = (await data.toDomain());
            }));
  }

  getLastTransport() async {
    await shopUseCase
        .searchProduct(
          SearchProductRequest(
            limit: 5,
            page: 0,
            // offerType: "VENTE",
            status: "AVAILABLE",
            codeTypeProduit: codeTransport,
          ),
        )
        .then((response) => response.fold((failure) {
              inspect(failure);
            }, (data) async {
              lastTransport.value = (await data.toDomain());
            }));
  }

  getLastIntrantProduct() async {
    await shopUseCase
        .searchProduct(
          SearchProductRequest(
            limit: 5,
            page: 0,
            offerType: "VENTE",
            status: "AVAILABLE",
            codeTypeProduit: codeIntrant,
          ),
        )
        .then((response) => response.fold((failure) {
              inspect(failure);
            }, (data) async {
              lastListProdIntrant.value = (await data.toDomain());
            }));
  }

  getSimilarProduct(ProductModel product) async {
    similarProduct.value = [];
    await shopUseCase
        .searchProduct(SearchProductRequest(
          limit: 10,
          page: 0,
          offerType: product.typeOffers,
          status: "AVAILABLE",
          codeTypeProduit: int.parse(product.code),
          speculationId: product.speculation.speculationId != null
              ? [product.speculation.speculationId!]
              : null,
          priceMax: product.price + (product.price * 0.2),
          priceMin: product.price - (product.price * 0.2),
          quantityMax: product.quantity + (product.quantity * 0.2).toInt(),
          quantityMin: product.quantity - (product.quantity * 0.2).toInt(),
        ))
        .then((response) => response.fold((failure) {}, (data) async {
              List<ProductModel> prods = await data.toDomain();
              await Future.forEach(prods, (ProductModel prod) {
                if (prod.id != product.id) {
                  similarProduct.add(prod);
                }
              });
            }));
  }

  @override
  void onInit() async {
    listRegionMap.value = locationController.listRegionMap;
    listeSpeculationMapInSelectField.value =
        speculationController.listeSpeculationMap;
    listeSpeculationMap.value = speculationController.listeSpeculationMap;
    listSpeculation.value = speculationController.listSpeculation;
    listCategoryMap.value = speculationController.listCategorysMap;
    getLastPrestation();
    getLastTransport();
    getLastProduct();
    getListProduct(0);
    getNbSpecultionOffreAchat();
    getListCategoryProduct();
    getIntrantCategories();
    fetchIntrantAll();
    scrollController0.addListener(_scrollListener);
    scrollController1.addListener(_scrollListener);

    super.onInit();
  }

  // getListProduct() {
  //   if (shopPageIndex.value == 0) {
  //     getListProductVente();
  //   } else {
  //     getListProductAchat();
  //   }
  // }

  filterProduct() async {
    isFilter.value = true;
    isInitList.value = true;

    listProductAgricul.value = [];
    listProductIntrant.value = [];

    Get.back(result: typeProductCodeInFilter.value == codeIntrant ? 1 : 0);
    getListProduct(
      shopPageIndex.value,
    );
  }

  refreshListProduct() async {
    loadMore.value = false;
    nextPage.value = 0;

    await getListProduct(
      shopPageIndex.value,
    );
  }

  //fetchIntrantAll

  fetchIntrantAll() async {
    await repositoryImpl.getIntrantAll().then((value) {
      value.fold((l) {
        inspect(l);
      }, (r) {
        inspect(r);
        if (r.data != null) {
          if (r.data!.items != null) {
            listIntrant.value = r.data!.items!;

            listIntrantMap.value = r.data!.items!
                .map((e) => {"value": e.id, "label": e.libelle})
                .toList();
            listeSpeculationMapInSelectField.value = r.data!.items!
                .map((e) => {"value": e.id, "label": e.libelle})
                .toList();
          }
        }
      });
    });
  }

  filterIntrantFnCategorie(int idCategorie) {
    listCategoryIntrantMapInSelectFeild.value = [];
    for (ItemIntrant element in listIntrant) {
      if (element.categorieId == idCategorie) {
        listCategoryIntrantMapInSelectFeild
            .add({"value": element.id, "label": element.libelle});
        update();
      }
    }
  }

  filterProdAgricolFnCategorie(int idCategorie) {
    listeSpeculationMapInSelectField.value = [];

    for (SpeculationModel element in listSpeculation) {
      if (element.categoryInfo!.categorieId == idCategorie) {
        listeSpeculationMapInSelectField
            .add({"value": element.speculationId, "label": element.name});
      }
    }
  }
  // getListProduct(int indexPage) async {
  //   getList();
  // }

  getListProduct(
    int indexPage,
  ) async {
    // isError.value = false;

    await shopUseCase
        .searchProduct(
      SearchProductRequest(
          codeTypeProduit:
              indexPage == 0 ? codeProductAgricultural : codeIntrant,
          // offerType: shopPageIndex.value == 0 ? "VENTE" : "ACHAT",
          limit: Constant.offset,
          page: nextPage.value,
          categorieId: categoryController.value.text != "" &&
                  categoryController.value.text != "0"
              ? int.parse(categoryController.value.text)
              : null,
          speculationId: productController.value.text != ""
              ? [int.parse(productController.value.text)]
              : null,
          regionId: regionController.value.text != ""
              ? int.parse(regionController.value.text)
              : null,
          noteMax: noteController.value.end,
          noteMin: noteController.value.start,
          priceMax: prixMaxController.value.text != ""
              ? double.parse(prixMaxController.value.text)
              : null,
          priceMin: prixMinController.value.text != ""
              ? double.parse(prixMinController.value.text)
              : null,
          status: "AVAILABLE",
          offerType: typeOffer.value),
    )
        .then((response) async {
      await response.fold(
        (l) async {
          if (isInitList.value == true) {
            isError.value = true;
          }
          loadMore.value = false;
          isInitList.value = false;
        },
        (searchData) async {
          totalObjectInBd.value = searchData.totalObject.orZero();
          lastPage.value = searchData.previousPage.orZero();
          totalPage.value = searchData.totalOfPages.orZero();
          nextPage.value = searchData.nextPage.orZero();
          if (loadMore.value == true) {
            List<ProductModel> prods = await searchData.toDomain();
            if (indexPage == 0) {
              listProductAgricul.addAll(prods);
            } else {
              listProductIntrant.addAll(prods);
            }

            /// offre de vente
          } else {
            List<ProductModel> prods = await searchData.toDomain();
            if (indexPage == 0) {
              listProductAgricul.value = prods;
            } else {
              listProductIntrant.value = prods;
            }

            //offre dachat
          }
          loadMore.value = false;
          isInitList.value = false;

          //update();
        },
      );
    });
  }

  gitUserListOffreAchat() async {
    isListLoadMesDemandes.value = true;
    Get.to(const ListSpeculationOffreAchat());

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    AppSharedPreference(sharedPreferences)
        .getUserInformation()
        .then((user) async {
      await shopUseCase
          .searchProduct(
            SearchProductRequest(
                offerType: "ACHAT",
                limit: 100,
                page: 0,
                fournisseurId: user.id),
          )
          .then((response) => response.fold((l) {
                Get.back();
                inspect(l);
              }, (searchData) async {
                inspect(searchData);
                if (searchData.data != null) {
                  if (searchData.data!.isNotEmpty) {
                    listSpeculationOffreAchatShow.value = [];
                    listSpeculationOffreAchat.value = [];
                    listSpeculationOffreAchatShow
                        .addAll(await searchData.toDomain());
                    listSpeculationOffreAchat = listSpeculationOffreAchatShow;
                  }
                }
                isListLoadMesDemandes.value = false;
              }));

      // Get.back();
    });
  }

  gitUserListOffrePreAchat() async {
    isListLoadMesDemandesPreAchat.value = true;
    Get.to(const ListSpeculationOffreAchat());

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    AppSharedPreference(sharedPreferences)
        .getUserInformation()
        .then((user) async {
      await shopUseCase
          .searchProduct(
            SearchProductRequest(
                offerType: "PREACHAT",
                limit: 100,
                page: 0,
                fournisseurId: user.id),
          )
          .then((response) => response.fold((l) {
                Get.back();
                isListLoadMesDemandesPreAchat.value = false;
              }, (searchData) async {
                inspect(searchData);
                if (searchData.data != null) {
                  if (searchData.data!.isNotEmpty) {
                    listSpeculationOffrePreAchatShow.value = [];
                    listSpeculationOffrePreAchat.value = [];
                    listSpeculationOffrePreAchatShow
                        .addAll(await searchData.toDomain());
                    listSpeculationOffrePreAchat =
                        listSpeculationOffrePreAchatShow;
                  }
                }
                isListLoadMesDemandesPreAchat.value = false;
              }));

      // Get.back();
    });
  }

  Future<ProductModel> getDetailsProduct(ProductModel product) async {
    ProductModel productDetails = product;
    await shopUseCase.getDetailProduct(product.id).then((response) async {
      await response.fold((l) {}, (details) async {
        await details.then((value) {
          productDetails = value!;
        });
      });
    });

    return productDetails;
  }

  findOffreAchat(ProductModel product) async {
    isLoadOffreVenteForAchatProduct.value = true;
    listProductOffreAchatFind.value = [];
    Get.dialog(const LoaderComponent());
    await repositoryImpl.getListOffreAchatFind(product.id).then(
          (response) => response.fold(
            (failure) {
              isLoadOffreVenteForAchatProduct.value = false;
              Get.back();
            },
            (data) async {
              await Future.forEach(
                data.data,
                (int idProduct) async {
                  await shopUseCase.getDetailProduct(idProduct).then(
                    (response) async {
                      await response.fold(
                        (failure) {},
                        (data) async {
                          if (await data != null) {
                            listProductOffreAchatFind.add((await data)!);
                          }
                        },
                      );
                    },
                  );
                },
              );
              isLoadOffreVenteForAchatProduct.value = false;
              Get.back();
              Get.to(ListOffreAchatFind(
                listProd: listProductOffreAchatFind,
                product: product,
              ));
            },
          ),
        );
  }

  resetFilter() {
    isFilter.value = false;
    nextPage.value = 0;
    loadMore.value = false;
    isInitList.value = false;
    categoryController.value = TextEditingController();

    regionController.value = TextEditingController();
    prixMaxController.value = TextEditingController();
    prixMinController.value = TextEditingController();
    productController.value = TextEditingController();
    listCategoryIntrantMapInSelectFeild.value = listIntrantMap;
    listeSpeculationMapInSelectField = listeSpeculationMap;
    getListProduct(0);
    // getList();
    Get.back(result: 0);
  }

  // getList(bool isRefresh) {
  //   if (isRefresh == true) {
  //     listProd.value = [];
  //   }
  //   if (isFilter.value == true) {
  //     // filterProduct();
  //   } else {
  //     //getListProduct(limitPage.value);
  //   }
  // }
  getListCategoryProduct() async {
    await repositoryImpl.getListCategoryProduct().then((response) {
      response.fold((l) => null, (responseData) {
        listCategorieSpeculation.value = responseData.data;
      });
    });
  }

  void _scrollListener() async {
    limitPage.value = nextPage.value;

    if (shopPageIndex.value == 0) {
      if (scrollController0.offset >=
              scrollController0.position.maxScrollExtent &&
          !scrollController0.position.outOfRange) {
        await loadList();
      }
    } else if (shopPageIndex.value == 1) {
      if (scrollController1.hasClients) {
        if (scrollController1.offset >=
                scrollController1.position.maxScrollExtent &&
            !scrollController1.position.outOfRange) {
          await loadList();
        }
      }
    }
  }

  getlistSpreculationOffreAchat(
      bool isInit, SpeculationModel speculationOffreAchat) async {
    if (isInit == true) {
      initLoadOffreAchat = true.obs;
    } else {
      loadOffreAchat = true.obs;
    }
    isInitList.value = true;

    await shopUseCase
        .searchProduct(SearchProductRequest(
            limit: 10,
            page: 0,
            offerType: "ACHAT",
            speculationId: [speculationOffreAchat.speculationId!]))
        .then((value) => value.fold((l) {
              loadMore.value = false;
              initLoadOffreAchat = false.obs;
              loadOffreAchat = false.obs;
              refreshList.value = false;
              isInitList.value = true;
            }, (right) async {
              List<ProductModel> listProd = [];
              listProd = await right.toDomain();
              inspect(right);
              if (isInit == true) {
                listSpeculationOffreAchat.value = [];
                listSpeculationOffreAchat.addAll(listProd);
              } else {
                listSpeculationOffreAchat.addAll(listProd);
              }
            }));
  }

  loadList() async {
    if (loadMore.value == false) {
      loadMore.value = true;
      await getListProduct(shopPageIndex.value);
    }
    // nextPage.value = listProduct.length ~/ Constant.offset;
  }

  // getListProduct(int page) async {
  //   (await shopUseCase.searchProduct(SearchProductRequest(
  //     limit: Constant.offset,
  //     page: page,
  //     offerType: "VENTE",
  //   )))
  //       .fold((failure) {
  //     isLoadOffreVente.value = false;
  //   }, (data) async {
  //     lastPage.value = data.totalOfPages!;
  //     totalObjectInBd.value = data.totalObject!;
  //     totalPage.value = data.totalOfPages!;

  //     if (page <= lastPage.value) {
  //       listProdNoFilter.addAll(await data.toDomain());
  //     }
  //     listProduct.value = listProdNoFilter;
  //     isLoadOffreVente.value = false;
  //     loadMore.value = false;
  //     refreshList.value = false;

  //     // right -> success (data)
  //     // listAdvice.add(data);
  //   });
  // }

  fetchSpeculation() async {
    isInitList.value = true;
    listeSpeculation = [];
    listeSpeculation = await SpeculationRepo().getAllItem();
    isInitList.value = true;
  }

  getNbSpecultionOffreAchat() async {
    isInitList.value = true;

    List<int> ids = [];
    List<ItemNbOffreAchatBySpeculation> list = [];
    await repositoryImpl.getListNbOfferAchatBySpeculation().then((response) {
      response.fold((l) {
        inspect(l);
      }, (data) async {
        list = data.data;
        inspect(list);
        await Future.forEach(data.data,
            (ItemNbOffreAchatBySpeculation element) {
          ids.add(element.speculationId);
        });
        await Future.forEach(listeSpeculation, (SpeculationModel element) {
          if (ids.contains(element.speculationId)) {
            listeSpeculationWithOffreAchat.add(NbSpeculationOffreAchatModel(
                speculation: element,
                nb: list
                    .firstWhere((itemNb) =>
                        itemNb.speculationId == element.speculationId)
                    .nombreOffer));
          }
        });
      });
    });
    isInitList.value = true;
  }

  searchSpeculationOrCategory(
    SearchItemModel searchItem,
  ) async {
    Get.dialog(const LoaderComponent());
    await shopUseCase
        .searchProduct(
          SearchProductRequest(
            offerType: typeOffer.value,
            limit: 10,
            page: 0,
            speculationId:
                searchItem.type == "speculation" ? [searchItem.id] : null,
            categorieId: searchItem.type == "categorie" ? searchItem.id : null,
          ),
        )
        .then((response) => response.fold((l) => null, (searchData) async {
              //listProd.value = await searchData.toDomain();
            }));
    Get.back();
    Get.to(ListProductSearchView(
      titre: searchItem.name,
    ));
  }

  searchInListOffreAchatProduct() async {
    List<ProductModel> prod = [];
    if (searchInListOffreAchatInput.value.text != "") {
      await Future.forEach(listSpeculationOffreAchat, (ProductModel element) {
        if (element.title
            .toLowerCase()
            .contains(searchInListOffreAchatInput.value.text.toLowerCase())) {
          prod.add(element);
        }
      });
    } else {
      prod = listSpeculationOffreAchat;
    }

    listSpeculationOffreAchatShow.value = prod;
  }

  searchInListOffrePreAchatProduct() async {
    List<ProductModel> prod = [];
    if (searchInListOffrePreAchatInput.value.text != "") {
      await Future.forEach(listSpeculationOffrePreAchat,
          (ProductModel element) {
        if (element.title.toLowerCase().contains(
            searchInListOffrePreAchatInput.value.text.toLowerCase())) {}
      });
    } else {
      prod = listSpeculationOffrePreAchat;
    }

    listSpeculationOffrePreAchatShow.value = prod;
  }

  Future<bool> verifyConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
    } on SocketException catch (_) {
      return false;
    }
    return false;
  }

  //get all countries in world
  //ProductModel convertIntrantModelInProductModel(IntrantProduct intrant) {}
}

class NbSpeculationOffreAchatModel {
  int nb;
  SpeculationModel speculation;

  NbSpeculationOffreAchatModel({required this.nb, required this.speculation});
}
