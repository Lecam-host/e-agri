import 'package:eagri/presentations/catalogue/addPrduct/add_product_view.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/shop/controller/shop_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';

import '../ressources/color_manager.dart';
import '../ressources/strings_manager.dart';
import 'list_product_achat.dart';

class ListSpeculationOffreAchat extends StatefulWidget {
  const ListSpeculationOffreAchat({
    Key? key,
  }) : super(key: key);

  @override
  State<ListSpeculationOffreAchat> createState() =>
      _ListSpeculationOffreAchatState();
}

class _ListSpeculationOffreAchatState extends State<ListSpeculationOffreAchat>  {
  ShopController shopController = Get.put(ShopController());
  @override
  void initState() {
     shopController.gitUserListOffrePreAchat();
    // shopController.getlistSpreculationOffreAchat(true, widget.speculation);

    super.initState();
  }
  @override
  void dispose() {
   

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
        appBar: AppBar(
          leading: const BackButtonCustom(),
          title: const Text(
            "Mes demandes d'achat",
            // style: getBoldTextStyle(color: ColorManager.black),
          ),
          // bottom: TabBar(
          //             controller: _tabController,
          //             indicatorColor: ColorManager.primary,
          //             tabs:[ Tab(
          //   child: Text(
          //     "Achat",
          //     style: getMeduimTextStyle(color: ColorManager.black),
          //   ),
          // ), Tab(
          //   child: Text(
          //     "Pre-achat",
          //     style: getMeduimTextStyle(color: ColorManager.black),
          //   ),
          // ),],
          //           ),
        ),
        floatingActionButton:
            FloatingActionButton(
                    backgroundColor: ColorManager.primary,
                    onPressed: () {
                       Get.to(const AddProductView(
                                                  isOffreAchat: true,
                                                  typeOffer: "ACHAT",
                                                ));
                      //  Get.bottomSheet(Container(
                      //    padding: const EdgeInsets.all(10),
                      //   margin: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
                      //   decoration: BoxDecoration(
                      //       color: Colors.white, borderRadius: BorderRadius.circular(10)),
                      //   child:  Column(
                      //     mainAxisSize: MainAxisSize.min,
                      //     children: [
                      //    const Text("Choisir le type d'offre"),
                      //  const SizedBox(height: 10,),
                      //   ListTile(
                      //     trailing:const Icon(IconManager.arrowRight),
                      //     title: const Text('Achat'),onTap: (){
                                      
                      //   },),  
                      //   ListTile(
                      //      trailing:const Icon(IconManager.arrowRight),
                      //     title: const Text('Pre-achat'),onTap: (){
                      //                  Get.to(const AddProductView(
                      //                             isOffreAchat: true,
                      //                             typeOffer: "PREACHAT",
                      //                           ));
                      //   },), 
                      //  ],),)
                      //  );
                     
                    },
                    child: Icon(
                      Icons.add,
                      color: ColorManager.white,
                    ),
                  ),
               
        body:   Obx(() =>  shopController.isListLoadMesDemandes.value == true
                      ? SingleChildScrollView(
                        child: Column(
                            children: [
                              ...List.generate(
                                10,
                                (index) {
                                  return shimmerCustom();
                                  // here by default width and height is 0
                                },
                              )
                            ],
                          ),
                      ): demandeAchatList()),);
  }
 Widget demandeAchatListPreAchat() {
    return SingleChildScrollView(
                child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 10),
                  child:
                       Column(
                          children: [
                            const SizedBox(
                              height: 10,
                            ),
                            TextField(
                              onChanged: (value) {
                                shopController. searchInListOffrePreAchatProduct();
                              },
                              controller: shopController
                                  .searchInListOffrePreAchatInput.value,
                              decoration: InputDecoration(
                                suffixIcon: Icon(
                                  Icons.search,
                                  color: ColorManager.grey,
                                ),
                                filled: true,
                                fillColor: Colors.grey.withOpacity(0.2),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                  borderSide: const BorderSide(
                                      width: 0, style: BorderStyle.none),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                  borderSide: const BorderSide(
                                      width: 0, style: BorderStyle.none),
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                  borderSide: const BorderSide(
                                      width: 0, style: BorderStyle.none),
                                ),
                                hintText: AppStrings.searchSomething,
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            if (shopController
                                .listSpeculationOffrePreAchatShow.isNotEmpty)
                              ListProductAchat(
                                listProd:
                                    shopController.listSpeculationOffrePreAchatShow,
                              ),
                            if (shopController
                                    .listSpeculationOffrePreAchatShow.isEmpty &&
                                    shopController.isListLoadMesDemandesPreAchat.value == false
                               )
                              const EmptyComponenent(
                                message: "Aucun élément trouvé",
                              ),
                            
                          ],
                        ),
                ),
              );
  }

  Widget demandeAchatList() {
    return  SingleChildScrollView(
                child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 10),
                  child:  Column(
                          children: [
                            const SizedBox(
                              height: 10,
                            ),
                            TextField(
                              onChanged: (value) {
                                shopController.searchInListOffreAchatProduct();
                              },
                              controller: shopController
                                  .searchInListOffreAchatInput.value,
                              decoration: InputDecoration(
                                suffixIcon: Icon(
                                  Icons.search,
                                  color: ColorManager.grey,
                                ),
                                filled: true,
                                fillColor: Colors.grey.withOpacity(0.2),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                  borderSide: const BorderSide(
                                      width: 0, style: BorderStyle.none),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                  borderSide: const BorderSide(
                                      width: 0, style: BorderStyle.none),
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                  borderSide: const BorderSide(
                                      width: 0, style: BorderStyle.none),
                                ),
                                hintText: AppStrings.searchSomething,
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            if (shopController
                                .listSpeculationOffreAchatShow.isNotEmpty)
                              ListProductAchat(
                                listProd:
                                    shopController.listSpeculationOffreAchatShow,
                              ),
                            if (shopController
                                    .listSpeculationOffreAchatShow.isEmpty &&
                                shopController.isListLoadMesDemandes.value == false)
                              const EmptyComponenent(
                                message: "Aucun élément trouvé",
                              ),
                            // if (shopController.listSpeculationOffreAchat.isEmpty)
                            //   Column(
                            //     children: [
                            //       const EmptyComponenent(
                            //         message: "Vous n'avez pas de demande",
                            //       ),
                            //       const SizedBox(
                            //         height: 10,
                            //       ),
                            //       DefaultButton(
                            //         text: "Ajouter une démande",
                            //         press: () {
                            //           Get.to(const AddProductView(
                            //             isOffreAchat: true,
                            //           ));
                            //         },
                            //       ),
                            //     ],
                            //   ),
                          ],
                        ),
                ),
              );
  }

  Shimmer shimmerCustom() {
    return Shimmer.fromColors(
      baseColor: const Color.fromARGB(255, 115, 115, 115),
      highlightColor: const Color.fromARGB(255, 181, 181, 181),
      child: Container(
        margin: const EdgeInsets.only(top: 10),
        height: 60,
        child: Row(
          children: [
            Container(
              height: 60,
              margin: const EdgeInsets.only(bottom: 10, left: 10),
              width: 40,
              decoration: BoxDecoration(
                color: ColorManager.white,
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  color: ColorManager.white,
                  height: 10,
                  width: 150,
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  color: ColorManager.white,
                  height: 10,
                  width: 200,
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  color: ColorManager.white,
                  height: 10,
                  width: 100,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
