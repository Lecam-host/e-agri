import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../ressources/assets_manager.dart';
import '../../ressources/color_manager.dart';
import '../../search_filter/filter_view.dart';
import '../controller/shop_controller.dart';

class FilterBottom extends StatelessWidget {
  const FilterBottom({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final shopController = Get.put(ShopController());

    return Obx(
      () => Stack(
        children: [
          if (shopController.isFilter.value == true)
            Positioned(
              top: 2,
              right: 5,
              child: Container(
                height: 10,
                width: 10,
                decoration: const BoxDecoration(
                  color: Color(0xFFFF4848),
                  shape: BoxShape.circle,
                ),
              ),
            ),
          IconButton(
            icon: SvgPicture.asset(
              SvgManager.filter,
              colorFilter: ColorFilter.mode(
                  shopController.isFilter.value == false
                      ? ColorManager.grey
                      : Colors.black,
                  BlendMode.srcIn),
              height: 30,
            ),
            onPressed: () {
              Get.to(const FilterView());
            },
          ),
        ],
      ),
    );
  }
}
