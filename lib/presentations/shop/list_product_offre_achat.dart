import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../common/state/loader_component.dart';
import 'controller/shop_controller.dart';
import 'list_product_achat.dart';

class ListProductOfreAchat extends StatelessWidget {
  const ListProductOfreAchat({super.key});

  @override
  Widget build(BuildContext context) {
    final shopController = Get.put(ShopController());
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: Obx(
          () {
            return shopController.isLoadOffreVenteForAchatProduct.value
                ? SizedBox(
                    height: MediaQuery.of(context).size.width,
                    child: const Center(
                      child: LoaderComponent(),
                    ),
                  )
                : ListProductAchat(
                    listProd: shopController.listProductOffreAchatFind,
                  );
          },
        ),
      ),
    );
  }
}
