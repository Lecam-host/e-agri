import 'package:eagri/domain/model/Product.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';

import '../../app/constant.dart';
import '../common/product_card.dart';

class ListProduct extends StatefulWidget {
  const ListProduct({
    Key? key,
    required this.listProd,
  }) : super(key: key);
  final RxList<ProductModel> listProd;

  @override
  State<ListProduct> createState() => _ListProductState();
}

class _ListProductState extends State<ListProduct> {
  // ShopController shopController = Get.find();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (widget.listProd.isNotEmpty) ...[
          GridView.builder(
            shrinkWrap: true,
            // controller: shopController.scrollController,
            // shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            gridDelegate:const  SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: Constant.widthPoint,
              mainAxisSpacing: 15,
              crossAxisSpacing: 12,
              mainAxisExtent: 320,
            ),
            itemCount: widget.listProd.length,
            itemBuilder: (context, index) {
              return ProductCard(data: widget.listProd[index]);
            },
          ),
        ] else ...[
          const Center(
            child: EmptyComponenent(
              message: "Aucun produit trouvé",
            ),
          )
        ]
      ],
    );
  }
}
