import 'package:eagri/presentations/common/default_button.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:eagri/presentations/ressources/values_manager.dart';
import 'package:eagri/presentations/shop/controller/shop_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../app/constant.dart';
import '../common/shimmer.dart';
import '../common/state/error_component.dart';
import 'list_product.dart';

class ShopListProductBloc extends StatefulWidget {
  const ShopListProductBloc({super.key});

  @override
  State<ShopListProductBloc> createState() => _ShopListProductBlocState();
}


class _ShopListProductBlocState extends State<ShopListProductBloc> {
ShopController shopController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Obx(() => RefreshIndicator(
          onRefresh: () async {
            await shopController.refreshListProduct();
          },
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            controller: shopController.shopPageIndex.value == 0
                ? shopController.scrollController0
                : shopController.scrollController1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (shopController.shopPageIndex.value == 0)
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        categorieButton(
                          categoryId: "",
                          name: "Tout",
                        ),
                        ...List.generate(
                          shopController.listCategorieSpeculation.length,
                          (index) {
                            return categorieButton(
                              categoryId: shopController
                                  .listCategorieSpeculation[index].categoryId.toString(),
                              name: shopController
                                  .listCategorieSpeculation[index].name,
                            );
                            // here by default width and height is 0
                          },
                        )
                      ],
                    ),
                  ),
                if (shopController.shopPageIndex.value == 1)
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        categorieButton(
                          categoryId: "",
                          name: "Tout",
                        ),
                        ...List.generate(
                          shopController.listCategorieIntrant.length,
                          (index) {
                            return categorieButton(
                              categoryId: shopController
                                  .listCategorieIntrant[index].categoryId.toString(),
                              name: shopController
                                  .listCategorieIntrant[index].libelle,
                            );
                            // here by default width and height is 0
                          },
                        )
                      ],
                    ),
                  ),
                const SizedBox(
                  height: 10,
                ),
                if (shopController.isInitList.value == true)
                  GridView.builder(
                    shrinkWrap: true,
                    // controller: shopController.scrollController,
                    // shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    gridDelegate:
                        const SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: Constant.widthPoint,
                      mainAxisSpacing: 5,
                      crossAxisSpacing: 5,
                      mainAxisExtent: 210,
                    ),
                    itemCount: 10,
                    itemBuilder: (context, index) {
                      return shimmerProductCustom();
                    },
                  ),
                if (shopController.isError.value == true &&
                    shopController.isInitList.value == false) ...[
                  const ErrorComponent(),
                  DefaultButton(
                    text: "Réessayer",
                    press: () async {
                      setState(() {
                        shopController.isInitList.value = false;
                        shopController.isError.value = false;
                      });

                      await shopController
                          .getListProduct(shopController.shopPageIndex.value);
                    },
                  ),
                ] else if (shopController.isInitList.value == false) ...[
                  ListProduct(
                    listProd: shopController.shopPageIndex.value == 0
                        ? shopController.listProductAgricul
                        : shopController.listProductIntrant,
                  ),
                ],

                if (shopController.loadMore.value == true)
                  const SizedBox(
                    height: 70,
                    child: Center(
                      child: LoaderComponent(),
                    ),
                  )

                // if (shopController.listProductVente.isEmpty &&
                //     shopController.shopPageIndex.value == 0)
                //   const EmptyComponenent(
                //     message: "Aucun produit trouvé",
                //   ),
                // if (shopController.listProductAchat.isEmpty &&
                //     shopController.shopPageIndex.value == 1)
                //   const EmptyComponenent(
                //     message: "Aucun produit trouvé",
                //   ),

                // if (shopController.listProductVente.isNotEmpty)
                //   if (shopController.shopPageIndex.value == 0)
                //     ListProduct(
                //         listProd: shopController.listProductVente),
                // if (shopController.shopPageIndex.value == 1)

                // if (shopController.loadMore.value == true)
                //   SizedBox(
                //     width: 70,
                //     height: 40,
                //     child: Lottie.asset(JsonAssets.loader, height: 70),
                //   ),
              ],
            ),
          ),
        ));
  }

  categorieButton({
    required String categoryId,
    required String name,
  }) {
    return Obx(
      () => InkWell(
        onTap: () async {
          shopController.isInitList.value = true;
  shopController.renitialiseFilter();
          if (categoryId != "") {
            setState(() {
              
              shopController.categoryController.value.text =
                  categoryId.toString();
            });
          } else {
            setState(() {
              shopController.categoryController.value =TextEditingController();
            });
          }
          shopController.nextPage.value = 0;
          shopController.loadMore.value = false;
          await shopController
              .getListProduct(shopController.shopPageIndex.value);
        },
        child: Container(
          decoration: BoxDecoration(
            color: shopController.categoryController.value.text ==
                    categoryId
                ? ColorManager.primary
                : ColorManager.grey,
            borderRadius: BorderRadius.circular(15),
          ),
          padding:
              const EdgeInsets.only(right: 10, left: 10, bottom: 7, top: 5),
          margin: const EdgeInsets.only(
            left: AppMargin.m5,
            bottom: AppMargin.m5,
          ),
          child: Text(
            name.toString(),
            style: getRegularTextStyle(
              color: shopController.categoryController.value.text ==
                      categoryId.toString()
                  ? ColorManager.white
                  : ColorManager.white,
            ),
          ),
        ),
      ),
    );
  }
}
