import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:eagri/presentations/ressources/values_manager.dart';
import 'package:eagri/presentations/shop/controller/shop_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

import '../../domain/model/category_model.dart';
import '../ressources/assets_manager.dart';
import 'list_product.dart';

final shopController = Get.put(ShopController());

class IntrantShopScreen extends StatelessWidget {
  const IntrantShopScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: AppMargin.m5),
        child: Obx(
          () {
            return shopController.isInitList.value == true
                ? SizedBox(
                    height: MediaQuery.of(context).size.width,
                    child: const Center(
                      child: LoaderComponent(),
                    ),
                  )
                : RefreshIndicator(
                    onRefresh: () async {
                      await shopController.refreshListProduct();
                    },
                    child: SingleChildScrollView(
                      physics: const BouncingScrollPhysics(),
                      //controller: shopController.scrollController,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              children: [
                                categorieButton(
                                  categorie: CategoryModel(
                                      categoryId: 0, name: "Tout"),
                                ),
                                // ...List.generate(
                                //   shopController.allListProdIntrant.length,
                                //   (index) {
                                //     return categorieButton(
                                //         categorie: shopController
                                //             .listCategorieSpeculation[index]);
                                //     // here by default width and height is 0
                                //   },
                                // )
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          if (shopController.refreshList.value == true)
                            SizedBox(
                              width: 70,
                              height: 40,
                              child:
                                  Lottie.asset(JsonAssets.loader, height: 70),
                            ),
                          if (shopController.allListProdIntrant.isNotEmpty)
                            if (shopController.shopPageIndex.value == 0)
                              ListProduct(
                                  listProd: shopController.allListProdIntrant),
                        ],
                      ),
                    ),
                  );
          },
        ),
      ),
    );
  }
}

categorieButton({
  required CategoryModel categorie,
}) {
  return InkWell(
      onTap: () async {
        if (categorie.categoryId != 0) {
          shopController.categoryController.value.text =
              categorie.categoryId.toString();
        } else {
          shopController.categoryController.value.text = "0";
        }
        shopController.nextPage.value = 0;
        await shopController.getListProduct(shopController.shopPageIndex.value);
      },
      child: Obx(
        () => Container(
          decoration: BoxDecoration(
            color: shopController.categoryController.value.text ==
                    categorie.categoryId.toString()
                ? ColorManager.primary
                : ColorManager.grey,
            borderRadius: BorderRadius.circular(15),
          ),
          padding:
              const EdgeInsets.only(right: 10, left: 10, bottom: 7, top: 5),
          margin: const EdgeInsets.only(
            left: AppMargin.m5,
            bottom: AppMargin.m5,
          ),
          child: Text(
            categorie.name,
            style: getRegularTextStyle(
              color: shopController.categoryController.value.text ==
                      categorie.categoryId.toString()
                  ? ColorManager.white
                  : ColorManager.white,
            ),
          ),
        ),
      ));
}
