import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/buttons/cart_bottom_component.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/search_filter/filter_view.dart';
import 'package:eagri/presentations/shop/controller/shop_controller.dart';
import 'package:eagri/presentations/shop/shop_list_product.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import '../../app/constant.dart';
import '../../app/functions.dart';
import '../../controllers/connection_controller.dart';
import '../../domain/model/user_model.dart';
import '../cart/controller/cart_controller.dart';
import '../common/connection_widget.dart';
import '../ressources/assets_manager.dart';
import '../ressources/styles_manager.dart';
import '../search_filter/controller/filter_controller.dart';

enum TypeOffre { vente, achat }

class ShopScreen extends StatefulWidget {
  const ShopScreen({Key? key,this.indexPage=0,this.typeOffer="VENTE",this.appBarTitle=""}) : super(key: key);
  final int indexPage;
  final String typeOffer;
  final String appBarTitle;


  @override
  State<ShopScreen> createState() => _ShopScreenState();
}

class _ShopScreenState extends State<ShopScreen>
    with SingleTickerProviderStateMixin {
  CartController cartController = Get.put(CartController());
  ShopController shopController = Get.find();
  FilterController filterController = Get.put(FilterController());
  ConnectionController connectionController = Get.put(ConnectionController());
  final GlobalKey<ScaffoldState> _key = GlobalKey(); //

  final _focusNodePrixMin = FocusNode();
  final _focusNodePrixMax = FocusNode();
  late TabController _tabController;
  List<Widget> tabChildren(List<String> scopes) => [
        Container(
            margin: const EdgeInsets.only(top: 10, right: 10, left: 10),
            child: const ShopListProductBloc()),
        Container(
          margin: const EdgeInsets.only(top: 10, right: 10, left: 10),
          child: const ShopListProductBloc(),
        ),
      ];
  List<Widget> listTabs(List<String> scopes) => [
        if (isInScopes(UserPermissions.SHOW_PRODUCT_AGRICOLE, scopes) == true)
          Tab(
            child: Text(
              "Produits agricole",
              style: getMeduimTextStyle(color: ColorManager.black),
            ),
          ),
        if (isInScopes(UserPermissions.SHOW_PRODUCT_INTRANT, scopes) == true)
          Tab(
            child: Text(
              "Intrants",
              style: getMeduimTextStyle(color: ColorManager.black),
            ),
          ),
      ];
  void _handleTabSelection() {
    shopController.categoryController.value =TextEditingController();
    shopController.shopPageIndex.value = _tabController.index;
    shopController.nextPage.value = 0;
    shopController.isInitList.value = true;
    shopController.getListProduct(_tabController.index);
  }

  @override
  void dispose() {
    _tabController.dispose();

    _focusNodePrixMin.dispose();
    _focusNodePrixMax.dispose();
    _tabController.removeListener(_handleTabSelection);
    // shopController.resetFilter();

    super.dispose();
  }

  @override
  void initState() {
    shopController.isInitList.value = true;
    shopController.shopPageIndex.value = widget.indexPage;
    shopController.nextPage.value = 0;
    shopController.typeOffer.value=widget.typeOffer;
    //shopController.resetFilter();
     
    shopController. listProductAgricul.value=[];
    shopController. listProductIntrant.value=[];

   
    shopController.getListProduct(widget.indexPage);

    _tabController = TabController(initialIndex: widget.indexPage, length: 2, vsync: this,);

    _tabController.addListener(_handleTabSelection);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
        builder: (context, user, child) => DefaultTabController(
              length: listTabs(user.scopes!).length,
              child: Scaffold(
                backgroundColor: ColorManager.backgroundColor,
                key: _key,
                appBar: PreferredSize(
                  preferredSize: const Size.fromHeight(90),
                  child: AppBar(
                    leading:const BackButtonCustom(),
                    toolbarHeight: 40,
                    titleSpacing: 20,
                    elevation: 1,
                    title: Obx(
                        () => connectionController.connectReseau.value == true
                            ? Text(
                              widget.appBarTitle,
                                style: TextStyle(
                                  fontSize: 15,
                                  fontStyle: FontStyle.normal,
                                  color: ColorManager.black,
                                ),
                              )
                            : const NoConnectionComponennt()),
                    bottom: TabBar(
                      controller: _tabController,
                      indicatorColor: ColorManager.primary,
                      tabs: listTabs(user.scopes!),
                    ),
                    actions: [
                      Container(
                        margin: const EdgeInsets.only(
                          top: 5,
                        ),
                        child: IconButton(
                          icon: SvgPicture.asset(
                            SvgManager.filter,
                            width: 20,
                          ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              PageTransition(
                                type: PageTransitionType.rightToLeft,
                                child: const FilterView(),
                                isIos: true,
                                duration: const Duration(
                                    milliseconds: Constant.navigatorDuration),
                              ),
                            ).then((value) {
                              _tabController.animateTo(value);
                            });
                          },
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(
                          top: 5,
                        ),
                        child: CartButtomComponent(),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                    ],
                  ),
                ),
                body: TabBarView(
                  controller: _tabController,
                  children: tabChildren(user.scopes!),
                ),
              ),
            ));
  }
}
