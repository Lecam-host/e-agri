import 'package:eagri/domain/model/Product.dart';
import 'package:eagri/presentations/shop/controller/shop_controller.dart';
import 'package:eagri/presentations/shop/offre_achat_card.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ListProductAchat extends StatefulWidget {
  const ListProductAchat({Key? key, required this.listProd, this.loadMore})
      : super(key: key);
  final RxList<ProductModel> listProd;
  final bool? loadMore;
  @override
  State<ListProductAchat> createState() => _ListProductAchatState();
}

final ShopController shopController = Get.find();

class _ListProductAchatState extends State<ListProductAchat> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: const NeverScrollableScrollPhysics(),
      padding: const EdgeInsets.only(bottom: 10),
      shrinkWrap: true,
      itemCount: widget.listProd.length,
      itemBuilder: (context, index) {
        return OffreAchatCard(
          data: widget.listProd[index],
        );
      },
    );
  }
}
