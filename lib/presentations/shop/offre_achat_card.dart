import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:eagri/presentations/shop/controller/shop_controller.dart';
import 'package:flutter/material.dart';

import '../../app/functions.dart';
import '../../domain/model/Product.dart';

import "package:get/get.dart";

typedef ProductCardOnTaped = void Function(ProductModel data);

class OffreAchatCard extends StatelessWidget {
  const OffreAchatCard({
    super.key,
    required this.data,
    this.ontap,
  });

  final ProductModel data;
  final ProductCardOnTaped? ontap;

  @override
  Widget build(BuildContext context) {
    ShopController shopController = Get.put(ShopController());
    // final data = datas[index % datas.length];
    const borderRadius = BorderRadius.all(Radius.circular(20));
    return InkWell(
      borderRadius: borderRadius,
      onTap: () async {
        await shopController.findOffreAchat(data);
      },
      child: Container(
        margin: const EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
          color: ColorManager.white,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            CachedNetworkImage(
              imageUrl: data.images![0],
              imageBuilder: (context, imageProvider) => Container(
                width: 100,
                height: 80,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    scale: 1,
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              placeholder: (context, url) => Container(
                width: 100,
                height: 80,
                color: ColorManager.grey1.withOpacity(0.2),
              ),
              errorWidget: (context, url, error) => Container(
                height: double.infinity,
                width: double.infinity,
                color: ColorManager.grey1.withOpacity(0.2),
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 20,
                  width: 200,
                  child: Text(
                    data.title,
                    style: getBoldTextStyle(
                      fontSize: 14,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Text(
                  "Prix: ${data.price.toString()} FCFA",
                  style: getRegularTextStyle(
                    fontSize: 14,
                  ),
                ),
                Text(
                  "Qte: ${data.quantity.toString()}  ${data.unite} ",
                  style: getRegularTextStyle(
                    fontSize: 14,
                  ),
                ),
                Text(
                  convertDate(data.publicationDate.toString()),
                  style: getBoldTextStyle(
                    fontSize: 12,
                    color: ColorManager.grey,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
