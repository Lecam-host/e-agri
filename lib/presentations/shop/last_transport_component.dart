import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:eagri/presentations/shop/controller/shop_controller.dart';
import 'package:eagri/presentations/transport/component/transport_card.dart';
import 'package:eagri/presentations/transport/transport_screen.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';


import '../home/components/section_title.dart';
import '../ressources/color_manager.dart';
import '../ressources/size_config.dart';



class LastTransportComponenent extends StatelessWidget {
  const LastTransportComponenent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ShopController shopController = Get.find();

    return  Obx(() => shopController.lastTransport.isNotEmpty?Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
          SizedBox(height: getProportionateScreenWidth(30)),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: SectionTitle(
              title: "Transports",
              style:
                  getMeduimTextStyle(color: ColorManager.black, fontSize: 14),
              press: () {
               Get.to(const TransportScreen());
              }),
        ),
        SizedBox(height: getProportionateScreenWidth(10)),
        
             SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Container(
                  margin: const EdgeInsets.only(right: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ...List.generate(
                        shopController.lastTransport.length,
                        (index) {
                          return Container(
                            margin: const EdgeInsets.only(left: 10),
                            width: MediaQuery.of(context).size.width/1.3,
                            child: TransportCard(
                              data: shopController.lastTransport[index],
                            ),
                          );
                          // here by default width and height is 0
                        },
                      ),
                      // SizedBox(width: getProportionateScreenWidth(20)),
                    ],
                  ),
                ),
              )
          
      ],
    ):  const SizedBox());
  }
}
