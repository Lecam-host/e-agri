import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../domain/model/Product.dart';
import 'list_product.dart';

class ListOffreAchatFind extends StatefulWidget {
  const ListOffreAchatFind(
      {Key? key, required this.listProd, required this.product})
      : super(key: key);

  final RxList<ProductModel> listProd;
  final ProductModel product;
  @override
  State<ListOffreAchatFind> createState() => _ListOffreAchatFindState();
}

class _ListOffreAchatFindState extends State<ListOffreAchatFind> {
  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
          appBar: AppBar(
            leading: const BackButtonCustom(),
            title: Text(
              "offre de vente pour ${widget.product.title}",
              style: getSemiBoldTextStyle(
                fontSize: 14,
              ),
            ),
          ),
          body: widget.listProd.isNotEmpty
              ? Container(
                  margin: const EdgeInsets.all(10),
                  child: ListProduct(
                    listProd: widget.listProd,
                  ),
                )
              : const EmptyComponenent(
                  message:
                      "Nous n'avons pas trouvé d'offre correspondant à votre demande",
                ),
        ));
  }
}
