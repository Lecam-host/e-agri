import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/app/constant.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:eagri/presentations/shop/controller/shop_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../ressources/color_manager.dart';

class ShopOffreAchatBloc extends StatefulWidget {
  const ShopOffreAchatBloc({super.key});

  @override
  State<ShopOffreAchatBloc> createState() => _ShopOffreAchatBlocState();
}

class _ShopOffreAchatBlocState extends State<ShopOffreAchatBloc> {
  var shopController = Get.put(ShopController());

  @override
  void initState() {
    // shopController.getNbSpecultionOffreAchat();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Obx(
        () {
          return shopController.isInitList.value
              ? SizedBox(
                  height: MediaQuery.of(context).size.width,
                  child: const Center(
                    child: LoaderComponent(),
                  ),
                )
              : shopController.listeSpeculationWithOffreAchat.isNotEmpty
                  ? GridView.builder(
                      gridDelegate:
                          const SliverGridDelegateWithMaxCrossAxisExtent(
                        maxCrossAxisExtent: Constant.widthPoint / 1.7,
                        mainAxisSpacing: 10,
                        crossAxisSpacing: 10,
                        mainAxisExtent: 120,
                      ),
                      itemCount:
                          shopController.listeSpeculationWithOffreAchat.length,
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () {
                            // Get.to(ListSpeculationOffreAchat(
                            //   speculation: shopController
                            //       .listeSpeculationWithOffreAchat[index]
                            //       .speculation,
                            // ));
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                color: ColorManager.white,
                                borderRadius: BorderRadius.circular(10)),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: CachedNetworkImage(
                                    imageUrl: shopController
                                        .listeSpeculationWithOffreAchat[index]
                                        .speculation
                                        .imageUrl!,
                                    imageBuilder: (context, imageProvider) =>
                                        Container(
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                        borderRadius: const BorderRadius.only(
                                            topLeft: Radius.circular(10),
                                            topRight: Radius.circular(10)),
                                        image: DecorationImage(
                                          scale: 1,
                                          image: imageProvider,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    placeholder: (context, url) => Container(
                                      height: double.infinity,
                                      width: double.infinity,
                                      color:
                                          ColorManager.grey1.withOpacity(0.2),
                                    ),
                                    errorWidget: (context, url, error) =>
                                        Container(
                                      height: double.infinity,
                                      width: double.infinity,
                                      color:
                                          ColorManager.grey1.withOpacity(0.2),
                                    ),
                                  ),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                      color: ColorManager.white,
                                      borderRadius: BorderRadius.circular(10)),
                                  padding: const EdgeInsets.all(3),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "${shopController.listeSpeculationWithOffreAchat[index].nb.toString()} Offres",
                                        style: getBoldTextStyle(
                                            color: Colors.black, fontSize: 13),
                                      ),
                                      SizedBox(
                                        height: 50,
                                        child: Text(
                                          shopController
                                              .listeSpeculationWithOffreAchat[
                                                  index]
                                              .speculation
                                              .name
                                              .toString(),
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 2,
                                          style: getRegularTextStyle(
                                              color: Colors.black,
                                              fontSize: 13),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      }
                      // delegate: SliverChildBuilderDelegate(
                      //   (context, index) => InkWell(
                      //     onTap: () {
                      //       Get.to(ListSpeculationOffreAchat(
                      //         speculation: shopController.listeSpeculation[index],
                      //       ));
                      //     },
                      //     child: Container(
                      //       alignment: Alignment.center,
                      //       decoration: BoxDecoration(
                      //         image: DecorationImage(
                      //             image: CachedNetworkImageProvider(
                      //               shopController.listeSpeculation[index].imageUrl
                      //                   .toString(),
                      //             ),
                      //             colorFilter: ColorFilter.mode(
                      //               Colors.black.withOpacity(0.7),
                      //               BlendMode.darken,
                      //             ),
                      //             fit: BoxFit.cover),
                      //         color: Colors.black,
                      //         borderRadius: BorderRadius.circular(7),
                      //       ),
                      //       child: Column(
                      //         mainAxisAlignment: MainAxisAlignment.center,
                      //         children: [
                      //           Text(
                      //             shopController.listeSpeculation[index].name
                      //                 .toString(),
                      //             overflow: TextOverflow.ellipsis,
                      //             style: getRegularTextStyle(
                      //                 color: Colors.white, fontSize: 13),
                      //           ),
                      //           Text(
                      //             "200",
                      //             style: getBoldTextStyle(
                      //                 color: Colors.white, fontSize: 13),
                      //           ),
                      //         ],
                      //       ),
                      //     ),
                      //   ),
                      //   childCount: shopController.listeSpeculation.length,
                      // ),
                      )
                  : const EmptyComponenent(
                      message: "Offre d'achat vide",
                    );
        },
      ),
    );
  }
}
