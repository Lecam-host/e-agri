import 'package:eagri/app/functions.dart';
import 'package:eagri/presentations/common/buttons/cart_bottom_component.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:eagri/presentations/shop/controller/shop_controller.dart';
import 'package:eagri/presentations/shop/shop_screen.dart';
import 'package:eagri/presentations/shop/some_intrants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import '../../app/constant.dart';
import '../../domain/model/user_model.dart';
import '../home/components/last_product.dart';
import '../prestations/prestations_home.dart';
import '../ressources/assets_manager.dart';
import '../ressources/color_manager.dart';

import '../transport/transport_screen.dart';
import 'last_prestation_component.dart';
import 'last_transport_component.dart';

class ShopHome extends StatefulWidget {
  const ShopHome({Key? key}) : super(key: key);

  @override
  State<ShopHome> createState() => _ShopHomeState();
}

class _ShopHomeState extends State<ShopHome> {
  ShopController shopController = Get.find();
  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
      builder: (context, user, child) =>  Scaffold(
      backgroundColor: ColorManager.backgroundColor,
      appBar: AppBar(
        actions: [
          Container(
            margin: const EdgeInsets.only(
              top: 5,
            ),
            child: CartButtomComponent(),
          ),
          // const FilterBottom(),
        ],
        automaticallyImplyLeading: false,
        leading: const BackButton(),
        title: const Text("Place du marche"),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 10,
            ),
            Container(
                margin: const EdgeInsets.only(left: 10),
                child: Text(
                  "Type de produit",
                  style: getSemiBoldTextStyle(fontSize: 16),
                )),
            const SizedBox(
              height: 10,
            ),
            Container(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  typeProductCard(
                      "Agricole",
                      ImageAssets.agricole,
                      const ShopScreen(
                        appBarTitle: "Produit agricole",
                      )),
                  const SizedBox(
                    width: 10,
                  ),
                  typeProductCard(
                      "Intrant",
                      ImageAssets.mettreEngrais,
                      const ShopScreen(
                        indexPage: 1,
                        appBarTitle: "Produit agricole",
                      )),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  typeProductCard("Transport", ImageAssets.transport,
                      const TransportScreen()),
                  const SizedBox(
                    width: 10,
                  ),
                  typeProductCard("Prestation", ImageAssets.prestation,
                      const PrestationHome()),
                ],
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            Container(
                margin: const EdgeInsets.only(left: 10),
                child: Text(
                  "Type d'offres",
                  style: getSemiBoldTextStyle(fontSize: 16),
                )),
            const SizedBox(
              height: 10,
            ),
            Container(
              padding: const EdgeInsets.only(left: 10),
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    if(isInScopes(UserPermissions.PLACEDEMARCHE_PRODUIT_AGRICOLE_VENTE, user.scopes!))
                    typeOffreCard(
                        "Vente",
                        SvgManager.price,
                        ColorManager.primary2,
                        const ShopScreen(
                          typeOffer: "VENTE",
                          appBarTitle: "Produit en vente",
                        )),
                    const SizedBox(
                      width: 10,
                    ),
                     if(isInScopes(UserPermissions.PLACEDEMARCHE_PRODUIT_AGRICOLE_ACHAT, user.scopes!))
                    typeOffreCard(
                        "Achat",
                        SvgManager.price,
                        ColorManager.primary,
                        const ShopScreen(
                          typeOffer: "ACHAT",
                          appBarTitle: "Offre d'achat",
                        )),
                    const SizedBox(
                      width: 10,
                    ),
                    typeOffreCard(
                        "Pre-achat",
                        SvgManager.price,
                        ColorManager.primary3,
                        const ShopScreen(
                          typeOffer: "PREACHAT",
                          appBarTitle: "Offre de pre-achat",
                        )),
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            const LastProduct(),
            const SizedBox(
              height: 30,
            ),
            const SomeIntrant(),
            const SizedBox(
              height: 30,
            ),
            const LastPrestationComponenent(),
            const SizedBox(
              height: 30,
            ),
            const LastTransportComponenent(),
          ],
        ),
      ),
    ));
  }

  Widget typeProductCard(String titre, String image, Widget widget) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            PageTransition(
              type: PageTransitionType.rightToLeft,
              child: widget,
            ));
      },
      child: Container(
        height: 100,
        width: MediaQuery.of(context).size.width / 2.2,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: ColorManager.black,
          image: DecorationImage(
            fit: BoxFit.cover,
            colorFilter: ColorFilter.mode(
              Colors.black.withOpacity(0.3),
              BlendMode.darken,
            ),
            image: AssetImage(
              image,
            ),
            // colorFilter:
          ),
        ),
        child: Align(
          alignment: Alignment.bottomLeft,
          child: Container(
            margin: const EdgeInsets.only(left: 10, bottom: 10),
            width: MediaQuery.of(context).size.width / 2.5,
            child: Text(
              titre,
              style: getSemiBoldTextStyle(
                fontSize: 18,
                color: ColorManager.white,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget typeOffreCard(String titre, String icon, Color color, Widget widget) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            PageTransition(
              type: PageTransitionType.rightToLeft,
              child: widget,
            ));
      },
      child: Container(
        height: 50,
        padding: const EdgeInsets.all(10),
        decoration:
            BoxDecoration(color: color, borderRadius: BorderRadius.circular(5)),
        child: Row(
          children: [
            Text(
              titre,
              style: getSemiBoldTextStyle(color: ColorManager.white),
            ),
            SvgPicture.asset(
              icon,
              height: 50,
            )
          ],
        ),
      ),
    );
  }
}
