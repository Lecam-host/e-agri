import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:eagri/presentations/shop/controller/shop_controller.dart';
import 'package:eagri/presentations/shop/shop_screen.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import 'package:get/get.dart';

import '../common/product_card.dart';
import '../home/components/section_title.dart';
import '../ressources/color_manager.dart';
import '../ressources/size_config.dart';
import 'intrant_shop_screen.dart';

class SomeIntrant extends StatefulWidget {
  const SomeIntrant({Key? key}) : super(key: key);

  @override
  State<SomeIntrant> createState() => _SomeIntrantState();
}

class _SomeIntrantState extends State<SomeIntrant> {
  @override
  void initState() {
    shopController.getLastIntrantProduct();
    super.initState();
    
  }
  @override
  Widget build(BuildContext context) {
    ShopController shopController = Get.find();

    return Obx(() => shopController.lastListProdIntrant.isNotEmpty? Container(
      padding: const EdgeInsets.only(bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
            SizedBox(height: getProportionateScreenWidth(30)),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: SectionTitle(
                title: "Intrants",
                style: getSemiBoldTextStyle(
                    color: ColorManager.black, fontSize: 14),
                press: () {
                  Get.to( const ShopScreen(appBarTitle: "Produit agricole",indexPage: 1,));
                }),
          ),
          SizedBox(height: getProportionateScreenWidth(10)),
           SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ...List.generate(
                        shopController.lastListProdIntrant.length,
                        (index) {
                          return Container(
                            margin: const EdgeInsets.only(left: 10),
                            width: 200,
                            child: ProductCard(
                              isIntrant: true,
                            data: shopController.lastListProdIntrant[index] ,
                            ),
                          );
                          // here by default width and height is 0
                        },
                      ),
                      // SizedBox(width: getProportionateScreenWidth(20)),
                    ],
                  ),
                )
              
        ],
      ),
    ):const SizedBox());
  }

  Shimmer shimmerCustom() {
    return Shimmer.fromColors(
      baseColor: const Color.fromARGB(255, 175, 175, 175),
      highlightColor: const Color.fromARGB(255, 213, 213, 213),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.only(bottom: 10, left: 10),
            width: 150,
            height: 130,
            decoration: BoxDecoration(
              color: ColorManager.white,
              borderRadius: BorderRadius.circular(15),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 10, left: 10),
            width: 130,
            height: 10,
            decoration: BoxDecoration(
              color: ColorManager.white,
              borderRadius: BorderRadius.circular(15),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 10, left: 10),
            width: 110,
            height: 10,
            decoration: BoxDecoration(
              color: ColorManager.white,
              borderRadius: BorderRadius.circular(15),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 10, left: 10),
            width: 150,
            height: 10,
            decoration: BoxDecoration(
              color: ColorManager.white,
              borderRadius: BorderRadius.circular(15),
            ),
          ),
        ],
      ),
    );
  }
}
