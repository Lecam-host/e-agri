// ignore_for_file: use_build_context_synchronously

import 'dart:io';

import 'package:another_flushbar/flushbar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/data/repository/api/repository_impl.dart';
import 'package:eagri/domain/model/Product.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/default_button.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import '../../app/di.dart';
import '../../domain/model/image_model.dart';
import '../ressources/color_manager.dart';
import '../ressources/styles_manager.dart';

class UpdateImage extends StatefulWidget {
  const UpdateImage({super.key, required this.product});
  final ProductModel product;
  @override
  State<UpdateImage> createState() => _UpdateImageState();
}

class _UpdateImageState extends State<UpdateImage> {
  List<File> newImages = [];
  late ProductModel productShow;
  @override
  void initState() {
    productShow = widget.product;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Modifier l'image"),
        leading: const BackButtonCustom(),
      ),
      body: Container(
        margin: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 10,
            ),
            // Text('modif image'),
            Wrap(
              spacing: 20,
              runSpacing: 20,
              children: [
                ...productShow.imagesWithId!.map(
                  (item) {
                    return Stack(
                      clipBehavior: Clip.none,
                      alignment: AlignmentDirectional.bottomCenter,
                      children: [
                        Container(
                          width: 150,
                          height: 150,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            border: Border.all(
                              color: Colors.grey,
                              width: 1,
                            ),
                          ),
                          child: CachedNetworkImage(
                            imageUrl: item.link,
                            imageBuilder: (context, imageProvider) => Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                image: DecorationImage(
                                  scale: 1,
                                  image: imageProvider,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            placeholder: (context, url) => SizedBox(
                              height: MediaQuery.of(context).size.height,
                              width: MediaQuery.of(context).size.width,
                            ),
                            errorWidget: (context, url, error) =>
                                const Icon(Icons.error),
                          ),
                          // child: Image.file(),
                        ),
                        Positioned(
                          right: -10,
                          top: -10,
                          child: GestureDetector(
                            onTap: () {
                              Get.defaultDialog(
                                  title: "Supprimer",
                                  titleStyle: getRegularTextStyle(fontSize: 20),
                                  content: const Text(
                                    "Vous voulez supprimer cette image ?",
                                  ),
                                  onCancel: () {
                                    Get.back();
                                  },
                                  textCancel: "Annuler",
                                  textConfirm: "Supprimer",
                                  buttonColor: ColorManager.primary,
                                  cancelTextColor: Colors.black,
                                  // confirmTextColor: Colors.black,
                                  onConfirm: () async {
                                    Get.back();
                                    deleteProduct(item);
                                  });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.red,
                                  borderRadius: BorderRadius.circular(10)),
                              child: const Icon(
                                Icons.close,
                                color: Colors.white,
                                size: 25,
                              ),
                            ),
                          ),
                        )
                      ],
                    );
                  },
                ),
              ],
            ),

            if (newImages.isNotEmpty) ...[
              const SizedBox(
                height: 20,
              ),
              Text(
                'Nouvelle image',
                style: getMeduimTextStyle(
                  fontSize: 16,
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Wrap(
                spacing: 20,
                runSpacing: 20,
                children: [
                  ...newImages.map(
                    (item) {
                      return Stack(
                        clipBehavior: Clip.none,
                        alignment: AlignmentDirectional.bottomCenter,
                        children: [
                          Container(
                            width: 150,
                            height: 150,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                border: Border.all(
                                  color: Colors.grey,
                                  width: 1,
                                ),
                                image: DecorationImage(
                                    image: FileImage(File(item.path)),
                                    fit: BoxFit.cover)),
                            // child: Image.file(),
                          ),
                          Positioned(
                            right: -10,
                            top: -10,
                            child: GestureDetector(
                              onTap: () {
                                newImages.removeAt(newImages.indexOf(item));
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    color: Colors.red,
                                    borderRadius: BorderRadius.circular(10)),
                                child: const Icon(
                                  Icons.close,
                                  color: Colors.white,
                                  size: 25,
                                ),
                              ),
                            ),
                          )
                        ],
                      );
                    },
                  ),
                ],
              ),
            ],

            const SizedBox(
              height: 10,
            ),
            if (newImages.length + productShow.images!.length < 4)
              Center(
                child: GestureDetector(
                  onTap: () {
                    // pickImages(PrestationController);
                    selectGalleryOrCamera();
                  },
                  child: Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(7),
                      border: Border.all(
                        color: Colors.grey,
                        width: 1,
                      ),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Icon(
                          Icons.add,
                          color: Colors.grey,
                        ),
                        Text(
                          "Ajouter",
                          style: getRegularTextStyle(color: Colors.grey),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            const SizedBox(
              height: 10,
            ),
            if (newImages.isNotEmpty)
              DefaultButton(
                text: "Enregistrer les nouvelles images",
                press: () async {
                  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
                  Get.dialog(const LoaderComponent());
                  await repositoryImpl
                      .addOtherImages(productShow.id, newImages)
                      .then((response) {
                    response.fold((failure) {
                      Get.back();
                      showCustomFlushbar(context, failure.message,
                          ColorManager.error, FlushbarPosition.TOP);
                    }, (data) async {
                      productShow = (await data)!;
                      setState(() {
                        newImages = [];
                        productShow;
                      });
                      Get.back();
                      showCustomFlushbar(context, "Image ajoutée",
                          ColorManager.succes, FlushbarPosition.TOP);
                    });
                  });
                },
              ),
          ],
        ),
      ),
    );
  }

  deleteProduct(ImageProduct img) async {
    RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
    Get.dialog(const LoaderComponent());
    await repositoryImpl.deleteImage(img.id).then((response) async {
      response.fold((failure) {
        Get.back();
        showCustomFlushbar(
          context,
          failure.message,
          ColorManager.error,
          FlushbarPosition.TOP,
        );
      }, (data) {
        Get.back();

        showCustomFlushbar(
          context,
          "Image supprimée",
          ColorManager.succes,
          FlushbarPosition.TOP,
        );
      });
    });
  }

  addFileAndConvert(XFile file) {
    final newFile = File(file.path);
    setState(() {
      newImages.add(newFile);
    });
  }

  selectGalleryOrCamera() {
    return Get.defaultDialog(
      title: "Ajouter une image",
      titleStyle: TextStyle(
        color: ColorManager.primary2,
      ),
      content: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          IconButton(
            onPressed: () {
              pickImages(ImageSource.camera);
            },
            icon: const Icon(
              Icons.camera_alt,
              size: 35,
            ),
          ),
          const SizedBox(width: 20),
          IconButton(
            onPressed: () {
              pickImages(ImageSource.gallery);
            },
            icon: const Icon(Icons.image, size: 35),
          ),
        ],
      ),
    );
  }

  Future<void> pickImages(ImageSource source) async {
    final picker = ImagePicker();
    final XFile? selectedImages =
        await picker.pickImage(imageQuality: 50, source: source);

    if (selectedImages != null) {
      addFileAndConvert(selectedImages);
      // Get.back();
      // controller.files.refresh();
    }
    Get.back();
  }
}
