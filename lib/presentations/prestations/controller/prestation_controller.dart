import 'dart:developer';
import 'dart:io';

import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/app/app_prefs.dart';
import 'package:eagri/app/constant.dart';
import 'package:eagri/data/mapper/shop_mapper.dart';
import 'package:eagri/data/repository/api/repository_impl.dart';
import 'package:eagri/data/request/request.dart';
import 'package:eagri/data/request/shop_resquests.dart';
import 'package:eagri/domain/model/user_model.dart';
import 'package:eagri/domain/usecase/location_usecase.dart';
import 'package:eagri/presentations/catalogue/controllers/catalogue_controller.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/common/state/succes_page_view.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

import '../../../app/di.dart';
import '../../../app/functions.dart';
import '../../../data/request/prestation_request.dart';
import '../../../domain/model/Product.dart';
import '../../../domain/model/prestation_model.dart';

import '../../common/location_select/location_select_widget.dart';
import '../addPrestation/add_image_description.dart';
import '../addPrestation/add_location.dart';
import '../addPrestation/add_prestation_screen.dart';
import '../addPrestation/add_prix_unite.dart';
import '../addPrestation/add_souscategory.dart';

class PrestationController extends GetxController {
  CatalogueController catalogueController = Get.put(CatalogueController());
  AppSharedPreference appSharedPreference = instance<AppSharedPreference>();
  RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
  var isLoadPrestation = true.obs;
  var listPrestation = <ProductModel>[].obs;
  var listCategory = <CategoryPrestation>[].obs;
  var categorieIdControler = 0.obs;
  var souscategorieIdControler = 0.obs;
  var priceController = TextEditingController().obs;
  var uniteController = TextEditingController().obs;
  var descriptionController = TextEditingController().obs;

  var dateDeDisponibilite = TextEditingController().obs;
  var productRegionValue = 0.obs;
  var productDepartementValue = 0.obs;
  var productSprefectureValue = 0.obs;
  var productLocaliteValue = 0.obs;
  var loadDemandeRecu = true.obs;
  var loadDemandeSend = true.obs;
  var locationController = LocationReturnModel().obs;
  var listRegionMap = <Map<String, dynamic>>[].obs;
  var listeDepartementMap = <Map<String, dynamic>>[].obs;
  var listeSousPrefectureMap = <Map<String, dynamic>>[].obs;
  var listLocaliteMap = <Map<String, dynamic>>[].obs;
  final files = <File>[].obs;
  var listCategoryMap = <Map<String, dynamic>>[].obs;
  var listSousCategoryCategoryMap = <Map<String, dynamic>>[].obs;
  var listUniteMap = <Map<String, dynamic>>[].obs;

  var prixMinController = TextEditingController().obs;
  var prixMaxController = TextEditingController().obs;

  var valueDisponibility = "IMMEDIAT".obs;
  LocationUsecase locationUsecase = instance<LocationUsecase>();

  var listSousCategoryCategory = <SousCategoryPrestation>[].obs;
  var locationReturn = LocationReturnModel().obs;
  var isloadParams = true.obs;
  var isLoadMorePresentation = false.obs;
  var currentPage = 0.obs;
  var listPrestationSend = <DataDemande>[].obs;
  var listPrestationRecu = <DataDemande>[].obs;

//demande prestation input**********************

  var dateDeDebut = TextEditingController().obs;
  var dateDeFin = TextEditingController().obs;
  var quantity = TextEditingController().obs;

//demande prestation input**********************
  var filterCategorieId = TextEditingController(text: "0").obs;
  var filterSousCategorieId = TextEditingController(text: "0").obs;

  var scrollController = ScrollController().obs;
  var page = 0.obs;
  var limitPage = 10.obs;
  var totalItems = 0.obs;
  var nextPage = 0.obs;

  List<Widget> listeStep = [
    const AddPrestationCategoryScreen(),
    const SousCategoryBloc(),
    const AddPriceUniteBloc(),
    const AddLocation(),
    const AddProductImageDescriptionBloc(),
  ];
  @override
  onInit() async {
    scrollController.value.addListener(_scrollListener);
    isLoadPrestation.value = true;
    await getListPrestation();
    isLoadPrestation.value = false;

    getListCategoryPrestation();
    super.onInit();
  }

  validContrat(int offerId) async {
    Get.dialog(const LoaderComponent());
    await appSharedPreference.getUserInformation().then((user) {
      repositoryImpl.validContractPdf(user.id!, offerId, true).then((response) {
        response.fold(
          (failure) {
            Get.back();
            showCustomFlushbar(Get.context!, "Echec", ColorManager.error,
                FlushbarPosition.TOP);
          },
          (data) {
            Get.back();
            Get.back();
            getListDemandeReceive();
            getListDemandeSend();
            Get.off(const SuccessPageView(
              message: "Contrat accepté",
            ));
          },
        );
      });
    });
  }

  demandePrestation(int prestationId) async {
    int regionId = locationReturn.value.region != null
        ? locationReturn.value.region!.id
        : 0;
    int departementId = locationReturn.value.departement != null
        ? locationReturn.value.departement!.id
        : 0;
    int sprefectureId = locationReturn.value.sprefecture != null
        ? locationReturn.value.sprefecture!.id
        : 0;
    int localiteId = locationReturn.value.localite != null
        ? locationReturn.value.localite!.id
        : 0;
    List<int> location = [
      regionId,
      departementId,
      sprefectureId,
      localiteId,
    ];

    inspect(location);
    Get.dialog(const LoaderComponent());
    await appSharedPreference.getUserInformation().then((value) {
      repositoryImpl
          .demandePrestion(
        DemandePrestionRequest(
            dateEnd: dateDeFin.value.text,
            dateStart: dateDeDebut.value.text,
            prestationId: prestationId,
            userId: value.id!,
            quantity: int.parse(quantity.value.text),
            location: location),
      )
          .then((response) {
        response.fold((l) {
          Get.back();

          showCustomFlushbar(Get.context!, l.message, ColorManager.error,
              FlushbarPosition.TOP);
        }, (r) {
          Get.back();
          Get.off(const SuccessPageView(
            message: "Votre démande a bien été envoyée au prestataire",
          ));
        });
      });
    });
  }

  addFileAndConvert(XFile file) {
    final newFile = File(file.path);
    files.add(newFile);
  }

  fetchSpFectureOfDepartement(int idDepartement) async {
    (await locationUsecase.getListSpOfDepartement(idDepartement)).fold(
      (l) => null,
      (r) {
        listeSousPrefectureMap.value = r.listSousPrefecture!
            .map((element) =>
                {"value": element.sousPrefectureId, "label": element.name})
            .toList();
      },
    );
  }

  getParams() async {
    await getListCategoryPrestation();
    await getRegion();
    await getPrestationUnites();
    isloadParams.value = false;
  }

  fetchDepartement(int idRegion) async {
    (await locationUsecase.getListDepartement(idRegion)).fold(
      (l) => null,
      (r) {
        listeDepartementMap.value = r.listDepartement!
            .map((element) =>
                {"value": element.departementId, "label": element.name})
            .toList();
      },
    );
  }

  Future getRegion() async {
    (await locationUsecase.getListRegion()).fold((failure) {}, (result) {
      listRegionMap.value = result.data
          .map((element) => {"value": element.regionId, "label": element.name})
          .toList();
    });
  }

  fecthLocalite(int idSp) async {
    (await locationUsecase.getListLocaliteOfSp(idSp)).fold(
      (l) => null,
      (r) {
        listLocaliteMap.value = r.listLocalite!
            .map((element) =>
                {"value": element.localiteId, "label": element.name})
            .toList();
      },
    );
  }

  getListCategoryPrestation() async {
    await repositoryImpl
        .getListCategoryPrestation()
        .then((value) => value.fold((l) => null, (list) async {
              listCategory.value = list.data;
              listCategoryMap.value = list.data
                  .map(
                      (element) => {"value": element.id, "label": element.name})
                  .toList();
            }));
  }

  fetchSousCategory(int id) {
    listSousCategoryCategoryMap.value = listCategory
        .firstWhere((element) => element.id == id)
        .sousCategoryPurchases!
        .map((element) => {"value": element.id, "label": element.name})
        .toList();
  }

  resetFilter(int filterCategorieIdNew) {
    locationController.value.region = null;
    locationController.value.departement = null;
    locationController.value.sprefecture = null;
    locationController.value.localite = null;
    filterCategorieId.value.text = "$filterCategorieIdNew";
    prixMinController.value.clear();
    prixMaxController.value.clear();
  }

  getListPrestation() async {
    await repositoryImpl
        .searchProduct(
          SearchProductRequest(
            status: "AVAILABLE",
            limit: limitPage.value,
            page: page.value,
            codeTypeProduit: codePrestation,
            categorieId: filterCategorieId.value.text != "0"
                ? int.parse(filterCategorieId.value.text)
                : null,
            priceMax: prixMaxController.value.text != ""
                ? double.parse(prixMaxController.value.text)
                : null,
            priceMin: prixMinController.value.text != ""
                ? double.parse(prixMinController.value.text)
                : null,
            regionId: locationController.value.region != null
                ? locationController.value.region!.id
                : null,
            departementId: locationController.value.departement != null
                ? locationController.value.departement!.id
                : null,
            sprefectureId: locationController.value.sprefecture != null
                ? locationController.value.sprefecture!.id
                : null,
            locationId: locationController.value.localite != null
                ? locationController.value.localite!.id
                : null,
          ),
        )
        .then((value) => value.fold((l) => null, (list) async {
              listPrestation.value = await list.toDomain();
            }));

    isLoadPrestation.value = false;
  }

  moreGetListPrestation() async {
    await repositoryImpl
        .searchProduct(
          SearchProductRequest(
            limit: limitPage.value,
            page: page.value,
            codeTypeProduit: codePrestation,
            categorieId: filterCategorieId.value.text != "0"
                ? int.parse(filterCategorieId.value.text)
                : null,
            priceMax: prixMaxController.value.text != ""
                ? double.parse(prixMaxController.value.text)
                : null,
            priceMin: prixMinController.value.text != ""
                ? double.parse(prixMinController.value.text)
                : null,
            regionId: locationController.value.region != null
                ? locationController.value.region!.id
                : null,
            departementId: locationController.value.departement != null
                ? locationController.value.departement!.id
                : null,
            sprefectureId: locationController.value.sprefecture != null
                ? locationController.value.sprefecture!.id
                : null,
            locationId: locationController.value.localite != null
                ? locationController.value.localite!.id
                : null,
          ),
        )
        .then((value) => value.fold((l) => null, (list) async {
              final data = await list.toDomain();
              for (var i = 0; i < data.length; i++) {
                listPrestation.add(data[i]);
              }
              isLoadMorePresentation.value = false;
            }));
  }

  getListDemandeReceive() async {
    loadDemandeSend.value = true;
    await appSharedPreference.getUserInformation().then((value) async {
      await repositoryImpl
          .getDemandePrestationRecu(value.id!)
          .then((response) async {
        response.fold((l) {
          loadDemandeSend.value = false;
        }, (r) async {
          listPrestationRecu.value = [];
          await Future.forEach(r.data, (PrestationData element) async {
            UserModel user = UserModel();
            await repositoryImpl
                .getInfoById(
              InfoByIdRequest(userId: element.userId),
            )
                .then((response) async {
              await response.fold((l) {}, (userData) async {
                if (userData.user != null) {
                  user = UserModel(
                      firstName: userData.user!.firstname,
                      lastName: userData.user!.laststname);
                }
              });
            });
            listPrestationRecu.add(DataDemande(
              user: user,
              prestationDemandeData: element,
              isReceive: true,
            ));
          });

          loadDemandeRecu.value = false;

          //  repositoryImpl.getDetailProduct(idProduct)
        });
      });
    });
  }

  getListDemandeSend() async {
    loadDemandeSend.value = true;
    await appSharedPreference.getUserInformation().then((value) async {
      await repositoryImpl
          .getDemandePrestationSend(value.id!)
          .then((response) async {
        response.fold((l) {
          loadDemandeSend.value = false;
        }, (r) async {
          listPrestationSend.value = [];
          await Future.forEach(r.data, (PrestationData element) async {
            UserModel user = UserModel();
            await repositoryImpl
                .getInfoById(
              InfoByIdRequest(userId: element.userId),
            )
                .then((response) async {
              await response.fold((l) {}, (userData) async {
                if (userData.user != null) {
                  user = UserModel(
                      firstName: userData.user!.firstname,
                      lastName: userData.user!.laststname);
                }
              });
            });
            listPrestationSend.add(DataDemande(
              user: user,
              prestationDemandeData: element,
              isReceive: false,
            ));
          });
          loadDemandeSend.value = false;

          //  repositoryImpl.getDetailProduct(idProduct)
        });
      });
    });
  }

  addPrestation() async {
    final DateFormat formatter = DateFormat('yyyy-MM-dd');
    Get.dialog(const LoaderComponent());
    await createCatalogue();

    await appSharedPreference.getUserInformation().then((value) async {
      AddPrestationRequest addPrestationRequest = AddPrestationRequest(
        availability: valueDisponibility.value,
        categorieId: categorieIdControler.value,
        description: descriptionController.value.text,
        fournisseurId: value.id!,
        location: [
          productRegionValue.value,
          productDepartementValue.value,
          productSprefectureValue.value,
          productLocaliteValue.value
        ],
        availabilityDate: valueDisponibility.value == "IMMEDIAT"
            ? null
            : formatter.format(DateTime.parse(dateDeDisponibilite.value.text)),
        paymentId: 1,
        price: int.parse(priceController.value.text),
        speculationId: souscategorieIdControler.value,
        unitOfMeasurment: uniteController.value.text,
        images: files,
      );
      await repositoryImpl
          .addPrestion(addPrestationRequest)
          .then((response) async {
        response.fold((l) {
          Get.back();
          showCustomFlushbar(Get.context!, "Echec de la publiation",
              ColorManager.error, FlushbarPosition.TOP);
        }, (resultat) {
          catalogueController.getListFnCodeProduct(codePrestation.toString());
          Get.back();
          Get.back();
          Get.off(const SuccessPageView(
            message: "Votre prestation a bien été publiée",
          ));
        });
      });
    });
  }

  Future<bool> validateDemandePrestation(int prestationId) async {
    Get.dialog(const LoaderComponent());
    bool isAccepted = false;
    await repositoryImpl
        .validateDemandePrestation(prestationId)
        .then((response) => response.fold((l) async {
              Get.back();
              showCustomFlushbar(
                  Get.context!,
                  "Désolé, Une erreur s'est produite veuillez réessayer plustard",
                  ColorManager.red,
                  FlushbarPosition.TOP);
            }, (r) async {
              //  await appSharedPreference.getUserInformation().then((value) {});
              getListDemandeReceive();
              isAccepted = true;
              Get.back();
              showCustomFlushbar(Get.context!, "Démande acceptée",
                  ColorManager.succes, FlushbarPosition.TOP);
            }));
    return isAccepted;
  }

  getPrestationUnites() async {
    await repositoryImpl.getListPrestationUnite().then((response) {
      response.fold((l) => null, (r) {
        listUniteMap.value = r.data
            .map((element) => {"value": element.id, "label": element.name})
            .toList();
      });
    });
  }

  void _scrollListener() async {
    // limitPage.value = nextPage.value;
    if (isLoadMorePresentation.value &&
        totalItems.value > listPrestation.length) {
      return null;
    }
    if (scrollController.value.offset >=
            scrollController.value.position.maxScrollExtent &&
        !scrollController.value.position.outOfRange) {
      log("EN LOAD MORE");
      page.value = page.value + 1;
      isLoadMorePresentation.value = true;
      await moreGetListPrestation();
    }
  }
}
