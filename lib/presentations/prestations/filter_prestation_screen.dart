import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/default_button.dart';
import 'package:eagri/presentations/prestations/controller/prestation_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:select_form_field/select_form_field.dart';

import '../common/location_select/location_select_widget.dart';
import '../ressources/styles_manager.dart';

class FilterPrestationScreen extends StatefulWidget {
  const FilterPrestationScreen({Key? key}) : super(key: key);

  @override
  State<FilterPrestationScreen> createState() => _FilterPrestationScreenState();
}

class _FilterPrestationScreenState extends State<FilterPrestationScreen> {
  final _focusNodePrixMin = FocusNode();
  final _focusNodePrixMax = FocusNode();
  @override
  void dispose() {
    _focusNodePrixMin.dispose();
    _focusNodePrixMax.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    PrestationController prestationController = Get.find();
    return Scaffold(
      appBar: AppBar(
        leading: const BackButtonCustom(),
        title: const Text('Filtrer'),
      ),
      body: Container(
        margin: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Catégories de prestation',
              style: getMeduimTextStyle(fontSize: 14),
            ),
            const SizedBox(
              height: 10,
            ),
            Obx(
              () => SelectFormField(
                controller: prestationController.filterCategorieId.value,

                type: SelectFormFieldType.dialog, // or can be dialog
                enableSearch: true,
                // initialValue: 'circle',
                labelText: 'Choisir la Categorie',

                items: prestationController.listCategoryMap,
                onChanged: (val) {
                  setState(() {
                    prestationController.filterSousCategorieId.value.clear();
                  });
                  prestationController.fetchSousCategory(int.parse(val));
                },
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              'Sous catégorie de la prestation',
              style: getMeduimTextStyle(fontSize: 14),
            ),
            const SizedBox(
              height: 10,
            ),
            Obx(
              () => SelectFormField(
                controller: prestationController.filterSousCategorieId.value,

                type: SelectFormFieldType.dialog, // or can be dialog
                enableSearch: true,
                // initialValue: 'circle',
                labelText: 'Choisir la sous Categorie',

                items: prestationController.listSousCategoryCategoryMap,
                onChanged: (val) {},
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              'Prix',
              style: getBoldTextStyle(fontSize: 14),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: SizedBox(
                    width: double.infinity,
                    height: 40,
                    child: TextField(
                      controller: prestationController.prixMinController.value,
                      focusNode: _focusNodePrixMin,
                      keyboardType: const TextInputType.numberWithOptions(),
                      decoration: const InputDecoration(
                          label: Text('Prix min'),
                          hintText: 'Enter le prix min'),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: SizedBox(
                    height: 40,
                    child: TextField(
                      controller: prestationController.prixMaxController.value,
                      focusNode: _focusNodePrixMax,
                      keyboardType: const TextInputType.numberWithOptions(),
                      decoration: const InputDecoration(
                          label: Text('Prix max'),
                          hintText: 'Enter le prix max'),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              'Lieu',
              style: getMeduimTextStyle(fontSize: 14),
            ),
            const SizedBox(
              height: 10,
            ),
            LocationSelectWidget(
              locationReturn: prestationController.locationController.value,
            ),
            const SizedBox(
              height: 10,
            ),
            DefaultButton(
                text: 'Filtrer',
                press: () async {
                  Get.back();
                  await prestationController.getListPrestation();
                }),
          ],
        ),
      ),
    );
  }
}
