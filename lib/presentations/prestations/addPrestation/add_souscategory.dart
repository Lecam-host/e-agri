import 'package:eagri/presentations/prestations/controller/prestation_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../ressources/color_manager.dart';
import '../../ressources/icon_manager.dart';
import '../../ressources/styles_manager.dart';

class SousCategoryBloc extends StatefulWidget {
  const SousCategoryBloc({
    Key? key,
  }) : super(key: key);

  @override
  State<SousCategoryBloc> createState() => _SousCategoryBlocState();
}

class _SousCategoryBlocState extends State<SousCategoryBloc> {
  PrestationController prestationController = Get.put(PrestationController());
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          "Choissez la sous categorie",
          style: getBoldTextStyle(
            color: Colors.black,
            fontSize: 20,
          ),
        ),
        const SizedBox(
          height: 30,
        ),
        ListView.builder(
          shrinkWrap: true,
          itemCount: prestationController.listSousCategoryCategory.length,
          itemBuilder: (context, index) => Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              color: ColorManager.white,
              child: ListTile(
                onTap: () {
                  prestationController.souscategorieIdControler.value =
                      prestationController.listSousCategoryCategory[index].id;
                  prestationController.currentPage.value++;
                },
                leading: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: ColorManager.grey,
                  ),
                  height: 30,
                  width: 30,
                ),
                title: Text(
                  prestationController.listSousCategoryCategory[index].name,
                ),
                trailing: const Icon(IconManager.arrowRight),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
