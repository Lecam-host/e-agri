import 'package:eagri/presentations/ressources/icon_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../common/state/loader_component.dart';
import '../../ressources/color_manager.dart';
import '../../ressources/styles_manager.dart';
import '../controller/prestation_controller.dart';

class AddPrestationCategoryScreen extends StatefulWidget {
  const AddPrestationCategoryScreen({Key? key}) : super(key: key);

  @override
  State<AddPrestationCategoryScreen> createState() =>
      _AddPrestationCategoryScreenState();
}

class _AddPrestationCategoryScreenState
    extends State<AddPrestationCategoryScreen> {
  PrestationController prestationController = Get.put(PrestationController());
  @override
  void initState() {
    prestationController.getParams();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(
        () {
          return prestationController.isloadParams.value
              ? const LoaderComponent()
              : Column(
                  children: [
                    Text(
                      "Choissez la categorie",
                      style: getBoldTextStyle(
                        color: Colors.black,
                        fontSize: 20,
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    ListView.builder(
                      shrinkWrap: true,
                      itemCount: prestationController.listCategory.length,
                      itemBuilder: (context, index) => Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: Container(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          color: ColorManager.white,
                          child: ListTile(
                            onTap: () {
                              prestationController
                                      .listSousCategoryCategory.value =
                                  prestationController.listCategory[index]
                                      .sousCategoryPurchases!;
                              prestationController.categorieIdControler.value =
                                  prestationController.listCategory[index].id;
                              prestationController.currentPage.value++;
                            },
                            leading: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: ColorManager.grey,
                              ),
                              height: 30,
                              width: 30,
                            ),
                            title: Text(
                              prestationController.listCategory[index].name,
                            ),
                            trailing: const Icon(IconManager.arrowRight),
                          ),
                        ),
                      ),
                    ),
                  ],
                );
        },
      ),
    );
  }
}
