import 'package:eagri/presentations/prestations/controller/prestation_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:select_form_field/select_form_field.dart';

class AddLocation extends StatelessWidget {
  const AddLocation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    PrestationController prestationController = Get.put(PrestationController());
    return Column(
      children: [
        const SizedBox(
          height: 30,
        ),
        SelectFormField(
          type: SelectFormFieldType.dialog,
          enableSearch: true, // or can be dialog
          //initialValue: 'circle',

          labelText: 'Choisir la region',
          items: prestationController.listRegionMap,
          onChanged: (val) {
            prestationController.productRegionValue.value = int.parse(val);

            prestationController.fetchDepartement(int.parse(val));
          },
        ),
        const SizedBox(
          height: 30,
        ),
        Obx(() => prestationController.listeDepartementMap.isEmpty
            ? Container()
            : Column(
                children: [
                  SelectFormField(
                    type: SelectFormFieldType.dropdown, // or can be dialog
                    labelText: "Choisir le departement",
                    items: prestationController.listeDepartementMap,
                    onChanged: (val) {
                      prestationController.productDepartementValue.value =
                          int.parse(val);
                      prestationController
                          .fetchSpFectureOfDepartement(int.parse(val));
                    },
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                ],
              )),
        Obx(
          () => prestationController.listeSousPrefectureMap.isEmpty
              ? Container()
              : Column(
                  children: [
                    SelectFormField(
                      type: SelectFormFieldType.dropdown, // or can be dialog
                      labelText: "Choisir la sous-prefercture",
                      items: prestationController.listeSousPrefectureMap,
                      onChanged: (val) {
                        prestationController.productSprefectureValue.value =
                            int.parse(val);
                        prestationController.fecthLocalite(int.parse(val));
                      },
                    ),
                    const SizedBox(
                      height: 30,
                    )
                  ],
                ),
        ),
        Obx(
          () => prestationController.listLocaliteMap.isEmpty
              ? Container()
              : SelectFormField(
                  type: SelectFormFieldType.dropdown, // or can be dialog
                  labelText: "Choisir la localité",
                  items: prestationController.listLocaliteMap,
                  onChanged: (val) {
                    prestationController.productLocaliteValue.value =
                        int.parse(val);
                  },
                ),
        )
      ],
    );
  }
}
