// ignore_for_file: constant_identifier_names

import 'package:eagri/presentations/common/date_field.dart';
import 'package:eagri/presentations/prestations/controller/prestation_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:select_form_field/select_form_field.dart';

import '../../ressources/styles_manager.dart';

class AddPriceUniteBloc extends StatefulWidget {
  const AddPriceUniteBloc({Key? key}) : super(key: key);

  @override
  State<AddPriceUniteBloc> createState() => _AddPriceUniteBlocState();
}

enum Disponibility { FUTURE, IMMEDIAT }

class _AddPriceUniteBlocState extends State<AddPriceUniteBloc> {
  Disponibility disponibilityValue = Disponibility.IMMEDIAT;

  PrestationController prestationController = Get.put(PrestationController());
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Form(
              child: Column(
            children: [
              const SizedBox(
                height: 20,
              ),
              TextFormField(
                controller: prestationController.priceController.value,
                decoration: InputDecoration(
                  // hintText:"La ",
                  label: Text(
                    "Prix",
                    style: getRegularTextStyle(color: Colors.grey),
                  ),
                ),
                keyboardType: TextInputType.number,
                maxLines: null,
              ),
              const SizedBox(
                height: 20,
              ),
              SelectFormField(
                type: SelectFormFieldType.dialog,
                enableSearch: true, // or can be dialog
                //initialValue: 'circle',
                controller: prestationController.uniteController.value,
                labelText: "Choisir l'unité",
                items: prestationController.listUniteMap,
                onChanged: (val) {},
              ),
              const SizedBox(
                height: 20,
              ),
              Obx(
                () => Column(
                  children: [
                    const Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Le produit est-il disponible maintenant ?",
                      ),
                    ),
                    RadioListTile(
                      title: const Text("Oui"),
                      value: Disponibility.IMMEDIAT,
                      groupValue: disponibilityValue,
                      onChanged: (value) {
                        setState(() {
                          disponibilityValue = Disponibility.IMMEDIAT;
                          prestationController.valueDisponibility.value =
                              "IMMEDIAT";
                        });
                        // print(addProductController.valueDisponibility);
                      },
                    ),
                    RadioListTile(
                      title: const Text("Non"),
                      value: Disponibility.FUTURE,
                      groupValue: disponibilityValue,
                      onChanged: (value) {
                        setState(
                          () {
                            disponibilityValue = Disponibility.FUTURE;
                            prestationController.valueDisponibility.value =
                                "FUTUR";
                          },
                        );
                        // print(addProductController.valueDisponibility);
                      },
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    prestationController.valueDisponibility.value == "IMMEDIAT"
                        ? Container()
                        : DateField(
                            dateController:
                                prestationController.dateDeDisponibilite.value,
                            hintText: "Choisir la date de disponibilité",
                          ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
            ],
          ))
        ],
      ),
    );
  }
}
