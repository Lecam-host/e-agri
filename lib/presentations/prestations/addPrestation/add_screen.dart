import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/prestations/controller/prestation_controller.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

import '../../common/default_button.dart';

class AddPrestationScreen extends StatefulWidget {
  const AddPrestationScreen({super.key});

  @override
  State<AddPrestationScreen> createState() => _AddPrestationScreenState();
}

class _AddPrestationScreenState extends State<AddPrestationScreen> {
  PrestationController prestationController = Get.put(PrestationController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        title: const Text('Publication'),
        centerTitle: true,
        actions: [
          TextButton(
            onPressed: () {
              Get.back();
            },
            child: Text(
              "Annuler",
              style: getSemiBoldTextStyle(color: ColorManager.primary2),
            ),
          ),
        ],
      ),
      // floatingActionButton: FloatingActionButton(
      //     onPressed: () {
      //       // await prestationController.addIntrant();
      //     },
      //     child: const Icon(
      //       Icons.arrow_forward_ios,
      //     )),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            Column(
              children: [
                Obx(() {
                  return Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Étape ${prestationController.currentPage.value + 1}/${prestationController.listeStep.length}",
                      style: getSemiBoldTextStyle(fontSize: 16),
                    ),
                  );
                }),
                const SizedBox(
                  height: 10,
                ),
                Obx(
                  () {
                    return StepProgressIndicator(
                      totalSteps: prestationController.listeStep.length,
                      currentStep: prestationController.currentPage.value + 1,
                      selectedColor: ColorManager.primary,
                      unselectedColor: Colors.grey,
                    );
                  },
                ),
                const SizedBox(
                  height: 10,
                ),
                Obx(
                  () {
                    return prestationController.currentPage.value <= 0
                        ? Container()
                        : GestureDetector(
                            onTap: () {
                              prestationController.currentPage.value--;
                            },
                            child: Row(
                              children: [
                                Icon(
                                  Icons.arrow_back_ios,
                                  color: ColorManager.primary2,
                                ),
                                Text(
                                  "Retour",
                                  style: getSemiBoldTextStyle(
                                      fontSize: 16,
                                      color: ColorManager.primary2),
                                ),
                              ],
                            ),
                          );
                  },
                )
              ],
            ),
            const SizedBox(height: 20),
            Expanded(
              child: Obx(() {
                return AnimatedContainer(
                    duration: const Duration(milliseconds: 500),
                    curve: Curves.easeInOut,
                    child: prestationController
                        .listeStep[prestationController.currentPage.value]);
              }),
            ),
            const SizedBox(
              height: 10,
            ),
            Obx(
              () {
                return prestationController.currentPage.value > 1
                    ? DefaultButton(
                        text: prestationController.currentPage.value <
                                prestationController.listeStep.length - 1
                            ? "Suivant"
                            : "Publier",
                        press: () async {
                          if (prestationController.currentPage.value <
                              prestationController.listeStep.length - 1) {
                            if (prestationController.currentPage.value == 2) {
                              if (prestationController
                                      .priceController.value.text ==
                                  "") {
                                showCustomFlushbar(
                                  context,
                                  "Entrer le prix",
                                  ColorManager.error,
                                  FlushbarPosition.TOP,
                                );
                              } else if (prestationController
                                      .uniteController.value.text ==
                                  "") {
                                showCustomFlushbar(
                                  context,
                                  "Entrer l'unité",
                                  ColorManager.error,
                                  FlushbarPosition.TOP,
                                );
                              } else if (prestationController
                                          .dateDeDisponibilite.value.text ==
                                      "" &&
                                  prestationController
                                          .valueDisponibility.value ==
                                      "FUTUR") {
                                showCustomFlushbar(
                                  context,
                                  "Entrer la date de disponibilté",
                                  ColorManager.error,
                                  FlushbarPosition.TOP,
                                );
                              } else {
                                prestationController.currentPage.value++;
                              }
                            } else if (prestationController
                                        .productRegionValue.value ==
                                    0 &&
                                prestationController.currentPage.value == 3) {
                              showCustomFlushbar(
                                context,
                                "Entrer la region",
                                ColorManager.error,
                                FlushbarPosition.TOP,
                              );
                            } else {
                              prestationController.currentPage.value++;
                            }
                          } else {
                            if (prestationController
                                    .descriptionController.value.text ==
                                "") {
                              showCustomFlushbar(
                                context,
                                "Entrer la description",
                                ColorManager.error,
                                FlushbarPosition.TOP,
                              );
                            } else if (prestationController.files.isEmpty) {
                              showCustomFlushbar(
                                context,
                                "Veuillez ajouter des images",
                                ColorManager.error,
                                FlushbarPosition.TOP,
                              );
                            } else {
                              await prestationController.addPrestation();
                            }

                            //  await prestationController.addIntrant();
                            // Get.to(SuccessPageView());
                            // Get.defaultDialog(
                            //   title: "Succès",
                            //   titleStyle: TextStyle(
                            //     color: ColorManager.primary2,
                            //   ),
                            //   onConfirm: () {},
                            //   onCancel: () {
                            // Get.to(SuccessPageView());
                            //   },
                            //   textCancel: "Non",
                            //   cancelTextColor: ColorManager.primary,
                            //   textConfirm: "Oui",
                            //   buttonColor: ColorManager.primary,
                            //   content: const Text(
                            //     "Voulez vous publiez le même produit dans d'autre région ?",
                            //     textAlign: TextAlign.center,
                            //   ),
                            // );

                          }
                        },
                      )
                    : const SizedBox();
              },
            ),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }
}
