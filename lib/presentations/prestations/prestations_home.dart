import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/prestations/component/prestation_card.dart';
import 'package:eagri/presentations/prestations/controller/prestation_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

import '../../app/constant.dart';
import '../../app/functions.dart';
import '../../domain/model/user_model.dart';
import '../ressources/assets_manager.dart';
import '../ressources/color_manager.dart';
import '../ressources/styles_manager.dart';
import 'filter_prestation_screen.dart';

class PrestationHome extends StatefulWidget {
  const PrestationHome({Key? key}) : super(key: key);

  @override
  State<PrestationHome> createState() => _PrestationHomeState();
}

class _PrestationHomeState extends State<PrestationHome> {
  PrestationController prestationController = Get.put(PrestationController());
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
        builder: (context, user, child) => Scaffold(
              backgroundColor: ColorManager.backgroundColor,
              appBar: AppBar(
                leading: const BackButtonCustom(),
                title: const Text("Prestation"),
                actions: [
                  if (isInScopes(
                          UserPermissions.RESERVATION_FILTER, user.scopes!) ==
                      true)
                    Container(
                      margin: const EdgeInsets.only(
                        top: 5,
                      ),
                      child: IconButton(
                        icon: SvgPicture.asset(
                          SvgManager.filter,
                          width: 20,
                        ),
                        onPressed: () {
                          Navigator.push(
                            context,
                            PageTransition(
                              type: PageTransitionType.rightToLeft,
                              child: const FilterPrestationScreen(),
                              isIos: true,
                              duration: const Duration(
                                  milliseconds: Constant.navigatorDuration),
                            ),
                          ).then((value) {
                            super.widget;
                            //_tabController.animateTo(value);
                          });
                        },
                      ),
                    ),
                ],
              ),
              body: Column(
               
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 5,
                  ),
                  Obx(
                    () => SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(children: [
                        categoryButton("Tout", 0),
                        ...List.generate(
                            prestationController.listCategory.length, (i) {
                          return categoryButton(
                              prestationController.listCategory[i].name,
                              prestationController.listCategory[i].id);
                        }),
                      ]),
                    ),
                  ),
                  Obx(
                    () => prestationController.isLoadPrestation.value == false
                        ? prestationController.listPrestation.isNotEmpty
                            ? Expanded(
                              child: ListView.builder(
                                  shrinkWrap: true,
                                  controller: prestationController
                                      .scrollController.value,
                                  itemCount:
                                      prestationController.totalItems.value >
                                              prestationController
                                                  .listPrestation.length
                                          ? prestationController
                                                  .listPrestation.length +
                                              1
                                          : prestationController
                                              .listPrestation.length,
                                  itemBuilder: (context, index) => index ==
                                              prestationController
                                                  .listPrestation.length &&
                                          prestationController
                                                  .totalItems.value >
                                              prestationController
                                                  .listPrestation.length
                                      ? shimmerCustom()
                                      : Container(
                                        padding:
                                            const EdgeInsets.symmetric(
                                                horizontal: 10,vertical: 10),
                                        child: PrestationCard(
                                          productModel:
                                              prestationController
                                                  .listPrestation[index],
                                        ),
                                      ),
                                ),
                            )
                            : const EmptyComponenent(
                                message: "Liste vide",
                              )
                        : Expanded(
                          child: SingleChildScrollView(
                              child: Column(
                                children: [
                                  shimmerCustom(),
                                  shimmerCustom(),
                                  shimmerCustom(),
                                  shimmerCustom(),
                                ],
                              ),
                            ),
                        ),
                  ),
                ],
              ),
            ));
  }

  Widget categoryButton(String titre, int categorieId) {
    return Obx(
      () => InkWell(
        onTap: () async {
          setState(() {
            prestationController.page.value = 0;
            prestationController.filterCategorieId.value.text =
                categorieId.toString();
          });
          prestationController.isLoadPrestation.value = true;
          await prestationController.getListPrestation();
        },
        child: Container(
          margin: const EdgeInsets.only(
            left: 5,
            bottom: 5,
          ),
          padding: const EdgeInsets.all(5),
          decoration: BoxDecoration(
              color: prestationController.filterCategorieId.value.text ==
                      categorieId.toString()
                  ? ColorManager.primary
                  : ColorManager.lightGrey,
              borderRadius: const BorderRadius.all(Radius.circular(10))),
          child: Text(
            titre,
            style: getRegularTextStyle(color: ColorManager.white),
          ),
        ),
      ),
    );
  }
}

Shimmer shimmerCustom() {
  return Shimmer.fromColors(
    baseColor: const Color.fromARGB(255, 178, 178, 178),
    highlightColor: const Color.fromARGB(255, 224, 223, 223),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.all(5),
          height: 10,
          width: 150,
          decoration: BoxDecoration(
            color: ColorManager.white,
            borderRadius: BorderRadius.circular(5),
          ),
        ),
        Container(
          margin: const EdgeInsets.all(5),
          height: 10,
          width: 200,
          decoration: BoxDecoration(
            color: ColorManager.white,
            borderRadius: BorderRadius.circular(5),
          ),
        ),
        Container(
          margin: const EdgeInsets.all(10),
          width: double.infinity,
          height: 200.0,
          decoration: BoxDecoration(
            color: ColorManager.white,
            borderRadius: BorderRadius.circular(15),
          ),
        ),
      ],
    ),
  );
}
