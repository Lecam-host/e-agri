import 'package:another_flushbar/flushbar.dart';
import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/date_field.dart';
import 'package:eagri/presentations/common/default_button.dart';
import 'package:eagri/presentations/common/flushAlert_componenent.dart';
import 'package:eagri/presentations/prestations/controller/prestation_controller.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../domain/model/Product.dart';
import '../common/location_select/location_select_widget.dart';
import '../ressources/styles_manager.dart';

class DemandePrestation extends StatefulWidget {
  const DemandePrestation({Key? key, required this.prestation})
      : super(key: key);
  final ProductModel prestation;
  @override
  State<DemandePrestation> createState() => _DemandePrestationState();
}

class _DemandePrestationState extends State<DemandePrestation> {
  PrestationController prestationController = Get.put(PrestationController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButtonCustom(),
        title: const Text('Demande'),
      ),
      body: Container(
        margin: const EdgeInsets.all(10),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextFormField(
                controller: prestationController.quantity.value,
                decoration: InputDecoration(
                  // hintText:"La ",

                  label: Text(
                    "Quantité",
                    style: getRegularTextStyle(color: Colors.grey),
                  ),
                ),
                keyboardType: TextInputType.number,
              ),
              const SizedBox(
                height: 20,
              ),
              const Text(
                  'Pendant combien de temps avez-vous besoin de ce prestataire'),
              const SizedBox(
                height: 10,
              ),
              DateField(
                  dateController: prestationController.dateDeDebut.value,
                  hintText: "Date de debut"),
              const SizedBox(
                height: 10,
              ),
              DateField(
                  dateController: prestationController.dateDeFin.value,
                  hintText: "Date de fin"),
              const SizedBox(
                height: 30,
              ),
              const Text(
                  'Precisez le lieu dans lequel le service sera effectué'),
              LocationSelectWidget(
                locationReturn: prestationController.locationReturn.value,
              ),
              DefaultButton(
                text: "Envoyer",
                press: () {
                  // inspect(prestationController.locationReturn.value);

                  if (prestationController.dateDeFin.value.text == "" ||
                      prestationController.dateDeDebut.value.text == "" ||
                      prestationController.quantity.value.text == "") {
                    showCustomFlushbar(
                        context,
                        "Veuillez remplir tous les champs",
                        ColorManager.black,
                        FlushbarPosition.BOTTOM);
                  } else if (prestationController.locationReturn.value.region ==
                      null) {
                    showCustomFlushbar(context, "Precisez au moins la région",
                        ColorManager.black, FlushbarPosition.BOTTOM);
                  } else {
                    prestationController
                        .demandePrestation(widget.prestation.id);
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
