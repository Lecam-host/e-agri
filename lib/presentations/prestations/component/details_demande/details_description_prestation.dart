import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';

import '../../../../app/functions.dart';
import '../../../../domain/model/prestation_model.dart';

class DetatilsDescriptionPrestation extends StatelessWidget {
  final DataDemande demandeData;

  const DetatilsDescriptionPrestation({Key? key, required this.demandeData})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            demandeData.prestation!.title,
            style: getBoldTextStyle(
              fontSize: 14,
              color: ColorManager.primary,
            ),
          ),
          Text(
            "Service",
            style: getRegularTextStyle(
              color: ColorManager.grey,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            "Info sur la demande",
            style: getMeduimTextStyle(
              color: ColorManager.black,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            color: ColorManager.white,
            width: double.infinity,
            padding: const EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  demandeData.isReceive == true
                      ? "Demande reçue"
                      : "Demande envoyée",
                  style: getBoldTextStyle(
                    fontSize: 12,
                    color: ColorManager.jaune,
                  ),
                ),
                Text(
                  "Type de demande",
                  style: getRegularTextStyle(
                    color: ColorManager.grey,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                cardInfo("Demandeur",
                    "${demandeData.user.firstName} ${demandeData.user.lastName}"),
                const SizedBox(
                  height: 10,
                ),
                cardInfo(
                  "Période démandée",
                  " Du ${convertDate(demandeData.prestationDemandeData.dateStart)} au ${convertDate(demandeData.prestationDemandeData.dateEnd)}",
                ),
                const SizedBox(
                  height: 10,
                ),
                cardInfo(
                  "Quantité ",
                  "${demandeData.prestationDemandeData.quantity} ${demandeData.prestation!.unite}",
                ),
                const SizedBox(
                  height: 10,
                ),
                cardInfo(
                  "Cout du service ",
                  "${separateur(demandeData.prestationDemandeData.total.toDouble())} FCFA",
                ),
                const SizedBox(
                  height: 10,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

Widget cardInfo(String title, String value) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        value,
        style: getBoldTextStyle(
          fontSize: 12,
          color: ColorManager.black,
        ),
      ),
      Text(
        title,
        style: getRegularTextStyle(
          color: ColorManager.grey,
        ),
      ),
    ],
  );
}
