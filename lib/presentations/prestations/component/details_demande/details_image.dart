import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/domain/model/prestation_model.dart';
import 'package:flutter/material.dart';

import '../../../../app/constant.dart';
import '../../../ressources/size_config.dart';

class DemandeImages extends StatefulWidget {
  const DemandeImages({
    Key? key,
    required this.dataDemande,
  }) : super(key: key);

  final DataDemande dataDemande;

  @override
  DemandeImagesState createState() => DemandeImagesState();
}

class DemandeImagesState extends State<DemandeImages> {
  int selectedImage = 0;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (widget.dataDemande.prestation!.images!.isNotEmpty) ...[
          SizedBox(
            width: getProportionateScreenWidth(238),
            child: AspectRatio(
              aspectRatio: 1,
              child: CachedNetworkImage(
                imageUrl: widget.dataDemande.prestation!.images![selectedImage],
                imageBuilder: (context, imageProvider) => Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    image: DecorationImage(
                      scale: 1,
                      image: imageProvider,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                placeholder: (context, url) => SizedBox(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                ),
                errorWidget: (context, url, error) => const Icon(Icons.error),
              ),
            ),
          ),
          SizedBox(height: getProportionateScreenWidth(20)),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ...List.generate(
                widget.dataDemande.prestation!.images!.length,
                (index) => buildSmallProductPreview(index),
              ),
            ],
          ),
        ],
        if (widget.dataDemande.prestation!.images!.isEmpty) ...[
          SizedBox(
            width: getProportionateScreenWidth(238),
            child: AspectRatio(
              aspectRatio: 1,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                ),
                child: const Icon(Icons.image),
              ),
            ),
          ),
          AnimatedContainer(
            duration: defaultDuration,
            margin: const EdgeInsets.only(right: 15),
            height: getProportionateScreenWidth(48),
            width: getProportionateScreenWidth(48),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.white,
              border: Border.all(color: kPrimaryColor.withOpacity(0)),
            ),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
              ),
              child: const Icon(Icons.image),
            ),
          ),
        ],
      ],
    );
  }

  GestureDetector buildSmallProductPreview(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedImage = index;
        });
      },
      child: AnimatedContainer(
        duration: defaultDuration,
        margin: const EdgeInsets.only(right: 15),
        height: getProportionateScreenWidth(48),
        width: getProportionateScreenWidth(48),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          border: Border.all(
              color: kPrimaryColor.withOpacity(selectedImage == index ? 1 : 0)),
        ),
        child: CachedNetworkImage(
          imageUrl: widget.dataDemande.prestation!.images![index],
          imageBuilder: (context, imageProvider) => Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                scale: 1,
                image: imageProvider,
                fit: BoxFit.cover,
              ),
            ),
          ),
          placeholder: (context, url) => SizedBox(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
          ),
          errorWidget: (context, url, error) => const Icon(Icons.error),
        ),
      ),
    );
  }
}
