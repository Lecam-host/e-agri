import 'package:eagri/presentations/common/default_button.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/prestations/component/pdf_contrat.dart';
import 'package:eagri/presentations/prestations/controller/prestation_controller.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../app/di.dart';
import '../../../data/repository/api/repository_impl.dart';
import '../../../domain/model/prestation_model.dart';
import 'details_demande/details_demande_appbar.dart';
import 'details_demande/details_description_prestation.dart';
import 'details_demande/details_image.dart';

class DetailDemandeRecu extends StatefulWidget {
  const DetailDemandeRecu({Key? key, required this.demande}) : super(key: key);

  final DataDemande demande;
  @override
  State<DetailDemandeRecu> createState() => _DetailDemandeRecuState();
}

class _DetailDemandeRecuState extends State<DetailDemandeRecu> {
  PrestationController prestationController = Get.find();
  late DataDemande demandeShow;
  bool isload = true;
  getDetailsProduct() async {
    setState(() {
      isload = true;
    });

    RepositoryImpl repositoryImpl = instance<RepositoryImpl>();
    await repositoryImpl
        .getDetailProduct(demandeShow.prestationDemandeData.offerId)
        .then((response) async {
      await response.fold((failure) {}, (data) async {
        demandeShow.prestation = (await data);
        setState(() {
          demandeShow;
        });
      });
    });
    setState(() {
      isload = false;
    });
  }

  @override
  void initState() {
    demandeShow = widget.demande;
    getDetailsProduct();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        margin: const EdgeInsets.all(10),
        child: demandeShow.prestation != null
            ? demandeShow.isReceive == true
                ? demandeShow.prestationDemandeData.isValid == false
                    ? DefaultButton(
                        text: "Accepter",
                        press: () {
                          showDialog(
                              context: context,
                              barrierDismissible: false,
                              builder: (context) {
                                return AlertDialog(
                                  backgroundColor: Colors.white,
                                  contentPadding: const EdgeInsets.all(12),
                                  content: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      const Text(
                                        'Vous-voulez vraiment accepter cette demande ?',
                                        textAlign: TextAlign.center,
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      DefaultButton(
                                        isActive: false,
                                        text: "Annuler",
                                        press: () {
                                          Navigator.pop(context);
                                        },
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      DefaultButton(
                                        text: "Accepter",
                                        press: () async {
                                          Navigator.pop(context);
                                          await prestationController
                                              .validateDemandePrestation(widget
                                                  .demande
                                                  .prestationDemandeData
                                                  .id)
                                              .then((value) {
                                            setState(() {
                                              demandeShow.prestationDemandeData
                                                  .isValid = value;
                                            });
                                          });
                                        },
                                      ),
                                    ],
                                  ),
                                );
                              });
                        },
                      )
                    : const SizedBox()
                : demandeShow.prestation != null
                    ? demandeShow.prestationDemandeData.isValid == true
                        ? DefaultButton(
                            text: "Voir le contrat",
                            press: () {
                              Get.to(PdfContratPrestation(
                                  demande: demandeShow,
                                  pdfLink: demandeShow
                                      .prestationDemandeData.contract!));
                            },
                          )
                        : const SizedBox()
                    : const SizedBox()
            : const SizedBox(),
      ),
      backgroundColor: ColorManager.backgroundColor,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(AppBar().preferredSize.height),
          child: DetailsDemandeAppBar(
            demandeData: demandeShow,
          )),
      body: isload == false
          ? demandeShow.prestation != null
              ? Hero(
                  tag: "demande",
                  child: ListView(
                    shrinkWrap: true,
                    children: [
                      DemandeImages(
                        dataDemande: demandeShow,
                      ),
                      DetatilsDescriptionPrestation(
                        demandeData: demandeShow,
                      )
                    ],
                  ),
                )
              : const EmptyComponenent(
                  message: "Cette prestation n'est plus disponible",
                )
          : const LoaderComponent(),
    );
  }
}
