import 'package:animations/animations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../app/constant.dart';
import '../../../app/functions.dart';
import '../../../domain/model/Product.dart';
import '../../../domain/model/user_model.dart';
import '../../common/product_card.dart';
import '../../details_produit/details_produit_screen.dart';

class PrestationInfo extends StatefulWidget {
  const PrestationInfo({Key? key, required this.productModel})
      : super(key: key);
  final ProductModel productModel;
  @override
  State<PrestationInfo> createState() => _PrestationInfoState();
}

class _PrestationInfoState extends State<PrestationInfo> {
  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
        builder: (context, user, child) => Container(
          margin:const EdgeInsets.only(bottom: 10),
          color: ColorManager.white,
          child: Column(
           
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Stack(
                     children: [
                       if (widget.productModel.images!.isNotEmpty) 
                       showImages(),
                       if (widget.productModel.images!.isEmpty) 
                       const SizedBox(  height: 150,width: double.infinity,child: Center(child: Icon(Icons.image),),),
                       
                     ],
                   ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    padding: const EdgeInsets.all(5),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                       Text(
                      "${widget.productModel.categorieProduct!.name} (${widget.productModel.title})",
                      style: getBoldTextStyle(
                        color: ColorManager.blue,
                      ),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    Text(
                      "Publié le :${convertDate(widget.productModel.publicationDate)}",
                      style: getRegularTextStyle(
                        color: ColorManager.grey,
                        fontSize: 10,
                      ),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    SizedBox(
                      height: 35,
                      child: Text(
                        "${widget.productModel.description}",
                        style: getRegularTextStyle(),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Prix : ${widget.productModel.price} FCFA par ${widget.productModel.unite}",
                      style: getBoldTextStyle(),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.person,
                          color: ColorManager.grey,
                          size: 14,
                        ),
                        Text(
                          "${widget.productModel.user != null ? widget.productModel.user!.firstName : ""} ${widget.productModel.user != null ? widget.productModel.user!.lastName : ""}",
                          style: getRegularTextStyle(color: ColorManager.grey),
                        ),
                        const Spacer(),
                      ],
                    ),
                    
                    const SizedBox(
                      height: 10,
                    ),
                    ],),
                  ),
                  // Text(
                  //   "(${widget.productModel.title})",
                  //   style: getBoldTextStyle(
                  //     color: ColorManager.blue,
                  //   ),
                  // ),
                 
                ],
              ),
        ));
  }

  Widget showImages() {
    return Column(children: [
       if (widget.productModel.images!.isNotEmpty)
              CachedNetworkImage(
                imageUrl: widget.productModel.images![0],
                imageBuilder: (context, imageProvider) => Container(
                  width: double.maxFinite,
                  height: 150,
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(5),
                        topRight: Radius.circular(5)),
                    image: DecorationImage(
                      scale: 1,
                      image: imageProvider,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                placeholder: (context, url) => Container(
                  color: ColorManager.grey.withOpacity(0.2),
                  height: 150,
                  width: double.infinity,
                ),
                errorWidget: (context, url, error) => Container(
                  color: ColorManager.grey.withOpacity(0.2),
                  height: 150,
                  width: double.infinity,
                  child: const Icon(Icons.error),
                ),
              ),
            if (widget.productModel.images!.isEmpty)
              Container(
                width: double.maxFinite,
                height: 150,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10)),
                ),
                child: const Icon(Icons.image),
              ),

    ],);
    
    
  }
}

class PrestationCard extends StatelessWidget {
  const PrestationCard({
    super.key,
    required this.productModel,
    this.ontap,
    this.isOffreAchatFind = false,
    this.isIntrant = false,
  });
  final ProductModel productModel;

  final bool isOffreAchatFind;
  final bool isIntrant;

  final ProductCardOnTaped? ontap;
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openElevation: 0,
      closedElevation: 0,
      transitionType: transitionType,
      transitionDuration: transitionDuration,
      openBuilder: (context, _) => DetailsProduitScreen(
          product: ProductDetailsArguments(
        product: productModel,
        isIntrant: isIntrant,
      )),
      closedBuilder: (context, VoidCallback openContainer) => PrestationInfo(
        productModel: productModel,
      ),
    );
  }
}
