import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/ressources/color_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../domain/model/prestation_model.dart';
import '../../common/pdf_screen.dart';
import '../controller/prestation_controller.dart';

class PdfContratPrestation extends StatefulWidget {
  const PdfContratPrestation(
      {Key? key, required this.pdfLink, required this.demande})
      : super(key: key);
  final DataDemande demande;

  final String pdfLink;
  @override
  State<PdfContratPrestation> createState() => _PdfContratPrestationState();
}

class _PdfContratPrestationState extends State<PdfContratPrestation> {
  PrestationController prestationController = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Contrat"),
          leading: const BackButtonCustom(),
          actions: [
            if (widget.demande.prestationDemandeData.isAccept == null ||
                widget.demande.prestationDemandeData.isAccept == false)
              TextButton(
                onPressed: () {
                  prestationController.validContrat(
                      widget.demande.prestationDemandeData.offerId);
                },
                child: Text(
                  "Accepter",
                  style: getBoldTextStyle(
                    color: ColorManager.blue,
                    fontSize: 15,
                  ),
                ),
              ),
          ],
        ),
        body: PdfScreenn(pdfLink: widget.pdfLink));
  }
}
