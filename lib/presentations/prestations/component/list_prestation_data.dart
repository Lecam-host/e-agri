import 'package:eagri/app/functions.dart';
import 'package:eagri/domain/model/prestation_model.dart';
import 'package:eagri/presentations/common/state/empty_component.dart';
import 'package:eagri/presentations/ressources/assets_manager.dart';
import 'package:eagri/presentations/ressources/icon_manager.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:page_transition/page_transition.dart';

import '../../ressources/color_manager.dart';
import 'detail_prestation_recu.dart';

class ListPrestationData extends StatelessWidget {
  const ListPrestationData({Key? key, required this.listPrestation})
      : super(key: key);
  final RxList<DataDemande> listPrestation;
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => listPrestation.isNotEmpty
          ? ListView.builder(
              shrinkWrap: true,
              itemCount: listPrestation.length,
              itemBuilder: (context, index) => Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      PageTransition(
                        type: PageTransitionType.bottomToTop,
                        child: DetailDemandeRecu(
                          demande: listPrestation[index],
                        ),
                        isIos: true,
                        duration: const Duration(milliseconds: 400),
                      ),
                    );
                  },
                  child: Stack(
                    children: [
                      Container(
                        width: double.infinity,
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        color: ColorManager.white,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                CircleAvatar(
                                  backgroundColor:
                                      listPrestation[index].isReceive == true
                                          ? ColorManager.primary4
                                          : ColorManager.primaryOpacity70,
                                  child: Image.asset(
                                    listPrestation[index].isReceive == true
                                        ? IconAssetManager.receiveIcon
                                        : IconAssetManager.sendIcon,
                                    width: 25,
                                  ),
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "${listPrestation[index].user.firstName.toString()} ${listPrestation[index].user.lastName.toString()}",
                                      style: getMeduimTextStyle(
                                        color: ColorManager.black,
                                        fontSize: 12,
                                      ),
                                    ),
                                    Text(
                                      'Demandeur',
                                      style: getRegularTextStyle(
                                        color: ColorManager.grey1,
                                        fontSize: 12,
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      "${convertDate(listPrestation[index].prestationDemandeData.dateStart)} - ${convertDate(listPrestation[index].prestationDemandeData.dateEnd)} ",
                                      style: getMeduimTextStyle(
                                        color: ColorManager.black,
                                        fontSize: 12,
                                      ),
                                    ),
                                    Text(
                                      'Periode de demandée',
                                      style: getRegularTextStyle(
                                        color: ColorManager.grey1,
                                        fontSize: 12,
                                      ),
                                    ),
                                    Text(
                                      listPrestation[index]
                                                  .prestationDemandeData
                                                  .isValid ==
                                              true
                                          ? "Acceptée"
                                          : "Pas en acceptée",
                                      style: getMeduimTextStyle(
                                        color: listPrestation[index]
                                                    .prestationDemandeData
                                                    .isValid ==
                                                true
                                            ? ColorManager.primary
                                            : ColorManager.red,
                                        fontSize: 12,
                                      ),
                                    ),
                                    Text(
                                      'Statut',
                                      style: getRegularTextStyle(
                                        color: ColorManager.grey1,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            IconButton(
                                onPressed: () {},
                                icon: const Icon(IconManager.arrowRight))
                            // if (listPrestation[index].prestationDemandeData.isValid ==
                            //     false)
                            // TextButton(
                            //     onPressed: () {
                            //       showDialog(
                            //           context: context,
                            //           barrierDismissible: false,
                            //           builder: (context) {
                            //             return AlertDialog(
                            //               backgroundColor: Colors.white,
                            //               contentPadding: const EdgeInsets.all(12),
                            //               content: Column(
                            //                 mainAxisSize: MainAxisSize.min,
                            //                 children: [
                            //                   const Text(
                            //                     'Vous-voulez vraiment accepter cette démande',
                            //                     textAlign: TextAlign.center,
                            //                   ),
                            //                   TextButton(
                            //                     onPressed: () {
                            //                       Navigator.pop(context);
                            //                     },
                            //                     child: Text('Non',
                            //                         style: getBoldTextStyle(
                            //                             color: ColorManager.grey)),
                            //                   ),
                            //                   TextButton(
                            //                     onPressed: () async {
                            //                       Navigator.pop(context);
                            //                       await prestationController
                            //                           .validateDemandePrestation(
                            //                               listPrestation[index]
                            //                                   .prestation
                            //                                   .id);
                            //                     },
                            //                     child: Text('Oui',
                            //                         style: getBoldTextStyle(
                            //                             color: ColorManager.primary,
                            //                             fontSize: 14)),
                            //                   ),
                            //                 ],
                            //               ),
                            //             );
                            //           });
                            //     },
                            //     child: Text(
                            //       "Accepter",
                            //       style: getMeduimTextStyle(
                            //         color: ColorManager.primary,
                            //         fontSize: 12,
                            //       ),
                            //     ))
                          ],
                        ),
                      ),
                      if (listPrestation[index].prestationDemandeData.isValid ==
                          true)
                        Positioned(
                          top: 50,
                          left: 30,
                          child: CircleAvatar(
                            radius: 10,
                            backgroundColor: ColorManager.primary,
                            child: Icon(
                              Icons.check,
                              size: 10,
                              color: ColorManager.white,
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
              ),
            )
          : const EmptyComponenent(
              message: "Aucune demande trouvée",
            ),
    );
  }
}
