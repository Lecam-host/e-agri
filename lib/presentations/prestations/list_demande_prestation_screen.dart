import 'package:eagri/presentations/common/buttons/back_button.dart';
import 'package:eagri/presentations/common/state/loader_component.dart';
import 'package:eagri/presentations/prestations/controller/prestation_controller.dart';
import 'package:eagri/presentations/ressources/styles_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../ressources/color_manager.dart';
import 'component/list_prestation_data.dart';

class ListDemandePrestationScreen extends StatefulWidget {
  const ListDemandePrestationScreen({Key? key}) : super(key: key);

  @override
  State<ListDemandePrestationScreen> createState() =>
      _ListDemandePrestationScreenState();
}

class _ListDemandePrestationScreenState
    extends State<ListDemandePrestationScreen>
    with SingleTickerProviderStateMixin {
  late TabController tabController;
  PrestationController prestationController = Get.put(PrestationController());
  @override
  void initState() {
    prestationController.getListDemandeReceive();
    prestationController.getListDemandeSend();

    tabController = TabController(
      length: 2,
      vsync: this,
    );
    super.initState();
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          leading: const BackButtonCustom(),
          title: const Text('Demandes de prestation'),
          bottom: TabBar(
            indicatorColor: ColorManager.primary,
            controller: tabController,
            tabs: [
              Text(
                "Envoyées",
                style: getBoldTextStyle(),
              ),
              Text(
                "Reçues",
                style: getBoldTextStyle(),
              ),
            ],
          ),
        ),
        body: TabBarView(
          controller: tabController,
          children: [
            Obx(
              () => prestationController.loadDemandeSend.value == true
                  ? const LoaderComponent()
                  : ListPrestationData(
                      listPrestation: prestationController.listPrestationSend,
                    ),
            ),
            Obx(
              () => prestationController.loadDemandeRecu.value == true
                  ? const LoaderComponent()
                  : ListPrestationData(
                      listPrestation: prestationController.listPrestationRecu,
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
